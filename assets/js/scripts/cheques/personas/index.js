class CTRL_APP {
    constructor() {
        this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    limpiar_busqueda() {
        $('form#form_filter').trigger("reset");
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    agregar() {
        $("body").LoadingOverlay("show");

        var url_tipo = 'cliente'
        if(tipo_persona_id == 2){
            url_tipo = 'proveedor'
        }

        setTimeout(function () {
            window.location.replace(PATH + "/cheques/personas/nuevo/"+url_tipo);
        }, 500)
    }

    administrar_persona($this) {
        var $id = $($this).data('renglon_id');
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/personas/editar?id="+$id);
        }, 500)
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            responsive: true,
            pageLength: 10,
            'ajax': {
                'url': PATH + 'cheques/api/listado_personas',
                'data' : function(){
                    var fecha = moment($('input#fecha').val());
                    var anio = null;
                    var mes = null;
                    var dia = null;
                    if(fecha.isValid()){
                        anio = fecha.format('YYYY');
                        mes = fecha.format('MM');
                        dia = fecha.format('DD');
                    }
                    return {
                        'anio': anio,
                        'mes': mes, 
                        'dia': dia,
                        'nomenclatura': $('select#nomenclatura option:selected').val(),
                        'folio': $('input#folio').val(),
                        'fecha': $('input#fecha').val(),
                        'tipo_persona_id': tipo_persona_id
                    };
                },
                'type': 'get'
            },
            'order': [
                [0, 'desc']
            ],
            'columns': [
                {
                    title: 'RFC',
                    data: 'rfc',
                    // className: 'text-left',
                    // render: function ( data, type, row ) {
                    //     return APP.zfill(parseInt(data),6);
                    // },    
                },
                {
                    title: 'Nombre',
                    render: function (data, type, row) {
                        var html = ((row.apellido1 != null) ? row.apellido1 + ' ' : '');
                        html += ((row.apellido2 != null) ? row.apellido2 + ' ' : '');
                        html += ((row.nombre != null) ? row.nombre + ' ' : '');
                        return $.trim(html).toUpperCase();
                    },
                },
                {
                    title: 'Tipo',
                    data: 'tipo_persona',
                    render: function (data, type, row) {
                        return ((data != null) ? data : '');
                    },
                },
                {
                    title: 'Télefono',
                    data: 'telefono',
                    render: function (data, type, row) {
                        return $.trim(data).toUpperCase();
                    },
                },
               
                {
                    title: 'Fecha de registro',
                    data: 'created_at',
                    render: function (data, type, row) {
                        var fecha = moment(data, 'YYYY-MM-DD HH:mm:ss.SSSSSS');
                        var html = '';
                        if (fecha.isValid()) {
                            html = '<center>' + fecha.format('HH:mm') + '<br/><small><b>' + fecha.format('DD/MM/YYYY') + '</b></small></center>';
                        }
                        return html;
                    },
                },
                {
                    title: 'Acciones',
                    width: '140px',
                    render: function (data, type, row) {
                        let ver_asientos = '';
                        ver_asientos = '<button title="Adm. asientos" data-renglon_id= "' + row.persona_id + '"  onclick="APP.administrar_persona(this)" class="btn btn-secondary btn-sm"><i class="fas fa-pencil-alt"></i></button>';
                        return '<center>' + ver_asientos + '</center>';
                    }
                }
            ]
        });
    }
}

const APP = new CTRL_APP();