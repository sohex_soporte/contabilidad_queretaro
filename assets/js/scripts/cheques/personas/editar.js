class CTRL_APP {

    initialized_domicilio = false;

    constructor () {
        var ids = $('select#id_poliza_nomenclatura option:selected').val();
        this.ver_polizas(ids);
    }


    get_estados(){
        $.get( PATH+'/api/domicilios/getEstados', function( data ) {
            try {

                $('select#id_estado').remove('onchange');

                var value_default = $('select#id_estado').attr('value');

                if(value_default == undefined){
                    APP.borrar_municipios();
                    APP.borrar_colonias();
                    APP.borrar_codigo_postal();
                }
                
                $.each(data.data, function (i, item) {
                    $('select#id_estado').append($('<option>', { 
                        value: item.id,
                        text : item.nombre 
                    }));
                });

                $('select#id_estado').select2({
                    theme: 'bootstrap4',
                });

                if(value_default == undefined){
                    $('select#id_estado').val('').trigger('change')
                }else{
                    $('select#id_estado').val(value_default).trigger('change');
                    APP.get_municipios();
                    $('select#id_estado').removeAttr('value');
                }

                $('select#id_estado').attr('onchange', 'APP.get_municipios()');
                
            } catch (error) {
                
            }    
        }, "json" );   
    }

    get_municipios(){
        $.get( PATH+'/api/domicilios/getMunicipios',{ id: $('select#id_estado option:selected').val() }, function( data ) {
            try {

                $('select#id_municipio').remove('onchange');

                var value_default = $('select#id_municipio').attr('value');
                
                if(value_default == undefined){
                    APP.borrar_colonias();
                    APP.borrar_codigo_postal();
                }
                
                $.each(data.data, function (i, item) {
                    var option = $('<option>', { 
                        value: item.id,
                        text : item.nombre,
                        
                    })
                    $('select#id_municipio').append(option);
                });

                if(value_default == undefined){
                    $("select#id_municipio").select2("close")
                    $('select#id_municipio').val('').trigger('change')
                }else{
                
                    $('select#id_municipio option[value='+value_default+']').prop('selected', true);
                    $('select#id_municipio').trigger('change');
                    APP.get_colonias();
                    $('select#id_municipio').removeAttr('value');
                }
                $('select#id_municipio').attr('onchange', 'APP.get_colonias()');

            } catch (error) {
                
            }    
        }, "json" );   
    }

    
    get_colonias(){
        $.get( PATH+'/api/domicilios/getColonias',{ id: $('select#id_municipio option:selected').val() }, function( data ) {
            try {

                $('select#id_colonia').remove('onchange');

                var value_default = $('select#id_colonia').attr('value');
                
                if(value_default == undefined){
                    APP.borrar_codigo_postal();
                }

                $.each(data.data, function (i, item) {
                    var option = $('<option>', { 
                        value: item.id,
                        text : item.nombre 
                    });
                    
                    option.attr({
                        codigo_postal: item.codigo_postal
                    });

                    $('select#id_colonia').append(option);

                });

                if(value_default == undefined){
                    $("select#id_colonia").select2("close")
                    $('select#id_colonia').val('').trigger('change')
                }else{
                
                    $('select#id_colonia option[value='+value_default+']').prop('selected', true);
                    $('select#id_colonia').trigger('change');
                    $('select#id_colonia').removeAttr('value');
                }
                
                $('select#id_colonia').attr('onchange', 'APP.get_codigo_postal()');

            } catch (error) {
            }    
        }, "json" );   
    }


    get_codigo_postal(){
        var cp = $('select#id_colonia option:selected').attr('codigo_postal');
        $("input#codigo_postal").val(cp)
    }

    borrar_municipios(){
        $('select#id_municipio option').remove();
        $('select#id_municipio').val('').trigger('change')
    }

    borrar_colonias(){
        $('select#id_colonia option').remove();
        $('select#id_colonia').val('').trigger('change')
    }

    borrar_codigo_postal(){
        $("input#codigo_postal").attr('')
    }


    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    ver_polizas(valor){
        if(valor == 'DO'){
            $('div#polizas_fijas_content').show();
        }else{
            $('div#polizas_fijas_content').hide();
        }

    }

    guardar(){

        // var data_save = $('form#data_form').serializeArray();
        


        var data_send = $('form#data_form').serializeArray();
        data_send.push({
            name: 'estado',
            value: $('select#id_estado option:selected').text()
        })

        data_send.push({
            name: 'municipio',
            value: $('select#id_municipio option:selected').text()
        })

        data_send.push({
            name: 'colonia',
            value: $('select#id_colonia option:selected').text()
        })

        data_send.push({ name: "identificador", value: identificador });
        console.log(data_send);

        $('small.form-text.text-danger').html('');
        setTimeout(() => {
            $.ajax({
                type: 'post',
                url: PATH+'/cheques/api/editar_persona.json',
                data: data_send,
                dataType: "json",
                success: function(data, status, xhr){
                    console.log(data);
                    if (data.status != "error") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: 'El registro se ha guardado correctamente',
                        }).then((result) => {
                            $("body").LoadingOverlay("show");
                            setTimeout(function () {
                                window.location.replace(PATH+"/cheques/personas?id="+data.data.cheque_id);
                            }, 250)
                        });
                    } else {
                        $.each(data.message, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    }

                }
            });
        },500);
        
    }
}
  
  var APP;


  $(function() {

    setTimeout(() => {

        APP = new CTRL_APP();

        APP.get_estados();
    }, 500);


    $('select#id_municipio').select2({
        theme: 'bootstrap4',
    });
    

    $('select#id_colonia').select2({
        theme: 'bootstrap4',
    });

    $('select#id_poliza_nomenclatura').select2({
        theme: 'bootstrap4',
    });

    $('select#id_cuenta_subtotal').select2({
        theme: 'bootstrap4',
    });
    $('select#subtotal_tipo').select2({
        theme: 'bootstrap4',
    });

    $('select#id_cuenta_iva').select2({
        theme: 'bootstrap4',
    });
    $('select#iva_tipo').select2({
        theme: 'bootstrap4',
    });

    $('select#id_cuenta_total').select2({
        theme: 'bootstrap4',
    });
    $('select#total_tipo').select2({
        theme: 'bootstrap4',
    });
  })