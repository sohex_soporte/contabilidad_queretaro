var app;
var ctx;
var cw;
var ch;
var cx;
var cy;
var factorDeAlisamiento;
var puntos;
var Trazados;
var dibujar;
var tag_firma;
class CTRL_APP {
    constructor() {
        this.renglon_id = null;
        // this.cargar_tabla();

        try {
            if(id_datos_cheque_estatus == 2){
                $('div#contenedor_cheque').find('input, textarea, button, select').prop('disabled',true);
            }
        } catch (error) {
            
        }
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    formatState(data) {
        var element = data.element;
        var html = '<small><b>' + data.text + '</b></small>';
        html += '<br/><span>' + $(element).attr('desc') + '</span>';
        return $(html);
    }

    formatNomenclaturas(data) {
        var html = '<small><b>' + data.id + '</b></small>';
        html += '<br/><span>' + data.text + '</span>';
        return $(html);
    }

    modal_agregar_asiento() {

        $.ajax({
            type: 'get',
            url: PATH + '/cheques/api/modal_agregar_asiento.json',
            data: {
                transaccion_id: transaccion_id
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();
                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Agregar asiento');
                    $('#modal_agregar_asiento_button').attr('onclick','APP.modal_agregar_asiento_guardar();');

                    $('select#modal_cuentas').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento'),
                        templateResult: APP.formatState
                    });
                    $("select#modal_cuentas").val('').trigger('change');

                    $('select#modal_tipo').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento')
                    });
                    $("select#modal_tipo").val('').trigger('change');

                    try {
                        $('textarea#concepto').html(data.data.asiento.concepto);
                    } catch (error) {
                    }


                    // $('select#tipo_unidad').select2({
                    //     theme: 'bootstrap4',
                    //     dropdownParent: $('div#modal_agregar_asiento'),
                    //     templateResult: APP.formatState
                    // });
                    // $('select#tipo_unidad option[value=act]').prop('selected', true);
                    // $("select#tipo_unidad").trigger('change');

                }, 250);
            },
        });
    }









    tag_firma = null;
    tag_modal =  null;
    firmar (dom_firma, dom_modal) {
        if (dom_modal != undefined) {
            APP.tag_modal = dom_modal;
            $('div#' + APP.tag_modal).modal('hide');
        } else {
            APP.tag_modal = null;
        }
        $('#firmaDigital').appendTo("body").modal('show');

        if (dom_modal != undefined) {
            $('#firmaDigital').on('hidden.bs.modal', function () {
                if (APP.tag_modal != null) {
                    $('div#' + APP.tag_modal).modal('show');
                }
            });
        }

        // firma_rapida();
        // return;


        setTimeout(() => {
            APP.tag_firma = dom_firma;


            var canvas = document.getElementById("canvas");
            ctx = canvas.getContext("2d");
            cw = canvas.width = $('canvas#canvas').width();
                cx = cw / 2;
            ch = canvas.height = $('canvas#canvas').height();
            // console.log(ch);
                cy = ch / 2;

            dibujar = false;
            factorDeAlisamiento = 1;
            Trazados = [];
            puntos = [];
            ctx.lineJoin = "miter";

            canvas.addEventListener('mousedown', function (evt) {
                // console.log('mousedown');
                dibujar = true;
                //ctx.clearRect(0, 0, cw, ch);
                puntos.length = 0;
                ctx.beginPath();

            }, false);

            canvas.addEventListener('mouseup', function (evt) {
                // console.log('mouseup');
                APP.redibujarTrazados();
            }, false);

            canvas.addEventListener("mouseout", function (evt) {
                // console.log('mouseout');
                APP.redibujarTrazados();
            }, false);

          
            canvas.addEventListener("mousemove", function (evt) {
                // console.log('mousemove');
                if (dibujar) {
                    var m = APP.oMousePos(canvas, evt);
                    puntos.push(m);
                    ctx.lineTo(m.x, m.y);
                    ctx.stroke();
                }
            }, false);

            


            canvas.addEventListener('touchstart', function (e) {
                // console.log('touchstart');
                dibujar = true;
                //ctx.clearRect(0, 0, cw, ch);
                puntos.length = 0;
                ctx.beginPath();

                mousePos = APP.getTouchPos(canvas, e);
                // console.log(mousePos);
                e.preventDefault(); // Prevent scrolling when touching the canvas
                var touch = e.touches[0];
                var mouseEvent = new MouseEvent("mousedown", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas.dispatchEvent(mouseEvent);
                

                //APP.redibujarTrazados();
            }, false);

            canvas.addEventListener("touchend", function (e) {
                e.preventDefault(); // Prevent scrolling when touching the canvas
                var mouseEvent = new MouseEvent("mouseup", {});
                canvas.dispatchEvent(mouseEvent);
            }, false);
            canvas.addEventListener("touchleave", function (e) {
                // Realiza el mismo proceso que touchend en caso de que el dedo se deslice fuera del canvas
                e.preventDefault(); // Prevent scrolling when touching the canvas
                var mouseEvent = new MouseEvent("mouseup", {});
                canvas.dispatchEvent(mouseEvent);
            }, false);
            canvas.addEventListener("touchmove", function (e) {
                e.preventDefault(); // Prevent scrolling when touching the canvas
                var touch = e.touches[0];
                var mouseEvent = new MouseEvent("mousemove", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas.dispatchEvent(mouseEvent);
            }, false);





        }, 50);
    }

    onerror ($this) {
        $this.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
        // $this.onerror = null;
    }
    // ---------------------------------------------------------
    limpiar () {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        var canvas = document.getElementById("draw-canvas");
        // canvas.width = canvas.width;
        Trazados.length = 0;
        puntos.length = 0;
    }
    reducirArray (n, elArray) {
        var nuevoArray = [];
        nuevoArray[0] = elArray[0];
        for (var i = 0; i < elArray.length; i++) {
            if (i % n == 0) {
                nuevoArray[nuevoArray.length] = elArray[i];
            }
        }
        nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
        Trazados.push(nuevoArray);
    }

    calcularPuntoDeControl (ry, a, b) {
        var pc = {}
        pc.x = (ry[a].x + ry[b].x) / 2;
        pc.y = (ry[a].y + ry[b].y) / 2;
        return pc;
    }

    alisarTrazado (ry) {
        if (ry.length > 1) {
            var ultimoPunto = ry.length - 1;
            ctx.beginPath();
            ctx.moveTo(ry[0].x, ry[0].y);
            for ( var i = 1; i < ry.length - 2; i++) {
                var pc = APP.calcularPuntoDeControl(ry, i, i + 1);
                ctx.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
            }
            ctx.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
            ctx.stroke();
        }
    }





    redibujarTrazados () {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        APP.reducirArray(factorDeAlisamiento, puntos);
        for (var i = 0; i < Trazados.length; i++)
            APP.alisarTrazado(Trazados[i]);
    }

    oMousePos (canvas, evt) {
        var ClientRect = canvas.getBoundingClientRect();
        return { //objeto
            x: Math.round(evt.clientX - ClientRect.left),
            y: Math.round(evt.clientY - ClientRect.top)
        }
    }

    oTouchPos (canvas, evt) {
        var ClientRect = canvas.getBoundingClientRect();
        var touches = evt.changedTouches;

        var mouseEvent = new MouseEvent("mousedown", {
            clientX: touches.clientX,
            clientY: touches.clientY
        });
        canvas.dispatchEvent(mouseEvent);
        //console.log(mouseEvent);

        return { //objeto
            x: Math.round(touches[0].pageX - ClientRect.left),
            y: Math.round(touches[0].pageY - ClientRect.top)
        }
    }
    general_convertCanvasToImage = null;
    convertCanvasToImage () {
        let canvas = document.getElementById("canvas");
        // let image = new Image();
        // image.src = canvas.toDataURL();
        // console.log(image);

        $('img#' + APP.tag_firma).attr('src', canvas.toDataURL());
        $('input[name=' + APP.tag_firma + ']').val(canvas.toDataURL());
        // this.finalizar_cheque_guardar();
        // return image;

        if($('select#tipo_movimeinto option:selected').val() == 1){

            if($.trim(canvas.toDataURL()).length > 0 ){
                console.log($.trim(canvas.toDataURL()).length);
                this.finalizar_cheque_guardar();
            }else{
                $('small#msg_firma').html('El campo Firma de autorización es obligatorio')
            }

        }else{
            this.finalizar_cheque_guardar();
        }

        
    }
    // ---------------------------------------------------------------------------

    getTouchPos(canvasDom, touchEvent) {
        var rect = canvasDom.getBoundingClientRect();
        // console.log(touchEvent);
        /*
        Devuelve el tamaño de un elemento y su posición relativa respecto
        a la ventana de visualización (viewport).
        */
        return {
            x: touchEvent.touches[0].clientX - rect.left, // Popiedad de todo evento Touch
            y: touchEvent.touches[0].clientY - rect.top
        };
    }


    finalizar_cheque_guardar(){
        
        
        // this.firmar('firma_tecnico_equipo','modal_equipo_diagnostico')

        $.ajax({
            type: 'post',
            url:  PATH+'/cheques/api/finalizar_cheque.json',
            dataType: "json",
            data:{
                transaccion_id: transaccion_id,
                id_datos_cheque: datos_cheque_id,
                tipo_movimeinto: $('select#tipo_movimeinto option:selected').val(),
                firma_autorizacion: $('input#firma_tecnico_equipo').val()
            },
            success: function(data, status, xhr){
                window.location.reload();
            }   
        });
    }





    finalizar_cheque(){
        
        
        this.firmar('firma_tecnico_equipo','modal_equipo_diagnostico')

        // $.ajax({
        //     type: 'post',
        //     url:  PATH+'/cheques/api/finalizar_cheque.json',
        //     dataType: "json",
        //     data:{
        //         transaccion_id: transaccion_id,
        //         id_datos_cheque: datos_cheque_id,
        //         firma_autorizacion: $('input#firma_tecnico_equipo').val()
        //     },
        //     success: function(data, status, xhr){
        //         window.location.reload();
        //     }   
        // });
    }

    modal_editar_asiento(_this) {

        APP.renglon_id = $(_this).data('renglon_id');

        $.ajax({
            type: 'get',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: {
                renglon_id: $(_this).data('renglon_id'),
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();


                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Editar asiento');
                    $('#modal_agregar_asiento_button').attr('onclick','APP.modal_editar_asiento_guardar();');

                    $('select#modal_cuentas').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento'),
                        templateResult: APP.formatState
                    });

                    $('select#modal_tipo').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento')
                    });

                    try {
                        $("div#modal_agregar_asiento select#modal_cuentas option[value="+data.data.asiento.cuenta+"]").prop('selected',true);
                        $("div#modal_agregar_asiento select#modal_cuentas").trigger('change');
                    } catch (error) {
                        
                    }

                    try {
                        // console.log(data);
                        if(data.data.asiento.cargo > 0){
                            $("div#modal_agregar_asiento select#modal_tipo option[value=1]").prop('selected',true);
                        }else{
                            $("div#modal_agregar_asiento select#modal_tipo option[value=2]").prop('selected',true);
                        }
                        $("div#modal_agregar_asiento select#modal_tipo").trigger('change');
                    } catch (error) {
                    }

                    try {
                        if(data.data.asiento.cargo > 0){
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento.cargo);
                        }else{
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento.abono);
                        }
                    } catch (error) {
                        
                    }

                    try {
                        $("div#modal_agregar_asiento  #concepto").val(data.data.asiento.concepto);
                    } catch (error) {
                        
                    }


                    // $('select#tipo_unidad').select2({
                    //     theme: 'bootstrap4',
                    //     dropdownParent: $('div#modal_agregar_asiento'),
                    //     templateResult: APP.formatState
                    // });
                    // $('select#tipo_unidad option[value=act]').prop('selected', true);
                    // $("select#tipo_unidad").trigger('change');

                }, 250);
            },
        });
    }

    cancelar_asiento($this) {

        var id = $($this).data('renglon_id');

        $.ajax({
            type: 'get',
            url: PATH + '/asientos/api/aplicar_asiento',
            data: {
                'asiento_id': id,
                'estatus': 'CANCELAR'
            },
            success: function (data, status, xhr) {
                
            },
        });
    }

    modal_agregar_asiento_guardar() {
        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "polizas_id",
            value: polizas_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });

        $('small.form-text.text-danger').html('');
        $.ajax({
            type: 'post',
            url: PATH + '/cheques/api/modal_agregar_asiento.json',
            data: data_send,
            success: function (data, status, xhr) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
        });
    }

    modal_editar_asiento_guardar() {
        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "polizas_id",
            value: polizas_id
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "renglon_id",
            value: APP.renglon_id
        });

        $('small.form-text.text-danger').html('');
        $.ajax({
            type: 'post',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: data_send,
            success: function (data, status, xhr) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
        });
    }

    calcular_total() {
        try {
            var precio_unitario = $('input#precio_unitario').val();
            var cantidad = $('input#cantidad').val();
            $('input#precio').val( precio_unitario * cantidad);    
        } catch (error) {
            $('input#precio').val(0);
        }
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            responsive: true,
            pageLength: 50,
            'ajax': {
                'url': PATH + 'asientos/api/detalle',
                'data': function () {
                    return {
                        'transaccion_id': transaccion_id
                    };
                },
                'type': 'get'
            },
            'order': [],
            'columns': [{
                    title: 'No. Asiento',
                    data: 'asiento_id',
                    className: 'text-left',
                    render: function (data, type, row) {
                        return APP.zfill(parseInt(data), 6);
                    },
                },
                {
                    title: 'Concepto',
                    data: 'concepto'
                },
                {
                    title: 'Cuenta',
                    render: function (data, type, row) {
                        return '' + row.cuenta + '<br/><small><b>' + row.cuenta_descripcion + '</b></small>';
                    }
                },
                // {
                //     title: 'Precio unitario',
                //     data: 'precio_unitario',
                //     className: 'text-right',
                //     render: function (data, type, row) {
                //         return (data > 0) ? APP.formatMoney(data) : '';
                //     }

                // },
                // {
                //     title: 'Cantidad',
                //     data: 'cantidad_articulos',
                //     className: 'text-right',
                //     render: function (data, type, row) {
                //         return (data > 0) ? data : '';
                //     }

                // },
                {
                    title: 'Cargo',
                    data: 'cargo',
                    className: 'text-right',
                    render: function (data, type, row) {
                        return (data > 0) ? APP.formatMoney(data) : '-';
                    }

                },
                {
                    title: 'Abono',
                    data: 'abono',
                    className: 'text-right',
                    render: function (data, type, row) {
                        return (data > 0) ? APP.formatMoney(data) : '-';
                    }
                },
               
                // { 
                //     title: 'Acumulado general',
                //     data: 'acumulado',
                //     className: 'text-right',
                //     render: function ( data, type, row ) {
                //         //return (data >= 0)? '<span class="text-success float-left"><i class="fas fa-plus-square"></i></span>'+APP.formatMoney(data) :'<span class="text-danger float-left"><i class="fas fa-minus-square"></i></span>'+(APP.formatMoney(data*-1));
                //         return (data >= 0)? ''+APP.formatMoney(data) :'<span class="text-danger float-left"><i class="fas fa-minus-square"></i></span>'+(APP.formatMoney(data*-1));
                //     }  
                // },
                {
                    title: 'Estatus',
                    data: 'estatus_id',
                    render: function (data, type, row) {
                        var html = '';
                        switch (data) {
                            case 'APLICADO':
                                html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                            case 'ANULADO':
                                html = '<h6><span class="badge badge-secondary">Anulado</span></h6>';
                                break;
                            case 'POR_APLICAR':
                                html = '<h6><span class="badge badge-primary">Por aplicar</span></h6>';
                                break;
                            default:
                                html = '<h6><span class="badge badge-warning">Cancelado</span></h6>';
                                //html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                        }
                        return html;
                    },
                },
                {
                    title: 'Fecha de registro',
                    data: 'fecha_registro',
                    render: function (data, type, row) {
                        var fecha = moment(data, 'YYYY-MM-DD HH:mm:ss.SSSSSS');
                        var html = '';
                        if (fecha.isValid()) {
                            html = '<center>' + fecha.format('HH:mm:ss') + '<br/><small><b>' + fecha.format('DD/MM/YYYY') + '</b></small></center>';
                        }
                        return html;
                    },
                },
                // {
                //     title: 'Acciones',
                //     width: '140px',
                //     render: function (data, type, row) {
                //         let cancelar = '';
                //         cancelar = '<button title="Cancelar" data-renglon_id= "' + row.asiento_id + '"  onclick="APP.cancelar_asiento(this)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                //         return '<center>' + cancelar + '</center>';
                //     }
                // }
            ]
        });
    }

    eliminar_renglon(_this) {
        Swal.fire({
            title: 'Desea eliminar el asiento?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            denyButtonText: 'Cancelar',
        }).then((result) => {
            // console.log(result);
            if (result.value == true) {
                $.ajax({
                    url: PATH + '/asientos/api/aplicar_asiento',
                    type: 'POST',
                    data: {
                        asiento_id: $(_this).data('renglon_id'),
                        estatus: "ANULADO"
                    },
                    success: function(response) {
                        if (response.estatus != "error") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito!',
                                text: response.mensaje,
                            }).then((result) => {
                                $("body").LoadingOverlay("show");
                                setTimeout(function () {
                                    location.reload();
                                }, 250)
                            });

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    },
                });
            } else {
                return false;
            }
        })
    }
    
    // eliminar_operacion(_this) {
    //     Swal.fire({
    //         title: 'Desea eliminar la operación?',
    //         showDenyButton: true,
    //         showCancelButton: false,
    //         confirmButtonText: 'Aceptar',
    //         denyButtonText: 'Cancelar',
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax({
    //                 url: PATH + '/administrador/api/reportes/delete_operacion',
    //                 type: 'POST',
    //                 data: {
    //                     operacion_id: $(_this).data('operacion_id'),
    //                 },
    //                 success: function(response) {
    //                     if (response.estatus != "error") {
    //                         Swal.fire({
    //                             icon: 'success',
    //                             title: 'Éxito!',
    //                             text: response.mensaje,
    //                         }).then((result) => {
    //                             $("body").LoadingOverlay("show");
    //                             setTimeout(function () {
    //                                 location.reload();
    //                             }, 250)
    //                         });

    //                     } else {
    //                         Swal.fire({
    //                             icon: 'error',
    //                             title: 'Error',
    //                             text: response.mensaje,
    //                         })
    //                     }
    //                 },
    //             });
    //         } else {
    //             return false;
    //         }
    //     })
    // }

}

const APP = new CTRL_APP();