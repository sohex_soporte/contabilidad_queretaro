class CTRL_APP {
    constructor() {
        // this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    limpiar_busqueda() {
        $('form#form_filter').trigger("reset");
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    agregar_cheque() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/nuevo");
        }, 500)
    }

    administrar_personas() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/personas");
        }, 500)
    }

    administrar_asientos($this) {
        var $id = $($this).data('renglon_id');
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/asientos?id="+$id);
        }, 500)
    }
    administrar_cheque($this) {
        var $id = $($this).data('renglon_id');
        if(getAcrobatInfo.acrobat == 'installed'){
            $("body").LoadingOverlay("show");
            
            var ifrm = document.createElement("iframe");
            ifrm.setAttribute("src", PATH + "/cheques/imprimir/index?id="+$id);
            ifrm.setAttribute("class", 'embed-responsive-item');
            ifrm.setAttribute("onload", '$("body").LoadingOverlay("hide");');
            // ifrm.style.width = "640px";
            // ifrm.style.height = "480px";

            $('div#modal_pdf div.modal-body').html('<div class="embed-responsive embed-responsive-21by9">'+ifrm.outerHTML+'</div>');
            $('div#modal_pdf div.modal-body div.embed-responsive').html(ifrm);
            
            $('div#modal_pdf').modal();
        }else{
            window.open(PATH + "/cheques/imprimir/index?id="+$id,'_blank');
        }
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            responsive: true,
            pageLength: 10,
            'ajax': {
                'url': PATH + 'cheques/api/detalleAll',
                'data' : function(){
                    var fecha = moment($('input#fecha').val());
                    var anio = null;
                    var mes = null;
                    var dia = null;
                    if(fecha.isValid()){
                        anio = fecha.format('YYYY');
                        mes = fecha.format('MM');
                        dia = fecha.format('DD');
                    }
                    return {
                        'anio': anio,
                        'mes': mes, 
                        'dia': dia,
                        'nomenclatura': $('select#nomenclatura option:selected').val(),
                        'folio': $('input#folio').val(),
                        'fecha': $('input#fecha').val()
                    };
                },
                'type': 'get'
            },
            'order': [
                [0, 'desc']
            ],
            'columns': [{
                    title: 'Folio',
                    data: 'folio',
                    render: function (data, type, row) {
                        return $.trim(data).toUpperCase();
                    },
                },
                {
                    title: 'Poliza',
                    data: 'PolizaNomenclatura_id',
                    render: function (data, type, row) {
                        return ((data != null) ? data + row.dia : '');
                    },
                },
                {
                    title: 'Ejercicio',
                    data: 'ejercicio',
                    render: function (data, type, row) {
                        return ((data != null) ? data : '');
                    },
                },
                {
                    title: 'Nombre',
                    render: function (data, type, row) {
                        var html = ((row.apellido1 != null) ? row.apellido1 + ' ' : '');
                        html += ((row.apellido2 != null) ? row.apellido2 + ' ' : '');
                        html += ((row.nombre != null) ? row.nombre + ' ' : '');
                        return $.trim(html);
                    },
                },
                {
                    title: 'RFC',
                    data: 'rfc',
                    // className: 'text-left',
                    // render: function ( data, type, row ) {
                    //     return APP.zfill(parseInt(data),6);
                    // },    
                },
                {
                    title: 'Fecha de registro',
                    data: 'datos_cheque_fecha_registro',
                    render: function (data, type, row) {
                        var fecha = moment(data, 'YYYY-MM-DD HH:mm:ss.SSSSSS');
                        var html = '';
                        if (fecha.isValid()) {
                            html = '<center>' + fecha.format('HH:mm:ss') + '<br/><small><b>' + fecha.format('DD/MM/YYYY') + '</b></small></center>';
                        }
                        return html;
                    },
                },
                {
                    title: 'Acciones',
                    width: '140px',
                    render: function (data, type, row) {
                        let ver_asientos = '';
                        var imprimir = '<button title="Adm. asientos" data-renglon_id= "' + row.datos_cheque_id + '"  onclick="APP.administrar_cheque(this)" class="btn btn-secondary btn-sm"><i class="fas fa-print"></i></button>';
                        ver_asientos = '<button title="Adm. asientos" data-renglon_id= "' + row.datos_cheque_id + '"  onclick="APP.administrar_asientos(this)" class="btn btn-secondary btn-sm"><i class="fas fa-pencil-alt"></i></button>';
                        return '<center>' + imprimir + '&nbsp;' + ver_asientos + '</center>';
                    }
                }
            ]
        });
    }
}


var getAcrobatInfo = new function() {

    var getBrowserName = function() {
        return this.name = this.name || function() {
            var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

            if(userAgent.indexOf("chrome") > -1){
                return "chrome";
            } else if(userAgent.indexOf("safari") > -1){
                return "safari";
            } else if(userAgent.indexOf("msie") > -1 || navigator.appVersion.indexOf('Trident/') > 0){
                return "ie";
            } else if(userAgent.indexOf("firefox") > -1){
                return "firefox";
            } else {
                //return "ie";
                return userAgent;
            }
        }();
    };

    var getActiveXObject = function(name) {
        try { return new ActiveXObject(name); } catch(e) {}
    };

    var getNavigatorPlugin = function(name) {
        for(key in navigator.plugins) {
            var plugin = navigator.plugins[key];
            if(plugin.name == name) return plugin;
        }
    };

    var getPDFPlugin = function() {
        return this.plugin = this.plugin || function() {
            if(getBrowserName() == 'ie') {
                //
                // load the activeX control
                // AcroPDF.PDF is used by version 7 and later
                // PDF.PdfCtrl is used by version 6 and earlier
                return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
            } else {
                return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
            }
        }();
    };

    var isAcrobatInstalled = function() {
        return !!getPDFPlugin();
    };

    var getAcrobatVersion = function() {
        try {
            var plugin = getPDFPlugin();

            if(getBrowserName() == 'ie') {
                var versions = plugin.GetVersions().split(',');
                var latest = versions[0].split('=');
                return parseFloat(latest[1]);
            }

            if(plugin.version) return parseInt(plugin.version);
            return plugin.name
        }
        catch(e) {
            return null;
        }
    }

    //
    // The returned object
    //
    return {
        browser: getBrowserName(),
        acrobat: isAcrobatInstalled() ? 'installed' : false,
        acrobatVersion: getAcrobatVersion()
    };
};

const APP = new CTRL_APP();