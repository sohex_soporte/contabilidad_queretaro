class CTRL_APP {
    constructor() {
        
    }

    modal_detalle_anticipo($this){
        var btn = $($this).data('renglon_id');

        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.href = PATH + "/cheques/anticipos/detalle/"+btoa(btn);
        }, 500)
    }

    modal_detalle_anticipo2($this){
        var btn = $($this).data('renglon_id');

        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.href = PATH + "/cheques/anticipos/buscar_cheque?id="+btn;
        }, 500)
    }




    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    limpiar_busqueda() {
        $('form#form_filter').trigger("reset");
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    agregar_cheque() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/nuevo");
        }, 500)
    }

    
    
    

    administrar_personas() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/personas");
        }, 500)
    }

    administrar_asientos($this) {
        var $id = $($this).data('renglon_id');
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/asientos?id="+$id);
        }, 500)
    }
    administrar_cheque($this) {
        var $id = $($this).data('renglon_id');
        if(getAcrobatInfo.acrobat == 'installed'){
            $("body").LoadingOverlay("show");
            // console.log(PATH + "/cheques/imprimir/index?id="+$id);
            var ifrm = document.createElement("iframe");
            ifrm.setAttribute("src", PATH + "/cheques/imprimir/index?id="+$id+'&_='+ Date.now());
            ifrm.setAttribute("class", 'embed-responsive-item');
            ifrm.setAttribute("onload", '$("body").LoadingOverlay("hide");');
            // ifrm.style.width = "640px";
            // ifrm.style.height = "480px";

            $('div#modal_pdf div.modal-body').html('<div class="embed-responsive embed-responsive-21by9">'+ifrm.outerHTML+'</div>');
            $('div#modal_pdf div.modal-body div.embed-responsive').html(ifrm);
            
            $('div#modal_pdf').modal();
        }else{
            window.open(PATH + "/cheques/imprimir/index?id="+$id,'_blank');
        }
    }

}


var getAcrobatInfo = new function() {

    var getBrowserName = function() {
        return this.name = this.name || function() {
            var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

            if(userAgent.indexOf("chrome") > -1){
                return "chrome";
            } else if(userAgent.indexOf("safari") > -1){
                return "safari";
            } else if(userAgent.indexOf("msie") > -1 || navigator.appVersion.indexOf('Trident/') > 0){
                return "ie";
            } else if(userAgent.indexOf("firefox") > -1){
                return "firefox";
            } else {
                //return "ie";
                return userAgent;
            }
        }();
    };

    var getActiveXObject = function(name) {
        try { return new ActiveXObject(name); } catch(e) {}
    };

    var getNavigatorPlugin = function(name) {
        for(key in navigator.plugins) {
            var plugin = navigator.plugins[key];
            if(plugin.name == name) return plugin;
        }
    };

    var getPDFPlugin = function() {
        return this.plugin = this.plugin || function() {
            if(getBrowserName() == 'ie') {
                //
                // load the activeX control
                // AcroPDF.PDF is used by version 7 and later
                // PDF.PdfCtrl is used by version 6 and earlier
                return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
            } else {
                return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
            }
        }();
    };

    var isAcrobatInstalled = function() {
        return !!getPDFPlugin();
    };

    var getAcrobatVersion = function() {
        try {
            var plugin = getPDFPlugin();

            if(getBrowserName() == 'ie') {
                var versions = plugin.GetVersions().split(',');
                var latest = versions[0].split('=');
                return parseFloat(latest[1]);
            }

            if(plugin.version) return parseInt(plugin.version);
            return plugin.name
        }
        catch(e) {
            return null;
        }
    }

    //
    // The returned object
    //
    return {
        browser: getBrowserName(),
        acrobat: isAcrobatInstalled() ? 'installed' : false,
        acrobatVersion: getAcrobatVersion()
    };
};

const APP = new CTRL_APP();