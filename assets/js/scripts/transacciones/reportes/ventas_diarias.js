class CTRL_APP {
    constructor() {
        this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    limpiar_busqueda() {
        $('form#form_filter').trigger("reset");
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    agregar_cheque() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/nuevo");
        }, 500)
    }

    administrar_personas() {
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/personas");
        }, 500)
    }

    abrir_pdf(){
        var url = "https://mylsa.iplaneacion.com/dms/contabilidad_apis/contabilidad/transacciones/ventas_diarias/documento?"+$.param({
            fecha_inicio: $('input#fecha_ini').val(),
            fecha_fin: $('input#fecha_fin').val(),
            nomenclatura: $('select#nomenclatura option:selected').val()
        });
        window.open(url, "_blank");
    }

    cargar_tabla() {
        var url = "https://mylsa.iplaneacion.com/dms/contabilidad_apis/contabilidad/transacciones/ventas_diarias/contenido?"+$.param({
            fecha_inicio: $('input#fecha_ini').val(),
            fecha_fin: $('input#fecha_fin').val(),
            nomenclatura: $('select#nomenclatura option:selected').val()
        });
        
        
        $('iframe#contenedor_documento').on("load", function(e){
            var element = document.getElementById("contenedor_documento");
            var content = (element.contentDocument || element.contentWindow);
            content.body.style.fontSize = "150%";
        });
        $('iframe#contenedor_documento').attr('src',url);

    }
}

const APP = new CTRL_APP();