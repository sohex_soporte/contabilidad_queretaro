class CTRL_APP {
    constructor () {
      this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload( null, false );
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            pageLength: 50,
            'ajax' : {
                'url' : PATH+'asientos/api/detalle',
                'data' : function(){
                    var fecha = moment($('input#fecha').val());
                    var anio = null;
                    var mes = null;
                    var dia = null;
                    if(fecha.isValid()){
                        anio = fecha.format('YYYY');
                        mes = fecha.format('MM');
                        dia = fecha.format('DD');
                    }
                    return {
                        'fecha': $('input#fecha').val(),
                        'cuenta': $('input#cuenta').val(),
                        // 'mes': mes, 
                        // 'dia': dia,
                        'nomenclatura': $('select#nomenclatura option:selected').val(),
                        'folio': $('input#folio').val(),
                        'limite': 1000,
                        'estatus': 'TODOS'
                    };
                },
                'type' : 'get'
            },
            'order': [
                [3,'desc']
            ],
            'columns': [
                { 
                    title: 'ID',
                    data: 'transaccion_id',
                    className: 'text-left',
                    render: function ( data, type, row ) {
                        return '<small><b>' + data + '</b></small>';
                    },    
                },
                { 
                    title: 'Asiento',
                    data: 'asiento_id',
                    className: 'text-left',
                    render: function ( data, type, row ) {
                        return APP.zfill(parseInt(data),7);
                    },    
                },
               
                { 
                    title: 'Origen',
                    data: 'transaccion_origen',
                    render: function ( data, type, row ) {
                        var html = '';//'<small><b>'+(row.procedencia_nombre != null? row.procedencia_nombre : '')+'</b></small><br/>';
                        html += '<span>'+(row.procedencia_nombre != null? row.procedencia_nombre : '')+'</span>';
                        // switch (data) {
                        //     case 'CAJA':
                        //         html = 'CAJAS';
                        //         break;
                        //     case 'CHEQUE':
                        //         html = 'CHEQUES';
                        //         break;
                        // }
                        return html;
                    },
                },
                { 
                    title: 'Fecha Transaccion',
                    data: 'transaccion_fecha',
                    // className: 'text-right',
                    render: function ( data, type, row ) {
                        var fecha = moment(data,'YYYY-MM-DD');
                        var html = '';
                        if(fecha.isValid()){
                            html = '<center>'+ fecha.format('DD/MM/YYYY')+'</b></small></center>';
                        }
                        return html;
                    },  
                },
                { 
                    title: 'Poliza',
                    data: 'poliza',
                    render: function ( data, type, row ) {
                        var html = ((data != null)? '<center>'+data+'</center>' : '');
                        // html += '<small><b>'+ row.fecha_creacion+'</b></small>';

                        var fecha = moment(row.fecha_creacion,'YYYY-MM-DD');
                        // var html = '';
                        if(fecha.isValid()){
                            html += '<center><small><b>'+ fecha.format('DD/MM/YYYY')+'</b></small></center>';
                        }

                        return html;
                    },  
                },
                { 
                    title: 'Concepto',
                    data: 'concepto',
                    render: function ( data, type, row ) {
                        return '<p>'+ data+'</p>';
                    },  
                },
                { 
                    title: 'Folio',
                    data: 'folio',
                    render: function ( data, type, row ) {
                        return $.trim(data).toUpperCase();
                    },   
                },
                { 
                    title: 'Departamento',
                    data: 'departamento_descripcion',
                    render: function ( data, type, row ) {
                        var html = '<small><b>'+((row.sucursal_nombre != null)? row.sucursal_nombre : '-')+'</b></small><br/>';
                        html += '<small>'+$.trim(data).toUpperCase()+'</small>';
                        return html;
                    } 
                },
                { 
                    title: 'Cuenta',
                    render: function ( data, type, row ) {
                        return (row.cuenta > 0)? row.cuenta +'<br/><small><b>'+ row.cuenta_descripcion+'</b></small>' : row.cuenta_descripcion;
                    }  
                },
                { 
                    title: 'Cargo',
                    data: 'cargo',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                    
                },
                { 
                    title: 'Abono',
                    data: 'abono',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                },
                // { 
                //     title: 'Monto',
                //     data: 'monto',
                //     className: 'text-right',
                //     render: function ( data, type, row ) {
                //         return (data >= 0)? '<span class="text-success float-left"><i class="fas fa-plus-square"></i></span>'+APP.formatMoney(data) :'<span class="text-danger float-left"><i class="fas fa-minus-square"></i></span>'+(APP.formatMoney(data*-1));
                //     }  
                // },
                
                { 
                    title: 'Estatus',
                    data: 'estatus_id',
                    render: function ( data, type, row ) {
                        var html = '';
                        switch (data) {
                            case 'APLICADO':
                                html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                            case 'ANULADO':
                                html = '<h6><span class="badge badge-secondary">Anulado</span></h6>';
                                break;
                            case 'POR_APLICAR':
                                html = '<h6><span class="badge badge-primary">Por aplicar</span></h6>';
                                break;
                            default:
                                html = '<h6><span class="badge badge-warning">Cancelado</span></h6>';
                                //html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                        }
                        return html;
                    }, 
                },
                { 
                    title: 'Fecha de registro',
                    data: 'fecha_registro',
                    render: function ( data, type, row ) {
                        var fecha = moment(data,'YYYY-MM-DD HH:mm:ss.SSSSSS');
                        var html = '';
                        if(fecha.isValid()){
                            html = '<center>'+fecha.format('HH:mm:ss') +'<br/><small><b>'+ fecha.format('DD/MM/YYYY')+'</b></small></center>';
                        }
                        return html;
                    },  
                },
                
              ]
        });
    }

    // Getter
    // get area() {
    //    return this.calcArea();
    //  }
    
    // calcArea () {
    //   return this.alto * this.ancho;
    // }
  }
  
  const APP = new CTRL_APP();