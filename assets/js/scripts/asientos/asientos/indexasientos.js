class CTRL_APP {
    constructor () {
      this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    formatMoney(money){
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload( null, false );
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            pageLength: 50,
            'ajax' : {
                'url' : PATH+'asientos/api/listado_asientos',
                'data' : function(){
                    return {
                        'folio': $('#folio').val()
                    };
                },
                'type' : 'get'
            },
            'order': [
                [3,'desc']
            ],
            'columns': [
                {
                    title: '#',
                    data: 'id',
                },
                { 
                    title: 'Cuenta',
                    data: 'cuenta',
                    className: 'text-left'
                },
                {
                    title: 'Folio',
                    data: 'folio',
                },
                { 
                    title: 'Fecha de registro',
                    data: 'fecha_creacion',
                    // render: function ( data, type, row ) {
                    //     var fecha = moment(data,'YYYY-MM-DD HH:mm:ss.SSSSSS');
                    //     var html = '';
                    //     if(fecha.isValid()){
                    //         html = '<center>'+fecha.format('HH:mm:ss') +'<br/><small><b>'+ fecha.format('DD/MM/YYYY')+'</b></small></center>';
                    //     }
                    //     return html;
                    // },  
                },
                { 
                    title: 'Número orden',
                    data: 'numero_orden',
                    render: function ( data, type, row ) {
                        return (data != null && data != "")? parseInt(data) :'<center>-</center>';
                    }     
                },
                {
                    title: 'Cantidad',
                    data: 'cantidad',
                    render: function ( data, type, row ) {
                        return (data != null && data != "")? parseInt(data) :'<center>-</center>';
                    }  
                },
                {
                    title: 'Concepto',
                    data: 'concepto'
                },
                { 
                    title: 'Costo promedio',
                    data: 'costo',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    } 
                },
                // { 
                //     title: 'Costo Unitario',
                //     data: 'costo_unitario',
                //     render: function ( data, type, row ) {
                //         return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                //     } 
                // },
                { 
                    title: 'Precio Unitario',
                    data: 'precio_unitario',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    } 
                },
                { 
                    title: 'Cargo',
                    data: 'cargo',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                    
                },
                { 
                    title: 'Abono',
                    data: 'abono',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                },
                { 
                    title: 'Subtotal',
                    data: 'subtotal',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                },
                { 
                    title: 'IVA',
                    data: 'iva',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                },
                { 
                    title: 'Total',
                    data: 'total',
                    className: 'text-right',
                    render: function ( data, type, row ) {
                        return (data > 0)? APP.formatMoney(data) :'<center>-</center>';
                    }  
                },
                { 
                    title: 'Estatus',
                    data: 'estatus_id',
                    render: function ( data, type, row ) {
                        var html = '';
                        switch (data) {
                            case 'APLICADO':
                                html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                            case 'ANULADO':
                                html = '<h6><span class="badge badge-secondary">Anulado</span></h6>';
                                break;
                            case 'POR_APLICAR':
                                html = '<h6><span class="badge badge-primary">Por aplicar</span></h6>';
                                break;
                            default:
                                html = '<h6><span class="badge badge-warning">Cancelado</span></h6>';
                                //html = '<h6><span class="badge badge-success">Aplicado</span></h6>';
                                break;
                        }
                        return html;
                    }, 
                },
                { 
                    title: 'Acciones',
                    render: function ( data, type, row ) {
                        let editar = `<button class="btn btn-secondary" data-id="${row.id}" onclick="APP.openModal(this)"><i class="fa fa-pen"></i></button>`;
                        return editar;
                    }  
                    
                },
                
              ]
        });
    }

    calcularTotales(){
        let total = 0;
        let subtotal = 0;
        let iva = 0;
        let cantidad = $("#cantidad").val();
        cantidad = cantidad != "" && cantidad > 0 ? cantidad : 0;
        let precio_uni = $("#precio_unitario").val();
        precio_uni = precio_uni != "" && precio_uni > 0 ? precio_uni : 0;
        total = cantidad * precio_uni;
        subtotal = total / 1.16;
        iva = total - subtotal;
        $("#total").val(parseFloat(total).toFixed(4));
        $("#iva").val(parseFloat(iva).toFixed(4));
        $("#subtotal").val(parseFloat(subtotal).toFixed(4));

    }

    openModal(_this) {
        var id = $(_this).data("id");
        $.ajax({
            dataType: "JSON",
            url: PATH+'asientos/api/open_modal_asientos',
            data: { id: id},
            type: "GET",
            success: function(data) {
                $("div#modalGeneral").empty();
                $("div#modalGeneral").html(data.contenido);
                $("div#modalGeneral").modal("show");
            }
        });
    }
    save(_this) {
        $("small").html('');
        var sendData = $('form[id="form_modal_asientos"]').serializeArray();
        //console.log(sendData);
        $.ajax({
            dataType: "json",
            type: "POST",
            url: PATH +"asientos/api/save_asientos" ,
            data: sendData,
            success: function(result) {
                if (result.estatus == "error") {
                    $.each(result.info, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                            console.log(value);
                        }
                    });
                } else {
                    if (result.data) {
                        $("#modalGeneral").modal("hide");
                        Swal.fire({
                            text: result.mensaje,
                            type: "success"
                        }).then(function() {
                            let table =$('table#tabla_listado').DataTable();
                            table.ajax.reload( null, false );
                        });
                    }
                }
            }
        });
    }
  }

  
  const APP = new CTRL_APP();