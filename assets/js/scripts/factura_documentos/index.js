class FACTURACION_DOCUMENTOS_APP {
    constructor() {
        this.cargar_tabla();
    }

    formatMoney(money) {
        let dollarUS = Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });
        return dollarUS.format(money)
    }

    buscar_tabla() {
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    limpiar_busqueda() {
        $('form#form_filter').trigger("reset");
        let table = $('table#tabla_listado').DataTable();
        table.ajax.reload(null, false);
    }

    open_modal(_this) {
        console.log(_this);
        let sat = $(_this).data('sat_uuid');
        let pdf = $(_this).data('pdf');
        let xml = $(_this).data('xml');
        let estatus = $(_this).data('estatus');
        let resp_api = $(_this).data('respuesta');

        if (estatus == 8) {
            $('#div-botones').show();
        } else {
            $('#div-botones').hide();
        }

        $('#respuesta').html(resp_api);
        $("#sat_uuid").html(sat);
        $("#factura").attr('href', pdf);
        $("#xml").attr('href', xml);


        $("#modal_doc").modal("show");
    }

    generarFactura(_this) {

        let data_send = { 'documento_id': $(_this).data('id') };
        $.ajax({
            type: 'post',
            url: PATH + '/factura_documentos/api/getDataFile',
            data: data_send,
            dataType: "json",
            beforeSend: function() {
                $("body").LoadingOverlay("show");
            },
            success: function(data, status, xhr) {
                if (data.estatus != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: data.mensaje,
                    });

                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Érror!',
                        text: data.mensaje,
                    });
                }

            },
            error: function() {
                $("body").LoadingOverlay("hide");
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
                let table = $('table#tabla_listado').DataTable();
                table.ajax.reload(null, false);
            }
        });
    }

    deleteDocument(_this) {

        let data_send = { 'documento_id': $(_this).data('id') };
        $.ajax({
            type: 'post',
            url: PATH + '/factura_documentos/api/deleteArchivo',
            data: data_send,
            dataType: "json",
            beforeSend: function() {
                $("body").LoadingOverlay("show");
            },
            success: function(data, status, xhr) {
                if (data.estatus != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: data.mensaje,
                    });
                    let table = $('table#tabla_listado').DataTable();
                    table.ajax.reload(null, false);
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Érror!',
                        text: data.mensaje,
                    });
                }

            },
            error: function() {
                $("body").LoadingOverlay("hide");
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }

    showDocument(_this) {
        let documento_name = $(_this).data('nombre_archivo');
        $.ajax({
            type: 'get',
            url: PATH + '/factura_documentos/api/descargarArchivo?nombre_archivo=' + documento_name + '&tipo_factura=1',
            dataType: "json",
            beforeSend: function() {
                $("body").LoadingOverlay("show");
            },
            success: function(data, status, xhr) {
                console.log(data);
                $("#documento").html('');
                $("#documento").html(data);
                $("#modal_ver_documento").modal("show");
            },
            error: function() {
                $("body").LoadingOverlay("hide");
            },
            complete: function() {
                $("body").LoadingOverlay("hide");
            }
        });
    }


    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: {
                "url": PATH_LANGUAGE
            },
            responsive: true,
            pageLength: 10,
            'ajax': {
                'url': PATH + '/factura_documentos/api/getDocumentosFactura',
                "dataSrc": "",
                'type': 'get'
            },
            'order': [
                [0, 'desc']
            ],
            'columns': [{
                    title: '#',
                    data: 'id',
                },
                {
                    title: 'Nombre archivo',
                    data: 'nombre_archivo'
                },
                {
                    title: 'Tipo archivo',
                    data: 'tipo_archivo'
                },
                {
                    title: 'Estatus',
                    data: 'estatus'
                },
                {
                    title: 'Fecha',
                    data: 'created_at'
                },
                {
                    title: 'Acciones',
                    width: '160px',
                    render: function(data, type, row) {
                        console.log(row);
                        let acciones = '';
                        let procesar = '';
                        let file = '';
                        let elimina = '';

                        let resp = row.respuesta_api != null && row.respuesta_api != '' ? row.respuesta_api.replace(/['"]+/g, '') : '';
                        if (row.estatus_documento_factura_id != 8) {
                            procesar = '<button title="Procesar documento" data-id= "' + row.id + '"  onclick="APP.generarFactura(this)" class="btn btn-secondary btn-sm"><i class="fas fa-cog"></i></button>';
                        } else {
                            procesar = '<button title="Documento procesado" data-id= "' + row.id + '" disabled="disabled" class="btn btn-secondary btn-sm"><i class="fas fa-cog"></i></button>';
                        }
                        if (row.estatus_documento_factura_id == 6 || row.estatus_documento_factura_id == 7 || row.estatus_documento_factura_id == 8) {
                            acciones += '<button title="Ver" data-id= "' + row.id + '" data-sat_uuid ="' + row.sat_uuid + '" data-pdf="' + row.pdf + '" data-xml="' + row.xml + '"  data-estatus="' + row.estatus_documento_factura_id + '" data-respuesta="' + resp + '"  onclick="APP.open_modal(this)" class="btn btn-primary btn-sm ml-1"><i class="fas fa-eye"></i></button>';
                        } else {
                            acciones += '<button title="Ver" disabled="disabled" data-id= "' + row.id + '" data-sat_uuid ="' + row.sat_uuid + '" data-pdf="' + row.pdf + '" data-xml="' + row.xml + '"  data-estatus="' + row.estatus_documento_factura_id + '" data-respuesta="' + resp + '"  onclick="APP.open_modal(this)" class="btn btn-primary btn-sm ml-1"><i class="fas fa-eye"></i></button>';
                        }

                        file = '<button title="Ver contenido archivo" data-nombre_archivo= "' + row.nombre_archivo + '"  onclick="APP.showDocument(this)" class="btn btn-success btn-sm ml-1"><i class="fas fa-file"></i></button>';
                        if (row.estatus_documento_factura_id != 8) {
                            elimina = '<button title="Eliminar archivo" data-id= "' + row.id + '"  onclick="APP.deleteDocument(this)" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>';
                        } else {
                            elimina = '<button  data-id= "' + row.id + '" disabled="disabled" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>';
                        }

                        return '<center>' + procesar + ' ' + acciones + ' ' + file + ' ' + elimina + '</center>';
                    }
                },
            ],
            createdRow: function(row, data, dataIndex) {
                console.log(data);
                if (data.estatus_documento_factura_id == 8) {
                    $(row).find('td').addClass('table-success text-end border-all');
                }
            }
        });
    }
}

const APP = new FACTURACION_DOCUMENTOS_APP();