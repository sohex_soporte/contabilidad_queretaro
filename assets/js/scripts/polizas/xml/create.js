class APP_CLASS {
    constructor() {
    }


    finalizar_parte_poliza() {
        $.ajax({
            url: PATH + '/polizas/xml_api/finalizar_parte_poliza',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id_poliza_xml: id_poliza_act
            },
            success: function(response) {

                $('div#contenedor_modal').html(atob(response.data.html));
                $('div#contenedor_modal div.modal').modal();

            },
            error: function(respuesta) {
                
            }
        });
    }

    finalizar_parte_poliza_guardar() {

        var data_send = $('form#contenedor_tabla').serializeArray();
        data_send.push({'name':'id_poliza_act', value: id_poliza_act})

        $.ajax({
            url: PATH + '/polizas/xml_api/finalizar_parte_poliza_guardar',
            type: 'POST',
            dataType: 'JSON',
            data: data_send,
            success: function(response) {
                window.location.reload();
            },
            error: function(respuesta) {
                
            }
        });
    }

    crear_cheque() {
        $.ajax({
            url: PATH + '/polizas/xml_api/crear_cheque',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id_poliza_xml: id_poliza_act
            },
            success: function(response) {

                $('div#contenedor_modal').html(atob(response.data.html));
                $('div#contenedor_modal div.modal').modal();

            },
            error: function(respuesta) {
                
            }
        });
    }

    habilitar_boton() {
        var band = false;
        $("div.modal.show input.largerCheckbox").each(function (index) {
            if ($(this).prop('checked') == true) {
                band = true;
            }
        });
        console.log(band);
        if (band == false) {
            $('div.modal.show #crear_cheque_bton').prop('disabled',true);
        } else {
            $('div.modal.show #crear_cheque_bton').prop('disabled',false);
        }
    }


    crear_cheque_guardar() {

        var data_send = $('form#contenedor_tabla').serializeArray();
        data_send.push({
            name: 'id_persona', value: id_persona
        })
        data_send.push({
            name: 'id_poliza_xml', value: id_poliza_act
        })
        

        $.ajax({
            url: PATH + '/polizas/xml_api/crear_sub_transacccion',
            type: 'POST',
            dataType: 'JSON',
            data: data_send,
            success: function(response) {
                app.reload_page();
            },
            error: function(respuesta) {
                
            }
        });
    }

    reload_page(){
        $.LoadingOverlay("show");
        setTimeout(() => {
            location.reload();
        }, 500);
    }

}

const app = new APP_CLASS();