class CTRL_APP {
    constructor () {
      this.cargar_tabla();
    }

    zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                 return ("-" + numberOutput.toString()); 
            } else {
                 return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    buscar_tabla(){
        let table =$('table#tabla_listado').DataTable();
        table.ajax.reload();
    }

    consultar_poliza($this){
        var id = $($this).data('piliza_id');
        window.location.href = PATH+"/polizas/consultar/"+id;
    }

    cargar_tabla() {
        $('table#tabla_listado').dataTable({
            language: PATH_LANGUAGE,
            pageLength: 150,
            searching: false,
            paging: false,
            info: false,
            "paging": false,
            'ajax' : {
                'url' : PATH+'polizas/api/polizas',
                'data' : function(){
                    var fecha = moment($('input[name=fecha]').val());

                    return {
                        fecha: fecha.format('YYYY-MM-DD'),
                        'anio': fecha.format('YYYY'),
                        'mes': fecha.format('MM'), 
                        'dia': fecha.format('DD')
                    };
                },
                'type' : 'get'
            },
            'order': [0,'desc'],
            'columns': [
                { 
                    title: 'Fecha del movimiento',
                    data: 'fecha_creacion',
                    // type: "date",
                    orderable: false,
                    render: function ( data, type, row ) {
                        if(type === 'display'){
                            var fecha = moment( row.updated_at );
                            var html = '';
                            if(fecha.isValid()){
                                html = '<center>'+fecha.format('HH:mm')+'<br/><small>'+fecha.format('DD/MM/YYYY')+'</small></center>';
                            }
                            return html;
                        }else{
                            return row.updated_at;
                        }
                    }
                },
                { 
                    title: 'Poliza',
                    data: 'PolizaNomenclatura_id',
                    orderable: false,
                    render: function ( data, type, row ) {
                        return '<span>'+((data != null)? data+'-' : '')+row.dia+'</span>';
                    },  
                },
                { 
                    title: 'Concepto',
                    data: 'nomenclatura',
                    orderable: false,
                    render: function ( data, type, row ) {
                        var descripcion = '';
                        try {
                            descripcion = (typeof row.poliza_fija != 'undefined' && row.PolizaNomenclatura_id == 'DO')? row.poliza_fija : data;
                        } catch (error) {
                            descripcion = data;
                        }
                        return ((descripcion != null)? descripcion : '');
                    },  
                },
                { 
                    title: 'Acciones',
                    orderable: false,
                    render: function ( data, type, row ) {
                        var consultar = '<button title="Consultar" data-piliza_id= "' + row.id + '"  onclick="APP.consultar_poliza(this)" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>';

                        return consultar ;
                    }
                }

                
             
                
              ]
        });
    }
  }
  
  const APP = new CTRL_APP();