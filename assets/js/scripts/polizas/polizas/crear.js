class CTRL_APP {
    constructor () {
    }

    guardar() {
        $("small.form-text.text-danger").html('');
        $.ajax({
            url: PATH + '/asientos/api/crear_poliza',
            type: 'POST',
            data: $('form#consultar').serializeArray(),
            success: function(response) {

                if (response.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'La poliza se ha guardado correctamente',
                    }).then((result) => {
                        window.location.href = PATH+"/polizas/consultar/"+response.data.poliza_id+'/'+response.data.transaccion_id;
                    });

                } else {
                    if (response.message) {
                        $.each(response.message, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Favor de revisar los campos obligatorios',
                        })
                    }
                }
            },
            error: function(respuesta) {
                console.log(respuesta);
            }
        });
    }
  }
  
  const app = new CTRL_APP();