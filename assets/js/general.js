var code400 = function () {
    ErrorCustom('No tiene permisos de acceder!', "");
}
var code404 = function () {
    ErrorCustom('La petición realizada no se encuentra, favor de intentarlo de nuevo mas tarde.', "");
}
var code500 = function () {
    ErrorCustom('El servidor no se encuentra disponible, intenta de nuevo', "" );
}
var code409 = function () {
    ErrorCustom('Sesión ha expirado por inactividad en el sistema', function () {
        window.location.href = '/Account/LogOff';
    });
}

//mensaje y tipo: success, danger, etc
var exito = function(mensaje,tipo){
  bootbox.dialog({
      message: mensaje,
      closeButton: false,
      buttons:
      {
          "danger":
          {
              "label": "<i class='icon-remove'></i> Cerrar",
              "className": "btn-sm btn-"+tipo+"",

          }
      }
  });
}

var exito_redirect = function(mensaje,tipo,redirect){
  bootbox.dialog({
      message: mensaje,
      closeButton: false,
      buttons:
      {
          "danger":
          {
              "label": "<i class='icon-remove'></i> Cerrar",
              "className": "btn-sm btn-"+tipo+"",
              "callback": function () {
                  window.location.href = redirect;
              }

          }
      }
  });
}

var ajaxJson = function (url, data, metodo, asincrono, callback) {
    if (metodo == "")
        metodo = "POST";
    $.ajax({
        type: metodo,
        url: url,
        enctype: 'multipart/form-data',
        datatype: "JSON",
        async: asincrono,
        cache: false,
        data: data,
        statusCode: {
            200: function (result) {
            	//console.log(result);
                if (callback != "") {
                    var call = $.Callbacks();
                    call.add(callback);
                    call.fire(result);
                }
            },
            401: code400,
            404: code404,
            500: code500,
            409: code409
        }
    });

}

var ErrorCustom = function (mensaje, callback) {
    if (mensaje == "" || mensaje == undefined)
        mensaje = '<span>Ocurrió un error al procesar la petición</span>';
    else
        mensaje = "<span>" + mensaje + "</span>";
    bootbox.dialog({
        message: mensaje,
        closeButton: false,
        buttons:
        {
            "danger":
            {
                "label": "<i class='icon-remove'></i> Cerrar",
                "className": "btn-sm btn-danger",
                "callback": function () {
                    if (callback != "") {
                        var call = $.Callbacks();
                        call.add(callback);
                        call.fire();
                    }
                }
            }
        }
    });
}
