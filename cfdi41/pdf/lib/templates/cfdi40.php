<?php
/**
 * Template para generar PDF de CFDI 3.3
 * Soporta complementos de Pagos 1.0 y Comercio Exterior 1.1
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.1.0 (10/05/2018)
 */

$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$tipoComprobante = $cfdi->xml->getAttribute('TipoDeComprobante');
$cce11 = $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('cce11:ComercioExterior');
?>
<style type="text/css">
<!--

.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#000;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
	padding-left: 1.6mm;
	padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
	padding-top: 1.3mm;
	padding-bottom: 1.3mm;
	border: solid 0.25mm #000;
}

.cell-padding2{
	padding-top: 1.3mm;
	padding-bottom: 1.3mm;
}
.border-left1{
	
}

.cell-padding-narrow{
	padding-top: 1mm;
	padding-bottom: 1mm;
	border: solid 0.25mm #000;
}

.cell-padding-narrow2{
	padding-top: 1mm;
	padding-bottom: 1mm;
	padding-left: 1.6mm;
	padding-right: 1.6mm;
	
}
.cell-padding-big{
	padding-top: 2.6mm;
	padding-bottom: 2.6mm;
	
}

.border-gray{
	border: solid 0.25mm #000<?php //echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm #000;<?php //echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm #000;<?php //echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm #000;<?php //echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{
	
}
table.sat-info h5{
	line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }

-->
</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>

	<table class="page-head">
		<tr>
			<td style="width:71%;text-align:center"><?php if(!empty($logo)) {
				echo '<img src="'.$logo.'" style="height:90px">';
			} ?></td>
			
			<td style="width:1%"></td>
			<td style="width:28%">
				<table class="text-center">
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Serie - Folio</th>
						<th style="width:45%" class="border-gray cell-padding-v">Tipo</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v"><?php echo $cfdi->getSerieFolio(); ?></td>
						<td style="width:45%" class="border-gray cell-padding-v"><?php echo $cfdi->getTipoComprobante(); ?><br/></td>
					</tr>
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Fecha</th>
						<th style="width:45%" class="border-gray cell-padding-v">Lugar Expedición</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v"><?php echo $cfdi->xml->getAttribute('Fecha'); ?><br/></td>
						<td style="width:45%" class="border-gray cell-padding-v"><?php echo $cfdi->xml->getAttribute('LugarExpedicion'); ?><br/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table class="spacing-top-2mm">
		<tr>
			<td style="width:49.5%;vertical-align:top">
				<table>
					<tr><th style="width:100%" class="text-center cell-padding-v">EMISOR</th></tr>
					<tr>
						<td style="width:100%" class="cell-padding border-gray">
							<table>
								<tr>
									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Emisor')->getAttribute('Nombre'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>RFC:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Emisor')->getAttribute('Rfc'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo $cfdi->getRegimenFiscal(); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>Dirección fiscal:</b> <span>Av. Constituyentes No. 42 Ote, Villas del Sol Querétaro, CP 76046</span></p></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:49.5%;vertical-align:top">
				<table>
					<tr><th style="width:100%" class="text-center cell-padding-v">RECEPTOR</th></tr>
					<tr>
						<td style="width:100%" class="cell-padding border-gray">
							<table>
								<tr>
									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('Nombre'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>RFC:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('Rfc'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>Uso del CFDI:</b> <span><?php echo $cfdi->getUsoCfdi(); ?></span></p></td>
								</tr>
								<?php
								if($numRegIdTrib = $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('NumRegIdTrib')) {
								?>
								<tr>
									<td style="width:100%"><p><b>Núm. Reg. ID Trib.:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('NumRegIdTrib'); ?></span></p></td>
								</tr>
								<?php
								}
								?>
								<tr>
									<td style="width:100%"><p><b>Dirección fiscal:</b> <span><?php echo $receptor_direccion; ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>Régimen fiscal:</b> <span><?php echo $regimen_fiscal; ?></span></p></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


<?php
if($cce11->children) {
?>
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">COMERCIO EXTERIOR</th></tr>
		<tr>
			<td class="100p cell-padding border-gray">
				<table>
					<tr>
						<td style="width:50%; vertical-align: top">
							<table>
								<tr>
									<td style="width:30%" class="text-bold">Tipo operación:</td>
									<td style="width:70%"><?php echo $cce11->getAttribute('TipoOperacion'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Incoterm:</td>
									<td><?php echo $cce11->getAttribute('Incoterm'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Clave de Pedimento</td>
									<td><?php echo $cce11->getAttribute('ClaveDePedimento'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Subdivisión:</td>
									<td><?php echo $cce11->getAttribute('Subdivision'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Observaciones:</td>
									<td><?php echo $cce11->getAttribute('Observaciones', '-'); ?></td>
								</tr>
							</table>
						</td>
						<td style="width:50%; vertical-align: top">
							<table>
								<tr>
									<td style="width: 40%" class="text-bold">Tipo de Cambio USD</td>
									<td style="width: 60%"><?php echo $cfdi->formatDecimal($cce11->getAttribute('TipoCambioUSD')); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Total USD</td>
									<td>$<?php echo $cfdi->formatDecimal($cce11->getAttribute('TotalUSD')); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Cert. Origen:</td>
									<td><?php echo $cce11->getAttribute('CertificadoOrigen', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Núm. Cert. Origen:</td>
									<td><?php echo $cce11->getAttribute('NumCertificadoOrigen', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Núm. Exportador Confiable:</td>
									<td><?php echo $cce11->getAttribute('NumeroExportadorConfiable', '-'); ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table class="productos" class="spacing-top-2mm">
		<thead>
			<tr><th style="width:100%" colspan="7" class="text-center cell-padding-v">MERCANCIAS</th></tr>
			<tr>
				<th style="width:12%" class="cell-padding">No. ident.</th>
				<th style="width:10%" class="cell-padding">Frac. Aran.</th>
				<th style="width:27%" class="cell-padding">Descripción</th>
				<th style="width:14%" class="cell-padding">Cant. Aduana</th>
				<th style="width:10%" class="cell-padding">U. Aduana</th>
				<th style="width:14%" class="cell-padding text-right">Valor U. Aduana</th>
				<th style="width:13%" class="cell-padding text-right">Valor dólares</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$numItems = count($cce11->getChildren('cce11:Mercancias')->children);
			foreach ($cce11->getChildren('cce11:Mercancias')->children as $i => $row) {
				$isLast = $numItems-1 == $i;
			?>
			<tr<?php echo $isLast ? ' class="last-row"' : ''; ?>>
				<td style="width:12%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> border-left"><?php echo $row->getAttribute('NoIdentificacion'); ?></td>
				<td style="width:10%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $row->getAttribute('FraccionArancelaria'); ?></td>
				<td style="width:27%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>">
					<?php
					if($row->children) {
						$arr = array();
						foreach ($row->children as $row2) {
							$arr2 = array();
							$r = $row2->getAttribute('Marca');
							if(!empty($r)) $arr2[] = 'Marca: '.$r;
							$r = $row2->getAttribute('Modelo');
							if(!empty($r)) $arr2[] = 'Modelo: '.$r;
							$r = $row2->getAttribute('SubModelo');
							if(!empty($r)) $arr2[] = 'Sub Modelo: '.$r;
							$r = $row2->getAttribute('NumeroSerie');
							if(!empty($r)) $arr2[] = 'Numero Serie: '.$r;
							$arr[] = implode(', ', $arr2);
						?>
						<?php
						}
						echo implode('<br/>', $arr);
					}else{
						echo '-';
					}
					?>
				</td>
				<td style="width:14%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $cfdi->formatDecimal($row->getAttribute('CantidadAduana')); ?></td>
				<td style="width:10%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $row->getAttribute('UnidadAduana'); ?></td>
				<td style="width:14%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> text-right">$<?php echo $cfdi->formatDecimal($row->getAttribute('ValorUnitarioAduana')); ?></td>
				<td style="width:13%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> text-right border-right">$<?php echo $cfdi->formatDecimal($row->getAttribute('ValorDolares')); ?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
<?php
}
?>




<?php
if($tipoComprobante == 'P') {
	$aux = 1;
	$compPagos10 = $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('pago20:Pagos');
	foreach ($compPagos10->children as $pagoItem) {
		//echo $aux;
		

		if($aux != 1):
?>
	
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">PAGO</th></tr>
		<tr>
			<td class="100p cell-padding border-gray">
				<table>
					<tr>
						<td style="width:30%; vertical-align: top">
							<table>
								<tr>
									<td style="width:38%" class="text-bold">Fecha de Pago</td>
									<td style="width:62%"><?php echo $pagoItem->getAttribute('FechaPago', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Forma de Pago</td>
									<td><?php echo $pagoItem->getAttribute('FormaDePagoP', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Moneda</td>
									<td><?php echo $pagoItem->getAttribute('MonedaP', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Tipo de Cambio</td>
									<td><?php echo $pagoItem->getAttribute('TipoCambioP', '-'); ?></td>
								</tr>
							</table>
						</td>
						<td style="width:30%; vertical-align: top">
							<table>
								<tr>
									<td style="width: 38%" class="text-bold">Monto</td>
									<td style="width: 62%">$<?php echo $cfdi->formatDecimal($pagoItem->getAttribute('Monto', '-')); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Núm. Operacion</td>
									<td><?php echo $pagoItem->getAttribute('NumOperacion', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Cuenta Ben.</td>
									<td><?php echo $pagoItem->getAttribute('CtaBeneficiario', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Cuenta Ord.</td>
									<td><?php echo $pagoItem->getAttribute('CtaOrdenante', '-'); ?></td>
								</tr>
							</table>
						</td>
						<td style="width:40%; vertical-align: top">
							<table>
								<tr>
									<td style="width: 30%" class="text-bold">RFC Banco Ben.</td>
									<td style="width: 70%"><?php echo $pagoItem->getAttribute('RfcEmisorCtaBen', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">RFC Banco Ord.</td>
									<td><?php echo $pagoItem->getAttribute('RfcEmisorCtaOrd', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Banco Ord. Ext.</td>
									<td><?php echo $pagoItem->getAttribute('NomBancoOrdExt', '-'); ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table class="productos" class="spacing-top-2mm">
		<thead>
			<tr><th style="width:100%" colspan="9" class="text-center cell-padding-v">DOCUMENTOS RELACIONADOS</th></tr>
			<tr>
				<th style="width:19%" class="cell-padding">UUID</th>
				<th class="cell-padding">Folio</th>
				<th class="cell-padding">Moneda</th>
				<th class="cell-padding">T.C.</th>
				<th class="cell-padding">Método</th>
				<th class="cell-padding">Parcialidad</th>
				<th class="cell-padding text-right">Saldo Ant.</th>
				<th class="cell-padding text-right">Pagado</th>
				<th class="cell-padding text-right">Saldo</th>
			</tr>
		</thead>
		<tbody>
<?php
		if($pagoItem->children) {
			$numItems = count($pagoItem->children);
			foreach ($pagoItem->children as $i => $doctoRel) {
				$isLast = $numItems-1 == $i;
?>
			<tr<?php echo $isLast ? ' class="last-row"' : ''; ?>>
				<td style="width:19%" class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> border-left"><?php echo strtoupper($doctoRel->getAttribute('IdDocumento', '-')); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $doctoRel->getAttribute('Serie').$doctoRel->getAttribute('Folio') ?: '-'; ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $doctoRel->getAttribute('MonedaDR', '-'); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $doctoRel->getAttribute('TipoCambioDR', '-'); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $doctoRel->getAttribute('MetodoDePagoDR', '-'); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?>"><?php echo $doctoRel->getAttribute('NumParcialidad', '-'); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> text-right">$<?php echo $cfdi->formatDecimal($doctoRel->getAttribute('ImpSaldoAnt')); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> text-right">$<?php echo $cfdi->formatDecimal($doctoRel->getAttribute('ImpPagado')); ?></td>
				<td class="cell-padding-narrow<?php echo $isLast ? ' border-bottom' : ''; ?> border-right text-right">$<?php echo $cfdi->formatDecimal($doctoRel->getAttribute('ImpSaldoInsoluto')); ?></td>
			</tr>
<?php
			}
		}
?>
		</tbody>
	</table>
<?php
endif;
$aux ++;
	}

}
?>


<?php if($tipo_factura == 3):?>
<div class="cell-padding">

	<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 20%">Pedido</th>
				<th class="cell-padding2" style="width:30%">Cliente</th>
				<th class="cell-padding2" style="width: 48%">Clave vendedor</th>
				
		
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $pedido;?></td>
				<td style="width: 30%" class="cell-padding-narrow2 border-left1"><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('Nombre'); ?></td>
				<td style="width: 48%" class="cell-padding-narrow2 border-left1"><?php echo $vendedor_nombre;?></td>
				
		
			</tr>
			
			
		</tbody>
	</table>


	<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 20%">No. inventario</th>
				<th class="cell-padding2" style="width:30%"></th>
				<th class="cell-padding2" style="width: 20%">No. puertas</th>
				<th class="cell-padding2" style="width:10%">N. cilindros</th>
				<th class="cell-padding2 " style="width:18%">Capacidad</th>
		
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $no_inventario;?></td>
				<td style="width: 30%" class="cell-padding-narrow2 border-left1"></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $no_puertas;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $no_cilindros;?></td>
				<td style="width: 18%" class="cell-padding-narrow2 border-left1"><?php echo $capacidad;?>Pas</td>
		
			</tr>
			
			
		</tbody>
	</table>


	<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 20%">Combustible</th>
				<th class="cell-padding2" style="width:30%">Motor</th>
				<th class="cell-padding2" style="width: 20%">Marca</th>
				<th class="cell-padding2" style="width:10%">Linea</th>
				<th class="cell-padding2 " style="width:18%">Modelo</th>
		
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $combustible;?></td>
				<td style="width: 30%" class="cell-padding-narrow2 border-left1"><?php echo $motor;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $marca;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $linea;?></td>
				<td style="width: 18%" class="cell-padding-narrow2 border-left1"><?php echo $modelo;?></td>
		
			</tr>
			
			
		</tbody>
	</table>





<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 20%">Clave vehicular</th>
				<th class="cell-padding2" style="width:30%">VIN</th>
				<th class="cell-padding2" style="width: 20%">Pedimento</th>
				<th class="cell-padding2" style="width:10%">Fecha</th>
				<th class="cell-padding2 " style="width:18%">Aduana</th>
		
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $clave_vehicular;?></td>
				<td style="width: 30%" class="cell-padding-narrow2 border-left1"><?php echo $niv;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $numero;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $fecha;?></td>
				<td style="width: 18%" class="cell-padding-narrow2 border-left1"><?php echo $aduana;?></td>
		
			</tr>
			
			
		</tbody>
	</table>

</div>
<?php endif;?>



<?php if($tipo_factura == 1):?>
<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 10%">Tipo</th>
				<th class="cell-padding2" style="width:40%">Serie</th>
				<th class="cell-padding2" style="width: 10%">Modelo</th>
				<th class="cell-padding2" style="width:10%">Placas</th>
				<th class="cell-padding2 " style="width:10%">Color</th>
				<th class="cell-padding2 " style="width:10%">KM</th>
				<th class="cell-padding2 " style="width:10%">No.Economico</th>
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $tipo;?></td>
				<td style="width: 40%" class="cell-padding-narrow2 border-left1"><?php echo $serie;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $modelo;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $placas;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $color;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $km;?></td>
				<td style="width: 10%" class="cell-padding-narrow2 border-left1"><?php echo $no_economico;?></td>
			</tr>
			
			
		</tbody>
	</table>

	<table class="productos" class="spacing-top-2mm">
		<thead>
			
			<tr>
				<th class="cell-padding2" style="width: 20%">Orden</th>
				<th class="cell-padding2" style="width: 20px;%">Versión</th>
				<th class="cell-padding2" style="width: 20%">Fecha recepción</th>
				<th class="cell-padding2" style="width:20%">Asesor</th>
				<th class="cell-padding2 " style="width:20%">Transmisión</th>
				
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $orden;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $version;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $fecha_recepcion;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $asesor;?></td>
				<td style="width: 20%" class="cell-padding-narrow2 border-left1"><?php echo $transmision;?></td>
				
			</tr>
			
			
		</tbody>
	</table>

<?php endif;?>


	<table class="productos" class="spacing-top-2mm">
		<thead>
			<tr><th style="width:100%" colspan="4" class="text-center cell-padding-v">CONCEPTOS</th></tr>
			<tr>
				
				<th class="cell-padding" style="width:10%">Cantidad</th>
				
				<th class="cell-padding" style="width:68%">Descripción</th>
				<th class="cell-padding text-right" style="width:10%">Precio</th>
				<th class="cell-padding text-right" style="width:12%">Importe</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($cfdi->xml->getChildren('cfdi:Conceptos')->children as $i => $row) {
			?>
			<tr>
				
				<td style="width:10%" class="cell-padding-narrow"><?php echo $cfdi->formatDecimal($row->getAttribute('Cantidad')); ?></td>
				
				<td style="width:68%" class="cell-padding-narrow"><?php echo $row->getAttribute('Descripcion', '-'); ?></td>
				<td style="width:10%" class="cell-padding-narrow text-right">$<?php echo number_format($row->getAttribute('ValorUnitario'), 2, '.', ''); ?></td>
				<td style="width:12%" class="cell-padding-narrow text-right border-right">$<?php echo number_format($row->getAttribute('Importe'), 2, '.', ''); ?></td>
			</tr>
			<tr class="last-row">
				<td style="width:100%" colspan="4" class="border-left border-bottom cell-padding-narrow border-right text-muted">
					<b>ClaveProdServ:</b> <?php echo $row->getAttribute('ClaveProdServ', '-'); ?>.
					<b>ClaveUnidad:</b> <?php echo $row->getAttribute('ClaveUnidad', '-'); ?>.
					<b>Unidad:</b> <?php echo $row->getAttribute('Unidad', '-'); ?>.
					<b>Impuestos:</b> <?php echo $cfdi->getImpuestos($row) ?: '-'; ?>.
					
					<?php
					$temp = $row->getChildren('cfdi:CuentaPredial')->getAttribute('Numero', '');
					if($temp != '') echo '<b>C. Predial:</b> '.$temp;
					?>
					<?php
					$temp = $row->getChildren('cfdi:InformacionAduanera')->getAttribute('NumeroPedimento', '');
					if($temp != '') echo '<b>No. Pedimento:</b> '.$temp;
					?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>


<?php
if($cfdi->xml->getChildren('cfdi:CfdiRelacionados')->children) {
?>
	<table class="spacing-top-2mm">
		<tr><th colspan="2" class="100p text-center cell-padding-v">CFDI RELACIONADO</th></tr>
		<tr>
			<th style="width:18%" class="cell-padding-narrow">Tipo de Relación</th>
			<td style="width:82%" class="cell-padding-narrow border-gray">
				<?php echo $cfdi->getTipoRelacion(); ?>
			</td>
		</tr>
		<tr>
			<th style="width:18%" class="cell-padding-narrow">UUID</th>
			<td style="width:82%" class="cell-padding-narrow border-gray">
				<?php
				foreach ($cfdi->xml->getChildren('cfdi:CfdiRelacionados')->children as $i => $row) {
					if($i > 0) echo '<br/>';
					echo $row->getAttribute('UUID');
				}
				?>
			</td>
		</tr>
	</table>
<?php
}
?>


<?php
if($tipoComprobante != 'P') {
?>
	<table class="spacing-top-2mm">
		<tr>
			<td style="width:74%">
				<table>
					<tr><th colspan="2" class="100p text-center cell-padding-v">DATOS GENERALES</th></tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->getFormaPago();
						$condicionesPago = $cfdi->xml->getAttribute('CondicionesDePago');
						if(!empty($condicionesPago)) echo ' | <b>Condiciones Pago</b>: '.$condicionesPago;
						?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->getMetodoPago(); ?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">T.C. / Moneda</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->xml->getAttribute('TipoCambio', '1.000000'); ?> / <?php echo $cfdi->xml->getAttribute('Moneda'); ?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Total con Letra</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->totalConLetra(); ?></td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:25%">
				<table>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Subtotal</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo number_format($cfdi->xml->getAttribute('SubTotal'), 2, '.', ''); ?></td>
					</tr>
					
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Traslados</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getChildren('cfdi:Impuestos')->getAttribute('TotalImpuestosTrasladados')); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Retenciones</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getChildren('cfdi:Impuestos')->getAttribute('TotalImpuestosRetenidos')); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">TOTAL</th>
						<td style="width:55%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($cfdi->xml->getAttribute('Total'), 2, '.', ''); ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php
}
?>


<?php if($cfdi->timbrado()) { ?>
	<table class="spacing-top-2mm">
		<tr>
			<td class="100p cell-padding border-gray">
				<table class="sat-info">
					<tr>
						<td style="width:16%;vertical-align:top">
							<qrcode value='<?php echo $cfdi->getQr(); ?>' ec="M" style="width: 28.5mm; background-color: white; color: black; border:none"></qrcode>
							<h5 class="text-center spacing-top-2mm"">Versión de CFDI</h5>
							<p class="text-center font-system"><?php echo $cfdi->xml->getAttribute('Version') ?></p>
						</td>
						<td style="width:84%">
							<table class="100p">
								<tr>
									<td style="width:21%">
										<h5>Fecha de Timbrado</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('FechaTimbrado'); ?></p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado SAT</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('NoCertificadoSAT'); ?></p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado Emisor</h5>
										<p class="font-system"><?php echo $cfdi->xml->getAttribute('NoCertificado'); ?></p>
									</td>
									<td style="width:35%">
										<h5>Folio Fiscal</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID'); ?></p>
									</td>
								</tr>
							</table>
							<h5 class="spacing-top-1mm">Cadena Original</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->getCadenaOriginalTFD(), $charsPerLineBase, '<br>'); ?></p>
							<h5 class="spacing-top-1mm">Sello Digital del CFDI</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('SelloCFD'), $charsPerLineBase, '<br>'); ?></p>
							<h5 class="spacing-top-1mm">Sello Digital del SAT</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('SelloSAT'), $charsPerLineBase, '<br>'); ?></p>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="100p">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php } ?>

	<?php
	if(!empty($mensajeFactura)){ ?>
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">NOTA</th></tr>
		<tr>
			<td style="width:100%" class="text-center cell-padding border-gray">
				<?php echo $mensajeFactura ?>
			</td>
		</tr>
	</table>
<?php
}
?>


	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una representación impresa de un CFDI.</p>
	</div>

	<?php
	if($mensajeSello){
		$color = '#ff0101';
		$w = 4.5;
	?>
		<div style="position: absolute;
			left:50%;
			top:30%;
			margin-left:-<?php echo (strlen($mensajeSello)*$w)/2 ?>mm;
			margin-top:-5mm;
			width:<?php echo strlen($mensajeSello)*$w ?>mm;
			height:8mm;
			border:solid 1mm <?php echo $color ?>;
			padding:1.5mm 0 0 0;
			text-align:center;
			font-weight:bold;
			font-size:16pt;
			color:<?php echo $color ?>;
		">
			<?php echo $mensajeSello ?>
		</div>
	<?php
	}
	?>
</page>