<?php
/**
 * Template para generar PDF de Factura (CFDI 3.2)
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.0.0
 */

$charsPerLineBase = 113;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$xml = $xmlParser->getObject();

$timbrado = $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID') != null;

?>
<style type="text/css">
<!--

.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
th,
.cell-padding{
	padding:1.3mm 1.6mm;
}
.cell-padding-narrow{
	padding:1mm 1.6mm;
}
.cell-padding-big{
	padding:2.6mm 1.6mm;
}

.border-gray{
	border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{
	
}
table.sat-info h5{
	line-height: 120%;
}
-->
</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>
	<div>
		<table class="page-head">
			<tr>
<?php
if(!empty($logo)){ ?>
				<td style="width:28%">
					<div class="logo"><?php echo '<img src="'.$logo.'" style="height:120px;width:200px">'; ?></div>
				</td>
				<td style="width:0.5%"></td>
				<td style="width:43.5%"><?php
}else{ ?>
				<td style="width:72%"><?php
} ?>
					<h1 class="font-medium"><?php echo $xml->getChildren('cfdi:Emisor')->getAttribute('nombre'); ?></h1>
					<p class="font-medium spacing-top-0mm"><?php echo $xmlParser->getDomicilio($xml->getChildren('cfdi:Emisor')->getChildren('cfdi:DomicilioFiscal')->attributes); ?></p>
					<p class="font-medium spacing-top-0mm"><b>RFC:</b> <span><?php echo $xml->getChildren('cfdi:Emisor')->getAttribute('rfc'); ?></span></p>
					<p class="font-medium spacing-top-0mm"><b>Régimen Fiscal:</b> <span><?php echo $xml->getChildren('cfdi:Emisor')->getChildren('cfdi:RegimenFiscal')->getAttribute('Regimen'); ?></span></p>
				</td>
				<td style="width:1%"></td>
				<td style="width:27%">
					<table class="text-center">
						<tr>
							<th class="60p cell-padding" style="font-size:8pt;vertical-align:middle"><?php echo $tipoComprobante ?></th>
							<td class="40p text-right text-bold cell-padding border-gray" style="font-size:8pt;vertical-align:middle"><?php echo $xmlParser->getSerieFolio($xml); ?></td>
						</tr>
					</table>
					<table class="text-center spacing-top-3mm">
						<tr><th class="100p">Lugar de Expedición</th></tr>
						<tr><td class="100p cell-padding border-gray">
							<?php echo $xml->getAttribute('LugarExpedicion'); ?><br/>
						</td></tr>
						<tr><th class="100p">Fecha y Hora de Emisión</th></tr>
						<tr><td class="100p cell-padding border-gray">
							<?php echo $xml->getAttribute('fecha'); ?>
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div class="spacing-top-2mm">
		<table>
			<tr><th class="100p">RECEPTOR</th></tr>
			<tr>
				<td class="100p cell-padding border-gray">
					<table>
						<tr>
							<td style="width:83%">
								<p><b>Razón Social:</b> <span><?php echo $xml->getChildren('cfdi:Receptor')->getAttribute('nombre'); ?></span></p>
							</td>
							<td style="width:17%">
								<p class="spacing-top-1mm"><b>RFC:</b> <span><?php echo $xml->getChildren('cfdi:Receptor')->getAttribute('rfc'); ?></span></p>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="100p">
								<p><b>Domicilio:</b> <span><?php echo $xmlParser->getDomicilio($xml->getChildren('cfdi:Receptor')->getChildren('cfdi:Domicilio')->attributes); ?></span></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div class="spacing-top-2mm">
		<table class="productos">
			<tr>
				<th style="width:9%" class="text-right first-row">Cantidad</th>
				<th style="width:9%" class="text-left">Unidad</th>
				<th style="width:59%">Descripción</th>
				<th style="width:11%" class="text-right">P. Unitario</th>
				<th style="width:12%" class="text-right">Importe</th>
			</tr>
			<?php
			$descuento = (float)$xml->getAttribute('descuento', 0);
			$conceptos = $xml->getChildren('cfdi:Conceptos')->children;
			$inc1 = empty($descuento) ? 0 : 1;
			$count = count($conceptos);
			foreach ($conceptos as $i => $row) {
				$final = $count-1+$inc1 == $i;
				$subtotal = (float)$row->getAttribute('valorUnitario') * (float)$row->getAttribute('cantidad');
			?>
			<tr<?php if($i==$count-1) echo ' class="last-row"'; ?>>
				<td style="width:9%" class="first-row border-left<?php if($final) echo ' border-bottom'; ?> text-right cell-padding-narrow"><?php echo $row->getAttribute('cantidad'); ?></td>
				<td style="width:9%" class="<?php if($final) echo ' border-bottom'; ?> cell-padding-narrow"><?php echo $row->getAttribute('unidad'); ?></td>
				<td style="width:59%" class="<?php if($final) echo ' border-bottom'; ?> cell-padding-narrow"><?php
				
				echo $row->getAttribute('descripcion');

				$numCuentaPredial = $row->getChildren('cfdi:CuentaPredial')->getAttribute('numero');
				if(!empty($numCuentaPredial)){
					echo ' (Cta. Predial: '.$numCuentaPredial.')';
				}
				?></td>
				<td style="width:11%" class="<?php if($final) echo ' border-bottom'; ?> text-right cell-padding-narrow"><?php echo $row->getAttribute('valorUnitario'); ?></td>
				<td style="width:12%" class="border-right<?php if($final) echo ' border-bottom'; ?> text-right cell-padding-narrow"><?php echo $subtotal; ?></td>
			</tr>
			<?php
			}
			if($inc1){ ?>
			<tr class="last-row">
				<td class="first-row border-left border-bottom text-right cell-padding-narrow"></td>
				<td class="border-bottom text-center cell-padding-narrow"></td>
				<td class="border-bottom cell-padding-narrow">DESCUENTO</td>
				<td class="border-bottom text-right cell-padding-narrow"></td>
				<td class="border-right border-bottom text-right cell-padding-narrow">- <?php echo $descuento; ?></td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td style="width:74%">
					<table>
						<tr>
							<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
							<td style="width:82%" class="cell-padding-narrow border-gray"><?php
							echo $xml->getAttribute('formaDePago');
							$condicionesPago = $xml->getAttribute('condicionesDePago', null);
							if(!empty($condicionesPago)) echo ' (Condiciones: '.$condicionesPago.')';
							?></td>
						</tr>
						<tr>
							<th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
							<td style="width:82%" class="cell-padding-narrow border-gray">
							<?php
							$metodoPago = $xml->getAttribute('metodoDePago', null);
							echo empty($metodoPago) ? 'No especificado' : $metodoPago;

							$cuentaPago = $xml->getAttribute('NumCtaPago', null);
							if(!empty($cuentaPago)) echo ' (Cuenta: '.$cuentaPago.')';
							?>
							</td>
						</tr>
						<tr>
							<th style="width:18%" class="cell-padding-narrow">Total con Letra</th>
							<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo CantidadConLetra::convertir($xml->getAttribute('total'), 'PESO', 'MXN'); ?></td>
						</tr>
					</table>
				</td>
				<td style="width:1%">
				</td>
				<td style="width:25%">
					<table>
						<tr>
							<th style="width:50%" class="cell-padding-narrow">Subtotal</th>
							<td style="width:50%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo ((float)$xml->getAttribute('subTotal') - (float)$xml->getAttribute('descuento')); ?></td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">IVA (+)</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $xmlParser->getIvaImporte($xml); ?></td>
						</tr>
<?php
$ivaRet = $xmlParser->getIvaRetenidoImporte($xml);
if($ivaRet){ ?>
						<tr>
							<th class="cell-padding-narrow">IVA Retenido (-)</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $ivaRet; ?></td>
						</tr>
<?php
} ?>
<?php
$isrRet = $xmlParser->getIsrRetenidoImporte($xml);
if($isrRet){ ?>
						<tr>
							<th class="cell-padding-narrow">ISR Retenido (-)</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $isrRet; ?></td>
						</tr>
<?php
} ?>
						<tr>
							<th class="cell-padding-narrow">Total</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $xml->getAttribute('total'); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<?php if($timbrado) { ?>
	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td class="100p cell-padding border-gray">
					<table class="sat-info">
						<tr>
							<td style="width:14%">
								<qrcode value='<?php echo $xmlParser->getQr($xml); ?>' ec="M" style="width: 25mm; background-color: white; color: black; border:none"></qrcode>
							</td>
							<td style="width:86%">
								<table class="100p">
									<tr>
										<td style="width:24%">
											<h5>Fecha y Hora de Certificación </h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('FechaTimbrado'); ?></p>
										</td>
										<td style="width:21%">
											<h5>No. Serie Certificado SAT</h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('noCertificadoSAT'); ?></p>
										</td>
										<td style="width:23%">
											<h5>No. Serie Certificado Emisor</h5>
											<p class="font-system"><?php echo $xml->getAttribute('noCertificado'); ?></p>
										</td>
										<td style="width:32%">
											<h5>Folio Fiscal</h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID'); ?></p>
										</td>
									</tr>
								</table>
								<h5 class="spacing-top-1mm">Sello Digital del CFDI</h5>
								<p class="font-system"><?php echo chunk_split($xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('selloCFD'), $charsPerLineBase, '<br>'); ?></p>
								<h5 class="spacing-top-1mm">Sello Digital del SAT</h5>
								<p class="font-system"><?php echo chunk_split($xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('selloSAT'), $charsPerLineBase, '<br>'); ?></p>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="100p">
								<h5 class="spacing-top-1mm">Cadena Original del Complemento de Certificación Digital del SAT</h5>
								<p class="font-system"><?php echo chunk_split($xmlParser->getCadenaOriginalTFD($xml), $charsPerLineBase+19, '<br>'); ?></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>

	<?php if(!empty($mensajeFactura)){ ?>
	<div class="spacing-top-2mm bg-gray cell-padding-big text-center">
		<?php echo $mensajeFactura ?>
	</div>
	<?php } ?>

	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una representación impresa de un CFDI.</p>
	</div>

	<?php
	if($mensajeSello){
		$color = '#ff0101';
		$w = 4.5;
	?>
		<div style="position: absolute;
			left:50%;
			top:30%;
			margin-left:-<?php echo (strlen($mensajeSello)*$w)/2 ?>mm;
			margin-top:-5mm;
			width:<?php echo strlen($mensajeSello)*$w ?>mm;
			height:8mm;
			border:solid 1mm <?php echo $color ?>;
			padding:1.5mm 0 0 0;
			text-align:center;
			font-weight:bold;
			font-size:16pt;
			color:<?php echo $color ?>;
		">
			<?php echo $mensajeSello ?>
		</div>
	<?php
	}
	?>
</page>