<?php declare(strict_types=1);

require_once '/var/www/web/dms/contabilidad_queretaro/cfdi/pdf_xml/vendor/autoload.php';

$cfdifile = '/var/www/web/dms/contabilidad_queretaro/cfdi/pdf_xml/prueba.xml';
$xml = file_get_contents($cfdifile);

// clean cfdi
$xml = \CfdiUtils\Cleaner\Cleaner::staticClean($xml);

// create the main node structure
$comprobante = \CfdiUtils\Nodes\XmlNodeUtils::nodeFromXmlString($xml);

// create the CfdiData object, it contains all the required information
$cfdiData = (new \PhpCfdi\CfdiToPdf\CfdiDataBuilder())
    ->build($comprobante);

// create the converter
$converter = new \PhpCfdi\CfdiToPdf\Converter(
    new \PhpCfdi\CfdiToPdf\Builders\Html2PdfBuilder()
);

// create the invoice as output.pdf
$converter->createPdfAs($cfdiData, 'output1.pdf');