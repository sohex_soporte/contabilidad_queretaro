<?php
// error_reporting(1);
//ini_set('display_errors', 1);

//define('LIB_BASE', dirname(dirname(__FILE__)).'/lib/');

//require LIB_BASE.'Cfdi2Pdf.php';
//$pdfTemplateDir = LIB_BASE.'templates/';
require '/var/www/web/dms/contabilidad_queretaro/cfdi/pdf/lib/Cfdi2Pdf.php';
$pdfTemplateDir = '/var/www/web/dms/contabilidad_queretaro/cfdi/pdf/lib/templates/';

$pdf = new Cfdi2Pdf($pdfTemplateDir);

// datos del archivo. No se mostrarán en el PDF (requeridos)
$pdf->autor  = 'Nombre Sitio Web';
$pdf->titulo = 'Factura PDF';
$pdf->asunto = 'Comprobante CFDI';

// texto a mostrar en la parte superior (requerido)
$pdf->encabezado = 'Demo Empresa SA de CV';

// nombre del archivo PDF (opcional)
$pdf->nombreArchivo = 'pdf-cfdi.pdf';

// mensaje a mostrar en el pie de pagina (opcional)
$pdf->piePagina = 'Pie de página';

// texto libre a mostrar al final del documento (opcional)
$pdf->mensajeFactura = 'Mensaje opcional';

// Solo compatible con CFDI 3.3 (opcional)
$pdf->direccionExpedicion = "Calle #123\nCol. ABC";

// ruta del logotipo (opcional)
//$pdf->logo = dirname(__FILE__).'/logo.png';
$pdf->logo = '/var/www/web/dms/contabilidad_queretaro/cfdi/pdf/ejemplos/logo.png';

// mensaje a mostrar encima del documento (opcional)
// $pdf->mensajeSello = 'CANCELADO';

// Cargar el XML desde un string...
// $ok = $pdf->cargarCadenaXml($cadenaXml);

// Cargar el XML desde un archivo...
// $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
// $archivoXml = 'ejemplo_cfdi33_cce11.xml';
// $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
//$archivoXml = 'ejemplos/pago1.xml';
$archivoXml = "/var/www/web/dms/contabilidad_queretaro/statics/facturas/facturas_xml/E716E15F337DB8384464DA2C247A8D9F.xml";//$this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';
            
$ok = $pdf->cargarArchivoXml($archivoXml);

if($ok) {
    // Generar PDF para mostrar en el explorador o descargar
    $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

    // Guardar PDF en la ruta especificada
    // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
    // $ok = $pdf->guardarPdf($ruta);

    // Obtener PDF como string
    // $pdfStr = $pdf->obtenerPdf();

    if($ok) {
        // PDF generado correctamente.
    } else {
        echo 'Error al generar PDF.';
    }
}else{
    echo 'Error al cargar archivo XML.';
}
