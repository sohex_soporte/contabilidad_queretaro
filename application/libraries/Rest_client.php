<?php defined('BASEPATH') or exit('No direct script access allowed');

class Rest_client
{

    private $http_code;
    private $message;
    private $headers;
    private $response;

    /**
     * Toma una matriz de parámetros y devuelve el resultado de la función exec_api_get, que toma la
     * misma matriz de parámetros
     *
     * @param params matriz de parámetros para pasar a la API
     *
     * @return El valor de retorno es el resultado de la función exec_api_get().
     */
    public function get($params = array())
    {
        return $this->exec_api_get($params);
    }

    /* This is a function that is used to get data from the API. */
    public function exec_api_get($params = array())
    {
        $timeout = array_key_exists('timeout', $params) ? $params['timeout'] : 30;
        $url = array_key_exists('url', $params) ? trim($params['url'], '/') : (array_key_exists('server', $params) ? trim($params['server'], '/') : '');
        $method = array_key_exists('method', $params) ? trim($params['method'], '/') : '';
        $response_format = (array_key_exists('response', $params) && $params['response'] == 'object') ? 'object' : 'array';

        if (strlen(trim($method)) > 0) {
            $url = "{$url}/{$method}";
        }
        if (array_key_exists('params', $params)) {
            $url .= '?' . http_build_query($params['params']);
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0');
        // curl_setopt($curl, CURLOPT_ENCODING , "gzip");
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));

        $response = curl_exec($curl);

        $info = curl_getinfo($curl);
        $this->headers = (isset($info["header_size"])) ? substr($response, 0, $info["header_size"]) : "";
        $this->response = (isset($info["header_size"])) ? substr($response, $info["header_size"]) : "";
        $this->http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // $this->headers = $this->set_headers($response, $curl);

        // utils::pre($this->response);
        if ($response === false) {
            log_message('error', 'URL GET: ' . $url);
            log_message('error', curl_error($curl));
            $result = false;
        } else {
            $is_valid = $this->json_validate($this->response);
            if ($is_valid === true) {
                if ($response_format == 'object') {
                    $result = (array) @json_decode($this->response);
                } else {
                    $result = @json_decode($this->response, true);
                }
            } else {
                log_message('error', 'URL GET: ' . $url);
                log_message('error', $is_valid);
                log_message('error', (string) $this->response);
                $result = false;
            }
        }
        curl_close($curl);
        return $result;
    }

    /**
     * It takes an array of parameters, builds a URL, and then makes a POST request to that URL
     *
     * @param params array of parameters to be sent to the API
     */
    public function exec_api_post($params = array())
    {
        $timeout = array_key_exists('timeout', $params) ? $params['timeout'] : 30;
        $url = array_key_exists('url', $params) ? trim($params['url'], '/') : (array_key_exists('server', $params) ? trim($params['server'], '/') : '');
        $method = array_key_exists('method', $params) ? trim($params['method'], '/') : '';
        $response_format = (array_key_exists('response', $params) && $params['response'] == 'object') ? 'object' : 'array';

        if ($method != '') {
            $url = "{$url}/{$method}";
        }

        $parameters = '';
        if (array_key_exists('params', $params)) {
            $parameters = http_build_query($params['params']);
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $parameters,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $response = curl_exec($curl);

        $this->http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // $this->set_headers($response, $curl);

        if ($response === false) {
            log_message('error', 'URL POST: ' . $url);
            log_message('error', curl_error($curl));
            $result = false;
        } else {
            $is_valid = $this->json_validate($response);
            if ($is_valid === true) {
                if ($response_format == 'object') {
                    $result = (array) @json_decode($response);
                } else {
                    $result = @json_decode($response, true);
                }
            } else {
                log_message('error', 'URL POST: ' . $url);
                log_message('error', $is_valid);
                log_message('error', (string) $response);
                $result = false;
            }
        }
        curl_close($curl);
        return $result;
    }

    /**
     * It returns the HTTP code of the last request
     *
     * @return The http code.
     */
    public function get_http_code()
    {
        return $this->http_code;
    }

    /**
     * It returns the message.
     *
     * @return The message property of the object.
     */
    public function get_message()
    {
        $header = $this->headersToArray($this->headers);
        $message = array($header) && array_key_exists('message', $header) ? $header['message'] : false;
        return ($this->json_validate($message) === true) ? @json_decode($message, true) : $message;
    }

    /**
     * It returns true if the JSON is valid, and an error message if it's not
     *
     * @param string The string to be decoded.
     *
     * @return a boolean value.
     */
    private function json_validate($string)
    {
        // decode the JSON data
        $result = @json_decode($string);
        $error = false;

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = false; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        // everything is OK
        return ($error === false) ? true : $error;
    }

    /**
     * Devuelve los encabezados de la respuesta.
     *
     * @param result El resultado de la función curl_exec().
     *
     * @return Los encabezados de la respuesta.
     */
    private function set_headers($result)
    {
        return $this->headersToArray($result);
    }

    /**
     * Toma una cadena de encabezados y devuelve una matriz de encabezados
     *
     * @param str La cadena que se va a analizar.
     *
     * @return Los encabezados de la respuesta.
     */
    private function headersToArray($str)
    {
        $headers = array();
        $headersTmpArray = explode("\r\n", $str);
        for ($i = 0; $i < count($headersTmpArray); ++$i) {
            // we dont care about the two \r\n lines at the end of the headers
            if (strlen($headersTmpArray[$i]) > 0) {
                // the headers start with HTTP status codes, which do not contain a colon so we can filter them out too
                if (strpos($headersTmpArray[$i], ":")) {
                    $headerName = substr($headersTmpArray[$i], 0, strpos($headersTmpArray[$i], ":"));
                    $headerValue = substr($headersTmpArray[$i], strpos($headersTmpArray[$i], ":") + 1);
                    $headers[$headerName] = utf8_encode($headerValue);
                }
            }
        }
        return $headers;
    }

}
