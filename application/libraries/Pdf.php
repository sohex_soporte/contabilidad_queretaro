<?php defined('BASEPATH') OR exit('No direct script access allowed');
  
    use Dompdf\Dompdf;
    
    class Pdf
    {
        public $CI;
        function __construct()
        {
            $this->CI = & get_instance();
        }

        public function create($html,$filename)
        {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->stream($filename.'.pdf');
      }
}