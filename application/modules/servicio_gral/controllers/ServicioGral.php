<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ServicioGral extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $datos['index'] = '';
        $this->blade->render('index', $datos);
    }
}
