<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Balanza_comprobacion extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
       
        $this->load->model('MaBalanza_model');
        $anios_balanza = $this->MaBalanza_model->getList_anios();
        if(is_array($anios_balanza)){
            foreach ($anios_balanza as $key => $value) {

                $mes_balanza = $this->MaBalanza_model->getList_mes($value['anio']);
                if(is_array($mes_balanza)){
                    foreach ($mes_balanza as $key_mes => $value_mes) {
                        $mes_balanza[$key_mes]['letra'] = utils::mes_espanol($value_mes['mes']); 
                    }
                    
                }
                $anios_balanza[$key]['mes'] = $mes_balanza;

            }
        }

        $datos = array(
            'balanzas' => $anios_balanza
        );

        $this->blade->render('/balanza_comprobacion/index',$datos);
    }

    public function visor_simple(){

        $mes = base64_decode($this->input->get('mes'));
        $anio = base64_decode($this->input->get('anio'));

        $this->load->model('MaBalanza_model');
        $balanza = $this->MaBalanza_model->getList_balanza_simple($mes,$anio);

        $datos = array(
            'mes' => $mes,
            'anio' => $anio,
            'balanza' => $balanza
        );
        $this->blade->render('/balanza_comprobacion/visor_simple',$datos);
    }

    public function visor(){

        $mes = base64_decode($this->input->get('mes'));
        $anio = base64_decode($this->input->get('anio'));

        $this->load->model('MaBalanza_model');
        $balanza = $this->MaBalanza_model->getList_balanza($mes,$anio);

        $datos = array(
            'mes' => $mes,
            'anio' => $anio,
            'balanza' => $balanza
        );
        $this->blade->render('/balanza_comprobacion/visor',$datos);
    }
}