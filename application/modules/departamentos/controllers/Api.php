<?php defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    public $type = 'api';

    public $http_status = 200;
    public $data;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->data = array(
            'data' => false,
            'message' => false
        );
    }

    public function listado_get()
    {
        $this->load->model('CaDepartamentos_model');
        $this->data =  $this->CaDepartamentos_model->getAll();
        $this->response( $this->data , 201 );
    }
}