@layout('template_blade/estructura')

@section('contenido')
<script type="text/javascript">
   function cambiar_estatus(campo){
    var url ="<?php echo base_url()?>index.php/autos_nuevos/cambiar_estatus/<?php  echo $id;?>/"+campo;
        ajaxJson(url,{},
                  "POST","",function(result){
        });
   }

   function cambiar_estatus_aprobado(campo){
    var url ="<?php echo base_url()?>index.php/autos_nuevos/cambiar_estatus_aprobado/<?php  echo $id;?>/"+campo;
        ajaxJson(url,{},
                  "POST","",function(result){
                    location.reload();
        });
   }

      
</script>


     

<style>
.bordeado{
    border-style: ridge;
}
.fondo_rojo{
    /*background-color: #fab1a0;*/
}
.checkbox_tamano{
            width: 40px;
            height: 40px;
        }
</style>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    DOCUMENTACIÓN FACTURACIÓN
    
</h4>
<div class="row">
    <div class="col-6">
        <!--inicio de la PRIMERA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>C.: FACTURACION TRADICIONAL</h3>
            </div>

            <div class="col-md-12 bordeado " >
                <div class="row">
                    <div class="col-md-10">
                        <input type="hidden" id="id_venta_unidad" value="">
                        <?php renderTexto('file_pedido', 'PEDIDO'); ?>
                        
                    </div>
                    <div class="col-md-2 mt-3">
                            <input class="checkbox_tamano" onclick="cambiar_estatus('file_pedido')" type="checkbox" value="" id="file_pedido" name="file_pedido" <?php echo  $valores->file_pedido == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('file_remision', 'REMISION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('file_remision')" type="checkbox" value="" id="file_remision" name="file_remision" <?php echo  $valores->file_remision == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife', 'IFE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('ife')" type="checkbox" value="" id="ife" name="ife" <?php echo  $valores->ife == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
           
            <div class="col-md-12 bordeado " >
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp', 'CURP'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('curp')" type="checkbox" value="" id="curp" name="curp" <?php echo  $valores->curp == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('comprobante_domicilio')" type="checkbox" value="" id="comprobante_domicilio" name="comprobante_domicilio" <?php echo  $valores->comprobante_domicilio == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_id_clt', 'FORMATO ID DEL CLT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('formato_id_clt')" type="checkbox" value="" id="formato_id_clt" name="formato_id_clt" <?php echo  $valores->formato_id_clt == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <input class="checkbox_tamano" onclick="cambiar_estatus('caja_anticipo')" type="checkbox" value="" id="caja_anticipo" name="caja_anticipo" <?php echo  $valores->caja_anticipo == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_facturacion', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('formato_abono_ford_facturacion')" type="checkbox" value="" id="formato_abono_ford_facturacion" name="formato_abono_ford_facturacion" <?php echo  $valores->formato_abono_ford_facturacion == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('situacion_fiscal')" type="checkbox" value="" id="situacion_fiscal" name="situacion_fiscal" <?php echo  $valores->situacion_fiscal == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ficha_seguimiento', 'HOJA SICOP "FICHA DE SEGUIMIENTO"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('ficha_seguimiento')" type="checkbox" value="" id="ficha_seguimiento" name="ficha_seguimiento" <?php echo  $valores->ficha_seguimiento == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('contacto_cliente', 'HOJA SICOP "CORREO DE CONTACTO DEL CLIENTE"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('contacto_cliente')" type="checkbox" value="" id="contacto_cliente" name="contacto_cliente" <?php echo  $valores->contacto_cliente == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('vobo_area_procesos', 'VoBo AREA DE PROCESOS (MAIL)'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('vobo_area_procesos')" type="checkbox" value="" id="vobo_area_procesos" name="vobo_area_procesos" <?php echo  $valores->vobo_area_procesos == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('acta_constitutiva', 'ACTA CONSTITUTIVA PERSONA MORAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('acta_constitutiva')" type="checkbox" value="" id="acta_constitutiva" name="acta_constitutiva" <?php echo  $valores->acta_constitutiva == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('poder_representante_legal', 'PODER REPESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">

                    <input class="checkbox_tamano" onclick="cambiar_estatus('poder_representante_legal')" type="checkbox" value="" id="poder_representante_legal" name="poder_representante_legal" <?php echo  $valores->poder_representante_legal == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife_representante_legal', 'IFE REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('ife_representante_legal')" type="checkbox" value="" id="ife_representante_legal" name="ife_representante_legal" <?php echo  $valores->ife_representante_legal == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp_representante_legal', 'CURP REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('curp_representante_legal')" type="checkbox" value="" id="curp_representante_legal" name="curp_representante_legal" <?php echo  $valores->curp_representante_legal == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('consulta_lista_negra', 'CONSULTA DE LISTAS NEGRAS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('consulta_lista_negra')" type="checkbox" value="" id="consulta_lista_negra" name="consulta_lista_negra" <?php echo  $valores->consulta_lista_negra == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado ">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_renuncia', 'CARTA RENUNCIA EXT. GARANTIA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('carta_renuncia')" type="checkbox" value="" id="carta_renuncia" name="carta_renuncia" <?php echo  $valores->carta_renuncia == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
        </div>

        <!-- fin de la PRIMERA columna ------------------------->
    </div>
    <div class="col-6">
        <!-- inicio de la SEGUNDA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>F.- FLOTILLAS</h3>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('orden_compra', 'ORDEN DE COMPRA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('orden_compra')" type="checkbox" value="" id="orden_compra" name="orden_compra" <?php echo  $valores->orden_compra == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('cotizacion_ford', 'COTIZACION FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('cotizacion_ford')" type="checkbox" value="" id="cotizacion_ford" name="cotizacion_ford" <?php echo  $valores->cotizacion_ford == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('boletin_street_program', 'BOLETIN STREET PROGRAM'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('boletin_street_program')" type="checkbox" value="" id="boletin_street_program" name="boletin_street_program" <?php echo  $valores->boletin_street_program == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>D.: FORD CREDIT, BANCOS Y AUTOPCION</h3>
            </div>

            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprobacion_ford_credit', 'APROBACION FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('aprobacion_ford_credit')" type="checkbox" value="" id="aprobacion_ford_credit" name="aprobacion_ford_credit" <?php echo  $valores->aprobacion_ford_credit == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO o ENG'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('recibo_caja_anticipo')" type="checkbox" value="" id="recibo_caja_anticipo" name="recibo_caja_anticipo" <?php echo  $valores->recibo_caja_anticipo == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caratula_firma_otros_bancos', 'CARATULA DE FIRMA OTROS BANCOS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('caratula_firma_otros_bancos')" type="checkbox" value="" id="caratula_firma_otros_bancos" name="caratula_firma_otros_bancos" <?php echo  $valores->caratula_firma_otros_bancos == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_credit', 'FORMATO DE BONO FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('formato_abono_ford_credit')" type="checkbox" value="" id="formato_abono_ford_credit" name="formato_abono_ford_credit" <?php echo  $valores->formato_abono_ford_credit == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_ford_credit', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('formato_abono_ford_ford_credit')" type="checkbox" value="" id="formato_abono_ford_ford_credit" name="formato_abono_ford_ford_credit" <?php echo  $valores->formato_abono_ford_ford_credit == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>E.- CONAUTO</h3>
            </div>

            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_credito_autorizada', 'CARTA CREDITO AUTORIZADA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('carta_credito_autorizada')" type="checkbox" value="" id="carta_credito_autorizada" name="carta_credito_autorizada" <?php echo  $valores->carta_credito_autorizada == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprov_cond_auto_opcion', 'APROBACION DE COND. AUTO OPCION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('aprov_cond_auto_opcion')" type="checkbox" value="" id="aprov_cond_auto_opcion" name="aprov_cond_auto_opcion" <?php echo  $valores->aprov_cond_auto_opcion == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_pago_cliente', 'RECIBO DE CAJA DE PAGO CLIENTE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('recibo_caja_pago_cliente')" type="checkbox" value="" id="recibo_caja_pago_cliente" name="recibo_caja_pago_cliente" <?php echo  $valores->recibo_caja_pago_cliente == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                    <input class="checkbox_tamano" onclick="cambiar_estatus('formato_abono_ford')" type="checkbox" value="" id="formato_abono_ford" name="formato_abono_ford" <?php echo  $valores->formato_abono_ford == 1 ? "checked" : "";?>>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin de la SEGUNDA columna ------------------------->
    </div>
</div>
<hr />
<?php if($valores->aprobado == 0):?>
<div class="row">
    <div class="col-12 text-end">
        
            <button type="button" onclick="cambiar_estatus_aprobado('aprobado')" class="btn btn-info"> Aprobar documentación</button>
        
    </div>
</div>
<?php endif;?>
<?php if($valores->aprobado == 1):?>
<div class="row">
    <div class="col-12 text-end">
        <a href="<?php echo base_url(); ?>index.php/autos_nuevos/facturar_auto_primer_paso/<?php echo $id;?>/<?php echo $remision_id;?>">
            <button type="button" class="btn btn-info"> Facturar auto</button>
        </a>
    </div>
</div>
<?php endif;?>


@endsection