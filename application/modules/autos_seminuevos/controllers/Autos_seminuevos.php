<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Autos_seminuevos extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session','curl'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    /*
    * se muestra el listado de las solicitudes de autos nuevos
    */
    public function solicitudes_facturas()
    {
        $datos['solicitudes'] = "";
        $this->blade->render('solicitudes', $datos);
    }

    /*
    * se muestra la documentación de un solicitud
    */
    public function ver_documentacion()
    { 
        $id = $_GET['id'];
        $remision_id = $_GET['remision_id'];
        $valor = $this->Mgeneral->get_row('solicitud',$id,'factura_solicitud');
        /*$dataFromApi = $this->curl->curlGet('https://mylsa.iplaneacion.com/dms/autos_nuevos_queretaro/ventas/facturas/documentosByVentaId?id='.$id,true);
        $obj = json_decode($dataFromApi);
        $aux_array = array();
        foreach($obj->data as $row_json):
            $aux_array[$row_json->nombre_documento] = $row_json->url_documento;
        endforeach;
        $datos['aux_array'] = $aux_array;
        */
        $datos['valores'] = $valor;
        $datos['id'] = $id;
        $datos['remision_id'] = $remision_id;
        $this->blade->render('documentacion_new', $datos);
    }

    public function ver_documentacion1()
    { 
        $id = $_GET['id'];
        $remision_id = $_GET['remision_id'];
        $tipo_auto = $_GET['tipo_auto'];
        $valor = $this->Mgeneral->get_row('solicitud',$id,'factura_solicitud');
        $datos['rows'] = $this->Mgeneral->get_table('solicitudes_factura');
        $datos['valores'] = $valor;
        $datos['id'] = $id;
        $datos['remision_id'] = $remision_id;
        //1.-nuevo, 2.-seminuevo
        $datos['tipo_auto'] = $tipo_auto;
        $this->blade->render('documentacion1', $datos);
    }

    /*
    * solicitar fecturar auto
    */
    public function facturar_auto_primer_paso($id_solicitud,$remision_id)
    {
        //$datos['solicitud'] = "";
        //$this->blade->render('ver_documentacion',$datos);
        redirect('facturacion_autos/paso1/' . $id_solicitud.'/'.$remision_id);
    }

    public function cambiar_estatus($id_remision,$campo){
        $valor = $this->Mgeneral->get_row('solicitud',$id_remision,'factura_solicitud');
        $data = array();
        if($valor->$campo == 1){
            $data[$campo] = 0;
            $this->Mgeneral->update_table_row('factura_solicitud',$data,'solicitud',$id_remision);

        }else{
            $data[$campo] = 1;
            $this->Mgeneral->update_table_row('factura_solicitud',$data,'solicitud',$id_remision);
        }
        
        echo json_encode($data);
        exit();

    }

    public function cambiar_estatus_aprobado($id_remision,$campo){
        $valor = $this->Mgeneral->get_row('solicitud',$id_remision,'factura_solicitud');
        $data = array();
        if($valor->$campo == 1){
            $data[$campo] = 0;
            $this->Mgeneral->update_table_row('factura_solicitud',$data,'solicitud',$id_remision);

        }else{
            $data[$campo] = 1;
            $this->Mgeneral->update_table_row('factura_solicitud',$data,'solicitud',$id_remision);
        }
        
        echo json_encode($data);
        exit();

    }

    public function documentacion_contabilidad($solicitud){
        $response = array();

        if(isset($solicitud)){

            $valor = $this->Mgeneral->get_row('solicitud',$solicitud,'factura_solicitud');
            if(is_object($valor)){
                $response['data'] = $valor;
                $response['error'] = 0;
                if($valor->aprobado == 1){
                    $response['aprobado'] = 1;
                }else{
                    $response['aprobado'] = 0;
                }
                
            }else{
                $data['solicitud'] = $solicitud;
                $this->Mgeneral->save_register('factura_solicitud', $data);
                $valor = $this->Mgeneral->get_row('solicitud',$solicitud,'factura_solicitud');
                $response['data'] = $valor;
                $response['error'] = 0;
                if($valor->aprobado == 1){
                    $response['aprobado'] = 1;
                }else{
                    $response['aprobado'] = 0;
                }
            }


        }else{
            $response['data'] = "";
            $response['error'] = 0;

        }

        echo json_encode($response);

        exit();

    }

    public function info(){
        
        phpinfo();
      }
}
