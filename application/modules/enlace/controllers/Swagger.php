<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @OA\Info(
 *  title="Sistema Garantias Matriz",
 *  version="1.0"
 * )
 * @OA\Server(url="https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/")
 * @OA\Server(url="http://localhost/contabilidad_queretaro/")
 */
class Swagger extends CI_Controller
{
    protected $format    = 'json';

    public function __construct()
    {
        parent::__construct();
    }

    public function json()
    {
        $openapi = \OpenApi\Generator::scan([
            __FILE__,
            FCPATH.'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'asientos'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR,
            FCPATH.'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'enlaces'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR,
        ]);
        header('Content-Type: application/json');
        echo $openapi->toJson();
        exit;
    }
}
