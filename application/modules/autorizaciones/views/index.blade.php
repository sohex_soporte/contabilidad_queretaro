@layout('template_blade/estructura')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding: 0px !important;
        margin-left: 0px !important;
    }
</style>
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    AUTORIZACIÓN DE CREDITO CLIENTES
</h4>

<div class="card mt-3">
    <div class="card-body">
        <div class="row ">
            <div class="col-2">
                <div class="form-group">
                    <label><b>Número cliente</b></label>
                    <input type="text" id="numero_cliente" class="form-control buscar_enter">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label><b>Nombre cliente</b></label>
                    <input type="text" id="nombre" class="form-control buscar_enter">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label><b>Apellidos cliente</b></label>
                    <input type="text" id="apellidos" class="form-control buscar_enter">
                </div>
            </div>
            <div class="pull-right mt-4">
                <button type="button" class="btn btn-primary mt-2" onclick="app.filtrarTabla()"><i class="fa fa-search"></i> Buscar</button>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <table class="table table-striped" id="clientes_dms" width="100%" cellspacing="0">
                </table>
            </div>
        </div>

    </div>
</div>

<div class="modal" id="modal_autorizacion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Autorizar plazo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label><b>* Plazo credito</b></label>
                    <select name="plazo_credito_id" class="form-control" id="plazo_credito_id">
                        <option value="">Seleccion una opción</option>
                    </select>
                </div>
                <div class="form-group">
                    <label><b>* Límite de credito</b></label>
                    <input type="text" name="limite_credito" class="form-control money_format" />
                </div>
                <div class="form-group">
                    <label><b>Comentarios</b></label>
                    <textarea type="text" name="comentarios" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="app.autorizaPlazo()" class="btn btn-primary">Aplicar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modal_cobranza" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Historico cobranza</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <table class="table table-striped" id="historico_cobranza" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ base_url('assets/components/jquery/dist/jquery.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/bootstrap-4.6/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js" referrerpolicy="no-referrer"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    var app;
    var table = 'table#clientes_dms';
    var table_historico_cobranza = 'table#historico_cobranza';
    var cliente_id = '';

    class Funciones {
        create_table() {
            $(table).DataTable(this.settings_table());
            $(table_historico_cobranza).DataTable(this.settings_table_historico_cobranza());
        }

        filtrarTabla = () => {
            $(table).DataTable().ajax.reload()
        }

        settings_table() {
            return {
                ajax: {
                    url: API_URL_DMS + 'clientes/busqueda',
                    type: 'GET',
                    data: function(data) {
                        data.numero_cliente = document.getElementById('numero_cliente').value
                        data.nombre = document.getElementById('nombre').value
                        data.Apellidos = document.getElementById('apellidos').value
                    },
                },
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                processing: true,
                serverSide: true,
                searching: false,
                bFilter: false,
                lengthChange: false,
                bInfo: true,
                order: [
                    [0, 'asc']
                ],
                columns: [{
                        title: 'Número cliente',
                        data: function(data) {
                            return data.numero_cliente
                        }
                    },
                    {
                        title: 'Razón social',
                        data: 'nombre_empresa'

                    },
                    {
                        title: 'Nombre cliente',
                        data: function(data) {
                            return data.nombre + ' ' +  data.apellido_paterno + ' ' +  data.apellido_materno
                        }
                    },
                    {
                        title: 'RFC',
                        data: 'rfc'
                    },
                    {
                        title: 'Teléfono',
                        data: 'telefono',
                    },
                    {
                        title: 'Correo electrónico',
                        data: 'correo_electronico',
                    },
                    {
                        title: 'Tiene crédito',
                        data: 'aplica_credito',
                        render: function(data, type, row) {
                            return data && data == true ? 'Si' : 'No';
                        }
                    },
                    {
                        title: 'Plazo crédito',
                        data: 'plazo_credito',
                        render: function(data, type, row) {
                            return data && data.id ? data.nombre : 'Sin plazo';
                        }
                    },
                    {
                        title: 'Límite crédito',
                        data: 'limite_credito',
                        render: function(data, type, row) {
                            return data ? '<span class="money_format">' + data + '</span>' : '';
                        }
                    },
                    {
                        title: 'Acciones',
                        data: 'aplica_credito',
                        width: '150px',
                        render: function(data, type, row) {
                            let aplica_credito = '';
                            let autoriza_plazo = '';
                            let historico_cartera = '';
                            if (data && data == true) {
                                aplica_credito = '<button title="Eliminar credito" data-cliente_id= "' + row.id + '" data-autoriza="2" onclick="app.Autoriza_credito(this)" class="btn btn-danger"><i class="fas fa-times"></i></button>';
                            }
                            autoriza_plazo = '<button title="Edita Plazo Crédito" data-cliente_id= "' + row.id + '" data-tipo_plazo_credito="' + row.plazo_credito_id + '" onclick="app.OpenModalPlazo(this)" class="btn btn-primary"><i class="fas fa-edit"></i></button>';
                            historico_cartera = '<button title="Cartera cliente" data-cliente_id= "' + row.id + '"  onclick="app.openModalCartera(this)" class="btn btn-dark"><i class="fas fa-book"></i></button>';

                            return aplica_credito + ' ' + autoriza_plazo + ' ' + historico_cartera;
                        }

                    },
                ]
            }
        }
        settings_table_historico_cobranza() {
            return {
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                searching: false,
                bFilter: false,
                lengthChange: false,
                bInfo: false,
                paging: false,
                order: [
                    [0, 'desc']
                ],
                columns: [{
                        title: '-',
                        data: 'id',
                        "visible": false,
                    },
                    {
                        title: 'Monto',
                        data: 'monto_actual'
                    },
                    {
                        title: 'Fecha operación',
                        data: 'fecha_operacion'

                    },
                    {
                        title: 'Comentarios',
                        data: 'comentarios'
                    },
                ]
            }
        }

        Autoriza_credito(_this) {
            let data = {
                aplica_credito : $(_this).data('autoriza') == 1 ? 1 : 0,
                plazo_credito_id: null,
                limite_credito: 0
            };
            let cliente_id = $(_this).data('cliente_id');
            ajax.put(`clientes/actualizaCredito/` + cliente_id, data, function(response, header) {
                if (header.status == 201 || header.status == 200) {
                    let form = {
                        cliente_id: cliente_id,
                        plazo_credito_id: null,
                        monto_actual: 0,
                        fecha_operacion: '<?php echo date('Y-m-d'); ?>',
                        comentarios: 'Credito cancelado'
                    }
                    ajax.post(`cartera-cliente`, form, function(result, cabecera) {
                        if (cabecera.status == 201 || cabecera.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: cabecera.message,
                            }).then(function() {
                                let mensaje = $(_this).data('autoriza') == 1 ? 'Credito autorizado correctamente' : 'Credito cancelado correctamente';
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Éxito',
                                    text: mensaje,
                                });
                                app.filtrarTabla();
                            });
                        }
                    }, false, false);

                }
            }, false, false);
        }

        autorizaPlazo(_this) {
            let limite_credito = $("input[name*='limite_credito']").val();
            let data = {
                plazo_credito_id: $("select[name*='plazo_credito_id']").val(),
                limite_credito: limite_credito.replace(",", "")
            }
            ajax.destruir();
            ajax.put(`clientes/actualizaPlazoCredito/` + cliente_id, data, function(response, header) {
                if (header.status == 201 || header.status == 200) {
                    let form = {
                        cliente_id: cliente_id,
                        plazo_credito_id: $("select[name*='plazo_credito_id']").val(),
                        monto_actual: limite_credito.replace(",", ""),
                        fecha_operacion: '<?php echo date('Y-m-d'); ?>',
                        comentarios: $("[name*='comentarios']").val()
                    }
                    ajax.post(`cartera-cliente`, form, function(result, cabecera) {
                        if (cabecera.status == 201 || cabecera.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: cabecera.message,
                            }).then(function() {
                                $("#modal_autorizacion").modal("hide");
                                app.filtrarTabla();
                            });

                        }
                    });
                } else {
                    return ajax.showValidations(header);
                }
            }, false, false)
        }

        OpenModalPlazo(_this) {
            cliente_id = $(_this).data('cliente_id');
            $("#modal_autorizacion").modal("show");
            $("select[name*='plazo_credito_id']").attr('data', $(_this).data("tipo_plazo_credito"));
            $("input[name*='limite_credito']").val('');
            ajax.getCatalogo('catalogo-plazo-credito', $("select[name*='plazo_credito_id']"), true);
        }

        get_busqueda(cliente_id) {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    dataType: "json",
                    type: 'GET',
                    dataSrc: "",
                    data: {
                        'cliente_id': cliente_id
                    },
                    url: API_URL_DMS + 'cartera-cliente/buscar-por-cliente',
                    success: function(response) {
                        let listado = $(table_historico_cobranza).DataTable();
                        listado.clear().draw();
                        if (response && response.length > 0) {
                            response.forEach(listado.row.add);
                            listado.draw();
                        }
                        resolve(response);

                    }
                });
            })
        }

        openModalCartera(_this) {
            cliente_id = $(_this).data('cliente_id');
            app.get_busqueda(cliente_id).then(x => {
                $("#modal_cobranza").modal("show");
            })
        }

    }

    $(function() {
        app = new Funciones();
        app.create_table();
        $('.money_format').mask("#,##0.00", {
            reverse: true
        });
        $(".buscar_enter").keypress(function(e) {
        if (e.which == 13) {
            app.filtrarTabla();
        }
    });
    });
</script>
@endsection