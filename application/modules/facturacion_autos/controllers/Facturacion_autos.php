<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facturacion_autos extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session', 'curl'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    public function prueba_error()
    {


        $dataFromApi = $this->curl->curlGet('https://mylsa.iplaneacion.com/dms/autos_nuevos_queretaro/seminuevos/api/get?id_remision=1', true);


        $decoded_json = json_decode($dataFromApi, false);
        var_dump($decoded_json);
        var_dump($decoded_json->unidad_descripcion);
    }

    /**
     * en este pas van a guardar los datos de la factura
     */
    public function paso1($id_solicitud = 0, $remision_id, $tipo_auto)
    {


        if ($tipo_auto == 1) {
            $dataFromApi = $this->curl->curlGet('https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/unidades/' . $remision_id, true);
            $obj = json_decode($dataFromApi);
            $obj = $obj[0];


            $data_pedido = $this->curl->curlGet(API_URL_AUTOS . 'ventas/ventas_api/api/getByVentaId?venta_id=' . $id_solicitud, true);
            $obj_pedido = json_decode($data_pedido);
            $precio_venta = $obj_pedido->data->automovil->u_precio_venta;
            $vendedor_nombre = $obj_pedido->data->vendedor->nombre;
            $vendedor_clave = $obj_pedido->data->vendedor->clave;
            $pedido = $obj_pedido->data->num_pedido;
            $nombre_cliente = $obj_pedido->data->cliente->nombre_empresa;
            $cliente_rfc = $obj_pedido->data->cliente->rfc;
            $codigo_postal = $obj_pedido->data->cliente->codigo_postal;
            $correo_electronico = $obj_pedido->data->cliente->correo_electronico;
            $direccion = $obj_pedido->data->cliente->direccion;
            $numero_int = $obj_pedido->data->cliente->numero_int;
            $numero_ext = $obj_pedido->data->cliente->numero_ext;
            $colonia = $obj_pedido->data->cliente->colonia;
            $municipio = $obj_pedido->data->cliente->municipio;
            $estado = $obj_pedido->data->cliente->estado;
            $direccion_completa = $direccion . " " . $numero_int . " " . $numero_ext . " " . $colonia . " " . $municipio . " " . $estado . " " . $codigo_postal;
            $numero_economico = $obj_pedido->data->automovil->numero_economico;
            $capacidad_personas = $obj->capacidad;
            $tipos_compra = $obj_pedido->data->automovil->tipo_venta;
        }

        if ($tipo_auto == 2) {
            $dataFromApi = $this->curl->curlGet(API_URL_AUTOS . 'seminuevos/api/get?id_remision=' . $remision_id, true);
            $obj = json_decode($dataFromApi);
            //$obj = $obj[0];
            //datos del pedido
            $data_pedido = $this->curl->curlGet(API_URL_AUTOS . 'ventas/ventas_api/api/getByVentaId?venta_id=' . $id_solicitud, true);
            $obj_pedido = json_decode($data_pedido);
            $precio_venta = $obj_pedido->data->automovil->u_precio_venta;
            $vendedor_nombre = $obj_pedido->data->vendedor->nombre;
            $vendedor_clave = $obj_pedido->data->vendedor->clave;
            $pedido = $obj_pedido->data->num_pedido;
            $nombre_cliente = $obj_pedido->data->cliente->nombre_empresa;
            $cliente_rfc = $obj_pedido->data->cliente->rfc;
            $codigo_postal = $obj_pedido->data->cliente->codigo_postal;
            $correo_electronico = $obj_pedido->data->cliente->correo_electronico;

            $direccion = $obj_pedido->data->cliente->direccion;
            $numero_int = $obj_pedido->data->cliente->numero_int;
            $numero_ext = $obj_pedido->data->cliente->numero_ext;
            $colonia = $obj_pedido->data->cliente->colonia;
            $municipio = $obj_pedido->data->cliente->municipio;
            $estado = $obj_pedido->data->cliente->estado;
            $direccion_completa = $direccion . " " . $numero_int . " " . $numero_ext . " " . $colonia . " " . $municipio . " " . $estado . " " . $codigo_postal;
            $numero_economico = $obj_pedido->data->automovil->numero_economico;

            $tipos_compra = $obj_pedido->data->automovil->tipo_venta;
        }




        // var_dump($obj[0]->unidad_descripcion);
        // die();

        $aux = $this->Mgeneral->get_row('id_solicitud', $id_solicitud, 'factura_vehiculo');
        //datos generales de la factura
        $id_factura = "";
        if (!is_object($aux)) {
            $emisor = $this->Mgeneral->get_row('id', 1, 'datos');
            $id_factura = get_guid();
            $data['facturaID'] = $id_factura;
            $data['status'] = 1;
            $data['emisor_RFC'] = $emisor->rfc;
            $data['emisor_regimenFiscal'] = $emisor->regimen;
            $data['emisor_nombre'] = $emisor->razon_social;
            $data['receptor_uso_CFDI'] = "G03";
            $data['pagada'] = "SI";
            $data['tipo_factura'] = 3;
            $this->Mgeneral->save_register('factura_vehiculo', $data);
        } else {
            $id_factura = $aux->facturaID;
        }

        $data_receptor['intercambio'] = $tipos_compra;
        if ($tipo_auto == 1) {
            $data_receptor['receptor_nombre'] = $nombre_cliente;
            //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
            $data_receptor['fatura_lugarExpedicion'] = $codigo_postal; //codigo postal
            $data_receptor['receptor_email'] = $correo_electronico;
            $data_receptor['receptor_direccion'] = $direccion_completa;
            $data_receptor['capacidad_kg'] = $obj->capacidad_kg;
            $data_receptor['capacidad_personas'] = $capacidad_personas;
            $data_receptor['tipo_auto_nuevo'] = $obj->tipo_auto;
            $data_receptor['combustible'] = $obj->combustible;
            $data_receptor['pdf_modelo'] = $obj->modelo;
        }

        if ($tipo_auto == 2) {
            $data_receptor['receptor_nombre'] = $nombre_cliente;
            //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
            $data_receptor['fatura_lugarExpedicion'] = $codigo_postal; //codigo postal
            $data_receptor['receptor_email'] = $correo_electronico;
            $data_receptor['receptor_direccion'] = $direccion_completa;
            $data_receptor['pdf_modelo'] = $obj->modelo;
        }

        if ($tipo_auto == 1) {
            $impor = "";
            if ($obj->nacional_importado == "Nacional") {
                $impor = "no";
            }
            if ($obj->nacional_importado == "Importado") {
                $impor = "si";
            }
            $data_receptor['unidad_importada'] = $impor;
        }

        if ($tipo_auto == 2) {
            $data_receptor['unidad_importada'] = $obj->unidad_importada;
        }



        $data_receptor['factura_moneda'] = "MNX";
        $data_receptor['factura_fecha'] = date('Y-m-d H:i:s');
        $data_receptor['receptor_RFC'] = $cliente_rfc;
        $data_receptor['factura_folio'] = "1111";
        $data_receptor['factura_serie'] = "2222";
        $data_receptor['factura_formaPago'] = "01";
        $data_receptor['factura_medotoPago'] = "PPD";
        $data_receptor['factura_tipoComprobante'] = "I";
        $data_receptor['receptor_uso_CFDI'] = "G03";
        //$data['pagada'] = $this->input->post('pagada');
        $data_receptor['comentario'] = $obj->leyenda_dcto;
        //$data['almacen'] = $this->input->post('almacen');
        $data_receptor['id_solicitud'] = $id_solicitud;
        $data_receptor['remision_id'] = $remision_id;
        $data_receptor['tipo_auto'] = $tipo_auto;
        $data_receptor['regimen_fiscal'] = $obj->regimen_fiscal;
        // este es para seminuevo
        if ($tipo_auto == 2) {
            $data_receptor['tipo_persona'] = $obj->id_regimen_fiscal;
        }
        //para seminuevo
        if ($tipo_auto == 2) {
            $data_receptor['tipo_auto_seminuevo'] = $obj->id_tipo_automovil;
        }
        // este es para seminuevo
        if ($tipo_auto == 2) {
            $data_receptor['total_compra_seminuevo'] = $obj->c_total;
        }

        $data_receptor['vendedor_nombre'] = $vendedor_nombre;
        $data_receptor['vendedor_clave'] = $vendedor_clave;
        $data_receptor['pedido'] = $pedido;
        $data_receptor['nombre_cliente'] = $nombre_cliente;
        $data_receptor['numero_economico'] = $obj->economico; //$numero_economico;

        $this->Mgeneral->update_table_row('factura_vehiculo', $data_receptor, 'facturaID', $id_factura);

        $response['id_factura_auto'] = $id_factura;

        //datos del vehiculo
        $data_concepto['concepto_facturaId'] = $id_factura;

        if ($tipo_auto == 1) {
            $data_concepto['concepto_NoIdentificacion'] = $obj->economico;
        }
        if ($tipo_auto == 2) {
            $data_concepto['concepto_NoIdentificacion'] = $obj->economico;
        }


        $data_concepto['concepto_unidad'] = 4;
        //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
        if ($tipo_auto == 1) {
            $data_concepto['clave_sat'] = $obj->clave_sat;
            $data_concepto['unidad_sat'] = $obj->unidad;
        }
        if ($tipo_auto == 2) {
            $data_concepto['clave_sat'] = '80141615';
            $data_concepto['unidad_sat'] = 'H87';
        }


        if ($tipo_auto == 1) {
            $data_concepto['concepto_nombre'] = $obj->unidad_descripcion;
        }
        if ($tipo_auto == 2) {
            $data_concepto['concepto_nombre'] = $obj->c_concepto;
        }


        $data_concepto['concepto_precio'] = number_format($obj->c_subtotal, 2, '.', '');
        $data_concepto['concepto_importe'] = number_format($obj->c_subtotal, 2, '.', '');
        $data_concepto['impuesto_iva'] = "002";
        $data_concepto['impuesto_iva_tipoFactor'] = "cuota";
        $data_concepto['impuesto_iva_tasaCuota'] = ".16";
        $data_concepto['impuesto_ISR'] = "";
        $data_concepto['impuesto_ISR_tasaFactor'] = "";
        $data_concepto['impuestoISR_tasaCuota'] = "";
        $data_concepto['tipo'] = 0;
        $data_concepto['nombre_interno'] = "";
        $data_concepto['id_producto_servicio_interno'] = 0;
        $data_concepto['concepto_cantidad'] = "1";
        $importe_iva = $obj->c_subtotal * .16;
        $data_concepto['importe_iva'] = number_format($importe_iva, 2, '.', '');
        $data_concepto['fecha_creacion'] = date('Y-m-d H:i:s');

        if ($tipo_auto == 1) {
            $data_concepto['clave_vehicular'] = $obj->clave_vehicular;
            $data_concepto['niv'] = $obj->serie;
            $data_concepto['numero'] = $obj->pedimento;
            $data_concepto['fecha'] = $obj->fecha_pedimento;
            $data_concepto['aduana'] = $obj->aduana;
        }

        if ($tipo_auto == 1) {
            $data_concepto['c_valor_unidad'] = $obj->c_valor_unidad;
            $data_concepto['c_equipo_base'] = $obj->c_equipo_base;
            $data_concepto['c_gastos_traslado'] = $obj->c_gastos_traslado;
            $data_concepto['c_holdback'] = $obj->c_holdback;
            $data_concepto['c_cuenta_pub'] = $obj->c_cuenta_pub;
            $data_concepto['c_donativo_ccf'] = $obj->c_donativo_ccf;
            $data_concepto['c_plan_piso'] = $obj->c_plan_piso;
        }

        if ($tipo_auto == 2) {
            $data_concepto['clave_vehicular'] = $obj->clave_vehicular;
            $data_concepto['niv'] = $obj->serie;
            $data_concepto['numero'] = $obj->numero_pedimento;
            $data_concepto['fecha'] = $obj->fecha_pedimento;
            $data_concepto['aduana'] = $obj->aduana_pedimento;
        }

        $data_concepto['economico'] = $obj->economico;
        $data_concepto['motor'] = $obj->motor;
        $data_concepto['combustible'] = $obj->combustible;
        $data_concepto['no_inventario'] = $obj->no_inventario;

        if ($tipo_auto == 1) {
            $data_concepto['repuve'] = $obj->repuve;
        }

        if ($tipo_auto == 2) {
            $data_concepto['repuve'] = $obj->repuve;
        }
        if ($tipo_auto == 1) {
            $data_concepto['cilindros'] = $obj->cilindros;
        }
        if ($tipo_auto == 2) {
            $data_concepto['cilindros'] =  $obj->numero_cilindros;
        }

        $data_concepto['serie_corta'] = $obj->serie_corta;
        $data_concepto['puertas'] = $obj->puertas;
        if ($tipo_auto == 1) {
            $data_concepto['precio_costo'] = $obj->precio_costo;
        }
        if ($tipo_auto == 2) {
            $data_concepto['precio_costo'] = 0;
        }

        if ($tipo_auto == 1) {
            $data_concepto['id_linea'] = $obj->linea_id_contabilidad;
        }

        if ($tipo_auto == 2) {
            $data_concepto['id_linea'] = 0;
        }

        $data_concepto['no_inventario'] = $obj->no_inventario;
        if ($tipo_auto == 1) {
            $data_concepto['procedencia'] = $obj->procedencia;
        }
        if ($tipo_auto == 2) {
            $data_concepto['procedencia'] = 'procedencia';
        }

        $data_concepto['no_puertas'] = $obj->puertas;
        if ($tipo_auto == 1) {
            $data_concepto['no_cilindros'] = $obj->cilindros;
        }
        if ($tipo_auto == 2) {
            $data_concepto['no_cilindros'] = $obj->cilindros;
        }

        if ($tipo_auto == 1) {
            $data_concepto['capacidad'] = $obj->capacidad;
        }
        if ($tipo_auto == 2) {
            $data_concepto['capacidad'] = 2;
        }

        $data_concepto['combustible'] = $obj->combustible;
        $data_concepto['marca'] = "FORD";
        $data_concepto['linea'] = $obj->modelo_descripcion;
        $data_concepto['leyenda_dcto'] = $obj->leyenda_dcto;

        if ($tipo_auto == 2) {
            // $data_concepto['base_iva'] = $obj->b_iva;
            $data_concepto['precio_venta'] = $precio_venta; //$obj->b_precio_venta;
        }
        if ($tipo_auto == 1) {
            // $data_concepto['base_iva'] = $obj->b_iva;
            $data_concepto['precio_venta'] = $precio_venta; //$obj->b_precio_venta;
        }

        $aux_conce = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');

        if (!is_object($aux_conce)) {
            $this->Mgeneral->save_register('factura_conceptos_vehiculo', $data_concepto);
        } else {
            $this->Mgeneral->update_table_row('factura_conceptos_vehiculo', $data_concepto, 'concepto_facturaId', $id_factura);
        }



        //var_dump($data_concepto);
        //var_dump($data_receptor);
        //var_dump($data);
        //die();

        redirect("facturacion_vehiculos/nueva/" . $id_factura);
    }
}
