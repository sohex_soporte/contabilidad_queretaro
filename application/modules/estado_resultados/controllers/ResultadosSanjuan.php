<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ResultadosSanjuan extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $data['cuentas'] =
            [
                ['title' => 'VOLÚMEN VENTAS'],
                $this->calcular_cuenta(110001),
                $this->calcular_cuenta(123001),
                $this->calcular_cuenta(500056),
                ['title' => 'VENTAS'],
                $this->calcular_cuenta(110001),
                $this->calcular_cuenta(110002),
                $this->calcular_cuenta(360001),
                $this->calcular_cuenta(340001),
                $this->calcular_cuenta(370003),
                ['nombre' => 'TOTAL VENTAS'],
                ['title' => 'UTILIDAD BRUTA'],
                ['nombre' => 'VEHICULOS'],
                ['nombre' => 'SEMINUEVOS'],
                ['nombre' => 'SERVICIO'],
                $this->calcular_utilidad_bruta(),
                ['nombre' => 'HOJALATERA Y PINTURA'],
                ['nombre' => 'TOTAL'],
                ['title' => 'SUELDOS Y COMISIONES'],
                ['nombre' => 'Ventas Nuevos'],
                ['nombre' => 'Ventas Semi Nuevos'],
                ['nombre' => 'Refacciones y Servicio'],
                ['nombre' => 'Administración'],
                ['nombre' => 'Prestaciones'],
                ['nombre' => 'Uniformes'],
                ['nombre' => 'Limpieza y Vigilancia'],
                ['nombre' => 'Subsidios U. Nuevas'],
                ['nombre' => 'Publicidad'],
                ['nombre' => 'Promoción'],
                ['nombre' => 'Traslado Unidades'],
                ['nombre' => 'Almacenamiento'],
                ['nombre' => 'Conauto'],
                ['nombre' => 'Gasolina y Lubricantes'],
                ['nombre' => 'Teléfono'],
                ['nombre' => 'Gastos de Viaje'],
                ['nombre' => 'Herramientas'],
                ['nombre' => 'Entrenamiento'],
                ['nombre' => 'Servicio Gratuito	'],
                ['nombre' => 'Fletes'],
                ['nombre' => 'Impuestos'],
                ['nombre' => 'Depreciación Activos'],
                ['nombre' => 'Mantenimiento'],
                ['nombre' => 'Renta'],
                ['nombre' => 'Agua y Luz'],
                ['nombre' => 'Seguros y Finanzas'],
                ['nombre' => 'Honorarios'],
                ['nombre' => 'Administración'],
                ['nombre' => 'Cuotas y Donativos'],
                ['nombre' => 'Sistemas'],
                ['nombre' => 'Papelería'],
                ['nombre' => 'Otros'],
                ['nombre' => 'Total gastos'],
                ['nombre' => 'Utilidad de Operación']
            ];
        $this->blade->render('sanjuan/index', $data);
    }

    public function index2()
    {
        $this->blade->render('sanjuan/index_');
    }

    private function calcular_cuenta($cuenta)
    {
        $this->load->model('Estado_resultadosmodel');
        $name = $this->Estado_resultadosmodel->nombre_cuenta($cuenta);
        $datos['nombre'] = $name ? $name['descripcion'] : 'Sin nombre';
        $total_meses = 0;
        if ($name) {
            for ($i = 1; $i <= 12; $i++) {
                $total = $this->Estado_resultadosmodel->saldo_cuenta($cuenta, $i, date('Y'));
                $total_meses += $total['saldo_mes'];
                $datos['meses'][$i] = $total ? $total['saldo_mes'] : 0;
            }
            $datos['total'] = $total_meses;
        }
        return $datos;
    }

    private function calcular_utilidad_bruta()
    {
        $this->load->model('Estado_resultadosmodel');
        $this->Estado_resultadosmodel->getRefacciones();
        $datos['nombre'] = 'REFACCIONES';
        $total_meses = 0;
        $utilidad_bruta = $this->Estado_resultadosmodel->get_refacciones_reporte();
        for ($i = 1; $i <= 12; $i++) {
            $total_meses += $utilidad_bruta['mes_' . $i] ? $utilidad_bruta['mes_' . $i] : 0;
            $datos['meses'][$i] = $utilidad_bruta['mes_' . $i] ? $utilidad_bruta['mes_' . $i] : 0;
        }
        $datos['total'] = $total_meses;
        return $datos;
    }
}
