<?php

/**

 **/
class Estado_resultadosmodel extends CI_Model
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
  }


  public function nombre_cuenta($cuenta)
  {
    $this->db->select('decripcion as descripcion')
      ->from('cuentas_dms')
      ->where('cuenta', $cuenta);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function saldo_cuenta($cuenta, $mes, $anio)
  {
    $this->db->select('saldo_mes')
      ->from('ma_balanza')
      ->where('cuenta', $cuenta)
      ->where('mes', $mes)
      ->where('anio', $anio);
    $query = $this->db->get();

    return $query->row_array();
  }

  public function getRefacciones()
  {
    $database = $this->load->database('default', TRUE);
    $query = $database->query("call pa_calcular_refacciones_sjr();");
    $database->close();
    return $query->result_array();
  }

  public function get_refacciones_reporte()
  {
    $this->db->select('*')
      ->from('refacciones_sjr_temp')
      ->where('id', 14);
    $query = $this->db->get();

    return $query->row_array();
  }

}
