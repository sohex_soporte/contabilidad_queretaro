<?php defined('BASEPATH') or exit('No direct script access allowed');


class Api extends MY_Controller
{

    public $results = array('estatus' => 'ok', 'data' => false, 'info' => false);
    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model('Archivos_model', '', TRUE);
    }

    public function getAllArchivos()
    {
        $result = $this->Archivos_model->getAll();
        return $this->response($result, $this->http_status);
    }

    public function saveArchivo()
    {
        try{

        
            $this->load->library('ftp');
            $config['hostname'] = '';
            $config['username'] = '';
            $config['password'] = '';
            $config['debug'] = TRUE;

            $conn = $this->ftp->connect($config);
            $list = $this->ftp->list_files('/archivos');
            if($conn)
            {
                if(!empty($list))
                {
                    foreach($list as $archivo){
                        $nameArchivo = substr($archivo,10,strlen($archivo));
                        $res= $this->Archivos_model->get(['nombreArchivo' => $nameArchivo]);
                        if(!$res){
                            $data = [
                                'nombreArchivo' => $nameArchivo,
                                'url' => '/archivos/'.$nameArchivo
                            ];
                            $this->Archivos_model->insert($data);
                        }
                    }
                    $results['mensaje'] = "Proceso Terminado";
                }
            }
            else{
                $results['mensaje'] = "Error al conectar al FTP";
            }
            

            $this->ftp->close();
            //utils::pre();
        } catch (Exception $e) {
            $results['mensaje'] = $e;
            //$this->ftp->close();
        }
        
        echo json_encode($results);
    }


}