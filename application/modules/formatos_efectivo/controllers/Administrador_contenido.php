<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Administrador_contenido extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('CaFormatoFlujoEfectivo');
        $listado = $this->CaFormatoFlujoEfectivo->getAll();

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('Fecha','Banco','Acciones');

        if(is_array($listado) && count($listado)>0){
            foreach($listado as $key => $value){
                $button_ver_contenido = '<a href="'.site_url('formatos_efectivo/administrador_contenido/ver_contenido/'.$value['id']).'" type="button" class="btn btn-primary btn-sm"><i class="fas fa-list-ol"></i></a>';
                $this->table->add_row(utils::aFecha($value['fecha'],true), $value['banco_nombre'],$button_ver_contenido);
            }
        }else{
            $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 3);
            $this->table->add_row($cell);
        }

        $tabla = $this->table->generate();

        $data = [
            'listado' => $listado,
            'tabla' => $tabla
        ];

        $this->blade->render('/administrador_contenido/index', $data);
    }

    public function ver_contenido($formato_id)
    {
        $this->load->model('CaFormatoFlujoEfectivo');
        $this->carabiner->display('sweetalert2');

        $contenido = $this->CaFormatoFlujoEfectivo->get([
            'ca_formato_flujo_efectivo.id' => $formato_id
        ]);
        // utils::pre($contenido,false);
        
        $this->load->model('DeFormatoFlujoEfectivo');
        $contenido_listado = $this->DeFormatoFlujoEfectivo->getAll([
            'formato_flujo_efectivo_id' => $formato_id
        ]);
        // utils::pre($contenido_listado,false);

        $tabla = '';

        if(is_array($contenido) && count($contenido)>0){
            switch ((int)$contenido['banco_id']) {
                case 1:
                    $tabla = $this->construir_tabla_bbva($contenido_listado);
                    break;

                case 2:
                    $tabla = $this->construir_tabla_banamex($contenido_listado);
                    break;

                case 3:
                    $tabla = $this->construir_tabla_banorte($contenido_listado);
                    break;

                case 4:
                    $tabla = $this->construir_tabla_hsbc($contenido_listado);
                    break;

                case 5:
                    $tabla = $this->construir_tabla_scotiabank($contenido_listado);
                    break;
            }
        }

        // utils::pre($tabla);
        $this->load->model('CaPersonas_model');
        $this->load->model('CaProcedencias_model');
        $this->load->model('CaSucursales_model');
        $personas = $this->CaPersonas_model->getAll();
        $procedencias = $this->CaProcedencias_model->getAll();
        $sucursales = $this->CaSucursales_model->getAll();

        // PERSONAS VENDORAS API DMS
        $listado_vendores = utils::api_get([
            'url' => CONTABILIDAD_APIS.'dms_queretaro/usuarios/vendedores_mostrador'
        ]);

        $data = [
            'personas' => $personas,
            'sucursales' => $sucursales,
            'procedencias' => $procedencias,
            'contenido' => $contenido,
            'tabla' => $tabla,
            'listado_vendores' => ((is_array($listado_vendores) && array_key_exists('data',$listado_vendores))? $listado_vendores['data'] : false)
        ];
        $this->blade->render('administrador_contenido/ver_contenido', $data);
    }

    public function construir_tabla_scotiabank($contenido_listado){

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('Número','Fecha','Referencia Numérica','Importe','Tipo','Transacción','Saldo','Leyenda 1','Leyenda 2', 'Sucursal', 'Cliente/Proveedor', 'Vendedor', 'Departamento','Acciones');

        if(is_array($contenido_listado) && count($contenido_listado)>0){
            foreach($contenido_listado as $value){
                $reglon = (array)json_decode($value['reglon'],true);
                $nombre_completo = $value['nombre'] . ' ' . $value['apellido1'] . ' ' . $value['apellido2'];
                $this->table->add_row([
                    (array_key_exists('numero',$reglon)? $reglon['numero'] : '' ),
                    (array_key_exists('fecha',$reglon)? $reglon['fecha'] : '' ),
                    (array_key_exists('referencia_numerica',$reglon)? $reglon['referencia_numerica'] : '' ),
                    (array_key_exists('importe',$reglon)? utils::format($reglon['importe']) : '' ),
                    (array_key_exists('tipo',$reglon)? $reglon['tipo'] : '' ),
                    (array_key_exists('concepto',$reglon)? $reglon['concepto'] : '' ),
                    (array_key_exists('saldo',$reglon)? utils::format($reglon['saldo']) : '' ),
                    (array_key_exists('leyenda1',$reglon)? $reglon['leyenda1'] : '' ),
                    (array_key_exists('leyenda2',$reglon)? $reglon['leyenda2'] : '' ),
                    isset($value['sucursal']) ? $value['sucursal'] : '',
                    isset($value['persona_id']) ? ($value['tipo_persona_id'] == 1 ? 'Cliente - ' . $nombre_completo : 'Proveedor - ' . $nombre_completo) : '',
                    (array_key_exists('vendedor',$reglon)? $reglon['vendedor'] : '' ),
                    isset($value['procedencia']) ? $value['procedencia'] : '',
                    '<button onclick="open_modal(this);" id="accion_anexo" data-persona_id="'.$value['persona_id'].'" data-sucursal="'.$value['sucursal_id'].'" data-vendedor="'.$value['id_vendedor'].'" data-departamento="'.$value['procedencia_id'].'" data-estatus="'.$value['estatus'].'" data-id="'.$value['id'].'" class="btn btn-primary"><i class="fa fa-file text-white"></i></button>',

                ]);
            }
        }
        return $this->table->generate();
    }

    public function construir_tabla_hsbc($contenido_listado){

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('Referencia del banco','Descripción','Referencia de cliente','Tipo de TRN','Importe de crédito','Importe del débito','Saldo','Fecha del apunte', 'Sucursal', 'Cliente/Proveedor', 'Vendedor','Departamento', 'Acciones');

        if(is_array($contenido_listado) && count($contenido_listado)>0){
            foreach($contenido_listado as $value){
                $nombre_completo = $value['nombre'] . ' ' . $value['apellido1'] . ' ' . $value['apellido2'];
                $reglon = (array)json_decode($value['reglon'],true);
                $this->table->add_row([
                    (array_key_exists('referencia_banco',$reglon)? $reglon['referencia_banco'] : '' ),
                    (array_key_exists('concepto',$reglon)? $reglon['concepto'] : '' ),
                    (array_key_exists('referencia_cliente',$reglon)? $reglon['referencia_cliente'] : '' ),
                    (array_key_exists('tipo_trn',$reglon)? $reglon['tipo_trn'] : '' ),
                    (array_key_exists('haber',$reglon)? utils::format($reglon['haber']) : '' ),
                    (array_key_exists('debe',$reglon)? utils::format($reglon['debe']) : '' ),
                    (array_key_exists('saldo',$reglon)? utils::format($reglon['saldo']) : '' ),
                    (array_key_exists('fecha',$reglon)? $reglon['fecha'] : '' ),
                    isset($value['sucursal']) ? $value['sucursal'] : '',
                    isset($value['persona_id']) ? ($value['tipo_persona_id'] == 1 ? 'Cliente - ' . $nombre_completo : 'Proveedor - ' . $nombre_completo) : '',
                    (array_key_exists('vendedor',$reglon)? $reglon['vendedor'] : '' ),
                    isset($value['procedencia']) ? $value['procedencia'] : '',
                    '<button onclick="open_modal(this);" id="accion_anexo" data-persona_id="'.$value['persona_id'].'" data-sucursal="'.$value['sucursal_id'].'" data-vendedor="'.$value['id_vendedor'].'" data-departamento="'.$value['procedencia_id'].'" data-estatus="'.$value['estatus'].'" data-id="'.$value['id'].'" class="btn btn-primary"><i class="fa fa-file text-white"></i></button>',
                ]);
            }
        }
        return $this->table->generate();
    }

    public function construir_tabla_banorte($contenido_listado){

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('CUENTA', 'FECHA DE OPERACIÓN','FECHA', 'REFERENCIA', 'DESCRIPCIÓN', 'COD. TRANSAC', 'SUCURSAL', 'DEPÓSITOS','RETIROS','SALDO','MOVIMIENTO', 'SUCURSAL', 'CLIENTE/PROVEEDOR', 'VENDEDOR','DPTO', 'DESCRIPCIÓN DETALLADA', 'ACCIONES');

        if(is_array($contenido_listado) && count($contenido_listado)>0){
            foreach($contenido_listado as $value){
                $nombre_completo = $value['nombre'] . ' ' . $value['apellido1'] . ' ' . $value['apellido2'];
                $reglon = (array)json_decode($value['reglon'],true);
                $this->table->add_row([
                    (array_key_exists('cuenta',$reglon)? $reglon['cuenta'] : '' ),
                    (array_key_exists('fecha_operacion',$reglon)? $reglon['fecha_operacion'] : '' ),
                    (array_key_exists('fecha',$reglon)? $reglon['fecha'] : '' ),
                    (array_key_exists('referencia',$reglon)? $reglon['referencia'] : '' ),
                    (array_key_exists('concepto',$reglon)? $reglon['concepto'] : '' ),
                    (array_key_exists('cod_trans',$reglon)? $reglon['cod_trans'] : '' ),
                    (array_key_exists('sucursal',$reglon)? $reglon['sucursal'] : '' ),
                    (array_key_exists('haber',$reglon)? utils::format($reglon['haber']) : '' ),
                    (array_key_exists('debe',$reglon)? utils::format($reglon['debe']) : '' ),
                    (array_key_exists('saldo',$reglon)? utils::format($reglon['saldo']) : '' ),
                    (array_key_exists('movimiento',$reglon)? $reglon['movimiento'] : '' ),
                    isset($value['sucursal']) ? $value['sucursal'] : '',
                    isset($value['persona_id']) ? ($value['tipo_persona_id'] == 1 ? 'Cliente - ' . $nombre_completo : 'Proveedor - ' . $nombre_completo) : '',
                    (array_key_exists('vendedor',$reglon)? $reglon['vendedor'] : '' ),
                    isset($value['procedencia']) ? $value['procedencia'] : '',
                    (array_key_exists('descripcion_detallada',$reglon)? $reglon['descripcion_detallada'] : '' ),
                    '<button onclick="open_modal(this);" id="accion_anexo" data-persona_id="'.$value['persona_id'].'" data-sucursal="'.$value['sucursal_id'].'" data-vendedor="'.$value['id_vendedor'].'" data-departamento="'.$value['procedencia_id'].'" data-estatus="'.$value['estatus'].'" data-id="'.$value['id'].'" class="btn btn-primary"><i class="fa fa-file text-white"></i></button>',
                ]);
            }
        }

        return $this->table->generate();

    }

    public function construir_tabla_banamex($contenido_listado){

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('Fecha', 'Descripción', 'Retiros', 'Depósitos', 'Saldo', 'Sucursal', 'Cliente/Proveedor', 'Vendedor', 'Departamento', 'Acciones');

        if(is_array($contenido_listado) && count($contenido_listado)>0){
            foreach($contenido_listado as $value){
                $nombre_completo = $value['nombre'] . ' ' . $value['apellido1'] . ' ' . $value['apellido2'];
                $this->table->add_row([
                    $value['fecha'],
                    $value['concepto'],
                    utils::format($value['cargo']),
                    utils::format($value['abono']),
                    utils::format($value['saldo']),
                    isset($value['sucursal']) ? $value['sucursal'] : '',
                    isset($value['persona_id']) ? ($value['tipo_persona_id'] == 1 ? 'Cliente - ' . $nombre_completo : 'Proveedor - ' . $nombre_completo) : '',
                    (array_key_exists('vendedor',$value)? $value['vendedor'] : '' ),
                    isset($value['procedencia']) ? $value['procedencia'] : '',
                    '<button onclick="open_modal(this);" id="accion_anexo" data-persona_id="'.$value['persona_id'].'" data-sucursal="'.$value['sucursal_id'].'" data-vendedor="'.$value['id_vendedor'].'" data-departamento="'.$value['procedencia_id'].'" data-estatus="'.$value['estatus'].'" data-id="'.$value['id'].'" class="btn btn-primary"><i class="fa fa-file text-white"></i></button>',
                ]);
            }
        }

        return $this->table->generate();

    }

    public function construir_tabla_bbva($contenido_listado){

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-bordered table-striped">'
        );
        $this->table->set_template($template);
        $this->table->set_heading('Fecha', 'Concepto / Referencia', 'Cargo', 'Abono', 'Saldo', 'Sucursal', 'Cliente/Proveedor', 'Vendedor','Departamento', 'Acciones');

        if(is_array($contenido_listado) && count($contenido_listado)>0){
            foreach($contenido_listado as $value){

                $nombre_completo = $value['nombre'] . ' ' . $value['apellido1'] . ' ' . $value['apellido2'];
                $this->table->add_row([
                    $value['fecha'],
                    $value['concepto'],
                    utils::format($value['cargo']),
                    utils::format($value['abono']),
                    utils::format($value['saldo']),
                    isset($value['sucursal']) ? $value['sucursal'] : '',
                    isset($value['persona_id']) ? ($value['tipo_persona_id'] == 1 ? 'Cliente - ' . $nombre_completo : 'Proveedor - ' . $nombre_completo) : '',
                    (array_key_exists('vendedor',$value)? $value['vendedor'] : '' ),
                    isset($value['procedencia']) ? $value['procedencia'] : '',
                    '<button onclick="open_modal(this);" id="accion_anexo" data-persona_id="'.$value['persona_id'].'" data-sucursal="'.$value['sucursal_id'].'" data-vendedor="'.$value['id_vendedor'].'" data-departamento="'.$value['procedencia_id'].'" data-estatus="'.$value['estatus'].'" data-id="'.$value['id'].'" class="btn btn-primary"><i class="fa fa-file text-white"></i></button>',

                ]);
            }
        }

        return $this->table->generate();

    }
}
