<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_ingresos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    // public function buscar_cuenta($importe_list,$fecha = false,$origen = false){
    //     $response = 0;
    //     if(is_array($importe_list)){
    //         foreach ($importe_list as $key => $value) {
    //             if($value['fecha_creacion'] == $fecha && $value['origen_transaccion_id'] == $origen ){                    
    //                 $response = $value['monto'];
    //                 break;
    //             }
                
    //         }
    //     }
    //     return $response;
    // }

    public function buscar_cuenta($fecha = false,$origen = false){
        $response = 0;
        $rst = $this->DeFormatoFlujoEfectivo->getSum(array(
            'ca_formato_flujo_efectivo.banco_id' => $origen,
            'ca_formato_flujo_efectivo.fecha' => $fecha
        ));

        if(is_array($rst)){
            $response = $rst['abono'];
        }
        return $response;
    }


    public function buscar_cuenta_cargo($fecha = false,$origen = false){
        $response = 0;
        $rst = $this->DeFormatoFlujoEfectivo->getSum(array(
            'ca_formato_flujo_efectivo.banco_id' => $origen,
            'ca_formato_flujo_efectivo.fecha' => $fecha
        ));

        if(is_array($rst)){
            $response = $rst['cargo'];
        }
        return $response;
    }

    public function buscar_cuenta2($fecha = false,$origen = false){
        $response = 0;
        $rst = $this->DeFormatoFlujoEfectivo->getSum(array(
            'de_formato_flujo_efectivo.procedencia_id' => $origen,
            'ca_formato_flujo_efectivo.fecha' => $fecha
        ));

        if(is_array($rst)){
            $response = $rst['abono'] - $rst['cargo'];
        }
        return $response;
    }
    public function buscar_cuenta3($fecha = false,$origen = false){
        $response = 0;
        $this->load->model('DeTiposIngresosLibres_model');
        $rst = $this->DeTiposIngresosLibres_model->get(array(
            'de_ingresos_libres.id_tipo_ingreso_libre' => $origen,
            'de_ingresos_libres.fecha' => $fecha
        ));

        if(is_array($rst)){
            $response = $rst['monto'];
        }
        return $response;
    }

    public function buscar(){ 
        
        $response = array();
        $this->load->model('DeFormatoFlujoEfectivo');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mes_concentrado', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('mes_concentrado');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaBancos_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();

            $listado_origenes = $this->CaBancos_model->getAll();


            
            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >',
                'heading_cell_start' => '<th class="table-warning">'
            );
            $this->table->set_template($template);

                $count_semanas = 1;
                $columna = ['BANCOS'];
                
                $columna[] = 'TOTAL SEMANA';
                $this->table->set_heading($columna);

                $columna_total = [];
                $columna_total_totales = [];
                $columna_total_label = [];
                $columna_total_label['titulo'] = array('data' => 'TOTAL DE FLUJO', 'class' => '');
                $total_monto_flujo = 0;

                $mes_concentrado_items = json_decode($semana,true);

                $columna_header = ['BANCOS'];

                $columna_header_band = false;
               
                $reglones = [];
                $reglones_total = [];

                if(is_array($listado_origenes) && count($listado_origenes)>0){
                    foreach ($listado_origenes as $key_origen => $value_origen) {
                        
                        $columna = [];
                        

                        $band_banco = false;
                        foreach($mes_concentrado_items as $mes_concentrado){
                            $num_semana = $mes_concentrado['semana'];
                            $dias = json_decode($mes_concentrado['dias']);

                            if($band_banco == false){
                                $band_banco = true;
                                $columna[] = $value_origen['nombre'];
                            }
                            
                            $monto_total = 0;
                            if(is_array($dias) && count($dias)>0){
                                foreach ($dias as $key_dias => $value_dias) {
                                    if($columna_header_band == false){
                                        $columna_header[] = '<center>'.utils::get_nombre_dia($value_dias).'<br/>'.utils::aFecha($value_dias,true).'</center>';
                                    }
                                
                                    $monto = $this->buscar_cuenta(
                                        $value_dias,
                                        $value_origen['id']
                                    ); 

                                    $dia = str_replace('-','',$value_dias); 
                                    if(array_key_exists($dia,$columna_total)){
                                        $columna_total[$dia] = $columna_total[$dia] + $monto;
                                    }else{
                                        $columna_total[$dia] = $monto;
                                    }
                                    if(array_key_exists($num_semana,$columna_total_totales)){
                                        $columna_total_totales[$num_semana] = $columna_total_totales[$num_semana] + $monto;
                                    }else{
                                        $columna_total_totales[$num_semana] = $monto;
                                    }
                                    $columna_total_label[$dia] = array('data' => utils::format($columna_total[$dia]), 'class' => 'comprobacion_total_sup','fecha'=> $value_dias);
                                    $reglones[$dia] = $monto;
                                    $reglones_total[$num_semana] = $columna_total_totales[$num_semana];
                                    
                                    $columna[] = utils::format($monto);
                                    $monto_total =  $monto_total + $monto;
                                    $total_monto_flujo = $total_monto_flujo + $columna_total[$dia];
                                    // utils::pre($total_monto_flujo,false);
                                }
                            }
                            $columna[] = array('data' => utils::format($monto_total), 'class' => 'table-primary');
                            
                            $columna_total_label[$num_semana] = array('data' => utils::format( $columna_total_totales[$num_semana]), 'class' => 'table-primary');
                            if($columna_header_band == false){
                                $columna_header[] = array('data' => 'TOTAL SEMANA '.$count_semanas, 'class' => 'table-primary');
                                $count_semanas = $count_semanas + 1;
                            }    
                        }

                        
                        
                        // utils::pre($reglones,false);
    
                        $columna[] = array('data' => utils::format(array_sum($reglones)), 'class' => 'table-secondary');
                        $reglones = [];
                        $this->table->add_row($columna);

                        // foreach ($reglones as $key_origen => $value_origen) {

                        // }
                        if($columna_header_band == false){

                            $columna_header[] = array('data' => 'TOTAL MES', 'class' => 'table-secondary');

                            $this->table->set_heading($columna_header);
                            $this->table->set_heading($columna_header);
                            $columna_header_band = true;
                        }

                        
                    }

                    // utils::pre($columna_total,false);
                    // utils::pre($columna_total_totales);

                    // $columna_total_label[] = utils::format( array_sum($columna_total_totales) );
                    // utils::pre($reglones_total);
                    $columna_total_label['mes'] = array('data' => utils::format(array_sum($reglones_total)), 'class' => 'table-secondary');
                    $this->table->add_row($columna_total_label);

                    //$columna_total_totales = [];

                    //utils::pre($reglones);

                    // $columna_total_label['total'] = utils::format( array_sum($columna_total) );
                    
                }
            

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            //utils::pre($this->table->generate());
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }


    public function buscar_cargos(){ 
        
        $response = array();
        $this->load->model('DeFormatoFlujoEfectivo');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mes_concentrado', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('mes_concentrado');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaBancos_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();

            $listado_origenes = $this->CaBancos_model->getAll();


            
            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >',
                'heading_cell_start' => '<th class="table-warning">'
            );
            $this->table->set_template($template);

                $count_semanas = 1;
                $columna = ['BANCOS'];
                
                $columna[] = 'TOTAL SEMANA';
                $this->table->set_heading($columna);

                $columna_total = [];
                $columna_total_totales = [];
                $columna_total_label = [];
                $columna_total_label['titulo'] = array('data' => '(  -  )  COMPRAS Y GASTOS', 'class' => '');
                $total_monto_flujo = 0;

                $mes_concentrado_items = json_decode($semana,true);

                $columna_header = ['BANCOS'];

                $columna_header_band = false;
               
                $reglones = [];
                $reglones_total = [];

                if(is_array($listado_origenes) && count($listado_origenes)>0){
                    foreach ($listado_origenes as $key_origen => $value_origen) {
                        
                        $columna = [];
                        

                        $band_banco = false;
                        foreach($mes_concentrado_items as $mes_concentrado){
                            $num_semana = $mes_concentrado['semana'];
                            $dias = json_decode($mes_concentrado['dias']);

                            if($band_banco == false){
                                $band_banco = true;
                                $columna[] = $value_origen['nombre'];
                            }
                            
                            $monto_total = 0;
                            if(is_array($dias) && count($dias)>0){
                                foreach ($dias as $key_dias => $value_dias) {
                                    if($columna_header_band == false){
                                        $columna_header[] = '<center>'.utils::get_nombre_dia($value_dias).'<br/>'.utils::aFecha($value_dias,true).'</center>';
                                    }
                                
                                    $monto = $this->buscar_cuenta_cargo(
                                        $value_dias,
                                        $value_origen['id']
                                    ); 

                                    $dia = str_replace('-','',$value_dias); 
                                    if(array_key_exists($dia,$columna_total)){
                                        $columna_total[$dia] = $columna_total[$dia] + $monto;
                                    }else{
                                        $columna_total[$dia] = $monto;
                                    }
                                    if(array_key_exists($num_semana,$columna_total_totales)){
                                        $columna_total_totales[$num_semana] = $columna_total_totales[$num_semana] + $monto;
                                    }else{
                                        $columna_total_totales[$num_semana] = $monto;
                                    }
                                    $columna_total_label[$dia] = array('data' => utils::format($columna_total[$dia]), 'class' => 'comprobacion_total_inf','fecha'=> $value_dias);
                                    $reglones[$dia] = $monto;
                                    $reglones_total[$num_semana] = $columna_total_totales[$num_semana];
                                    
                                    $columna[] = utils::format($monto);
                                    $monto_total =  $monto_total + $monto;
                                    $total_monto_flujo = $total_monto_flujo + $columna_total[$dia];
                                    // utils::pre($total_monto_flujo,false);
                                }
                            }
                            $columna[] = array('data' => utils::format($monto_total), 'class' => 'table-primary');
                            
                            $columna_total_label[$num_semana] = array('data' => utils::format( $columna_total_totales[$num_semana]), 'class' => 'table-primary');
                            if($columna_header_band == false){
                                $columna_header[] = array('data' => 'TOTAL SEMANA '.$count_semanas, 'class' => 'table-primary');
                                $count_semanas = $count_semanas + 1;
                            }    
                        }

                        
                        
                        // utils::pre($reglones,false);
    
                        $columna[] = array('data' => utils::format(array_sum($reglones)), 'class' => 'table-secondary');
                        $reglones = [];
                        // $this->table->add_row($columna);

                        // foreach ($reglones as $key_origen => $value_origen) {

                        // }
                        if($columna_header_band == false){

                            $columna_header[] = array('data' => 'TOTAL MES', 'class' => 'table-secondary');

                            $this->table->set_heading($columna_header);
                            $this->table->set_heading($columna_header);
                            $columna_header_band = true;
                        }

                        
                    }

                    // utils::pre($columna_total,false);
                    // utils::pre($columna_total_totales);

                    // $columna_total_label[] = utils::format( array_sum($columna_total_totales) );
                    // utils::pre($reglones_total);
                    $columna_total_label['mes'] = array('data' => utils::format(array_sum($reglones_total)), 'class' => 'table-secondary');
                    $this->table->add_row($columna_total_label);

                    //$columna_total_totales = [];

                    //utils::pre($reglones);

                    // $columna_total_label['total'] = utils::format( array_sum($columna_total) );
                    
                }
            

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            //utils::pre($this->table->generate());
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }



    public function buscar2(){ 
        
        $response = array();
        $this->load->model('DeFormatoFlujoEfectivo');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mes_concentrado', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('mes_concentrado');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaBancos_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();
            $this->load->model('CaTiposIngresosLibres_model');
            $this->db->where('tipo',1);
            $listado_origenes = $this->CaTiposIngresosLibres_model->getAll();


            
            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >',
                'heading_cell_start' => '<th class="table-warning">'
            );
            $this->table->set_template($template);

           
                $columna = ['BANCOS'];
                
                $columna[] = 'TOTAL SEMANA';
                $this->table->set_heading($columna);

                $columna_total = [];
                $columna_total_totales = [];
                $columna_total_label = [];
                $columna_total_label['titulo'] = array('data' => 'TOTAL DE FLUJO', 'class' => '');
                $total_monto_flujo = 0;

                $mes_concentrado_items = json_decode($semana,true);

                $columna_header = ['BANCOS'];

                $columna_header_band = false;
               
                $reglones = [];
                $reglones_total = [];

                if(is_array($listado_origenes) && count($listado_origenes)>0){
                    foreach ($listado_origenes as $key_origen => $value_origen) {
                        
                        $columna = [];
                        

                        $band_banco = false;
                        foreach($mes_concentrado_items as $mes_concentrado){
                            $num_semana = $mes_concentrado['semana'];
                            $dias = json_decode($mes_concentrado['dias']);

                            if($band_banco == false){
                                $band_banco = true;
                                $columna[] = $value_origen['nombre'];
                            }
                            
                            $monto_total = 0;
                            if(is_array($dias) && count($dias)>0){
                                foreach ($dias as $key_dias => $value_dias) {
                                    if($columna_header_band == false){
                                        $columna_header[] = '<center>'.utils::get_nombre_dia($value_dias).'<br/>'.utils::aFecha($value_dias,true).'</center>';
                                    }
                                
                                    $monto = $this->buscar_cuenta3(
                                        $value_dias,
                                        $value_origen['id']
                                    ); 

                                    $dia = str_replace('-','',$value_dias); 
                                    if(array_key_exists($dia,$columna_total)){
                                        $columna_total[$dia] = $columna_total[$dia] + $monto;
                                    }else{
                                        $columna_total[$dia] = $monto;
                                    }
                                    if(array_key_exists($num_semana,$columna_total_totales)){
                                        $columna_total_totales[$num_semana] = $columna_total_totales[$num_semana] + $monto;
                                    }else{
                                        $columna_total_totales[$num_semana] = $monto;
                                    }
                                    $columna_total_label[$dia] = array('data' => utils::format($columna_total[$dia]), 'class' => 'comprobacion_total total_ingresos_flujo','fecha'=> $value_dias);
                                    $reglones[$dia] = $monto;
                                    $reglones_total[$num_semana] = $columna_total_totales[$num_semana];
                                    
                                    $columna[] = array('data' => utils::format($monto), 'class' => '',"role"=>"button",'onclick' =>'editar_monto(this)','fecha'=>$value_dias,'identificador'=>$value_origen['id'],'monto'=> $monto,'reglon'=>$value_origen['nombre']);//utils::format($monto);
                                    $monto_total =  $monto_total + $monto;
                                    $total_monto_flujo = $total_monto_flujo + $columna_total[$dia];
                                    // utils::pre($total_monto_flujo,false);
                                }
                            }

                            $columna[] = array('data' => utils::format($monto_total), 'class' => 'table-primary');

                            $columna_total_label[$num_semana] = array('data' => utils::format( $columna_total_totales[$num_semana]), 'class' => 'table-primary');
                            if($columna_header_band == false){
                                $columna_header[] = array('data' => 'TOTAL SEMANA', 'class' => 'table-primary');
                            }    
                        }

                        
                        
                        // utils::pre($reglones,false);
    
                        $columna[] = array('data' => utils::format(array_sum($reglones)), 'class' => 'table-secondary');
                        $reglones = [];
                        $this->table->add_row($columna);

                        // foreach ($reglones as $key_origen => $value_origen) {

                        // }
                        if($columna_header_band == false){

                            $columna_header[] = array('data' => 'TOTAL MES', 'class' => 'table-secondary');
                            $this->table->set_heading($columna_header);
                            $columna_header_band = true;
                        }

                        
                    }

                    // utils::pre($columna_total,false);
                    // utils::pre($columna_total_totales);

                    // $columna_total_label[] = utils::format( array_sum($columna_total_totales) );
                    // utils::pre($reglones_total);
                    $columna_total_label['mes'] = array('data' => utils::format(array_sum($reglones_total)), 'class' => 'table-secondary');
                    $this->table->add_row($columna_total_label);

                    //$columna_total_totales = [];

                    //utils::pre($reglones);

                    // $columna_total_label['total'] = utils::format( array_sum($columna_total) );
                    
                }
            

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            //utils::pre($this->table->generate());
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }


    public function buscar4(){ 
        
        $response = array();
        $this->load->model('DeFormatoFlujoEfectivo');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mes_concentrado', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('mes_concentrado');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaBancos_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();
            $this->load->model('CaTiposIngresosLibres_model');
            $this->db->where('tipo',2);
            $listado_origenes = $this->CaTiposIngresosLibres_model->getAll();


            
            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >',
                'heading_cell_start' => '<th class="table-warning">'
            );
            $this->table->set_template($template);

           
                $columna = ['BANCOS'];
                
                $columna[] = 'TOTAL SEMANA';
                $this->table->set_heading($columna);

                $columna_total = [];
                $columna_total_totales = [];
                $columna_total_label = [];
                $columna_total_label['titulo'] = array('data' => 'TOTAL DE FLUJO', 'class' => '');
                $total_monto_flujo = 0;

                $mes_concentrado_items = json_decode($semana,true);

                $columna_header = ['BANCOS'];

                $columna_header_band = false;
               
                $reglones = [];
                $reglones_total = [];

                if(is_array($listado_origenes) && count($listado_origenes)>0){
                    foreach ($listado_origenes as $key_origen => $value_origen) {
                        
                        $columna = [];
                        

                        $band_banco = false;
                        foreach($mes_concentrado_items as $mes_concentrado){
                            $num_semana = $mes_concentrado['semana'];
                            $dias = json_decode($mes_concentrado['dias']);

                            if($band_banco == false){
                                $band_banco = true;
                                $columna[] = $value_origen['nombre'];
                            }
                            
                            $monto_total = 0;
                            if(is_array($dias) && count($dias)>0){
                                foreach ($dias as $key_dias => $value_dias) {
                                    if($columna_header_band == false){
                                        $columna_header[] = '<center>'.utils::get_nombre_dia($value_dias).'<br/>'.utils::aFecha($value_dias,true).'</center>';
                                    }
                                
                                    $monto = $this->buscar_cuenta3(
                                        $value_dias,
                                        $value_origen['id']
                                    ); 

                                    $dia = str_replace('-','',$value_dias); 
                                    if(array_key_exists($dia,$columna_total)){
                                        $columna_total[$dia] = $columna_total[$dia] + $monto;
                                    }else{
                                        $columna_total[$dia] = $monto;
                                    }
                                    if(array_key_exists($num_semana,$columna_total_totales)){
                                        $columna_total_totales[$num_semana] = $columna_total_totales[$num_semana] + $monto;
                                    }else{
                                        $columna_total_totales[$num_semana] = $monto;
                                    }
                                    $columna_total_label[$dia] = array('data' => utils::format($columna_total[$dia]), 'class' => ' ','dia'=> $dia);
                                    $reglones[$dia] = $monto;
                                    $reglones_total[$num_semana] = $columna_total_totales[$num_semana];
                                    
                                    $columna[] = array('data' => utils::format($monto), 'class' => 'comprobacion_total',"role"=>"button",'onclick' =>'editar_monto(this)','fecha'=>$value_dias,'identificador'=>$value_origen['id'],'monto'=> $monto,'reglon'=>$value_origen['nombre']);//utils::format($monto);
                                    // $columna[] = array('data' => utils::format($monto), 'class' => '','onclick' =>'editar_monto(this)','fecha'=>$dia,'identificador'=>$value_origen['id']);
                                    $monto_total =  $monto_total + $monto;
                                    $total_monto_flujo = $total_monto_flujo + $columna_total[$dia];
                                    // utils::pre($total_monto_flujo,false);
                                }
                            }

                            $columna[] = array('data' => utils::format($monto_total), 'class' => 'table-primary');

                            $columna_total_label[$num_semana] = array('data' => utils::format( $columna_total_totales[$num_semana]), 'class' => 'table-primary');
                            if($columna_header_band == false){
                                $columna_header[] = array('data' => 'TOTAL SEMANA', 'class' => 'table-primary');
                            }    
                        }

                        
                        
                        // utils::pre($reglones,false);
    
                        $columna[] = array('data' => utils::format(array_sum($reglones)), 'class' => 'table-secondary');
                        $reglones = [];
                        $this->table->add_row($columna);

                        // foreach ($reglones as $key_origen => $value_origen) {

                        // }
                        if($columna_header_band == false){

                            $columna_header[] = array('data' => 'TOTAL MES', 'class' => 'table-secondary');
                            $this->table->set_heading($columna_header);
                            $columna_header_band = true;
                        }

                        
                    }

                    // utils::pre($columna_total,false);
                    // utils::pre($columna_total_totales);

                    // $columna_total_label[] = utils::format( array_sum($columna_total_totales) );
                    // utils::pre($reglones_total);
                    $columna_total_label['mes'] = array('data' => utils::format(array_sum($reglones_total)), 'class' => 'table-secondary');
                    // $this->table->add_row($columna_total_label);

                    //$columna_total_totales = [];

                    //utils::pre($reglones);

                    // $columna_total_label['total'] = utils::format( array_sum($columna_total) );
                    
                }
            

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            //utils::pre($this->table->generate());
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }


    public function buscar3(){ 
        
        $response = array();
        $this->load->model('DeFormatoFlujoEfectivo');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mes_concentrado', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $semana = $this->input->post('mes_concentrado');
            $dias = @json_decode($this->input->post('dias'));

            $this->load->model('Asientos_model');
            $this->load->model('CaBancos_model');

            // $this->db->where_in('asientos.fecha_creacion',$dias);
            // $importe_list = $this->Asientos_model->get_total_por_fecha_origen();

            $listado_origenes = $this->CaBancos_model->getAll();


            
            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >',
                'heading_cell_start' => '<th class="table-warning">'
            );
            $this->table->set_template($template);

           
                $columna = ['BANCOS'];
                
                $columna[] = 'TOTAL SEMANA';
                $this->table->set_heading($columna);

                $columna_total = [];
                $columna_total_totales = [];
                $columna_total_label = [];
                $columna_total_label['titulo'] = array('data' => 'TOTAL DE FLUJO', 'class' => '');
                $total_monto_flujo = 0;

                $mes_concentrado_items = json_decode($semana,true);

                $columna_header = [''];

                $columna_header_band = false;
               
                $reglones = [];
                $reglones_total = [];

              
                        
                        $columna = [];
                        

                        $band_banco = false;
                        foreach($mes_concentrado_items as $mes_concentrado){
                            $num_semana = $mes_concentrado['semana'];
                            $dias = json_decode($mes_concentrado['dias']);

                            if($band_banco == false){
                                $band_banco = true;
                                $columna[] = 'SALDO INICIAL';
                            }
                            
                            $monto_total = 0;
                            if(is_array($dias) && count($dias)>0){
                                foreach ($dias as $key_dias => $value_dias) {
                                    if($columna_header_band == false){
                                        $columna_header[] = '<center>'.utils::get_nombre_dia($value_dias).'<br/>'.utils::aFecha($value_dias,true).'</center>';
                                    }
                                
                                    // $this->db->order_by('created_at','desc');
                                    // $this->db->order_by('orden','desc');
                                    // $this->db->limit(1);
                                    $monto_data = $this->DeFormatoFlujoEfectivo->getLast([
                                        'ca_formato_flujo_efectivo.fecha' => $value_dias
                                    ]); 

                                    if($monto_data == false){
                                        $monto = 0;
                                    }else{
                                        $monto = $monto_data['saldo'];
                                    }
                                    

                                    $dia = str_replace('-','',$value_dias); 
                                    if(array_key_exists($dia,$columna_total)){
                                        $columna_total[$dia] = $columna_total[$dia] + $monto;
                                    }else{
                                        $columna_total[$dia] = $monto;
                                    }
                                    if(array_key_exists($num_semana,$columna_total_totales)){
                                        $columna_total_totales[$num_semana] = $columna_total_totales[$num_semana] + $monto;
                                    }else{
                                        $columna_total_totales[$num_semana] = $monto;
                                    }
                                    $columna_total_label[$dia] = utils::format($columna_total[$dia]);
                                    $reglones[$dia] = $monto;
                                    $reglones_total[$num_semana] = $columna_total_totales[$num_semana];
                                    
                                    $columna[] = array('data' => utils::format($monto), 'class' => 'saldo_inicial total_saldo_inicial' ,'fecha'=>$value_dias);
                                    $monto_total =  $monto_total + $monto;
                                    $total_monto_flujo = $total_monto_flujo + $columna_total[$dia];
                                    // utils::pre($total_monto_flujo,false);
                                }
                            }
                            $columna[] = array('data' => utils::format($monto_total), 'class' => 'table-primary');
                            $columna_total_label[$num_semana] = array('data' => utils::format( $columna_total_totales[$num_semana]), 'class' => 'table-primary');
                            if($columna_header_band == false){
                                $columna_header[] = array('data' => 'TOTAL SEMANA', 'class' => 'table-primary');
                            }    
                        }

                        
                        
                        // utils::pre($reglones,false);
    
                        $columna[] = array('data' => utils::format(array_sum($reglones)), 'class' => 'table-secondary');
                        $reglones = [];
                        $this->table->add_row($columna);

                        // foreach ($reglones as $key_origen => $value_origen) {

                        // }
                        if($columna_header_band == false){

                            $columna_header[] = array('data' => 'TOTAL MES', 'class' => 'table-secondary');

                            $this->table->set_heading($columna_header);
                            $this->table->set_heading($columna_header);
                            $columna_header_band = true;
                        }

                        
                    

            // if(is_array($dias) && count($dias)>0){
            //     $set_row = array(
            //         'TOTAL'
            //     );
            //     $set_row_total = 0;

            //     foreach ($dias as $key_dias => $value_dias) {

            //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
            //             # 'cuenta' => $cuenta_id,
            //             'asientos.fecha_creacion' => $value_dias
            //         ]);
            //         $monto = 0;
            //         if(is_array($importe)){
            //             $monto = (array_key_exists('cargo',$importe) && array_key_exists('abono',$importe))? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
            //         }
            //         $set_row_total = $set_row_total + $monto;
            //         $set_row[] = utils::formatMoney($monto,2);
            //     }
            //     $set_row[] = utils::formatMoney($set_row_total,2);
            //     $this->table->add_row($set_row);
            // }
            //utils::pre($this->table->generate());
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }


    public function guardar_formulario_reglon(){ 
        
        $response = array();
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('banco_reglon', 'Banco', 'trim|required');
        $this->form_validation->set_rules('fecha_reglon', 'Fecha', 'trim|required');
        $this->form_validation->set_rules('monto_reglon', 'Monto', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $identificador_reglon = $this->input->post('identificador_reglon');
            $fecha_reglon = $this->input->post('fecha_reglon');
            $monto_reglon = $this->input->post('monto_reglon');

            $this->load->model('DeTiposIngresosLibres_model');
            $datos = $this->DeTiposIngresosLibres_model->get([
                'id_tipo_ingreso_libre' => $identificador_reglon,
                'fecha' => $fecha_reglon
            ]);

            if(is_array($datos) && count($datos) > 0){
                $this->DeTiposIngresosLibres_model->update([
                    'monto' => $monto_reglon
                ],[
                    'id_tipo_ingreso_libre' => $identificador_reglon,
                    'fecha' => $fecha_reglon
                ]);
            }else{
                $this->DeTiposIngresosLibres_model->insert([
                    'monto' => $monto_reglon,
                    'id_tipo_ingreso_libre' => $identificador_reglon,
                    'fecha' => $fecha_reglon
                ]);
            }
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

}
