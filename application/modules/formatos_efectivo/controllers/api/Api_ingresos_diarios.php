<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_ingresos_diarios extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ingresos_diarios(){
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

            $this->load->model('DeFormatoFlujoEfectivo');
            $this->db->where('de_formato_flujo_efectivo.abono > 0', null,false );
            $ingresos = $this->DeFormatoFlujoEfectivo->getAll(array(
                'ca_formato_flujo_efectivo.fecha' => $fecha
            ));
            // utils::pre($this->db->last_query());
            //$egresos = $this->Asientos_model->get_listado_egresos();

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','CONCEPTO','FECHA DE VALIDACION','VENDEDOR','MONTO'
            ));
            
            $monto_total = 0;
            if(is_array($ingresos) && count($ingresos)>0){
                foreach ($ingresos as $value_ingreso) {
                    $this->table->add_row([
                        $value_ingreso['fecha'],
                        $value_ingreso['concepto'],
                        utils::aFecha($value_ingreso['fecha_de_validacion'],true),
                        $value_ingreso['vendedor'],
                        utils::format($value_ingreso['abono'])
                    ]);
                    $monto_total = $monto_total + $value_ingreso['abono'];
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 5);
                $this->table->add_row($cell);
            }

            $cell = array('data' => 'TOTAL', 'colspan' => 4);
            $this->table->add_row($cell, utils::format($monto_total));

            
            $this->contenido->data = array(
                'tabla' => base64_encode( utf8_decode($this->table->generate()) )
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
    

    public function egresos_diarios(){
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

           
            $this->load->model('DeFormatoFlujoEfectivo');
            $this->db->where('de_formato_flujo_efectivo.cargo > 0', null,false );
            $egresos = $this->DeFormatoFlujoEfectivo->getAll(array(
                'ca_formato_flujo_efectivo.fecha' => $fecha
            ));

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','CONCEPTO','FECHA DE VALIDACION','MONTO'
            ));
            
            $monto_total = 0;
            if(is_array($egresos) && count($egresos)>0){
                foreach ($egresos as $value_egreso) {
                    $this->table->add_row([
                        $value_egreso['fecha'],
                        $value_egreso['concepto'],
                        utils::aFecha($value_egreso['fecha_de_validacion'],true),
                        utils::format($value_egreso['cargo'])
                    ]);
                    $monto_total = $monto_total + $value_egreso['cargo'];
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 4);
                $this->table->add_row($cell);
            }

            $cell = array('data' => 'TOTAL', 'colspan' => 3);
            $this->table->add_row($cell, utils::formatMoney($monto_total,2));

            
            $this->contenido->data = array(
                'tabla' => base64_encode( utf8_decode($this->table->generate()) )
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
}
