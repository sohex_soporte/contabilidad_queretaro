<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Api_flujo_efectivo extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        exit();
    }

    public function buscar_cuenta($importe_list, $cuenta = false, $origen = false)
    {
        $response = 0;
        if (is_array($importe_list)) {
            foreach ($importe_list as $key => $value) {
                if ($value['cuenta'] == $cuenta && $value['procedencia_id'] == $origen) {
                    $response = $value['monto'];
                    break;
                }
            }
        }
        return $response;
    }

    public function primer_reporte()
    {

        $response = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required|valid_date');

        if ($this->form_validation->run($this) != FALSE) {

            $this->load->model('Asientos_model');
            $this->load->model('CaCuentas_model');
            $this->load->model('CaProcedencias_model');

            $fecha = $this->input->post('fecha');

            $this->db->where_in('cuentas_dms.id', $this->CaCuentas_model::CTA_BANCARIAS);
            $listado_cuentas = $this->CaCuentas_model->getAll();
            $listado_origenes = $this->CaProcedencias_model->getAll();

            $this->db->where_in('asientos.cuenta', $this->CaCuentas_model::CTA_BANCARIAS);
            $this->db->where_in('asientos.estatus_id', [
                $this->Asientos_model::ESTATUS_POR_APLICAR,
                $this->Asientos_model::ESTATUS_APLICADO
            ]);
            $importe_list = $this->Asientos_model->get_total_por_cuenta_bancaria([
                'asientos.fecha_creacion' => $fecha
            ]);

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >'
            );
            $this->table->set_template($template);

            $set_heading = array('Cuenta', 'Descripcion');
            if (is_array($listado_origenes) && count($listado_origenes) > 0) {
                foreach ($listado_origenes as $key_origen => $origen) {
                    $set_heading[] = $origen['nombre'];
                }
            }
            $set_heading[] = 'Total ingreso';
            $this->table->set_heading($set_heading);



            if (is_array($listado_cuentas) && count($listado_cuentas) > 0) {
                foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                    if (is_array($listado_origenes) && count($listado_origenes) > 0) {
                        $set_row = array(
                            $cuenta['cuenta'],
                            $cuenta['decripcion']
                        );
                        $set_row_total = 0;

                        foreach ($listado_origenes as $key_origen => $origen) {
                            $origen_transaccion_id = $origen['id'];
                            $cuenta_id = $cuenta['id'];

                            $monto = $this->buscar_cuenta(
                                $importe_list,
                                $cuenta_id,
                                $origen_transaccion_id
                            );

                            $set_row_total = $set_row_total + $monto;
                            $set_row[] = utils::format($monto);
                        }
                        $set_row[] = utils::format($set_row_total);
                        $this->table->add_row($set_row);
                    }
                }

                if (is_array($listado_origenes) && count($listado_origenes) > 0) {
                    $set_row = array(
                        '',
                        'TOTALES'
                    );
                    $set_row_total = 0;

                    foreach ($listado_origenes as $key_origen => $origen) {
                        $origen_transaccion_id = $origen['id'];

                        $this->db->where_in('asientos.cuenta', $this->CaCuentas_model::CTA_BANCARIAS);
                        $this->db->where_in('asientos.estatus_id', [
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            $this->Asientos_model::ESTATUS_APLICADO
                        ]);
                        $importe = $this->Asientos_model->get_total_cuentas_ingreso([
                            'asientos.procedencia_id' => $origen_transaccion_id,
                            'asientos.fecha_creacion' => $fecha
                        ]);
                        $monto = 0;
                        if (is_array($importe)) {
                            $monto = (array_key_exists('cargo', $importe) && array_key_exists('abono', $importe)) ? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
                        }
                        $set_row_total = $set_row_total + $monto;
                        $set_row[] = utils::format($monto);
                    }
                    $set_row[] = utils::format($set_row_total);
                    $this->table->add_row($set_row);
                }
            }

            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate()),
                'num_cuentas' => is_array($listado_cuentas) ? count($listado_cuentas) : 0
            );
        } else {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

    public function segundo_reporte()
    {
        $response = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required|valid_date');

        if ($this->form_validation->run($this) != FALSE) {

            $this->load->model('Asientos_model');
            $this->load->model('CaCuentas_model');
            $this->load->model('CaProcedencias_model');

            $fecha = $this->input->post('fecha');

            $this->db->where_in('cuentas_dms.id', $this->CaCuentas_model::CTA_BANCARIAS);
            $listado_cuentas = $this->CaCuentas_model->getAll();

            $listado_origenes = $this->CaProcedencias_model->getAll();

            $this->db->where_in('asientos.cuenta', $this->CaCuentas_model::CTA_BANCARIAS);
            $this->db->where_in('asientos.estatus_id', [
                $this->Asientos_model::ESTATUS_POR_APLICAR,
                $this->Asientos_model::ESTATUS_APLICADO
            ]);
            $importe_list = $this->Asientos_model->get_total_por_cuenta_bancaria([
                'ca_transacciones.fecha' => $fecha
            ]);
            // utils::pre($this->db->last_query());

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped table-sm" >'
            );
            $this->table->set_template($template);

            $set_heading = array('Fecha', 'Descripcion');
            if (is_array($listado_cuentas) && count($listado_cuentas) > 0) {
                foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                    $set_heading[] = $cuenta['decripcion'];
                }
            }
            $set_heading[] = 'Total ingreso';
            $this->table->set_heading($set_heading);

            $set_row_cont = array();
            $set_row_label = array(
                '',
                'TOTALES'
            );

            if (is_array($listado_origenes) && count($listado_origenes) > 0) {
                foreach ($listado_origenes as $key_origen => $origen) {

                    if (is_array($listado_cuentas) && count($listado_cuentas) > 0) {
                        $set_row = array(
                            utils::get_dia_mes_format($fecha),
                            $origen['nombre']
                        );
                        $set_row_total = 0;

                        

                        foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                            $origen_transaccion_id = $origen['id'];
                            $cuenta_id = $cuenta['id'];

                            $monto = $this->buscar_cuenta(
                                $importe_list,
                                $cuenta_id,
                                $origen_transaccion_id
                            );

                            $set_row_total = $set_row_total + $monto;
                            $set_row[] = utils::format($monto);

                            if(array_key_exists($cuenta_id, $set_row_cont)){
                                $set_row_cont[$cuenta_id] = $set_row_cont[$cuenta_id] + (float)$monto;
                            }else{
                                $set_row_cont[$cuenta_id] = (float)$monto;
                            }
                            $set_row_label[$cuenta_id] = utils::format( $set_row_cont[$cuenta_id] );
                        }
                        $set_row[] = utils::format($set_row_total);
                        $this->table->add_row($set_row);
                    }
                }
                // utils::pre($set_row_cont);
                $set_row_label[] = utils::format( array_sum($set_row_cont) );
                $this->table->add_row($set_row_label);

                // if (is_array($listado_cuentas) && count($listado_cuentas) > 0) {
                //     $set_row = array();
                //     $set_row_label = array(
                //         '',
                //         'TOTALES'
                //     );

                //     $set_row_total = 0;

                //     foreach ($listado_cuentas as $key_cuenta => $cuenta) {
                //         $cuenta_id = $cuenta['id'];

                //         $this->db->where_in('asientos.cuenta', $this->CaCuentas_model::CTA_BANCARIAS);
                //         $this->db->where_in('asientos.estatus_id', [
                //             $this->Asientos_model::ESTATUS_POR_APLICAR,
                //             $this->Asientos_model::ESTATUS_APLICADO
                //         ]);
                //         $importe = $this->Asientos_model->get_total_cuentas_ingreso([
                //             'asientos.procedencia_id' => $origen_transaccion_id,
                //             'asientos.fecha_creacion' => $fecha
                //         ]);

                //         $monto = 0;
                //         if (is_array($importe)) {
                //             $monto = (array_key_exists('cargo', $importe) && array_key_exists('abono', $importe)) ? ((float)$importe['abono'] - (float)$importe['cargo']) : 0;
                //         }
                //         $set_row_total = $set_row_total + $monto;
                //         $set_row[$origen_transaccion_id] = $set_row[$origen_transaccion_id] + $monto;
                //         $set_row_label[$origen_transaccion_id] = utils::format( $set_row[$origen_transaccion_id] );
                //     }
                //     $set_row[] = utils::format( array_sum($set_row) );
                //     $this->table->add_row($set_row);
                // }
            }

            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate()),
                'num_cuentas' => is_array($listado_cuentas) ? count($listado_cuentas) : 0
            );
        } else {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

    public function save_documento()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_banco', 'Tipo banco', 'trim|required');

        $data['validation'] = false;
        $data['estatus'] = 'ok';

        if ($this->form_validation->run() == true) {
            if (!is_array($_FILES["documento"]["error"]) && $_FILES["documento"]["error"] == 0) {
                $fileTmpPath = $_FILES['documento']['tmp_name'];
                $fileName = $_FILES['documento']['name'];
                $fileNameCmps = explode(".", $fileName);
                $fileExtension = strtolower(end($fileNameCmps));
                $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

                $allowedfileExtensions = array('xls', 'xlsx');
                if (in_array($fileExtension, $allowedfileExtensions)) {
                    $uploadFileDir = FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'documentos_excel'.DIRECTORY_SEPARATOR;

                    $dest_path = $uploadFileDir . $newFileName;

                    $banco_id = (string)$this->input->post('tipo_banco');
                    $proceso = false;

                    if (move_uploaded_file($fileTmpPath, $dest_path)) {
                        switch ($banco_id) {
                            case 1:
                            case '1': //BBVA
                                $proceso = $this->procesar_formato_bbva($dest_path);
                                break;

                            case 2:
                            case '2': //BANAMEX
                                $proceso = $this->procesar_formato_banamex($dest_path);
                                break;
                            
                            case 3:
                            case '3': //BANORTE
                                $proceso = $this->procesar_formato_banorte($dest_path);
                                break;

                            case 4:
                            case '4': //HSBC
                                $proceso = $this->procesar_formato_hsbc($dest_path);
                                break;
                            
                            case 5:
                            case '5': //SCOTIANBANK
                                $proceso = $this->procesar_formato_scotianbank($dest_path);
                                break;

                            default:
                                return false;
                                break;
                        }

                        if ($proceso) {

                            if(is_array($proceso) && count($proceso)>0 ) {

                                $this->load->model('CaFormatoFlujoEfectivo');
                                $formato_flujo_efectivo_id = $this->CaFormatoFlujoEfectivo->insert([
                                    'banco_id'=> $banco_id,
                                    'fecha' => utils::get_date()
                                ]);

                                $this->load->model('DeFormatoFlujoEfectivo');
                                foreach ($proceso as $key => $value) {
                                    if( strlen(trim($value['concepto']))>0 ){
                                        $this->DeFormatoFlujoEfectivo->insert([
                                            'formato_flujo_efectivo_id' => $formato_flujo_efectivo_id,
                                            'orden' => $value['orden'],
                                            'fecha' => $value['fecha'],
                                            'concepto' => $value['concepto'],
                                            'cargo' => $value['debe'],
                                            'abono' => $value['haber'],
                                            'saldo' => $value['saldo'],
                                            'reglon' => $value['reglon']
                                        ]);
                                    }
                                }
                            }
                            
                            $data['respuesta'] = $proceso;
                            $data['mensaje'] = 'Excel procesado correctamente';
                        } else {
                            $data['estatus'] = 'error';
                            $data['mensaje'] = 'Surgio un error al procesar el formato';
                        }
                    } else {
                        $message = 'Hubo algún error al mover el archivo al directorio de carga. Asegúrese de que el servidor web pueda escribir en el directorio de carga';
                        $data['estatus'] = 'error';
                        $data['mensaje'] = $message;
                    }
                } else {
                    $data['estatus'] = 'error';
                    $data['mensaje'] = 'Subida fallida. Extensiones permitidas : ' . implode(',', $allowedfileExtensions);
                }
            } else {
                $message = 'Falta adjuntar el archivo';
                $data['estatus'] = 'error';
                $data['mensaje'] = $message;
            }
        } else {
            $data['estatus'] = 'error';
            $data['validation'] = true;
            $data['info'] = $this->form_validation->error_array();
        }
        print_r(json_encode($data));
        exit();
    }

    public function procesar_formato_bbva($dest_path)
    {
        $documento = IOFactory::load($dest_path);
        $sheet = $documento->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        // $columnas = $sheet->getColumnDimensions();
        // if (count($columnas) != 5) {
        //     return false;
        // }

        $row_inicial = 4;
        $data = [];
        $orden = 1;
        for ($row = $row_inicial; $row <= $highestRow; $row++) {
            $registro = [
                'fecha' => $sheet->getCell("B" . $row)->getValue(),
                'concepto' => $sheet->getCell("C" . $row)->getValue(),
                'debe' => utils::saveMoney($sheet->getCell("D" . $row)->getValue()),
                'haber' => utils::saveMoney($sheet->getCell("E" . $row)->getValue()),
                'saldo' => utils::saveMoney($sheet->getCell("F" . $row)->getValue())
            ];
            $registro['reglon'] = json_encode($registro);
            $registro['orden'] = $orden;
            $data[] = $registro;
            $orden++;
        }
        return $data;
    }

    public function procesar_formato_banamex($dest_path)
    {
        $documento = IOFactory::load($dest_path);
        $sheet = $documento->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $row_inicial = 4;
        $data = [];
        $orden = 1;
        for ($row = $row_inicial; $row <= $highestRow; $row++) {
            $registro = [
                'fecha' => $sheet->getCell("B" . $row)->getValue(),
                'concepto' => $sheet->getCell("C" . $row)->getValue(),
                'debe' => utils::saveMoney($sheet->getCell("D" . $row)->getValue()),
                'haber' => utils::saveMoney($sheet->getCell("E" . $row)->getValue()),
                'saldo' => utils::saveMoney($sheet->getCell("F" . $row)->getValue())
            ];
            $registro['reglon'] = json_encode($registro);
            $registro['orden'] = $orden;
            $data[] = $registro;
            $orden++;
        }
        return $data;
    }


    public function procesar_formato_banorte($dest_path)
    {
        $documento = IOFactory::load($dest_path);
        $sheet = $documento->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        // $columnas = $sheet->getColumnDimensions();
       
        $row_inicial = 4;
        $orden = 1;
        $data = [];
        $registro_ext = [];

        for ($row = $row_inicial; $row <= $highestRow; $row++) {
            
            $registro = [
                'fecha' => $sheet->getCell("D" . $row)->getValue(),
                'concepto' => $sheet->getCell("F" . $row)->getValue(),
                'debe' => utils::saveMoney($sheet->getCell("J" . $row)->getValue()),
                'haber' => utils::saveMoney($sheet->getCell("I" . $row)->getValue()),
                'saldo' => utils::saveMoney($sheet->getCell("K" . $row)->getValue())
            ];
            
            $registro_ext['cuenta'] = $sheet->getCell("B" . $row)->getValue();
            $registro_ext['fecha_operacion'] = $sheet->getCell("C" . $row)->getValue();
            $registro_ext['referencia'] = $sheet->getCell("E" . $row)->getValue();
            $registro_ext['cod_trans'] = $sheet->getCell("G" . $row)->getValue();
            $registro_ext['sucursal'] = $sheet->getCell("H" . $row)->getValue();
            $registro_ext['movimiento'] = $sheet->getCell("L" . $row)->getValue();
            $registro_ext['descripcion_detallada'] = $sheet->getCell("M" . $row)->getValue();
            $registro_ext['cheque'] = $sheet->getCell("N" . $row)->getValue();

            $registro['reglon'] = json_encode(array_merge($registro,$registro_ext));
            $registro['orden'] = $orden;
            $data[] = $registro;
            $orden++;
        }
        return $data;
    }

    public function procesar_formato_hsbc($dest_path)
    {
        $documento = IOFactory::load($dest_path);
        $sheet = $documento->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $columnas = $sheet->getColumnDimensions();
        if (count($columnas) != 8) {
            return false;
        }
        $row_inicial = 4;
        $orden = 1;
        $data = [];
        $registro_ext = [];

        for ($row = $row_inicial; $row <= $highestRow; $row++) {

            $registro = [
                'fecha' => $sheet->getCell("I" . $row)->getValue(),
                'concepto' => $sheet->getCell("C" . $row)->getValue(),
                'debe' => utils::saveMoney($sheet->getCell("G" . $row)->getValue()),
                'haber' => utils::saveMoney($sheet->getCell("F" . $row)->getValue()),
                'saldo' => utils::saveMoney($sheet->getCell("H" . $row)->getValue())
            ];

            $registro_ext = [
                'referencia_banco' => $sheet->getCell("B" . $row)->getValue(),
                'referencia_cliente' => $sheet->getCell("D" . $row)->getValue(),
                'tipo_trn' => $sheet->getCell("E" . $row)->getValue(),
            ];
            
            $registro['reglon'] = json_encode(array_merge($registro,$registro_ext));
            $registro['orden'] = $orden;
            $data[] = $registro;
            $orden++;
        }
        return $data;
    }

    public function procesar_formato_scotianbank($dest_path)
    {
        $documento = IOFactory::load($dest_path);
        $sheet = $documento->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        
        $row_inicial = 4;
        $orden = 1;
        $data = [];
        $registro_ext = [];

        for ($row = $row_inicial; $row <= $highestRow; $row++) {

            $importe = utils::saveMoney($sheet->getCell("E" . $row)->getValue());
            $tipo = trim($sheet->getCell("F" . $row)->getValue());

            $registro = [
                'fecha' => $sheet->getCell("C" . $row)->getValue(),
                'concepto' => $sheet->getCell("G" . $row)->getValue(),
                'debe' => (strtoupper($tipo) == 'CARGO')? $importe : null,
                'haber' => (strtoupper($tipo) != 'CARGO')? $importe : null,
                'saldo' => utils::saveMoney($sheet->getCell("H" . $row)->getValue())
            ];

            $registro_ext = [
                'numero' => $sheet->getCell("B" . $row)->getValue(),
                'referencia_numerica' => $sheet->getCell("D" . $row)->getValue(),
                'importe' => $sheet->getCell("E" . $row)->getValue(),
                'tipo' => $sheet->getCell("F" . $row)->getValue(),
                'leyenda1' => $sheet->getCell("I" . $row)->getValue(),
                'leyenda2' => $sheet->getCell("J" . $row)->getValue(),
            ];
            
            $registro['reglon'] = json_encode(array_merge($registro,$registro_ext));
            $registro['orden'] = $orden;
            $data[] = $registro;
            $orden++;

        }
        return $data;
    }
 
    public function tercer_reporte(){

        $fecha = $this->input->post('fecha');

        $this->load->library('table');
        $this->table->set_template(array(
            'table_open' => '<table class="table table-bordered table-striped" >'
        ));

    

        $cell_1 = array('data' => '<center>'.utils::aFecha($fecha).'</center>', 'colspan' => 4,'class'=>'table-secondary');
        $this->table->add_row($cell_1);
        $this->table->add_row('','BANCOS REAL','CAJA','DIFERENCIA');

        $this->load->model('CaBancos_model');
        $bancos = $this->CaBancos_model->getAll();
        $total_general = 0;
        $total_general2 = 0;
        $total_general3 = 0;

        $this->load->model('DeFormatoFlujoEfectivo');
        $this->load->model('Asientos_model');
        foreach ($bancos as $key => $value) {

            $final = $this->DeFormatoFlujoEfectivo->getLast(array(
                'ca_formato_flujo_efectivo.fecha' => $fecha,
                'ca_formato_flujo_efectivo.banco_id' => $value['id']
            ));

            $final_totales = $this->Asientos_model->getTotal([
                'fecha' => $fecha,
                'cuenta_id' => $value['cuenta_id']
            ]);

            $cantidad = 0;
            if(is_array($final) && count($final)>0){
                $cantidad = $final['saldo'];
                $total_general = $total_general + $cantidad;
            }

            $cantidad2 = 0;
            if(is_array($final_totales) && count($final_totales)>0){
                $cantidad2 = $final_totales['saldo'];
                $total_general2 = $total_general2 + $cantidad2;
            }

            $diferencia = $cantidad - $cantidad2;
            $total_general3 = $total_general3 + $diferencia;

            $cell_4 = array('data' => utils::format($cantidad), 'class'=>'table-active', 'style'=>'text-align:right');
            $cell_5 = array('data' => utils::format($cantidad2), 'class'=>'table-active', 'style'=>'text-align:right');
            $cell_6 = array('data' => utils::format($diferencia), 'class'=>'table-active', 'style'=>'text-align:right');

            $this->table->add_row($value['nombre'],$cell_4,$cell_5,$cell_6);
        }

        $cell_4 = array('data' => utils::format($total_general), 'class'=>'table-active', 'style'=>'text-align:right');
        $cell_5 = array('data' => utils::format($total_general2), 'class'=>'table-active', 'style'=>'text-align:right');
        $cell_6 = array('data' => utils::format($total_general3), 'class'=>'table-active', 'style'=>'text-align:right');

        $this->table->add_row('Total',$cell_4,$cell_5,$cell_6);
        $tabla2 = base64_encode( utf8_decode($this->table->generate()) );


        $this->contenido->data = array(
            'totales' => $tabla2
        ); 
        $this->response();
    }

    public function update_deformato()
    {
        $this->load->model('DeFormatoFlujoEfectivo');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cliente', 'Cliente', 'trim|required');
        // $this->form_validation->set_rules('estatus', 'Estatus', 'trim|required');
        $this->form_validation->set_rules('departamento', 'Departamento', 'trim|required');
        $this->form_validation->set_rules('sucursal', 'Sucursal', 'trim|required');
        $this->form_validation->set_rules('identificador', 'Identificador', 'trim|required');
        $this->form_validation->set_rules('id_vendedor', 'Vendedor', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {
            $contents = [
                'persona_id' => $this->input->post('cliente'),
                'procedencia_id' => $this->input->post('departamento'),
                'sucursal_id' => $this->input->post('sucursal'),
                'estatus' => (($this->input->post('id_vendedor') > 0)? 1 : 2),
                'id_vendedor' => $this->input->post('id_vendedor'),
                'vendedor' => $this->input->post('vendedor')
            ];
            $this->DeFormatoFlujoEfectivo->update($contents,['id' => $this->input->post('identificador')]);
            $this->contenido->error = false;
            $this->contenido->mensaje = 'Anexos actualizados correctamente';
        } else {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

}    
