<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Flujo_bancos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
    }

    public function index(){ 
        // $this->load->helper(array('form', 'html', 'validation', 'url'));

        $this->blade->render('flujo_bancos/index');
    }
    
}
