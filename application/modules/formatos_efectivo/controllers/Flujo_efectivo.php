<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Flujo_efectivo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            'listado' => ''
        ];
        $this->blade->render('/flujo_efectivo/index', $data);
    }

    public function procesar()
    {
        $this->carabiner->display('sweetalert2');
        $this->load->model('CaBancos_model');
        $data = [
            'bancos' => $this->CaBancos_model->getAll(),
            'listado' => ''
        ];
        $this->blade->render('flujo_efectivo/procesa_formato', $data);
    }
}
