@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Administrador de contenido
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">Contenido del archivo</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <form>
                            <div class="form-group row">
                                <label for="fecha" class="col-sm-2 col-form-label">Fecha</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly disabled="disabled" class="form-control" id="fecha" value="<?php echo utils::aFecha($contenido['fecha'], true); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fecha" class="col-sm-2 col-form-label">Banco</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly disabled class="form-control" id="fecha" value="<?php echo $contenido['banco_nombre']; ?>">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr />
                        <br />
                    </div>
                </div>

                <div class="row">
                    <div class=" col-sm-12">

                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table> -->
                        <div class="table-responsive">
                            <?php echo $tabla; ?>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-sm-12">
                        <a class="btn btn-secondary" href="<?php echo site_url('formatos_efectivo/administrador_contenido') ?>"><i class="fas fa-chevron-left"></i> Regresar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_anexo" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Anexo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_anexo">
                    <div class="form-group">
                        <label>* Cliente / Proveedor</label>
                        <select name="cliente" id="cliente" class="form-control text-uppercase">
                            <?php foreach ($personas as $persona) {
                                $nombre_persona = $persona['tipo_persona_id'] == 2 ? $persona['nombre'] :  $persona['nombre'] . ' ' . $persona['apellido1'] . ' ' . $persona['apellido2']; ?>
                                <option value="<?php echo $persona['persona_id'] ?>"> <?php echo $persona['tipo_persona'] . ' - ' .  $nombre_persona ?> </option>
                            <?php } ?>
                        </select>
                        <small id="msg_cliente" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>* Sucursal</label>
                        <select name="sucursal" id="sucursal" class="form-control text-uppercase">
                            <?php foreach ($sucursales as $sucursal) { ?>
                                <option value="<?php echo $sucursal['id'] ?>"> <?php echo mb_strtoupper($sucursal['nombre'],'UTF-8'); ?> </option>
                            <?php } ?>
                        </select>
                        <small id="msg_sucursal" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>* Departamento</label>
                        <select name="departamento" id="departamento" class="form-control text-uppercase">
                            <?php foreach ($procedencias as $procedencia) { ?>
                                <option value="<?php echo $procedencia['id'] ?>"> <?php echo mb_strtoupper($procedencia['nombre'],'UTF-8'); ?> </option>
                            <?php } ?>
                        </select>
                        <small id="msg_departamento" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label>* Vendedor</label>
                        <select name="id_vendedor" id="id_vendedor" class="form-control text-uppercase" onchange="actualizar_vendedor_nombre();">
                            <?php if(is_array($listado_vendores) && count($listado_vendores)>0){?>
                                <?php foreach ($listado_vendores as $vendor) { ?>
                                    <option value="<?php echo $vendor['id'] ?>"> <?php echo mb_strtoupper(trim($vendor['nombre'].' '.$vendor['apellido_paterno'].' '.$vendor['apellido_materno']),'UTF-8'); ?> </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <small id="msg_id_vendedor" class="form-text text-danger"></small>
                    </div>
                    <input type="hidden" name="identificador" id="identificador" value="" />
                    <input type="hidden" name="vendedor" id="vendedor" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="guardar_anexo()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_pdf" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection




@section('style')
<?php $this->carabiner->display('datatables', 'css') ?>
<?php $this->carabiner->display('select2', 'css') ?>
<style>
    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<?php $this->carabiner->display('datatables', 'js') ?>
<?php $this->carabiner->display('select2', 'js') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/cheques/index.js'); ?>"></script>
<script>

    $(function(){

        $('#modal_anexo').on('shown.bs.modal', function () {
            if($('#id_vendedor').hasClass("select2-hidden-accessible") == false){
                $('select#cliente').select2({
                    placeholder: 'Seleccione una opción',
                    theme: 'bootstrap4',
                    dropdownParent: $('div#modal_anexo')
                });

                $('select#sucursal').select2({
                    placeholder: 'Seleccione una opción',
                    theme: 'bootstrap4',
                    dropdownParent: $('div#modal_anexo')
                });

                $('select#departamento').select2({
                    placeholder: 'Seleccione una opción',
                    theme: 'bootstrap4',
                    dropdownParent: $('div#modal_anexo')
                });

                $('select#id_vendedor').select2({
                    placeholder: 'Seleccione una opción',
                    theme: 'bootstrap4',
                    dropdownParent: $('div#modal_anexo')
                });

            }

            $("#cliente").val('');
            $("#identificador").val('');
            $("#departamento").val('');
            // $("#vendedor").val('');
            
            $('select#cliente').val(null).trigger('change');
            $('select#sucursal').val(null).trigger('change');
            $('select#departamento').val(null).trigger('change');
            $('select#id_vendedor').val(null).trigger('change');

        })

    });

    function actualizar_vendedor_nombre(){
        setTimeout(() => {
            var vendedor = $('select#id_vendedor option:selected').text();
            $("input#vendedor").val( vendedor );
        }, 200);
    }

    function open_modal(_this) {
       
        $("#modal_anexo").modal('show');
        setTimeout(() => {
            try {
                $("#cliente").val($(_this).data('persona_id')).trigger('change');
                $("#sucursal").val($(_this).data('sucursal')).trigger('change');
                $("#departamento").val($(_this).data('departamento')).trigger('change');
                $("#identificador").val($(_this).data('id')).trigger('change');
                $("#id_vendedor").val($(_this).data('vendedor')).trigger('change');
            } catch (error) {}    
        }, 500);
        

    }

    function guardar_anexo() {
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_flujo_efectivo/update_deformato',
            data: $('form#form_anexo').serializeArray(),
            dataType: "json",
            traditional: true,
            beforeSend: function() {
            },
            success: function(data) {
                if (data.error == true) {
                    $.each(data.mensaje, function(index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: data.mensaje,
                    }).then(function() {
                        $("#modal_anexo").modal('hide');
                        window.location.reload();
                    });
                }

            }
        });
    }
</script>
@endsection