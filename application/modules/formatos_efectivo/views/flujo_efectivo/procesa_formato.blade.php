@layout('template_blade/estructura')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Adjuntar Excel
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card  mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form name="formExcel" id="formExcel" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>* Tipo banco</label>
                                            <select name="tipo_banco" id="tipo_banco" class="form-control mr-sm-2">
                                                <option value="">Seleccionar opción</option>
                                                 @foreach($bancos as $banco) 
                                                    <option value="{{ $banco['id']}}">{{ $banco['nombre']}}</option>
                                                @endforeach  
                                            </select>
                                            <small id="msg_tipo_banco" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>*Subir archivo</label>
                                            <div class="custom-file">
                                                <input type="file" id="documento" accept=".xls, .xlsx" name="documento" class="custom-file-input form-control">
                                                <label class="custom-file-label"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <button type="submit" id="subirFile" class="btn btn-primary mt-4 float-right col-12"><i class="fa fa-cog"></i>&nbsp;Procesar</button>
                                    </div>
                                </div>
                            </form>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr/>
                    </div>
                    <div class="col-sm-12">
                        <b class="mb-5">Formatos rellenables bancos</b>
                        <p><i class="fa fa-info-circle text-primary fa-2x"></i> Descargar el formato correspondiente a su elección. Es importante respetar el número de columnas y digitar los datos en la columna correspondiente </p>

                        <div class="row mt-5 justify-content-center">
                            <div class="col-sm-2">
                                <a href="{{ base_url('assets/ejemplos/formato_bbva.xlsx') }}" target="_blank"  class="col-md-2  text-center">
                                    <center><i class="fa fa-file-excel fa-4x text-success" title="Descargar formato"></i><br/><b style="color:#323232 !important">FORMATO BBVA</b></center>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ base_url('assets/ejemplos/formato_banamex.xlsx') }}" target="_blank" class="col-md-2  text-center">
                                    <center><i class="fa fa-file-excel fa-4x text-success" title="Descargar formato"></i> <br /> <b style="color:#323232 !important">FORMATO BANAMEX</b></center>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ base_url('assets/ejemplos/formato_banorte.xlsx') }}" target="_blank" class="col-md-2  text-center">
                                    <center><i class="fa fa-file-excel fa-4x text-success" title="Descargar formato"></i> <br /> <b style="color:#323232 !important">FORMATO BANORTE</b></center>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ base_url('assets/ejemplos/formato_hsbc.xlsx') }}" target="_blank" class="col-md-2  text-center">
                                    <center><i class="fa fa-file-excel fa-4x text-success" title="Descargar formato"></i> <br /> <b style="color:#323232 !important">FORMATO HSBC</b></center>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ base_url('assets/ejemplos/formato_scotiabank.xlsx') }}" target="_blank" class="col-md-2  text-center">
                                    <center><i class="fa fa-file-excel fa-4x text-success" title="Descargar formato"></i> <br /> <b style="color:#323232 !important">FORMATO SCOTIABANK</b></center>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-sm-12">
                       <a class="btn btn-secondary" href="<?php echo site_url('formatos_efectivo/administrador_contenido') ?>"><i class="fas fa-chevron-left"></i> Regresar</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script type="text/javascript">
    $('.custom-file-input').on('change', function() {
        var fileName = document.getElementById("documento").files[0].name;
        $('.custom-file-label').addClass("selected").html(fileName);
    })

    $("form#formExcel").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById("formExcel"));
        $.ajax({
            url: PATH + '/formatos_efectivo/api/api_flujo_efectivo/save_documento',
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result, status, xhr) {
                $('.custom-file-label').removeClass("selected").html('');
                if (result.estatus == 'error') {
                    if (!result.validation) {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: result.mensaje,
                        })
                    } else {
                        $.each(result.info, function(index, value) {
                            console.log(index);
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }
                        });
                    }
                } else {

                    Swal.fire({
                        type: 'Success',
                        text: result.mensaje,
                    }).then(function() {
                        window.location.href = PATH+"/formatos_efectivo/administrador_contenido";
                    });

                }
            }
        })
    });

    
</script>
@endsection