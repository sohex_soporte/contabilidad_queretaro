@layout('template_blade/estructura')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    INGRESOS
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Fecha: </b>
                                </span>
                                <select value="<?php echo date('m'); ?>" name="mes"
                                    class="form-control mr-sm-2"></select>
                                <input class="form-control mr-sm-2" type="number" name="anio" placeholder=""
                                    aria-label="" value="<?php echo date('Y'); ?>" min="2020"
                                    max="<?php echo date('Y'); ?>">
                                <small id="msg_fecha" class="form-text text-danger"></small>
                                <button class="btn btn-outline-success ml-2 my-2 my-sm-0" onclick="buscar();"
                                    type="button">Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-sm-12">
                        <div class="table-responsive" id="tercero_reporte">
                            <table id="contenedor_reporte" class="table table-bordered table-sm table-hover"></table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script id="tmp_calculos_semana" type="text/template">
    [{#contenido}]
<div class="row mt-4">
    <div class="col-sm-12">
        <div class="card" style="width: 100%;">
            <div class="card-body">
                <div id="fecha_semana_num_[{semana}]" class="col-sm-12">
                    <h5 class="card-title">Semana<br/>[{titulo}]</h5>
                    <div class="mt-5 contenido_tabla"><center><i class="fa fa-spinner fa-spin fa-4x fa-fw"></i></center></div>
                </div>
            </div>
        </div>
    </div>
</div>
[{/contenido}]
</script>



<div class="modal fade" id="ver_contenido_reglon" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Actualizar reglon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <form id="contenido_fomulario_modal">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Banco</label>
                        <input type="hidden" disabled class="form-control" name="identificador_reglon">
                        <input type="text" disabled class="form-control" name="banco_reglon">
                        <small id="msg_banco_reglon" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Fecha</label>
                        <input type="date" disabled class="form-control" name="fecha_reglon">
                        <small id="msg_fecha_reglon" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Monto</label>
                        <input type="number" step="0.01" min="0"  class="form-control" name="monto_reglon">
                        <small id="msg_monto_reglon" class="form-text text-danger"></small>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="guardar_editar_monto()" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.2.1/mustache.min.js"
    integrity="sha512-Qjrukx28QnvFWISw9y4wCB0kTB/ISnWXPz5/RME5o8OlZqllWygc1AB64dOBlngeTeStmYmNTNcM6kfEjUdnnQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    var _TABLE_GENERAL_1_ = '';
    var _TABLE_GENERAL_2_ = '';
    var _TABLE_GENERAL_3_ = '';
    var _TABLE_GENERAL_4_ = '';
    var _TABLE_GENERAL_5_ = '';
    var _GLOBAL_mes_concentrado = false;

    function zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */
        var zero = "0"; /* String de cero */

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }


    function obtener_numero_semana(mJsDate) {
        if (mJsDate.isValid()) {
            if ($.inArray(mJsDate.isoWeekday(), [6, 7]) == '-1') {
                return mJsDate.format('WW');
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    function obtener_dias(dayStr, endOfMonth) {
        var dias = [];
        dias.push(dayStr.format('Y-MM-DD'));
        for (let index = dayStr.isoWeekday(); index < 5; index++) {
            dayStr = dayStr.add(1, 'days');
            if (dayStr <= endOfMonth) {
                dias.push(dayStr.format('Y-MM-DD'));
            }
        }
        return dias;
    }

    function buscar() {

        $('small.form-text.text-danger').html('');
        $('div.contenido_general').html('');

        var anio = zfill($('input[name=anio]').val(), 4);
        var mes = zfill($('select[name=mes] option:selected').val(), 2);

        var semanas = [];
        var semanas_dias = [];

        let endOfMonth = moment(anio + "-" + zfill(mes, 2) + '-' + zfill(1, 2)).add(1, 'months').subtract(1, 'months')
            .endOf('month');

        var mes_concentrado = [];

        for (var i = 1; i <= 31; i++) {
            var dayStr = moment(anio + "-" + zfill(mes, 2) + '-' + zfill(i, 2));
            var calc_semana = obtener_numero_semana(dayStr);

            var semana_actual = moment();



            if ((calc_semana != null) && (calc_semana <= semana_actual.format('WW'))) {
                // console.log(calc_semana);
                if ($.inArray(calc_semana, semanas) == '-1') {
                    semanas.push(calc_semana);
                    var dias_semana = obtener_dias(dayStr, endOfMonth);

                    semanas_dias = {
                        'semana': calc_semana,
                        'dias': JSON.stringify(dias_semana)
                    };

                    var fecha_str = '';
                    try {
                        fecha_str = moment(dias_semana[0]).format('LL') + ' al ' + moment(dias_semana[dias_semana
                            .length - 1]).format('LL');
                    } catch (error) {
                        fecha_str = '';
                    }
                    var contenido = {
                        contenido: {
                            titulo: fecha_str,
                            semana: calc_semana
                        }
                    };

                    /*
                    var customTags = ['[{','}]'];
                    var template = document.getElementById('tmp_calculos_semana').innerHTML;
                    var rendered = Mustache.render(template, contenido ,{},customTags);
                    $('div.contenido_general').append(rendered);
                    */
                    mes_concentrado.push(semanas_dias);

                    //get_contenido(calc_semana,semanas_dias);

                }
            }


        }
        _GLOBAL_mes_concentrado = mes_concentrado;
        get_contenido3(mes_concentrado);
        get_contenido(mes_concentrado);
        get_contenido2(mes_concentrado);
        get_contenido4(mes_concentrado);
        get_contenido_cargos(mes_concentrado);
    }

    function get_contenido(mes_concentrado) {
        console.log(mes_concentrado);
        // Pace.track(function() {
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_ingresos/buscar',
            data: {
                mes_concentrado: JSON.stringify(mes_concentrado)
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.error == true) {
                    $.each(data.mensaje, function (index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    let listado = atob(data.data.tabla);

                    // $('div#primer_reporte').html(listado);
                    _TABLE_GENERAL_2_ = $(listado);
                    construir_tabla();
                }

            }
        });
        // });
    }
    function get_contenido_cargos(mes_concentrado) {
        console.log(mes_concentrado);
        // Pace.track(function() {
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_ingresos/buscar_cargos',
            data: {
                mes_concentrado: JSON.stringify(mes_concentrado)
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.error == true) {
                    $.each(data.mensaje, function (index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    let listado = atob(data.data.tabla);

                    // $('div#primer_reporte').html(listado);
                    _TABLE_GENERAL_5_ = $(listado);
                    construir_tabla();
                }

            }
        });
        // });
    }
    function get_contenido2(mes_concentrado) {
        console.log(mes_concentrado);
        // Pace.track(function() {
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_ingresos/buscar2',
            data: {
                mes_concentrado: JSON.stringify(mes_concentrado)
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.error == true) {
                    $.each(data.mensaje, function (index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    let listado = atob(data.data.tabla);

                    // $('div#segundo_reporte').html(listado);
                    _TABLE_GENERAL_3_ = $(listado);
                    construir_tabla();
                }

            }
        });
        // });
    }

    function get_contenido4(mes_concentrado) {
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_ingresos/buscar4',
            data: {
                mes_concentrado: JSON.stringify(mes_concentrado)
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.error == true) {
                    $.each(data.mensaje, function (index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    let listado = atob(data.data.tabla);

                    // $('div#segundo_reporte').html(listado);
                    _TABLE_GENERAL_4_ = $(listado);
                    construir_tabla();
                }

            }
        });
    }

    function get_contenido3(mes_concentrado) {
        
        $.ajax({
            type: 'POST',
            url: PATH + '/formatos_efectivo/api/api_ingresos/buscar3',
            data: {
                mes_concentrado: JSON.stringify(mes_concentrado)
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.error == true) {
                    $.each(data.mensaje, function (index, value) {
                        if ($('small#msg_' + index).length) {
                            $('small#msg_' + index).html(value);
                        }
                    });
                } else {
                    let listado = atob(data.data.tabla);

                    //$('div#tercero_reporte').html(listado);

                    _TABLE_GENERAL_1_ = $(listado);
                    construir_tabla();
                }

            }
        });
        
    }

    function construir_tabla(){

        
        var contenido_0 = "";
        var contenido_1 = '';
        try {
            if(_TABLE_GENERAL_1_ != ''){
                var tabla_contenido = _TABLE_GENERAL_1_;
                contenido_1 = ''+tabla_contenido.children('tbody').html()+'';
                try{
                
                    var tamanio_contenido = _TABLE_GENERAL_1_.children('tbody').children('tr').children('td');
                    var tamanio_contenido_size = tamanio_contenido.length;
                    contenido_0 = '<tr class="table-success" ><td colspan="'+(tamanio_contenido.length)+'">INGRESOS</td></tr>'
                
                }catch (error) {
                    contenido_0 = '';
                }
            }

        } catch (error) {
            console.log(error);
            contenido_1 = '';
        }

        var encabezado_2 = '';
        var contenido_2 = '';
        try {
            if(_TABLE_GENERAL_2_ != ''){
                encabezado_2 = ''+_TABLE_GENERAL_2_.children('thead').html()+'';
                contenido_2 = ''+_TABLE_GENERAL_2_.children('tbody').html()+'';
            }
        } catch (error) {
            contenido_2 = '';
        }

        var encabezado_2_cargos = '';
        var contenido_2_cargos = '';
        try {
            if(_TABLE_GENERAL_5_ != ''){
                // encabezado_2 = ''+_TABLE_GENERAL_5_.children('thead').html()+'';
                contenido_2_cargos = ''+_TABLE_GENERAL_5_.children('tbody').html()+'';
            }
        } catch (error) {
            contenido_2_cargos = '';
        }


        var contenido_3 = '';
        var contenido_4 = '';
        var contenido_5 = '';
        var encabezado_3 = '';
        try {
            if(_TABLE_GENERAL_3_ != ''){
                contenido_4 = ''+_TABLE_GENERAL_3_.children('tbody').html()+'';

                try{
                    encabezado_3 = ''+_TABLE_GENERAL_2_.children('thead').html()+'';
                    var tamanio_contenido = _TABLE_GENERAL_3_.children('tbody').children('tr').children('td');
                    var tamanio_contenido_size = tamanio_contenido.length;
                    contenido_3 = '<tr class="table-secondary" ><td colspan="'+(tamanio_contenido.length)+'">&nbsp;</td></tr>';
                    contenido_3 = contenido_3 + '<tr class="table-info"><td colspan="'+(tamanio_contenido.length)+'"><b>FLUJO DE EFECTIVO</b></td></tr>'
                    contenido_3 = contenido_3 + '<tr class="table-success" ><td colspan="'+(tamanio_contenido.length)+'">INGRESOS</td></tr>';
                    contenido_5 = '<tr class="table-secondary" ><td colspan="'+(tamanio_contenido.length)+'">&nbsp;</td></tr>';
                
                }catch (error) {
                    contenido_3 = '';
                    contenido_5 = '';
                    encabezado_3 = '';
                }

            }
        } catch (error) {
            contenido_4 = '';
        }

        var contenido_6 = '';
        try {
            if(_TABLE_GENERAL_4_ != ''){
                contenido_6 = ''+_TABLE_GENERAL_4_.children('tbody').html()+'';
            }
        } catch (error) {
            contenido_6 = '';
        }

        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
        });

        var contenido_7 = '';
        var contenido_7_cantidad = 0;
        var html_reglon = '<tr><td>&nbsp;</td>';
        if( _GLOBAL_mes_concentrado.length > 0 ){
            _GLOBAL_mes_concentrado.forEach(element => {
                var dias = JSON.parse(element.dias);
                if( dias.length > 0 ){
                    var cantidad_total = 0;
                    dias.forEach(element2 => {
                        
                        var cantidad = 0;
                        if($('td.comprobacion_total[fecha='+element2+']').length > 0){
                            
                            $( 'td.comprobacion_total[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad + parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            $( 'td.comprobacion_total_sup[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad - parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            
                        }
                        // if(cantidad.length > 0){
                            cantidad_total = cantidad_total + cantidad;
                            contenido_7_cantidad = contenido_7_cantidad + cantidad;
                            html_reglon = html_reglon +'<td>'+((cantidad == 0)? '<center>-</center>' : formatter.format(cantidad))+'</td>';
                        // }

                    });
                    html_reglon = html_reglon +'<td class="table-primary">'+((cantidad_total == 0)? '<center>-</center>' : formatter.format(cantidad_total))+'</td>';
                    cantidad_total = 0;
                }
            });
        }
        //html_reglon = html_reglon + '<tr>';
        html_reglon = html_reglon + '<td class="table-secondary" >'+((contenido_7_cantidad == 0)? '<center>-</center>' : formatter.format(contenido_7_cantidad))+'</td></tr>';
        contenido_7 = html_reglon;

        $('table#contenedor_reporte').html( '<tbody>'+contenido_0 + contenido_1 + encabezado_2 + contenido_2  +contenido_3+encabezado_3+contenido_4+contenido_5+contenido_6+ contenido_7+contenido_5+'</tbody>' );


        var contenido_8 = '';
        var contenido_8_cantidad = 0;
        html_reglon = '<tr><td>(  =  )  TOTAL DE INGRESOS DEL DIA</td>';
        if( _GLOBAL_mes_concentrado.length > 0 ){
            _GLOBAL_mes_concentrado.forEach(element => {
                var dias = JSON.parse(element.dias);
                if( dias.length > 0 ){
                    var cantidad_total = 0;
                    dias.forEach(element2 => {
                        
                        var cantidad = 0;
                        if($('td.total_ingresos_flujo[fecha='+element2+']').length > 0){
                            
                           
                            $( 'td.total_ingresos_flujo[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad + parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            
                        }
                        // if(cantidad.length > 0){
                            cantidad_total = cantidad_total + cantidad;
                            contenido_8_cantidad = contenido_8_cantidad+ cantidad;
                            html_reglon = html_reglon +'<td class="saldo_inicial" fecha="'+element2+'">'+((cantidad == 0)? '<center>-</center>' : formatter.format(cantidad))+'</td>';
                        // }

                    });
                    html_reglon = html_reglon +'<td class="table-primary" >'+((cantidad_total == 0)? '<center>-</center>' : formatter.format(cantidad_total))+'</td>';
                    cantidad_total = 0;
                }
            });
        }
        html_reglon = html_reglon + '<td class="table-secondary" >'+((contenido_8_cantidad == 0)? '<center>-</center>' : formatter.format(contenido_8_cantidad))+'</td></tr>';
        contenido_8 = html_reglon;

        $('table#contenedor_reporte').html( '<tbody>'+contenido_0 + contenido_1 + encabezado_2 + contenido_2  +contenido_3+encabezado_3+contenido_4+contenido_5+contenido_6+ contenido_7+contenido_5+contenido_8+contenido_2_cargos+'</tbody>' );


        var contenido_9 = '';
        var contenido_9_cantidad = 0;
        var html_reglon = '<tr><td>(  =  )SALDO EN EFECTIVO AL FINAL DEL DIA</td>';
        if( _GLOBAL_mes_concentrado.length > 0 ){
            _GLOBAL_mes_concentrado.forEach(element => {
                var dias = JSON.parse(element.dias);
                if( dias.length > 0 ){
                    var cantidad_total = 0;
                    dias.forEach(element2 => {
                        
                        var cantidad = 0;
                        if($('td.comprobacion_total[fecha='+element2+']').length > 0){
                            
                            $( 'td.saldo_inicial[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad + parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            $( 'td.comprobacion_total_inf[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad - parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            
                        }
                        // if(cantidad.length > 0){
                            cantidad_total = cantidad_total + cantidad;
                            contenido_9_cantidad = contenido_9_cantidad +  cantidad;
                            html_reglon = html_reglon +'<td class="total_saldo_final" fecha="'+element2+'" >'+((cantidad == 0)? '<center>-</center>' : formatter.format(cantidad))+'</td>';
                        // }

                    });
                    html_reglon = html_reglon +'<td class="table-primary">'+((cantidad_total == 0)? '<center>-</center>' : formatter.format(cantidad_total))+'</td>';
                    cantidad_total = 0;
                }
            });
        }
        html_reglon = html_reglon + '<td class="table-secondary" >'+((contenido_9_cantidad == 0)? '<center>-</center>' : formatter.format(contenido_9_cantidad))+'</td></tr>';
        contenido_9 = html_reglon;


        $('table#contenedor_reporte').html( '<tbody>'+contenido_0 + contenido_1 + encabezado_2 + contenido_2  +contenido_3+encabezado_3+contenido_4+contenido_5+contenido_6+ contenido_7+contenido_5+contenido_8+contenido_2_cargos+contenido_9+'</tbody>' );

        var contenido_10 = '';
        var contenido_10_cantidad = 0;
        var html_reglon = '<tr><td>UTILIDAD O ( PERDIDA ) EN FLUJO</td>';
        if( _GLOBAL_mes_concentrado.length > 0 ){
            _GLOBAL_mes_concentrado.forEach(element => {
                var dias = JSON.parse(element.dias);
                if( dias.length > 0 ){
                    var cantidad_total = 0;
                    dias.forEach(element2 => {
                        
                        var cantidad = 0;
                        if($('td.comprobacion_total[fecha='+element2+']').length > 0){
                            
                            $( 'td.total_saldo_final[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad + parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            $( 'td.total_saldo_inicial[fecha='+element2+']' ).each(function( index ) {
                                if($( this ).text() != '-'){
                                    cantidad = cantidad - parseFloat($.trim($( this ).text()).split('$').join('').split(',').join(''))
                                }
                            });

                            
                        }
                        // if(cantidad.length > 0){
                            cantidad_total = cantidad_total + cantidad;
                            contenido_10_cantidad = contenido_10_cantidad + cantidad;
                            html_reglon = html_reglon +'<td  >'+((cantidad == 0)? '<center>-</center>' : formatter.format(cantidad))+'</td>';
                        // }

                    });
                    html_reglon = html_reglon +'<td class="table-primary">'+((cantidad_total == 0)? '<center>-</center>' : formatter.format(cantidad_total))+'</td>';
                    cantidad_total = 0;
                }
            });
        }
        html_reglon = html_reglon + '<td class="table-secondary" >'+((contenido_10_cantidad == 0)? '<center>-</center>' : formatter.format(contenido_10_cantidad))+'</td></tr>';
        contenido_10 = html_reglon;

        $('table#contenedor_reporte').html( '<tbody>'+contenido_0 + contenido_1 + encabezado_2 + contenido_2  +contenido_3+encabezado_3+contenido_4+contenido_5+contenido_6+ contenido_7+contenido_5+contenido_8+contenido_2_cargos+contenido_9+contenido_10+'</tbody>' );
    }

    function editar_monto(obj){
        $('input[name=identificador_reglon]').val( $(obj).attr('identificador') );
        $('input[name=banco_reglon]').val( $(obj).attr('reglon') );
        $('input[name=fecha_reglon]').val( $(obj).attr('fecha') );
        $('input[name=monto_reglon]').val( $(obj).attr('monto') );
        $('div#ver_contenido_reglon').modal('show');
    }

    function guardar_editar_monto(){
        $.ajax({
            url: PATH + '/formatos_efectivo/api/api_ingresos/guardar_formulario_reglon',
            type: 'POST',
            data: {
                identificador_reglon: $("input[name=identificador_reglon]").val(),
                banco_reglon: $("input[name=banco_reglon]").val(),
                fecha_reglon: $("input[name=fecha_reglon]").val(),
                monto_reglon: $("input[name=monto_reglon]").val()
            },
            success: function(response) {
                if (response.estatus != "error") {
                    $("#ver_contenido_reglon").modal("hide");
                    get_contenido2(_GLOBAL_mes_concentrado);
                    get_contenido4(_GLOBAL_mes_concentrado);
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: 'Éxito!',
                    //     text: response.mensaje,
                    // }).then((result) => {
                    //     get_contenido2(_GLOBAL_mes_concentrado);
                    // });

                } else {
                    if (response.errores) {
                        $.each(response.errores, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.mensaje,
                        })
                    }
                }
            },
           
        });
        // $('div#ver_contenido_reglon').modal('show');
    }




    $(function () {

        // Pace.stop();

        moment.locale('es');
        var value = $('select[name=mes]').attr('value');
        for (let index = 1; index <= 12; index++) {
            var mes = moment(index, "MM").format('MMMM')
            if (zfill(index, 2) == value) {
                $('select[name=mes]').append('<option value="' + zfill(index, 2) + '" selected>' + mes +
                    '</option>');
            } else {
                $('select[name=mes]').append('<option value="' + zfill(index, 2) + '">' + mes + '</option>');
            }
        }
        buscar();

    })
</script>
@endsection