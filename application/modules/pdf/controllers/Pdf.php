<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

/**

 **/
class Pdf extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html','url'));

      date_default_timezone_set('America/Mexico_City');

       //if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function carta_factura($id_factura){
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura_vehiculo');
    $conceptos = $this->Mgeneral->get_row('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');

    require_once 'cfdi41/pdf/lib/vendor/autoload.php';
    // require_once '/var/www/web/dms/contabilidad_queretaro/cfdi41/pdf/lib/vendor/autoload.php';

   
    
    $datos['test'] = "";

    setlocale(LC_ALL,"es_ES");
    $fecha = date('Y/m/d');
    $fecha = str_replace("/", "-", $fecha);			
    $newDate = date("d-m-Y", strtotime($fecha));				
    $mesDesc = strftime("%B de %Y", strtotime($newDate));
    
    $datos['fecha'] = date('d').' de '.$mesDesc;
    $datos['cliente'] = $datos_fac->receptor_nombre;
    $datos['domicilio'] = $datos_fac->receptor_direccion;

    $datos['marca'] = $conceptos->marca;
    $datos['modelo'] = $conceptos->linea;
    $datos['no_eco'] = $conceptos->no_inventario;
    $datos['tipo'] = $conceptos->linea;
    $datos['motor'] = $conceptos->motor;
    $datos['serie'] = $conceptos->niv;
    $datos['valor'] = $conceptos->concepto_importe + $conceptos->importe_iva;;
    $datos['registro_federal'] = '';

    $datos['leyenda'] = $conceptos->leyenda_dcto;
    $html = $this->load->view('pdf/formato_carta_factura', $datos, true);
    $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
    $html2pdf->pdf->SetDisplayMode('fullpage');

        //$html2pdf->pdf->SetAuthor( $this->autor );
        //$html2pdf->pdf->SetTitle( $this->titulo );
        //$html2pdf->pdf->SetSubject( $this->asunto );

    $html2pdf->writeHTML($html);
    ob_end_clean();
    $html2pdf->output('carta_factura.pdf','I');
    exit();
  }
}