<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facturacion_vehiculos extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session', 'curl'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
    ini_set('display_errors', -1);
    ini_set('display_startup_errors', -1);
    error_reporting(E_ALL);
  }

  public function lista($filtro = 1)
  {

    $titulo['titulo'] = "Lista de Facturas";
    $titulo['titulo_dos'] = "Lista de facturas";
    $datos['row'] = $this->Mgeneral->get_row('id', 1, 'informacion');
    $datos['facturas'] = $this->Mgeneral->get_table('factura_vehiculo');
    /*if($filtro == 1){
            $datos['facturas'] = $this->Mgeneral->get_table('factura_vehiculo');
        }else{
          $datos['facturas'] = $this->Mgeneral->get_result('prefactura',1,'factura');
        }*/
    $datos['filtro'] = $filtro;
    $datos['index'] = '';

    $this->blade->render('lista', $datos);
  }




  public function nueva($id_factura)
  {
    // error_reporting(E_ALL);
    //ini_set('display_errors', '1');


    $datos['forma_pagos'] = $this->Mgeneral->get_table('ca_formas_pago');
    $datos['regimen_fiscal'] = $this->Mgeneral->get_table('ca_remimen_fiscal');
    $datos['metodo_pagos'] = $this->Mgeneral->get_table('ca_metodos_pago');
    $datos['tipo_comprobante'] = $this->Mgeneral->get_table('ca_tipo_comprobante');
    $datos['uso_cfdi'] = $this->Mgeneral->get_table('ca_uso_cfdi');
    if ($id_factura == '0') {

      $datos['emisor'] = $this->Mgeneral->get_row('id', 1, 'datos');
      $datos['factura'] = "";
    } else {

      $datos['emisor'] = $this->Mgeneral->get_row('id', 1, 'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura', $id_factura, 'factura_cfdi_relacionados_vehiculo');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
      $datos['index'] = '';
    }

    $this->blade->render('alta', $datos);
  }


  public function guarda_datos($id_factura)
  {
    $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'required');
    //$this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente', 'required');
    $this->form_validation->set_rules('factura_moneda', 'factura_moneda', 'required');
    $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion', 'required');
    $this->form_validation->set_rules('factura_fecha', 'factura_fecha', 'required');
    $this->form_validation->set_rules('receptor_email', 'receptor_email', 'required');
    $this->form_validation->set_rules('factura_folio', 'factura_folio', 'required');
    $this->form_validation->set_rules('factura_serie', 'factura_serie', 'required');
    $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion', 'required');
    $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago', 'required');
    $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago', 'required');
    $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante', 'required');
    $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC', 'required');
    $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI', 'required');
    //$this->form_validation->set_rules('pagada', 'pagada', 'required');

    //$this->form_validation->set_rules('comentario', 'comentario', 'required');
    $response = validate($this);

    if ($response['status']) {

      /*if($id_factura == 0){
          $emisor = $this->Mgeneral->get_row('id',1,'datos');
          $id_factura = get_guid();
          $data['facturaID'] = $id_factura;
          $data['status'] = 1;
          $data['emisor_RFC'] = $emisor->rfc;
          $data['emisor_regimenFiscal'] = $emisor->regimen;
          $data['emisor_nombre'] = $emisor->razon_social;
          $data['receptor_uso_CFDI'] = "G03";
          $data['pagada'] = "SI";
          $data['tipo_factura'] = 3;
          $this->Mgeneral->save_register('factura_vehiculo', $data);
         }
         */

      $data['receptor_nombre'] = $this->input->post('receptor_nombre');
      //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
      $data['factura_moneda'] = $this->input->post('factura_moneda');
      $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
      $data['factura_fecha'] = date('Y-m-d H:i:s'); //$this->input->post('numero');
      $data['receptor_email'] = $this->input->post('receptor_email');
      $data['factura_folio'] = $this->input->post('factura_folio');
      $data['factura_serie'] = $this->input->post('factura_serie');
      $data['receptor_direccion'] = $this->input->post('receptor_direccion');
      $data['factura_formaPago'] = $this->input->post('factura_formaPago');
      $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
      $data['factura_tipoComprobante'] = $this->input->post('factura_tipoComprobante');
      $data['receptor_RFC'] = $this->input->post('receptor_RFC');
      $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
      $data['RegimenFiscalReceptor'] = $this->input->post('RegimenFiscalReceptor');
      //$data['pagada'] = $this->input->post('pagada');
      $data['comentario'] = $this->input->post('comentario');
      //$data['almacen'] = $this->input->post('almacen');

      $this->Mgeneral->update_table_row('factura_vehiculo', $data, 'facturaID', $id_factura);

      $response['id_factura_auto'] = $id_factura;
    }
    //  echo $response;
    echo json_encode(array('output' => $response));

    exit();
  }


  public function conceptos($id_factura)
  {

    $datos['emisor'] = $this->Mgeneral->get_row('id', 1, 'datos');
    $datos['factura'] = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');
    $datos['medidas'] = $this->Mgeneral->get_result('status', 0, 'medidas'); //$this->Mgeneral->get_table('medidas');
    $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura', $id_factura, 'factura_cfdi_relacionados_vehiculo');
    $datos['conceptos'] = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    $datos['productos'] = $this->Mgeneral->get_result('status', 0, 'productos'); //$this->Mgeneral->get_table('productos');
    $datos['toma_vehiculo'] = $this->Mgeneral->get_row('id_factura', $id_factura, 'factura_toma_auto');
    $datos['facturaID'] = $id_factura;
    $titulo['titulo'] = "Factura";
    $titulo['titulo_dos'] = "prefactura";
    $datos['row'] = $this->Mgeneral->get_row('id', 1, 'informacion');

    $datos['aduanas'] = $this->Mgeneral->get_table('ca_aduana');

    $datos['index'] = '';

    $this->blade->render('conceptos_new', $datos);
  }

  public function guarda_datos_concepto($id_factura)
  {
    //$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
    //$response = validate($this);
    $response['status'] = true;

    if ($response['status']) {

      $data['concepto_facturaId'] = $id_factura;
      $data['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');
      $data['concepto_unidad'] = $this->input->post('concepto_unidad');
      //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
      $data['clave_sat'] = $this->input->post('clave_sat');
      $data['unidad_sat'] = $this->input->post('unidad_sat');
      $data['concepto_nombre'] = $this->input->post('concepto_nombre');
      $data['concepto_precio'] = $this->input->post('concepto_precio');
      $data['concepto_importe'] = $this->input->post('concepto_importe');
      $data['impuesto_iva'] = $this->input->post('impuesto_iva');
      $data['impuesto_iva_tipoFactor'] = $this->input->post('impuesto_iva_tipoFactor');
      $data['impuesto_iva_tasaCuota'] = $this->input->post('impuesto_iva_tasaCuota');
      $data['impuesto_ISR'] = $this->input->post('impuesto_ISR');
      $data['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
      $data['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
      $data['tipo'] = $this->input->post('tipo');
      $data['nombre_interno'] = $this->input->post('nombre_interno');
      $data['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');
      $data['concepto_cantidad'] = $this->input->post('concepto_cantidad');
      $data['importe_iva'] = $this->input->post('importe_iva');
      $data['base_iva'] = $this->input->post('base_iva');
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      $data['clave_vehicular'] = $this->input->post('clave_vehicular');
      $data['niv'] = $this->input->post('niv');
      $data['numero'] = $this->input->post('numero');
      $data['fecha'] = $this->input->post('fecha');
      $data['aduana'] = $this->input->post('aduana');

      $concepto_id = $this->input->post('concepto_id');
      $this->Mgeneral->update_table_row('factura_conceptos_vehiculo', $data, 'concepto_id', $concepto_id);

      //$this->Mgeneral->save_register('factura_conceptos_vehiculo',$data);
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
    exit();
  }


  public function ver_factura($id_factura)
  {

    $datos['emisor'] = $this->Mgeneral->get_row('id', 1, 'datos');
    $datos['factura'] = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');
    $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura', $id_factura, 'factura_cfdi_relacionados_vehiculo');
    $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    $datos['facturaID'] = $id_factura;
    $titulo['titulo'] = "Factura";
    $titulo['titulo_dos'] = "prefactura";
    $datos['row'] = $this->Mgeneral->get_row('id', 1, 'informacion');

    $datos['toma_vehiculo'] = $this->Mgeneral->get_result('id_factura', $id_factura, 'factura_toma_auto');

    $this->blade->render('ver_factura', $datos);
  }



  public function generar_factura($id_factura)
  {

    //        ini_set('display_errors', -1);

    //error_reporting(E_ALL);

    $this->load->model('MfacturaVehiculo', '', TRUE);
    $factura_nueva = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');
    $conceptos = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');

    //echo '<a download href="https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/statics/facturas/prefactura/15618ACAD1D1B45F0C5B98695DA78636.xml">descargar</a>';

    //die();


    $precio_sin_iva = 0;
    //CUANDO ES UN SEMINUEVO
    if ($factura_nueva->tipo_auto == 2) {
      /*
          1	persona fisica
          2	persona fisica con actividad empresarial
          3	persona moral
          */
      //persona fisica , el iva es normal
      if ($factura_nueva->tipo_persona == 1) {
        $precio_sin_iva = (float) round($conceptos->precio_venta / 1.16, 2);
      }
      if (($factura_nueva->tipo_persona == 2) || ($factura_nueva->tipo_persona == 3)) {
        /*//precio de compra sin iva
              $precio_compra1 = $factura_nueva->total_compra_seminuevo * 1.16;
              $precio_comision_con_iva1 = $conceptos->precio_venta - $precio_compra1;
              //esto es la base del iva
              $precio_sin_iva = (float) round($precio_comision_con_iva1 / 1.16, 2);  
              */


        //precio de compra sin iva
        $precio_compra1_sin_iva = $factura_nueva->total_compra_seminuevo / 1.16;

        $comision = $conceptos->precio_venta - $factura_nueva->total_compra_seminuevo;
        $comision_sin_iva =  $comision / 1.16;

        $total_real_sin_iva = $conceptos->precio_venta - ($comision_sin_iva * .16);


        //$precio_comision_con_iva1 = $conceptos->precio_venta - $precio_compra1;
        //esto es la base del iva
        //$precio_sin_iva = (float) round($precio_comision_con_iva1 / 1.16, 2);
        $precio_sin_iva = (float) round($total_real_sin_iva, 2);
      }
    } else {
      //CUANDO ES UN NUEVO
      //$iva_total =  number_format($conceptos->concepto_importe * .16 , 2, '.', '');
      $precio_sin_iva =  (float) round($conceptos->precio_venta / 1.16, 2);
    }



    //AQUÍ SE CALCULA EL IVA 
    $iva_total = 0;
    $base_iva = 0;
    //CUANDO ES UN SEMINUEVO
    if ($factura_nueva->tipo_auto == 2) {
      /*
          1	persona fisica
          2	persona fisica con actividad empresarial
          3	persona moral
        */
      //persona fisica , el iva es normal
      if ($factura_nueva->tipo_persona == 1) {

        $iva_total = (float) round(($conceptos->precio_venta / 1.16) * .16, 2);
        $base_iva = (float) round($conceptos->precio_venta / 1.16, 2); //$conceptos->concepto_importe;
      }
      if (($factura_nueva->tipo_persona == 2) || ($factura_nueva->tipo_persona == 3)) {
        //precio de compra sin iva
        /* $precio_compra = $factura_nueva->total_compra_seminuevo * 1.16;
                 $precio_comision_con_iva = $conceptos->precio_venta - $precio_compra;
                 //esto es la base del iva
                 $base_iva = (float) round($precio_comision_con_iva / 1.16 , 2);
                 //iva que se cobra
                 $iva_real = $base_iva * .16;

                 $iva_total = (float) round($iva_real , 2);  
                 */
        $comision = $conceptos->precio_venta - $factura_nueva->total_compra_seminuevo;
        $comision_sin_iva =  $comision / 1.16;
        $total_real_sin_iva = $conceptos->precio_venta - ($comision_sin_iva * .16);
        $iva_total =  (float) round($comision_sin_iva * .16, 2);
        $base_iva = (float) round($comision_sin_iva, 2);
      }
    } else {
      //CUANDO ES UN NUEVO
      $base_iva = (float) round($conceptos->precio_venta / 1.16, 2);
      $iva_total = (float) round($base_iva * .16, 2);
    }



    $cfdi = $this->MfacturaVehiculo;
    $cfdi->cargarInicio();
    $cfdi->cabecera($factura_nueva->tipo_auto, $factura_nueva->enajenacion);

    $cfdi->fact_serie        = $factura_nueva->factura_serie;                             // 4.1 Número de serie.
    $cfdi->fact_folio        = $factura_nueva->factura_folio; //mt_rand(1000, 9999);             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
    $cfdi->NoFac             = $cfdi->fact_serie . $cfdi->fact_folio;         // 4.3 Serie de la factura concatenado con el número de folio.
    $cfdi->fact_tipcompr     = $factura_nueva->factura_tipoComprobante;                             // 4.4 Tipo de comprobante.
    $cfdi->fact_exportacion  = "01";                            // 4.5 Atributo requerido para expresar si el comprobante ampara una operación de exportación.
    $cfdi->tasa_iva          = 16;
    if ($factura_nueva->tipo_auto == 1) {
      // 4.6 Tasa del impuesto IVA.
      $cfdi->subTotal          = $precio_sin_iva; //$this->redondeo( $this->Mgeneral->factura_subtotal2($id_factura), 2, '.', '');//$factura_nueva->subtotal;//$this->Mgeneral->factura_subtotal($id_factura);                              // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    }
    if ($factura_nueva->tipo_auto == 2) {
      // 4.6 Tasa del impuesto IVA.
      $cfdi->subTotal          = $precio_sin_iva; //$this->redondeo( $this->Mgeneral->factura_subtotal2_seminuevo_subtotal($id_factura), 2, '.', '');//$factura_nueva->subtotal;//$this->Mgeneral->factura_subtotal($id_factura);                              // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    }
    $cfdi->descuento         = 0;                               // 4.8 Descuento (se calculan mas abajo).
    if ($factura_nueva->tipo_auto == 1) {
      $cfdi->IVA               = $iva_total; //$this->redondeo($this->Mgeneral->factura_iva_total2($id_factura), 2, '.', ''); //$factura_nueva->iva;//$this->Mgeneral->factura_iva_total($id_factura);                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
    }
    if ($factura_nueva->tipo_auto == 2) {
      $cfdi->IVA               = $iva_total; // $this->redondeo($this->Mgeneral->factura_iva_total2_seminuevo($id_factura), 2, '.', ''); //$factura_nueva->iva;//$this->Mgeneral->factura_iva_total($id_factura);                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
    }
    if ($factura_nueva->tipo_auto == 1) {
      $cfdi->total             = (float) round($precio_sin_iva + $iva_total, 2);; //$this->redondeo($this->Mgeneral->factura_subtotal2($id_factura) + $this->Mgeneral->factura_iva_total2($id_factura), 2)  ;     //$factura_nueva->total;//$this->truncateFloat($this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura), 2)  ;                            // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    }
    if ($factura_nueva->tipo_auto == 2) {
      $cfdi->total             = (float) round($precio_sin_iva + $iva_total, 2); // $this->redondeo($this->Mgeneral->factura_subtotal2_seminuevo($id_factura), 2)  ;     //$factura_nueva->total;//$this->truncateFloat($this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura), 2)  ;                            // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    }
    $cfdi->fecha_fact        = date("Y-m-d") . "T" . date("H:i:s"); // 4.11 Fecha y hora de facturación.
    $cfdi->NumCtaPago        = "";                          // 4.12 Número de cuenta (sólo últimos 4 dígitos, opcional).
    $cfdi->condicionesDePago = "";                   // 4.13 Condiciones de pago.
    $cfdi->formaDePago       = $factura_nueva->factura_formaPago;                            // 4.14 Forma de pago.
    $cfdi->metodoDePago      = $factura_nueva->factura_medotoPago;                           // 4.15 Clave del método de pago. Consultar catálogos de métodos de pago del SAT.
    $cfdi->TipoCambio        = 1;                               // 4.16 Tipo de cambio de la moneda.
    $cfdi->LugarExpedicion   = '66260'; ///$factura_nueva->fatura_lugarExpedicion;                         // 4.17 Lugar de expedición (código postal).
    $cfdi->moneda            = 'MXN'; //$factura_nueva->factura_moneda;                           // 4.18 Moneda
    $cfdi->totalImpuestosRetenidos   = 0;                       // 4.19 Total de impuestos retenidos (se calculan mas abajo).
    $cfdi->totalImpuestosTrasladados = 0;
    //CP 45079
    $cfdi->datosGenerales();



    ### 9. DATOS GENERALES DEL EMISOR #################################################  
    $cfdi->emisor_rs = "ESCUELA KEMPER URGATE";  // 9.1 Nombre o Razón social.
    $cfdi->emisor_rfc = "EKU9003173C9";  // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
    $cfdi->emisor_ClaRegFis = "601"; // 9.3 Clave del Régimen fiscal.  
    $cfdi->datosEmisor();

    ### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
    $cfdi->receptor_rfc = "MASO451221PM4";  // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
    $cfdi->receptor_rs  = "MARIA OLIVIA MARTINEZ SAGAZ";  // 10.4 Nombre o razón social.
    $cfdi->DomicilioFiscalReceptor = "80290";             // 10.5 Domicilio fiscal del Receptor (código postal).
    $cfdi->RegimenFiscalReceptor = "616";                 // 10.6 Régimen fiscal del receptor.
    $cfdi->UsoCFDI = "S01";                               // Uso del CFDI.
    $cfdi->datosReceptor();



    $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    foreach ($conceptos_factura as $concepto_fac) :


      $concepto = array();
      $concepto['ClaveProdServ'] = $concepto_fac->clave_sat;
      $concepto['NoIdentificacion'] = $concepto_fac->niv;
      $concepto['Cantidad'] = 1;
      $concepto['ClaveUnidad'] = $concepto_fac->unidad_sat;
      $unidad_s = $this->Mgeneral->get_row('ClaveUnidad', $concepto_fac->unidad_sat, 'ca_claveunidad');
      $concepto['Unidad'] = $unidad_s->Nombre; //$concepto_fac->id_producto_servicio_interno;
      $concepto['Descripcion'] = $concepto_fac->concepto_nombre;


      if ($factura_nueva->tipo_auto == 2) {
        $concepto['ValorUnitario'] = $precio_sin_iva; //$cfdi->subTotal  ;
      } else {
        $concepto['ValorUnitario'] = $precio_sin_iva; //$concepto_fac->concepto_precio;
      }



      if ($factura_nueva->tipo_auto == 2) {
        $total_total =
          $concepto['Importe'] = $precio_sin_iva; //$cfdi->subTotal ;//number_format($concepto_fac->precio_venta, 2, '.', '');
      } else {
        $concepto['Importe'] = $precio_sin_iva; //$concepto_fac->concepto_importe;
      }



      $concepto['Descuento'] = 0;
      $concepto['ObjetoImp'] = '02';
      $concepto['version'] = '1.1';
      $concepto['ClaveVehicular'] = $concepto_fac->clave_vehicular;
      $concepto['Niv'] = $concepto_fac->niv;
      $concepto['numero'] = $concepto_fac->numero;
      $concepto['fecha'] = $concepto_fac->fecha;
      $concepto['aduana'] = $concepto_fac->aduana;

      $concepto['nacional_importado'] = $factura_nueva->unidad_importada;



      //auto seminuevo
      if ($factura_nueva->tipo_auto == 2) {

        // $iva_total = 0;
        // $iva_total =  number_format($concepto_fac->base_iva * .16, 2, '.', '');

        $impuesto = array();
        $impuesto['TipoFactor'] = "Tasa";
        $impuesto['Base'] =  $base_iva; //$concepto_fac->base_iva;//$this->redondeo($concepto_fac->concepto_importe, 2, '.', '');
        $impuesto['Impuesto'] = '002';
        $impuesto['TipoFactor'] = 'Tasa';
        $impuesto['TasaOCuota'] = '0.160000';
        $impuesto['Importe'] = $iva_total; //$this->redondeo($concepto_fac->concepto_importe * .16, 2, '.', '');//$concepto_fac->importe_iva;

        $concepto['Base'] = $base_iva; // $concepto_fac->base_iva;
        $concepto['tipo_auto'] = $factura_nueva->tipo_auto;

        $concepto['traslado'] = $impuesto;
        $cfdi->conceptos($concepto); // agrega los conceptos y los impuestos

      }

      //auto nuevo
      if ($factura_nueva->tipo_auto == 1) {
        $impuesto = array();
        $impuesto['TipoFactor'] = "Tasa";
        $impuesto['Base'] = $base_iva; // $this->redondeo($concepto_fac->concepto_importe, 2, '.', '');
        $impuesto['Impuesto'] = '002';
        $impuesto['TipoFactor'] = 'Tasa';
        $impuesto['TasaOCuota'] = '0.160000';
        $impuesto['Importe'] = $iva_total; //$this->redondeo($concepto_fac->concepto_importe * .16, 2, '.', '');//$concepto_fac->importe_iva;
        $concepto['tipo_auto'] = $factura_nueva->tipo_auto;

        $concepto['traslado'] = $impuesto;
        $cfdi->conceptos($concepto); // agrega los conceptos y los impuestos

      }


    endforeach;


    $cfdi->impuestosGeneral(); // agrega impuestos generales

    $toma_vehiculo = $this->Mgeneral->get_row('id_factura', $id_factura, 'factura_toma_auto');
    $enajenacion = 0;
    $data_enajenacion = array();
    if (is_object($toma_vehiculo)) {
      $data_enajenacion['Version'] = "1.0";
      $data_enajenacion['montoAdquisicion'] = $toma_vehiculo->toma_monto_adquisicion;
      $data_enajenacion['montoEnajenacion'] = $toma_vehiculo->toma_monto_enajenacion;
      $data_enajenacion['claveVehicular'] = $toma_vehiculo->toma_clave_vehicular;
      $data_enajenacion['marca'] = $toma_vehiculo->toma_marca;
      $data_enajenacion['tipo'] = $toma_vehiculo->toma_tipo;
      $data_enajenacion['modelo'] = $toma_vehiculo->toma_modelo;
      $data_enajenacion['numeroMotor'] = $toma_vehiculo->toma_numero_motor;
      $data_enajenacion['NIV'] = $toma_vehiculo->toma_niv;
      $data_enajenacion['valor'] = $toma_vehiculo->toma_valor;
      $data_enajenacion['numero'] = $toma_vehiculo->numero;
      $data_enajenacion['fecha'] = $toma_vehiculo->fecha;
      $data_enajenacion['aduana'] = $toma_vehiculo->aduana;
      $enajenacion = 1;
    }
    $cfdi->agregar_complemento($data_enajenacion, $enajenacion);

    $cfdi->sellar();





    //$cfdi->MfacturaVehiculo->obtenerXml();
    //var_dump($cfdi->obtenerXml());
    //echo $cfdi->obtenerXml();
    //print_r($cfdi->obtenerXml());
    //die();





    if ($cfdi->obtenerXml()) {
      //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
      //file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      file_put_contents("/var/www/web/dms/contabilidad_queretaro/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      //file_put_contents("https://planificadorempresarial.com/facturacion/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      $data_url_prefactura['url_prefactura'] = base_url() . "statics/facturas/prefactura/$id_factura.xml";
      $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
      $this->Mgeneral->update_table_row('factura_vehiculo', $data_url_prefactura, 'facturaID', $id_factura);
      $this->enviar_factura($id_factura);
      //echo $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";
    } else {
      $data_respone['error'] = 1;
      $data_respone['error_mensaje'] = "Ocurrio un error";
      $data_respone['factura'] = $id_factura;


      echo json_encode($data_respone);
      die;
    }
    //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
    //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
    //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

    //$this->enviar_factura($id_factura);

    die;

    // Mostrar objeto que contiene los datos del CFDI
    print_r($cfdi);


    die;



    die('OK');
  }

  function truncateFloat($numero, $digitos)
  {
    $truncar = 10 ** $digitos;
    return intval($numero * $truncar) / $truncar;
  }

  public function redondeo($number, $significance = 2)
  {
    return (float) round($number, $significance);
    // return (float)( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : 0;
  }




  public function enviar_factura($id_factura)
  {
    //echo $id_factura;
    //echo "".base_url()."index.php/factura/generar_factura/".$id_factura;
    //echo  $datos_fac->url_prefactura;
    $datos_fac = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');


    # Username and Password, assigned by FINKOK
    $username = 'liberiusg@gmail.com';
    $password = '*Libros7893811';

    # Read the xml file and encode it on base64
    $invoice_path = "/var/www/web/dms/contabilidad_queretaro/statics/facturas/prefactura/$id_factura.xml"; //$this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
    $xml_file = fopen($invoice_path, "rb");
    $xml_content = fread($xml_file, filesize($invoice_path));
    fclose($xml_file);

    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$xml_content = base64_encode($xml_content);

    # Consuming the stamp service
    $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
    $client = new SoapClient($url);

    $params = array(
      "xml" => $xml_content,
      "username" => $username,
      "password" => $password
    );
    $response = $client->__soapCall("stamp", array($params));
    //print_r($response);
    ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
    //print $response->stampResult->xml;

    ####mostrar el código de error en caso de presentar alguna incidencia
    #print $response->stampResult->Incidencias->Incidencia->CodigoError;
    ####mostrar el mensaje de incidencia en caso de presentar alguna
    if (isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)) {
      $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
      //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $this->Mgeneral->update_table_row('factura_vehiculo', $data_sat, 'facturaID', $id_factura);
      $data_respone['error'] = 1;
      $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $data_respone['factura'] = $id_factura;
      echo json_encode($data_respone);
      die;
    } else {
      $data_sat['sat_uuid'] = $response->stampResult->UUID;
      $data_sat['sat_fecha'] = $response->stampResult->Fecha;
      $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
      $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
      $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
      $data_sat['sat_error'] = "0";
      $data_sat['sat_codigo_error'] = "0";
      $this->Mgeneral->update_table_row('factura_vehiculo', $data_sat, 'facturaID', $id_factura);
      //file_put_contents($this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
      file_put_contents("/var/www/web/dms/contabilidad_queretaro/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
      $data_respone['error'] = 0;
      $data_respone['error_mensaje'] = "Factura timbrada";
      $data_respone['factura'] = $id_factura;
      $data_respone['pdf'] = "" . base_url() . "index.php/facturacion/genera_pdf_old/" . $id_factura . "";
      $data_respone['xml'] = "" . base_url() . "statics/facturas/facturas_xml/" . $id_factura . ".xml";
      if ($datos_fac->tipo_auto == 1) {

        if ($datos_fac->intercambio == "intercambio") {
          $this->enviar_a_contabilidad_intercambio($id_factura, $datos_fac->capacidad_personas, $datos_fac->capacidad_kg, $datos_fac->tipo_auto_nuevo, $datos_fac->combustible);
        } else {
          $this->enviar_a_contabilidad($id_factura, $datos_fac->capacidad_personas, $datos_fac->capacidad_kg, $datos_fac->tipo_auto_nuevo, $datos_fac->combustible);
        }
        $this->enviar_id_factura($datos_fac->id_solicitud, $id_factura);
      }
      if ($datos_fac->tipo_auto == 2) {
        $this->enviar_a_contabilidad_seminuevos($id_factura, $datos_fac->tipo_auto_seminuevo, $datos_fac->tipo_persona);
        $this->enviar_id_factura_seminuevo($datos_fac->remision_id, $id_factura);
      }
      echo json_encode($data_respone);


      die;
    }
    #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
  }


  public function genera_pdf_old($id_factura)
  {
    $datos_fac = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');

    $conceptos = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');

    //$datos_cliente = $this->Mgeneral->get_row('id',$datos_fac->receptor_id_cliente,'clientes');

    $datas_empresa = $this->Mgeneral->get_row('id', 1, 'datos');
    $logo = 'cfdi41/logo_mylsa.png'; //$this->Mgeneral->get_row('id',1,'informacion')->logo;
    require '/var/www/web/dms/contabilidad_queretaro/cfdi41/pdf/lib/Cfdi2Pdf.php'; //$this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
    $pdfTemplateDir = '/var/www/web/dms/contabilidad_queretaro/cfdi41/pdf/lib/templates/'; //$this->config->item('url_real').'cfdi/pdf/lib/templates/';

    $pdf = new Cfdi2Pdf($pdfTemplateDir);

    $pdf->tipo_factura = $datos_fac->tipo_factura;
    $pdf->tipo = $datos_fac->pdf_tipo;
    $pdf->serie = $datos_fac->pdf_serie;
    $pdf->modelo = $datos_fac->pdf_modelo;
    $pdf->placas = $datos_fac->pdf_placas;
    $pdf->color = $datos_fac->pdf_color;
    $pdf->km = $datos_fac->pdf_km;
    $pdf->no_economico = $datos_fac->pdf_no_economico;
    $pdf->orden = $datos_fac->pdf_orden;
    $pdf->version = $datos_fac->pdf_version;
    $pdf->fecha_recepcion = $datos_fac->pdf_fecha_recepcion;
    $pdf->asesor = $datos_fac->pdf_asesor;
    $pdf->transmision = $datos_fac->pdf_transmision;

    $pdf->no_inventario = $datos_fac->numero_economico; //$conceptos->no_inventario;
    $pdf->procedencia = $conceptos->procedencia;
    $pdf->no_puertas = $conceptos->no_puertas;
    $pdf->no_cilindros = $conceptos->cilindros;
    $pdf->capacidad = $conceptos->capacidad;
    $pdf->combustible = $conceptos->combustible;
    $pdf->motor = $conceptos->motor;
    $pdf->marca = $conceptos->marca;
    $pdf->linea = $conceptos->linea;
    $pdf->receptor_direccion = $datos_fac->receptor_direccion;
    $pdf->regimen_fiscal = $datos_fac->RegimenFiscalReceptor;

    $pdf->pedido = $datos_fac->pedido;
    $pdf->vendedor_nombre = $datos_fac->vendedor_clave . ' ' . $datos_fac->vendedor_nombre;
    $pdf->vendedor_clave = $datos_fac->vendedor_clave;


    $pdf->clave_vehicular = $conceptos->clave_vehicular;
    $pdf->niv = $conceptos->niv;
    $pdf->numero = $conceptos->numero;
    $pdf->fecha = $conceptos->fecha;
    $pdf->aduana = $conceptos->aduana;


    // datos del archivo. No se mostrarán en el PDF (requeridos)
    $pdf->autor  = 'www.fordmylsaqueretaro.mx/';
    $pdf->titulo = 'Factura';
    $pdf->asunto = 'CFDI';

    // texto a mostrar en la parte superior (requerido)
    $pdf->encabezado = 'Mylsa Querétaro'; //$datas_empresa->nombre;

    // nombre del archivo PDF (opcional)
    $pdf->nombreArchivo = 'pdf-cfdi.pdf';

    // mensaje a mostrar en el pie de pagina (opcional)
    $pdf->piePagina = 'Factura';

    // texto libre a mostrar al final del documento (opcional)
    $pdf->mensajeFactura = ""; //$datos_cliente->comentario_extra;

    // Solo compatible con CFDI 3.3 (opcional)
    $pdf->direccionExpedicion = 'Av. Constituyentes No. 42 Ote, Villas del Sol Querétaro'; //$datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

    // ruta del logotipo (opcional)
    $pdf->logo = '/var/www/web/dms/contabilidad_queretaro/cfdi41/logo_mylsa.png'; //"/var/www/web/dms/contabilidad_queretaro/".$logo;//$this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

    // mensaje a mostrar encima del documento (opcional)
    // $pdf->mensajeSello = 'CANCELADO';

    // Cargar el XML desde un string...
    // $ok = $pdf->cargarCadenaXml($cadenaXml);

    // Cargar el XML desde un archivo...
    // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
    // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
    // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
    $archivoXml = "/var/www/web/dms/contabilidad_queretaro/statics/facturas/facturas_xml/$id_factura.xml"; //$this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

    $ok = $pdf->cargarArchivoXml($archivoXml);

    //echo $ok;


    if ($ok) {
      // Generar PDF para mostrar en el explorador o descargar
      $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

      // Guardar PDF en la ruta especificada
      // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
      // $ok = $pdf->guardarPdf($ruta);

      // Obtener PDF como string
      // $pdfStr = $pdf->obtenerPdf();

      if ($ok) {
        // PDF generado correctamente.
      } else {
        echo 'Error al generar PDF.';
      }
    } else {
      echo 'Error al cargar archivo XML.';
    }
    exit();
  }


  /*
      * guarda toma 
      */
  public function guarda_toma($id_factura)
  {
    //$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
    //$response = validate($this);
    $response['status'] = true;

    if ($response['status']) {

      $data['id_factura'] = $id_factura;
      $data['factura_id'] = "";
      $data['toma_monto_adquisicion'] = $this->input->post('toma_monto_adquisicion');
      $data['toma_monto_enajenacion'] = $this->input->post('toma_monto_enajenacion');
      $data['toma_clave_vehicular'] = $this->input->post('toma_clave_vehicular');
      $data['toma_marca'] = $this->input->post('toma_marca');
      $data['toma_tipo'] = $this->input->post('toma_tipo');
      $data['toma_modelo'] = $this->input->post('toma_modelo');
      $data['toma_numero_motor'] = $this->input->post('toma_numero_motor');
      $data['toma_niv'] = $this->input->post('toma_niv');
      $data['toma_valor'] = $this->input->post('toma_valor');
      $data['numero'] = $this->input->post('numero');
      $data['fecha'] = $this->input->post('fecha');
      $data['aduana'] = $this->input->post('aduana');
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      $data_aux['enajenacion'] = 1;
      $this->Mgeneral->update_table_row('factura_vehiculo', $data_aux, 'facturaID', $id_factura);

      if (!empty($this->input->post('id_toma'))) {
        $id_toma = $this->input->post('id_toma');
        $this->Mgeneral->update_table_row('factura_toma_auto', $data_aux, 'id', $id_toma);
      } else {
        $id_toma = $this->input->post('id_toma');
        $this->Mgeneral->save_register('factura_toma_auto', $data);
      }
    }
    //  echo $response;
    echo json_encode(array('output' => $response));
    exit();
  }

  public function enviar_a_contabilidad_intercambio($id_factura, $personas, $kilogramos, $tipo_auto, $combustible)
  {

    $concepto  = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    $linea_autos  = $this->Mgeneral->get_row('id', $concepto->id_linea, 'linea_autos');
    $iva_total = (float) round(($concepto->precio_venta / 1.16) * .16, 2);
    $base_iva = (float) round($concepto->precio_venta / 1.16, 2);

    /*$isan = 0;
        if(($tipo_auto == "A") && ($combustible == 'GASOLINA') && ($personas < 15 )){
         $isan =  $this->calcula_isan_autos($base_iva);
        }

        if(($tipo_auto == "C") && ($combustible == 'GASOLINA') && ($kilogramos < 4250 )){
         $isan =  $this->calcula_isan_camiones($base_iva);
        }
        */


    //$total_cuenta1 = $base_iva - $isan;
    $total_cuenta1 = $base_iva;

    $costo_unidad = $concepto->c_valor_unidad + $concepto->c_equipo_base + $concepto->c_gastos_traslado + $concepto->c_plan_piso;


    $total_cuenta1 = $costo_unidad - 750;



    //en folio se puso el economico

    //tipo = 1 abono, 2 cargo
    //cuenta venta de unidad. sale de las lineas que se dan de 

    $this->contabilidad('VN', 1, 825, $base_iva, 'VENTAS INTERCAMBIOS', 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta clienetes 110001
    $this->contabilidad('VN', 2, 40, $iva_total + $base_iva, $this->Mgeneral->get_row('id', 40, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    // cuenta iva facturado 240013
    $this->contabilidad('VN', 1, 411, $iva_total, $this->Mgeneral->get_row('id', 411, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta inventario unidad. salen de las linea que se da de alta
    $this->contabilidad('VN', 1, $linea_autos->cuenta_inventario, $total_cuenta1/*$base_iva*/, $this->Mgeneral->get_row('id', $linea_autos->cuenta_inventario, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta costo unidad. sale de la linea que se da de alta
    $this->contabilidad('VN', 2, 1274, $costo_unidad, 'COSTO INTERCAMBIOS', 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta incentivos , esta no va, hasta despues 110010
    // $this->contabilidad('VN',1 ,$cuenta,$total,$concepto,$departamento,'APLICADO',$folio,'',1);
    //cuenta isan 240006
    //$this->contabilidad('VN',1 ,404,$isan,$this->Mgeneral->get_row('id',404,'cuentas_dms')->decripcion,17,'APLICADO','VN-'.$concepto->economico,'',1);

    //FALTA VALIDAR SI ES INTERCAMBIO
    // cuenta acondicionamiento esto es en todos menos en intercambios
    $this->contabilidad('VN', 1, 2842, 750, '2842', 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
  }

  public function enviar_a_contabilidad($id_factura, $personas, $kilogramos, $tipo_auto, $combustible)
  {

    $concepto  = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    $linea_autos  = $this->Mgeneral->get_row('id', $concepto->id_linea, 'linea_autos');
    $iva_total = (float) round(($concepto->precio_venta / 1.16) * .16, 2);
    $base_iva = (float) round($concepto->precio_venta / 1.16, 2);
    $isan = 0;
    if (($tipo_auto == "A") && ($combustible == 'GASOLINA') && ($personas < 15)) {
      $isan =  $this->calcula_isan_autos($base_iva);
    }

    if (($tipo_auto == "C") && ($combustible == 'GASOLINA') && ($kilogramos < 4250)) {
      $isan =  $this->calcula_isan_camiones($base_iva);
    }


    $total_cuenta1 = $base_iva - $isan;

    $total_cuenta1 = $total_cuenta1 - 750;

    $costo_unidad = $concepto->c_valor_unidad + $concepto->c_equipo_base + $concepto->c_gastos_traslado + $concepto->c_plan_piso;

    //en folio se puso el economico

    //tipo = 1 abono, 2 cargo
    //cuenta venta de unidad. sale de las lineas que se dan de 

    $this->contabilidad('VN', 1, $linea_autos->cuenta_ventas, $total_cuenta1, $this->Mgeneral->get_row('id', $linea_autos->cuenta_ventas, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta clienetes 110001
    $this->contabilidad('VN', 2, 40, $iva_total + $base_iva, $this->Mgeneral->get_row('id', 40, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    // cuenta iva facturado 240013
    $this->contabilidad('VN', 1, 411, $iva_total, $this->Mgeneral->get_row('id', 411, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta inventario unidad. salen de las linea que se da de alta
    $this->contabilidad('VN', 1, $linea_autos->cuenta_inventario, $costo_unidad/*$base_iva*/, $this->Mgeneral->get_row('id', $linea_autos->cuenta_inventario, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta costo unidad. sale de la linea que se da de alta
    $this->contabilidad('VN', 2, $linea_autos->cuenta_costo, $costo_unidad, $this->Mgeneral->get_row('id', $linea_autos->cuenta_costo, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
    //cuenta incentivos , esta no va, hasta despues 110010
    // $this->contabilidad('VN',1 ,$cuenta,$total,$concepto,$departamento,'APLICADO',$folio,'',1);
    //cuenta isan 240006
    $this->contabilidad('VN', 1, 404, $isan, $this->Mgeneral->get_row('id', 404, 'cuentas_dms')->decripcion, 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);

    //FALTA VALIDAR SI ES INTERCAMBIO
    // cuenta acondicionamiento esto es en todos menos en intercambios
    $this->contabilidad('VN', 1, 2842, 750, '2842', 17, 'APLICADO', 'VN-' . $concepto->economico, '', 1);
  }

  public function enviar_a_contabilidad_seminuevos($id_factura, $tipo_auto, $tipo_persona)
  {

    $conceptos  = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');

    $factura  = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');

    $iva_total = (float) round(($conceptos->precio_venta / 1.16)  * .16, 2);

    $base_iva = (float) round($conceptos->precio_venta / 1.16, 2);

    //CAMIONES GRABADOS PERSONA MORA Y PERSONA FISICA CON ACTIVIDAD EMPRESARIAL
    if ((($tipo_persona == 2) || ($tipo_persona == 3)) && ($tipo_auto == 1)) {

      $comision = $conceptos->precio_venta - $factura->total_compra_seminuevo;
      $comision_sin_iva =  $comision / 1.16;
      $total_real_sin_iva = $conceptos->precio_venta - ($comision_sin_iva * .16);
      $iva_total =  (float) round($comision_sin_iva * .16, 2);
      $base_iva = (float) round($comision, 2);



      //tipo = 1 abono, 2 cargo
      //41 = 110002 CLIENETS USADOS , TOTAL CON IVA
      $this->contabilidad('VU', 2, 41, $iva_total + $base_iva, 'CLIENETS USADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //830 = 320004 VENTA CAMIONES GRABADOS , TOTAL SIN IVA
      $this->contabilidad('VU', 1, 830, $base_iva, 'VENTA CAMIONES GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //411 = 240013 IVA FACTURADO, IVA
      $this->contabilidad('VU', 1, 411, $iva_total, 'IVA FACTURADO', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //1278 = 420003 COSTO CAMIONES GRABADOS, 
      $this->contabilidad('VU', 2, 1278, $factura->total_compra_seminuevo, 'COSTO CAMIONES GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //121 = 122004 INVENTARIO CAMIONES GRABADOS, 
      $this->contabilidad('VU', 1, 121, $factura->total_compra_seminuevo, 'INVENTARIO CAMIONES GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
    }
    //CAMIONES EXCENTOS PERSONA FISICA
    if ((($tipo_persona == 1)) && ($tipo_auto == 1)) {
      $iva_total = (float) round(($conce->precio_venta / 1.16) * .16, 2);
      $base_iva = (float) round($conce->precio_venta / 1.16, 2);
      //tipo = 1 abono, 2 cargo
      //41 = 110002 CLIENETS USADOS , TOTAL CON IVA
      $this->contabilidad('VU', 2, 41, $iva_total + $base_iva, 'CLIENETS USADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //829 = 320003 VENTA CAMIONES EXCENTOS , TOTAL SIN IVA
      $this->contabilidad('VU', 1, 829, $base_iva, 'VENTAS CAMIONES EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //411 = 240013 IVA FACTURADO, IVA
      $this->contabilidad('VU', 1, 411, $iva_total, 'IVA FACTURADO', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //1278 = 420003 COSTO CAMIONES EXCENTOS, 
      $this->contabilidad('VU', 2, 1278, $factura->total_compra_seminuevo, 'COSTO CAMIONES EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //120 = 122003 INVENTARIO CAMIONES EXCENTOS, 
      $this->contabilidad('VU', 1, 121, $factura->total_compra_seminuevo, 'INVENTARIO CAMIONES EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
    }

    //AUTOS GRABADOS PERSONA MORA Y PERSONA FISICA CON ACTIVIDAD EMPRESARIAL
    if ((($tipo_persona == 2) || ($tipo_persona == 3)) && ($tipo_auto == 2)) {

      $comision = $conceptos->precio_venta - $factura->total_compra_seminuevo;
      $comision_sin_iva =  $comision / 1.16;
      $total_real_sin_iva = $conceptos->precio_venta - ($comision_sin_iva * .16);
      $iva_total =  (float) round($comision_sin_iva * .16, 2);
      $base_iva = (float) round($comision, 2);
      //tipo = 1 abono, 2 cargo
      //41 = 110002 CLIENETS USADOS , TOTAL CON IVA
      $this->contabilidad('VU', 2, 41, $iva_total + $base_iva, 'CLIENETS USADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //828 = 320002 VENTA AUTOS GRABADOS , TOTAL SIN IVA
      $this->contabilidad('VU', 1, 828, $base_iva, 'VENTAS AUTOS GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //411 = 240013 IVA FACTURADO, IVA
      $this->contabilidad('VU', 1, 411, $iva_total, 'IVA FACTURADO', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //1277 = 420002 COSTO AUTOS GRABADOS, 
      $this->contabilidad('VU', 2, 1277, $factura->total_compra_seminuevo, 'COSTO AUTOS GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //119 = 122003 INVENTARIO AUTOS GRABADOS, 
      $this->contabilidad('VU', 1, 119, $factura->total_compra_seminuevo, 'INVENTARIO AUTOS GRABADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
    }
    //AUTOS EXCENTOS PERSONA FISICA
    if ((($tipo_persona == 1)) && ($tipo_auto == 2)) {

      $iva_total = (float) round(($conce->precio_venta / 1.16) * .16, 2);
      $base_iva = (float) round($conce->precio_venta / 1.16, 2);

      //tipo = 1 abono, 2 cargo
      //41 = 110002 CLIENETS USADOS , TOTAL CON IVA
      $this->contabilidad('VU', 2, 41, $iva_total + $base_iva, 'CLIENETS USADOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //827 = 320001 VENTA AUTOS EXCENTOS , TOTAL SIN IVA
      $this->contabilidad('VU', 1, 827, $base_iva, 'VENTAS AUTOS EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //411 = 240013 IVA FACTURADO, IVA
      $this->contabilidad('VU', 1, 411, $iva_total, 'IVA FACTURADO', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //1276 = 420002 COSTO AUTOS EXCENTOS, 
      $this->contabilidad('VU', 2, 1276, $factura->total_compra_seminuevo, 'COSTO AUTOS EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
      //118 = 122003 INVENTARIO AUTOS EXCENTOS, 
      $this->contabilidad('VU', 1, 118, $factura->total_compra_seminuevo, 'INVENTARIO AUTOS EXCENTOS', 18, 'APLICADO', 'VU-' . $conceptos->economico, '', 1);
    }
  }

  public function contabilidad($clave_poliza, $tipo, $cuenta, $total, $concepto, $departamento, $estatus, $folio, $cliente_id, $sucursal_id)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/agregar");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "clave_poliza=" . $clave_poliza . "&tipo=" . $tipo . "&cuenta=" . $cuenta . "&total=" . $total . "&concepto=" . $concepto . "&departamento=" . $departamento . "&estatus=" . $estatus . "&folio=" . $folio . "&cliente_id=" . $cliente_id . "&sucursal_id=" . $sucursal_id . ""
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);
  }

  public function enviar_id_factura($id_solicitud, $id_factura)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://mylsa.iplaneacion.com/dms/autos_nuevos_queretaro/ventas/ventas_api/api/numeroFactura");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "venta_id=" . $id_solicitud . "&numero_factura=" . $id_factura . ""
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);
  }

  public function enviar_id_factura_seminuevo($id_solicitud, $id_factura)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/autos_nuevos_queretaro/seminuevos/api/automoviles_facturas',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'estatus=1&factura=' . $id_factura . '&remision=' . $id_solicitud . '',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ci_session=t2lvq3aiqsk6lv3642dj55rps36gud53'
      ),
    ));

    $response = curl_exec($curl);
  }



  public function reporte_general($ipo_auto)
  {

    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);

    $titulo['titulo'] = "Lista de Facturas";
    $titulo['titulo_dos'] = "Lista de facturas";
    $datos['row'] = $this->Mgeneral->get_row('id', 1, 'informacion');

    $this->db->where('tipo_auto', $ipo_auto);
    $res_fac = $this->db->get('factura_vehiculo')->result();
    $datos['facturas'] = $res_fac;
    $datos['filtro'] = 0; //$filtro;
    $datos['index'] = '';
    $this->blade->render('reporte_general', $datos);
  }

  public function pdf_reporte_general($tipo_auto)
  {
    if ($tipo_auto == 1) {
      //ini_set('display_errors', 1);
      //ini_set('display_startup_errors', 1);
      // error_reporting(E_ALL);

      require_once 'cfdi41/pdf/lib/vendor/autoload.php';
      setlocale(LC_ALL, "es_ES");
      $fecha = date('Y/m/d');
      $fecha = str_replace("/", "-", $fecha);
      $newDate = date("d-m-Y", strtotime($fecha));
      $mesDesc = strftime("%B de %Y", strtotime($newDate));

      $datos['fecha'] = date('d') . ' de ' . $mesDesc;
      $datos['registro_federal'] = '';

      $datos['leyenda'] = "";

      $datos['row'] = $this->Mgeneral->get_row('id', 1, 'informacion');

      $this->db->where('tipo_auto', $tipo_auto);
      $res_fac = $this->db->get('factura_vehiculo')->result();
      $datos['facturas'] = $res_fac;
      $datos['filtro'] = 0; //$filtro;
      $datos['index'] = '';

      $html = $this->load->view('facturacion_vehiculos/pdf/reporte_general', $datos, true);
      $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
      $html2pdf->pdf->SetDisplayMode('fullpage');
      $html2pdf->writeHTML($html);
      ob_end_clean();
      $html2pdf->output('reporte_general.pdf', 'I');
      exit();
    }
  }

  public function calcula_isan_autos($subtotal)
  {
    $isan = 0;
    $tabla = $this->Mgeneral->get_table('tabla_isan');
    foreach ($tabla as $row) :
      if (($subtotal > $row->limite_inferior) && ($subtotal < $row->limite_superior)) {
        $primera_resta = $subtotal -  $row->cuota_fija;
        $segunda_resta = $primera_resta - $row->limite_inferior;
        $dividir = $row->tasa_para_aplicarse / 100;
        $dividir2 = ($row->tasa_para_aplicarse / 100) + 1;
        $dividir3 = $dividir / $dividir2;
        $tercera_operacion = $segunda_resta * $dividir3;
        $isan = $tercera_operacion + $row->cuota_fija;
      }
    endforeach;
    return $isan;
  }

  public function calcula_isan_camiones($subtotal)
  {
    $isan = 0;
    $operacion = $subtotal / 1.05;
    $isan = $operacion * .05;
    return $isan;
  }

  public function pruebas_isan()
  {
    $id_factura = "3F7DA58860D59B9E54F6E64DF626AE21";
    $personas = "5";
    $kilogramos = "0";
    $tipo_auto = "A";
    $combustible = "GASOLINA";

    $concepto  = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    $linea_autos  = $this->Mgeneral->get_row('id', $concepto->id_linea, 'linea_autos');
    $iva_total = (float) round(($concepto->precio_venta / 1.16) * .16, 2);
    $base_iva = (float) round($concepto->precio_venta / 1.16, 2);
    $isan = 0;
    if (($tipo_auto == "A") && ($combustible == 'GASOLINA') && ($personas < 15)) {
      $isan =  $this->calcula_isan_autos($base_iva);
    }

    if (($tipo_auto == "C") && ($combustible == 'GASOLINA') && ($kilogramos < 4250)) {
      $isan =  $this->calcula_isan_camiones($base_iva);
    }
    echo $isan;

    $total_cuenta1 = 0;

    if ($isan > 0) {
      $total_cuenta1 = $base_iva - $isan;
    } else {
      $total_cuenta1 = $base_iva;
    }

    echo "</br>";
    echo $total_cuenta1;
  }


  //BUSQUEDA 
  public function empList()
  {

    // POST data
    $postData = $this->input->post();

    // Get data
    $data = $this->Mgeneral->busqueda_clave_serv_prod($postData);

    echo json_encode($data);
    exit();
  }

  public function empList2()
  {

    // POST data
    $postData = $this->input->post();

    // Get data
    $data = $this->Mgeneral->busqueda_clave_unidad($postData);

    echo json_encode($data);
    exit();
  }


  public function cambiar_asientos($folio, $tipo)
  {
    if ($tipo == 2) {
      $tipo = 18;
    }
    if ($tipo == 1) {
      $tipo = 17;
    }

    $url = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/detalle?departamento=' . $tipo . '&folio=';
    $dataFromApi = $this->curl->curlGet($url . $folio . '', true);
    $obj = json_decode($dataFromApi);
    $asientos = $obj->data;
    $datos['asientos'] = $asientos;
    $datos['index'] = '';
    $datos['folio'] = $folio;
    $datos['departamento'] = $tipo;

    $this->blade->render('cambiar_asientos', $datos);
  }

  public function empList3()
  {

    // POST data
    $postData = $this->input->post();

    // Get data
    $data = $this->Mgeneral->busqueda_asientos($postData);

    echo json_encode($data);
    exit();
  }

  public function guarda_asientos_folio($folio, $departamento)
  {

    // se anula el folio de contabilidad manando el folio
    $sucursal = 1; // esta es queretaro, la 2 es san juan
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/aplicar_folio',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'estatus=ANULADO&folio=' . $folio . '&sucursal_id=' . $sucursal . '',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ci_session=t2lvq3aiqsk6lv3642dj55rps36gud53'
      ),
    ));
    $response = curl_exec($curl);

    $post = $this->input->post('save');
    $clave_poliza = 0;
    if ($departamento == 17) {
      $clave_poliza = 'VN';
    }
    if ($departamento == 18) {
      $clave_poliza = 'VU';
    }

    foreach ($post as $key) :
      $cuenta_dms = $this->Mgeneral->get_row('cuenta', $key['cuenta'][0], 'cuentas_dms');
      $this->contabilidad($clave_poliza, $tipo, $cuenta_dms->id, $total, $cuenta_dms->decripcion, $departamento, 'APLICADO', $folio, '', $sucursal);
      echo $key['cuenta'][0];
      echo "/";
      echo $key['cargo'][0];
      echo "/";
      echo $key['abono'][0];
      echo "<br/>";


    endforeach;
    //echo json_encode($post);
    exit();
  }





  public function generar_factura_api($id_factura)
  {

    //        ini_set('display_errors', -1);

    //error_reporting(E_ALL);

    $this->load->model('MfacturaVehiculo', '', TRUE);
    $factura_nueva = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura_vehiculo');
    $conceptos = $this->Mgeneral->get_row('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');


    $cfdi = $this->MfacturaVehiculo;
    $cfdi->cargarInicio();
    $cfdi->cabecera($factura_nueva->tipo_auto, $factura_nueva->enajenacion);

    $cfdi->fact_serie        = $factura_nueva->factura_serie;                             // 4.1 Número de serie.
    $cfdi->fact_folio        = $factura_nueva->factura_folio; //mt_rand(1000, 9999);             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
    $cfdi->NoFac             = $cfdi->fact_serie . $cfdi->fact_folio;         // 4.3 Serie de la factura concatenado con el número de folio.
    $cfdi->fact_tipcompr     = $factura_nueva->factura_tipoComprobante;                             // 4.4 Tipo de comprobante.
    $cfdi->fact_exportacion  = "01";                            // 4.5 Atributo requerido para expresar si el comprobante ampara una operación de exportación.
    $cfdi->tasa_iva          = 16;
    $cfdi->subTotal          = $factura_nueva->subtotal; //$this->redondeo( $this->Mgeneral->factura_subtotal2_seminuevo_subtotal($id_factura), 2, '.', '');//$factura_nueva->subtotal;//$this->Mgeneral->factura_subtotal($id_factura);                              // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    $cfdi->descuento         = 0;                               // 4.8 Descuento (se calculan mas abajo).
    $cfdi->IVA               = $factura_nueva->iva; // $this->redondeo($this->Mgeneral->factura_iva_total2_seminuevo($id_factura), 2, '.', ''); //$factura_nueva->iva;//$this->Mgeneral->factura_iva_total($id_factura);                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
    $cfdi->total             = $factura_nueva->total; // $this->redondeo($this->Mgeneral->factura_subtotal2_seminuevo($id_factura), 2)  ;     //$factura_nueva->total;//$this->truncateFloat($this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura), 2)  ;                            // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    $cfdi->fecha_fact        = date("Y-m-d") . "T" . date("H:i:s"); // 4.11 Fecha y hora de facturación.
    $cfdi->NumCtaPago        = "";                          // 4.12 Número de cuenta (sólo últimos 4 dígitos, opcional).
    $cfdi->condicionesDePago = "";                   // 4.13 Condiciones de pago.
    $cfdi->formaDePago       = $factura_nueva->factura_formaPago;                            // 4.14 Forma de pago.
    $cfdi->metodoDePago      = $factura_nueva->factura_medotoPago;                           // 4.15 Clave del método de pago. Consultar catálogos de métodos de pago del SAT.
    $cfdi->TipoCambio        = 1;                               // 4.16 Tipo de cambio de la moneda.
    $cfdi->LugarExpedicion   = '66260'; ///$factura_nueva->fatura_lugarExpedicion;                         // 4.17 Lugar de expedición (código postal).
    $cfdi->moneda            = 'MXN'; //$factura_nueva->factura_moneda;                           // 4.18 Moneda
    $cfdi->totalImpuestosRetenidos   = 0;                       // 4.19 Total de impuestos retenidos (se calculan mas abajo).
    $cfdi->totalImpuestosTrasladados = 0;
    //CP 45079
    $cfdi->datosGenerales();



    ### 9. DATOS GENERALES DEL EMISOR #################################################  
    $cfdi->emisor_rs = "ESCUELA KEMPER URGATE";  // 9.1 Nombre o Razón social.
    $cfdi->emisor_rfc = "EKU9003173C9";  // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
    $cfdi->emisor_ClaRegFis = "601"; // 9.3 Clave del Régimen fiscal.  
    $cfdi->datosEmisor();

    ### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
    $cfdi->receptor_rfc = "MASO451221PM4";  // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
    $cfdi->receptor_rs  = "MARIA OLIVIA MARTINEZ SAGAZ";  // 10.4 Nombre o razón social.
    $cfdi->DomicilioFiscalReceptor = "80290";             // 10.5 Domicilio fiscal del Receptor (código postal).
    $cfdi->RegimenFiscalReceptor = "616";                 // 10.6 Régimen fiscal del receptor.
    $cfdi->UsoCFDI = "S01";                               // Uso del CFDI.
    $cfdi->datosReceptor();



    $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId', $id_factura, 'factura_conceptos_vehiculo');
    foreach ($conceptos_factura as $concepto_fac) :


      $concepto = array();
      $concepto['ClaveProdServ'] = $concepto_fac->clave_sat;
      $concepto['NoIdentificacion'] = $concepto_fac->niv;
      $concepto['Cantidad'] = 1;
      $concepto['ClaveUnidad'] = $concepto_fac->unidad_sat;
      //$unidad_s = $this->Mgeneral->get_row('ClaveUnidad',$concepto_fac->unidad_sat,'ca_claveunidad');
      $concepto['Unidad'] = $concepto_fac->concepto_unidad; //$concepto_fac->id_producto_servicio_interno;
      $concepto['Descripcion'] = $concepto_fac->concepto_nombre;
      $concepto['ValorUnitario'] = $concepto_fac->concepto_precio;
      $concepto['Importe'] = $concepto_fac->concepto_importe;




      $concepto['Descuento'] = 0;
      $concepto['ObjetoImp'] = '02';
      $concepto['version'] = '1.1';
      $concepto['ClaveVehicular'] = $concepto_fac->clave_vehicular;
      $concepto['Niv'] = $concepto_fac->niv;
      $concepto['numero'] = $concepto_fac->numero;
      $concepto['fecha'] = $concepto_fac->fecha;
      $concepto['aduana'] = $concepto_fac->aduana;

      $concepto['nacional_importado'] = $factura_nueva->unidad_importada;



      $impuesto = array();
      $impuesto['TipoFactor'] = "Tasa";
      $impuesto['Base'] = $concepto_fac->Base;
      $impuesto['Impuesto'] = '002';
      $impuesto['TipoFactor'] = 'Tasa';
      $impuesto['TasaOCuota'] = '0.160000';
      $impuesto['Importe'] = $concepto_fac->importe_iva; //$this->redondeo($concepto_fac->concepto_importe * .16, 2, '.', '');//$concepto_fac->importe_iva;
      $concepto['tipo_auto'] = $factura_nueva->tipo_auto;

      $concepto['traslado'] = $impuesto;
      $cfdi->conceptos($concepto); // agrega los conceptos y los impuestos




    endforeach;


    $cfdi->impuestosGeneral(); // agrega impuestos generales

    $toma_vehiculo = $this->Mgeneral->get_row('id_factura', $id_factura, 'factura_toma_auto');
    $enajenacion = 0;
    $data_enajenacion = array();
    if (is_object($toma_vehiculo)) {
      $data_enajenacion['Version'] = "1.0";
      $data_enajenacion['montoAdquisicion'] = $toma_vehiculo->toma_monto_adquisicion;
      $data_enajenacion['montoEnajenacion'] = $toma_vehiculo->toma_monto_enajenacion;
      $data_enajenacion['claveVehicular'] = $toma_vehiculo->toma_clave_vehicular;
      $data_enajenacion['marca'] = $toma_vehiculo->toma_marca;
      $data_enajenacion['tipo'] = $toma_vehiculo->toma_tipo;
      $data_enajenacion['modelo'] = $toma_vehiculo->toma_modelo;
      $data_enajenacion['numeroMotor'] = $toma_vehiculo->toma_numero_motor;
      $data_enajenacion['NIV'] = $toma_vehiculo->toma_niv;
      $data_enajenacion['valor'] = $toma_vehiculo->toma_valor;
      $data_enajenacion['numero'] = $toma_vehiculo->numero;
      $data_enajenacion['fecha'] = $toma_vehiculo->fecha;
      $data_enajenacion['aduana'] = $toma_vehiculo->aduana;
      $enajenacion = 1;
    }
    $cfdi->agregar_complemento($data_enajenacion, $enajenacion);

    $cfdi->sellar();





    //$cfdi->MfacturaVehiculo->obtenerXml();
    //var_dump($cfdi->obtenerXml());
    //echo $cfdi->obtenerXml();
    //print_r($cfdi->obtenerXml());
    //die();





    if ($cfdi->obtenerXml()) {
      //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
      //file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      file_put_contents("/var/www/web/dms/contabilidad_queretaro/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      //file_put_contents("https://planificadorempresarial.com/facturacion/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      $data_url_prefactura['url_prefactura'] = base_url() . "statics/facturas/prefactura/$id_factura.xml";
      $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
      $this->Mgeneral->update_table_row('factura_vehiculo', $data_url_prefactura, 'facturaID', $id_factura);
      $this->enviar_factura($id_factura);
      //echo $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";
    } else {
      $data_respone['error'] = 1;
      $data_respone['error_mensaje'] = "Ocurrio un error";
      $data_respone['factura'] = $id_factura;


      echo json_encode($data_respone);
      die;
    }
    //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
    //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
    //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

    //$this->enviar_factura($id_factura);

    die;

    // Mostrar objeto que contiene los datos del CFDI
    print_r($cfdi);


    die;



    die('OK');
  }



  public function api_genera_factaura()
  {

    date_default_timezone_set('America/Mexico_City');
    ini_set('display_errors', -1);
    ini_set('display_startup_errors', -1);
    error_reporting(E_ALL);

    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $factura_campo = json_decode($json);



    $emisor = $this->Mgeneral->get_row('id', 1, 'datos');
    $id_factura = get_guid();
    $data['facturaID'] = $id_factura;
    $data['status'] = 1;
    $data['emisor_RFC'] = $emisor->rfc;
    $data['emisor_regimenFiscal'] = $emisor->regimen;
    $data['emisor_nombre'] = $emisor->razon_social;
    $data['receptor_uso_CFDI'] = "G03";
    $data['pagada'] = "SI";

    //$data['sucursal'] = $this->validar_capo_json($factura_campo->sucursal);
    //$data['tipo'] = $this->validar_capo_json($factura_campo->tipo);
    $this->Mgeneral->save_register('factura_vehiculo', $data);


    $data_datos['tipo_auto'] = $factura_campo->tipo_auto; //nuevo 1, usado 2
    $data_datos['enajenacion'] = $factura_campo->enajenacion; // 1 con toma, 0 sin toma

    // var_dump($factura_campo->enajenacion_auto);


    if ($factura_campo->enajenacion == 1) :
      foreach ($factura_campo->enajenacion_auto as $enaje_au) :


        $data_toma['id_factura'] = $id_factura;
        $data_toma['factura_id'] = "";
        $data_toma['toma_monto_adquisicion'] = $enaje_au->toma_monto_adquisicion;
        $data_toma['toma_monto_enajenacion'] = $enaje_au->toma_monto_enajenacion;
        $data_toma['toma_clave_vehicular'] = $enaje_au->toma_clave_vehicular;
        $data_toma['toma_marca'] = $enaje_au->toma_marca;
        $data_toma['toma_tipo'] = $enaje_au->toma_tipo;
        $data_toma['toma_modelo'] = $enaje_au->toma_modelo;
        $data_toma['toma_numero_motor'] = $enaje_au->toma_numero_motor;
        $data_toma['toma_niv'] = $enaje_au->toma_niv;
        $data_toma['toma_valor'] = $enaje_au->toma_valor;
        $data_toma['numero'] = $enaje_au->numero;
        $data_toma['fecha'] = $enaje_au->fecha;
        $data_toma['aduana'] = $enaje_au->aduana;
        $data_toma['fecha_creacion'] = date('Y-m-d H:i:s');
        $this->Mgeneral->save_register('factura_toma_auto', $data_toma);

      endforeach;
    endif;



    $data_datos['receptor_nombre'] = $this->validar_capo_json($factura_campo->receptor_nombre, 'receptor_nombre');
    //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
    $data_datos['factura_moneda'] = 'MXN'; //$this->validar_capo_json($factura_campo->moneda);
    $data_datos['fatura_lugarExpedicion'] = $this->validar_capo_json($factura_campo->LugarExpedicion, 'fatura_lugarExpedicion');
    $data_datos['factura_fecha'] = date('Y-m-d H:i:s'); //$this->input->post('numero');
    $data_datos['receptor_email'] =  $this->validar_capo_json($factura_campo->receptor_email, 'receptor_email');
    $data_datos['factura_folio'] = $this->validar_capo_json($factura_campo->Folio, 'factura_folio');
    $data_datos['factura_serie'] = $this->validar_capo_json($factura_campo->Serie, 'factura_serie');
    $data_datos['receptor_direccion'] = $this->validar_capo_json($factura_campo->receptor_direccion, 'receptor_direccion');
    $data_datos['factura_formaPago'] = $this->validar_capo_json($factura_campo->FormaPago, 'factura_formaPago');
    $data_datos['factura_medotoPago'] = $this->validar_capo_json($factura_campo->MetodoPago, 'MetodoPago');
    $data_datos['factura_tipoComprobante'] = $this->validar_capo_json($factura_campo->TipoDeComprobante, 'factura_tipoComprobante');
    $data_datos['receptor_RFC'] = $this->validar_capo_json($factura_campo->receptor_RFC, 'receptor_RFC');
    $data_datos['receptor_uso_CFDI'] = $this->validar_capo_json($factura_campo->receptor_uso_CFDI, 'receptor_uso_CFDI');
    //$data['pagada'] = $this->input->post('pagada');
    $data_datos['DomicilioFiscalReceptor'] = $this->validar_capo_json($factura_campo->DomicilioFiscalReceptor, 'DomicilioFiscalReceptor');             // 10.5 Domicilio fiscal del Receptor (código postal).
    $data_datos['RegimenFiscalReceptor'] = $this->validar_capo_json($factura_campo->RegimenFiscalReceptor, 'RegimenFiscalReceptor');
    //$data['pagada'] = $this->input->post('pagada');
    $data_datos['comentario'] = $this->validar_capo_json($factura_campo->comentario, 'comentario');
    $data_datos['descuento'] = $this->validar_capo_json($factura_campo->descuento, 'descuento');
    $data_datos['condicionesDePago'] = $this->validar_capo_json($factura_campo->condicionesDePago, 'condicionesDePago');
    $data_datos['NumCtaPago'] = $this->validar_capo_json($factura_campo->NumCtaPago, 'NumCtaPago');

    $data_datos['total'] = $factura_campo->total;
    $data_datos['subtotal'] = $factura_campo->subtotal;
    $data_datos['iva'] = $factura_campo->iva;


    //if ($factura_campo->tipo_factura == 1) :
    $data_datos['pdf_tipo'] = $factura_campo->Tipo;
    $data_datos['pdf_serie'] = $factura_campo->SerieA;
    $data_datos['pdf_modelo'] = $factura_campo->Modelo;
    $data_datos['pdf_placas'] = $factura_campo->Placas;
    $data_datos['pdf_color'] = $factura_campo->Color;
    $data_datos['pdf_km'] = $factura_campo->KM;
    //$data_datos['pdf_no_economico'] = $factura_campo->noEconomico;
    $data_datos['pdf_orden'] = $factura_campo->Orden;
    $data_datos['pdf_version'] = $factura_campo->Version;
    $data_datos['pdf_fecha_recepcion'] = $factura_campo->fecha_recepcion;
    $data_datos['pdf_asesor'] = $factura_campo->Asesor;
    $data_datos['pdf_transmision'] = $factura_campo->Transmision;
    $data_datos['tipo_factura'] = 1;
    $data_datos['unidad_importada'] = "si";
    //endif;



    $this->Mgeneral->update_table_row('factura_vehiculo', $data_datos, 'facturaID', $id_factura);




    foreach ($factura_campo->conceptos as $concepto_fac) :

      $data_conceptos['concepto_facturaId'] = $id_factura;
      $data_conceptos['concepto_NoIdentificacion'] = $this->validar_capo_json($concepto_fac->concepto_NoIdentificacion, 'concepto_NoIdentificacion');
      $data_conceptos['concepto_unidad'] = $this->validar_capo_json($concepto_fac->unidad_interna, 'unidad_interna');
      //$data_conceptos['concepto_descuento'] = $this->input->post('concepto_descuento');
      $data_conceptos['clave_sat'] = $this->validar_capo_json($concepto_fac->clave_sat, 'clave_sat');
      $data_conceptos['unidad_sat'] = $this->validar_capo_json($concepto_fac->unidad_sat, 'unidad_sat');
      // $data_conceptos['unidad_interna'] = $this->validar_capo_json($concepto_fac->unidad_interna, 'unidad_interna');
      $data_conceptos['concepto_nombre'] = $this->validar_capo_json($concepto_fac->concepto_nombre, 'concepto_nombre');
      $data_conceptos['concepto_precio'] = $this->validar_capo_json($concepto_fac->concepto_precio, 'concepto_precio');
      $data_conceptos['concepto_importe'] = $this->validar_capo_json($concepto_fac->concepto_importe, 'concepto_importe');
      $data_conceptos['impuesto_iva'] = $this->validar_capo_json($concepto_fac->concepto_importe, 'impuesto_iva');
      $data_conceptos['impuesto_iva_tipoFactor'] = 0; //$this->input->post('impuesto_iva_tipoFactor');
      $data_conceptos['impuesto_iva_tasaCuota'] = 0; //$this->input->post('impuesto_iva_tasaCuota');
      $data_conceptos['impuesto_ISR'] = 0; //$this->input->post('impuesto_ISR');
      $data_conceptos['impuesto_ISR_tasaFactor'] = $this->validar_capo_json($concepto_fac->TipoFactor, 'impuesto_ISR_tasaFactor');
      $data_conceptos['impuestoISR_tasaCuota'] = $this->validar_capo_json($concepto_fac->TasaOCuota, 'impuestoISR_tasaCuota');
      $data_conceptos['tipo'] = '0'; //$this->input->post('tipo');
      $data_conceptos['nombre_interno'] = $this->validar_capo_json($concepto_fac->concepto_nombre, 'nombre_interno');
      $data_conceptos['id_producto_servicio_interno'] = $this->validar_capo_json($concepto_fac->id_producto_servicio_interno, 'id_producto_servicio_interno');
      $data_conceptos['concepto_cantidad'] = $this->validar_capo_json($concepto_fac->concepto_cantidad, 'concepto_cantidad');
      $data_conceptos['importe_iva'] = $this->validar_capo_json($concepto_fac->Importe, 'Importe');
      $data_conceptos['fecha_creacion'] = date('Y-m-d H:i:s');
      $data_conceptos['ObjetoImp'] = $this->validar_capo_json($concepto_fac->ObjetoImp, 'ObjetoImp');
      $data_conceptos['Base'] = $this->validar_capo_json($concepto_fac->Base, 'Base');

      $data_conceptos['clave_vehicular'] = $this->validar_capo_json($concepto_fac->clave_vehicular, 'clave_vehicular');
      $data_conceptos['niv'] = $this->validar_capo_json($concepto_fac->niv, 'niv');
      $data_conceptos['numero'] = $this->validar_capo_json($concepto_fac->numero, 'numero');
      $data_conceptos['fecha'] = $this->validar_capo_json($concepto_fac->fecha, 'fecha');
      $data_conceptos['aduana'] = $this->validar_capo_json($concepto_fac->aduana, 'aduana');
      // $data_conceptos['importado'] = $this->validar_capo_json($concepto_fac->importado, 'importado');

      $this->Mgeneral->save_register('factura_conceptos_vehiculo', $data_conceptos);




    endforeach;


    $this->generar_factura_api($id_factura);
    //$this->generar_prefactura($id_factura);

  }


  function validar_capo_json($campo, $label)
  {
    try {
      if (!isset($campo)) {
        throw new Exception("Falta campo  => " . $label);
      }
      return $campo;
    }
    //catch exception
    catch (Exception $e) {
      echo 'Message: ' . $e->getMessage();
      die();
    }
  }
}
