@section('style')

<style>
    .color_negro{
        color:"#000" !important;
    }
</style>
    
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){

      



      $("#cargando").hide();
      $('#guarda_factura').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_datos/<?php echo $factura->facturaID; /*if(is_object($factura)){echo $factura->facturaID;}else{ echo 0;}*/?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "factura_moneda":$("#factura_moneda").val(),
                      "RegimenFiscalReceptor":$("#RegimenFiscalReceptor").val(),
                      "fatura_lugarExpedicion":$("#fatura_lugarExpedicion").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "receptor_direccion":$("#receptor_direccion").val(),
                      "factura_formaPago":$("#factura_formaPago").val(),
                      "factura_medotoPago":$("#factura_medotoPago").val(),
                      "factura_tipoComprobante":$("#factura_tipoComprobante").val(),
                      "receptor_RFC":$("#receptor_RFC").val(),
                      "receptor_uso_CFDI":$("#receptor_uso_CFDI").val(),
                      "pagada":$("#pagada").val(),
                      "almacen":$("#almacen").val(),
                      "comentario":$("#comentario").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/conceptos/"+obj_output.id_factura_auto);
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });


      

     /* $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>index.php/facturacion/ver_relaciones_facturas",
          enctype: 'multipart/form-data',
          datatype: "JSON",
        //async: asincrono,
          cache: false,
          data: {id_factura:"<?php echo $factura->facturaID?>"},
          statusCode: {
              200: function (result) {
                //console.log(result);
                 $("#relacionados").html("");
                 $("#relacionados").html(result);
                 $(".eliminar_relacion").click(function(event){
                      event.preventDefault();
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                      $.get(url);
                  });

              },
              401: code400,
              404: code404,
              500: code500,
              409: code409
          }
      });

        $("#agregar_relacion").click(function(){

          $.ajax({
              type: 'POST',
              url: "<?php echo base_url()?>index.php/facturacion/relaciones_facturas",
              enctype: 'multipart/form-data',
              datatype: "JSON",
            //async: asincrono,
              cache: false,
              data: {tipo_relacion:$("#id_relacion").val(),uuid:$("#uuid").val(),id_factura:"<?php echo $factura->facturaID?>"},
              statusCode: {
                  200: function (result) {
                    //console.log(result);
                     $("#relacionados").html("");
                     $("#relacionados").html(result);
                     $(".eliminar_relacion").click(function(event){
                          event.preventDefault();
                          id = $(event.currentTarget).attr('flag');
                          url = $("#delete"+id).attr('href');
                          $("#borrar_"+id).slideUp();
                          $.get(url);
                      });

                  },
                  401: code400,
                  404: code404,
                  500: code500,
                  409: code409
              }
          });


        });*/

    });

function seleccionar_cliente(id_cliente){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_cliente/"+id_cliente;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);

              $('#receptor_nombre').val(obj.nombre);
              

              $('#receptor_RFC').val(obj.rfc);
              $('#receptor_email').val(obj.email);
              $('#receptor_direccion').val(obj.ciudad);
              $('#receptor_id_cliente').val(obj.id);
              $('#factura_formaPago').val(obj.factura_formaPago);
              $('#receptor_uso_CFDI').val(obj.uso_CFDI);



              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}
</script>

@endsection
@section('contenido')



<div class="container-fuid">

<h4>
    <?php  if($factura->tipo_auto == 1){ echo "AUTO NUEVO"; }?>
    <?php  if($factura->tipo_auto == 2){ echo "AUTO SEMINUEVO"; }?>
</h4>

  <form action="" method="post" novalidate="novalidate" id="guarda_factura">

    <div class="row">

        <div class="col-4">

            <div class="form-group">
                <label for="factura_moneda" class="text-dark"> <span>Razón social cliente:</span></label>
                    <input id="receptor_nombre" name="receptor_nombre" type="text" class="form-control-sm alto form-control cc-exp" value="<?php  if(is_object($factura)){ echo $factura->receptor_nombre;}?>" >
                   
            </div>

        </div>

        <div class="col-1">

            <div class="form-group">
                <label for="email" class="text-dark">Folio:</label>
                    <input type="text" class="form-control-sm alto form-control" id="factura_folio" name="factura_folio" value="<?php  if(is_object($factura)){ echo $factura->factura_folio;}?>">
            </div>
        </div>

        <div class="col-1">
            <div class="form-group">
                <label for="email" class="text-dark">Serie:</label>
                <input type="text" class="form-control-sm alto form-control" id="factura_serie" name="factura_serie" value="<?php if(is_object($factura)){echo $factura->factura_serie;} ;?>">
            </div>
        </div>

        <div class="col-2">
            <div class="form-group">
                <label for="email" class="text-dark">Fecha:</label>
                    <input type="text" class="form-control-sm alto form-control" id="factura_fecha" name="factura_fecha" value="<?php echo date('Y-m-d H:i:s');?>">
                </div>
        </div>

    </div>

    <div class="row">

        <div class="col-3">
            <div class="form-group">
                <label for="receptor_RFC" class="text-dark">RFC:</label>
                <input id="receptor_RFC" name="receptor_RFC" type="text" class="form-control-sm alto form-control cc-exp" value="<?php  if(is_object($factura)){echo $factura->receptor_RFC;}?>" placeholder="RFC" >

            </div>
        </div>

        <div class="col-4">
            <div class="form-group">
                <label for="receptor_email" class="text-dark">Email:</label>
                <input id="receptor_email" name="receptor_email" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($factura)){echo $factura->receptor_email;} ?>" placeholder="Email" autocomplete="cc-exp">

            </div>
        </div>

        <div class="col-1">
            <div class="form-group">
                <label for="email" class="text-dark">CP:</label>
                <input type="text" class="form-control-sm alto form-control" id="fatura_lugarExpedicion" name="fatura_lugarExpedicion" value="<?php if(is_object($factura)){ echo $factura->fatura_lugarExpedicion;}?>">
            </div>
        </div>

        

       

    </div>
    <div class="row">

        <div class="col-8">
            <div class="form-group">
            <label for="email" class="text-dark">Dirección:</label>
            <input id="receptor_direccion" name="receptor_direccion" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($factura)){echo $factura->receptor_direccion;} ?>" placeholder="Razon social" autocomplete="cc-exp">


            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-2">
            <div class="form-group">
                <label for="factura_formaPago" class="text-dark">Forma de pago:</label>
                <select  class="form-control-sm alto form-control" name="factura_formaPago" id="factura_formaPago">
                                
                                <?php foreach($forma_pagos as $forma_p):?>
                                     <option value="<?php echo $forma_p->id;?>" <?php if(is_object($factura)){if($factura->factura_formaPago ==$forma_p->id){echo "selected";}}?> ><?php echo $forma_p->id;?>-<?php echo $forma_p->nombre;?></option>
                                <?php endforeach;?>

                              </select>


            </div>
        </div>

        <div class="col-2">
            <div class="form-group">
                <label for="factura_medotoPago" class="text-dark">Metodo pago:</label>
                <select class="form-control-sm alto form-control" id="factura_medotoPago" name="factura_medotoPago">
                    <?php foreach($metodo_pagos as $metodo_p):?>
                         <option value="<?php echo $metodo_p->id;?>" <?php if(is_object($factura)){if($factura->factura_medotoPago == $metodo_p->id){echo "selected";}}?> ><?php echo $metodo_p->id;?>-<?php echo $metodo_p->nombre;?></option>
                      <?php endforeach;?>
                    

                </select>
            </div>
        </div>

        





        <div class="col-2">
            <div class="form-group">
                <label for="receptor_RFC" class="text-dark">Tipo comprovante:</label>
                <select class="form-control-sm alto form-control" id="factura_tipoComprobante" name="factura_tipoComprobante" >
                      <?php foreach($tipo_comprobante as $tipo_c):?>
                        <option value="<?php echo $tipo_c->id;?>" <?php if(is_object($factura)){ if($factura->factura_tipoComprobante == $tipo_c->id){echo "selected";} }?> ><?php echo $tipo_c->id;?>-<?php echo $tipo_c->nombre;?></option>
                      <?php endforeach;?>      
                </select>
            </div>
        </div>




        <div class="col-2">
            <div class="form-group">
                <label for="receptor_uso_CFDI" class="text-dark">Uso del CFDI:</label>
                <select class="form-control-sm alto form-control" id="receptor_uso_CFDI" name="receptor_uso_CFDI" >
                  <?php foreach($uso_cfdi as $uso_c):?>
                    <option value="<?php echo $uso_c->id;?>" <?php if(is_object($factura)){ if($factura->receptor_uso_CFDI == $uso_c->id){echo "selected";} }?> ><?php echo $uso_c->id;?>-<?php echo $uso_c->nombre;?></option>
                  <?php endforeach;?>
                </select>
            </div>
        </div>
                      

    </div>

    <div class="row">
    <div class="col-4">
            <div class="form-group">
                <label for="RegimenFiscalReceptor" class="text-dark">Regimen fiscal:</label>
                <select class="form-control-sm alto form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" >
                  <?php foreach($regimen_fiscal as $regimen):?>
                    <option value="<?php echo $regimen->regimen;?>" <?php if(is_object($factura)){ if($factura->RegimenFiscalReceptor == $regimen->regimen){echo "selected";} }?> ><?php echo $regimen->regimen;?>-<?php echo $regimen->descripcion;?></option>
                  <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="col-1">
            <div class="form-group">
                <label for="factura_moneda" class="text-dark">Moneda:</label>
                <input type="text" class="form-control-sm alto form-control" id="factura_moneda" name="factura_moneda" value="<?php if(is_object($factura)){echo $factura->factura_moneda;}else{echo "MXN";} ?>" >
            </div>
        </div>
    
    </div>

    <hr/>
    <!--div><label class="text-dark">Agregar relaciones de CFDI</label></div>
    <div class="row">
      
            

            
            <div class="col-4">
                <div class="form-group">
                          <label for="comentario">Tipo relación:</label>
                          <select class="form-control-sm form-control" id="id_relacion">
                           <option value="01">01.-nota de credito de los documentos</opcion>
                           <option value="02">02.-nota de debito de los documtntos relacionados</opcion>
                           <option value="03">03.-devoclución de mercancía sobre facturas o traslados</opcion>
                           <option value="04">04.-sustituto de los CFDI previos</opcion>
                           <option value="05">05.-traslados de mercancías facturados previamente</opcion>
                           <option value="06">06.-factura generada por los traslados previos</opcion>
                           <option value="07">07.-CFDI por aplicación de anticipo</opcion>
                           <option value="08">08.-factura generada por pagos en parcialidades</opcion>
                           <option value="09">09.-factura generada por pagos diferidos</opcion>
                          </select>
                </div>
            </div>
           

            <div class="col-4">
                <div class="form-group">
                          <label for="comentario" class="text-dark">UUID:</label>
                          <input id="uuid" name="uuid" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="UUID" autocomplete="cc-exp">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="comentario" class="text-dark">Opción:</label><br/>
                    <button type="button" id="agregar_relacion" class="btn btn-outline-success">Agregar relación</button>
                </div>
            </div>
       
    
    </div>

    <div class="row" id="relacionados">
        <div class="col-12">
        </div>
    </div-->

    <hr/>
    <div class="" align="right">
            <button title="Editar" id="payment-button" type="submit" class="btn  btn-info ">
                <i class="fa fa-edit fa-lg"></i>&nbsp;
                <span id="payment-button-amount">Guardar y Siguiente</span>
            </button>      
        </div>
  </form>
</div>

@endsection