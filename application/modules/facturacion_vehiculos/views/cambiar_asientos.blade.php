@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')



<script src="<?php echo base_url()?>statics/js/isloading.js"></script>


<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        $('#empTable2').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?=base_url()?>index.php/facturacion_vehiculos/empList3'
          },
          'columns': [
           
             { data: 'id'},
             { data: 'cuenta' },
             { data: 'descripcion' },
             {
                "data": "opciones", "render": function (data) {
                       
                    botones = "";

                    botones += '<button type="button" class="btn btn-info" onclick="seleccionar_cuenta(\''+data.cuenta+'\')">seleccionar</button>';

                    return botones;
 
                }
            }
             

             
          ]
        });

        var suma_abono = 0;
        $('.abono').each(function(){
            suma_abono += parseFloat($(this).val());
        });
        $(".abono_total").val(suma_abono);

        var suma_cargo = 0;
        $('.cargo').each(function(){
            suma_cargo += parseFloat($(this).val());
        });
        $(".cargo_total").val(suma_cargo);

        $(".agregar_asiento").click(function(){
            html_tabla = '';
            html_tabla +='<th>';
            html_tabla +='<input type="text" value="" class="cuenta" />';
            html_tabla +='</th>';
            html_tabla +='<th>';
            html_tabla +='<input type="text" value="" class="cargo" />';
            html_tabla +='</th>';
            html_tabla +='<th>';
            html_tabla +='<input type="text" value="" class="abono" />';
            html_tabla +='</th>';
            $('#tabla_asientos tr:last').after(html_tabla);
        });
     

    } );

    function calcular(){
        var suma_abono = 0;
        $('.abono').each(function(){
            //if($(this).val() == ""){ $(this).val(0)}
            suma_abono += parseFloat($(this).val());
        });
        $(".abono_total").val(suma_abono);

        var suma_cargo = 0;
        $('.cargo').each(function(){
            //if($(this).val() == ""){ $(this).val(0)}
            suma_cargo += parseFloat($(this).val());
        });
        $(".cargo_total").val(suma_cargo);

    }

    function seleccionar_cuenta(clave){
        aux2 = $('#aux1').val();
      
      $('#c_'+aux2).val(""+clave+"");
      $('.modal_cuentas').modal('toggle');

    }

    function enviar_id(id){
        $('#aux1').val(id);
    }
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>





<div class="content mt-3">
    <div class="animated fadeIn">
    <!--button class="agregar_asiento">agregar asiento</button-->
    <form method="post" action="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_asientos_folio/<?php echo $folio;?>/<?php echo $departamento;?>">
    <table id="tabla_asientos"> 
       <?php foreach($asientos as $row):?>
        <tr>
            <th>
                <input type="text" name="save[<?php echo $row->asiento_id;?>][cuenta][]" value="<?php echo $row->cuenta;?>"  class="cuenta " onclick="enviar_id(<?php echo $row->asiento_id;?>)" id="c_<?php echo $row->asiento_id;?>" data-toggle="modal" data-target=".modal_cuentas"/>
                    <?php echo $row->cuenta_descripcion;?>
            </th>
            <th>
                <input type="text" name="save[<?php echo $row->asiento_id;?>][cargo][]" value=" <?php echo (float) round($row->cargo , 4);?>" class="cargo" onkeyup="calcular()" />
            </th>
            <th>
                <input type="text" name="save[<?php echo $row->asiento_id;?>][abono][]" value="<?php echo (float) round($row->abono , 4);?>" class="abono" onkeyup="calcular()" />
            </th>
        </tr>
        <?php endforeach;?>
        <tr>
            <th>
            <input type="text" name="save[0][cuenta][]" value="" class="cuenta " onclick="enviar_id(0)" id="c_0" data-toggle="modal" data-target=".modal_cuentas"/>
            <label id="label_0"></label>
            </th>
            <th>
                <input type="text" name="save[0][cargo][]"  value="0" class="cargo" onkeyup="calcular()" />
            </th>
            <th>
                <input type="text" name="save[0][abono][]" value="0" class="abono" onkeyup="calcular()" />
                
            </th>
        </tr>
        <tr>
            <th>
            <input type="text" name="save[2][cuenta][]" value="" class="cuenta " onclick="enviar_id(2)" id="c_2" data-toggle="modal" data-target=".modal_cuentas"/>
            <label id="label_2"></label>
            </th>
            <th>
                <input type="text" name="save[2][cargo][]"  value="0" class="cargo" onkeyup="calcular()" />
            </th>
            <th>
                <input type="text" name="save[2][abono][]" value="0" class="abono" onkeyup="calcular()" />
                
            </th>
        </tr>
        <tr>
            <th>
            <input type="text" name="save[3][cuenta][]" value="" class="cuenta " onclick="enviar_id(3)" id="c_3" data-toggle="modal" data-target=".modal_cuentas"/>
            <label id="label_3"></label>
            </th>
            <th>
                <input type="text" name="save[3][cargo][]"  value="0" class="cargo" onkeyup="calcular()" />
            </th>
            <th>
                <input type="text" name="save[3][abono][]" value="0" class="abono" onkeyup="calcular()" />
                
            </th>
        </tr>
        <tr>
            <th>
                Totales
            </th>
            <th>
                <input type="text" value="" class="cargo_total" />
            </th>
            <th>
                <input type="text" value="" class="abono_total" />
            </th>
        </tr>
    </table>

    <button type="submit" class="">guardar</button>
    </form>
        

    </div><!-- .animated -->
</div><!-- .content -->




<div class="modal fade modal_cuentas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <input type="text" id="aux1"/>
    <table id='empTable2' class='display dataTable'>

      <thead>
        <tr>
          <th>id</th>
          <th>cuenta</th>
          <th>Descripcion</th>
          <th>Opciones</th>
          
        </tr>
      </thead>

      </table>
    </div>
  </div>
</div>


	
@endsection
