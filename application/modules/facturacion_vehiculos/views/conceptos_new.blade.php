@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')

<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">


<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script-->

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

      $('#empTable').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?=base_url()?>index.php/facturacion_vehiculos/empList'
          },
          'columns': [
           
             { data: 'id'},
             { data: 'ClaveProdServ' },
             { data: 'Des' },
             {
                "data": "opciones", "render": function (data) {
                       
                    botones = "";

                    botones += '<button type="button" class="btn btn-info" onclick="seleccionar_clave_prod(\''+data.ClaveProdServ+'\')">seleccionar</button>';

                    return botones;
 
                }
            }
             

             
          ]
        });


        $('#empTable2').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?=base_url()?>index.php/facturacion_vehiculos/empList2'
          },
          'columns': [
           
             { data: 'id'},
             { data: 'ClaveUnidad' },
             { data: 'Nombre' },
             {
                "data": "opciones", "render": function (data) {
                       
                    botones = "";
                    variable_string = data.ClaveUnidad;
                    botones += '<button type="button" class="btn btn-info" onclick="seleccionar_clave_unidad(\''+variable_string+'\')">seleccionar</button>';

                    return botones;
 
                }
            }
             

             
          ]
        });


      $(".textnumeros").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

      $("#cargando").hide();
      $('#enviar_concepto').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_datos_concepto/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"concepto_NoIdentificacion":$("#concepto_NoIdentificacion").val(),
                      "concepto_unidad":$("#concepto_unidad").val(),
                      //"concepto_descuento":$("#concepto_descuento").val(),
                      "clave_sat":$("#clave_sat").val(),
                      "unidad_sat":$("#unidad_sat").val(),
                      "concepto_nombre":$("#concepto_nombre").val(),
                      "concepto_precio":$("#concepto_precio").val(),
                      "concepto_importe":$("#concepto_importe").val(),
                      "impuesto_iva":$("#impuesto_iva").val(),
                      "impuesto_iva_tipoFactor":$("#impuesto_iva_tipoFactor").val(),
                      "impuesto_iva_tasaCuota":$("#impuesto_iva_tasaCuota").val(),
                      //"impuesto_ISR":$("#factura_medotoPago").val(),
                      //"impuesto_ISR_tasaFactor":$("#factura_tipoComprobante").val(),
                      //"impuestoISR_tasaCuota":$("#receptor_RFC").val(),
                      "tipo":$("#tipo").val(),
                      //"nombre_interno":$("#pagada").val(),
                      "base_iva":$("#base_iva").val(),
                      "id_producto_servicio_interno":$("#id_producto_servicio_interno").val(),
                      "concepto_cantidad":$("#concepto_cantidad").val(),
                      "importe_iva":$("#importe_iva").val(),
                      "clave_vehicular":$("#clave_vehicular").val(),
                      "niv":$("#niv").val(),
                      "numero":$("#numero").val(),
                      "fecha":$("#fecha").val(),
                      "concepto_id":$("#concepto_id").val(),
                      "aduana":$("#aduana").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS ACTUALIZADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/conceptos/<?php echo $factura->facturaID?>");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });

    /*  $("#concepto_nombre").autocomplete({
            source: "<?php echo base_url()?>index.php/Factura/buscar_producto",
             select: function(event, ui) {



           }
    });*/
    });


    function seleccionar_producto(id_producto){
      //$('#exampleModal').modal('hide');
      var url_sis ="<?php echo base_url()?>index.php/facturacion/get_dados_producto/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                  var obj = JSON.parse(data);
                  $('#concepto_nombre').val(obj.productoNombre);
                  $('#concepto_NoIdentificacion').val(obj.productoReferencia);
                  $('#concepto_unidad').val(obj.productoMedida);
                  $('#clave_sat').val(obj.clave_sat);
                  $('#unidad_sat').val(obj.unidad_sat);
                  $('#id_producto_servicio_interno').val(obj.productoId);
                  $('#tipo').val(obj.productoClasificado);
                  $('#precios').html("Precio público:$"+obj.productoPrecioPublico+
                                     "&nbsp &nbsp Precio mayoreo:$"+obj.productoPrecioMayoreo+
                                     "&nbsp &nbsp Cantidad mayoreo:"+obj.productoCantidadMayoreo+
                                     "&nbsp &nbsp Precio credito:$"+obj.productoPrecioCredito);

                  console.log("SUCCESS : ", data);

                  //seleccionar_producto_unidad(id_producto);

                  $("#concepto_cantidad").val(1);
                  $("#concepto_precio").val(obj.productoPrecioPublico);
                  $("#concepto_importe").val(obj.productoPrecioPublico*1);
                  $("#importe_iva").val($("#concepto_importe").val()*.16);

                  $("#concepto_cantidad").keyup(function(){
                    $("#concepto_cantidad").css("background-color", "yellow");
                    //alert($(this).val());
                    if($("#concepto_cantidad").val() < obj.productoCantidadMayoreo ){
                      $("#concepto_precio").val(obj.productoPrecioPublico);
                      $("#concepto_importe").val(obj.productoPrecioPublico*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);
                    }else if($("#concepto_cantidad").val() >= obj.productoCantidadMayoreo){
                      $("#concepto_precio").val(obj.productoPrecioMayoreo);
                      $("#concepto_importe").val(obj.productoPrecioMayoreo*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);
                    }

                    $("#impuesto_ieps_tasaCuota").val(obj.ieps);
                    $("#importe_ieps").val( $("#concepto_importe").val() * obj.ieps);
                  });



                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
    }


  function seleccionar_producto_unidad(id_producto){

      var url_sis ="<?php echo base_url()?>index.php/facturacion/unidades_productos/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                 $("#load_unidad").html("");
                 $("#load_unidad").html(data);



                  console.log("SUCCESS : ", data);





                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });

    }


    /* agregar una toma de autos a la factura */
    function agregar_toma_de_auto(){
      $("#enviar2").hide();
        $("#cargando2").show();
        var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_toma/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"toma_monto_adquisicion":$("#toma_monto_adquisicion").val(),
                      "toma_monto_enajenacion":$("#toma_monto_enajenacion").val(),
                      "toma_clave_vehicular":$("#toma_clave_vehicular").val(),
                      "toma_marca":$("#toma_marca").val(),
                      "toma_tipo":$("#toma_tipo").val(),
                      "toma_modelo":$("#toma_modelo").val(),
                      "toma_numero_motor":$("#toma_numero_motor").val(),
                      "toma_niv":$("#toma_niv").val(),
                      "toma_valor":$("#toma_valor").val(),
                      "numero":$("#toma_numero").val(),
                      "fecha":$("#toma_fecha").val(),
                      "id_toma":$("#id_toma").val(),
                      "aduana":$("#tomaaduana").val()
                      },
                  "POST","",function(result){
                  correoValido = false;
                  console.log(result);
                  json_response = JSON.parse(result);
                  obj_output = json_response.output;
                  obj_status = obj_output.status;
                  if(obj_status == false){
                    aux = "";
                    $.each( obj_output.errors, function( key, value ) {
                      aux +=value+"<br/>";
                    });
                    exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                    $("#enviar2").show();
                    $("#cargando2").hide();
                  }
                  if(obj_status == true){
                    exito_redirect("DATOS ACTUALIZADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/conceptos/<?php echo $factura->facturaID?>");
                    $("#enviar2").show();
                    $("#cargando2").hide();
                  }


        });
    }

    function seleccionar_clave_prod(clave){
      $('#clave_sat').val(""+clave+"");
      $('.bd-example-modal-lg').modal('toggle');

    }

    function seleccionar_clave_unidad(clave){
      
      $('#unidad_sat').val(""+clave+"");
      $('.bd-example-modal-lg2').modal('toggle');

    }
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>


<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">
            <h4>
                <?php  if($factura->tipo_auto == 1){ echo "AUTO NUEVO"; }?>
                <?php  if($factura->tipo_auto == 2){ echo "AUTO SEMINUEVO"; }?>
            </h4>
            </strong>
            <input type="hidden" id="concepto_id" value="<?php echo $conceptos->concepto_id;?>"/>

        </div>
        <div class="card-body" style="background:#bdc3c7">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">
                <?php
                //CUANDO ES UN SEMINUEVO
                if($factura->tipo_auto == 2){
                  /*
                    1	persona fisica
                    2	persona fisica con actividad empresarial
                    3	persona moral
                  */
                  //persona fisica , el iva es normal
                  if($factura->tipo_persona == 1){
                    
                  }
                  if(($factura->tipo_persona == 2) || ($factura->tipo_persona == 3) ){
                    echo "el IVA se calcula solo de la comisión";
                  }
                  
                  
                }else{
                  
                }
                ?>

                  <form action="" method="post" novalidate="novalidate" id="enviar_concepto">



                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Concepto</label>
                                  <input id="concepto_nombre" name="concepto_nombre" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->concepto_nombre?>" placeholder="Nombre" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
                                  <input type="hidden" id="id_producto_servicio_interno" name="id_producto_servicio_interno"/>
                                  <input type="hidden" id="tipo" name="tipo"/>
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">No. identificación interna</label>
                                  <input id="concepto_NoIdentificacion" name="concepto_NoIdentificacion" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->concepto_NoIdentificacion?>" placeholder="Numero identificación interna" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-3" style="display: none;">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Unidad interna</label>
                                 <div id="load_unidad"></div>
                                
                                <select name="concepto_unidad" id="concepto_unidad" class="form-control form-control-sm" style="width:75px;">
                                  <?php foreach($medidas as $row):?>
                                    <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                  
                                  <?php endforeach;?>
                                </select>
                            </div>
                          </div>

                      </div>


                      <div class="row">
                         <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clave vehicular</label>
                                  <input id="clave_vehicular" name="clave_vehicular" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->clave_vehicular;?>" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">VIN</label>
                                  <input id="niv" name="niv" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->niv;?>" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                         
                          <?php if($factura->unidad_importada == "si"):?>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Pedimento</label>
                                  <input id="numero" name="numero" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->numero;?>" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Fecha(YYYY-mm-dd)</label>
                                  <input id="fecha" name="fecha" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->fecha;?>" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Aduana</label>
                                  <input id="aduana" name="aduana" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->aduana;?>" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                             
                                  <!--select id="aduana" name="aduana"  class="form-control cc-exp alto form-control-sm">
                                      <?php foreach($aduanas as $aduana):?>
                                           <option value="<?php echo $aduana->aduana;?>" > <?php echo $aduana->aduana;?></option>
                                      <?php endforeach;?>
                                  </select-->
                              </div>
                          </div>
                        <?php endif;?>


                      </div>


                      <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                                <input id="clave_sat" name="clave_sat" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->clave_sat;?>" placeholder="" autocomplete="cc-exp" data-toggle="modal" data-target=".bd-example-modal-lg">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                             
                                <!--select class="form-control alto form-control-sm" id="clave_sat" name="clave_sat">
                                    <option value="25101503">25101503-Carros</option>
                                    <option value="25101504">25101504-Station wagons</option>
                                    <option value="25101505">25101505-Minivans o vans</option>
                                    <option value="25101507">25101507-Camiones ligeros o vehículos utilitarios deportivos</option>
                                    <option value="25101508">25101508-Carro deportivo</option>
                                    <option value="25101509">25101509-Carro eléctrico</option>
                                    <option value="25101600">25101600-Vehículos de transporte de productos y materiales</option>
                                    <option value="25101601">25101601-Volquetas</option>
                                    <option value="25101602">25101602-Remolques</option>
                                    <option value="25101604">25101604-Camiones de reparto</option>
                                    <option value="25101604">25101604-Camiones de reparto</option>
                                     
                                </select-->

                            </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>
                              <input id="unidad_sat" name="unidad_sat" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo $conceptos->unidad_sat;?>" placeholder="" autocomplete="cc-exp" data-toggle="modal" data-target=".bd-example-modal-lg2">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                             
                              <!--select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                
                                <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <option value="C62">C62-Uno</option>
                                <option value="XVN">XVN-Vehículo</option>
                                
                              </select-->
                          </div>
                        </div>


                        <!--div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                                <input id="concepto_cantidad" name="concepto_cantidad" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="<?php echo $conceptos->concepto_cantidad;?>" placeholder="Cantidad" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div-->
                        <?php
                         $precio_sin_iva = 0;
                          //CUANDO ES UN SEMINUEVO
                          if($factura->tipo_auto == 2){
                            /*
                              1	persona fisica
                              2	persona fisica con actividad empresarial
                              3	persona moral
                            */
                            //persona fisica , el iva es normal
                            if($factura->tipo_persona == 1){
                              $precio_sin_iva = (float) round($conceptos->precio_venta / 1.16, 2);
                              
                            }
                            if(($factura->tipo_persona == 2) || ($factura->tipo_persona == 3) ){
                              //precio de compra sin iva
                              $precio_compra1_sin_iva = $factura->total_compra_seminuevo / 1.16;

                              $comision = $conceptos->precio_venta - $factura->total_compra_seminuevo;
                              $comision_sin_iva =  $comision / 1.16;
                              
                              $total_real_sin_iva = $conceptos->precio_venta - ( $comision_sin_iva * .16 );

                              
                              //$precio_comision_con_iva1 = $conceptos->precio_venta - $precio_compra1;
                              //esto es la base del iva
                              //$precio_sin_iva = (float) round($precio_comision_con_iva1 / 1.16, 2);
                              $precio_sin_iva = (float) round($total_real_sin_iva, 2);


                              
                            }
                            
                            
                          }else{
                            //CUANDO ES UN NUEVO
                            //$iva_total =  number_format($conceptos->concepto_importe * .16 , 2, '.', '');
                            $precio_sin_iva = (float) round($conceptos->precio_venta / 1.16, 2);
                          }

                        ?>
                        <input id="concepto_cantidad" name="concepto_cantidad" type="hidden" class="form-control cc-exp alto form-control-sm textnumeros" value="<?php echo $precio_sin_iva;?>" placeholder="Cantidad" autocomplete="cc-exp">
                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Precio(sin IVA)</label>
                                <input id="concepto_precio" name="concepto_precio" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="<?php echo $precio_sin_iva;?>" placeholder="Precio" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Importe(sin IVA)</label>
                             
                              <input id="concepto_importe" name="concepto_importe" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="<?php echo $precio_sin_iva;?>" placeholder="Importe" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                      </div>








                      <div class="row">
                          <div class="col-12">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Impuestos</label>
                              </div>
                          </div>

                      </div>
                        

                      

                      <div class="row" >
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">IVA</label>
                                  <input id="impuesto_iva" name="impuesto_iva" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="002" autocomplete="cc-exp" value="002">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Factor</label>
                                  <input id="impuesto_iva_tipoFactor" name="impuesto_iva_tipoFactor" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Cuota" autocomplete="cc-exp" value="cuota">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Tasa cuota</label>
                                <input id="impuesto_iva_tasaCuota" name="impuesto_iva_tasaCuota" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Tasa cuota" autocomplete="cc-exp" value=".16">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <?php 
                                $iva_total = 0;
                                $base_iva = 0;
                                //CUANDO ES UN SEMINUEVO
                                if($factura->tipo_auto == 2){
                                  /*
                                    1	persona fisica
                                    2	persona fisica con actividad empresarial
                                    3	persona moral
                                  */
                                  //persona fisica , el iva es normal
                                  if($factura->tipo_persona == 1){
                                    
                                    $iva_total = (float) round(($conceptos->precio_venta / 1.16)* .16 , 2); 
                                    $base_iva = (float) round($conceptos->precio_venta / 1.16 , 2);//$conceptos->concepto_importe;
                                  }
                                  if(($factura->tipo_persona == 2) || ($factura->tipo_persona == 3) ){
                                    //precio de compra sin iva
                                    /*$precio_compra = $factura->total_compra_seminuevo * 1.16;
                                    $precio_comision_con_iva = $conceptos->precio_venta - $precio_compra;
                                    //esto es la base del iva
                                    $base_iva = (float) round($precio_comision_con_iva / 1.16 , 2);
                                    //iva que se cobra
                                    $iva_real = $base_iva * .16;

                                    $iva_total = (float) round($iva_real , 2);  
                                    */ 


                                    $comision = $conceptos->precio_venta - $factura->total_compra_seminuevo;
                                    $comision_sin_iva =  $comision / 1.16;
                                    
                                    $total_real_sin_iva = $conceptos->precio_venta - ( $comision_sin_iva * .16 );

                                    $iva_total =  (float) round($comision_sin_iva * .16, 2);

                                    $base_iva = (float) round($comision, 2);
                                  }
                                  
                                  
                                }else{
                                  //CUANDO ES UN NUEVO
                                   
                                  $base_iva = (float) round($conceptos->precio_venta / 1.16, 2);
                                  $iva_total = (float) round($base_iva * .16 , 2);  
                                }

                                  
                                ?>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Base IVA</label>
                                <input id="base_iva" name="base_iva" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="base iva" autocomplete="cc-exp" value="<?php echo $base_iva;?>">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Importe Impuesto</label>
                                
                                <input id="importe_iva" name="importe_iva" type="text" class="form-control cc-exp alto form-control-sm" placeholder="Tasa cuota" autocomplete="cc-exp" value="<?php echo $iva_total;?>">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>


                      </div>

                      


                      <div align="right">
                        <button title="Editar" id="enviar1" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Actualizar concepto</span>
                            <span id="cargando" style="display:none;">Sending…</span>
                        </button>
                        <!--div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                       </div-->
                      </div>
                  </form>
              </div>

          </div>

        </div>
    </div> <!-- .card -->

<hr/>
    <div class="row">
      <div class="col-12">
          Agregar toma de auto
          <input type="hidden" id="id_toma" value="<?php echo isset($toma_vehiculo->id) ? $toma_vehiculo->id : "";?>" />
      </div>

    </div>
    <div class="row">

      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Mongo adquisición</label>
            <input id="toma_monto_adquisicion" name="toma_monto_adquisicion" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_monto_adquisicion) ? $toma_vehiculo->toma_monto_adquisicion : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Mongo enajenación</label>
            <input id="toma_monto_enajenacion" name="toma_monto_enajenacion" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_monto_enajenacion) ? $toma_vehiculo->toma_monto_enajenacion: "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Clave vehicular(Longitud Máxima 7, Longitud Mínima 1)</label>
            <input id="toma_clave_vehicular" name="toma_clave_vehicular" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_clave_vehicular) ? $toma_vehiculo->toma_clave_vehicular : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Marca(Longitud Máxima 50 caracteres)</label>
            <input id="toma_marca" name="toma_marca" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_marca) ? $toma_vehiculo->toma_marca: "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Tipo(Longitud Máxima 50 caracteres)</label>
            <input id="toma_tipo" name="toma_tipo" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_tipo) ? $toma_vehiculo->toma_tipo : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>


      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Número aduana</label>
            <input id="toma_numero" name="toma_numero" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_numero) ? $toma_vehiculo->toma_numero : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Fecha aduana(YYYY-mm-dd)</label>
            <input id="toma_fecha" name="toma_fecha" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_fecha) ? $toma_vehiculo->toma_fecha : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Aduana</label>
            <input id="toma_aduana" name="toma_aduana" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_aduana) ? $toma_vehiculo->toma_aduana : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>


    </div>

    <div class="row">
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Modelo</label>
            <input id="toma_modelo" name="toma_modelo" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->otma_modelo) ? $toma_vehiculo->otma_modelo : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Número motor(Longitud Máxima 17)</label>
            <input id="toma_numero_motor" name="toma_numero_motor" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_numero_motor) ? $toma_vehiculo->toma_numero_motor : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">VIN(Longitud Máxima 17)</label>
            <input id="toma_niv" name="toma_niv" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_niv) ? $toma_vehiculo->toma_niv : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Valor</label>
            <input id="toma_valor" name="toma_valor" type="text" class="form-control cc-exp alto form-control-sm" value="<?php echo isset($toma_vehiculo->toma_valor) ? $toma_vehiculo->toma_valor : "";?>" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>

    </div>

    <div align="right">
      <button title="Editar" id="enviar2" type="button" class="btn btn-lg btn-info " onclick="agregar_toma_de_auto()">
        <i class="fa fa-edit fa-lg"></i>&nbsp;
        <span id="payment-button-amount">Actualizar toma de auto</span>
        <span id="cargando2" style="display:none;">Sending…</span>
      </button>
                        
    </div>


<hr/>





    <div align="right">
      <a href="<?php echo base_url()?>index.php/facturacion_vehiculos/ver_factura/<?php echo $facturaID;?>">
      <button title="Editar" id="enviar1" type="button" class="btn btn-lg btn-info ">
          <i class="fa fa-edit fa-lg"></i>&nbsp;
          <span id="payment-button-amount">Siguiente</span>

      </button>
     </a>
      <!--div align="center">
       <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
     </div-->
    </div>

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->






<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <table id='empTable' class='display dataTable'>

      <thead>
        <tr>
          <th>ID</th>
          <th>ClaveServProd</th>
          <th>Descripción</th>
          <th>Opciones</th>
          
        </tr>
      </thead>

      </table>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <table id='empTable2' class='display dataTable'>

      <thead>
        <tr>
          <th>ID</th>
          <th>ClaveUnidad</th>
          <th>Nombre</th>
          <th>Opciones</th>
          
        </tr>
      </thead>

      </table>
    </div>
  </div>
</div>
	
@endsection
