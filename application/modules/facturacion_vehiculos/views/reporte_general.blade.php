@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')




<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <div class="">
      
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <div class="col-md-12 text-right">
                    <a href="{{base_url('/facturacion_vehiculos/pdf_reporte_general/1')}}" class="btn btn-primary btn-sm" target="_blank">PDF</a>
                  </div>
                   
                   <br/>
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Economico</th>
                        <th>Vehiculo</th>
                        <th>Vendedor</th>
                        <th>Cliente</th>
                        <th>cuenta</th>
                        <th>Debe</th>
                        <th>Haber</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($facturas)):?>
                      <?php foreach($facturas as $row):?>
                        <?php $concepto_row = $this->Mgeneral->get_row('concepto_facturaId',$row->facturaID,'factura_conceptos_vehiculo');?>
                        <tr >
                          <td><?php echo $row->factura_fecha;?></td>
                          <td><?php echo $concepto_row->economico;?></td>
                          
                          <td>
                            <?php echo $concepto_row->concepto_nombre;?>
                            <br/>
                            <?php 
                              
                                    
                                $iva_total = (float) round(($concepto_row->precio_venta / 1.16)* .16 , 2); 
                                $base_iva = (float) round($concepto_row->precio_venta / 1.16 , 2);//$conceptos->concepto_importe;
                              
                              
                              if(($row->tipo_persona == 2) || ($row->tipo_persona == 3) ){
                                //precio de compra sin iva
                                $precio_compra = $row->total_compra_seminuevo * 1.16;
                                $precio_comision_con_iva = $concepto_row->precio_venta - $precio_compra;
                                //esto es la base del iva
                                $base_iva = (float) round($precio_comision_con_iva / 1.16 , 2);
                                //iva que se cobra
                                $iva_real = $base_iva * .16;

                                $iva_total = (float) round($iva_real , 2);   
                              }
                            ?>
                            <!--b>Subtotal:$<?php echo $base_iva;?> </b>
                            <b>IVA:$<?php echo $iva_total;?> </b>
                            <b>Total:$<?php echo $base_iva+$iva_total;?> </b-->
                          </td>
                          <td><?php echo $row->vendedor_nombre;?></td>
                          <td>
                            <?php echo $row->nombre_cliente;?>
                            <br/>
                            <?php 
                            if($row->tipo_auto == 2){
                              if($row->tipo_persona == 1){ echo "<b>Persona fisica</b>";}
                              if($row->tipo_persona == 2){ echo "<b>Persona fisica con actividad empresarial</b>";}
                              if($row->tipo_persona == 3){ echo "<b>Persona moral</b>";}
                            }
                             
                            ?>
                            
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <?php 
                        if($row->tipo_auto == 2){
                          $folio_new = 'VU-'.$concepto_row->economico;
                          $url = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/detalle?departamento=18&folio=';
                        }
                        if($row->tipo_auto == 1){
                          $folio_new = 'VN-'.$concepto_row->economico;
                          $url = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/detalle?departamento=17&folio=';
                        }
                        $dataFromApi = $this->curl->curlGet($url.$folio_new.'',true);
                        $obj = json_decode($dataFromApi);
                        $asientos = $obj->data;
                        $total_abono = 0;
                        $total_cargo = 0;
                        //$total_total = 0;

                        
                        ?>
                        
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td colspan="3">
                          <!--a href="<?php echo base_url();?>index.php/facturacion_vehiculos/cambiar_asientos/<?php echo $folio_new?>/<?php echo $row->tipo_auto;?>">cambiar asientos</a-->
                          </td>
                        </tr>
                        <?php foreach($asientos as $asi):?>
                          <?php if($asi->estatus_id != 'POR_APLICAR'):?>
                        <tr >
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>

                          
                          </td>
                          
                          <td><?php echo $asi->cuenta .' '.$asi->cuenta_descripcion;?></td>
                          <td>$<?php echo (float) round($asi->cargo , 2);?></td>
                          <td>$<?php echo (float) round($asi->abono , 2);?></td>
                          <?php 
                            $total_abono += (float) round($asi->abono , 2);
                            $total_cargo += (float) round($asi->cargo, 2);
                           // $total_total += 0;
                          ?>
                        </tr>
                        <?php endif;?>
                        <?php endforeach;?>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          
                          <td>TOTALES</td>
                          <td>$<?php echo (float) round($total_cargo, 2);?></td>
                          <td>$<?php echo (float) round($total_abono, 2);?></td>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

	
@endsection
