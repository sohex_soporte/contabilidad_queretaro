@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')

<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){


      $(".textnumeros").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

      $("#cargando").hide();
      $('#enviar_concepto').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_datos_concepto/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"concepto_NoIdentificacion":$("#concepto_NoIdentificacion").val(),
                      "concepto_unidad":$("#concepto_unidad").val(),
                      //"concepto_descuento":$("#concepto_descuento").val(),
                      "clave_sat":$("#clave_sat").val(),
                      "unidad_sat":$("#unidad_sat").val(),
                      "concepto_nombre":$("#concepto_nombre").val(),
                      "concepto_precio":$("#concepto_precio").val(),
                      "concepto_importe":$("#concepto_importe").val(),
                      "impuesto_iva":$("#impuesto_iva").val(),
                      "impuesto_iva_tipoFactor":$("#impuesto_iva_tipoFactor").val(),
                      "impuesto_iva_tasaCuota":$("#impuesto_iva_tasaCuota").val(),
                      //"impuesto_ISR":$("#factura_medotoPago").val(),
                      //"impuesto_ISR_tasaFactor":$("#factura_tipoComprobante").val(),
                      //"impuestoISR_tasaCuota":$("#receptor_RFC").val(),
                      "tipo":$("#tipo").val(),
                      //"nombre_interno":$("#pagada").val(),
                      "id_producto_servicio_interno":$("#id_producto_servicio_interno").val(),
                      "concepto_cantidad":$("#concepto_cantidad").val(),
                      "importe_iva":$("#importe_iva").val(),
                      "clave_vehicular":$("#clave_vehicular").val(),
                      "niv":$("#niv").val(),
                      "numero":$("#numero").val(),
                      "fecha":$("#fecha").val(),
                      "aduana":$("#aduana").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/conceptos/<?php echo $factura->facturaID?>");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });

    /*  $("#concepto_nombre").autocomplete({
            source: "<?php echo base_url()?>index.php/Factura/buscar_producto",
             select: function(event, ui) {



           }
    });*/
    });


    function seleccionar_producto(id_producto){
      //$('#exampleModal').modal('hide');
      var url_sis ="<?php echo base_url()?>index.php/facturacion/get_dados_producto/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                  var obj = JSON.parse(data);
                  $('#concepto_nombre').val(obj.productoNombre);
                  $('#concepto_NoIdentificacion').val(obj.productoReferencia);
                  $('#concepto_unidad').val(obj.productoMedida);
                  $('#clave_sat').val(obj.clave_sat);
                  $('#unidad_sat').val(obj.unidad_sat);
                  $('#id_producto_servicio_interno').val(obj.productoId);
                  $('#tipo').val(obj.productoClasificado);
                  $('#precios').html("Precio público:$"+obj.productoPrecioPublico+
                                     "&nbsp &nbsp Precio mayoreo:$"+obj.productoPrecioMayoreo+
                                     "&nbsp &nbsp Cantidad mayoreo:"+obj.productoCantidadMayoreo+
                                     "&nbsp &nbsp Precio credito:$"+obj.productoPrecioCredito);

                  console.log("SUCCESS : ", data);

                  //seleccionar_producto_unidad(id_producto);

                  $("#concepto_cantidad").val(1);
                  $("#concepto_precio").val(obj.productoPrecioPublico);
                  $("#concepto_importe").val(obj.productoPrecioPublico*1);
                  $("#importe_iva").val($("#concepto_importe").val()*.16);

                  $("#concepto_cantidad").keyup(function(){
                    $("#concepto_cantidad").css("background-color", "yellow");
                    //alert($(this).val());
                    if($("#concepto_cantidad").val() < obj.productoCantidadMayoreo ){
                      $("#concepto_precio").val(obj.productoPrecioPublico);
                      $("#concepto_importe").val(obj.productoPrecioPublico*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);
                    }else if($("#concepto_cantidad").val() >= obj.productoCantidadMayoreo){
                      $("#concepto_precio").val(obj.productoPrecioMayoreo);
                      $("#concepto_importe").val(obj.productoPrecioMayoreo*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);
                    }

                    $("#impuesto_ieps_tasaCuota").val(obj.ieps);
                    $("#importe_ieps").val( $("#concepto_importe").val() * obj.ieps);
                  });



                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
    }


  function seleccionar_producto_unidad(id_producto){

      var url_sis ="<?php echo base_url()?>index.php/facturacion/unidades_productos/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                 $("#load_unidad").html("");
                 $("#load_unidad").html(data);



                  console.log("SUCCESS : ", data);





                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });

    }


    /* agregar una toma de autos a la factura */
    function agregar_toma_de_auto(){
      $("#enviar2").hide();
        $("#cargando2").show();
        var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/guarda_toma/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"toma_monto_adquisicion":$("#toma_monto_adquisicion").val(),
                      "toma_monto_enajenacion":$("#toma_monto_enajenacion").val(),
                      "toma_clave_vehicular":$("#toma_clave_vehicular").val(),
                      "toma_marca":$("#toma_marca").val(),
                      "toma_tipo":$("#toma_tipo").val(),
                      "toma_modelo":$("#toma_modelo").val(),
                      "toma_numero_motor":$("#toma_numero_motor").val(),
                      "toma_niv":$("#toma_niv").val(),
                      "toma_valor":$("#toma_valor").val(),
                      "numero":$("#toma_numero").val(),
                      "fecha":$("#toma_fecha").val(),
                      "aduana":$("#tomaaduana").val()
                      },
                  "POST","",function(result){
                  correoValido = false;
                  console.log(result);
                  json_response = JSON.parse(result);
                  obj_output = json_response.output;
                  obj_status = obj_output.status;
                  if(obj_status == false){
                    aux = "";
                    $.each( obj_output.errors, function( key, value ) {
                      aux +=value+"<br/>";
                    });
                    exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                    $("#enviar2").show();
                    $("#cargando2").hide();
                  }
                  if(obj_status == true){
                    exito_redirect("DATOS GUARDADOS CON EXITO","success","");
                    $("#enviar2").show();
                    $("#cargando2").hide();
                  }


        });
    }
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta de conceptos </strong>

        </div>
        <div class="card-body" style="background:#bdc3c7">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="enviar_concepto">



                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre concepto</label>
                                  <input id="concepto_nombre" name="concepto_nombre" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Nombre" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
                                  <input type="hidden" id="id_producto_servicio_interno" name="id_producto_servicio_interno"/>
                                  <input type="hidden" id="tipo" name="tipo"/>
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">No. identificación interna</label>
                                  <input id="concepto_NoIdentificacion" name="concepto_NoIdentificacion" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Numero identificación interna" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Unidad interna</label>
                                 <div id="load_unidad"></div>
                                <!--input id="concepto_unidad" name="concepto_unidad" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Unidad interna" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span-->
                                <select name="concepto_unidad" id="concepto_unidad" class="form-control form-control-sm" style="width:75px;">
                                  <?php foreach($medidas as $row):?>
                                    <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                  
                                  <?php endforeach;?>
                                </select>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                         <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clave vehicular</label>
                                  <input id="clave_vehicular" name="clave_vehicular" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Niv</label>
                                  <input id="niv" name="niv" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Fecha(YYYY-mm-dd)</label>
                                  <input id="fecha" name="fecha" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Aduana</label>
                             
                                  <select id="aduana" name="aduana"  class="form-control cc-exp alto form-control-sm">
                                      <?php foreach($aduanas as $aduana):?>
                                           <option value="<?php echo $aduana->aduana;?>" > <?php echo $aduana->aduana;?></option>
                                      <?php endforeach;?>
                                  </select>
                              </div>
                          </div>

                      </div>


                      <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                                <select class="form-control alto form-control-sm" id="clave_sat" name="clave_sat">
                                    <option value="25101503">25101503-Carros</option>
                                    <option value="25101504">25101504-Station wagons</option>
                                    <option value="25101505">25101505-Minivans o vans</option>
                                    <option value="25101507">25101507-Camiones ligeros o vehículos utilitarios deportivos</option>
                                    <option value="25101508">25101508-Carro deportivo</option>
                                    <option value="25101509">25101509-Carro eléctrico</option>
                                    <option value="25101600">25101600-Vehículos de transporte de productos y materiales</option>
                                    <option value="25101601">25101601-Volquetas</option>
                                    <option value="25101602">25101602-Remolques</option>
                                    <option value="25101604">25101604-Camiones de reparto</option>
                                    <option value="25101604">25101604-Camiones de reparto</option>
                                     
                                </select>

                            </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>
                              <select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                
                                <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <option value="C62">C62-Uno</option>
                                <option value="XVN">XVN-Vehículo</option>
                                
                              </select>
                          </div>
                        </div>


                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                                <input id="concepto_cantidad" name="concepto_cantidad" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Cantidad" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Precio</label>
                                <input id="concepto_precio" name="concepto_precio" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Precio" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Importe</label>
                              <input id="concepto_importe" name="concepto_importe" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Importe" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                      </div>






                      <div class="row">
                          <div class="col-12">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Impuestos</label>
                              </div>
                          </div>

                      </div>


                      <div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">IVA</label>
                                  <input id="impuesto_iva" name="impuesto_iva" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="002" autocomplete="cc-exp" value="002">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Factor</label>
                                  <input id="impuesto_iva_tipoFactor" name="impuesto_iva_tipoFactor" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Cuota" autocomplete="cc-exp" value="cuota">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Tasa cuota</label>
                                <input id="impuesto_iva_tasaCuota" name="impuesto_iva_tasaCuota" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Tasa cuota" autocomplete="cc-exp" value=".16">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Importe Impuesto</label>
                                <input id="importe_iva" name="importe_iva" type="text" class="form-control cc-exp alto form-control-sm" placeholder="Tasa cuota" autocomplete="cc-exp" value="">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>


                      </div>

                      


                      <div align="right">
                        <button title="Editar" id="enviar1" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Agregar concepto</span>
                            <span id="cargando" style="display:none;">Sending…</span>
                        </button>
                        <!--div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                       </div-->
                      </div>
                  </form>
              </div>

          </div>

        </div>
    </div> <!-- .card -->

<hr/>
    <div class="row">
      <div class="col-12">
          Agregar toma de auto
      </div>

    </div>
    <div class="row">

      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Mongo adquisición</label>
            <input id="toma_monto_adquisicion" name="toma_monto_adquisicion" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Mongo enajenación</label>
            <input id="toma_monto_enajenacion" name="toma_monto_enajenacion" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Clave vehicular(Longitud Máxima 7, Longitud Mínima 1)</label>
            <input id="toma_clave_vehicular" name="toma_clave_vehicular" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Marca(Longitud Máxima 50 caracteres)</label>
            <input id="toma_marca" name="toma_marca" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Tipo(Longitud Máxima 50 caracteres)</label>
            <input id="toma_tipo" name="toma_tipo" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>


      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Número aduana</label>
            <input id="toma_numero" name="toma_numero" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Fecha aduana(YYYY-mm-dd)</label>
            <input id="toma_fecha" name="toma_fecha" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Aduana</label>
            <input id="toma_aduana" name="toma_aduana" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>

    </div>

    <div class="row">
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Modelo</label>
            <input id="toma_modelo" name="toma_modelo" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Número motor(Longitud Máxima 17)</label>
            <input id="toma_numero_motor" name="toma_numero_motor" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">NIV(Longitud Máxima 17)</label>
            <input id="toma_niv" name="toma_niv" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>
      <div class="col-2">
          <div class="form-group">
            <label for="cc-exp" class="control-label mb-1">Valor</label>
            <input id="toma_valor" name="toma_valor" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="" autocomplete="cc-exp">
            <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
          </div>
      </div>

    </div>

    <div align="right">
      <button title="Editar" id="enviar2" type="button" class="btn btn-lg btn-info " onclick="agregar_toma_de_auto()">
        <i class="fa fa-edit fa-lg"></i>&nbsp;
        <span id="payment-button-amount">Agregar toma de auto</span>
        <span id="cargando2" style="display:none;">Sending…</span>
      </button>
                        
    </div>


<hr/>
    
<div class="row">
  <div class="col-12">
    Auto
  </div>

</div>
    <div class="row" style="background:#fff;">
      <div class="col-12" id="factura_concepos_load">

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Concepto</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Precio</th>
              <th scope="col">IVA</th>
              <th scope="col">Total</th>
              <th scope="col">clave_vehicular</th>
              <th scope="col">niv</th>
              <th scope="col">numero</th>
              <th scope="col">fecha</th>
              <th scope="col">aduana</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($conceptos as $conc):?>
            <tr>
              <th ><?php echo $conc->concepto_nombre;?></th>
              <td><?php echo $conc->concepto_cantidad;?></td>
              <td><?php echo $conc->concepto_precio;?></td>
              <td><?php echo $conc->importe_iva;?></td>
              <td><?php echo $conc->concepto_importe+$conc->importe_iva;?></td>

              <th scope="col"><?php echo $conc->clave_vehicular;?></th>
              <th scope="col"><?php echo $conc->niv;?></th>
              <th scope="col"><?php echo $conc->numero;?></th>
              <th scope="col"><?php echo $conc->fecha;?></th>
              <th scope="col"><?php echo $conc->aduana;?></th>


              <td><a href="<?php echo base_url();?>index.php/factura_vehiculos/eliminar_concepto/<?php echo $conc->concepto_id;?>/<?php echo $facturaID;?>" class="text-danger">Eliminar</a></td>
            </tr>
          <?php endforeach;?>
          </tbody>
        </table>

      </div>
    </div>

  
<hr/>

<div class="row">
  <div class="col-12">
    Toma de auto
  </div>

</div>
<div class="row" style="background:#fff;">
      <div class="col-12" id="factura_concepos_load">

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Monto adquisición</th>
              <th scope="col">Monto enajenación</th>
              <th scope="col">Calve vehicular</th>
              <th scope="col">Marca</th>
              <th scope="col">Tipo</th>
              <th scope="col">Modelo</th>
              <th scope="col">Número de motor</th>
              <th scope="col">NIV</th>
              <th scope="col">Valor</th>
            
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($toma_vehiculo as $tomas):?>
            <tr>
              <th ><?php echo $tomas->toma_monto_adquisicion;?></th>
              <td><?php echo $tomas->toma_monto_enajenacion;?></td>
              <td><?php echo $tomas->toma_clave_vehicular;?></td>
              <td><?php echo $tomas->toma_marca;?></td>
              <td><?php echo $tomas->toma_tipo;?></td>

              <th scope="col"><?php echo $tomas->toma_modelo;?></th>
              <th scope="col"><?php echo $tomas->toma_numero_motor;?></th>
              <th scope="col"><?php echo $tomas->toma_niv;?></th>
              <th scope="col"><?php echo $tomas->toma_valor;?></th>
              


              <td>
                
              </td>
            </tr>
          <?php endforeach;?>
          </tbody>
        </table>

      </div>
    </div>


    <div align="right">
      <a href="<?php echo base_url()?>index.php/facturacion_vehiculos/ver_factura/<?php echo $facturaID;?>">
      <button title="Editar" id="enviar1" type="button" class="btn btn-lg btn-info ">
          <i class="fa fa-edit fa-lg"></i>&nbsp;
          <span id="payment-button-amount">Siguiente</span>

      </button>
     </a>
      <!--div align="center">
       <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
     </div-->
    </div>

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->







	
@endsection
