<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte general</title>    
</head>
<body>
    <table>
        <tr><td><br></td></tr>
        <tr>
            <td>MYBSA, S.A. DE C.V.</td>
        </tr>
        <tr><td><br></td></tr>
        <tr>
            <td>QUERETARO a <?php if (isset($fecha)){ echo $fecha;} ?></td>
        </tr>
        <tr><td><br></td></tr>
        <tr>
            <td style="width: 320px;">Poliza de VENTAS DE UNIDADES NUEVAS</td>
            <td style="width: 150px;">No.de Poliza: VN</td>
            <td>Pagina: [[page_cu]]/[[page_nb]]</td>            
        </tr>
        <tr>
            <td>Ventas del : <?php if (isset($fecha)){ echo $fecha;} ?></td>
        </tr>    
    </table>
    <table id="table-reporte">
    <thead>
        <tr>
            <td>Afectacion</td>
            <td>Descripcion</td>
            <td>Debe</td>
            <td>Haber</td>
        </tr>
    </thead>
    <tbody>
        <?php if(is_array($facturas)):?>        
        <tr>
            <br>
            ---------------------------------------------------------------------------------------------------------------------------------
        </tr>
        <tr>
            <td><br></td>
        </tr>
        <?php foreach($facturas as $row):?>        
        <?php $concepto_row = $this->Mgeneral->get_row('concepto_facturaId',$row->facturaID,'factura_conceptos_vehiculo');?>
        <tr>
            <td>
                <?php 
                    
                        
                    $iva_total = (float) round(($concepto_row->precio_venta / 1.16)* .16 , 2); 
                    $base_iva = (float) round($concepto_row->precio_venta / 1.16 , 2);//$conceptos->concepto_importe;
                    
                    
                    if(($row->tipo_persona == 2) || ($row->tipo_persona == 3) ){
                    //precio de compra sin iva
                    $precio_compra = $row->total_compra_seminuevo * 1.16;
                    $precio_comision_con_iva = $concepto_row->precio_venta - $precio_compra;
                    //esto es la base del iva
                    $base_iva = (float) round($precio_comision_con_iva / 1.16 , 2);
                    //iva que se cobra
                    $iva_real = $base_iva * .16;

                    $iva_total = (float) round($iva_real , 2);   
                    }
                ?>
                <!--b>Subtotal:$<?php echo $base_iva;?> </b>
                <b>IVA:$<?php echo $iva_total;?> </b>
                <b>Total:$<?php echo $base_iva+$iva_total;?> </b-->
            </td>
        </tr>
        <?php 
        if($row->tipo_auto == 2){
            $folio_new = 'VU-'.$concepto_row->economico;
            $url = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/detalle?departamento=18&folio=';
        }
        if($row->tipo_auto == 1){
            $folio_new = 'VN-'.$concepto_row->economico;
            $url = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/detalle?departamento=17&folio=';
        }
        $dataFromApi = $this->curl->curlGet($url.$folio_new.'',true);
        $obj = json_decode($dataFromApi);
        $asientos = $obj->data;
        $total_abono = 0;
        $total_cargo = 0;
        $total_abono_formato = 0;
        $total_cargo_formato = 0;
        //$total_total = 0;

        
        ?>
        <?php 
            $formato_cargo = 0;
            $formato_abono = 0;
            foreach($asientos as $asi):
                $formato_cargo = 0;
                $formato_abono = 0;

        ?>
        <tr >
            <td><?php echo $asi->cuenta;?></td>
            <td><?php echo $asi->cuenta_descripcion;?></td>
            <td><?php echo number_format($asi->cargo, 2);?></td>
            <td><?php echo number_format($asi->abono, 2);?></td>
            <?php 
            $total_abono += (float) round($asi->abono, 2);
            $total_cargo += (float) round($asi->cargo, 2);
            ?>
        </tr>
        <?php 
            endforeach;          
        ?>            
            <tr>
                <td></td>
                <td></td>
                <td>---------------------</td>
                <td>---------------------</td>
            </tr>
            <tr>
                <td>SUMAS IGUALES:</td>
                <td></td>
                <td><?php echo number_format($total_abono, 2);?></td>
                <td><?php echo number_format($total_cargo, 2);?></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>            
        <?php endforeach;?>
        <?php endif;?>               
    </tbody>
    </table>
    <div style="position:absolute; bottom: 0; width: 100%;">
            <table>
                <tr>
                    <td style="width: 300px;">
                        ELABORADO POR:
                    </td>
                    <td>
                        REVISADO POR:
                    </td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>                    
                    <td style="width: 300px;">
                        --------------------------------
                    </td>
                    <td>
                        --------------------------------
                    </td>
                </tr>
                <tr>                    
                    <td style="width: 300px;">
                    </td>
                    <td>
                        CONTABILIDAD
                    </td>
                </tr>
                <tr><td><br><br></td></tr>                
            </table>
    </div>
</body>
</html>