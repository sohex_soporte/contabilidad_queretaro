@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')

<script>
$(document).ready(function(){
  $("#cargando").hide();
$('#enviar_factura').submit(function(event){
  event.preventDefault();

  var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando datos...</p>',
                            closeButton: false
                        });

  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/facturacion_vehiculos/generar_factura/<?php echo $factura->facturaID?>";
  ajaxJson(url,{},
            "POST",true,function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_error = json_response.error;
    if(obj_error == 0){
      dialog_load.modal('hide');
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/lista/");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_error == 1){
      dialog_load.modal('hide');
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+json_response.error_mensaje,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    


  });
});


$('#enviar_prefactura').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/facturacion/generar_prefactura/<?php echo $factura->facturaID?>";
  ajaxJson(url,{},
            "POST",true,function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_error = json_response.error;
    if(obj_error == 0){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/facturacion_vehiculos/lista/");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_error == 1){
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+json_response.error_mensaje,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    


  });
});

});
</script>

<?php echo exec('whoami'); ?>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Receptor</h5>
        <h6 class="card-text">
          <small class="text-muted">Razón Social:</small>
          <?php echo $factura->receptor_nombre?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">RFC:</small>
          <?php echo $factura->receptor_RFC?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Email:</small>
          <?php echo $factura->receptor_email?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Dirección:</small>
          <?php echo $factura->receptor_direccion?>
        </h6>
      </div>
    </div>


  </div>
  <div class="col-md-6">

    <div class="card">
      <div class="card-body">
        <h6 class="card-text">
          <h6 class="card-text">
            <small class="text-muted">Fecha:</small>
            <?php echo $factura->factura_fecha?>
          </h6>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Folio:</small>
          <?php echo $factura->factura_folio?>
          <small class="text-muted">Serie:</small>
          <?php echo $factura->factura_folio?>
        </h6>
        <h5 class="card-title">Emisor</h5>
        <h6 class="card-text">
          <small class="text-muted">Razón Social:</small>
          <?php echo $emisor->razon_social?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">RFC:</small>
          <?php echo $emisor->rfc?>
        </h6>
      </div>
    </div>

  </div>
</div>





<div class="row">
  <div class="col-12">
    Conceptos
  </div>

</div>
<div class="row" style="background:#fff;">
      <div class="col-12" id="factura_concepos_load">

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Clave</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Unidad</th>
              <th scope="col">Descripción</th>
              <th scope="col">Precio</th>
              <th scope="col">Importe</th>
              <th scope="col">IVA</th>
              
            
             
            </tr>
          </thead>
          <tbody>
            
          <?php foreach($conceptos as $conce):?>
            <tr>
              <td><?php echo $conce->clave_sat;?></td>
              <td><?php echo $conce->concepto_cantidad;?></td>
              <td><?php echo $conce->unidad_sat;?></td>
              <td><?php echo $conce->concepto_nombre;?></td>

              <?php
                         $precio_sin_iva = 0;
                          //CUANDO ES UN SEMINUEVO
                          if($factura->tipo_auto == 2){
                            /*
                              1	persona fisica
                              2	persona fisica con actividad empresarial
                              3	persona moral
                            */
                            //persona fisica , el iva es normal
                            if($factura->tipo_persona == 1){
                              $precio_sin_iva = (float) round($conce->precio_venta / 1.16, 2);
                              
                            }
                            if(($factura->tipo_persona == 2) || ($factura->tipo_persona == 3) ){
                              //precio de compra sin iva
                             /* $precio_compra1 = $factura->total_compra_seminuevo * 1.16;
                              $precio_comision_con_iva1 = $conce->precio_venta - $precio_compra1;
                              //esto es la base del iva
                              $precio_sin_iva = (float) round($precio_comision_con_iva1 / 1.16, 2);
                              */

                              $precio_compra1_sin_iva = $factura->total_compra_seminuevo / 1.16;

                              $comision = $conce->precio_venta - $factura->total_compra_seminuevo;
                              $comision_sin_iva =  $comision / 1.16;
                              
                              $total_real_sin_iva = $conce->precio_venta - ( $comision_sin_iva * .16 );

                              
                              //$precio_comision_con_iva1 = $conceptos->precio_venta - $precio_compra1;
                              //esto es la base del iva
                              //$precio_sin_iva = (float) round($precio_comision_con_iva1 / 1.16, 2);
                              $precio_sin_iva = (float) round($total_real_sin_iva, 2);
                              
                            }
                            
                            
                          }else{
                            //CUANDO ES UN NUEVO
                            //$iva_total =  number_format($conceptos->concepto_importe * .16 , 2, '.', '');
                            $precio_sin_iva = (float) round($conce->precio_venta/1.16, 2);
                          }

                        ?>
                <td>$<?php echo $precio_sin_iva;?></td>

                <?php 
                  $total_total = $precio_sin_iva;
                  
              ?>
              <td>$<?php echo $total_total;?></td>

              <?php 
                                $iva_total = 0;
                                $base_iva = 0;
                                //CUANDO ES UN SEMINUEVO
                                if($factura->tipo_auto == 2){
                                  /*
                                    1	persona fisica
                                    2	persona fisica con actividad empresarial
                                    3	persona moral
                                  */
                                  //persona fisica , el iva es normal
                                  if($factura->tipo_persona == 1){
                                    
                                    $iva_total = (float) round(($conce->precio_venta / 1.16)* .16 , 2); 
                                    $base_iva = (float) round($conce->precio_venta / 1.16 , 2);//$conceptos->concepto_importe;
                                  }
                                  if(($factura->tipo_persona == 2) || ($factura->tipo_persona == 3) ){
                                    //precio de compra sin iva
                                   /* $precio_compra = $factura->total_compra_seminuevo * 1.16;
                                    $precio_comision_con_iva = $conce->precio_venta - $precio_compra;
                                    //esto es la base del iva
                                    $base_iva = (float) round($precio_comision_con_iva / 1.16 , 2);
                                    //iva que se cobra
                                    $iva_real = $base_iva * .16;

                                    $iva_total = (float) round($iva_real , 2);   
                                    */
                                    $comision = $conce->precio_venta - $factura->total_compra_seminuevo;
                                    $comision_sin_iva =  $comision / 1.16;
                                    
                                    $total_real_sin_iva = $conce->precio_venta - ( $comision_sin_iva * .16 );

                                    $iva_total =  (float) round($comision_sin_iva * .16, 2);

                                    $base_iva = (float) round($comision, 2);
                                  }
                                  
                                  
                                }else{
                                  //CUANDO ES UN NUEVO
                                  $base_iva = (float) round($conce->precio_venta / 1.16, 2);
                                  $iva_total = (float) round($base_iva * .16 , 2);  
                                }

                                  
                                ?>
            
              <td>$<?php echo $iva_total;?></td>


            </tr>

            <tr>
                <td colspan="7">
                Clave vehicular:<?php echo $conce->clave_vehicular;?>&nbsp &nbsp
                VIN:<?php echo $conce->niv;?>&nbsp &nbsp
                Número:<?php echo $conce->numero;?>&nbsp &nbsp
                Fecha:<?php echo $conce->fecha;?>&nbsp &nbsp
                Aduana:<?php echo $conce->aduana;?>&nbsp &nbsp

               
                </td>

            </tr>

            
          <?php endforeach;?>
          </tbody>
        </table>

      </div>
    </div>










<div class="row">
  <div class="col-12">
    Toma de auto
  </div>

</div>
<div class="row" style="background:#fff;">
      <div class="col-12" id="factura_concepos_load">

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Monto adquisición</th>
              <th scope="col">Monto enajenación</th>
              <th scope="col">Calve vehicular</th>
              <th scope="col">Marca</th>
              <th scope="col">Tipo</th>
              <th scope="col">Modelo</th>
              <th scope="col">Número de motor</th>
              <th scope="col">NIV</th>
              <th scope="col">Valor</th>
            
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($toma_vehiculo as $tomas):?>
            <tr>
              <th ><?php echo $tomas->toma_monto_adquisicion;?></th>
              <td><?php echo $tomas->toma_monto_enajenacion;?></td>
              <td><?php echo $tomas->toma_clave_vehicular;?></td>
              <td><?php echo $tomas->toma_marca;?></td>
              <td><?php echo $tomas->toma_tipo;?></td>

              <th scope="col"><?php echo $tomas->toma_modelo;?></th>
              <th scope="col"><?php echo $tomas->toma_numero_motor;?></th>
              <th scope="col"><?php echo $tomas->toma_niv;?></th>
              <th scope="col"><?php echo $tomas->toma_valor;?></th>
              


              <td>
                
              </td>
            </tr>
          <?php endforeach;?>
          </tbody>
        </table>

      </div>
    </div>


<!--div class="row">
  <div class="col-md-12">

    <div class="card">

      <div class="card-body">
        <h5 class="card-title" align="center">CFDI Relacionados</h5>

        <div class="row">
          <div class="col-md-6">
            <small class="text-muted">Tipo de relación</small>

          </div>
          <div class="col-md-6">
            <small class="text-muted">UUID</small>
          </div>


          <?php foreach($factura_fcdi_relacionados as $relacion):?>
            <div class="col-md-6">
              <small class="text"><?php echo $relacion->relacionados_tipoRelacion;?></small>
            </div>
            <div class="col-md-6">
              <small class="text"><?php echo $relacion->relacion_UUID;?></small>
            </div>
            <hr/>
          <?php endforeach;?>

        </div>

      </div>
    </div>

  </div>

</div-->


<div class="row">
  <div class="col-md-8">

    <div class="card">

      <div class="card-body">
        <h5 class="card-title" align="center">Datos generales</h5>

        <div class="row">
          <div class="col-md-12">
            <small class="text">Tipo de comprobante: <?php echo $factura->factura_tipoComprobante;?></small>
            <br/>
            <small class="text">Forma de pago: <?php echo $factura->factura_formaPago;?></small>
            <br/>
            <small class="text">Método de pago: <?php echo $factura->factura_medotoPago;?></small>
            <br/>
            <small class="text">Total con letra: 
              <?php 
               $this->load->library('NumeroALetras');
             echo  $letras = NumeroALetras::convertir((float) round($precio_sin_iva + $iva_total, 2));
              ?>
            </small>

          </div>




        </div>

      </div>
    </div>

  </div>

 <div class="col-md-4">
   <div class="card">

     <div class="card-body">


       <div class="row">
         <div class="col-md-6">
           <small class="text">Subtotal:$<?php echo $precio_sin_iva;?> <?php //echo $this->Mgeneral->factura_subtotal2($facturaID);?></small>
           <br/>
           <small class="text">Descuento:$0<?php //echo $this->Mgeneral->factura_descuento2($facturaID);?></small>
           <br/>
           <small class="text">IVA: $<?php echo $iva_total;?><?php //echo $this->Mgeneral->factura_iva_total2($facturaID);?></small>
           <br/>
           <small class="text">Total: $<?php echo (float) round($precio_sin_iva + $iva_total, 2);?> <?php //echo $this->Mgeneral->factura_subtotal2($facturaID) + $this->Mgeneral->factura_iva_total2($facturaID);?></small>
         </div>




       </div>

     </div>
   </div>
 </div>


</div>




<div align="right">
  <label>Opciones:</label>

<!--form method="post" id="enviar_prefactura">
  <button id="enviar2" type="submit" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Prefactura</span>

  </button>
</form-->
<br/>

<form method="post" id="enviar_factura">
  <button title="Editar" id="enviar" type="submit" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Sellar y Firmar</span>

  </button>
</form>
<button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>

  <!--a href="<?php echo base_url()?>index.php/factura/generar_factura/<?php echo $facturaID;?>">
  <button id="enviar_factura" type="button" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Sellar y Firmar</span>

  </button>
</a-->
  <!--div align="center">
   <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
 </div-->
</div>


	
@endsection