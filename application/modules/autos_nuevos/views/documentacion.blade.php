@layout('template_blade/estructura')

@section('contenido')
<style>
.bordeado{
    border-style: ridge;
}
.fondo_rojo{
    background-color: #fab1a0;
}
</style>
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    DOCUMENTACIÓN FACTURACIÓN
</h4>
<div class="row">
    <div class="col-6">
        <!-- inicio de la PRIMERA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>C.: FACTURACION TRADICIONAL</h3>
            </div>

            <div class="col-md-12 bordeado <?php echo  isset($aux_array['file_pedido']) ? "" : "fondo_rojo";?>" >
                <div class="row">
                    <div class="col-md-10">
                        <input type="hidden" id="id_venta_unidad" value="">
                        <?php renderTexto('file_pedido', 'PEDIDO'); ?>
                        
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['file_pedido']) ? $aux_array['file_pedido'] : "#;"?>" download>ver archivo</a>

                    </div>
                </div>
            </div>
            
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['file_remision']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('file_remision', 'REMISION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['file_remision']) ? $aux_array['file_remision'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['ife']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife', 'IFE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['ife']) ? $aux_array['ife'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
           
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['curp']) ? "" : "fondo_rojo";?>" >
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp', 'CURP'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['curp']) ? $aux_array['curp'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['comprobante_domicilio']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['comprobante_domicilio']) ? $aux_array['comprobante_domicilio'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['formato_id_clt']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_id_clt', 'FORMATO ID DEL CLT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['formato_id_clt']) ? $aux_array['formato_id_clt'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['caja_anticipo']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['caja_anticipo']) ? $aux_array['caja_anticipo'] : "#";?>" download>ver archivo</a>

                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['formato_abono_ford_facturacion']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_facturacion', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['formato_abono_ford_facturacion']) ? $aux_array['formato_abono_ford_facturacion'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['situacion_fiscal']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['situacion_fiscal']) ? $aux_array['situacion_fiscal'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['ficha_seguimiento']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ficha_seguimiento', 'HOJA SICOP "FICHA DE SEGUIMIENTO"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['ficha_seguimiento']) ? $aux_array['ficha_seguimiento'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['contacto_cliente']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('contacto_cliente', 'HOJA SICOP "CORREO DE CONTACTO DEL CLIENTE"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['contacto_cliente']) ? $aux_array['contacto_cliente'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['vobo_area_procesos']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('vobo_area_procesos', 'VoBo AREA DE PROCESOS (MAIL)'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['vobo_area_procesos']) ? $aux_array['vobo_area_procesos'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['acta_constitutiva']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('acta_constitutiva', 'ACTA CONSTITUTIVA PERSONA MORAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['acta_constitutiva']) ? $aux_array['acta_constitutiva'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['situacion_fiscal']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['situacion_fiscal']) ? $aux_array['situacion_fiscal'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['comprobante_domicilio']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['comprobante_domicilio']) ? $aux_array['comprobante_domicilio'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['poder_representante_legal']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('poder_representante_legal', 'PODER REPESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">

                        <a href="<?php echo isset($aux_array['poder_representante_legal']) ? $aux_array['poder_representante_legal'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['ife_representante_legal']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife_representante_legal', 'IFE REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['ife_representante_legal']) ? $aux_array['ife_representante_legal'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['curp_representante_legal']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp_representante_legal', 'CURP REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['curp_representante_legal']) ? $aux_array['curp_representante_legal'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['consulta_lista_negra']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('consulta_lista_negra', 'CONSULTA DE LISTAS NEGRAS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['consulta_lista_negra']) ? $aux_array['consulta_lista_negra'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado <?php echo  isset($aux_array['carta_renuncia']) ? "" : "fondo_rojo";?>">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_renuncia', 'CARTA RENUNCIA EXT. GARANTIA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="<?php echo isset($aux_array['carta_renuncia']) ? $aux_array['carta_renuncia'] : "#";?>" download>ver archivo</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- fin de la PRIMERA columna ------------------------->
    </div>
    <div class="col-6">
        <!-- inicio de la SEGUNDA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>F.- FLOTILLAS</h3>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('orden_compra', 'ORDEN DE COMPRA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('cotizacion_ford', 'COTIZACION FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('boletin_street_program', 'BOLETIN STREET PROGRAM'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>D.: FORD CREDIT, BANCOS Y AUTOPCION</h3>
            </div>

            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprobacion_ford_credit', 'APROBACION FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO o ENG'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caratula_firma_otros_bancos', 'CARATULA DE FIRMA OTROS BANCOS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_credit', 'FORMATO DE BONO FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_ford_credit', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>E.- CONAUTO</h3>
            </div>

            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_credito_autorizada', 'CARTA CREDITO AUTORIZADA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprov_cond_auto_opcion', 'APROBACION DE COND. AUTO OPCION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_pago_cliente', 'RECIBO DE CAJA DE PAGO CLIENTE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 bordeado">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin de la SEGUNDA columna ------------------------->
    </div>
</div>
<hr />
<div class="row">
    <div class="col-12 text-end">
        <a href="<?php echo base_url(); ?>index.php/autos_nuevos/facturar_auto_primer_paso/<?php echo $id;?>/<?php echo $remision_id;?>">
            <button type="button" class="btn btn-info"> Facturar auto</button>
        </a>
    </div>
</div>


@endsection