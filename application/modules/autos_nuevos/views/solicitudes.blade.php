@layout('template_blade/estructura')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    SOLICITUDES DE FACTURACIÓN NUEVOS
</h4>

<div class="card mt-3">
    <div class="card-body">
        <div class="row " style="display:none">
            <div class="col-2">
                <div class="form-group">
                    <label><b>RFC</b></label>
                    <input type="text" id="numero_cliente" class="form-control buscar_enter">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label><b>Nombre cliente</b></label>
                    <input type="text" id="nombre" class="form-control buscar_enter">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label><b>Apellidos cliente</b></label>
                    <input type="text" id="apellidos" class="form-control buscar_enter">
                </div>
            </div>
            <div class="pull-right mt-4">
                <button type="button" class="btn btn-primary mt-2" onclick="app.filtrarTabla()"><i class="fa fa-search"></i> Buscar</button>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <table class="table table-striped" id="tabla_solicitudes" width="100%" cellspacing="0">
                </table>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')
<script src="{{ base_url('assets/components/jquery/dist/jquery.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="{{ base_url('assets/components/bootstrap-4.6/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js" referrerpolicy="no-referrer"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    var app;
    var table = 'table#tabla_solicitudes';
    var cliente_id = '';

    class Funciones {
        create_table() {
            $(table).DataTable(this.settings_table());
        }

        filtrarTabla = () => {
            $(table).DataTable().ajax.reload()
        }
        goToDocumentacion = (_this) => {
            window.location.href = PATH + '/autos_nuevos/ver_documentacion1?id=' + $(_this).data('guid')+ '&remision_id='+$(_this).data('remision_id')+ '&tipo_auto='+$(_this).data('tipo_auto')
        }
        settings_table() {
            return {
                ajax: {
                    url: API_URL_AUTOS + 'ventas/ventas_api/api/ventasAutos?tipo_venta_id=1&estatus_venta_id=5',
                    type: 'GET',
                    data: function(data) {},
                },
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                // processing: false,
                // serverSide: false,
                searching: false,
                bFilter: false,
                lengthChange: false,
                bInfo: true,
                order: [
                    [0, 'asc']
                ],
                columns: [{
                        title: 'Nombre cliente',
                        data: function(data) {
                            return data.nombre + ' ' + data.apellido_paterno + ' ' + data.apellido_materno
                        }
                    },
                    {
                        title: 'RFC',
                        data: 'rfc'
                    },
                    {
                        title: 'numero_serie',
                        data: 'numero_serie'
                    },
                    {
                        title: 'Modelo',
                        data: 'modelo'
                    },
                    {
                        title: 'Color',
                        data: 'color'
                    },
                    {
                        title: 'Total venta',
                        data: 'total_venta'
                    },
                    {
                        title: 'Acciones',
                        data: 'id',
                        width: '100px',
                        render: function(data, type, row) {
                            let documentacion = '<button title="Ver documentación credito" onclick="app.goToDocumentacion(this)" data-guid="' + row.id + '" data-remision_id="' + row.remision_id + '" data-tipo_auto="1" class="btn btn-primary"><i class="fas fa-file"></i></button>';
                            return documentacion
                        }

                    },
                ]
            }
        }

    }

    $(function() {
        app = new Funciones();
        app.create_table();
        $('.money_format').mask("#,##0.00", {
            reverse: true
        });
        $(".buscar_enter").keypress(function(e) {
            if (e.which == 13) {
                app.filtrarTabla();
            }
        });
    });
</script>
@endsection