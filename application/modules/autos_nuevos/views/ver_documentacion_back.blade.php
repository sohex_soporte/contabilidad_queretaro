<div class="row">
    <div class="col-6">
        <!-- inicio de la PRIMERA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>C.: FACTURACION TRADICIONAL</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <input type="hidden" id="id_venta_unidad" value="">
                        <?php renderTexto('file_pedido', 'PEDIDO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('file_remision', 'REMISION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife', 'IFE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp', 'CURP'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_id_clt', 'FORMATO ID DEL CLT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_facturacion', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ficha_seguimiento', 'HOJA SICOP "FICHA DE SEGUIMIENTO"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('contacto_cliente', 'HOJA SICOP "CORREO DE CONTACTO DEL CLIENTE"'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('vobo_area_procesos', 'VoBo AREA DE PROCESOS (MAIL)'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('acta_constitutiva', 'ACTA CONSTITUTIVA PERSONA MORAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('poder_representante_legal', 'PODER REPESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">

                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('ife_representante_legal', 'IFE REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('curp_representante_legal', 'CURP REPRESENTANTE LEGAL'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('consulta_lista_negra', 'CONSULTA DE LISTAS NEGRAS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_renuncia', 'CARTA RENUNCIA EXT. GARANTIA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- fin de la PRIMERA columna ------------------------->
    </div>
    <div class="col-6">
        <!-- inicio de la SEGUNDA columna ------------------------->
        <div class="row">
            <div class="col-md-12 mb-3">
                <h3>F.- FLOTILLAS</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('orden_compra', 'ORDEN DE COMPRA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('cotizacion_ford', 'COTIZACION FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('boletin_street_program', 'BOLETIN STREET PROGRAM'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>D.: FORD CREDIT, BANCOS Y AUTOPCION</h3>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprobacion_ford_credit', 'APROBACION FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO o ENG'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('caratula_firma_otros_bancos', 'CARATULA DE FIRMA OTROS BANCOS'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_credit', 'FORMATO DE BONO FORD CREDIT'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford_ford_credit', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <h3>E.- CONAUTO</h3>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('carta_credito_autorizada', 'CARTA CREDITO AUTORIZADA'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('aprov_cond_auto_opcion', 'APROBACION DE COND. AUTO OPCION'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('recibo_caja_pago_cliente', 'RECIBO DE CAJA DE PAGO CLIENTE'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <?php renderTexto('formato_abono_ford', 'FORMATO DE BONO FORD'); ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <a href="#">ver archivo</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin de la SEGUNDA columna ------------------------->
    </div>
</div>
<hr />
<div class="row">
    <div class="col-12 text-end">
        <a href="<?php echo base_url(); ?>index.php/autos_nuevos/facturar_auto_primer_paso/1">
            <button type="button" class="btn btn-info"> Facturar auto</button>
        </a>
    </div>
</div>