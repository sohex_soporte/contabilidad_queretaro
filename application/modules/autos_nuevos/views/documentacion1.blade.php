@layout('template_blade/estructura')

@section('contenido')
<script type="text/javascript">
   function cambiar_estatus(campo){
    var url ="<?php echo base_url()?>index.php/autos_nuevos/cambiar_estatus1/<?php  echo $id;?>/"+campo;
        ajaxJson(url,{},
                  "POST","",function(result){
        });
   }

   function cambiar_estatus_aprobado(campo){
    var url ="<?php echo base_url()?>index.php/autos_nuevos/cambiar_estatus_aprobado/<?php  echo $id;?>/"+campo;
        ajaxJson(url,{},
                  "POST","",function(result){
                    location.reload();
        });
   }

      
</script>


     

<style>
.bordeado{
    border-style: ridge;
}
.fondo_rojo{
    /*background-color: #fab1a0;*/
}
.checkbox_tamano{
            width: 20px;
            height: 20px;
        }
</style>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    DOCUMENTACIÓN FACTURACIÓN
    
</h4>

<div class="col-md-12 bordeado " >
        <div class="row">
            <div class="col-md-5">
                Documento     
            </div>
            <div class="col-md-1">
                Aprobado
            </div>
            <div class="col-md-4">
                Tipo persona
            </div>
            <div class="col-md-1">
                Encargado
            </div>
        </div>
    </div>
<?php foreach($rows as $row):?>
    <div class="col-md-12 bordeado " >
        <div class="row">
            <div class="col-md-5">
                <?php echo $row->DOCUMENTO;?>        
            </div>
            <div class="col-md-1">
                <?php

                $this->db->where('id_factura_solicitud',$valores->id);
                $this->db->where('id_solicitud_factura',$row->id);
                $res = $this->db->get('solicitudes_factura_campos')->row();
                if(is_object($res)):
                ?>
                    <input class="checkbox_tamano" onclick="cambiar_estatus('<?php echo $row->id;?>')" type="checkbox" value="" id="file_pedido" name="file_pedido" <?php echo  $res->aprobado == 1 ? "checked" : "";?>>
                <?php else:?>
                    <input class="checkbox_tamano" onclick="cambiar_estatus('<?php echo $row->id;?>')" type="checkbox" value="" id="file_pedido" name="file_pedido" <?php echo "";?>>
                <?php endif;?>
                
            </div>
            <div class="col-md-4">
                <?php 
                    if($row->FISICA == 1){
                        echo "Fisica/";
                    }
                    if($row->FISICA_ACT == 1){
                        echo "Fisica actividad empresarial/";
                    }
                    if($row->MORAL == 1){
                        echo "Moral/";
                    }
                ?>
            </div>
            <div class="col-md-1">
                <?php echo $row->ENCARGADO;?>
            </div>
        </div>
    </div>
<?php endforeach;?>

<hr />
<?php if($valores->aprobado == 0):?>
<div class="row">
    <div class="col-12 text-end">
        
            <button type="button" onclick="cambiar_estatus_aprobado('aprobado')" class="btn btn-info"> Aprobar documentación</button>
        
    </div>
</div>
<?php endif;?>
<?php if($valores->aprobado == 1):?>
<div class="row">
    <div class="col-12 text-end">
        <a href="<?php echo base_url(); ?>index.php/autos_nuevos/facturar_auto_primer_paso/<?php echo $id;?>/<?php echo $remision_id;?>/<?php echo $tipo_auto?>">
            <button type="button" class="btn btn-info"> Facturar auto</button>
        </a>
    </div>
</div>
<?php endif;?>


@endsection