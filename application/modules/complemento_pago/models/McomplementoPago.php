<?php
/**

 **/
class McomplementoPago extends CI_Model{

    public $root;
    public $xml;


    public $mifecha ; 
    public $NuevaFecha ; 
    public $NuevaHora ; 

    public $noCertificado = "30001000000400002434";

    public $fact_serie;                             // 4.1 Número de serie.
    public $fact_folio;             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
    public $NoFac;         // 4.3 Serie de la factura concatenado con el número de folio.
    public $fact_tipcompr;                             // 4.4 Tipo de comprobante.
    public $fact_exportacion;                            // 4.5 Atributo requerido para expresar si el comprobante ampara una operación de exportación.
    public $subTotal;                               // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    public $IVA;                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
    public $total;                               // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    public $fecha_fact; // 4.11 Fecha y hora de facturación.
    public $NumCtaPago;                          // 4.12 Número de cuenta (sólo últimos 4 dígitos, opcional).
    public $LugarExpedicion;                         // 4.17 Lugar de expedición (código postal).
    public $moneda;                           // 4.18 Moneda
    public $SumaPagos = 0;
 


    ### 9. DATOS GENERALES DEL EMISOR #################################################  
    public $emisor_rs ;  // 9.1 Nombre o Razón social.
    public $emisor_rfc ;  // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
    public $emisor_ClaRegFis ; // 9.3 Clave del Régimen fiscal.    

    ### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
    public $RFC_Recep ="";  // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
    
    public $receptor_rfc;  // 10.3 RFC.
    public $receptor_rs;  // 10.4 Nombre o razón social.
    public $DomicilioFiscalReceptor;             // 10.5 Domicilio fiscal del Receptor (código postal).
    public$RegimenFiscalReceptor;                 // 10.6 Régimen fiscal del receptor.
    public $UsoCFDI = "S01";  // Uso del CFDI.

    public $SumaBases;

   

    /**

     **/
    public function __construct()
    {
        parent::__construct();

        
    }

    public function cargarInicio(){

        $this->mifecha = date('Y-m-d H:i:s'); 
        $this->NuevaFecha = strtotime ( '+0 hour' , strtotime ($this->mifecha) ) ; 
        $this->NuevaFecha = strtotime ( '-1 minute' , $this->NuevaFecha ) ; 
        $this->NuevaFecha = strtotime ( '+0 second' , $this->NuevaFecha ) ; 
        $this->NuevaHora = date ( 'H:i:s' , $this->NuevaFecha); 

       // if (strlen($this->RFC_Recep)==12){$this->RFC_Recep = " ".$this->RFC_Recep; }else{$this->RFC_Recep = $this->RFC_Recep;}  // 10.2 Al RFC de personas morales se le antecede un espacio en blanco para que su longitud sea de 13 caracteres ya que estos son de longitud 12.
        //$this->receptor_rfc = $this->RFC_Recep;

        $this->xml = new DOMdocument('1.0', 'UTF-8'); 
        $this->root = $this->xml->createElement("cfdi:Comprobante");
        $this->root = $this->xml->appendChild($this->root); 
       // $this->conceptos();

    }


    public function imprimir(){

        $cfdi = $this->xml->saveXML();

        //echo htmlspecialchars($cfdi);
        echo $cfdi;
        
    }

    public function obtenerXml(){

        $cfdi = $this->xml->saveXML();

        return $cfdi;
        
    }

    function cabecera(){
        #== 11.2 Se crea e inserta el primer nodo donde se declaran los namespaces ======
        $this->cargaAtt($this->root, array(
            "xmlns:pago20"=>"http://www.sat.gob.mx/Pagos20",
            "xmlns:cfdi"=>"http://www.sat.gob.mx/cfd/4",
            "xmlns:xs"=>"http://www.w3.org/2001/XMLSchema",
            "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
            "xsi:schemaLocation"=>"http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd"
        )
       );
    }

    function datosGenerales(){
        #== 11.3 Rutina de integración de nodos =========================================
        $this-> cargaAtt($this->root, array(
            "Version"=>"4.0", 
            "Serie"=>$this->fact_serie,
            "Folio"=>$this->fact_folio,
            "Fecha"=>$this->fecha_fact,// date("Y-m-d")."T". $this->NuevaHora,
            "NoCertificado"=>$this->noCertificado,
            "SubTotal"=>$this->subTotal,
            "Moneda"=>$this->moneda,
            "Total"=>$this->total,
            "TipoDeComprobante"=>$this->fact_tipcompr,
            "Exportacion"=>$this->fact_exportacion,
            "LugarExpedicion"=>$this->LugarExpedicion
        )
        );
    }

    function datosEmisor(){
        $emisor = $this->xml->createElement("cfdi:Emisor");
        $emisor = $this->root->appendChild($emisor);
        $this->cargaAtt($emisor, array("Rfc"=>$this->emisor_rfc,
                            "Nombre"=>$this->emisor_rs,
                            "RegimenFiscal"=>$this->emisor_ClaRegFis
                            )
                        );

    }

    function datosReceptor(){
        $receptor = $this->xml->createElement("cfdi:Receptor");
        $receptor = $this->root->appendChild($receptor);
        $this->cargaAtt($receptor, array(
                    "Rfc"=>$this->receptor_rfc,
                    "Nombre"=>$this->receptor_rs,
                    "DomicilioFiscalReceptor"=>$this->DomicilioFiscalReceptor,
                    "RegimenFiscalReceptor"=>$this->RegimenFiscalReceptor,
                    "UsoCFDI"=>$this->UsoCFDI
                )
            );
    }

    function conceptos($data){

        $conceptos = $this->xml->createElement("cfdi:Conceptos");
        $conceptos = $this->root->appendChild($conceptos);
        $concepto = $this->xml->createElement("cfdi:Concepto");
        $concepto = $conceptos->appendChild($concepto);
        $this->cargaAtt($concepto, array(
                "ClaveProdServ"=>$data["ClaveProdServ"],
                "Cantidad"=>$data["Cantidad"],
                "ClaveUnidad"=>$data["ClaveUnidad"],
                "Descripcion"=>$data["Descripcion"],
                "ValorUnitario"=>$data["ValorUnitario"],
                "Importe"=>$data["Importe"],
                "ObjetoImp"=>$data["ObjetoImp"],
            )
        );
             
    }


    public function datos_complemento($data, $data2){
        $complemento = $this->xml->createElement("cfdi:Complemento");
        $complemento = $this->root->appendChild($complemento);

        $pagos = $this->xml->createElement("pago20:Pagos");
        $pagos = $complemento->appendChild($pagos);
        $this->cargaAtt($pagos, array(
                "Version"=>"2.0",
            )
        );

        $pago = $this->xml->createElement("pago20:Totales");
        $pago = $pagos->appendChild($pago);
        $this->cargaAtt($pago, array(
                "MontoTotalPagos"=>number_format($data['SumaPagos'],2,'.','')//total de los pagos que se desprenden de los nodos Pago
            )
        );

        $pago = $this->xml->createElement("pago20:Pago");
        $pago = $pagos->appendChild($pago);
        $this->cargaAtt($pago, array(
                "FechaPago"=>$data['FechaPago'],
                "FormaDePagoP"=>$data['FormaDePagoP'],
                "MonedaP"=>$data['MonedaP'],
                "TipoCambioP"=>$data['TipoCambioP'],
                "Monto"=>$data['Monto'],
                "NumOperacion"=>$data['NumOperacion']// esto es una referencia
            )
        );
        $this->agregar_pago($data2, $pago);
    }


    public function agregar_pago($data, $pago){
        $docRel = $this->xml->createElement("pago20:DoctoRelacionado");
        $docRel = $pago->appendChild($docRel);
        $this->cargaAtt($docRel, array(
            "IdDocumento"=>$data['IdDocumento'],
            "Serie"=>$data['Serie'],
            "Folio"=>$data['Folio'],
            "MonedaDR"=>$data['MonedaDR'],
            "EquivalenciaDR"=>$data['EquivalenciaDR'],
            "NumParcialidad"=>$data['NumParcialidad'],
            "ImpSaldoAnt"=>$data['ImpSaldoAnt'],
            "ImpPagado"=>$data['ImpPagado'],
            "ImpSaldoInsoluto"=>$data['ImpSaldoInsoluto'],
            "ObjetoImpDR"=>$data['ObjetoImpDR'],
            )
        );
    }



    public function cadenaOriginal(){
        libxml_use_internal_errors(true);
        $xsl = new DOMDocument;
        $xsl->load($this->config->item('url_real').'cfdi41/xml/lib/files/cadenaoriginal_4_0.xslt');
        // Configura el procesador
        $xml_sx = simplexml_load_string($this->xml->saveXML());
        $proc = new XSLTProcessor;
        $proc->importStyleSheet($xsl); // adjunta las reglas XSL
        $cadena_original =  $proc->transformToXML($xml_sx);
        return $cadena_original;
      }
  
      public function sellar(){
           #== 11.8 Proceso para obtener el sello digital del archivo .pem.key ========
        $keyid = openssl_get_privatekey(file_get_contents($this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.key.pem'));
        openssl_sign($this->cadenaOriginal(), $crypttext, $keyid, OPENSSL_ALGO_SHA256);
        openssl_free_key($keyid);
            
        #== 11.9 Se convierte la cadena digital a Base 64 ==========================
        $sello = base64_encode($crypttext); // Firma.
  
  
        #== 11.10 Proceso para extraer el certificado del sello digital ============
        $file = $this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.cer.pem';      // Ruta al archivo
        $datos = file($file);
        $certificado = ""; 
        $carga=false;  
        for ($i=0; $i<sizeof($datos); $i++){
            if (strstr($datos[$i],"END CERTIFICATE")) $carga=false;
            if ($carga) $certificado .= trim($datos[$i]);
  
            if (strstr($datos[$i],"BEGIN CERTIFICATE")) $carga=true;
        } 
  
        $this->root->setAttribute("Sello",$sello);
        $this->root->setAttribute("Certificado",$certificado);   # Certificado.
  
    }

    public function guardar_xml($ruta){
        #== Fin de la integración de nodos =========================================
        $NomArchCFDI = $ruta;//$SendaCFDI."PreCFDI-40_Factura_".$NoFac.".xml";
        
        #=== 11.12 Se guarda el archivo .XML antes de ser timbrado =======================
        $cfdi = $this->xml->saveXML();
        $this->xml->formatOutput = true;             
        $this->xml->save($NomArchCFDI); // Guarda el archivo .XML (sin timbrar) en el directorio predeterminado.
        unset($this->xml);
        
        #=== 11.13 Se dan permisos de escritura al archivo .xml. =========================
        chmod($NomArchCFDI, 0777); 
    }


    public function timbrar(){
        
    }


    function cargaAtt(&$nodo, $attr){
        global $xml, $cadena_original;
        $quitar = array('sello'=>1,'noCertificado'=>1,'certificado'=>1);
        foreach ($attr as $key => $val){
            $val = preg_replace('/\s\s+/', ' ', $val);
            $val = trim($val);
            if (strlen($val)>0){
                 $val = str_replace("|","/",$val);
                 $nodo->setAttribute($key,$val);
                 if (!isset($quitar[$key])) 
                   if (substr($key,0,3) != "xml" &&
                       substr($key,0,4) != "xsi:")
                    $cadena_original .= $val . "|";
            }
         }
     }

     public function getNumeroCertificado(){
        $d = openssl_x509_parse(
            $this->der2pem($this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.key.pem'),
            true
        );

        if($d && !empty($d['serialNumberHex'])) {
            $hex = $d['serialNumberHex'];
            $num = '';
            for($i=0;$i<strlen($hex);$i+=2) {
                $num .= chr(hexdec(substr($hex, $i, 2)));
            }
            return $num;
        }

        if($d && !empty($d['serialNumber'])) {
            $number = $d['serialNumber'];
            $hexvalues = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
            $hexval = '';
            while($number != '0'){
                $hexval = $hexvalues[bcmod($number,'16')].$hexval;
                $number = bcdiv($number,'16',0);
            }
            $number = '';
            $len = strlen($hexval);
            for($i=0; $i<$len;$i+=2){
                $number .=  substr($hexval, $i+1, 1);
            }

            return $number;
        }

        return null;
    }

    private static function der2pem($der_data) {
        return '-----BEGIN CERTIFICATE-----'.PHP_EOL
            .chunk_split(base64_encode($der_data), 64, PHP_EOL)
            .'-----END CERTIFICATE-----'.PHP_EOL;
    }

    public function toBase64(){
        return str_replace(
            array('\n', '\r'),
            '',
            base64_encode($this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.key.pem')
        );
    }



    

   
}