<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Complemento_pago extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        // $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        // date_default_timezone_set('America/Mexico_City');
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
       
    }


    public function lista_complementos(){
        
        $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
        $datos['complementos'] = $this->Mgeneral->get_table('complemento_pago');
        $this->blade->render('lista',$datos);
    }


    public function nueva($id_complemento= null){

        $datos['tipo_comprobante'] = $this->Mgeneral->get_table('ca_tipo_comprobante');
        $datos['forma_pagos'] = $this->Mgeneral->get_table('ca_formas_pago');

        if($id_complemento == null){
            //$datos['clientes'] = $this->Mgeneral->get_table('clientes');
            $datos['complemento'] = "";
            $datos['complemento_datos'] = "";
            $datos['complementoID'] = $id_complemento;
            $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');

        }else{
            //$datos['clientes'] = $this->Mgeneral->get_table('clientes');
            $datos['complemento'] = $this->Mgeneral->get_row('complementoID',$id_complemento,'complemento_pago');
            $datos['complemento_datos'] = $this->Mgeneral->get_row('id_complemento',$id_complemento,'complemento_datos');
            $datos['complementoID'] = $id_complemento;
            $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
        }

        
        $this->blade->render('nueva',$datos);
    }

    public function ver_relaciones_complementos(){
        $id_factura = $this->input->post('id_factura');
        $rows = $this->Mgeneral->get_result('id_complemento',$id_factura,'complemento_cfdi_relacionado_p');
        $rows_complemento = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');
        $html_uuid = "";
        foreach($rows as $row):
          if($rows_complemento->sat_error != "0"){
          $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                        <th scope="row">'.$row->uuid.'</th>
                        <td>'.$row->numParcialidad.'</td>
                        <td>'.$row->impSaldoAnt.'</td>
                        <td>'.$row->impPagado.'</td>
                        <td>'.$row->impSaldoInsoluto.'</td>
                        <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_complemento/'.$row->ccrpId.'" flag="'.$row->ccrpId.'" id="delete2'.$row->ccrpId.'" class="eliminar_relacion_p"><button type="button" class="btn btn-danger">borrar</button></a></td>
                      </tr>';
          }else{
            $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                          <th scope="row">'.$row->uuid.'</th>
                          <td>'.$row->numParcialidad.'</td>
                          <td>'.$row->impSaldoAnt.'</td>
                          <td>'.$row->impPagado.'</td>
                          <td>'.$row->impSaldoInsoluto.'</td>
                          <td></td>
                        </tr>';
          }
        endforeach;
  
        echo '<table class="table" style="background:#fff">
            <thead>
              <tr>
  
                <th scope="col">UUID</th>
                <th scope="col">No.</th>
                <th scope="col">Por pagar</th>
                <th scope="col">Imp. pagado</th>
                <th scope="col">saldo</th>
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
              '.$html_uuid.'
            </tbody>
          </table>';

          exit();
      }
  
      public function eliminar_relacion_complemento($id){
        $this->Mgeneral->delete_row('complemento_cfdi_relacionado_p','ccrpId',$id);
      }


      public function relaciones_complementos(){
        $id_factura = $this->input->post('id_complemento');
        $uuid = $this->input->post('uuid');
        $seriep = $this->input->post('seriep');
        $foliop = $this->input->post('foliop');
        $moneda_pe = $this->input->post('moneda_pe');
        $tipo_cambio_p = $this->input->post('tipo_cambio_p');
        $metodo_pago_p = $this->input->post('metodo_pago_p');
        $numero_parcial = $this->input->post('numero_parcial');
        $deuda_pagar = $this->input->post('deuda_pagar');
        $importe_pagado = $this->input->post('importe_pagado');
        $nuevo_saldo = $this->input->post('nuevo_saldo');
  
        $data['uuid'] = $uuid;
        $data['id_complemento'] = $id_factura;
        $data['serie'] = $seriep;
        $data['folio'] = $foliop;
        $data['monedaDR'] = $moneda_pe;
        $data['tipoCambioDR'] = $tipo_cambio_p;
        $data['metodoDePagoDR'] = $metodo_pago_p;
        $data['numParcialidad'] = $numero_parcial;
        $data['impSaldoAnt'] = $deuda_pagar;
        $data['impPagado'] = $importe_pagado;
        $data['impSaldoInsoluto'] = $nuevo_saldo;
        $data['fecha_creacion'] = time();
  
  
  
        $this->Mgeneral->save_register('complemento_cfdi_relacionado_p', $data);
  
        $rows = $this->Mgeneral->get_result('id_complemento',$id_factura,'complemento_cfdi_relacionado_p');
        $html_uuid = "";
        foreach($rows as $row):
          $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                        <th scope="row">'.$row->uuid.'</th>
                        <td>'.$row->numParcialidad.'</td>
                        <td>'.$row->impSaldoAnt.'</td>
                        <td>'.$row->impPagado.'</td>
                        <td>'.$row->impSaldoInsoluto.'</td>
                        <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_complemento/'.$row->ccrpId.'" flag="'.$row->ccrpId.'" id="delete2'.$row->ccrpId.'" class="eliminar_relacion_p"><button type="button" class="btn btn-danger">borrar</button></a></td>
                      </tr>';
        endforeach;
  
        echo '<table class="table" style="background:#fff">
            <thead>
              <tr>
  
                <th scope="col">UUID</th>
                <th scope="col">No.</th>
                <th scope="col">Por pagar</th>
                <th scope="col">Imp. pagado</th>
                <th scope="col">saldo</th>
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
              '.$html_uuid.'
            </tbody>
          </table>';

          exit();
  
      }



      public function ver_relaciones_facturas(){
        $id_factura = $this->input->post('id_factura');
        $rows = $this->Mgeneral->get_result('relacion_complemento',$id_factura,'complemento_cdfi_relacionado');
        $html_uuid = "";
        foreach($rows as $row):
          $html_uuid .= '<tr id="borrar_'.$row->ccrId.'">
                        <th scope="row">'.$row->tipo.'</th>
                        <td>'.$row->uuid.'</td>
                        <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_id/'.$row->ccrId.'" flag="'.$row->ccrId.'" id="delete'.$row->ccrId.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                      </tr>';
        endforeach;
  
        echo '<table class="table" style="background:#fff">
            <thead>
              <tr>
  
                <th scope="col">Relación</th>
                <th scope="col">UUID</th>
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
              '.$html_uuid.'
            </tbody>
          </table>';

          exit();
      }


      public function eliminar_relacion_id($id){
        $this->Mgeneral->delete_row('complemento_cdfi_relacionado','ccrId',$id);
      }




    public function buscar_factura($rfc=null){

        $rows = $this->Mgeneral->get_facturas_ppd_vehiculo($rfc);//get_result('receptor_RFC',$rfc,'factura');
        $html_facturas = "";
        foreach($rows as $row):
          $total = $this->Mgeneral->factura_subtotal2($row->facturaID) + $this->Mgeneral->factura_iva_total2($row->facturaID);
          $html_facturas .= '<tr id="'.$row->receptor_RFC.'">
                        <th scope="row">'.$row->receptor_RFC.'</th>
                        <td>'.$row->factura_medotoPago.'</td>
                        <td>'.$total.'</td>
                        <td>'.(($this->Mgeneral->factura_subtotal2($row->facturaID) + $this->Mgeneral->factura_iva_total2($row->facturaID))-$this->Mgeneral->complementoPagoPagado($row->sat_uuid)).'</td>
                        
                        <td><button type="button" class="btn elegir_fac" id="'.$row->sat_uuid.'" >Seleccionar</button></td>
                      </tr>';
        endforeach;
  
  
        echo '<table class="table" style="background:#fff">
            <thead>
              <tr>
  
                <th scope="col">RFC</th>
                <th scope="col">Metodo pago</th>
                <th scope="col">Total</th>
                <th scope="col">Saldo</th>
               
                <th scope="col">Opciones</th>
              </tr>
            </thead>
            <tbody>
              '.$html_facturas.'
            </tbody>
          </table>';
          exit();
      }

      public function datos_pago($uuid){

        $fatcura = $this->Mgeneral->get_row('sat_uuid',$uuid,'factura_vehiculo');
  
        $saldo = $this->Mgeneral->complementoPagoPagado($uuid);
        $total_saldo =  (($this->Mgeneral->factura_subtotal2($fatcura->facturaID) + $this->Mgeneral->factura_iva_total2($fatcura->facturaID))-$saldo);
  
        $saldo1 = $this->Mgeneral->complementoPagoPagado($uuid);
  
        $numero_pago = $this->Mgeneral->complemento_numero_pagos($uuid) + 1;
        $data['numero_pago'] = $numero_pago;
        $data['saldo'] = $total_saldo;
        $data['abonos'] =  $saldo1;
        echo json_encode($data);
      }


      public function crear_pre_complemento(){
        $emisor = $this->Mgeneral->get_row('id',1,'datos');
        $id_complemento = get_guid();
        $data['complementoID'] = $id_complemento;
        $data['status'] = 1;
        $data['emisorRFC'] = $emisor->rfc;
        $data['emisorRegimenFiscal'] = $emisor->regimen;
        $data['emisorNombre'] = $emisor->razon_social;
        $data['receptorUsoCFDI'] = "CP01";
        $data['monedaG'] = "XXX";
        $data['tipoDeComprobante'] = "P";
        $data['lugarExpedicion'] = codigo_postal();
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        //$data['receptor_uso_CFDI'] = "G03";
  
        $id_tabla = $this->Mgeneral->save_register('complemento_pago', $data);
  
        $data_c['folio'] = $id_tabla;
        $data_c['serie'] = $id_tabla.'-'.date('Y-m-d');
        $this->Mgeneral->update_table_row('complemento_pago',$data_c,'complementoID',$id_complemento);
  
        $data_complemento['id_complemento'] = $id_complemento;
        $this->Mgeneral->save_register('complemento_datos', $data_complemento);
        redirect('complemento_pago/nueva/'.$id_complemento);
  
      }



      public function guarda_datos($id_complemento=null){
        /*$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'required');
        $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente', 'required');
        $this->form_validation->set_rules('factura_moneda', 'factura_moneda', 'required');
        $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion', 'required');
        $this->form_validation->set_rules('factura_fecha', 'factura_fecha', 'required');
        $this->form_validation->set_rules('receptor_email', 'receptor_email', 'required');
        $this->form_validation->set_rules('factura_folio', 'factura_folio', 'required');
        $this->form_validation->set_rules('factura_serie', 'factura_serie', 'required');
        $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion', 'required');
        $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago', 'required');
        $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago', 'required');
        $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante', 'required');
        $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC', 'required');
        $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI', 'required');
        $this->form_validation->set_rules('pagada', 'pagada', 'required');
        $this->form_validation->set_rules('comentario', 'comentario', 'required');*/
        $response = validate($this);
        $response['status'] = true;
        if($response['status']){

        
          if($id_complemento == null){

            $emisor = $this->Mgeneral->get_row('id',1,'datos');
            $id_complemento = get_guid();
            $data['complementoID'] = $id_complemento;
            $data['status'] = 1;
            $data['emisorRFC'] = $emisor->rfc;
            $data['emisorRegimenFiscal'] = $emisor->regimen;
            $data['emisorNombre'] = $emisor->razon_social;
            $data['receptorUsoCFDI'] = "CP01";
            $data['monedaG'] = "XXX";
            $data['tipoDeComprobante'] = "P";
            $data['lugarExpedicion'] = codigo_postal();
            $data['fecha_creacion'] = date('Y-m-d H:i:s');
            //$data['receptor_uso_CFDI'] = "G03";
      
            $id_tabla = $this->Mgeneral->save_register('complemento_pago', $data);
      
            $data_c['folio'] = $id_tabla;
            $data_c['serie'] = $id_tabla.'-'.date('Y-m-d');
            $this->Mgeneral->update_table_row('complemento_pago',$data_c,'complementoID',$id_complemento);
      
            $data_complemento['id_complemento'] = $id_complemento;
            $this->Mgeneral->save_register('complemento_datos', $data_complemento);
          }

           
  
  
          $data['receptorNombre'] = $this->input->post("receptor_nombre");
          $data['receptorId'] = $this->input->post("receptor_id_cliente");
          $data['lugarExpedicion'] = $this->input->post("lugarExpedicion");
          $data['fechaPago'] = $this->input->post("fecha_pago");
          $data['receptor_email'] = $this->input->post("receptor_email");
          $data['folio'] = $this->input->post("factura_folio");
          $data['serie'] = $this->input->post("factura_serie");
          $data['comentario'] = $this->input->post("comentario");
          $data['receptorRFC'] = $this->input->post("receptorRFC");
          $this->Mgeneral->update_table_row('complemento_pago',$data,'complementoID',$id_complemento);
  
          $data_complemento['fecha'] = $this->input->post("complemento_fecha");
          $data_complemento['forma_pago'] = $this->input->post("complemento_forma_pago");
          $data_complemento['complemento_moneda'] = 'MXN';//$this->input->post("complemento_moneda");
          $data_complemento['tipo_cambio'] = $this->input->post("complemento_tipoCambio");
          $data_complemento['total_pago'] = $this->input->post("complemento_totalPago");
          $data_complemento['no_operacion'] = $this->input->post("complemento_no_operacion");
          $data_complemento['banco_ordenante'] = $this->input->post("complemento_banco_ordenante");
          $data_complemento['cta_ordenante'] = $this->input->post("complemento_cta_ordenante");
          $data_complemento['rfc_beneficiario'] = $this->input->post("complemento_rfc_beneficiario");
          $data_complemento['cta_beneficiario'] = $this->input->post("complemento_cta_beneficiario");
          $data_complemento['rfc_ordenante'] = $this->input->post("complemento_rfc_ordenante");
           $this->Mgeneral->update_table_row('complemento_datos',$data_complemento,'id_complemento',$id_complemento);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));

      exit();
  
      }
  

    public function genera_complemento_pago($id_complemento){

       $complemento_pago = $this->Mgeneral->get_row('complementoID',$id_complemento,'complemento_pago');
       $complemento_datos = $this->Mgeneral->get_row('id_complemento',$id_complemento,'complemento_datos');

       $complemento_cfdi_relacionado_p = $this->Mgeneral->get_result('id_complemento',$id_complemento,'complemento_cfdi_relacionado_p');
       $complemento_cfdi_relacionado = $this->Mgeneral->get_result('relacion_complemento',$id_complemento,'complemento_cdfi_relacionado');
      

        $this->load->model('McomplementoPago', '', TRUE);
        $cfdi = $this->McomplementoPago;
        $cfdi->cargarInicio();
        $cfdi->cabecera();
       
        $cfdi->fact_serie        = $complemento_pago->serie;//"A";                             // 4.1 Número de serie.
        $cfdi->fact_folio        = $complemento_pago->folio;//mt_rand(1000, 9999);             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
        //$cfdi->NoFac             = $fact_serie.$fact_folio;         // 4.3 Serie de la factura concatenado con el número de folio.
        $cfdi->fact_tipcompr     = "P";                             // 4.4 Tipo de comprobante.
        $cfdi->fact_exportacion  = "01";                            // 4.5 Atributo requerido para expresar si el comprobante ampara una operación de exportación.
        $cfdi->subTotal          = 0;                               // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
        $cfdi->IVA               = 0;                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
        $cfdi->total             = 0;                               // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
        $cfdi->fecha_fact        = $complemento_pago->fechaPago;//time();//date("Y-m-d")."T".date("H:i:s"); // 4.11 Fecha y hora de facturación.
        $cfdi->NumCtaPago        = "6473";                          // 4.12 Número de cuenta (sólo últimos 4 dígitos, opcional).
        $cfdi->LugarExpedicion   = $complemento_pago->lugarExpedicion;//"45079";                         // 4.17 Lugar de expedición (código postal).
        $cfdi->moneda            = "XXX";                           // 4.18 Moneda
        $SumaPagos = 0;
        $cfdi->datosGenerales();


        ### 9. DATOS GENERALES DEL EMISOR #################################################  
        $cfdi->emisor_rs = "ESCUELA KEMPER URGATE";  // 9.1 Nombre o Razón social.
        $cfdi->emisor_rfc = "EKU9003173C9";  // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
        $cfdi->emisor_ClaRegFis = "601"; // 9.3 Clave del Régimen fiscal.  
        $cfdi->datosEmisor();  

        ### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
        $cfdi->receptor_rfc = "MASO451221PM4";  // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
        $cfdi->receptor_rs  = "MARIA OLIVIA MARTINEZ SAGAZ";  // 10.4 Nombre o razón social.
        $cfdi->DomicilioFiscalReceptor = "80290";             // 10.5 Domicilio fiscal del Receptor (código postal).
        $cfdi->RegimenFiscalReceptor = "616";                 // 10.6 Régimen fiscal del receptor.
        $cfdi->UsoCFDI = "CP01";                               // Uso del CFDI.
        $cfdi->datosReceptor();


        //Conceptos ------------------------------
        $concepto = array();
        $concepto['ClaveProdServ'] = '84111506'; // 6.1 Clave del SAT correspondiente al artículo o servicio (consultar el catálogo de productos del SAT).$concepto_fac->clave_sat;
        $concepto['Cantidad'] = 1;//$concepto_fac->concepto_cantidad;
        $concepto['ClaveUnidad'] = 'ACT';// 6.4 Clave del SAT correspondiente a la unidad de medida (consultar el catálogo de productos del SAT).
        $concepto['Descripcion'] = 'Pago'; // 6.6 Descripción del artículo o servicio.$concepto_fac->concepto_nombre;
        $concepto['ValorUnitario'] = '0';// 6.7 Valor unitario del artículo o servicio.$concepto_fac->concepto_precio;
        $concepto['Importe'] = '0';// 6.8 Importe del artículo o servicio.$concepto_fac->concepto_importe;
        $concepto['ObjetoImp'] = '01';
        $cfdi->conceptos($concepto); // agrega los conceptos y los impuestos
        //conceptos fin ----------------------------


        $catos_comple = array();
        $catos_comple["FechaPago"] = $complemento_datos->fecha;//date("Y-m-d")."T".date("H:i:s");
        $catos_comple["FormaDePagoP"] = $complemento_datos->forma_pago;//"06";
        $catos_comple["MonedaP"] = $complemento_datos->complemento_moneda;//"MXN";
        $catos_comple["TipoCambioP"] = $complemento_datos->tipo_cambio;//"1";
        $catos_comple["Monto"] = $complemento_datos->total_pago;//"2000.00";
        $catos_comple["NumOperacion"] = $complemento_datos->no_operacion;//"AUT. 898872";// esto es una referencia
        $catos_comple["SumaPagos"] = $complemento_datos->total_pago;//"2000.00";
        //$cfdi->datos_complemento($catos_comple);

        foreach($complemento_cfdi_relacionado_p as $complem_p):
        // datos der la relacion ---------------
        $agragar_pago = array();
        $agragar_pago['IdDocumento'] = $complem_p->uuid;//'8B797440-C422-4900-A9BA-DCC74A7BBC02';
        $agragar_pago['Serie'] = $complem_p->serie;//'A';
        $agragar_pago['Folio'] = $complem_p->folio;//'001';
        $agragar_pago['MonedaDR'] = $complem_p->monedaDR;//'MXN';
        $agragar_pago['EquivalenciaDR'] = $complem_p->tipoCambioDR;//'1';
        $agragar_pago['NumParcialidad'] = $complem_p->numParcialidad;//'1';
        $agragar_pago['ImpSaldoAnt'] = $complem_p->impSaldoAnt;//'10000.00';
        $agragar_pago['ImpPagado'] = $complem_p->impPagado;//'2000.00';
        $agragar_pago['ImpSaldoInsoluto'] = $complem_p->impSaldoInsoluto;//'8000.00';
        $agragar_pago['ObjetoImpDR'] = '01';
        //$cfdi->agregar_pago($agragar_pago);
        $cfdi->datos_complemento($catos_comple,$agragar_pago);
        // fin datos ----------------------------
        endforeach;


        $cfdi->sellar();

       // echo $cfdi->obtenerXml();
        //die();


        if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/precomplemento/$id_complemento.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_precomplemento'] = base_url()."statics/facturas/precomplemento/$id_complemento.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('complemento_pago',$data_url_prefactura,'complementoID',$id_complemento);
            $this->enviar_complemento($id_complemento);
        }else{
            $data_respone['error'] = 1;
            $data_respone['error_mensaje'] = "Ocurrio un error";
            $data_respone['factura'] = $id_complemento;
            echo json_encode($data_respone);
            die;
        }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');




        
        
    }



    public function enviar_complemento($id_factura){
        $datos_fac = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');
  
        # Username and Password, assigned by FINKOK
        $username = 'liberiusg@gmail.com';
        $password = '*Libros7893811';
  
        # Read the xml file and encode it on base64
        $invoice_path = $this->config->item('url_real')."statics/facturas/precomplemento/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
        $xml_file = fopen($invoice_path, "rb");
        $xml_content = fread($xml_file, filesize($invoice_path));
        fclose($xml_file);
  
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$xml_content = base64_encode($xml_content);
  
        # Consuming the stamp service
        $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
        $client = new SoapClient($url);
  
        $params = array(
          "xml" => $xml_content,
          "username" => $username,
          "password" => $password
        );
        $response = $client->__soapCall("stamp", array($params));
        //print_r($response);
        ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
        //print $response->stampResult->xml;
  
        ####mostrar el código de error en caso de presentar alguna incidencia
        #print $response->stampResult->Incidencias->Incidencia->CodigoError;
        ####mostrar el mensaje de incidencia en caso de presentar alguna
        if(isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)){
          $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
          //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $this->Mgeneral->update_table_row('complemento_pago',$data_sat,'complementoID',$id_factura);
  
          $data_respone['error'] = 1;
          $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $data_respone['factura'] = $id_factura;
          echo json_encode($data_respone);
          
          //redirect('complemento_pago/lista_complementos');
          die;
        }else{
          $data_sat['sat_uuid'] = $response->stampResult->UUID;
          $data_sat['sat_fecha'] = $response->stampResult->Fecha;
          $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
          $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
          $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
          $data_sat['sat_error'] = "0";
          $data_sat['sat_codigo_error'] = "0";
  
          $this->Mgeneral->update_table_row('complemento_pago',$data_sat,'complementoID',$id_factura);
          file_put_contents($this->config->item('url_real')."/statics/facturas/complementoxml/$id_factura.xml", $response->stampResult->xml);
          $data_respone['error'] = 0;
          $data_respone['error_mensaje'] = "Factura timbrada";
          $data_respone['factura'] = $id_factura;
          $data_respone['sat_uuid'] = $response->stampResult->UUID;
          $data_respone['pdf'] = "".base_url()."index.php/complemento_pago/genera_pdf/".$id_factura."";
          $data_respone['xml'] = "".base_url()."statics/facturas/complementoxml/".$id_factura.".xml";
          echo json_encode($data_respone);
  
          //redirect('complemento_pago/lista_complementos');
          die;
        }
        #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      }

     


      public function genera_pdf($id_factura){
        $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
        $datos_fac = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');
        $logo = 'cfdi41/logo_mylsa.png';//$this->Mgeneral->get_row('id',1,'informacion')->logo;
        require '/var/www/web/dms/contabilidad_queretaro/cfdi41/pdf/lib/Cfdi2Pdf.php';//$this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
        $pdfTemplateDir = '/var/www/web/dms/contabilidad_queretaro/cfdi41/pdf/lib/templates/';//$this->config->item('url_real').'cfdi/pdf/lib/templates/';
  
        $pdf = new Cfdi2Pdf($pdfTemplateDir);
  
        // datos del archivo. No se mostrarán en el PDF (requeridos)
        $pdf->autor  = 'www.fumigacioneselastillero.com';
        $pdf->titulo = 'Complemento pago';
        $pdf->asunto = 'Comprobante CFDI';
  
        // texto a mostrar en la parte superior (requerido)
        $pdf->encabezado = $datas_empresa->nombre;
  
        // nombre del archivo PDF (opcional)
        $pdf->nombreArchivo = 'pdf-cfdi.pdf';
  
        // mensaje a mostrar en el pie de pagina (opcional)
        $pdf->piePagina = 'Complemento de pago';
  
        // texto libre a mostrar al final del documento (opcional)
        $pdf->mensajeFactura = '';
  
        // Solo compatible con CFDI 3.3 (opcional)
        $pdf->direccionExpedicion = $datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;
  
        // ruta del logotipo (opcional)
        $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';
  
        // mensaje a mostrar encima del documento (opcional)
        // $pdf->mensajeSello = 'CANCELADO';
  
        // Cargar el XML desde un string...
        // $ok = $pdf->cargarCadenaXml($cadenaXml);
  
        // Cargar el XML desde un archivo...
        // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
        // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
        // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
        $archivoXml = $this->config->item('url_real')."/statics/facturas/complementoxml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';
  
        $ok = $pdf->cargarArchivoXml($archivoXml);
  
        if($ok) {
            // Generar PDF para mostrar en el explorador o descargar
            $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador
  
            // Guardar PDF en la ruta especificada
            // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
            // $ok = $pdf->guardarPdf($ruta);
  
            // Obtener PDF como string
            // $pdfStr = $pdf->obtenerPdf();
  
            if($ok) {
                // PDF generado correctamente.
            } else {
                echo 'Error al generar PDF.';
                exit();
            }
        }else{
            echo 'Error al cargar archivo XML.';
            exit();
        }
  
    }






    public function api_genera_complemento(){


        $json = file_get_contents('php://input');
  
        // Converts it into a PHP object
        $factura_campo = json_decode($json);
        //utils::pre($factura_campo);
        $emisor = $this->Mgeneral->get_row('id',1,'datos');
        $id_complemento = get_guid();
        $data['complementoID'] = $id_complemento;
        $data['status'] = 1;
        $data['emisorRFC'] = $emisor->rfc;
        $data['emisorRegimenFiscal'] = $emisor->regimen;
        $data['emisorNombre'] = $emisor->razon_social;
        $data['receptorUsoCFDI'] = "CP01";
        $data['monedaG'] = "XXX";
        $data['tipoDeComprobante'] = "P";
        $data['lugarExpedicion'] = codigo_postal();
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        //$data['receptor_uso_CFDI'] = "G03";
        $id_tabla = $this->Mgeneral->save_register('complemento_pago', $data);
        $data_c['folio'] = $id_tabla;
        $data_c['serie'] = $id_tabla.'-'.date('Y-m-d');
        $this->Mgeneral->update_table_row('complemento_pago',$data_c,'complementoID',$id_complemento);
        $data_complemento1['id_complemento'] = $id_complemento;
        $this->Mgeneral->save_register('complemento_datos', $data_complemento1);

        

        $data1['uuid'] = $this->validar_capo_json($factura_campo->uuid,'uuid');// uuid
        $data1['id_complemento'] = $id_complemento;
        $data1['serie'] = $this->validar_capo_json($factura_campo->seriep,'seriep');//serie
        $data1['folio'] = $this->validar_capo_json($factura_campo->foliop,'foliop');
        $data1['monedaDR'] = $this->validar_capo_json($factura_campo->moneda_pe,'moneda_pe');
        $data1['tipoCambioDR'] = $this->validar_capo_json($factura_campo->tipo_cambio_p,'tipo_cambio_p');
        $data1['metodoDePagoDR'] = $this->validar_capo_json($factura_campo->metodo_pago_p,'metodo_pago_p');
        $data1['numParcialidad'] = $this->validar_capo_json($factura_campo->numero_parcial,'numero_parcial');
        $data1['impSaldoAnt'] = $this->validar_capo_json($factura_campo->deuda_pagar,'deuda_pagar');
        $data1['impPagado'] = $this->validar_capo_json($factura_campo->importe_pagado,'importe_pagado');
        $data1['impSaldoInsoluto'] = $this->validar_capo_json($factura_campo->nuevo_saldo,'nuevo_saldo');
        $data1['fecha_creacion'] = time();
  
        $this->Mgeneral->save_register('complemento_cfdi_relacionado_p', $data1);

       

          $data2['receptorNombre'] = $this->validar_capo_json($factura_campo->receptor_nombre,'receptorNombre');
          $data2['receptorId'] = 0;//$this->input->post("receptor_id_cliente");
          $data2['lugarExpedicion'] = $this->validar_capo_json($factura_campo->lugarExpedicion,'lugarExpedicion');
          $data2['fechaPago'] = $this->validar_capo_json($factura_campo->fecha_pago,'fecha_pago');//date("Y-m-d")."T".date("H:i:s");//$this->validar_capo_json($factura_campo->fecha_pago);
          $data2['receptor_email'] = $this->validar_capo_json($factura_campo->receptor_email,'receptor_email');
          $data2['folio'] = $this->validar_capo_json($factura_campo->factura_folio,'factura_folio');
          $data2['serie'] = $this->validar_capo_json($factura_campo->factura_serie,'factura_serie');
          $data2['comentario'] = $this->validar_capo_json($factura_campo->comentario,'comentario');
          $data2['receptorRFC'] = $this->validar_capo_json($factura_campo->receptorRFC,'receptorRFC');
          $this->Mgeneral->update_table_row('complemento_pago',$data2,'complementoID',$id_complemento);
  
          $data_complemento['fecha'] = $this->validar_capo_json($factura_campo->complemento_fecha,'complemento_fecha');
          $data_complemento['forma_pago'] = $this->validar_capo_json($factura_campo->complemento_forma_pago,'complemento_forma_pago');
          $data_complemento['complemento_moneda'] = 'MXN';//$this->input->post("complemento_moneda");
          $data_complemento['tipo_cambio'] = $this->validar_capo_json($factura_campo->complemento_tipoCambio,'complemento_tipoCambio');
          $data_complemento['total_pago'] = $this->validar_capo_json($factura_campo->complemento_totalPago,'complemento_totalPago');
          $data_complemento['no_operacion'] = $this->validar_capo_json($factura_campo->complemento_no_operacion,'complemento_no_operacion');
          $data_complemento['banco_ordenante'] = $this->validar_capo_json($factura_campo->complemento_banco_ordenante,'complemento_banco_ordenante');
          $data_complemento['cta_ordenante'] = $this->validar_capo_json($factura_campo->complemento_cta_ordenante,'complemento_cta_ordenante');
          $data_complemento['rfc_beneficiario'] = $this->validar_capo_json($factura_campo->complemento_rfc_beneficiario,'complemento_rfc_beneficiario');
          $data_complemento['cta_beneficiario'] = $this->validar_capo_json($factura_campo->complemento_cta_beneficiario,'complemento_cta_beneficiario');
          $data_complemento['rfc_ordenante'] = $this->validar_capo_json($factura_campo->complemento_rfc_ordenante,'complemento_rfc_ordenante');
          $this->Mgeneral->update_table_row('complemento_datos',$data_complemento,'id_complemento',$id_complemento);



  
        $this->genera_complemento_pago($id_complemento);
        //$this->generar_prefactura($id_factura);
        
      }
  
      function validar_capo_json($campo, $des){
        try {
          if(!isset($campo)) {
              throw new Exception("Falta campo".$campo .' ' .$des);
            }
            return $campo;
        }
        //catch exception
        catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
          die();
        }
  
      }



  
}
