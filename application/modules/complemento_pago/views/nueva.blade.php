@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')





<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript">
       menu_activo = "facturacion";
$("#menu_facturacion_complemento").last().addClass("menu_estilo");
    $(document).ready(function(){


      aux = $('#complemento_forma_pago').val();
      switch(aux) {
            case "01":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "02":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "03":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "04":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "05":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "06":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "08":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "12":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "13":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "14":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "15":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "17":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "23":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "24":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "25":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "26":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "27":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "28":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "29":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "30":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "31":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
          }
      
      $('#complemento_forma_pago').change(function(event){ 
        event.preventDefault();

          var value = $(this).val();
          
          switch(value) {
            case "01":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "02":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "03":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "04":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "05":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "06":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "08":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "12":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "13":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "14":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "15":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "17":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "23":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "24":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "25":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "26":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "27":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "28":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "29":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', false);
              $('#complemento_banco_ordenante').prop('disabled', false);
              $('#complemento_cta_ordenante').prop('disabled', false);
              $('#complemento_rfc_beneficiario').prop('disabled', false);
              $('#complemento_cta_beneficiario').prop('disabled', false);
              break;
            case "30":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
            case "31":
              $('#complemento_no_operacion').prop('disabled', false);
              $('#complemento_rfc_ordenante').prop('disabled', true);
              $('#complemento_banco_ordenante').prop('disabled', true);
              $('#complemento_cta_ordenante').prop('disabled', true);
              $('#complemento_rfc_beneficiario').prop('disabled', true);
              $('#complemento_cta_beneficiario').prop('disabled', true);
              break;
          }
          
      });



      $("#cargando").hide();
      $('#guarda_factura').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();

        var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando Datos...</p>',
                            closeButton: false
                        });

        var url ="<?php echo base_url()?>index.php/complemento_pago/guarda_datos/<?php echo $complementoID?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "lugarExpedicion":$("#lugarExpedicion").val(),
                      "fecha_pago":$("#fecha_pago").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "comentario":$("#comentario").val(),
                      "receptorRFC":$("#receptor_RFC").val(),

                      "complemento_fecha":$("#complemento_fecha").val(),
                      "complemento_forma_pago":$("#complemento_forma_pago").val(),
                      "complemento_moneda":$("#complemento_moneda").val(),
                      "complemento_tipoCambio":$("#complemento_tipoCambio").val(),
                      "complemento_totalPago":$("#complemento_totalPago").val(),
                      "complemento_no_operacion":$("#complemento_no_operacion").val(),
                      "complemento_banco_ordenante":$("#complemento_banco_ordenante").val(),
                      "complemento_cta_ordenante":$("#complemento_cta_ordenante").val(),
                      "complemento_rfc_beneficiario":$("#complemento_rfc_beneficiario").val(),
                      "complemento_cta_beneficiario":$("#complemento_cta_beneficiario").val(),
                      "complemento_rfc_ordenante":$("#complemento_rfc_ordenante").val(),
                       },
                  "POST",true,function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            dialog_load.modal('hide');
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            //exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/complemento_pago/genera_complemento_pago/<?php echo $complementoID?>");
            //$("#enviar").show();
            //$("#cargando").hide();
            $.ajax({
                  type: 'POST',
                  url: "<?php echo base_url()?>index.php/complemento_pago/genera_complemento_pago/<?php echo $complementoID?>",
                  enctype: 'multipart/form-data',
                  datatype: "JSON",
                async: true,
                  cache: false,
                  
                  statusCode: {
                      200: function (result) {

                        console.log(result);
                        json_response_dos = JSON.parse(result);
                        obj_output_d = json_response_dos;
                        obj_status_d = obj_output_d.error;
                        if(obj_status_d == 1){
                          dialog_load.modal('hide');
                            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+obj_output_d.error_mensaje,"danger");
                            $("#enviar").show();
                            $("#cargando").hide();
                        }else if(obj_status_d == 0){
                          dialog_load.modal('hide');
                            exito_redirect("COMPLEMENTO CREADO CON EXITO","success","<?php echo base_url()?>index.php/complemento_pago/lista_complementos");
                        }else{
                          dialog_load.modal('hide');
                          exito("<h3>ERROR intente de nuevo<h3/> <br/>","danger");
                            $("#enviar").show();
                            $("#cargando").hide();
                        }

                      },
                      401: code400,
                      404: code404,
                      500: code500,
                      409: code409
                  }
              });
          }


        });
      });



      $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>index.php/complemento_pago/ver_relaciones_complementos",
          enctype: 'multipart/form-data',
          datatype: "JSON",
        //async: asincrono,
          cache: false,
          data: {id_factura:"<?php echo $complementoID?>"},
          statusCode: {
              200: function (result) {
                //console.log(result);
                 $("#complementos_load").html("");
                 $("#complementos_load").html(result);
                 $(".eliminar_relacion_p").click(function(event){
                      event.preventDefault();
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete2"+id).attr('href');
                      $("#borrar2_"+id).slideUp();
                      $.get(url);
                  });

              },
              401: code400,
              404: code404,
              500: code500,
              409: code409
          }
      });

      $("#agregar_relacion_complemento").click(function(){

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url()?>index.php/complemento_pago/relaciones_complementos",
            enctype: 'multipart/form-data',
            datatype: "JSON",
          //async: asincrono,
            cache: false,
            data: {id_complemento:"<?php echo $complementoID?>",
                    uuid:$('#uuid2').val(),
                    seriep:$('#seriep').val(),
                    foliop:$('#foliop').val(),
                    moneda_pe:$('#moneda_p').val(),
                    tipo_cambio_p:$('#tipo_cambio_p').val(),
                    metodo_pago_p:$('#metodo_pago_p').val(),
                    numero_parcial:$('#numero_parcial').val(),
                    deuda_pagar:$('#deuda_pagar').val(),
                    importe_pagado:$('#importe_pagado').val(),
                    nuevo_saldo:$('#nuevo_saldo').val()},
            statusCode: {
                200: function (result) {
                  //console.log(result);
                   $("#complementos_load").html("");
                   $("#complementos_load").html(result);
                   $(".eliminar_relacion_p").click(function(event){
                        event.preventDefault();
                        id = $(event.currentTarget).attr('flag');
                        url = $("#delete2"+id).attr('href');
                        $("#borrar2_"+id).slideUp();
                        $.get(url);
                    });

                },
                401: code400,
                404: code404,
                500: code500,
                409: code409
            }
        });



      });

      $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>index.php/complemento_pago/ver_relaciones_facturas",
          enctype: 'multipart/form-data',
          datatype: "JSON",
        //async: asincrono,
          cache: false,
          data: {id_factura:"<?php echo $complementoID?>"},
          statusCode: {
              200: function (result) {
                //console.log(result);
                 $("#relacionados").html("");
                 $("#relacionados").html(result);
                 $(".eliminar_relacion").click(function(event){
                      event.preventDefault();
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                      $.get(url);
                  });

              },
              401: code400,
              404: code404,
              500: code500,
              409: code409
          }
      });

        $("#agregar_relacion").click(function(){


          $.ajax({
              type: 'POST',
              url: "<?php echo base_url()?>index.php/complemento_pago/relaciones_facturas",
              enctype: 'multipart/form-data',
              datatype: "JSON",
            //async: asincrono,
              cache: false,
              data: {tipo_relacion:$("#id_relacion").val(),uuid:$("#uuid").val(),id_complemento:"<?php echo $complementoID?>"},
              statusCode: {
                  200: function (result) {

                    //console.log(result);
                     $("#relacionados").html("");
                     $("#relacionados").html(result);
                     $(".eliminar_relacion").click(function(event){
                          event.preventDefault();
                          id = $(event.currentTarget).attr('flag');
                          url = $("#delete"+id).attr('href');
                          $("#borrar_"+id).slideUp();
                          $.get(url);
                      });

                  },
                  401: code400,
                  404: code404,
                  500: code500,
                  409: code409
              }
          });


        });





    });



    function seleccionar_cliente(id_cliente){
      //$('#exampleModal').modal('hide');

      var url_sis ="<?php echo base_url()?>index.php/complemento_pago/get_datos_cliente/"+id_cliente;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                  var obj = JSON.parse(data);

                  $('#receptor_nombre').val(obj.razon_social);
                  $('#receptor_RFC').val(obj.rfc);
                  $('#receptor_id_cliente').val(obj.id);
                  $('#receptor_email').val(obj.email);


                  console.log("SUCCESS : ", data);
                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
    }

    
    function buscafactura(){

      var url_sis ="<?php echo base_url()?>index.php/complemento_pago/buscar_factura/"+$("#receptor_RFC").val();
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                  $("#carga_busqueda_facturas").html(data);
                   $(".elegir_fac").click(function(e){
                    event.preventDefault();
                    var uuid_fac = $(this).attr('id');
                        //alert(""+id_rfc);
                        $("#uuid2").val(uuid_fac);
                        $("#seriep").val(num_serie(6) );
                        $("#foliop").val(num_folio(5));
                        datos_pago($(this).attr('id'));
                        $('#buscaFactura').modal('toggle');
                        
                    });
                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
    }

    function datos_pago($uuid){
      $.ajax({
              type: 'POST',
              url: "<?php echo base_url()?>index.php/complemento_pago/datos_pago/"+$uuid,
              enctype: 'multipart/form-data',
              datatype: "JSON",
            //async: asincrono,
              cache: false,
              data: {},
              statusCode: {
                  200: function (result) {
                    var obj = JSON.parse(result);  
                    pago_complemento = $("#complemento_totalPago").val();
                    $("#numero_parcial").val(obj.numero_pago);
                    $("#deuda_pagar").val(obj.saldo);
                    //$("#importe_pagado").val(obj.abonos);
                    $("#importe_pagado").val(pago_complemento);
                    $("#nuevo_saldo").val(obj.saldo - pago_complemento );

                  },
                  401: code400,
                  404: code404,
                  500: code500,
                  409: code409
              }
          });
    }


    function num_serie(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }

    function num_folio(length) {
       var result           = '';
       var characters       = '01234567890123456789012345678901234567890123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }



    
</script>
<style>

label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

<div class="row">
  <div class="col-lg-12">
    <div class="card" style="background:#ced6e0">

        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="guarda_factura">
                    <!--div align="right">
                    <button id="guardar_datos" type="button" class="btn btn-outline-success">
                        <i class="fa fa-edit fa-lg"></i>&nbsp;
                        <span id="payment-button-amount">Guardar datos</span>

                    </button>
                    </div>
                  <hr/-->

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="factura_moneda">Cliente:</label>
                                <input id="receptor_nombre" name="receptor_nombre" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento)){echo $complemento->receptorNombre; } ?>" placeholder="Cliente" autocomplete="cc-exp"  >
                                <input id="receptor_id_cliente" name="receptor_id_cliente" type="hidden" class="alto form-control cc-exp" value="<?php  if(is_object($complemento)){echo $complemento->receptorId;}?>" placeholder="receptor_id_cliente" autocomplete="cc-exp">
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group">
                                <label for="receptor_RFC">RFC:</label>
                                <input id="receptor_RFC" name="receptor_RFC" type="text" class="form-control-sm alto form-control cc-exp" value="<?php  if(is_object($complemento)){echo $complemento->receptorRFC;}?>" placeholder="RFC" autocomplete="cc-exp">

                            </div>
                        </div>

                        <div class="col-1">
                          <div class="form-group">
                            <label for="factura_moneda">Moneda:</label>
                            <input type="text" class="form-control-sm alto form-control" id="factura_moneda" name="factura_moneda" value="<?php  if(is_object($complemento)){echo $complemento->monedaG;}?>" disabled>
                          </div>
                       </div>
                       <div class="col-1">
                         <div class="form-group">
                           <label for="email">CP:</label>
                           <input type="text" class="form-control-sm alto form-control" id="lugarExpedicion" name="lugarExpedicion" value="<?php  if(is_object($complemento)){echo $complemento->lugarExpedicion;}?>" disabled>
                         </div>
                      </div>
                      <div class="col-2">
                        <div class="form-group">
                          <label for="email">Fecha:</label>
                          <input type="text" class="form-control-sm alto form-control" id="fecha_pago" name="fecha_pago" value="<?php echo date('Y-m-d H:i:s');?>" disabled>
                        </div>
                     </div>
                     <div class="col-1">
                       <div class="form-group">
                         <label for="email">Folio:</label>
                         <input type="text" class="form-control-sm alto form-control" id="factura_folio" name="factura_folio" value="<?php  if(is_object($complemento)){echo $complemento->folio;}?>">
                       </div>
                    </div>
                    <div class="col-1">
                      <div class="form-group">
                        <label for="email">Serie:</label>
                        <input type="text" class="form-control-sm alto form-control" id="factura_serie" name="factura_serie" value="<?php  if(is_object($complemento)){echo $complemento->serie;}?>">
                      </div>
                   </div>
                    </div>

                      <div class="row">

                        <div class="col-3">
                            <div class="form-group">
                                <label for="receptor_email">Email:</label>
                                <input id="receptor_email" name="receptor_email" type="text" class="form-control-sm alto form-control cc-exp" value="<?php  if(is_object($complemento)){echo $complemento->receptor_email;}?>" placeholder="Email" autocomplete="cc-exp">

                            </div>
                        </div>


                        <div class="col-2">
                          <div class="form-group">
                            <label for="factura_formaPago">Forma de pago:</label>
                            <select  class="form-control-sm alto form-control" name="factura_formaPago" id="factura_formaPago" disabled>
                              <option value="" selected></option>

                            </select>

                          </div>
                       </div>

                       <div class="col-2">
                         <div class="form-group">
                           <label for="receptor_RFC">Tipo comprovante:</label>
                           <select class="form-control-sm alto form-control" id="factura_tipoComprobante" name="factura_tipoComprobante" disabled>
                           <?php foreach($tipo_comprobante as $tipo_c):?>
                                <option value="<?php echo $tipo_c->id;?>" <?php if(is_object($complemento)){ if($complemento->factura_tipoComprobante == $tipo_c->id){echo "selected";} }?> ><?php echo $tipo_c->id;?>-<?php echo $tipo_c->nombre;?></option>
                            <?php endforeach;?>  
                           </select>
                         </div>
                      </div>


                      <div class="col-2">
                          <div class="form-group">
                            <label for="receptor_uso_CFDI">Uso del CFDI(CP01):</label>
                            <select class="form-control-sm alto form-control" id="receptor_uso_CFDI" name="receptor_uso_CFDI" disabled>

                            <?php foreach($uso_cfdi as $uso_c):?>
                                <option value="CP01" selected>CP01	Pagos</option>

                                <!--option value="<?php echo $uso_c->id;?>" <?php if(is_object($complemento)){ if($complemento->receptor_uso_CFDI == $uso_c->id){echo "selected";} }?> ><?php echo $uso_c->id;?>-<?php echo $uso_c->nombre;?></option-->
                            <?php endforeach;?>

                            </select>



                          </div>
                      </div>


                      <div class="col-3">
                        <div class="form-group">
                          <label for="comentario">Comentario:</label>
                          <input id="comentario" name="comentario" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento)){ echo $complemento->comentario;}?>" placeholder="Comentario" autocomplete="cc-exp">
                      </div>
                     </div>

                      </div>






<!--hr/>
<div><label>Agregar relaciones de CFDI</label></div>
                      <div class="row" style="background:#fff" >

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Tipo relación:</label>
                          <select class="form-control-sm form-control" id="id_relacion">
                           <option value="01">01.-nota de credito de los documentos</opcion>
                           <option value="02">02.-nota de debito de los documtntos relacionados</opcion>
                           <option value="03">03.-devoclución de mercancía sobre facturas o traslados</opcion>
                           <option value="04">04.-sustituto de los CFDI previos</opcion>
                           <option value="05">05.-traslados de mercancías facturados previamente</opcion>
                           <option value="06">06.-factura generada por los traslados previos</opcion>
                           <option value="07">07.-CFDI por aplicación de anticipo</opcion>
                           <option value="08">08.-factura generada por pagos en parcialidades</opcion>
                           <option value="09">09.-factura generada por pagos diferidos</opcion>
                          </select>
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">UUID:</label>
                          <input id="uuid" name="uuid" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="UUID" autocomplete="cc-exp">
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Opción:</label><br/>
                          <?php if($complemento->sat_error != "0"):?>
                          <button type="button" id="agregar_relacion" class="btn btn-outline-success">Agregar relación</button>
                        <?php endif;?>
                      </div>
                      </div>




                      </div>

                      <div class="row" id="relacionados" >
                        <div class="col-12">
                        </div>
                      </div-->




                      <hr/>


                      <div class="row" id="">
                        <div class="col-2">
                          <div class="form-group">
                            
                            <input id="complemento_fecha" name="complemento_fecha" type="hidden" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->fecha;}?>" placeholder="Fecha" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="factura_formaPago">Forma de pago:</label>
                            <select  class="form-control-sm alto form-control" name="complemento_forma_pago" id="complemento_forma_pago">
                               <?php foreach($forma_pagos as $forma_p):?>
                                     <option value="<?php echo $forma_p->id;?>" <?php if(is_object($complemento_datos)){if($complemento_datos->forma_pago ==$forma_p->id){echo "selected";}}?> ><?php echo $forma_p->id;?>-<?php echo $forma_p->nombre;?></option>
                                <?php endforeach;?>

                            </select>

                          </div>

                        </div>

                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Moneda:</label>
                            <input id="complemento_moneda" name="complemento_moneda" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->complemento_moneda;}?>" placeholder="MXN" autocomplete="cc-exp" disabled>
                        </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Tipo cambio:</label>
                            <input id="complemento_tipoCambio" name="complemento_tipoCambio" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->tipo_cambio;}?>" placeholder="" autocomplete="cc-exp" disabled>
                        </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Total Pago:</label>
                            <input id="complemento_totalPago" name="complemento_totalPago" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->total_pago;}?>" placeholder="Total Pago" autocomplete="cc-exp">
                        </div>
                        </div>


                      </div>

                      <div class="row">
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">No. operación:</label>
                            <input id="complemento_no_operacion" name="complemento_no_operacion" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->no_operacion;}?>" placeholder="" autocomplete="cc-exp">
                        </div>
                       </div>

                       <div class="col-2">
                         <div class="form-group">
                           <label for="comentario">RFC ordenante:</label>
                           <input id="complemento_rfc_ordenante" name="complemento_rfc_ordenante" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->rfc_ordenante;}?>" placeholder="" autocomplete="cc-exp">
                       </div>
                      </div>

                      <div class="col-2">
                        <div class="form-group">
                          <label for="comentario">Banco ordenante:</label>
                          <input id="complemento_banco_ordenante" name="complemento_banco_ordenante" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->banco_ordenante;}?>" placeholder="" autocomplete="cc-exp">
                      </div>
                     </div>

                     <div class="col-2">
                       <div class="form-group">
                         <label for="comentario">Cta ordenante:</label>
                         <input id="complemento_cta_ordenante" name="complemento_cta_ordenante" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->cta_ordenante;}?>" placeholder="" autocomplete="cc-exp">
                     </div>
                    </div>

                   <div class="col-2">
                     <div class="form-group">
                       <label for="comentario">RFC beneficiario:</label>
                       <input id="complemento_rfc_beneficiario" name="complemento_rfc_beneficiario" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->rfc_beneficiario;}?>" placeholder="" autocomplete="cc-exp">
                   </div>
                  </div>

                  <div class="col-2">
                    <div class="form-group">
                      <label for="comentario">Cta beneficiario:</label>
                      <input id="complemento_cta_beneficiario" name="complemento_cta_beneficiario" type="text" class="form-control-sm alto form-control cc-exp" value="<?php if(is_object($complemento_datos)){echo $complemento_datos->cta_beneficiario;}?>" placeholder="" autocomplete="cc-exp">
                  </div>
                 </div>

                </div>





                      <hr/>
                      <div><label>Agregar datos del CFDI relacionado (factura a pagar)</label></div>
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="comentario">UUID:</label><br/>
                            <input id="uuid2" name="uuid2" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="UUID" data-toggle="modal"  data-target="#buscaFactura" onclick="buscafactura()">
                        </div>
                        </div>
                        <div class="col-2">
                          <!--div class="form-group">
                            <label for="comentario">Opción:</label><br/>
                            <button type="button" id="agregar_relacion2" class="btn btn-outline-success">Buscar CFDI</button>
                        </div-->
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Serie:</label>
                            <input id="seriep" name="seriep" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Serie" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-1">
                          <div class="form-group">
                            <label for="comentario">Folio:</label>
                            <input id="foliop" name="foliop" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Folio" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-1">
                          <div class="form-group">
                            <label for="comentario">Moneda:</label>
                            <input id="moneda_p" name="moneda_p" type="text" class="form-control-sm alto form-control cc-exp" value="MXN" placeholder="MXN" autocomplete="cc-exp" disabled>
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Tipo de cambio:</label>
                            <input id="tipo_cambio_p" name="tipo_cambio_p" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="" autocomplete="cc-exp" disabled>
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">método de pago:</label>
                            <input id="metodo_pago_p" name="metodo_pago_" type="text" class="form-control-sm alto form-control cc-exp" value="PPD" placeholder="PPD" autocomplete="cc-exp" disabled>
                        </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Número de abono:</label>
                            <input id="numero_parcial" name="numero_parcial" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Número de abono" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Deuda por pagar:</label>
                            <input id="deuda_pagar" name="deuda_pagar" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Deuda por pagar" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Importe Pagado:</label>
                            <input id="importe_pagado" name="importe_pagado" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Importe Pagado" autocomplete="cc-exp">
                        </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="comentario">Nuevo saldo a pagar:</label>
                            <input id="nuevo_saldo" name="nuevo_saldo" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="Nuevo saldo a pagar" autocomplete="cc-exp">
                        </div>
                        </div>

                      </div>
                      <div class="" align="right">
                        <!--a href="<?php echo base_url()?>index.php/Fatura/conceptos/<?php echo $factura->facturaID?>"-->
                         <?php //if(is_object($complemento)):?>
                          <?php //if($complemento->sat_error != "0"):?>
                          <button id="agregar_relacion_complemento" type="button" class="btn  btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Agregar </span>

                          </button>
                        <?php //endif;?>
                        <?php //endif;?>
                        <!--/a-->
                      </div>

                      <div class="row" id="complementos_load" >
                        <div class="col-12">
                        </div>
                      </div>

<hr/>
                      <div class="" align="right">
                        <!--a href=""-->
                        
                        <?php //if(is_object($complemento)):?>

                        <?php //if($complemento->sat_error != "0"):?>
                            
                          <button id="enviar" type="submit" class="btn  btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Enviar y sellar</span>

                          </button>

                          <button class="btn btn-primary" id="cargando" type="button" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Enviando datos...
                            </button>

                        <?php //endif;?>

                        <?php //endif;?>

                        <?php if(is_object($complemento)):?>
                        <?php if($complemento->sat_error == "1"):?>
                          <?php echo $complemento->sat_codestatus;?>
                        <?php endif;?>
                        <?php endif;?>
                        <!--/a-->
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->



<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Cliente</th>
        <th>RFC</th>

        <th>Opciones</th>
      </tr>
    </thead>
    <!--tbody>
      <?php if(is_array($clientes)):?>
      <?php foreach($clientes as $row):?>
        <tr id="<?php echo $row->id;?>">
          <td><?php echo $row->razon_social;?></td>
          <td><?php echo $row->rfc;?></td>
          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_cliente(<?php echo $row->id;?>)" data-dismiss="modal" id="<?php echo $row->id;?>" data-id="<?php echo $row->id;?>">O</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody-->
  </table>
        </div>
      </div>

    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="buscaFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione una factura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div id="carga_busqueda_facturas"></div>
        </div>
      </div>

    </div>
  </div>
</div>









	
@endsection
