<div class="row">
    <div class="col-sm-12">
        <form id="form_operaciones">

            <input type="hidden" value="<?php echo $renglon_id; ?>" name="renglon_id_actual" />

            <div class="form-group">
                <label><b> Origen de la información</b></label>
                <select name="origen_informacion" id="origen_informacion" onchange="app.cambiar_origen_informacion(this)" class="custom-select">
                    
                    <option value="1">Balanza</option>
                    <!-- <option value="2">Reporte actual</option> -->
                    <option value="3">Reportes</option>
                    <option value="4">Cantidad fija</option>
                    <option value="5">Apartir de la información de otros modulos</option>
                </select>
                <small id="msg_tipo_formato" class="form-text text-danger"></small>
            </div>

            <div class="form-group">
                <label><b>* Tipo operación</b></label>
                <select name="tipo_operacion" id="tipo_operacion" class="form-control">
                    <option value=""></option>
                    <option value="+">+ (suma)</option>
                    <option value="-">- (resta)</option>
                    <option value="/">/ (división)</option>
                    <option value="*">* (multiplicación)</option>
                </select>
                <small id="msg_tipo_operacion" class="form-text text-danger"></small>
            </div>
            
            <div class="form-group" id="div_cantidad_fija" style="display:none">
                <label><b>* Cantidad</b></label>
                <input type="number" step="0.01" min="0" class="form-control" name="cantidad_fija" id="cantidad_fija" />
                <small id="msg_cantidad_fija" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_cuenta_id" style="display:none">
                <label><b>* Cuenta</b></label>
                <select style="width:100%" name="cuenta_id" id="cuenta_id" class="custom-select">
                    
                    <?php foreach ($cuentas_dms as $cuenta) { ?>
                        <option value="<?php echo $cuenta['id']; ?>"><?php echo '['.$cuenta['cuenta'] . '] ' . $cuenta['decripcion']; ?></option>
                    <?php } ?>
                </select>
                <small id="msg_cuenta_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_reporte_id"  style="display:none">
                <label><b>* Reporte</b></label>
                <select style="width:100%" name="reporte_id" onchange=" app.listado_reglones_reportes();" id="reporte_id" class="custom-select">
                    
                    <?php foreach ($reportes as $reporte) { ?>
                        <option value="<?php echo $reporte['id']; ?>"><?php echo '['.$reporte['id'].'] '.$reporte['nombre']; ?></option>
                    <?php } ?>
                </select>
                <small id="msg_reporte_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_renglon_id" style="display:none">
                <label><b>* Renglon</b></label>
                <select style="width:100%" name="renglon_id" id="renglon_id" class="custom-select">
                </select>
                <small id="msg_renglon_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_api_formulas_id" style="display:none">
                <label><b>* Información disponible</b></label>
                <select style="width:100%" name="api_formulas_id" id="api_formulas_id" class="custom-select">
                    <?php foreach ($formulas_apis as $reporte) { ?>
                        <option value="<?php echo $reporte['id']; ?>"><?php echo '['.$reporte['id'].'] '.$reporte['nombre']; ?></option>
                    <?php } ?>
                </select>
                <small id="msg_api_formulas_id" class="form-text text-danger"></small>
            </div>
        </form>            

    </div>
</div>