<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends MY_Controller
{

    public $reporte_id;
    public $results = array('estatus' => 'ok', 'data' => false, 'mensaje' => false, 'errores' => false);

    public function __construct()
    {
        parent::__construct();
        $this->load->model('CaReportes_model');
        $this->load->model('DeReporteReglones_model');
    }

    function listado()
    {
        $this->response['data'] = $this->CaReportes_model->getAll();
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    function listado_contenido()
    {
        $this->load->model('DeReglonesFormulas_model');
        $reporte_id = $this->input->get('reporte_id');
        $renglones = $this->DeReporteReglones_model->get_list(['reporte_id' => $reporte_id]);

        // if(is_array($renglones) && count($renglones)>0){
        //     foreach ($renglones as $key => $renglon) {
        //         $renglones[$key]['operaciones'] = $this->DeReglonesFormulas_model->get_list(['reporte_reglon_id' => $renglon['id']]);
        //     }
        // }
        $this->response['data'] = $renglones;
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    function listado_operacions_by_renglon()
    {
        $this->load->model('DeReglonesFormulas_model');
        $renglon_id = $this->input->post_get('renglon_id');
        $list =  $this->DeReglonesFormulas_model->get_list(['reporte_reglon_id' => $renglon_id]);

        $data = array(
            'listado' => $list,
            'renglon_id' => $renglon_id
        );
        
        $this->response['data'] = $list;
        $this->response['time'] = utils::get_datetime();
        $this->response['html'] = base64_encode(utf8_decode($this->load->view('api/reportes/listado_operacions_by_renglon',$data,true)));

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    function listado_reglones_reportes()
    {
        $reporte_id = $this->input->post_get('reporte_id');

        $this->load->model('DeReporteReglones_model');
        
        $list =  $this->DeReporteReglones_model->get_list(['reporte_id' => $reporte_id]);

        $data = array(
            'listado' => $list,
            'renglon_id' => $reporte_id
        );
        
        $this->response['data'] = $list;

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }


    function modal_agregar_operacion()
    {
        $this->load->model('DeReglonesFormulas_model');
        $renglon_id = $this->input->post_get('renglon_id');

        $this->load->model('CaCuentas_model');
        $cuentas_dms = $this->CaCuentas_model->getAll();

        $this->load->model('CaReportes_model');
        $this->db->order_by('id','asc');
        $reportes = $this->CaReportes_model->getAll();

        $this->load->model('CaFormulasApis_model');
        $CaFormulasApis_model = $this->CaFormulasApis_model->getAll();

        $data = array(
            'cuentas_dms' => $cuentas_dms,
            'renglon_id' => $renglon_id,
            'reportes' => $reportes,
            'formulas_apis' => $CaFormulasApis_model
        );
        
        // $this->response['data'] = $list;
        $this->response['html'] = base64_encode(utf8_decode($this->load->view('api/reportes/modal_agregar_operacion',$data,true)));

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    

    function save_reporte()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('reporte_nombre', 'Nombre del reporte', 'trim|required');
        $reporte_id = $this->input->post('reporte_id');
        if ($this->form_validation->run() == true) {
            $datos = [
                'nombre' => $this->input->post('reporte_nombre')
            ];
            if (!$reporte_id) {
                $action = $this->CaReportes_model->insert($datos);
            } else {
                $action = $this->CaReportes_model->update($datos, ['id' => $reporte_id]);
            }
            if ($action) {
                $reportes = $this->CaReportes_model->getAll();
                $this->session->unset_userdata('reportes');
                $this->session->set_userdata('reportes', $reportes);
                $this->response['mensaje'] = "Registro guardado correctamente";
                $this->response['data'] = $action;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al guardar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    

    function delete_reporte()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('reporte_id', 'reporte_id', 'trim|required|numeric');
        $reporte_id = $this->input->post('reporte_id');
        if ($this->form_validation->run() == true) {
            $datos = [
                'deleted_at' => date('Y-m-d h:m:i')
            ];

            $deleted_at = $this->CaReportes_model->update($datos, ['id' => $reporte_id]);

            if ($deleted_at) {
                $reportes = $this->CaReportes_model->getAll();
                $this->session->unset_userdata('reportes');
                $this->session->set_userdata('reportes', $reportes);
                $this->response['mensaje'] = "Registro eliminado correctamente";
                $this->response['data'] = $deleted_at;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al eliminar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }
    function save_cuenta()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('reporte_id', 'reporte_id', 'trim|required|numeric');
        $this->form_validation->set_rules('nombre_cuenta', 'Nombre cuenta', 'trim|required');
        if ($this->input->post('tipo_renglon') == 1) {
            $this->form_validation->set_rules('tipo_formato', 'Tipo de formato', 'trim|required');
        }
        $this->form_validation->set_rules('tipo_renglon', 'Tipo renglón', 'trim|required');
        $renglon_id = $this->input->post('renglon_id');
        if ($this->form_validation->run() == true) {
            $datos = [
                'reporte_id' => $this->input->post('reporte_id'),
                'nombre' => $this->input->post('nombre_cuenta'),
                'formato' => (($this->input->post('tipo_formato') == FALSE)? null : $this->input->post('tipo_formato')), //$this->input->post('tipo_formato'),
                'tipo' => (($this->input->post('tipo_renglon') == FALSE)? null : $this->input->post('tipo_renglon'))
            ];

            if($this->input->post('tipo_renglon') != 1){
                $datos['formato'] = null;
            }

            if (!$renglon_id) {
                $maximo = $this->DeReporteReglones_model->get_max_orden(['reporte_id' => $this->input->post('reporte_id')]);
                $orden_max = $maximo['orden'] >= 1 ? $maximo['orden'] + 1 : 1;
                $datos['orden'] = $orden_max;
                $action = $this->DeReporteReglones_model->insert($datos);
            } else {
                $action = $this->DeReporteReglones_model->update($datos, ['id' => $renglon_id]);
            }
            if ($action) {
                $this->response['mensaje'] = "Registro guardado correctamente";
                $this->response['data'] = $action;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al guardar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }
    function save_operacion()
    {
        $this->load->model('DeReglonesFormulas_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('origen_informacion', 'origen_informacion', 'trim|required');
        switch ($this->input->post('origen_informacion')) {
            case '1':
                $this->form_validation->set_rules('cuenta_id', 'Cuenta', 'trim|required');
                break;
            case '2':
                $this->form_validation->set_rules('renglon_id', 'Renglon', 'trim|required');
                break;
            case '3':
                $this->form_validation->set_rules('reporte_id', 'Reporte', 'trim|required');
                $this->form_validation->set_rules('renglon_id', 'Renglon', 'trim|required');
                break;
            
            case '4':
                $this->form_validation->set_rules('cantidad_fija', 'Cantidad', 'trim|required|numeric|is_natural_no_zero');
                break;
            case '5':
                $this->form_validation->set_rules('api_formulas_id', 'Información disponible', 'trim|required');
                break;
        }

        $this->form_validation->set_rules('renglon_id_actual', 'renglon_id_actual', 'trim|required|numeric');
        $this->form_validation->set_rules('tipo_operacion', 'Tipo de operación', 'trim|required');
        
        if ($this->form_validation->run() == true) {

            $origen_informacion = null;
            switch ($this->input->post('origen_informacion')) {
                case 1:
                    $origen_informacion = 'CALCULO_SOBRE_CUENTAS';
                    break;
                case 2:
                case 3:
                    $origen_informacion = 'CALCULO_SOBRE_REGLONES';
                    break;
                case 4:
                    $origen_informacion = 'CALCULO_SOBRE_CANTIDAD';
                    break;
                case 5:
                    $origen_informacion = 'CALCULO_SOBRE_APIS';
                    break;
            }

            $datos = array(
                'reporte_reglon_id' => $this->input->post('renglon_id_actual'),
                'tipo_operacion' => $this->input->post('tipo_operacion'),
                'tipo_calculo' => $origen_informacion
            );

            if($this->input->post('origen_informacion') == 1) {
                $datos['cuenta_id'] =  $this->input->post('cuenta_id');
                $datos['reporte_reglon_id_ref'] =  null;
            }elseif($this->input->post('origen_informacion') == 4) {
                $datos['cantidad'] =  $this->input->post('cantidad_fija');
            }elseif($this->input->post('origen_informacion') == 5) {
                $datos['formula_api_id'] =  $this->input->post('api_formulas_id');
            }else{
                $datos['cuenta_id'] = null;
                $datos['reporte_reglon_id_ref'] =  $this->input->post('renglon_id');   
            }
            
            $operacion_id = $this->input->post('operacion_id');
            if (!$operacion_id) {
                $maximo = $this->DeReglonesFormulas_model->get_max_orden(['reporte_reglon_id' => $this->input->post('renglon_id_actual')]);
                $orden_max = $maximo['orden'] >= 1 ? $maximo['orden'] + 1 : 1;
                $datos['orden'] = $orden_max;
                $action = $this->DeReglonesFormulas_model->insert($datos);
            } else {
                $action = $this->DeReglonesFormulas_model->update($datos, ['id' => $operacion_id]);
            }
            if ($action) {
                $this->response['mensaje'] = "Registro guardado correctamente";
                $this->response['data'] = $action;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al guardar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    function delete_renglon()
    {
        $this->load->model('DeReporteReglones_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('renglon_id', 'renglon_id', 'trim|required|numeric');
        $renglon_id = $this->input->post('renglon_id');
        if ($this->form_validation->run() == true) {
            $datos = [
                'deleted_at' => date('Y-m-d h:m:i')
            ];

            $deleted_at = $this->DeReporteReglones_model->update($datos, ['id' => $renglon_id]);

            if ($deleted_at) {
                $this->response['mensaje'] = "Registro eliminado correctamente";
                $this->response['data'] = $deleted_at;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al eliminar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }
    function delete_operacion()
    {
        $this->load->model('DeReglonesFormulas_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('operacion_id', 'operacion_id', 'trim|required');
        $operacion_id = $this->input->post('operacion_id');
        if ($this->form_validation->run() == true) {
            $datos = [
                'deleted_at' => date('Y-m-d h:m:i')
            ];

            $deleted_at = $this->DeReglonesFormulas_model->update($datos, ['id' => $operacion_id]);

            if ($deleted_at) {
                $this->response['mensaje'] = "Registro eliminado correctamente";
                $this->response['data'] = $deleted_at;
            } else {
                $this->response['estatus'] = 'error';
                $this->response['mensaje'] = "Ocurrio un error al eliminar el registro";
            }
        } else {
            $this->response['estatus'] = 'error';
            $this->response['errores'] = $this->form_validation->error_array();
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->response))
            ->_display();
        exit;
    }

    function actualizar_orden(){
        $orden_list = $this->input->post('orden');
        if(is_array($orden_list)){
            $this->load->model('DeReporteReglones_model');
            foreach ($orden_list as $key => $value) {
                $this->DeReporteReglones_model->update([
                    'orden' => $value['value']
                ],[
                    'id' => $value['id']
                ]);
            }
        }
    }
}
