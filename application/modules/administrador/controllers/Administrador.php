<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Administrador extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    //utils::pre($_SESSION);
    $this->blade->render('index', false);
  }

  public function logout()
  {
    $this->session->set_userdata('logged_in', false);
    $this->session->sess_destroy();
    redirect('inicio/index', 'refresh');
  }
}
