@layout('template_blade/estructura')
@section('contenido')
    <h1 class="h3 mb-4 text-gray-800">
        Semi Nuevos
    </h1>
    <table class="table table-bordered table-responsive table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th></th>
                <th>ENE21</th>
                <th>FEB21</th>
                <th>MAR21</th>
                <th>ABR21</th>
                <th>MAY21</th>
                <th>JUN21</th>
                <th>JUL21</th>
                <th>AGO21</th>
                <th>SEP21</th>
                <th>OCT21</th>
                <th>NOV21</th>
                <th>DIC21</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>

            @foreach($datos as $d => $dato)
              
              <tr>
                <td>{{$nombres[$d]}}</td>
                <td>{{$dato[1]}}</td>
                <td>{{$dato[2]}}</td>
                <td>{{$dato[3]}}</td>
                <td>{{$dato[4]}}</td>
                <td>{{$dato[5]}}</td>
                <td>{{$dato[6]}}</td>
                <td>{{$dato[7]}}</td>
                <td>{{$dato[8]}}</td>
                <td>{{$dato[9]}}</td>
                <td>{{$dato[10]}}</td>
                <td>{{$dato[11]}}</td>
                <td>{{$dato[12]}}</td>
                <td>-</td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
@endsection
