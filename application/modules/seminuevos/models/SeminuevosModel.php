<?php

/**

 **/
class SeminuevosModel extends CI_Model
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
  }





  public function save_register($table, $data)
  {
    $this->db->insert($table, $data);
    return $this->db->insert_id();
  }

  public function get_tabla($tabla)
  {
    return $this->db->get($tabla)->result();
  }

  public function actualizar_tabla($tabla, $id_tabla, $data, $id)
  {
    $this->db->update($tabla, $data, array($id_tabla => $id));
  }

  public function select_row($table, $id_table, $id)
  {
    $this->db->where($id_table, $id);
    return $this->db->get($table)->row();
  }

  public function select_result($table, $id_table, $id)
  {
    $this->db->where($id_table, $id);
    $this->db->order_by($id_table, "asc");
    return $this->db->get($table)->result();
  }

  public function select_result_publicaciones($table, $id_table, $id)
  {
    $this->db->where($id_table, $id);
    $this->db->order_by('principal', "asc");
    return $this->db->get($table)->result();
  }
  public function getAllData(){
    return $this->db->select('cargo,abono,id_id,abono,referencia,MONTH(fecha_creacion) as mes')
              ->where('abono !=',0)
              ->where('YEAR(fecha_creacion)',date('Y'))
              ->get('asientos a')
              ->result();
  }
}
