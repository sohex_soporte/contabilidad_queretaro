<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function listado_facturacion_get()
    {

        $fecha = $this->input->get('fecha');

        $this->load->model('DeFacturas_model');
        // $this->db->where('de_facturas.created_at BETWEEN "'.$fecha. ' 00:00:00" and "'.$fecha. ' 23:59:59" ');
        $this->data['data'] = $this->DeFacturas_model->detalle_list();
        // utils::pre($this->db->last_query());
        $this->response($this->data);
    }


    public function uso_cfdi_get()
    {
        $this->load->model('CaUsoPoliza_model');
        $this->data['data'] = $this->CaUsoPoliza_model->getAll();
        $this->response($this->data);
    }

    public function formas_pago_get()
    {
        $this->load->model('CaFormasPago_model');
        $this->data['data'] = $this->CaFormasPago_model->getAll();
        $this->response($this->data);
    }

    public function metodos_pago_get()
    {
        $this->load->model('CaMetodosPago_model');
        $this->data['data'] = $this->CaMetodosPago_model->getAll();
        $this->response($this->data);
    }

    public function tipo_comprobante_get()
    {
        $this->load->model('CaTipoComprobante_model');
        $this->data['data'] = $this->CaTipoComprobante_model->getAll();
        $this->response($this->data);
    }

    public function crear_factura_post()
    {
        $factura_id = null;
        $cfdi = null;
        $completo = 'no';
        $this->load->library('form_validation');
        $data_validation = $this->input->post();
        if (isset($data_validation)) {
            $detalle_factura = json_decode($this->input->post('detalle_factura'), true);
            if (is_array($detalle_factura)) {
                $data_validation = array_merge($data_validation, $detalle_factura);
            }
        }
        

        $this->form_validation->set_data($data_validation);
        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('sucursal_id', 'ca_sucursales', 'trim|required|exists[ca_sucursales.id]');
        $this->form_validation->set_rules('forma_pago_id', 'forma_pago_id', 'trim|required|exists[ca_metodos_pago.dms_id]');
        $this->form_validation->set_rules('metodo_pago_id', 'metodo_pago_id', 'trim|required|exists[ca_formas_pago.dms_id]');
        $this->form_validation->set_rules('receptor_rfc', 'receptor_rfc', 'trim|max_length[15]');
        $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('receptor_uso_cfdi', 'receptor_uso_cfdi', 'trim|required|exists[ca_uso_cfdi.dms_id]');
        $this->form_validation->set_rules('receptor_domicilio', 'receptor_domicilio', 'trim');
        $this->form_validation->set_rules('observaciones', 'observaciones', 'trim');

        $this->form_validation->set_rules('tipo_factura', 'tipo_factura', 'trim|required');
        if ($this->input->post('tipo_factura') == 1) { // tipo_factura: 1=servicio, 0 = refacciones
            $this->form_validation->set_rules('detalle_factura', 'detalle_factura', 'trim|required');
        }

        if ($this->form_validation->run($this) != FALSE) {

            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get(array(
                'folio' => $this->input->post('folio'),
                'sucursal_id' => $this->input->post('sucursal_id'),
            ));
            if (is_array($transaccion_datos)) {

                $this->load->model('DeFacturas_model');
                $factura_datos = $this->DeFacturas_model->get(array(
                    'transaccion_id' => $transaccion_datos['id'],
                    'completo' => 'si'
                ));
                if ($factura_datos == false) {

                    $this->load->model('CaFormasPago_model');
                    $forma_pago_datos = $this->CaFormasPago_model->get([
                        'dms_id' => $this->input->post('metodo_pago_id')
                    ]);
                    $this->load->model('CaMetodosPago_model');
                    $metodo_pago_datos = $this->CaMetodosPago_model->get([
                        'dms_id' => $this->input->post('forma_pago_id')
                    ]);
                    $this->load->model('CaUsoCFDI_model');
                    $uso_cfdi_datos = $this->CaUsoCFDI_model->get([
                        'dms_id' => $this->input->post('receptor_uso_cfdi')
                    ]);

                    $this->load->model('Asientos_model');
                    $this->db->where('asientos.cargo >', 0, false);
                    $this->db->not_like('cuentas_dms.decripcion', 'iva');
                    $this->db->like('cuentas_dms.decripcion', 'CLIENTE');

                    // $this->db->where_in('asientos.estatus_id', [
                    //     $this->Asientos_model::ESTATUS_APLICADO,
                    //     $this->Asientos_model::ESTATUS_POR_APLICAR //<-----------------------cambios
                    // ]);
                    if ($data_validation['forma_pago_id'] == 2) {
                        $estatus =  $this->Asientos_model::ESTATUS_APLICADO;
                    } else {
                        $estatus =  $this->Asientos_model::ESTATUS_APLICADO;
                    }
                    $asientos_datos = $this->Asientos_model->getDatosFactura([
                        'asientos.transaccion_id' => $transaccion_datos['id'],
                        'estatus_id' => $estatus
                    ]);

                    if($asientos_datos == false){
                        $this->db->like('cuentas_dms.decripcion', 'CTA');
                        $this->db->where_in('estatus_id',[
                            $this->Asientos_model::ESTATUS_APLICADO,
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            // $this->Asientos_model::ESTATUS_TEMPORAL
                        ]);
                        // $this->db->where('cuenta_descripcion','cta');
                        // $this->db->where('cuenta_descripcion','iva');
                        $asientos_datos = $this->Asientos_model->getDatosFactura([
                            'transaccion_id' => $transaccion_datos['id']
                        ]);
                    }

                    if($asientos_datos == false){
                        $this->db->like('cuentas_dms.decripcion', 'CLIENTE');
                        $this->db->where_in('estatus_id',[
                            $this->Asientos_model::ESTATUS_APLICADO,
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            // $this->Asientos_model::ESTATUS_TEMPORAL
                        ]);
                        $asientos_datos = $this->Asientos_model->getDatosFactura([
                            'transaccion_id' => $transaccion_datos['id']
                        ]);
                    }
                    
                    $conceptos = [];
                    $subtotal = 0;
                    $total_iva = 0;
                    $totales = 0;
                    if (is_array($asientos_datos)) {
                        foreach ($asientos_datos as $key => $value) {
                            if ($value['precio_unitario'] > 0) {
                                $moneda_abono_sin_iva = 0;
                                $moneda_abono_iva = 0;
                                $i = $value['precio_unitario'] - ($value['precio_unitario'] * 0.169);
                                $i_fin = $value['precio_unitario'] - ($value['precio_unitario'] * 0.10);
                                $error = 0;

                                while ($i < $i_fin) :
                                    $porcentaje = (float)($i * 0.16) + $i;
                                    if ($porcentaje >= $value['precio_unitario']) {
                                        $moneda_abono_iva = $i * 0.16;
                                        $moneda_abono_sin_iva = $value['precio_unitario'] - $moneda_abono_iva;
                                        $i = $i_fin;
                                    }

                                    $i = $i + 0.01;
                                    $error = $error + 1;

                                    if ($error >= 100000) {
                                        $i = $i_fin;
                                    }
                                endwhile;

                                if ($moneda_abono_iva == 0) {
                                    $moneda_abono_sin_iva = 0;
                                    $moneda_abono_iva = 0;
                                    $i = $value['precio_unitario'] - ($value['precio_unitario'] * 0.169);
                                    $i_fin = $value['precio_unitario'] - ($value['precio_unitario'] * 0.10);
                                    $error = 0;

                                    while ($i < $i_fin) :
                                        $porcentaje = (float)($i * 0.16) + $i;
                                        if ($porcentaje >= $value['precio_unitario']) {
                                            $moneda_abono_iva = $i * 0.16;
                                            $moneda_abono_sin_iva = $value['precio_unitario'] - $moneda_abono_iva;
                                            $i = $i_fin;
                                        }

                                        $i = $i + 0.1;
                                        $error = $error + 1;

                                        if ($error >= 50000) {
                                            $i = $i_fin;
                                        }
                                    endwhile;
                                }

                                $totales = $totales + utils::redondeo($value['precio_unitario']);
                                $moneda_abono_iva = utils::redondeo($moneda_abono_iva);
                                $moneda_abono_sin_iva = utils::redondeo($moneda_abono_sin_iva);

                                $total_iva = $total_iva + $moneda_abono_iva;
                                $subtotal = $subtotal + $moneda_abono_sin_iva;

                                $conceptos[] = [
                                    'id_producto_servicio_interno' => $value['producto_servicio_id'],
                                    'concepto_cantidad' => 1, //$value['cantidad'],
                                    'unidad_interna' => $value['unidad_id'],
                                    'concepto_nombre' => $value['concepto'],
                                    'concepto_precio' => $moneda_abono_sin_iva,
                                    'concepto_importe' => $moneda_abono_sin_iva,
                                    'concepto_NoIdentificacion' => $value['transaccion_id'] . '_' . ($key + 1),
                                    'clave_sat' => '52141807',
                                    'unidad_sat' => 'P83',
                                    'Impuesto' => '002',
                                    'TipoFactor' => 'Tasa',
                                    'TasaOCuota' => '.16',
                                    'Base' => $moneda_abono_sin_iva,
                                    'Importe' => $moneda_abono_iva,
                                ];
                            }
                        }
                    }

                    $this->load->library('parser');

                    $data = array(
                        # [CFD33]
                        'Folio' => $this->input->post('folio'),
                        'Serie' => '100', //$this->input->post('serie'),
                        'fecha' => $transaccion_datos['fecha'],
                        'FormaPago' => $forma_pago_datos['id'], # PARCHE DE CAMBIO POR ALGUN EQUIVOCACION DE ALGUN TIPO DE CATALOGO DMS
                        'metodo_pago' => $metodo_pago_datos['nombre'],
                        'subtotal' => $subtotal, //$this->input->post('sub_total'),
                        'moneda' => DEFAULT_TIPO_MONEDA,
                        'total' => utils::redondeo($subtotal + $total_iva), //$this->input->post('total'),
                        'TipoDeComprobante' => DEFAULT_TIPO_COMPROBANTE,
                        'MetodoPago' => $metodo_pago_datos['id'],
                        'LugarExpedicion' => DEFAULT_LUGAR_EXPEDICION,
                        'comentario' => strlen(trim($this->input->post('observaciones'))) > 0 ? trim($this->input->post('observaciones')) : 'sin comentarios',
                        'iva' => $total_iva, //$this->input->post('iva'),
                        'TipoCambio' => "1", //$this->input->post('tipocambio'),
                        # [RECEPTOR]
                        'receptor_RFC' => strlen(trim($this->input->post('receptor_rfc'))) > 0 ? $this->input->post('receptor_rfc') : 'XAXX010101000',
                        'receptor_nombre' => $this->input->post('receptor_nombre'),
                        'receptor_uso_CFDI' => $uso_cfdi_datos['id'],
                        'receptor_email' => 'correo@sohex.com', //$this->input->post('receptor_email'),
                        'receptor_direccion' => strlen(trim($this->input->post('receptor_domicilio'))) > 0 ? trim($this->input->post('receptor_domicilio')) : 'S/D',
                        # [CONCEPTO]
                        'conceptos' => $conceptos,
                        #NUEVO REQUERIMIENTO
                        'tipo_factura' => $this->input->post('tipo_factura'),
                    );
                    
                    if ($this->input->post('tipo_factura') == 1) {
                        $detalle_factura = json_decode($this->input->post('detalle_factura'), true);

                        $data_complementarios = [
                            'Tipo' => (array_key_exists('tipo_vehiculo', $detalle_factura)) ? $detalle_factura['tipo_vehiculo'] : 'Automóvil',
                            'Serie' => (array_key_exists('serie', $detalle_factura)) ? $detalle_factura['serie'] : '',
                            'Modelo' => (array_key_exists('modelo', $detalle_factura)) ? $detalle_factura['modelo'] : '',
                            'Placas' => (array_key_exists('placas', $detalle_factura)) ? $detalle_factura['placas'] : '',
                            'Color' => (array_key_exists('color', $detalle_factura)) ? $detalle_factura['color'] : '',
                            'KM' => (array_key_exists('km', $detalle_factura)) ? $detalle_factura['km'] : '',
                            'Orden' => (array_key_exists('orden', $detalle_factura)) ? $detalle_factura['orden'] : '',
                            'Version' => (array_key_exists('version', $detalle_factura)) ? $detalle_factura['version'] : '',
                            'fecha_recepcion' => (array_key_exists('fecha_recepcion', $detalle_factura)) ? $detalle_factura['fecha_recepcion'] : '',
                            'Asesor' => (array_key_exists('asesor', $detalle_factura)) ? $detalle_factura['asesor'] : '',
                            'Transmision' => (array_key_exists('transmision', $detalle_factura)) ? $detalle_factura['transmision'] : ''
                        ];
                        $data = array_merge($data, $data_complementarios);
                    }
                    // utils::pre($data);
                    $response = utils::api_post_json([
                        'url' => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/facturacion/api_genera_factaura',
                        // 'url' => (($this->input->post('forma_pago_id') == 2)? 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/complemento_pago/api_genera_complemento' : 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/facturacion/api_genera_factaura'),
                        'params' => @json_encode($data)
                        // 'params' => '{"moneda":"MXN","subtotal":"100","iva":"16","total":"116","fecha":1644552476,"LugarExpedicion":"28983","FormaPago":"27","MetodoPago":"PUE","Folio":"100","Serie":"100","TipoDeComprobante":"I","TipoCambio":"1","comentario":"comentario factura","receptor_RFC":"EKU9003173C9","receptor_uso_CFDI":"I08","receptor_nombre":"nombre razon social","receptor_email":"liberiusg@gmail.com","receptor_direccion":"direccion","conceptos":[[{"clave_sat":"52141807","concepto_cantidad":"1","unidad_sat":"P83","concepto_nombre":"nombre producto, servicio","concepto_precio":"100","concepto_importe":"100","Impuesto":"002","TipoFactor":"Tasa","TasaOCuota":".16","Base":"100","Importe":"16","id_producto_servicio_interno":"pro1","unidad_interna":"uni1","concepto_NoIdentificacion":"10"}]]}'
                    ]);
                    $completo = is_array($response) && array_key_exists('pdf', $response) ? 'si' : 'no';
                    $factura_id = $this->DeFacturas_model->insert([
                        'envio_api' => @json_encode($data),
                        'folio' => $this->input->post('folio'),
                        'fecha' => $transaccion_datos['fecha'],
                        'forma_pago_id' => $forma_pago_datos['id'],
                        'metodo_pago_id' => $metodo_pago_datos['id'],
                        'sub_total' => $subtotal,
                        'moneda' => DEFAULT_TIPO_MONEDA,
                        'total' => $total_iva + $subtotal,
                        'tipo_comprobante' => DEFAULT_TIPO_COMPROBANTE,
                        'lugar_expedicion' => DEFAULT_LUGAR_EXPEDICION,
                        'comentario' => $this->input->post('observaciones'),
                        'iva' => $total_iva,
                        'serie' => '100',
                        'tipo_cambio' => "1",
                        'transaccion_id' => $transaccion_datos['id'],
                        'receptor_rfc' => $this->input->post('receptor_rfc'),
                        'receptor_nombre' => $this->input->post('receptor_nombre'),
                        'receptor_uso_cfdi' => $uso_cfdi_datos['id'],
                        'receptor_email' => $this->input->post('receptor_email'),
                        'receptor_direccion' => $this->input->post('receptor_domicilio'),
                        // 'cfdi' => base64_encode($cfdi),
                        'error_mensaje' => is_array($response) && array_key_exists('error_mensaje', $response) ? $response['error_mensaje'] : null,
                        'factura' => is_array($response) && array_key_exists('factura', $response) ? $response['factura'] : null,
                        'pdf' => is_array($response) && array_key_exists('pdf', $response) ? $response['pdf'] : null,
                        'xml' => is_array($response) && array_key_exists('xml', $response) ? $response['xml'] : null,
                        'respuesta_api' => @json_encode($response),
                        'completo' => $completo,
                        'sat_uuid' => is_array($response) && array_key_exists('sat_uuid', $response) ? $response['sat_uuid'] : null
                    ]);
                    $cfdi = is_array($response) && array_key_exists('pdf', $response) ? $response['pdf'] : null;
                    $this->data['status'] = is_array($response) && array_key_exists('pdf', $response) ? 'success' : 'error';

                    $this->data['message'] = [
                        'FACTURA PROCESADA 1'
                    ];
                } else {
                    $cfdi = $factura_datos['pdf'];
                    $completo = $factura_datos['completo'];
                    $factura_id = $factura_datos['id'];
                    $this->data['message'] = [
                        'FACTURA RECUPERADA 2'
                    ];
                }
            }
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->data['data'] = [
            'factura_id' => $factura_id,
            'completo' => $completo,
            'cfdi' => $cfdi
        ];

        $this->response($this->data);
    }

    public function getComprobanteFiscal_get()
    {

        $folio = $this->input->get('folio');
        $this->load->model('CaTransacciones_model');
        $this->data['data'] = $this->CaTransacciones_model->getTransacionFactura($folio);
        $this->response($this->data);
    }

    public function complementoFiscal_post()
    {

        $this->load->model('ComplementoFacturas_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('sucursal_id', 'ca_sucursales', 'trim|required|exists[ca_sucursales.id]');
        $this->form_validation->set_rules('abono_id', 'abono_id', 'trim|required');
        if ($this->form_validation->run($this) != FALSE) {
            $where = [
                'folio' => $this->input->post('folio'),
                'abono_id' => $this->input->post('abono_id'),
                'sucursal_id' => $this->input->post('sucursal_id')
            ];
            $this->data['data'] = $this->ComplementoFacturas_model->get($where);
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function save_complemento_post()
    {

        $this->load->model('ComplementoFacturas_model');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sat_uuid', 'sat_uuid', 'trim|required');
        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('sucursal_id', 'ca_sucursales', 'trim|required|exists[ca_sucursales.id]');
        $this->form_validation->set_rules('abono_id', 'abono_id', 'trim|required');
        $this->form_validation->set_rules('respuesta_api', 'respuesta_api', 'trim|required');
        $this->form_validation->set_rules('pdf', 'pdf', 'trim');

        if ($this->form_validation->run($this) != FALSE) {
            $insert = $this->ComplementoFacturas_model->insert([
                'sat_uuid' => $this->input->post('sat_uuid'),
                'folio' => $this->input->post('folio'),
                'sucursal_id' => $this->input->post('sucursal_id'),
                'abono_id' => $this->input->post('abono_id'),
                'pdf' => $this->input->post('pdf'),
                'respuesta_api' => $this->input->post('respuesta_api'),
            ]);
            if ($insert) {
                $this->data['message'] = [
                    'Complemento guardado correctamente'
                ];
            } else {
                $this->data['message'] = [
                    'Ocurrio un error al guardar el complemento'
                ];
                $this->data['status'] = 'error';
            }
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }
}
