@layout('template_blade/estructura')
@section('style')

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>

@endsection
@section('contenido')

<div class="row mb-2 mt-2">
    <div class="col-sm-12">
        <h3>Datos de la poliza</h3>
    </div>
</div>

<div class="card">
  <div class="card-body">
    <h6 class="card-subtitle mb-2 text-muted">Detalle</h6>
    
    <div class="row mt-4">
        <div class="col-sm-12">
            <form>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Folio:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $factura_datos['folio']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">RFC:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $factura_datos['receptor_rfc']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Receptor:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $factura_datos['receptor_nombre']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Forma de pago:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo '['.$factura_datos['forma_pago_id'].'] '.$factura_datos['forma_pago']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Metodo de pago:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo '['.$factura_datos['metodo_pago_id'].'] '.$factura_datos['metodo_pago']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Uso de CFDI:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo '['.$factura_datos['receptor_uso_cfdi'].'] '.$factura_datos['uso_cfdi']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Subtotal:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $factura_datos['sub_total']; ?>">
                    </div>
                </div>
               
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Total:</label>
                    <div class="col-sm-10">
                    <input type="text" readonly disabled class="form-control-plaintext" id="" value="<?php echo $factura_datos['total']; ?>">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">PDF:</label>
                    <div class="col-sm-10">
                        <a target="_blank" href="<?php echo $factura_datos['pdf']; ?>" class="btn btn-info btn-sm" >
                            <i class="far fa-file-pdf"></i> CFDI
                        </a>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">XML:</label>
                    <div class="col-sm-10">
                        <a target="_blank" href="<?php echo $factura_datos['xml']; ?>" class="btn btn-info btn-sm" >
                            <i class="far fa-file-code"></i> Archivo XML
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr/>
    <h6 class="card-subtitle mb-2 text-muted mt-4 mb-2">Asientos</h6>

    <div class="row">
        <div class="col-sm-12">
            <?php echo $tabla_content; ?>
        </div>
    </div>


  </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a type="button" href="<?php echo site_url('facturacion'); ?>" class="btn btn-secondary mt-4 mb-3"><i class="fa fa-arrow-left"></i> Regresar</a>
    </div>
</div>

@endsection

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Archivo TXT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <pre><code style="font-size: 11px;"><?php print_r(base64_decode($factura_datos['cfdi'])); ?></code></pre>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@section('script')
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/polizas/polizas/index.js'); ?>"></script>
@endsection