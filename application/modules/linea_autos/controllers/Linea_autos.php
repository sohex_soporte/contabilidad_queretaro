<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Linea_autos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
       
    }

    public function lista($filtro = 1){

        
        $datos['lista'] = $this->Mgeneral->get_table('linea_autos');
        //$datos['filtro'] = $filtro;
        $datos['index'] = '';
        $this->blade->render('lista',$datos);
    }

    public function crear(){
        $datos['index'] = '';
        $datos['cuentas'] = $this->Mgeneral->get_table('cuentas_dms');
        $this->blade->render('crear',$datos);
    }

    public function empList(){
     
        // POST data
        $postData = $this->input->post();
   
        // Get data
        $data = $this->Mgeneral->getEmployees($postData);
   
        echo json_encode($data);
        exit();
     }

     public function guarda_datos(){
        error_reporting(0);

        $this->form_validation->set_rules('linea', 'linea', 'required');
        $this->form_validation->set_rules('cuenta_venta', 'cuenta_venta', 'required');
        $this->form_validation->set_rules('cuenta_inventario', 'cuenta_inventario', 'required');
        $this->form_validation->set_rules('cuenta_costo', 'cuenta_costo', 'required');
    
        $response = validate($this);
  
        if($response['status']){
          $data['linea'] = $this->input->post('linea');
          $data['cuenta_ventas'] = $this->input->post('cuenta_venta');
          $data['cuenta_inventario'] = $this->input->post('cuenta_inventario');
          $data['cuenta_costo'] = $this->input->post('cuenta_costo');
          $this->Mgeneral->save_register('linea_autos',$data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));

      exit();

     }

     public function ver($id){
        $datos['index'] = '';
        $datos['linea'] = $this->Mgeneral->get_row('id',$id,'linea_autos');
        $this->blade->render('ver',$datos);
    }

    public function api(){
        $datos = $this->Mgeneral->get_table('linea_autos');
        echo json_encode($datos);
        exit();
    }


    public function api_cuenta_inventario(){

        error_reporting(0);
        ini_set('display_errors', 0);

        $this->db->select('cuentas_dms.id, cuentas_dms.cuenta, cuentas_dms.decripcion as descripcion');
        $this->db->from('cuentas_dms');
        $this->db->join('linea_autos', 'cuentas_dms.id = linea_autos.cuenta_inventario');
        //$this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
        $resultado = $this->db->get()->result();
        $data['status'] = "ok";
        $data['data'] = $resultado;
        


       // $datos = $this->Mgeneral->get_table('linea_autos');
        echo json_encode($data);
        exit();
    }
}