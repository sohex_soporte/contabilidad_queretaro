@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')



<script src="<?php echo base_url()?>statics/js/isloading.js"></script>
<script type="text/javascript">
      menu_activo = "facturacion";
$("#menu_facturacion_facturar").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});



      $(".eliminar_factura").click(function(event){
           event.preventDefault();

            bootbox.dialog({
               message: "Desea cancelar la factura?",
               closeButton: true,
               buttons:
               {
                   "danger":
                   {
                       "label": "Aceptar ",
                       "className": "btn-danger",
                       "callback": function () {

                         id = $(event.currentTarget).attr('flag');
                         url = $("#delete"+id).attr('href');
                         //$.get(url);
                         var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Cancelando Factura...</p>',
                            closeButton: false
                        });
                         
                         ajaxJson(url,{},
                                   "POST",true,function(result){

                           console.log(result);

                           json_response = JSON.parse(result);

                           obj_status = json_response.error;
                           if(obj_status == true){
                            dialog_load.modal('hide');

                             exito("<h3>ERROR intente de nuevo<h3/> ","danger");
                           }
                           if(obj_status == false){
                            dialog_load.modal('hide');

                             exito_redirect("FACTURA CANCELADA CON EXITO","success","<?php echo base_url()?>index.php/factura/lista");
                           }
                         });
                       }
                   },
                   "cancel":
                   {
                       "label": "<i class='icon-remove'></i> Cancelar",
                       "className": "btn-sm btn-info",
                       "callback": function () {

                       }
                   }

               }
           });
       });

    } );
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <div class="">
      <a href="<?php echo base_url();?>index.php/linea_autos/crear/"><button type="button" class="btn btn-primary">Crear linea</button></a>
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   <!--label>Filtro:</label>
                   <?php 
                   $color_filtro_todo = "btn-light";
                   $color_filtro_prefactura = "btn-light";
                   if($filtro == 1){
                      $color_filtro_todo = "btn-secondary";
                      $color_filtro_prefactura = "btn-light";
                   }?>
                   <?php if($filtro == 0){
                      $color_filtro_todo = "btn-light";
                      $color_filtro_prefactura = "btn-secondary";
                   }?>
                   <a class=""   href="<?php echo base_url()?>index.php/factura/lista/1">
                      <button type="button" class="btn <?php echo $color_filtro_todo;?>">Todo</button>
                   </a>
                   <a class=""   href="<?php echo base_url()?>index.php/factura/lista/0">
                      <button type="button" class="btn <?php echo $color_filtro_prefactura;?>">Pre-Facturas</button>
                   </a>
                   <br/-->
                   <br/>
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Linea</th>
                        <th>Cuenta venta</th>
                        <th>Cuenta inventario</th>
                        <th>Cuenta costo</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($lista)):?>
                      <?php foreach($lista as $row):?>
                        <tr>
                          <td><?php echo $row->linea;?></td>
                          <td><?php echo $this->Mgeneral->get_row('id',$row->cuenta_ventas,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$row->cuenta_ventas,'cuentas_dms')->decripcion;?></td>
                          <td><?php echo $this->Mgeneral->get_row('id',$row->cuenta_inventario,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$row->cuenta_inventario,'cuentas_dms')->decripcion;?></td>
                          <td><?php echo $this->Mgeneral->get_row('id',$row->cuenta_costo,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$row->cuenta_costo,'cuentas_dms')->decripcion;?></td>
                          
                          
                          

                          <td>
                            <a href="<?php echo base_url()?>index.php/linea_autos/ver/<?php echo $row->id;?>">
                            <button type="button" class="btn btn-info">Ver</button>
                            </a>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


	
@endsection
