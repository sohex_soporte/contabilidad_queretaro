@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')



<script type="text/javascript">


</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <input type="hidden" id="tipo_cuenta"/>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                <div class="row">

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Linea auto</span></label>
                                <input id="linea" name="linea" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $linea->linea;?>" >
                            
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta venta</span></label>
                                <input id="cuenta_venta" name="cuenta_venta" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_venta1" name="cuenta_venta1" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $this->Mgeneral->get_row('id',$linea->cuenta_ventas,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$linea->cuenta_ventas,'cuentas_dms')->decripcion; ?>"  >
                            
                        </div>

                    </div>

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta inventario</span></label>
                                <input id="cuenta_inventario" name="cuenta_inventario" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_inventario1" name="cuenta_inventario1" type="text" class="form-control-sm alto form-control cc-exp"  value="<?php echo $this->Mgeneral->get_row('id',$linea->cuenta_inventario,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$linea->cuenta_inventario,'cuentas_dms')->decripcion; ?>" >
                            
                        </div>

                    </div>

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta costo</span></label>
                                <input id="cuenta_costo" name="cuenta_costo" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_costo1" name="cuenta_costo1" type="text" class="form-control-sm alto form-control cc-exp"  value="<?php echo $this->Mgeneral->get_row('id',$linea->cuenta_costo,'cuentas_dms')->cuenta." ".$this->Mgeneral->get_row('id',$linea->cuenta_costo,'cuentas_dms')->decripcion; ?>" >
                            
                        </div>

                    </div>

                </div>

                <div class="row">
                   <div class="col-4">
                       
                    </div>
                </div>

                   
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->





	
@endsection
