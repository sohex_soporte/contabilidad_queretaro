@layout('template_blade/estructura')

@section('included_css')
    
@endsection

@section('contenido')



<!--script src="<?php echo base_url()?>statics/js/isloading.js"></script-->
<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script-->

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('#bootstrap-data-table').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?=base_url()?>index.php/linea_autos/empList'
          },
          'columns': [
           
             { data: 'id'},
             { data: 'cuenta' },
             { data: 'decripcion' },
             { data: 'opciones',render: function(data, type) {
                    if (type === 'display') {

                        let link = data;// "http://datatables.net";
 
                        return '<button type="button" class="btn btn-info" onclick="seleccionar_cuenta('+data+')">seleccionar</button>';
                    }
                     
                    return data;
                }
                
                 },
             
          ]
        });

  
});

function seleccionar_c(tipo){
    $('#tipo_cuenta').val(tipo);

}

function seleccionar_cuenta(id_cuenta,cuenta){
    tipo_cuenta = $('#tipo_cuenta').val();
    if(tipo_cuenta == 1){
        $('#cuenta_venta').val(id_cuenta);
        $('#cuenta_venta1').val(cuenta);
    }
    if(tipo_cuenta == 2){
        $('#cuenta_inventario').val(id_cuenta);
        $('#cuenta_inventario1').val(cuenta);
    }
    if(tipo_cuenta == 3){
        $('#cuenta_costo').val(id_cuenta);
        $('#cuenta_costo1').val(cuenta);
    }
    $('#exampleModal').modal('hide');
}

function guardar_datos(){
    
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/linea_autos/guarda_datos/";
        ajaxJson(url,{"linea":$("#linea").val(),
                      "cuenta_venta":$("#cuenta_venta").val(),
                      "cuenta_inventario":$("#cuenta_inventario").val(),
                      "cuenta_costo":$("#cuenta_costo").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/linea_autos/lista/");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
     
}


</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <input type="hidden" id="tipo_cuenta"/>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                <div class="row">

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Linea auto</span></label>
                                <input id="linea" name="linea" type="text" class="form-control-sm alto form-control cc-exp" value="" >
                            
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta venta</span></label>
                                <input id="cuenta_venta" name="cuenta_venta" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_venta1" name="cuenta_venta1" type="text" class="form-control-sm alto form-control cc-exp" data-toggle="modal" data-target="#exampleModal" value="" onclick="seleccionar_c(1)" >
                            
                        </div>

                    </div>

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta inventario</span></label>
                                <input id="cuenta_inventario" name="cuenta_inventario" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_inventario1" name="cuenta_inventario1" type="text" class="form-control-sm alto form-control cc-exp" data-toggle="modal" data-target="#exampleModal" value="" onclick="seleccionar_c(2)">
                            
                        </div>

                    </div>

                    <div class="col-4">

                        <div class="form-group">
                            <label for="factura_moneda" class="text-dark"> <span>Cuenta costo</span></label>
                                <input id="cuenta_costo" name="cuenta_costo" type="hidden" class="form-control-sm alto form-control cc-exp"  >
                                <input id="cuenta_costo1" name="cuenta_costo1" type="text" class="form-control-sm alto form-control cc-exp" data-toggle="modal" data-target="#exampleModal" value="" onclick="seleccionar_c(3)">
                            
                        </div>

                    </div>

                </div>

                <div class="row">
                   <div class="col-4">
                       <button onclick="guardar_datos()" type="button" class="btn btn-primary">Guardar linea</button>

                    </div>
                </div>

                   
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->




<!-- Modal -->
<div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <table id="bootstrap-data-table" class="display" style="width:100%">
        <thead>
            <tr>
            <th>id</th>
                <th>Cuenta</th>
                <th>Descripción</th>
                <th>Opciones</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
            <th>id</th>
            <th>Cuenta</th>
                <th>Descripción</th>
                <th>Opciones</th>
                
            </tr>
        </tfoot>
    </table>
                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>
        
      </div>
    </div>
  </div>
</div>


	
@endsection
