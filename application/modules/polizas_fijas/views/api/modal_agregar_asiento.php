<form id="data_form_modal" >
    <div class="row">
        <div class="col-sm-12">
            
            <div class="form-group ">
                <label for="modal_tipo" class="col-form-label">Tipo:</label>
                
                    <select  class="form-control" id="modal_tipo" name="modal_tipo" >
                            <option value="1">Cargo</option>
                            <option value="2">Abono</option>
                    </select>
                    <small id="msg_modal_tipo" class="form-text text-danger"></small>
                
            </div>
        </div>
        
        <div class="col-sm-12">
            <div class="form-group">
                <label for="precio" class=" col-form-label">Monto</label>
                <input type="number" step="0.01" class="form-control" id="precio" name="precio" min="0" >
                <small id="msg_precio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="concepto" class="col-form-label">Concepto</label>
                <textarea type="text"  class="form-control" id="concepto" name="concepto"></textarea>
                <small id="msg_concepto" class="form-text text-danger"></small>
            </div>
        </div>
    </div>
</form>