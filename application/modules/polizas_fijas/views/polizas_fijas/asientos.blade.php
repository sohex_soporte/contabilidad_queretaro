@section('contenido')
<script>
    const transaccion_id = "<?php echo $datos_transaccion['id']; ?>";
    const poliza_id = "<?php echo $poliza_id; ?>";
</script>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    POLIZA FIJA <br />
    <small><?php echo utils::folio($datos_polizas['id'],2).' '.$datos_polizas['poliza']; ?></small>
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

            <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">LISTADO DE ASIENTOS</h5>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form action="<?php echo site_url('polizas_fijas/abrir_poliza/'.$datos_transaccion['poliza_fija_id']); ?>" class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Fecha: </b>
                                </span>
                                <input class="form-control mr-sm-2" type="date" name="fecha" placeholder=""
                                    aria-label="" value="<?php echo $datos_transaccion['fecha']; ?>" max="<?php echo utils::get_date(); ?>">
                                    <small id="msg_fecha" class="form-text text-danger"></small>
                                <button class="btn btn-primary ml-2 my-2 my-sm-0" type="submit">Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>

                
                <div class="row mb-4">
                    <div class=" col-sm-12">
                        <?php if($datos_transaccion['estatus_id'] == 'ACTIVO'){ ?>
                        <button class="btn btn-sm btn-secondary ml-2 my-2 my-sm-0 float-right"
                            onclick="APP.aplicar_transaccion();" type="button"><i class="fas fa-save"></i> Guardar poliza</button>

                        <button title="Agregar" class="btn btn-sm btn-success ml-2 my-2 my-sm-0 float-right"
                            onclick="APP.modal_agregar_asiento();" type="button"><i class="fas fa-plus"></i> Agregar
                            asiento</button>
                        <?php }else{ ?>
                            <div class="alert alert-info" role="alert">
                                La poliza ya fue guardada
                            </div>
                        <?php } ?>
                    </div>
                </div>
                

                <div class="row">
                    <div class=" col-sm-12">
                    <?php if($acumulado_abono <> $acumulado_cargo){ ?>
                            <div class="alert alert-danger" role="alert">
                                Los montos de cargos y abonos no coinciden
                            </div>
                        <?php } ?>
                        
                        <div class="table-responsive">
                        <?php echo $tabla; ?>
                        </div>
                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a type="button" href="<?php echo site_url('polizas_fijas'); ?>" class="btn btn-secondary mt-4 mb-3"><i
                class="fa fa-arrow-left"></i> Regresar</a>
    </div>
</div>


<div class="modal fade" id="modal_agregar_asiento" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="modal_agregar_asientoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_agregar_asientoLabel">Agregar asiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="modal_agregar_asiento_button" class="btn btn-primary" onclick="APP.modal_agregar_asiento_guardar();"><i
                        class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')

<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/polizas_fijas/polizas_fijas/asientos.js'); ?>"></script>
@endsection