<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Polizas_fijas extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('poliza','fija');
    }

    public function index()
    {

        $this->load->model('CaPolizasFijas_model');
        $listado_polizas = $this->CaPolizasFijas_model->getAll();

        $this->load->library('table');
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-striped table-bordered">',
        );
        $this->table->set_heading('Identificador', 'Nombre', 'Acciones');
        $this->table->set_template($template);

        if (is_array($listado_polizas)) {
            foreach ($listado_polizas as $listado) {
                $boton_asiento = '<a title="Agregar" href="' . site_url('polizas_fijas/abrir_poliza/' . $listado['id']) . '" class="btn btn-sm btn-secondary ml-2 my-2 my-sm-0 float-right"  type="button"><i class="fas fa-plus"></i></a>';
                $this->table->add_row(utils::folio($listado['id'], 2), $listado['poliza'], $boton_asiento);
            }
        }

        $data = [
            'listado_poliza' => $this->table->generate()
        ];

        $this->blade->render('/polizas_fijas/index', $data);
    }

    private function get_poliza_fija($fecha,$poliza_id){
        $this->load->model('CaPolizas_model');
        $poliza_datos = $this->CaPolizas_model->get([
            'ca_polizas.PolizaNomenclatura_id' => 'DO',
            'dia' => $poliza_id,
            'ca_polizas.fecha_creacion' => $fecha
        ]);

        if($poliza_datos == false){
            $d = new DateTime($fecha);
            $poliza_std_id = $this->get_poliza_fija_nuevo($fecha,$poliza_id);
        }else{
            $poliza_std_id = $poliza_datos['id'];
        }
        return $poliza_std_id;
    }

    private function get_poliza_fija_nuevo($fecha,$poliza_id){
        $this->load->model('CaPolizas_model');
        $d = new DateTime($fecha);
        $poliza_std_id = $this->CaPolizas_model->insert([
            'PolizaNomenclatura_id' => 'DO',
            'ejercicio' => $d->format('Y'),
            'mes' => $d->format('m'),
            'dia' => $poliza_id,
            'fecha_creacion' => $fecha,
        ]);
        return $poliza_std_id;
    }

    public function abrir_poliza($poliza_id)
    {
        $fecha = $this->input->get('fecha');
        if ($fecha == false) {
            $fecha = utils::get_date();
        }

        

        $this->load->model('CaTransacciones_model');
        $datos_poliza = $this->CaTransacciones_model->get([
            'origen' => 'POLIZA_FIJA',
            'poliza_fija_id' => $poliza_id,
            'fecha' => $fecha,
            'estatus_id' => 'ACTIVO'
        ]);

        $id = null;
        $id_poliza_fija = null;
        if (is_array($datos_poliza)) {
            $id = $datos_poliza['id'];
            $id_poliza_fija = $poliza_id;

            $poliza_std_id = $this->get_poliza_fija($fecha,$poliza_id);

        } else {
            $id = $this->CaTransacciones_model->insert([
                'folio' => 'DO'.utils::folio($poliza_id,3),
                'origen' => 'POLIZA_FIJA',
                'poliza_fija_id' => $poliza_id,
                'fecha' => $fecha,
                'estatus_id' => 'ACTIVO'
            ]);

            $poliza_std_id = $this->get_poliza_fija_nuevo($fecha,$poliza_id);

            $this->load->model('DePolizasFijas_model');
            $id_poliza_fija = $this->DePolizasFijas_model->insert([
                'id_transaccion' => $id,
                'id_poliza_fija' => $poliza_id,
                'fecha_poliza' => utils::get_date()
            ]);
        

            $this->load->model('DePolizasFijasConceptos_model');
            $listado_conceptos = $this->DePolizasFijasConceptos_model->getAll([
                'id_poliza_fija' => $poliza_id
            ]);

            $array_replace = [
                utils::aMes_espanol($fecha),
                utils::aMes_espanol($fecha,true),
                utils::get_anio(),
                substr(utils::get_anio(),-2)
            ];

            $array_search = [
                '{mes_letra}',
                '{mes_letra_min}',
                '{anio}',
                '{anio_min}'
            ];

            foreach ($listado_conceptos as $key => $value) {
                $concepto = str_replace($array_search, $array_replace,$value['concepto']);

                $this->load->model('Asientos_model');
                $asiento_id = $this->Asientos_model->insert([
                    'polizas_id' => $poliza_std_id,
                    'estatus_id' => 'TEMPORAL',
                    'transaccion_id' => $id,
                    'tipo_pago_id' => 'OTRO',
                    'cuenta' => 2974,
                    'cargo' => 0,
                    'abono' => 0,
                    'concepto' => $concepto,
                    'fecha_creacion' => utils::get_date(),
                    'precio_unitario' => 0,
                    'cantidad' => 0,
                    'departamento_id' => 9
                ]);


                $this->load->model('DePolizaFijasAsientos_model');
                $this->DePolizaFijasAsientos_model->insert([
                    'id_asiento' => $asiento_id,
                    'id_poliza_fija' => $id_poliza_fija,
                    'cargo' => 0,
                    'abono' => 0,
                ]);
            }

        }
        
        redirect('polizas_fijas/asientos/' . $id.'/'.$poliza_std_id);
    }

    public function asientos($transaccion_id,$poliza_id)
    {

        $this->load->model('CaTransacciones_model');
        $datos_transaccion = $this->CaTransacciones_model->get([
            'id' => $transaccion_id
        ]);

        $this->load->model('CaPolizasFijas_model');
        $datos_polizas = $this->CaPolizasFijas_model->get([
            'id' => $datos_transaccion['poliza_fija_id']
        ]);

        $response = utils::api_get([
            'url' => site_url('asientos/api/detalle?transaccion_id=' . $transaccion_id . '&estatus=POLIZAS_FIJAS')
        ]);
        // utils::pre($response);

        $this->load->library('table');
        $template = array(
            'table_open'            => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $header = [
            'No. Asiento',
            'Concepto',
            'Cargo',
            'Abono',
            // 'Acumulado',
            // 'Fecha de registro'

        ];

        if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
            $header[] = 'Acciones';
        }

        $this->table->set_heading($header);

        $acumulado = 0;
        $acumulado_abono = 0;
        $acumulado_cargo = 0;
        $cont = 1;
        if (is_array($response) && array_key_exists('data', $response) && is_array($response['data']) && count($response['data']) > 0) {
            foreach ($response['data'] as $key => $value) {

                // $acumulado = $acumulado + ($value['abono'] - $value['cargo']);
                $acumulado_abono = $acumulado_abono + $value['abono'];
                $acumulado_cargo = $acumulado_cargo + $value['cargo'];

                $contenido = array(
                    utils::folio($cont, 6),
                    $value['concepto'],
                    utils::format($value['cargo']),
                    utils::format($value['abono']),
                    // utils::format($acumulado),
                );

                if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                    $botones = '<button title="Editar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-primary ml-2 btn-sm" onclick="APP.modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>' .
                        '<button title="Eliminar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-danger ml-2 btn-sm" onclick="APP.eliminar_renglon(this);" ><i class="far fa-trash-alt"></i></div>';
                    $contenido[] = $botones;
                }

                $this->table->add_row($contenido);
                $cont++;
            }

            if (is_array($response) && array_key_exists('total', $response) && is_array($response['total']) && count($response['total']) > 0) {
                $cell = array('data' => 'Total', 'class' => '', 'colspan' => 2);
                if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                    $this->table->add_row($cell, utils::format($response['total']['cargo']), utils::format($response['total']['abono']), '');
                } else {
                    $this->table->add_row($cell, utils::format($response['total']['cargo']), utils::format($response['total']['abono']));
                }
            }
        } else {
            if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 5);
            } else {
                $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 4);
            }
            $this->table->add_row($cell);
        }

        $data_content = [
            'poliza_id' => $poliza_id,
            'datos_polizas' => $datos_polizas,
            'datos_transaccion' => $datos_transaccion,
            'tabla' => $this->table->generate(),
            'acumulado_abono' => $acumulado_abono,
            'acumulado_cargo' => $acumulado_cargo
        ];

        $this->blade->render('/polizas_fijas/asientos', $data_content);
    }
}
