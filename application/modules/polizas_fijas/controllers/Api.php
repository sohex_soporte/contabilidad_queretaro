<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    

    /** uso */
    public function modal_agregar_asiento_get(){

        $data_content = [
        ];

        $html = $this->load->view('/api/modal_agregar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
        );
        $this->response($this->data);
    }

    /** uso */
    public function modal_editar_asiento_get(){

        $renglon_id = $this->input->get('renglon_id');


        $this->load->model('Asientos_model');
        $asiento = $this->Asientos_model->get([
            'id' => $renglon_id
        ]);

        $data_content = [
        ];

        $html = $this->load->view('/api/modal_editar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
            'asiento' => $asiento
        );
        $this->response($this->data);
    }

    public function modal_agregar_asiento_borrar_post(){
        $this->load->model('DePolizaFijasAsientos_model');
        $this->DePolizaFijasAsientos_model->delete([
            // 'id_poliza_fija' => $this->db->post('id_poliza_fija'),
            'id_asiento' => $this->input->post('id_asiento')
        ]);
        exit(0);
    }

    public function modal_agregar_asiento_post(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');
        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->insert([
                'polizas_id' => $this->input->post('poliza_id'),
                'estatus_id' => 'TEMPORAL',
                'transaccion_id' => $this->input->post('transaccion_id'),
                'tipo_pago_id' => 'OTRO',
                'cuenta' => 2974,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                'departamento_id' => 9
            ]);


            $this->load->model('DePolizasFijas_model');
            $poliza_fija = $this->DePolizasFijas_model->get([
                'id_transaccion' => $this->input->post('transaccion_id'),
            ]);
            $id_poliza_fija = null;
            if($poliza_fija == false){
                $id_poliza_fija = $this->DePolizasFijas_model->insert([
                    'id_transaccion' => $this->input->post('transaccion_id'),
                    'id_poliza_fija' => $this->input->post('polizas_id'),
                    'fecha_poliza' => utils::get_date()
                ]);
            }else{
                $id_poliza_fija = $poliza_fija['id'];
            }

            $this->load->model('DePolizaFijasAsientos_model');
            $this->DePolizaFijasAsientos_model->insert([
                'id_asiento' => $asiento_id,
                'id_poliza_fija' => $id_poliza_fija,
                'cargo' => $cargo,
                'abono' => $abono,
            ]);

            $this->data['data'] = [
                'asiento_id' => $asiento_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


    public function modal_editar_asiento_post(){
        $this->load->library('form_validation');

        // $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');
        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $renglon_id = $this->input->post('renglon_id');
            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');
            
            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->update([
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
            ],[
                'id' => $renglon_id
            ]);


            

            $this->load->model('DePolizasFijas_model');
            $poliza_fija = $this->DePolizasFijas_model->get([
                'id_transaccion' => $this->input->post('transaccion_id'),
            ]);
            $id_poliza_fija = null;
            if($poliza_fija == false){
                $id_poliza_fija = $this->DePolizasFijas_model->insert([
                    'id_transaccion' => $this->input->post('transaccion_id'),
                    'id_poliza_fija' => $this->input->post('polizas_id'),
                    'fecha_poliza' => utils::get_date()
                ]);
            }else{
                $id_poliza_fija = $poliza_fija['id'];
            }

            $this->load->model('DePolizaFijasAsientos_model');
            $this->DePolizaFijasAsientos_model->delete([
                'id_poliza_fija' => $id_poliza_fija,
                'id_asiento' => $renglon_id
            ]);

            $this->load->model('DePolizaFijasAsientos_model');
            $this->DePolizaFijasAsientos_model->insert([
                'id_poliza_fija' => $id_poliza_fija,
                'id_asiento' => $renglon_id,
                'cargo' => $cargo,
                'abono' => $abono,
            ]);

            $this->data['data'] = [
                'asiento_id' => $renglon_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


}
