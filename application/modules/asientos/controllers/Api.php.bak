<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function agregar_post()
    {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('clave_poliza', 'clave_poliza', 'trim|required|exists[ca_polizas_nomenclatura.id]');
        // $this->form_validation->set_rules('fecha', 'fecha', 'trim|required|valid_date');
        $this->form_validation->set_rules('tipo', 'tipo de movimiento', 'trim|required|in_list[1,2]');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|required|exists[cuentas_dms.id]');

        $this->form_validation->set_rules('concepto', 'concepto', 'trim|required');
        // $this->form_validation->set_rules('referencia', 'referencia', 'trim|required');
        //$this->form_validation->set_rules('nombre', 'nombre', 'trim|required');

        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[POR_APLICAR,APLICADO]');
        $this->form_validation->set_rules('folio', 'folio', 'trim|required');

        $this->form_validation->set_rules('departamento', 'departamento', 'trim|required|exists[departamentos.id]');
        $this->form_validation->set_rules('cliente_id', 'cliente_id', 'trim');
        $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'trim');

        $this->form_validation->set_rules('producto_servicio_id', 'producto_servicio_id', 'trim|exists[ca_producto_servicio.id]');
        $this->form_validation->set_rules('unidad_id', 'unidad_id', 'trim|exists[ca_unidades.id]');
        $this->form_validation->set_rules('cantidad', 'cantidad', 'trim|numeric');
        $this->form_validation->set_rules('precio_unitario', 'precio_unitario', 'trim|numeric');

        $this->form_validation->set_rules('total', 'total', 'trim|required|numeric');

        $this->form_validation->set_rules('procedencia_id', 'procedencia_id', 'trim|in_list[1,2,99]');

        //$this->form_validation->set_rules('transaccion_id', 'transaccion_id', 'trim|exact_length[36]');
        //$this->form_validation->set_rules('origen_transaccion', 'origen transaccion', 'trim|exists[ca_origen_transaccion.id]');


        if ($this->form_validation->run($this) != FALSE) {
            $abono = ((int)$this->input->post('tipo') === 1) ? $this->input->post('total') : 0;
            $cargo = ((int)$this->input->post('tipo') === 2) ? $this->input->post('total') : 0;

            $fecha = date('Y-m-d'); //$this->input->post('fecha');

            // $transaccion_id = $this->input->post('transaccion_id');
            $cuenta_id = $this->input->post('cuenta');
            $estatus_id = $this->input->post('estatus');

            $unidad_id = $this->input->post('unidad_id');
            $precio_unitario = $this->input->post('precio_unitario');
            $cantidad = $this->input->post('cantidad');
            $producto_servicio_id = $this->input->post('producto_servicio_id');
            $procedencia_id = $this->input->post('procedencia_id');

            # PARCHE DE FUNCIONAMIENTO
            if ($cantidad == false && $precio_unitario == false) {
                $cantidad = 1;
                $precio_unitario = $this->input->post('total');
            }
            if ($unidad_id == false) {
                $unidad_id = DEFAULT_UNIDAD_ID;
            }
            if ($producto_servicio_id == false) {
                $producto_servicio_id = DEFAULT_PRODUCTO_SERVICIO_ID;
            }

            #VERIFICA SI EXISTE UN MOVIMIENTO
            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get(array(
                'folio' => $this->input->post('folio'),
                'origen' => $this->CaTransacciones_model::ORIGEN_CAJA
            ));

            if ($transaccion_datos == false) {
                $transaccion_id = $this->CaTransacciones_model->insert(
                    array(
                        'folio' => $this->input->post('folio'),
                        'origen' => $this->CaTransacciones_model::ORIGEN_CAJA,
                        'fecha' => $fecha,
                        'cliente_id' => $this->input->post('cliente_id'),
                        'proveedor_id' => $this->input->post('proveedor_id'),
                        'origen' => $this->CaTransacciones_model::ORIGEN_CAJA
                    )
                );
                 
            } else {
                $transaccion_id = $transaccion_datos['id'];
            }


            #CREACION DE POLIZA
            $this->load->model('CaPolizas_model');
            $folio_poliza = false;
            $polizas_datos = $this->CaPolizas_model->get(array(
                'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                'ejercicio' => utils::get_anio($fecha),
                'mes' => utils::get_mes($fecha),
                'dia' => utils::get_dia($fecha),
            ));
            if ($polizas_datos == false) {
                $clave_poliza = $this->input->post('clave_poliza');
                $dia = utils::get_dia($fecha);
                $poliza_id = $this->CaPolizas_model->insert(array(
                    'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                    'ejercicio' => utils::get_anio($fecha),
                    'mes' => utils::get_mes($fecha),
                    'dia' => $dia,
                    'fecha_creacion' => $fecha
                ));
                 
                $folio_poliza = $clave_poliza . $dia;
            } else {
                $poliza_id = $polizas_datos['id'];
                $folio_poliza = $polizas_datos['PolizaNomenclatura_id'] . $polizas_datos['dia'];
            }

            $this->load->model('CaOrigenTransaccion_model');
            $origen_transaccion_id = (($this->input->post('origen_transaccion') == false) ? $this->CaOrigenTransaccion_model::ORIGEN_OTROS : $this->input->post('origen_transaccion'));

            $this->load->model('Asientos_model');
            $content = array(
                'polizas_id' => $poliza_id,
                'transaccion_id' => $transaccion_id,
                'cuenta' => $cuenta_id,
                //'nombre' => $this->input->post('nombre'),
                'cargo' => $cargo,
                'abono' => $abono,
                // 'referencia' => $this->input->post('referencia'),
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => $fecha, //$this->input->post('fecha'),
                'estatus_id' => $this->input->post('estatus'),
                'origen_transaccion_id' => $origen_transaccion_id,
                // 'cliente_id' => $this->input->post('cliente_id'),
                'departamento_id' => $this->input->post('departamento'),

                'cantidad' => $cantidad,
                'unidad_id' => $unidad_id,
                'producto_servicio_id' => $producto_servicio_id,
                'precio_unitario' => $precio_unitario,
                'procedencia_id' => $procedencia_id
            );
            $asiento_id = $this->Asientos_model->insert($content);
             

            $balanza_id = null;
            if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
                #endregion
                $this->load->model('MaBalanza_model');
                $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);
                 

                if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
                    $this->Asientos_model->update(array(
                        'fecha_asiento_aplicado' => utils::get_datetime()
                    ), array(
                        'id' => $asiento_id
                    ));
                     
                    $balanza_id = $balaza['balanza_id'];
                }
            }

            $this->data['data'] = array(
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id,
                'folio_poliza' => $folio_poliza,
                'asiento_id' => $asiento_id,
                'balanza_id' => $balanza_id
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function modificar_post()
    {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim|required|exists[asientos.id]');
        $this->form_validation->set_rules('cargo', 'cargo', 'trim|numeric');
        $this->form_validation->set_rules('abono', 'abono', 'trim|numeric');
        $this->form_validation->set_rules('concepto', 'concepto', 'trim');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|numeric|exists[cuentas_dms.id]');

        if ($this->form_validation->run($this) != FALSE) {
            $abono = $this->input->post('abono');
            $precio_unitario = 0;
            if ((int)$abono == 0) {
                $cargo = $this->input->post('cargo');
                $precio_unitario = $abono;
            } else {
                $cargo = 0;
                $precio_unitario = $cargo;
            }

            $asiento_id = $this->input->post('asiento_id');
            $cuenta = $this->input->post('cuenta');
            $this->load->model('Asientos_model');
            if ($cuenta) {
                $content = array(
                    'precio_unitario' => $precio_unitario,
                    'cargo' => $cargo,
                    'abono' => $abono,
                    'cuenta' => $this->input->post('cuenta')
                );
            } else {
                $content = array(
                    'precio_unitario' => $precio_unitario,
                    'cargo' => $cargo,
                    'abono' => $abono,
                );
            }
<<<<<<< HEAD

            if($this->input->post('cuenta') != false && strlen(trim($this->input->post('cuenta'))) >0){
                $content['cuenta'] = $this->input->post('cuenta');
            }

            $asiento_id = $this->Asientos_model->update($content,[
=======
            $asiento_id = $this->Asientos_model->update($content, [
>>>>>>> a54fddec3afeaeb6427c806f7adc36feed054302
                'id' => $asiento_id
            ]);
             

            $this->data['data'] = array(
                'asiento_afectados' => $asiento_id,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function aplicar_asiento_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim|required|exists[asientos.id]');
        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[APLICADO,ANULADO]');

        if ($this->form_validation->run($this) != FALSE) {

            $asiento_id = $this->input->post('asiento_id');
            $estatus = $this->input->post('estatus');

            $asiento_response = $this->aplicar_asiento($asiento_id, $estatus);

            $this->data['data'] = $asiento_response;
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function aplicar_folio_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[APLICADO,ANULADO]');

        if ($this->form_validation->run($this) != FALSE) {

            $estatus = $this->input->post('estatus');

            $asientos_folio = 0;
            $asientos_aplicador = 0;

            $this->load->model('CaTransacciones_model');
            $listado_transaccion = $this->CaTransacciones_model->getAll(array(
                'folio' => $this->input->post('folio'),
                'origen' => $this->CaTransacciones_model::ORIGEN_CAJA
            ));
            if (is_array($listado_transaccion)) {
                foreach ($listado_transaccion as $transaccion_datos) {

                    if (is_array($transaccion_datos) && array_key_exists('id', $transaccion_datos)) {
                        $this->load->model('Asientos_model');
                        $this->db->where_in('estatus_id', [
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            $this->Asientos_model::ESTATUS_APLICADO,
                        ]);
                        $asientos_datos = $this->Asientos_model->get_list(array(
                            'transaccion_id' => $transaccion_datos['id']
                        ));

                        if (is_array($asientos_datos) && count($asientos_datos) > 0) {
                            $asientos_folio = count($asientos_datos);

                            foreach ($asientos_datos as $key => $value) {
                                // if($value['estatus_id'] == $this->Asientos_model::ESTATUS_POR_APLICAR){
                                $asiento_response = $this->aplicar_asiento($value['id'], $estatus);

                                if (is_array($asiento_response) && array_key_exists('balanza_id', $asiento_response) && $asiento_response['balanza_id'] != false) {
                                    $asientos_aplicador++;
                                }
                                // }
                            }
                        }
                    }
                }
            }

            $this->data['data'] = array(
                'asientos_folio' => $asientos_folio,
                'asientos_aplicador' => $asientos_aplicador
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }



    private function aplicar_asiento($asiento_id, $estatus_id = false)
    {
        $balanza_id = false;
        $asiento_id_movimiento = $asiento_id;

        $this->load->model('Asientos_model');

        $estatus_id = ($estatus_id == false) ? $this->Asientos_model::ESTATUS_POR_APLICAR :  $estatus_id;


        #VERIFICA SI EXISTE UN MOVIMIENTO POR PROCESAR
        if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
            $this->db->where_in('estatus_id', array($this->Asientos_model::ESTATUS_POR_APLICAR));
        } else {
            $this->db->where_in('estatus_id', array($this->Asientos_model::ESTATUS_POR_APLICAR, $this->Asientos_model::ESTATUS_APLICADO));
        }
        $asientos_datos = $this->Asientos_model->get(array(
            'id' => $asiento_id,
            // 'estatus_id' => $this->Asientos_model::ESTATUS_POR_APLICAR
        ));

        if (is_array($asientos_datos)) {
            #EXiSTE EL REGISTRO
            if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
                $this->load->model('MaBalanza_model');
                $balaza = $this->MaBalanza_model->aplicar_asiento($asientos_datos['cuenta'], $asientos_datos['cargo'], $asientos_datos['abono'], $asientos_datos['id']);
                 
                $contenido_actualizar = [];

                # VALIDACION SI ESTA APLICADO
                if (is_array($balaza) && array_key_exists('balanza_id', $balaza) && strlen(trim($balaza['balanza_id'])) > 0) {

                    $contenido_actualizar = array(
                        'fecha_asiento_aplicado' => utils::get_datetime(),
                        'estatus_id' => $this->Asientos_model::ESTATUS_APLICADO
                    );
                    $this->Asientos_model->update($contenido_actualizar, array(
                        'id' => $asiento_id
                    ));
                     
                    $balanza_id = $balaza['balanza_id'];
                }
            } else if ($estatus_id == $this->Asientos_model::ESTATUS_ANULADO) {

                // ESTATUS_CANCELADO_SIN_APLICAR
                $content_update = [
                    'fecha_asiento_anulado' => utils::get_datetime()
                ];

                if (strlen(trim($asientos_datos['fecha_asiento_aplicado'])) > 0) {
                    // AGREGA UN ASIENTO DE TIPO ANULADO
                    $content = array(
                        'polizas_id' => $asientos_datos['polizas_id'],
                        'transaccion_id' => $asientos_datos['transaccion_id'],
                        'cuenta' => $asientos_datos['cuenta'],
                        'cargo' => $asientos_datos['abono'],
                        'abono' => $asientos_datos['cargo'],
                        'concepto' => 'Anulación del movimiento ' . utils::folio($asiento_id, 6) . ': ' . $asientos_datos['concepto'],
                        'fecha_creacion' => $asientos_datos['fecha_creacion'],
                        'estatus_id' => $this->Asientos_model::ESTATUS_ANULADO,
                        'origen_transaccion_id' =>  $asientos_datos['origen_transaccion_id'],
                        'fecha_asiento_anulado' => utils::get_datetime()
                    );
                    $asiento_id_movimiento = $this->Asientos_model->insert($content);
                     

                    $this->load->model('MaBalanza_model');
                    $balaza = $this->MaBalanza_model->aplicar_asiento($asientos_datos['cuenta'], $asientos_datos['cargo'], $asientos_datos['abono'], $asiento_id_movimiento);
                     

                    $content_update['estatus_id'] = $this->Asientos_model::ESTATUS_CANCELADO;
                } else {
                    $content_update['estatus_id'] = $this->Asientos_model::ESTATUS_CANCELADO_SIN_APLICAR;
                }

                $this->Asientos_model->update(
                    $content_update,
                    array(
                        'id' => $asiento_id
                    )
                );
                 
            }
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = array('El asiento no se puede modificar');
        }

        return array(
            'balanza_id' => $balanza_id,
            'asiento_id' => $asiento_id_movimiento
        );
    }

    public function detalle_get()
    {
        $this->form_validation->set_data([
            'folio' => $this->input->get('folio'),
            'nomenclatura' => $this->input->get('nomenclatura'),
            'anio' => $this->input->get('anio'),
            'mes' => $this->input->get('mes'),
            'dia' => $this->input->get('dia'),
            'asiento_id' => $this->input->get('asiento_id'),
            'estatus' => $this->input->get('estatus'),
            'fecha' => $this->input->get('fecha'),
            'departamento' => $this->input->get('departamento'),
            'transaccion_id' => $this->input->get('transaccion_id'),
        ]);

        $this->form_validation->set_rules('folio', 'folio', 'trim');
        $this->form_validation->set_rules('nomenclatura', 'nomenclatura', 'trim|exists[ca_polizas_nomenclatura.id]');
        $this->form_validation->set_rules('anio', 'anio', 'trim|exact_length[4]');
        $this->form_validation->set_rules('mes', 'mes', 'trim');
        $this->form_validation->set_rules('dia', 'dia', 'trim');
        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim');
        $this->form_validation->set_rules('estatus_id', 'estatus', 'trim|in_list[POR_APLICAR,APLICADO,ANULADO,ACTIVOS]');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|valid_date');
        $this->form_validation->set_rules('departamento', 'departamento', 'trim|exists[departamentos.id]');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|exists[cuentas_dms.cuenta]');
        $this->form_validation->set_rules('transaccion_id', 'transaccion_id', 'trim|exists[ca_transacciones.id]');

        if ($this->form_validation->run($this) != FALSE) {

            $nomenclatura = $this->input->get('nomenclatura');
            $folio = $this->input->get('folio');
            $anio = $this->input->get('anio');
            $mes = $this->input->get('mes');
            $dia = $this->input->get('dia');
            $asiento_id = $this->input->get('asiento_id');
            $estatus_id = $this->input->get('estatus');
            $fecha = $this->input->get('fecha');
            $departamento = $this->input->get('departamento');
            $transaccion_id = $this->input->get('transaccion_id');
            $cuenta = $this->input->get('cuenta');

            if (strlen(trim($fecha)) > 0 && $fecha !== false) {
                $datetime = DateTime::createFromFormat('Y-m-d', $fecha);
                $anio = date_format($datetime, 'Y');
                $mes = date_format($datetime, 'm');
                $dia = date_format($datetime, 'd');
            }



            $contenido_busqueda = [];
            if (strlen($nomenclatura) > 0) {
                $contenido_busqueda['PolizaNomenclatura_id'] = $nomenclatura;
            }
            if (strlen($folio) > 0) {
                $contenido_busqueda['folio'] = $folio;
            }
            if (strlen($anio) > 0) {
                $contenido_busqueda['poliza_anio'] = $anio;
            }
            if (strlen($mes) > 0) {
                $contenido_busqueda['poliza_mes'] = $mes;
            }
            if (strlen($dia) > 0) {
                $contenido_busqueda['poliza_dia'] = $dia;
            }
            if (strlen($asiento_id) > 0) {
                $contenido_busqueda['asiento_id'] = $asiento_id;
            }
            if (strlen($estatus_id) > 0) {
                if ($estatus_id == 'ACTIVOS') {
                    $this->db->where_in('estatus_id', ['POR_APLICAR', 'APLICADO']);
                } else {
                    $contenido_busqueda['estatus_id'] = $estatus_id;
                }
            }
            if (strlen($departamento) > 0) {
                $contenido_busqueda['departamento_id'] = $departamento;
            }
            if (strlen($transaccion_id) > 0) {
                $contenido_busqueda['transaccion_id'] = $transaccion_id;
            }
            if (strlen($cuenta) > 0) {
                $contenido_busqueda['cuenta'] = $cuenta;
            }

            $this->load->model('Asientos_model');
            $listado = $this->Asientos_model->get_detalle($contenido_busqueda, [
                'fecha_registro' => 'asc'
            ]);

            $total = [
                'cargo' => 0,
                'abono' => 0,
                'total' => 0
            ];
            $acumulado = 0;
            if (is_array($listado) && count($listado) > 0) {
                foreach ($listado as $value) {
                    $total['cargo'] += utils::numberPrecision($value['cargo'], 3);
                    $total['abono'] += utils::numberPrecision($value['abono'], 3);

                    /*
                    $monto = ($value['monto']);
                    $acumulado = $acumulado + $monto;

                    $this->Asientos_model->update(array(
                        'acumulado' => $acumulado
                    ),array(
                        'id' => $value['asiento_id']
                    ));
                    */
                }

                $total['total'] += utils::numberPrecision(($total['abono'] - $total['cargo']), 3);
            }
            $this->data['data'] = $listado;
            $this->data['total'] = $total;
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }
}
