<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->library('UTF8');

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false,
        );
    }

    public function crear_poliza_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('clave_poliza', 'Nomenclatura de la poliza', 'trim|required|exists[ca_polizas_nomenclatura.id]');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required|valid_date');
        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|required');

        if ($this->form_validation->run($this) != false) {
            $fecha = $this->input->post('fecha');
            $sucursal_id = $this->input->post('sucursal_id');
            $dia = utils::get_dia($fecha);

            #CREACION DE POLIZA
            $this->load->model('CaPolizas_model');
            $folio_poliza = false;
            $polizas_datos = $this->CaPolizas_model->get(array(
                'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                'ejercicio' => utils::get_anio($fecha),
                'mes' => utils::get_mes($fecha),
                'dia' => utils::get_dia($fecha),
            ));
            if ($polizas_datos == false) {
                $clave_poliza = $this->input->post('clave_poliza');
                $dia = utils::get_dia($fecha);
                $poliza_id = $this->CaPolizas_model->insert(array(
                    'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                    'ejercicio' => utils::get_anio($fecha),
                    'mes' => utils::get_mes($fecha),
                    'dia' => $dia,
                    'fecha_creacion' => $fecha,
                ));

                $folio_poliza = $clave_poliza . $dia;
            } else {
                $poliza_id = $polizas_datos['id'];
                $folio_poliza = $polizas_datos['PolizaNomenclatura_id'] . $polizas_datos['dia'];
            }

            #VERIFICA SI EXISTE UN MOVIMIENTO
            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get(array(
                'folio' => $this->input->post('clave_poliza') . utils::folio($dia, 3),
                'fecha' => $fecha,
                'origen' => 'POLIZAS',
                'sucursal_id' => $sucursal_id,
            ));

            if ($transaccion_datos == false) {
                $transaccion_id = $this->CaTransacciones_model->insert(
                    array(
                        'folio' => $this->input->post('clave_poliza') . utils::folio($dia, 3),
                        'origen' => 'POLIZAS',
                        'fecha' => $fecha,
                        'sucursal_id' => $sucursal_id,
                    )
                );
            } else {
                $transaccion_id = $transaccion_datos['id'];
            }

            $this->load->model('ReTransaccionesPolizas_model');
            $datos_rel = $this->ReTransaccionesPolizas_model->get([
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id,
            ]);
            if ($datos_rel == false) {
                $rel_id = $this->ReTransaccionesPolizas_model->insert(
                    array(
                        'transaccion_id' => $transaccion_id,
                        'poliza_id' => $poliza_id,
                    )
                );
            } else {
                $rel_id = $datos_rel['id'];
            }

            $this->data['data'] = array(
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id,
                'folio_poliza' => $folio_poliza,
                'rel_id' => $rel_id,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    /**
     * @OA\Post(path="/asientos/api/agregar",
     *   tags={"ASIENTOS"},
     *   summary="Agregar asiento",
     *   description="....",
     *   @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               @OA\Property(
     *                   property="clave_poliza",
     *                   description="CLAVE DE LA POLIZA QUE AFECTARA",
     *                   type="string",
     *                   example="QT",
     *               ),
     *               @OA\Property(
     *                   property="numero_poliza",
     *                   description="NUMERO DE LA POLIZA QUE AFECTARA, SOLO SE ADMITEN HASTA 3 DIGITOS",
     *                   type="string",
     *                   example="4",
     *               ),
     *               @OA\Property(
     *                   property="tipo",
     *                   description="<pre class='code'>Tipos de movimientos:<ul><li>1 = ABONO</li><li>2 = CARGO</li></ul></pre>",
     *                   type="string",
     *                   example="1",
     *               ),
     *               @OA\Property(
     *                   property="cuenta",
     *                   description="159    132000    INVENTARIO REFACCIONES S.J.R.,158    131000    INV. REFACCIONES QUERETARO",
     *                   type="string",
     *                   example="159",
     *               ),
     *               @OA\Property(
     *                   property="concepto",
     *                   description="Descripcion del condepto del asiento",
     *                   type="string",
     *                   example="ASIENTO DE PRUEBA",
     *               ),
     *               @OA\Property(
     *                   property="estatus",
     *                   description="<pre class='code'>Tipos de movimientos:<ul><li>POR_APLICAR</li><li>APLICADO</li></ul></pre>",
     *                   type="string",
     *                   example="POR_APLICAR",
     *               ),
     *               @OA\Property(
     *                   property="folio",
     *                   description="<pre class='code'>Identificador unico para usarla para agurar asientos</pre>",
     *                   type="string",
     *                   example="TEMP-001",
     *               ),
     *               @OA\Property(
     *                   property="departamento",
     *                   description="<pre class='code'>Identificador del departamento al que pertenece el asiento</pre>",
     *                   type="string",
     *                   example="19",
     *               ),
     *               @OA\Property(
     *                   property="cantidad",
     *                   description="<pre class='code'>Cantidad Total</pre>",
     *                   type="string",
     *                   example="2",
     *               ),
     *               @OA\Property(
     *                   property="total",
     *                   description="<pre class='code'>Monto Total</pre>",
     *                   type="string",
     *                   example="100",
     *               ),
     *               @OA\Property(
     *                   property="sucursal_id",
     *                   description="<pre class='code'>Identificador de la sucursal</pre>",
     *                   type="string",
     *                   example="1",
     *               ),
     *               @OA\Property(
     *                   property="fecha",
     *                   description="<pre class='code'>Fecha de movimiento</pre>",
     *                   type="string",
     *                   example="2022-10-10",
     *               ),
     *               @OA\Property(
     *                   property="cliente_id",
     *                   description="<pre class='code'>ID CLIENTE</pre>",
     *                   type="string",
     *                   example="A01"
     *               ),
     *               @OA\Property(
     *                   property="cliente_nombre",
     *                   description="<pre class='code'>Nombre</pre>",
     *                   type="string",
     *                   example="CLIENTE"
     *               ),
     *               @OA\Property(
     *                   property="cliente_apellido1",
     *                   description="<pre class='code'>Primer apellido</pre>",
     *                   type="string",
     *                   example="PRIMER APELLIDO"
     *               ),
     *               @OA\Property(
     *                   property="cliente_apellido2",
     *                   description="<pre class='code'>Segundo apellido</pre>",
     *                   type="string",
     *                   example="SEGUNDO APELLIDO"
     *               ),
     *               @OA\Property(
     *                   property="costo_promedio",
     *                   description="<pre class='code'>Costo promedio</pre>",
     *                   type="string",
     *                   example="0"
     *               ),
     *               @OA\Property(
     *                   property="descuento",
     *                   description="<pre class='code'>Descuento</pre>",
     *                   type="string",
     *                   example="0"
     *               )
     *           )
     *       )
     *   ),
     *   @OA\Response(response=200,description="Success")
     * )
     */
    public function agregar_post()
    {
        $data_form = $this->input->post();
        $cuenta_id = $this->input->post('cuenta');

        $this->load->library('form_validation');

        if (strlen(trim($cuenta_id)) == 6) {
            $this->load->model('CaCuentas_model');
            $cuentas_data = $this->CaCuentas_model->get(['cuenta' => $cuenta_id]);
            if (is_array($cuentas_data) && array_key_exists('id', $cuentas_data)) {
                $data_form['cuenta'] = $cuentas_data['id'];
                $cuenta_id = $cuentas_data['id'];
            }
        }

        $this->form_validation->set_data($data_form);

        $this->form_validation->set_rules('clave_poliza', 'clave_poliza', 'trim|required|exists[ca_polizas_nomenclatura.id]');
        $this->form_validation->set_rules('numero_poliza', 'numero_poliza', 'trim|numeric');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|valid_date');
        $this->form_validation->set_rules('tipo', 'tipo de movimiento', 'trim|required|in_list[1,2]');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|required|exists[cuentas_dms.id]');

        $this->form_validation->set_rules('concepto', 'concepto', 'trim|required');
        // $this->form_validation->set_rules('referencia', 'referencia', 'trim|required');
        //$this->form_validation->set_rules('nombre', 'nombre', 'trim|required');

        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[POR_APLICAR,APLICADO]');
        $this->form_validation->set_rules('folio', 'folio', 'trim|required');

        $this->form_validation->set_rules('departamento', 'departamento', 'trim|required|exists[departamentos.id]');
        $this->form_validation->set_rules('proveedor_id', 'proveedor_id', 'trim');

        $this->form_validation->set_rules('producto_servicio_id', 'producto_servicio_id', 'trim|exists[ca_producto_servicio.id]');
        $this->form_validation->set_rules('unidad_id', 'unidad_id', 'trim|exists[ca_unidades.id]');
        $this->form_validation->set_rules('cantidad', 'cantidad', 'trim|numeric');
        $this->form_validation->set_rules('precio_unitario', 'precio_unitario', 'trim|numeric');

        $this->form_validation->set_rules('total', 'total', 'trim|required|numeric');

        $this->form_validation->set_rules('procedencia_id', 'procedencia_id', 'trim|in_list[1,2,99]');
        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|required|exists[ca_sucursales.id]');

        //$this->form_validation->set_rules('transaccion_id', 'transaccion_id', 'trim|exact_length[36]');
        //$this->form_validation->set_rules('origen_transaccion', 'origen transaccion', 'trim|exists[ca_origen_transaccion.id]');

        $this->form_validation->set_rules('cliente_id', 'cliente_id', 'trim');
        $this->form_validation->set_rules('cliente_nombre', 'cliente_nombre', 'trim');
        $this->form_validation->set_rules('cliente_apellido1', 'cliente_apellido1', 'trim');
        $this->form_validation->set_rules('cliente_apellido2', 'cliente_apellido2', 'trim');

        $this->form_validation->set_rules('costo_promedio', 'costo_promedio', 'trim');

        if ($this->form_validation->run($this) != false) {
            # PARCHE DE FUNCIONAMIENTO
            $numero_poliza = (float) $this->input->post('numero_poliza');
            $costo_promedio = (float) $this->input->post('costo_promedio');
            $precio_unitario = (float) $this->input->post('precio_unitario');
            $importe_total = (float) $this->input->post('total');
            $cantidad = (float) $this->input->post('cantidad');

            if (strlen(trim($cantidad)) == 0 || $cantidad == 0) {
                $cantidad = 1;
            }

            if (strlen(trim($precio_unitario)) == 0 || $precio_unitario == 0) {
                try {
                    $precio_unitario = $importe_total / $cantidad;
                } catch (\Throwable $th) {
                    $precio_unitario = 0;
                }
            }

            //parche
            $importe_subtotal = 0;
            $importe_iva = 0;

            try {

                $importe_generado_resp = $this->rest_client->get([
                    'server' => URL_CONTABILIDAD,
                    'method' => 'enlaces/general/calculo_iva',
                    'params' => [
                        'cantidad' => $importe_total,
                        'iva_incluido' => 1,
                    ],
                ]);
                
                if (is_array($importe_generado_resp) && array_key_exists('data', $importe_generado_resp) && is_array($importe_generado_resp['data'])) {
                    $importe_subtotal = $importe_generado_resp['data']['importe_subtotal'];
                    $importe_iva = $importe_generado_resp['data']['importe_iva'];
                }
            } catch (\Throwable $th) {
                $importe_subtotal = 0;
                $importe_iva = 0;
            }

            $abono = ((int) $this->input->post('tipo') === 1) ? $importe_total : 0;
            $cargo = ((int) $this->input->post('tipo') === 2) ? $importe_total : 0;

            $fecha = $this->input->post('fecha');
            if ($fecha == false) {
                $fecha = utils::get_date();
            }

            // $transaccion_id = $this->input->post('transaccion_id');
            // $cuenta_id = $this->input->post('cuenta');
            $estatus_id = $this->input->post('estatus');
            // if ($estatus_id == 'POR_APLICAR') {
            //     $estatus_id = 'APLICADO';
            // }

            $unidad_id = $this->input->post('unidad_id');
            $producto_servicio_id = $this->input->post('producto_servicio_id');
            $procedencia_id = $this->input->post('procedencia_id');
            $sucursal_id = $this->input->post('sucursal_id');

            if ($unidad_id == false) {
                $unidad_id = DEFAULT_UNIDAD_ID;
            }
            if ($producto_servicio_id == false) {
                $producto_servicio_id = DEFAULT_PRODUCTO_SERVICIO_ID;
            }

            #VERIFICA SI EXISTE UN MOVIMIENTO
            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get(array(
                'folio' => $this->input->post('folio'),
                'fecha' => $fecha,
                'origen' => 'CAJA',
                'departamento_id' => $this->input->post('departamento'),
                'sucursal_id' => $sucursal_id,
            ));

            # ADMINISTRACION DE CLIENTES
            $id_cliente = $this->input->post('cliente_id');
            $cliente_nombre = trim($this->input->post('cliente_nombre'));
            $cliente_apellido1 = trim($this->input->post('cliente_apellido1'));
            $cliente_apellido2 = trim($this->input->post('cliente_apellido2'));

            $id_persona = null;
            // if (strlen(trim($cliente_nombre)) > 0 || strlen(trim($cliente_apellido1)) > 0 || strlen(trim($cliente_apellido2)) > 0) {
            //     $this->load->model('MaPersonas_model');
            //     $persona_data = $this->MaPersonas_model->get([
            //         'nombre' => UTF8::uppercase($cliente_nombre),
            //         'apellido1' => UTF8::uppercase($cliente_apellido1),
            //         'apellido2' => UTF8::uppercase($cliente_apellido2),
            //     ]);
            //     if ($persona_data == false) {
            //         $id_persona = $this->MaPersonas_model->insert([
            //             'nombre' => UTF8::uppercase($cliente_nombre),
            //             'apellido1' => UTF8::uppercase($cliente_apellido1),
            //             'apellido2' => UTF8::uppercase($cliente_apellido2),
            //         ]);
            //     } else {
            //         $id_persona = $persona_data['id'];
            //     }
            // }

            if ($transaccion_datos == false) {
                $transaccion_id = $this->CaTransacciones_model->insert(
                    array(
                        'folio' => $this->input->post('folio'),
                        'fecha' => $fecha,
                        'cliente_id' => $id_cliente,
                        'proveedor_id' => $this->input->post('proveedor_id'),
                        'origen' => 'CAJA',
                        'departamento_id' => $this->input->post('departamento'),
                        'sucursal_id' => $sucursal_id,
                    )
                );
            } else {
                $transaccion_id = $transaccion_datos['id'];
            }

            $this->load->model('Asientos_model');

            if(strlen(trim($cliente_nombre)) == 0){
                $asiento_data = $this->Asientos_model->get([
                    'transaccion_id' => $transaccion_id,
                    'length(cliente_nombre) >' => 0
                ]);
                if(is_array($asiento_data) && array_key_exists('cliente_nombre',$asiento_data)){
                    $cliente_nombre = $asiento_data['cliente_nombre'];
                    $cliente_apellido1 = $asiento_data['cliente_apellido1'];
                    $cliente_apellido2 = $asiento_data['cliente_apellido2'];
                }
            }

            if(strlen(trim($numero_poliza))>0){
                $fecha_mod = date("Y-m-".utils::folio($numero_poliza,2), strtotime($fecha));
            }else{
                $fecha_mod = $fecha;
            }

            #CREACION DE POLIZA
            $this->load->model('CaPolizas_model');
            $folio_poliza = false;
            $polizas_datos = $this->CaPolizas_model->get(array(
                'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                'fecha_creacion' => $fecha_mod,
            ));

            if ($polizas_datos == false) {
                $clave_poliza = $this->input->post('clave_poliza');
                $dia = utils::get_dia($fecha_mod);
                $poliza_id = $this->CaPolizas_model->insert(array(
                    'PolizaNomenclatura_id' => $this->input->post('clave_poliza'),
                    'ejercicio' => utils::get_anio($fecha_mod),
                    'mes' => utils::get_mes($fecha_mod),
                    'dia' => $dia,
                    'fecha_creacion' => $fecha_mod,
                ));

                $folio_poliza = $clave_poliza . $dia;
            } else {
                $poliza_id = $polizas_datos['id'];
                $folio_poliza = $polizas_datos['PolizaNomenclatura_id'] . $polizas_datos['dia'];
            }

            $this->load->model('CaOrigenTransaccion_model');
            $origen_transaccion_id = (($this->input->post('origen_transaccion') == false) ? $this->CaOrigenTransaccion_model::ORIGEN_OTROS : $this->input->post('origen_transaccion'));

            $descuento = $this->input->post('descuento');

            $content = array(
                'id_persona' => $id_persona,
                'polizas_id' => $poliza_id,
                'transaccion_id' => $transaccion_id,
                'cuenta' => $cuenta_id,
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => $fecha,
                'estatus_id' => $estatus_id,
                'origen_transaccion_id' => $origen_transaccion_id,
                'departamento_id' => $this->input->post('departamento'),
                'fecha_asiento_calculo_iva' => utils::get_datetime(),
                'unidad_id' => $unidad_id,
                'producto_servicio_id' => $producto_servicio_id,
                'procedencia_id' => $procedencia_id,

                'costo' => ((strlen(trim($costo_promedio)) > 0) ? $costo_promedio : 0),

                'cantidad' => $cantidad,
                'precio_unitario' => $precio_unitario,
                'descuento' => ((strlen(trim($descuento)) > 0) ? $descuento : 0),

                'subtotal' => $importe_subtotal,
                'iva' => $importe_iva,
                'total' => ((strlen(trim($importe_total)) > 0) ? $importe_total : 0),

                'cargo' => $cargo,
                'abono' => $abono,

                'cliente_nombre' => $cliente_nombre,
                'cliente_apellido1' => $cliente_apellido1,
                'cliente_apellido2' => $cliente_apellido2,
            );
            $asiento_id = $this->Asientos_model->insert($content);

            $balanza_id = null;
            if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
                #endregion
                $this->load->model('MaBalanza_model');
                $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);

                if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
                    $this->Asientos_model->update(array(
                        'fecha_asiento_aplicado' => $fecha,
                    ), array(
                        'id' => $asiento_id,
                    ));

                    $balanza_id = $balaza['balanza_id'];
                }
            }

            $this->data['data'] = array(
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id,
                'folio_poliza' => $folio_poliza,
                'asiento_id' => $asiento_id,
                'balanza_id' => $balanza_id,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function modificar_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim|required|exists[asientos.id]');
        $this->form_validation->set_rules('cargo', 'cargo', 'trim|numeric');
        $this->form_validation->set_rules('abono', 'abono', 'trim|numeric');
        $this->form_validation->set_rules('concepto', 'concepto', 'trim');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|numeric|exists[cuentas_dms.id]');

        if ($this->form_validation->run($this) != false) {
            $abono = $this->input->post('abono');
            $precio_unitario = 0;
            if ((int) $abono == 0) {
                $cargo = $this->input->post('cargo');
                $precio_unitario = $abono;
            } else {
                $cargo = 0;
                $precio_unitario = $cargo;
            }

            $asiento_id = $this->input->post('asiento_id');
            $cuenta = $this->input->post('cuenta');
            $this->load->model('Asientos_model');
            if ($cuenta) {
                $content = array(
                    'cuenta' => $this->input->post('cuenta'),
                );
            } else {
                $content = array(
                    'precio_unitario' => $precio_unitario,
                    'cargo' => $cargo,
                    'abono' => $abono,
                );
            }

            if ($this->input->post('cuenta') != false && strlen(trim($this->input->post('cuenta'))) > 0) {
                $content['cuenta'] = $this->input->post('cuenta');
            }

            $asiento_id = $this->Asientos_model->update($content, [
                'id' => $asiento_id,
            ]);

            $this->data['data'] = array(
                'asiento_afectados' => $asiento_id,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function aplicar_asiento_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim|required|exists[asientos.id]');
        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[APLICADO,ANULADO]');

        if ($this->form_validation->run($this) != false) {
            $asiento_id = $this->input->post('asiento_id');
            $estatus = $this->input->post('estatus');

            $asiento_response = $this->aplicar_asiento($asiento_id, $estatus);

            $this->data['data'] = $asiento_response;
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function aplicar_folio_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('folio', 'folio', 'trim|required|exists[ca_transacciones.folio]');
        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[APLICADO,ANULADO]');
        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|required|exists[ca_sucursales.id]');

        if ($this->form_validation->run($this) != false) {
            $estatus = $this->input->post('estatus');

            $asientos_folio = 0;
            $asientos_aplicador = 0;

            $this->load->model('CaTransacciones_model');
            $listado_transaccion = $this->CaTransacciones_model->getAll(array(
                'folio' => $this->input->post('folio'),
                'sucursal_id' => $this->input->post('sucursal_id'),
                'origen' => $this->CaTransacciones_model::ORIGEN_CAJA,
            ));
            if (is_array($listado_transaccion)) {
                foreach ($listado_transaccion as $transaccion_datos) {
                    if (is_array($transaccion_datos) && array_key_exists('id', $transaccion_datos)) {
                        $this->load->model('Asientos_model');
                        $this->db->where_in('estatus_id', [
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            $this->Asientos_model::ESTATUS_APLICADO,
                        ]);
                        $asientos_datos = $this->Asientos_model->get_list(array(
                            'transaccion_id' => $transaccion_datos['id'],
                        ));

                        if (is_array($asientos_datos) && count($asientos_datos) > 0) {
                            $asientos_folio = count($asientos_datos);

                            foreach ($asientos_datos as $key => $value) {
                                // if($value['estatus_id'] == $this->Asientos_model::ESTATUS_POR_APLICAR){
                                $asiento_response = $this->aplicar_asiento($value['id'], $estatus);

                                if (is_array($asiento_response) && array_key_exists('balanza_id', $asiento_response) && $asiento_response['balanza_id'] != false) {
                                    $asientos_aplicador++;
                                }
                                // }
                            }
                        }
                    }
                }
            }

            $this->data['data'] = array(
                'asientos_folio' => $asientos_folio,
                'asientos_aplicador' => $asientos_aplicador,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function aplicar_transaccion_post()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('transaccion_id', 'transaccion_id', 'trim|required|exists[ca_transacciones.id]');
        $this->form_validation->set_rules('estatus', 'estatus', 'trim|required|in_list[APLICADO,ANULADO]');

        if ($this->form_validation->run($this) != false) {
            $estatus = $this->input->post('estatus');

            $asientos_folio = 0;
            $asientos_aplicador = 0;

            $this->load->model('CaTransacciones_model');
            $listado_transaccion = $this->CaTransacciones_model->getAll(array(
                'id' => $this->input->post('transaccion_id'),
                'estatus_id' => 'ACTIVO',
            ));
            if (is_array($listado_transaccion)) {
                foreach ($listado_transaccion as $transaccion_datos) {
                    if (is_array($transaccion_datos) && array_key_exists('id', $transaccion_datos)) {
                        $this->load->model('Asientos_model');
                        $this->db->where_in('estatus_id', [
                            $this->Asientos_model::ESTATUS_POR_APLICAR,
                            $this->Asientos_model::ESTATUS_APLICADO,
                            $this->Asientos_model::ESTATUS_TEMPORAL,
                        ]);
                        $asientos_datos = $this->Asientos_model->get_list(array(
                            'transaccion_id' => $transaccion_datos['id'],
                        ));

                        if (is_array($asientos_datos) && count($asientos_datos) > 0) {
                            $asientos_folio = count($asientos_datos);

                            foreach ($asientos_datos as $key => $value) {
                                // if($value['estatus_id'] == $this->Asientos_model::ESTATUS_POR_APLICAR){
                                $asiento_response = $this->aplicar_asiento($value['id'], $estatus);

                                if (is_array($asiento_response) && array_key_exists('balanza_id', $asiento_response) && $asiento_response['balanza_id'] != false) {
                                    $asientos_aplicador++;
                                }
                                // }
                            }
                        }

                        $this->load->model('CaTransacciones_model');
                        $this->CaTransacciones_model->update(
                            [
                                'estatus_id' => 'CERRADO',
                            ],
                            [
                                'id' => $transaccion_datos['id'],
                            ]
                        );
                    }
                }
            }

            $this->data['data'] = array(
                'asientos_folio' => $asientos_folio,
                'asientos_aplicador' => $asientos_aplicador,
            );
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function baja_get($poliza = 'ZZ')
    {
        $this->load->model('Asientos_model');
        $listado_asientos = $this->Asientos_model->get_detalle([
            'PolizaNomenclatura_id' => $poliza,
            'estatus_id' => 'APLICADO',
        ], [
            'asiento_id' => 'desc',
        ]);
        // utils::pre($listado_asientos);
        if (is_array($listado_asientos)) {
            foreach ($listado_asientos as $key => $value) {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/aplicar_asiento',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,

                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_FOLLOWLOCATION => 1,

                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('estatus' => 'ANULADO', 'asiento_id' => $value['asiento_id']),
                    CURLOPT_HTTPHEADER => array(
                        'Cookie: ci_session=4qs98q6k0h3do233m6s98eq4nldpo382',
                    ),
                ));

                $response = curl_exec($curl);
                if ($response === false) {
                    echo 'Curl error: ' . curl_error($curl);
                }

                curl_close($curl);
                utils::pre($response, false);
            }
        }
        exit('TERMINO');
    }

    private function aplicar_asiento($asiento_id, $estatus_id = false)
    {
        $balanza_id = false;
        $asiento_id_movimiento = $asiento_id;

        $this->load->model('Asientos_model');

        $estatus_id = ($estatus_id == false) ? $this->Asientos_model::ESTATUS_POR_APLICAR : $estatus_id;

        #VERIFICA SI EXISTE UN MOVIMIENTO POR PROCESAR
        if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
            $this->db->where_in('estatus_id', array($this->Asientos_model::ESTATUS_POR_APLICAR, $this->Asientos_model::ESTATUS_TEMPORAL));
        } else {
            $this->db->where_in('estatus_id', array($this->Asientos_model::ESTATUS_POR_APLICAR, $this->Asientos_model::ESTATUS_APLICADO, $this->Asientos_model::ESTATUS_TEMPORAL));
        }
        $asientos_datos = $this->Asientos_model->get(array(
            'id' => $asiento_id,
            // 'estatus_id' => $this->Asientos_model::ESTATUS_POR_APLICAR
        ));

        if (is_array($asientos_datos)) {
            #EXiSTE EL REGISTRO
            if ($estatus_id == $this->Asientos_model::ESTATUS_APLICADO) {
                $this->load->model('MaBalanza_model');
                $balaza = $this->MaBalanza_model->aplicar_asiento($asientos_datos['cuenta'], $asientos_datos['cargo'], $asientos_datos['abono'], $asientos_datos['id']);

                $contenido_actualizar = [];

                # VALIDACION SI ESTA APLICADO
                if (is_array($balaza) && array_key_exists('balanza_id', $balaza) && strlen(trim($balaza['balanza_id'])) > 0) {
                    $contenido_actualizar = array(
                        'fecha_asiento_aplicado' => utils::get_datetime(),
                        'estatus_id' => $this->Asientos_model::ESTATUS_APLICADO,
                    );
                    $this->Asientos_model->update($contenido_actualizar, array(
                        'id' => $asiento_id,
                    ));

                    $balanza_id = $balaza['balanza_id'];
                }
            } elseif ($estatus_id == $this->Asientos_model::ESTATUS_ANULADO) {
                // ESTATUS_CANCELADO_SIN_APLICAR
                $content_update = [
                    'fecha_asiento_anulado' => utils::get_datetime(),
                ];

                if (strlen(trim($asientos_datos['fecha_asiento_aplicado'])) > 0) {
                    // AGREGA UN ASIENTO DE TIPO ANULADO
                    $content = array(
                        'polizas_id' => $asientos_datos['polizas_id'],
                        'transaccion_id' => $asientos_datos['transaccion_id'],
                        'cuenta' => $asientos_datos['cuenta'],
                        'cargo' => $asientos_datos['abono'],
                        'abono' => $asientos_datos['cargo'],
                        'concepto' => 'Anulación del movimiento ' . utils::folio($asiento_id, 6) . ': ' . $asientos_datos['concepto'],
                        'fecha_creacion' => $asientos_datos['fecha_creacion'],
                        'estatus_id' => $this->Asientos_model::ESTATUS_ANULADO,
                        'origen_transaccion_id' => $asientos_datos['origen_transaccion_id'],
                        'fecha_asiento_anulado' => utils::get_datetime(),
                    );
                    $asiento_id_movimiento = $this->Asientos_model->insert($content);

                    $this->load->model('MaBalanza_model');
                    $balaza = $this->MaBalanza_model->aplicar_asiento($asientos_datos['cuenta'], $asientos_datos['cargo'], $asientos_datos['abono'], $asiento_id_movimiento);

                    $content_update['estatus_id'] = $this->Asientos_model::ESTATUS_CANCELADO;
                } else {
                    $content_update['estatus_id'] = $this->Asientos_model::ESTATUS_CANCELADO_SIN_APLICAR;
                }

                $this->Asientos_model->update(
                    $content_update,
                    array(
                        'id' => $asiento_id,
                    )
                );
            }
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = array('El asiento no se puede modificar');
        }

        return array(
            'balanza_id' => $balanza_id,
            'asiento_id' => $asiento_id_movimiento,
        );
    }

    /**
     * @OA\Get(path="/asientos/api/detalle",
     *      tags={"ASIENTOS"},
     *      summary="Listado general de asientos",
     *      description="",
     *      @OA\Parameter( name="nomenclatura", description="Nomenclatura", in = "query", required=false, @OA\Schema(type="string"),example="C0" ),
     *      @OA\Parameter( name="estatus", description="Estatus", in = "query", required=false, @OA\Schema(type="string"),example="POR_APLICAR,APLICADO" ),
     *      @OA\Parameter( name="folio", description="Folio", in = "query", required=false, @OA\Schema(type="string"),example="1" ),
     *      @OA\Parameter( name="transaccion_id", description="ID Transaccion", in = "query", required=false, @OA\Schema(type="string"),example="11F42B74-BC64-4362-959F-A034A85B08DB" ),
     *      @OA\Parameter( name="departamento", description="ID Departamento", in = "query", required=false, @OA\Schema(type="string"),example="6" ),
     *      @OA\Parameter( name="fecha", description="Fecha", in = "query", required=false, @OA\Schema(type="string"),example="2022-10-10" ),
     *      @OA\Response(response=200,description="Success")
     * )
     */
    public function detalle_get()
    {
        $this->form_validation->set_data([
            'folio' => $this->input->get('folio'),
            'nomenclatura' => $this->input->get('nomenclatura'),
            'anio' => $this->input->get('anio'),
            'mes' => $this->input->get('mes'),
            'dia' => $this->input->get('dia'),
            'asiento_id' => $this->input->get('asiento_id'),
            'estatus' => $this->input->get('estatus'),
            'fecha' => $this->input->get('fecha'),
            'departamento' => $this->input->get('departamento'),
            'transaccion_id' => $this->input->get('transaccion_id'),
            'limite' => $this->input->get('limite'),
            'sucursal_id' => $this->input->get('sucursal_id'),
        ]);

        $this->form_validation->set_rules('folio', 'folio', 'trim');
        $this->form_validation->set_rules('nomenclatura', 'nomenclatura', 'trim|exists[ca_polizas_nomenclatura.id]');
        $this->form_validation->set_rules('anio', 'anio', 'trim|exact_length[4]');
        $this->form_validation->set_rules('mes', 'mes', 'trim');
        $this->form_validation->set_rules('dia', 'dia', 'trim');
        $this->form_validation->set_rules('asiento_id', 'asiento_id', 'trim');
        $this->form_validation->set_rules('estatus_id', 'estatus', 'trim|in_list[POR_APLICAR,APLICADO,ANULADO,ACTIVOS]');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|valid_date');
        // $this->form_validation->set_rules('fecha', 'fecha', 'trim|valid_date');
        $this->form_validation->set_rules('departamento', 'departamento', 'trim|exists[departamentos.id]');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|exists[cuentas_dms.cuenta]');
        $this->form_validation->set_rules('transaccion_id', 'transaccion_id', 'trim|exists[ca_transacciones.id]');
        $this->form_validation->set_rules('limite', 'limite', 'trim|numeric');
        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|exists[ca_sucursales.id]');

        if ($this->form_validation->run($this) != false) {
            $nomenclatura = $this->input->get('nomenclatura');
            $folio = $this->input->get('folio');
            $anio = $this->input->get('anio');
            $mes = $this->input->get('mes');
            $dia = $this->input->get('dia');
            $asiento_id = $this->input->get('asiento_id');
            $estatus_id = $this->input->get('estatus');
            $fecha = $this->input->get('fecha');
            $departamento = $this->input->get('departamento');
            $transaccion_id = $this->input->get('transaccion_id');
            $cuenta = $this->input->get('cuenta');
            $sucursal_id = $this->input->get('sucursal_id');

            /*
            if (strlen(trim($fecha)) > 0 && $fecha !== false) {
            $datetime = DateTime::createFromFormat('Y-m-d', $fecha);
            $anio = date_format($datetime, 'Y');
            $mes = date_format($datetime, 'm');
            $dia = date_format($datetime, 'd');
            }
             */

            $contenido_busqueda = [];

            if (strlen(trim($fecha)) > 0 && $fecha !== false) {
                $contenido_busqueda['fecha_creacion'] = $fecha;
            }
            if (strlen($nomenclatura) > 0) {
                $contenido_busqueda['PolizaNomenclatura_id'] = $nomenclatura;
            }
            if (strlen($folio) > 0) {
                $contenido_busqueda['folio'] = $folio;
            }
            if (strlen($anio) > 0) {
                $contenido_busqueda['poliza_anio'] = $anio;
            }
            if (strlen($mes) > 0) {
                $contenido_busqueda['poliza_mes'] = $mes;
            }
            if (strlen($sucursal_id) > 0) {
                $contenido_busqueda['sucursal_id'] = $sucursal_id;
            }
            if (strlen($dia) > 0) {
                $contenido_busqueda['poliza_dia'] = $dia;
            }

            if (strlen($asiento_id) > 0) {
                if (is_array(@json_decode($asiento_id)) == true) {
                    $this->db->where_in('asiento_id', @json_decode($asiento_id));
                } else {
                    $contenido_busqueda['asiento_id'] = $asiento_id;
                }
            }

            if (strlen($estatus_id) > 0) {
                if ($estatus_id == 'TODOS') {
                    $this->db->where_in('estatus_id', ['POR_APLICAR', 'APLICADO', 'ANULADO', 'CANCELADO', 'TEMPORAL','CANCELADO_SIN_APLICAR']);
                } elseif ($estatus_id == 'ACTIVOS') {
                    $this->db->where_in('estatus_id', ['POR_APLICAR', 'APLICADO']);
                } elseif ($estatus_id == 'POLIZAS_FIJAS') {
                    $this->db->where_in('estatus_id', ['POR_APLICAR', 'APLICADO', 'TEMPORAL']);
                } else {
                    $contenido_busqueda['estatus_id'] = $estatus_id;
                }
            } else {
                $this->db->where_in('estatus_id', ['POR_APLICAR', 'APLICADO']);
            }
            if (strlen($departamento) > 0) {
                $contenido_busqueda['departamento_id'] = $departamento;
            }
            if (strlen($transaccion_id) > 0) {
                $contenido_busqueda['transaccion_id'] = $transaccion_id;
            }
            if (strlen($cuenta) > 0) {
                $contenido_busqueda['cuenta'] = $cuenta;
            }

            $this->load->model('Asientos_model');

            $limite = $this->input->get('limite');
            if ($limite != false) {
                $this->db->limit($limite);
            }

            $listado = $this->Asientos_model->get_detalle($contenido_busqueda, [
                'fecha_registro' => 'desc',
            ]);
            //  utils::pre($this->db->last_query());

            $total = [
                'cargo' => 0,
                'abono' => 0,
                'total' => 0,
            ];
            $acumulado = 0;
            if (is_array($listado) && count($listado) > 0) {
                foreach ($listado as $value) {
                    $total['cargo'] += utils::numberPrecision($value['cargo'], 3);
                    $total['abono'] += utils::numberPrecision($value['abono'], 3);

                    /*
                $monto = ($value['monto']);
                $acumulado = $acumulado + $monto;

                $this->Asientos_model->update(array(
                'acumulado' => $acumulado
                ),array(
                'id' => $value['asiento_id']
                ));
                 */
                }

                $total['total'] += utils::numberPrecision(($total['abono'] - $total['cargo']), 3);
            }
            $this->data['data'] = $listado;
            $this->data['total'] = $total;
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }

    public function listado_asientos_get()
    {
        $this->load->model('Asientos_model');
        $folio = $this->input->get('folio');
        $this->data['data'] = $this->Asientos_model->getListAsientos($folio);
        //utils::pre($this->data);
        $this->response($this->data);
    }

    public function open_modal_asientos_get()
    {
        $this->load->model('Asientos_model');
        $id = $this->input->get('id');
        $where = ['id' => $id];

        $this->data['data'] = $this->Asientos_model->get($where);
        //utils::pre($this->data['data']);
        $html = $this->load->view('asientos/asientos/modales/asientos_modal.blade.php', $this->data, true);
            $view = array(
                'contenido' => $html,
            );
        //utils::pre($this->data);
        $this->response($view);
    }

    public function save_asientos_post()
    {
        $this->load->model('Asientos_model');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('cantidad', 'Cantidad', 'trim|required');
        $this->form_validation->set_rules('precio_unitario', 'Precio unitario', 'trim|required');
        $this->form_validation->set_rules('costo', 'Costo', 'trim|required|callback_validar_costo');
        $this->form_validation->set_rules('tipo_asiento', 'Tipo asiento', 'trim|required');
		

        if ($this->form_validation->run() == true) {
			$id = $this->input->post('id');
            $id = intval($id);
            $tipo = intval($this->input->post('tipo_asiento'));
            $precio_unitario = $this->input->post('precio_unitario');
            $cantidad = $this->input->post('cantidad');
            $total = $cantidad * $precio_unitario;
            $subtotal = $total  / 1.16;
            $iva = $total - $subtotal;
            
            $where = ['id' => $id];
			$data = [
                'cantidad' => $cantidad,
                'precio_unitario' => $precio_unitario,
                'costo' => $this->input->post('costo'),
                'abono' => $tipo == 2 ? $precio_unitario : 0,
                'cargo' => $tipo == 1 ? $precio_unitario : 0,
                'subtotal' => $subtotal,
                'iva' => $iva,
                'total' => $total,
            ];
			//utils::pre($data);
			$update = $this->Asientos_model->update($data,$where);

			if ($update) {
                $this->data['data'] = true;
				$this->data['mensaje'] = 'Registro actualizado correctamente';
			} else {
				$this->data['estatus'] = 'error';
				$this->data['mensaje'] = 'Ocurrió un error al actualizar el registro en la bd';
			}

		}
		else{
			$this->data['estatus'] = 'error';
        	$this->data['info'] = $this->form_validation->error_array();
		}
		return $this->response($this->data);
    }

    public function validar_costo($costo)
    {
        $precio_unitario = $this->input->post('precio_unitario');

        if ($costo > $precio_unitario) {
            $this->form_validation->set_message('validar_costo', 'El costo no puede ser mayor al precio unitario');
            return false;
        }

        return true;
    }
}
