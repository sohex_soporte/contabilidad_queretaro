@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
   Listado asientos
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="folio">Folio</label>
                            <input class="form-control" type="text" id="folio" name="folio">
                        </div>
                    </div>
                    <div class="col-sm-2"><br>
                        <button class="btn btn-primary mt-1" onclick="APP.buscar_tabla()">Buscar</button>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-sm-12">
                        <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12">
<div id="modalGeneral" class="modal fade " tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>
</div>



@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('style')
<?php $this->carabiner->display('sweetalert2','css') ?>
@endsection

@section('script')
<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" ></script>
<script src="<?php echo base_url('assets/js/scripts/asientos/asientos/indexasientos.js'); ?>"></script>
@endsection