
    <div class="modal-dialog modal-dialog-centered modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modificar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_modal_asientos">
                    <?php
                        $tipo = 0;
                        if($data['cargo'] > 0)
                            $tipo = 1;
                        else
                            $tipo = 2;
                    ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><b>Concepto </b></label>
                                <input type="text" id="concepto" name="concepto" value="<?php echo isset($data['concepto']) ? $data['concepto'] : ''; ?>" class="form-control" disabled/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Costo promedio </b></label>
                            <input type="number" id="costo" name="costo" min="0" value="<?php echo isset($data['costo']) ? (int)$data['costo'] : ''; ?>" class="form-control" />
                            <small id="msg_costo" class="form-text text-danger"></small>
                         </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Cantidad </b></label>
                                <input type="number" id="cantidad" name="cantidad" min="0" oninput="APP.calcularTotales()" value="<?php echo isset($data['cantidad']) ? (int)$data['cantidad'] : ''; ?>" class="form-control" />
                                <small id="msg_cantidad" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Precio unitario </b></label>
                                <input type="number" id="precio_unitario" name="precio_unitario" oninput="APP.calcularTotales()" min="0" class="form-control" value="<?php echo isset($data['precio_unitario']) ? $data['precio_unitario'] : ''; ?>" />
                                <small id="msg_precio_unitario" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Tipo Asiento</b></label>
                                <select name="tipo_asiento" id="tipo_asiento"  class="custom-select mb-3" value="<?php echo $tipo ?>">
                                    <option value="">Seleccione una opción</option>    
                                    <option value="1" <?php echo $tipo == 1 ?  'selected' : '';  ?>>Cargo</option>
                                    <option value="2" <?php echo $tipo == 2 ?  'selected' : '';  ?>>Abono</option>
                                </select>
                                <small id="msg_tipo_asiento" class="form-text text-danger"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Subtotal </b></label>
                                <input type="text" id="subtotal" name="subtotal" class="form-control" value="<?php echo isset($data['subtotal']) ? $data['subtotal'] : ''; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>IVA </b></label>
                                <input type="text" id="iva" name="iva" class="form-control" value="<?php echo isset($data['iva']) ? $data['iva'] : ''; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Total </b></label>
                                <input type="text" id="total" name="total" class="form-control" value="<?php echo isset($data['total']) ? $data['total'] : ''; ?>" readonly/>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id" value="<?php echo isset($data['id']) ? $data['id'] : ''; ?>" />

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" onclick="APP.save()" class="btn btn-primary"> Guardar</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
