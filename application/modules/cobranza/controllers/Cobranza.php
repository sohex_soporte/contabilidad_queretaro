<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cobranza extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));
        date_default_timezone_set('America/Mexico_City');
    }

    public function index(){ 

        $this->blade->render('index',false);
    }
    
}
