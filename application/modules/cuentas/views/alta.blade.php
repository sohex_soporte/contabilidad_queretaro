@layout('template_blade/estructura')



@section('contenido')

<script>
$(document).ready(function(){
    $('#guardar_formulario').submit(function(event){
        event.preventDefault();
        var url ="<?php echo base_url()?>index.php/cuentas/guarda";
        ajaxJson(url,{"fecha":$("#fecha").val(),
                        "poliza":$("#poliza").val(),
                        "tipo":$("#tipo").val(),
                        "concepto":$("#concepto").val()},
                    "POST","",function(result){
            correoValido = false;
            console.log(result);
            json_response = JSON.parse(result);
            obj_output = json_response.output;
            obj_status = obj_output.status;
            if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
                aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            }
            if(obj_status == true){
            exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/polizas/index");
            }


        });
    });

});
</script>

<h1 class="h3 mb-4 text-gray-800">Alta cuenta</h1>



    <div class="row">

        <div class="col-4">

            <form id="guardar_formulario">
        
                <div class="form-group">
                    <label for="">Cuenta</label>
                    <input type="text" class="form-control" id="fecha" placeholder="Nombre" name="fecha">
                </div>
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" id="poliza" placeholder="Cuenta" name="poliza">
                </div>
                <div class="form-group">
                    <label for="">Descripción</label>
                    <input type="text" class="form-control" id="concepto" placeholder="Desciprion" name="concepto">
                </div>
                
                
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
       
        </div>
       
    </div>


    



	
@endsection
