<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cuentas extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
        $datos['index'] = '';
        $datos['cuentas'] = $this->Mgeneral->get_table('cuentas_dms');
        
        $this->blade->render('lista',$datos);
    }

    public function alta_cuenta(){
        $datos['index'] = '';
        //$datos['cuentas'] = $this->Mgeneral->get_table('cuentas_dms');
        
        $this->blade->render('alta',$datos);
    }

    

    
  
}