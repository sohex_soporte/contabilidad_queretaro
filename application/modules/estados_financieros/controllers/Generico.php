<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generico extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() { 
        $reporte_id = $this->input->get('reporte_id');
        if (!$reporte_id) {
            redirect('inicio');
        }
        $this->load->model('CaReportes_model');

        $data = array(
            'id' =>  base64_decode($reporte_id),
            'reporte' => $this->CaReportes_model->get(['id' => base64_decode($reporte_id)])
        );
        
        $this->blade->render('/generico/index',$data);
    }

}
