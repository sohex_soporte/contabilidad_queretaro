<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apis_construir_contenido extends MY_Controller
{

    public $reporte_id;
    public $anio;

    public function __construct()
    {
        parent::__construct();
    }


    public function get_contenido_reporte(){
        $this->reporte_id = $this->input->get_post('reporte_id');
        $this->anio = $this->input->get_post('anio');
       
        
        // $this->load->library('form_validation');
        // $this->form_validation->set_rules('anio', 'Año', 'trim|required');
        
        // if ($this->form_validation->run($this) != FALSE)
        // {
        $this->load->model('MaBalanza_model');
        $this->load->model('DeReporteReglones_model');
        $this->load->model('DeReglonesFormulas_model');
        $this->load->model('DeReporteContenido_model');

        //$anio = $this->input->post('anio');

        $this->load->model('DeReporteContenido_model');
        $listado = $this->DeReporteContenido_model->get_contenido([
            'de_reporte_contenido.reporte_id' => $this->reporte_id,
            'de_reporte_contenido.anio' => $this->anio
        ]);

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-striped" >'
        );

        $this->table->set_template($template);

        $this->table->set_heading(array(
            '','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE','TOTAL'
        ));

        if(is_array($listado) && count($listado)>0){
            foreach ($listado as $value_listado) {

                $clase = '';
                if($value_listado['tipo'] == 3){
                    $clase = 'table-primary ';
                }else if($value_listado['tipo'] == 2){
                    $clase = '';
                }

                if($value_listado['tipo'] == 3){
                    $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase,'colspan'=>14);
                    $this->table->add_row([
                        $cell_0
                    ]);
                }else{
                    $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase);
                    $cell_1 = array('data' => utils::format($value_listado['enero'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_2 = array('data' => utils::format($value_listado['febrero'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_3 = array('data' => utils::format($value_listado['marzo'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_4 = array('data' => utils::format($value_listado['abril'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_5 = array('data' => utils::format($value_listado['mayo'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_6 = array('data' => utils::format($value_listado['junio'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_7 = array('data' => utils::format($value_listado['julio'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_8 = array('data' => utils::format($value_listado['agosto'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_9 = array('data' => utils::format($value_listado['septiembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_10 = array('data' => utils::format($value_listado['octubre'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_11 = array('data' => utils::format($value_listado['noviembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_12 = array('data' => utils::format($value_listado['diciembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                    $cell_13 = array('data' => utils::format($value_listado['total'],$value_listado['formato']), 'class' => $clase.' text-right');

                    $this->table->add_row([
                        $cell_0,
                        $cell_1,
                        $cell_2,
                        $cell_3,
                        $cell_4,
                        $cell_5,
                        $cell_6,
                        $cell_7,
                        $cell_8,
                        $cell_9,
                        $cell_10,
                        $cell_11,
                        $cell_12,
                        $cell_13
                    ]);
                }
            }
        }

        $this->contenido->data = array(
            'tabla' => base64_encode(utf8_decode($this->table->generate()))
        ); 

        // }
        // else
        // {
        //     $this->contenido->error = true;
        //     $this->contenido->mensaje = $this->form_validation->error_array();
        // }

        $this->response();
    }
}
