<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apis_nuevos_general extends MY_Controller
{

    public $reporte_id;

    public function __construct()
    {
        parent::__construct();
        $this->reporte_id = 1;

    }

    public function obtener_valor($mes,$anio,$cuenta_id,$listado_balanza){
        $monto = 0;
        if(is_array($listado_balanza) && count($listado_balanza)>0){
            foreach ($listado_balanza as $value_balanza) {
                
                if($value_balanza['cuenta_id'] == $cuenta_id && $value_balanza['mes'] == utils::folio($mes,2) && $value_balanza['anio'] == $anio ){
                    $monto = (float)$value_balanza['saldo_mes'];
                    
                }
            }
        }
        return $monto;
    }

    public function generacion_reporte(){
        $anio = date('Y');
        $this->construir_reporte($anio);
    }

    public function construir_reporte($anio){
        
        $listado_balanza = $this->MaBalanza_model->get_list([
            'ma_balanza.anio' => $anio
        ]);

        $reglones = $this->DeReporteReglones_model->get_list([
            'reporte_id' => $this->reporte_id
        ]);

        $this->DeReporteContenido_model->delete([
            'reporte_id' => $this->reporte_id
        ]);
        
        if(is_array($reglones) && count($reglones)>0){
            foreach ($reglones as $key_reglon => $value_reglon) {
                
                $listado_formulas = $this->DeReglonesFormulas_model->get_list([
                    'reporte_reglon_id' => $value_reglon['id']
                ]);

                $monto_total = 0;

                for ($mes=1; $mes <= 12 ; $mes++) { 
                    
                    $cuenta_monto = 0;
                    
                    if(is_array($listado_formulas) && count($listado_formulas)>0){
                        
                        foreach ($listado_formulas as $key_formula => $value_formula) {

                            $valor_celda = $this->obtener_valor($mes,$anio,$value_formula['cuenta_id'],$listado_balanza);
                            
                            try {
                                switch ($value_formula['tipo_operacion']) {
                                    case '+':
                                        $cuenta_monto = $cuenta_monto + $valor_celda;
                                        break;
                                    case '-':
                                        $cuenta_monto = $cuenta_monto - $valor_celda;
                                        break;
                                    case '/':
                                        $cuenta_monto = $cuenta_monto / $valor_celda;
                                        break;
                                    case '*':
                                        $cuenta_monto = $cuenta_monto * $valor_celda;
                                        break;
                                    default: 
                                        $cuenta_monto = $cuenta_monto + $valor_celda;
                                        break;
                                }            
                            }
                            catch(ErrorException $e){
                                $cuenta_monto = 0;
                            }
                        }
                    }
                    $monto_total = $monto_total + $cuenta_monto;
                    $columnas[$mes] = $cuenta_monto;
                }
                $columnas[] = $monto_total;

                $this->DeReporteContenido_model->insert([
                    'reporte_id' => $this->reporte_id,
                    'anio' => $anio,
                    'reporte_reglon_id' => $value_reglon['id'],
                    'enero' => array_key_exists(1,$columnas)? $columnas[1] : null,
                    'febrero' => array_key_exists(2,$columnas)? $columnas[2] : null,
                    'marzo' => array_key_exists(3,$columnas)? $columnas[3] : null,
                    'abril' => array_key_exists(4,$columnas)? $columnas[4] : null,
                    'mayo' => array_key_exists(5,$columnas)? $columnas[5] : null,
                    'junio' => array_key_exists(6,$columnas)? $columnas[6] : null,
                    'julio' => array_key_exists(7,$columnas)? $columnas[7] : null,
                    'agosto' => array_key_exists(8,$columnas)? $columnas[8] : null,
                    'septiembre' => array_key_exists(9,$columnas)? $columnas[9] : null,
                    'octubre' => array_key_exists(10,$columnas)? $columnas[10] : null,
                    'noviembre' => array_key_exists(11,$columnas)? $columnas[11] : null,
                    'diciembre' => array_key_exists(12,$columnas)? $columnas[12] : null,
                    'total' => $monto_total
                ]);
                
            }

            $this->calcular_utilidad_bruta($anio);
            $this->calcular_utilidad_bruta_real($anio);
            $this->calcular_porcentaje_utilidad_bruta($anio);
            $this->total_gastos_ventas($anio);
            $this->total_gastos_ventas_porcentaje($anio);
            $this->calcular_utilidad_departamental($anio);
            $this->calcular_utilidad_departamental_porcentaje($anio);

        }
    }

    public function get_contenido_reporte(){

        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('anio', 'Año', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {
            $this->load->model('MaBalanza_model');
            $this->load->model('DeReporteReglones_model');
            $this->load->model('DeReglonesFormulas_model');
            $this->load->model('DeReporteContenido_model');

            $anio = $this->input->post('anio');

            $this->construir_reporte($anio);

            $this->load->model('DeReporteContenido_model');
            $listado = $this->DeReporteContenido_model->get_contenido([
                'de_reporte_contenido.reporte_id' => $this->reporte_id,
                'de_reporte_contenido.anio' => $anio
            ]);

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                '','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE','TOTAL'
            ));

            if(is_array($listado) && count($listado)>0){
                foreach ($listado as $value_listado) {

                    $clase = '';
                    if($value_listado['tipo'] == 3){
                        $clase = 'table-primary ';
                    }else if($value_listado['tipo'] == 2){
                        $clase = '';
                    }

                    if($value_listado['tipo'] == 3){
                        $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase,'colspan'=>14);
                        $this->table->add_row([
                            $cell_0
                        ]);
                    }else{
                        $cell_0 = array('data' => $value_listado['nombre'], 'class' => $clase);
                        $cell_1 = array('data' => utils::format($value_listado['enero'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_2 = array('data' => utils::format($value_listado['febrero'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_3 = array('data' => utils::format($value_listado['marzo'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_4 = array('data' => utils::format($value_listado['abril'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_5 = array('data' => utils::format($value_listado['mayo'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_6 = array('data' => utils::format($value_listado['junio'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_7 = array('data' => utils::format($value_listado['julio'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_8 = array('data' => utils::format($value_listado['agosto'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_9 = array('data' => utils::format($value_listado['septiembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_10 = array('data' => utils::format($value_listado['octubre'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_11 = array('data' => utils::format($value_listado['noviembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_12 = array('data' => utils::format($value_listado['diciembre'],$value_listado['formato']), 'class' => $clase.' text-right');
                        $cell_13 = array('data' => utils::format($value_listado['total'],$value_listado['formato']), 'class' => $clase.' text-right');

                        $this->table->add_row([
                            $cell_0,
                            $cell_1,
                            $cell_2,
                            $cell_3,
                            $cell_4,
                            $cell_5,
                            $cell_6,
                            $cell_7,
                            $cell_8,
                            $cell_9,
                            $cell_10,
                            $cell_11,
                            $cell_12,
                            $cell_13
                        ]);
                    }
                }
            }

            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 

        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }

    private function calcular_utilidad_bruta($anio){
        
        $this->load->model('DeReporteContenido_model');

        #TOTAL VEHICULOS
        $total_vehiculos = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);

        #COSTO DE VENTAS
        $costos_ventas = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 5
        ]);
        if(is_array($costos_ventas) && is_array($total_vehiculos)){

            $content = [];
            foreach ($costos_ventas as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total_vehiculos[$key] - $costos_ventas[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 3,
            ]);
        }
    }

    private function calcular_utilidad_bruta_real($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 3
        ]);

        if(is_array($total)){

            $content = [];
            foreach ($total as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 4,
            ]);
        }
    }


    private function calcular_porcentaje_utilidad_bruta($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $utilidad_real = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);

         #TOTAL VENTAS VEHICULOS
         $total_ventas = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);

        if(is_array($utilidad_real) && is_array($total_ventas)){

            $content = $this->calcular_procentaje($utilidad_real,$total_ventas);

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 31,
            ]);
        }
    }   

    public function total_gastos_ventas($anio){

        #TOTAL GASTOS DE VENTA
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $this->db->where("reporte_reglon_id BETWEEN 7 AND 26");
        $total = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio
        ]);

        if(is_array($total)){

            $content = [];
            foreach ($total as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total[$key];
                }
            }

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 27,
            ]);
        }
    }

    public function total_gastos_ventas_porcentaje($anio){

        $this->load->model('DeReporteContenido_model');

        #TOTAL GASTOS DE VENTA
        $total_gastos = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 27
        ]);

        #TOTAL VENTAS VEHICULOS
        $total_ventas = $this->DeReporteContenido_model->get_contenido_suma([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 1
        ]);



        if(is_array($total_ventas) && is_array($total_gastos)){

            $content = $this->calcular_procentaje($total_gastos,$total_ventas);

            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 28,
            ]);
        }
    }

    private function calcular_utilidad_departamental($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total_superior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);
        
        #TOTAL GASTOS DE VENTA
        $total_inferior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 27
        ]);
        if(is_array($total_inferior) && is_array($total_superior)){

            $content = [];
            foreach ($total_inferior as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    $content[$key] = $total_superior[$key] - $total_inferior[$key];
                }
            }

            # UTILIDAD DEPARTAMENTAL
            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 29,
            ]);
        }
    }

    private function calcular_utilidad_departamental_porcentaje($anio){
        
        $this->load->model('DeReporteContenido_model');

        #UTILIDAD BRUTA REAL
        $total_superior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 4
        ]);
        
        #TOTAL GASTOS DE VENTA
        $total_inferior = $this->DeReporteContenido_model->get([
            'reporte_id' => $this->reporte_id,
            'anio' => $anio,
            'reporte_reglon_id' => 29
        ]);
        if(is_array($total_inferior) && is_array($total_superior)){

            $content = $this->calcular_procentaje($total_inferior,$total_superior);

            # UTILIDAD DEPARTAMENTAL
            $this->DeReporteContenido_model->update($content,[
                'reporte_id' => $this->reporte_id,
                'anio' => $anio,
                'reporte_reglon_id' => 30,
            ]);
        }
    }

    private function calcular_procentaje($total_inferior,$total_superior){
        $content = [];
        if(is_array($total_inferior) && is_array($total_superior)){

            foreach ($total_inferior as $key => $value) {
                if(in_array($key,array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre','total'))){
                    try {
                        $content[$key] = 0;

                        $negativo = false;
                        if($total_superior[$key] < 0 && $total_inferior[$key] < 0){
                            $negativo = true;
                        }

                        $val_1 = $total_superior[$key] * 100;
                        if($val_1 <> 0 && $total_inferior[$key] <> 0){
                            $val_1 = $val_1 / $total_inferior[$key];
                            if($val_1 <> 0){
                                $content[$key] = ($negativo == true && ($val_1/100) > 0 )? $val_1/100*-1 : $val_1/100;
                            }
                        }
                        
                    }catch(ErrorException $e){
                        $content[$key] = 0;
                    }
                }
            }
        }
        return $content;
    }

}
