<?php defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH .'adapter'.DIRECTORY_SEPARATOR.'RestServer.php';

class General extends RestServer
{
    public $type = 'API';
 
    public function __construct()
    {
        parent::__construct();
        $this->load->library('UTF8');
    }

    /**
    * @OA\Get(path="/enlaces/general/calculo_iva",
    *      tags={"ASIENTOS"},
    *      summary="Listado general de asientos",
    *      description="",
    *      @OA\Parameter( name="cantidad", description="Cantidad", in = "query", required=true, @OA\Schema(type="string"),example="100" ),
    *      @OA\Parameter( name="iva_incluido", description="IVA incluido 2=No incluido, 1=Incluido", in = "query", required=false, @OA\Schema(type="string"),example="1" ),
    *      @OA\Response(response=201,description="Success"),
    *      @OA\Response(response=401,description="No Autorizado"),
    *      @OA\Response(response=406,description="Calculo fuera de rango")
    * )
    */
    public function calculo_iva_get(){

        $this->status = 'ok';
        $calculo_data = array();

        $cantidad = $this->input->get('cantidad');
        $iva_incluido = $this->input->get('iva_incluido');
        if($iva_incluido == null){
            $iva_incluido = 1;
        }
        $iva = utils::redondeo($cantidad * 0.16,2);

        if($iva_incluido === 2 || $iva_incluido === '2'){
            $this->status = 'ok';
            $calculo_data = array(
                'importe_subtotal' => (float)$cantidad,
                'importe_iva' => $iva,
                'importe_total' => utils::redondeo($cantidad + $iva,2),
            );
        }else{

            $this->load->model('MaCalculoIva_model');
            
            for($i  = 0;$i < 4;$i++){
                $calculo_data =$this->MaCalculoIva_model->calculo_iva($cantidad);
                if(array_key_exists('importe_subtotal',$calculo_data) && $calculo_data['importe_subtotal'] > 0){
                    $i=5;
                    $this->status = 'ok';
                }else{
                    $this->status = 'error';
                }
            }
        }

        $this->data = $calculo_data;
        $this->send();

    }

}