<?php

class Procesar_factura_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false){
        $this->db->select('name,field,limit')
            ->where($where);
        return $this->db->get('procesar_factura')->result_array();
    }

}