<?php

class Documentos_facturas_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->select('*')
            ->where($where);
        return $this->db->get()->result_array();
    }

    public function getList($where = false)
    {
        $this->db->select('doc.id, doc.created_at, doc.nombre_archivo, est.descripcion as estatus,  tipo.descripcion as tipo_archivo, doc.estatus_documento_factura_id, proc.sat_uuid, proc.pdf,proc.xml, proc.respuesta_api')
            ->from('documentos_facturas as doc')
            ->join('estatus_documento_factura as est', 'doc.estatus_documento_factura_id = est.id')
            ->join('documentos_facturas_procesadas as proc', 'proc.factura =doc.factura','left')
            ->join('tipo_documento_factura as tipo', 'tipo.id = proc.tipo_documento_id','left')
            ->where($where);
            //->where('doc.estatus_documento_factura_id !=', 9);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get($where = false)
    {
        $this->db->select('*')
            ->where($where);
        return $this->db->get('documentos_facturas')->row_array();
    }

    public function insert($content)
    {
        $this->db->set('created_at', date('Y-m-d H:i:s'));
        $this->db->insert('documentos_facturas', $content);
        return $this->db->insert_id();
    }

    public function update($contents, $where)
    {
        $this->db->where($where);
        return $this->db->update('documentos_facturas', $contents);
    }
}
