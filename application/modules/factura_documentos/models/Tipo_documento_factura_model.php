<?php

class Tipo_documento_factura_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->select('*')
            ->where($where);
        return $this->db->get('tipo_documento_factura')->result_array();
    }

    public function get($where = false)
    {
        $this->db->select('*')
            ->where($where);
        return $this->db->get('tipo_documento_factura')->row_array();
    }
}
