<?php

class Documentos_facturas_procesadas_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->select('*')
            ->where($where);
        return $this->db->get('documentos_facturas_procesadas')->result_array();
    }

    public function insert($content)
    {
        $this->db->set('created_at', date('Y-m-d H:i:s'));
        $this->db->insert('documentos_facturas_procesadas', $content);
        return $this->db->insert_id();
    }

    public function update($contents, $where)
    {
        $this->db->where($where);
        return $this->db->update('documentos_facturas_procesadas', $contents);
    }
}
