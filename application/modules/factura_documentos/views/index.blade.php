@layout('layout_login')
@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Documentos facturas
</h4>

<script>

</script>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class=" col-sm-12">

                        <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_doc" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for=""><b>Sat_UUID:</b></label>
                    <p id="sat_uuid"></p><br>
                    <div id="div-botones">
                        <center>
                            <a class="btn btn-danger" id="factura" target="_blank" href="">Ver PDF</a>
                            <a class="btn btn-primary ml-3" id="xml" target="_blank" href="">Ver XML</a>
                        </center>
                    </div>
                </div>
                <div class="row"><br><br>
                    <div class="col-12">
                        <label for=""><b>Respuesta:</b></label>
                        <p class="" id="respuesta" style="word-wrap: break-word;"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="modal_ver_documento" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row"><br><br>
                    <div class="col-12">
                        <p class="" id="documento" style="word-wrap: break-word;"></p>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>



    @endsection

    @section('style')
    <?php $this->carabiner->display('datatables', 'css') ?>
    <style>
      
        .table thead th {
            padding: .45rem !important;
            vertical-align: top;
            border-top: 1px solid #e3e6f0;
            font-size: 12px !important;
            color: #000;

        }

        .table thead th {
            text-align: left;
        }

        td {
            text-align: left;
        }
    </style>
    @endsection

    @section('script')
    <?php $this->carabiner->display('datatables', 'js') ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo base_url('assets/js/scripts/factura_documentos/index.js'); ?>"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @endsection