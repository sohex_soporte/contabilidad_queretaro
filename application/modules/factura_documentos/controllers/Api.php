<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller
{
    protected $hostname = '';
    protected $username = '';
    protected $password = '';

    public $results = array('estatus' => 'ok', 'data' => false, 'info' => false);

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url', 'general', 'file', 'download'));
        $this->load->model('Documentos_facturas_model');
        $this->load->model('Estatus_documento_factura_model');
        $this->hostname = 'ftp.xcanpet.com';
        $this->username = 'mylsa@queretaro.mylsa';
        $this->password = 'dBQTrqV5r(C#';
    }

    public function getDataFile()
    {
        $documento_id =  $this->input->post('documento_id');
        $this->form_validation->set_data([
            'documento_id' => $documento_id
        ]);
        $this->form_validation->set_rules('documento_id', 'documento_id', 'trim|required');
        if ($this->form_validation->run() == true) {
            $this->load->model('Procesar_factura_model');
            $this->load->model('Documentos_facturas_procesadas_model');
            $dataDocumento = $this->Documentos_facturas_model->get(['id' => $documento_id]);
            $nombre_archivo = $dataDocumento['nombre_archivo'];
            $tipo_factura = $dataDocumento['tipo_factura'];

            $response = $this->handleConexion($nombre_archivo, $tipo_factura);
            if ($response['error']) {
                $this->responseEstatus($response['estatus'], $nombre_archivo);
            } else {
                $file_code = $response['contenido'];
                $tipo_factura = $this->findString($file_code, '[TIPO_FACTURA]', '[FIN_TIPO_FACTURA]');
                if ($tipo_factura) {
                    $tipo = $this->handleTipoDocumento(trim($tipo_factura));
                    if ($tipo && isset($tipo['id'])) {
                        $conceptillos = [];
                        $arrayFields = $this->Procesar_factura_model->getAll(['tipo' => $tipo['id'], 'estatus' => 1, 'concepto' => 0]);
                        foreach ($arrayFields as $val) {
                            $data[$val['field']] = $this->findString($file_code, $val['name'], $val['limit']);
                        }

                        $conceptillos = $this->handleConceptos($file_code, $tipo['id'], $nombre_archivo);
                        $respuestaTipoDocumento = $this->handleEstructuraDatos($tipo['id'], $data, $conceptillos);
                        //utils::pre($respuestaTipoDocumento['contenido']);
                        $apiFactura = $this->sendDataAPI($respuestaTipoDocumento['contenido'], $respuestaTipoDocumento['url_api']);
                        $responseApi = json_decode($apiFactura);
                        if ($responseApi) {
                            if ($responseApi->error) {
                                $data_insert = [
                                    'documento_factura_id' => $documento_id,
                                    'tipo_documento_id' => $tipo['id'],
                                    'respuesta_api' => $apiFactura,
                                    'envio_api' => json_encode($respuestaTipoDocumento['contenido']),
                                    'factura' => $responseApi->factura,
                                ];
                                $this->Documentos_facturas_procesadas_model->insert($data_insert);
                                $this->updateEstatus(6, $nombre_archivo);
                                $this->results['estatus'] = 'error';
                                $this->results['mensaje'] = $responseApi->error_mensaje;
                            } else {
                                $data_insert = [
                                    'documento_factura_id' => $documento_id,
                                    'tipo_documento_id' => $tipo['id'],
                                    'respuesta_api' => $apiFactura,
                                    'envio_api' => json_encode($respuestaTipoDocumento['contenido']),
                                    'factura' => $responseApi->factura,
                                    'sat_uuid' => $responseApi->sat_uuid,
                                    'pdf' => $responseApi->pdf,
                                    'xml' => $responseApi->xml,
                                ];
                                $this->Documentos_facturas_procesadas_model->insert($data_insert);
                                $this->updateEstatus(8, $nombre_archivo);
                                $this->results['data'] = json_decode($apiFactura);
                                $this->results['estatus'] = 'success';
                                $this->results['mensaje'] = $this->getMessageEstatus(8);
                            }
                            $this->Documentos_facturas_model->update(
                                [
                                    'factura' => $responseApi->factura
                                ],
                                ['nombre_archivo' => $nombre_archivo]
                            );
                        } else {
                            $this->responseEstatus(7, $nombre_archivo);
                        }
                    } else {
                        $this->responseEstatus(5, $nombre_archivo);
                    }
                } else {
                    $this->responseEstatus(4, $nombre_archivo);
                }
            }
        } else {
            $this->results['estatus'] = 'error';
            $this->results['info'] = $this->form_validation->error_array();
        }
        echo json_encode($this->results);
        exit();
    }
    private function handleConceptos($file_code, $tipo_id, $nombre_archivo)
    {
        $conceptos = [];
        switch ($tipo_id) {
            case 2:
                $conceptos = $this->handleConceptoTipoRefacciones($file_code, 2, $nombre_archivo);
                break;
            case 3:
                $conceptos = $this->handleConceptoTipoRefacciones($file_code, 3, $nombre_archivo);
                break;
            case 4:
                $conceptos = $this->handleConceptoTipoRefacciones($file_code, 4, $nombre_archivo);
                break;
            case 5:
                $conceptos = $this->handleConceptoTipoRefacciones($file_code, 5, $nombre_archivo);
                break;
            default:
                break;
        }
        return $conceptos;
    }
    private function handleConceptoTipoRefacciones($file_code, $tipo_id, $nombre_archivo)
    {
        $conceptillos = [];
        $multipleConceptos = $this->findString($file_code, '[SECCION_CONCEPTOS]', '[FIN_SECCION_CONCEPTOS]');
        if ($multipleConceptos) {
            $numero_conceptos = substr_count($multipleConceptos, 'CLAVEPRODSERV');
            for ($i = 1; $i <= $numero_conceptos; $i++) {
                $conceptos_text[$i] = $this->findString($multipleConceptos, '[CONCEPTO_' . $i . ']', '[FIN_CONCEPTO_' . $i . ']');
            }

            $arrayConceptos = $this->Procesar_factura_model->getAll(['tipo' => $tipo_id, 'estatus' => 1, 'concepto' => 1]);
            foreach ($conceptos_text as $key => $val) {
                foreach ($arrayConceptos as $c) {
                    $conceptillos[$key][$c['field']] = $this->findString($val, $c['name'], $c['limit']);
                }
            }
        } else {
            $this->responseEstatus(4, $nombre_archivo);
        }

        return $conceptillos;
    }
    private function handleEstructuraDatos($tipo_id, $data, $conceptos)
    {
        $sendData = [];
        $url_api = '';
        switch ($tipo_id) {
            case 1:
                $sendData =  $this->armarData($data);
                $url_api = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/index.php/complemento_pago/api_genera_complemento';
                break;
            case 2:
                $sendData =  $this->armarDataAutos($data, $conceptos);
                $url_api = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/api_factura_ser_ref/api_genera_factura';
                break;
            case 4:
                $sendData =  $this->armarDataAutos($data, $conceptos);
                $url_api = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/api_factura_ser_ref/api_genera_factura';
                break;

            case 3:
                $sendData = $this->armarDataRefacciones($data, $conceptos);
                $url_api = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/api_factura_ser_ref/api_genera_factura';
                break;
            case 5:
                $sendData = $this->armarDataServicios($data, $conceptos);
                $url_api = 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/api_factura_ser_ref/api_genera_factura';
                break;
            default:
                break;
        }
        return [
            'contenido' => $sendData,
            'url_api' => $url_api
        ];
    }
    private function handleTipoDocumento($tipo_factura)
    {
        $this->load->model('Tipo_documento_factura_model');
        return  $this->Tipo_documento_factura_model->get(['descripcion' => $tipo_factura]);
    }

    private function armarDataConceptos($conceptos = [])
    {
        $array_conceptos = [];
        $sum_importe = 0;
        $sum_importetrtc = 0;
        if ($conceptos) {
            foreach ($conceptos as $value) {
                $sum_importe += round($this->cleanString($value['importe']), 2);
                $sum_importetrtc += round($this->cleanString($value['importetrtc']), 2);
                $array_conceptos[] = [
                    'id_producto_servicio_interno' => $this->cleanString($value['claveprodserv']),
                    'concepto_cantidad' => $this->cleanString($value['cantidad']),
                    'unidad_interna' => $this->cleanString($value['unidad']),
                    'concepto_nombre' => $this->cleanString($value['descripcion']),
                    'concepto_precio' => $this->cleanString($value['valorunitario']),
                    'concepto_importe' => $this->cleanString($value['importe']),
                    'concepto_NoIdentificacion' => $this->cleanString($value['noidentificacion']),
                    'clave_sat' => '52141807',
                    'unidad_sat' => 'P83',
                    'Impuesto' => $this->cleanString($value['impuestotc']),
                    'TipoFactor' => $this->cleanString($value['tipofactortc']),
                    'TasaOCuota' => round($this->cleanString($value['tasaocuotatc']), 2),
                    'Base' => round($this->cleanString($value['basetc']), 2),
                    'Importe' => round($this->cleanString($value['importetrtc']), 2),
                    "concepto_descuento" => $this->cleanString($value['descuentoc']),
                    "ObjetoImp" => $this->cleanString($value['objetoimp'])
                ];
            }
        }

        return $array_conceptos;
    }

    private function armarDataConceptosAutos($conceptos = [])
    {
        $array_conceptos = [];
        $sum_importe = 0;
        $sum_importetrtc = 0;
        if ($conceptos) {
            foreach ($conceptos as $value) {
                // $sum_importe += round($this->cleanString($value['importe']), 2);
                // $sum_importetrtc += round($this->cleanString($value['importe']), 2);
                $array_conceptos[] = [
                    'id_producto_servicio_interno' => $this->cleanString($value['productoServicioInterno']),
                    'concepto_cantidad' => $this->cleanString($value['conceptoCantidad']),
                    'unidad_interna' => $this->cleanString($value['unidadInterna']),
                    'concepto_nombre' => $this->cleanString($value['conceptoNombre']),
                    'concepto_precio' => $this->cleanString($value['conceptoPrecio']),
                    'concepto_importe' => $this->cleanString($value['conceptoImporte']),
                    'concepto_NoIdentificacion' => $this->cleanString($value['conceptoNoIdentificacion']),
                    'clave_sat' => $this->cleanString($value['conceptoSAT']),
                    'unidad_sat' => $this->cleanString($value['unidadSAT']),
                    'Impuesto' => $this->cleanString($value['impuesto']),
                    'TipoFactor' => $this->cleanString($value['tipoFactor']),
                    'TasaOCuota' => round($this->cleanString($value['tasaCuota']), 2),
                    'Base' => round($this->cleanString($value['base']), 2),
                    'Importe' => round($this->cleanString($value['importe']), 2),
                    "concepto_descuento" => $this->cleanString($value['conceptoDescuento']),
                    "ObjetoImp" => $this->cleanString($value['objetoImp']),
                    "clave_vehicular" => $this->cleanString($value['claveVehicular']),
                    "niv" => $this->cleanString($value['niv']),
                    "numero" => $this->cleanString($value['numero']),
                    "fecha" => $this->cleanString($value['fechaConcepto']),
                    "aduana" => $this->cleanString($value['aduana'])
                ];
            }
        }

        return $array_conceptos;
    }


    private function armarDataEnajenacion($data = [])
    {
        $array_enajenacion = [];
        //utils::pre($data);
        if ($data) {
            $array_enajenacion[] = [
                'toma_monto_enajenacion' => $this->cleanString($data['toma_monto_enajenacion']),
                'toma_monto_adquisicion' => $this->cleanString($data['toma_monto_adquisicion']),
                'toma_clave_vehicular' => $this->cleanString($data['toma_clave_vehicular']),
                'toma_marca' => $this->cleanString($data['toma_marca']),
                'toma_tipo' => $this->cleanString($data['toma_tipo']),
                'toma_modelo' => $this->cleanString($data['toma_modelo']),
                'toma_numero_motor' => $this->cleanString($data['toma_numero_motor']),
                'toma_niv' => $this->cleanString($data['toma_niv']),
                'toma_valor' => $this->cleanString($data['toma_valor']),
                'numero' => $this->cleanString($data['numero']),
                'fecha' => $this->cleanString($data['fecha_enajenacion']),
                'aduana' => $this->cleanString($data['aduana'])
            ];
        }
        //utils::pre($array_enajenacion);
        return $array_enajenacion;
    }

    private function armarDataRefacciones($data = [], $conceptos = [])
    {
        $array_conceptos = $this->armarDataConceptos($conceptos);
        //$tot = $sum_importe + $sum_importetrtc;
        // utils::pre($tot);
        // utils::pre($sum_importe,false);
        // utils::pre($sum_importetrtc);
        $array_refacciones = [
            'Folio' => $this->cleanString($data['factura_folio']),
            'Serie' => $this->cleanString($data['factura_serie']),
            'fecha' => $this->cleanString($data['factura_fecha']),
            'FormaPago' => $this->cleanString($data['factura_formaPago']),
            'metodo_pago' => $this->cleanString($data['factura_metodoPago']), //se repite
            'subtotal' => $this->cleanString($data['factura_subtotal']),
            'iva' => (float)($this->cleanString($data['factura_total'])) - (float)($this->cleanString($data['factura_subtotal'])),
            'total' => $this->cleanString($data['factura_total']),
            'moneda' => $this->cleanString($data['factura_moneda']),
            'TipoDeComprobante' => $this->cleanString($data['factura_tipodeComprobante']),
            'MetodoPago' => $this->cleanString($data['factura_metodoPago']),  //se repite
            'LugarExpedicion' => $this->cleanString($data['factura_lugarExpedicion']),
            'comentario' => $this->cleanString($data['mensaje']),
            'TipoCambio' => $this->cleanString($data['factura_tipocambio']),
            'receptor_RFC' => $this->cleanString($data['rfcr']),
            'receptor_nombre' => $this->cleanString($data['nombrer']),
            'receptor_uso_CFDI' => $this->cleanString($data['usocfdi']),
            'receptor_email' => $this->cleanString($data['email']),
            'receptor_direccion' =>  '',
            'DomicilioFiscalReceptor' => $this->cleanString($data['cpr']),
            'RegimenFiscalReceptor' => $this->cleanString($data['regimenFiscalr']),
            'descuento' => $this->cleanString($data['factura_descuento']),
            'condicionesDePago' => $this->cleanString($data['factura_condicionesdePago']),
            'NumCtaPago' => $this->cleanString($data['numcuentapago']),
            'conceptos' => $array_conceptos,
            'tipo_factura' => 0,
        ];
        return $array_refacciones;
    }

    private function armarDataServicios($data = [], $conceptos = [])
    {

        $array_conceptos = $this->armarDataConceptos($conceptos);

        $array_servicios = [
            'Folio' => $this->cleanString($data['factura_folio']),
            'Serie' => $this->cleanString($data['factura_serie']),
            'fecha' => $this->cleanString($data['factura_fecha']),
            'FormaPago' => $this->cleanString($data['factura_formaPago']),
            'metodo_pago' => $this->cleanString($data['factura_condicionesPago']),
            'subtotal' => $this->cleanString($data['factura_subtotal']),
            'iva' => (float)($this->cleanString($data['factura_total'])) - (float)($this->cleanString($data['factura_subtotal'])),
            'total' => $this->cleanString($data['factura_total']),
            'moneda' => $this->cleanString($data['factura_moneda']),
            'TipoDeComprobante' => $this->cleanString($data['factura_tipoComprobante']),
            'MetodoPago' => $this->cleanString($data['factura_metodopago']),
            'LugarExpedicion' => $this->cleanString($data['factura_lugarexpedicion']),
            'comentario' => $this->cleanString($data['mensaje']),
            'TipoCambio' => $this->cleanString($data['factura_tipodecambio']),
            'receptor_RFC' => $this->cleanString($data['rfcr']),
            'receptor_nombre' => $this->cleanString($data['nombrer']),
            'receptor_uso_CFDI' => $this->cleanString($data['usocfdi']),
            'receptor_email' => $this->cleanString($data['email']),
            'receptor_direccion' =>  $this->cleanString($data['direccionr']),
            'DomicilioFiscalReceptor' => $this->cleanString($data['cpr']),
            'RegimenFiscalReceptor' => $this->cleanString($data['regimenFiscalr']),
            'descuento' => $this->cleanString($data['descuento']),
            'condicionesDePago' => $this->cleanString($data['factura_condicionesPago']),
            'NumCtaPago' => $this->cleanString($data['numcuentapago']),
            'conceptos' => $array_conceptos,
            'tipo_factura' => 1,
            'Tipo' => $this->cleanString($data['tipo_venta']),
            'Modelo' => $this->cleanString($data['modelo']),
            'Placas' => $this->cleanString($data['placa']),
            'Color' => $this->cleanString($data['color']),
            'KM' => $this->cleanString($data['km']),
            'Orden' => $this->cleanString($data['orden']),
            'Version' => $this->cleanString($data['unidad']),
            'fecha_recepcion' => '', //TODO VERIFICAR DONDE OBTENERLO Y NUM ECONOMICO NO VIENE EN LA API
            'Asesor' => $this->cleanString($data['asesor']),
            'Transmision' => '', //TODO NO VIENE EN EL ARCHIVO DE TEXTO DE PRUEBA
        ];
        //utils::pre($array_servicios); 
        return $array_servicios;
    }

    //CRP09393
    private function armarData($data = [])
    {

        $array = [
            'uuid' => $this->cleanString($data['UUID']),
            'seriep' => $this->cleanString($data['seriepago']),
            'foliop' => $this->cleanString($data['foliopago']) . '-' . $this->cleanString($data['numparcialidad']),
            'moneda_pe' => $this->cleanString($data['monedap']),
            'tipo_cambio_p' => $this->cleanString($data['tipocambiop']),
            'metodo_pago_p' => $this->cleanString($data['factura_metodoPago']),
            'numero_parcial' => $this->cleanString($data['numparcialidad']),
            'deuda_pagar' => '',
            'importe_pagado' => $this->cleanString($data['imppagado']),
            'nuevo_saldo' => '',
            'receptor_nombre' => $this->cleanString($data['nombrer']),
            'lugarExpedicion' => $this->cleanString($data['lugarexpedicion']),
            'fecha_pago' => $this->cleanString($data['fechapago']),
            'receptor_email' =>  '',
            'factura_folio' => $this->cleanString($data['factura_folio']),
            'factura_serie' => $this->cleanString($data['factura_serie']),
            'comentario' => '',
            'receptorRFC' => $this->cleanString($data['rfcr']),
            'complemento_fecha' => '',
            'complemento_forma_pago' => $this->cleanString($data['metododepagodr']),
            'complemento_tipoCambio' => '',
            'complemento_totalPago' => $this->cleanString($data['monto']),
            'complemento_no_operacion' => '',
            'complemento_banco_ordenante' => '',
            'complemento_cta_ordenante' => '',
            'complemento_rfc_beneficiario' => '',
            'complemento_cta_beneficiario' => '',
            'complemento_rfc_ordenante' => '',

        ];
        //echo '<pre>'; print_r($array); exit();
        return $array;
    }

    private function armarDataAutos($data = [], $conceptos = [])
    {
        $array_enajenacion = $this->armarDataEnajenacion($data);
        $array_conceptos = $this->armarDataConceptosAutos($conceptos);

        $array_autos = [
            'Folio' => $this->cleanString($data['folio']),
            'Serie' => $this->cleanString($data['serie']),
            'fecha' => $this->cleanString($data['fecha']),
            'FormaPago' => $this->cleanString($data['formaPago']),
            'metodo_pago' => $this->cleanString($data['condicionesPago']),
            'subtotal' => $this->cleanString($data['subtotal']),
            'iva' => (float)($this->cleanString($data['total'])) - (float)($this->cleanString($data['subtotal'])),
            'total' => $this->cleanString($data['total']),
            'moneda' => $this->cleanString($data['moneda']),
            'TipoDeComprobante' => $this->cleanString($data['tipoComprobante']),
            'MetodoPago' => $this->cleanString($data['metodoPago']),
            'LugarExpedicion' => $this->cleanString($data['lugarExpedicion']),
            'comentario' => $this->cleanString($data['mensajePDF']),
            'TipoCambio' => $this->cleanString($data['tipoCambio']),
            'receptor_RFC' => $this->cleanString($data['rfcr']),
            'receptor_nombre' => $this->cleanString($data['nombrer']),
            'receptor_uso_CFDI' => $this->cleanString($data['usocfdi']),
            'receptor_email' => $this->cleanString($data['emailr']),
            'receptor_direccion' =>  $this->cleanString($data['direccionr']),
            'DomicilioFiscalReceptor' => $this->cleanString($data['cpr']),
            'RegimenFiscalReceptor' => $this->cleanString($data['regimenFiscalr']),
            'descuento' => $this->cleanString($data['descuento']),
            'condicionesDePago' => $this->cleanString($data['condicionesPago']),
            'NumCtaPago' => $this->cleanString($data['numCuentar']),
            'tipo_auto' => $this->cleanString($data['tipoAuto']),
            'enajenacion' => $this->cleanString($data['enajenacion']),
            'Tipo' => $this->cleanString($data['tipo']),
            'SerieA' => $this->cleanString($data['serieA']),
            'Modelo' => $this->cleanString($data['modelo']),
            'Placas' => $this->cleanString($data['placas']),
            'Color' => $this->cleanString($data['color']),
            'KM' => $this->cleanString($data['km']),
            'Orden' => $this->cleanString($data['orden']),
            'Version' => $this->cleanString($data['version']),
            'fecha_recepcion' => '', //$this->cleanString($data['fechaRecepcion']), 
            'Asesor' => $this->cleanString($data['asesor']),
            'Transmision' => $this->cleanString($data['transmision']),
            'unidad_importada' => $this->cleanString($data['unidadImportada']),
            'enajenacion_auto' => $array_enajenacion,
            'conceptos' => $array_conceptos,
            //'tipo_factura' => 1,
        ];
        //utils::pre($array_autos); 
        return $array_autos;
    }

    private function sendDataAPI($data, $url)
    {
        //utils::pre(json_encode($data));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $data_return = false;
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            log_message('error', curl_error($curl));
            $data_return = $response;
        } else {
            $data_return = $response;
        }
        curl_close($curl);

        return $data_return;
    }
    private function handleConexion($nombre_archivo, $tipo_factura)
    {
        $directorio = $tipo_factura == 1 ? 'Facturas' : 'Factura_Autos';
        $file_code = '';
        if ($this->conexion()) {
            $list = $this->ftp->list_files($directorio);
            $ruta_descarga = (array_search($directorio . $nombre_archivo, $list) !== FALSE) ? $directorio . $nombre_archivo : trim($directorio, DIRECTORY_SEPARATOR) . '/' . $nombre_archivo;

            if (array_search($ruta_descarga, $list) !== FALSE) {
                $temp_path = tempnam(sys_get_temp_dir(), $nombre_archivo);
                $this->ftp->download($ruta_descarga, $temp_path);
                $file_code = file_get_contents($temp_path);
                $response['error'] = FALSE;
            } else {
                $response['estatus'] = 3;
                $response['error'] = TRUE;
            }
        } else {
            $response['estatus'] = 2;
            $response['error'] = TRUE;
        }
        $response['contenido'] = $file_code;
        return $response;
    }
    private function handleDeleteArchivo($nombre_archivo)
    {
        $directorio = 'Facturas';
        if ($this->conexion()) {
            $list = $this->ftp->list_files($directorio);
            $ruta_descarga = (array_search($directorio . $nombre_archivo, $list) !== FALSE) ? $directorio . $nombre_archivo : trim($directorio, DIRECTORY_SEPARATOR) . '/' . $nombre_archivo;
            if (array_search($ruta_descarga, $list) !== FALSE) {
                $fileArchivo = $this->ftp->delete_file($ruta_descarga);
                if ($fileArchivo != true) {
                    $response['mensaje'] = "No se pudo eliminar el archivo: " . $ruta_descarga;
                    $response['respuesta'] = false;
                } else {
                    $response['respuesta'] = true;
                }
            } else {
                $response['respuesta'] = false;
                $response['mensaje'] = "No se encontro el archivo: " . $ruta_descarga;
            }
        }
        return $response;
    }
    private function responseEstatus($estatus_id, $nombre_archivo)
    {
        $this->updateEstatus($estatus_id, $nombre_archivo);
        $this->results['estatus'] = 'error';
        $this->results['mensaje'] = $this->getMessageEstatus($estatus_id);
    }
    private function updateEstatus($estatus_id, $nombre_archivo)
    {
        return $this->Documentos_facturas_model->update(
            [
                'estatus_documento_factura_id' => $estatus_id
            ],
            ['nombre_archivo' => $nombre_archivo]
        );
    }
    private function getMessageEstatus($estatus_id)
    {
        $respuesta = $this->Estatus_documento_factura_model->get(['id' => $estatus_id]);
        return $respuesta['descripcion'];
    }
    private function conexion()
    {
        $this->load->library('ftp');
        $config['hostname'] = $this->hostname;
        $config['username'] = $this->username;
        $config['password'] = $this->password;
        return $this->ftp->connect($config);
    }
    private function cleanString($texto)
    {
        return trim(str_replace([';', '|', ','], ['', '', ''], utf8_encode($texto)));
    }
    public function findString($texto, $search, $limitText)
    {
        $pos = strpos($texto, $search) >= 0 ?  strpos($texto, $search) + strlen($search) :  null;
        if ($pos) {
            $str_cadena = (substr($texto, $pos));
            if ($search == "UNIDAD" || $search == "TOTAL" || $search == "descuento") {
                $pos = strrpos($str_cadena, $search) + strlen($search);
                $str_cadena = (substr($str_cadena, $pos));
                $str_cadena = substr($str_cadena, 0, strpos($str_cadena, $limitText));
            } else if ($search == "importe") {
                $pos = strpos($str_cadena, $search) + strlen($search);
                $str_cadena = (substr($str_cadena, $pos));
                $str_cadena = substr($str_cadena, 0, strpos($str_cadena, $limitText));
            } else {
                $str_cadena = substr($str_cadena, 0, strpos($str_cadena, $limitText));
            }

            return  $str_cadena;
        }
        return false;
    }

    public function save_documento_factura()
    {
        $this->form_validation->set_rules('nombre_archivo', 'nombre_archivo', 'trim|required');
        $this->form_validation->set_rules('tipo_factura', 'tipo_factura', 'trim|required');

        if ($this->form_validation->run() == true) {
            $existe = $this->Documentos_facturas_model->get(['nombre_archivo' => $this->input->post('nombre_archivo')]);
            if (!$existe) {
                $data = [
                    'nombre_archivo' => $this->input->post('nombre_archivo'),
                    'tipo_factura' => $this->input->post('tipo_factura') ? $this->input->post('tipo_factura') : 1
                ];
                $insert = $this->Documentos_facturas_model->insert($data);
                if ($insert) {
                    $this->results['estatus'] = 'success';
                    $this->results['mensaje'] = 'Registro ingresado correctamente';
                }
            } else {
                $this->results['estatus'] = 'error';
                $this->results['mensaje'] = 'Ya existe un documento con este nombre';
            }
        } else {
            $this->results['estatus'] = 'error';
            $this->results['info'] = $this->form_validation->error_array();
        }
        print_r(json_encode($this->results));
    }

    public function descargarArchivo()
    {
        $nombre_archivo = $this->input->get('nombre_archivo');
        $tipo_factura = $this->input->get('tipo_factura');
        $data =  $this->handleConexion($nombre_archivo, $tipo_factura);
        print_r(json_encode(utf8_encode($data['contenido'])));
        exit();
    }

    public function deleteArchivo()
    {
        $documento_id = $this->input->post('documento_id');
        $file = $this->Documentos_facturas_model->get(['id' => $documento_id]);
        $this->form_validation->set_rules('documento_id', 'documento_id', 'trim|required');
        if ($this->form_validation->run() == true) {
            $data =  $this->handleDeleteArchivo($file['nombre_archivo']);
            if ($data) {
                $update = $this->Documentos_facturas_model->update(
                    [
                        'estatus_documento_factura_id' => 9
                    ],
                    ['id' => $documento_id]
                );
                if ($update) {
                    $this->results['estatus'] = 'success';
                    $this->results['mensaje'] = 'Registro eliminado correctamente';
                } else {
                    $this->results['estatus'] = 'error';
                    $this->results['mensaje'] = 'Archivo eliminado, pero no actualizado en base de datos';
                }
            } else {
                $this->results['estatus'] = 'error';
                $this->results['mensaje'] = 'No sé pudo eliminar el archivo';
            }
        } else {
            $this->results['estatus'] = 'error';
            $this->results['info'] = $this->form_validation->error_array();
        }
        print_r(json_encode($this->results));
        exit();
    }

    public function getDocumentosFactura()
    {
        $where = [
            'doc.estatus_documento_factura_id !=' => 9,
            'doc.tipo_factura' => 1
        ];
        $docs = $this->Documentos_facturas_model->getList($where);
        print_r(json_encode($docs));
        exit();
    }

    public function getDocumentosFacturaAutos()
    {
        $where = [
            'doc.estatus_documento_factura_id !=' => 9,
            'doc.tipo_factura' => 2
        ];
        $docs = $this->Documentos_facturas_model->getList($where);
        print_r(json_encode($docs));
        exit();
    }

    public function conexionSSH()
    {
        $dataFile      = '/prueba.txt';
        $sftpPort = 22;
        $sftpRemoteDir = '/uploads';
        $ch = curl_init('sftp://' . $this->hostname . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile));
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
        curl_setopt($ch, CURLOPT_DIRLISTONLY, '1L');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        echo $response;
        exit();
    }
}
