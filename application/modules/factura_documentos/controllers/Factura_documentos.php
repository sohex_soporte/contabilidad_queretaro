<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Factura_documentos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Documentos_facturas_model', '', TRUE);
        $this->load->library(array('session','curl'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function index(){
          
        $this->blade->render('index',[]);
    }

    public function autos(){
          
        $this->blade->render('index_factura_autos',[]);
    }
}