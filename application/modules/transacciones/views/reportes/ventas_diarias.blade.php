@layout('template_blade/estructura')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Ventas diarias servicio clientes
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <form id="form_filter" type="get" href="<?php echo site_url('cheques/index'); ?>">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="card-title mb-5">Filtros</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="fecha" class="col-form-label">Fecha de inicio</label>
                                <input type="date" class="form-control" id="fecha_ini" name="fecha_ini" value="<?php echo $fecha_ini; ?>" max="<?php echo utils::get_date(); ?>">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="fecha" class="col-form-label">Fecha fin</label>
                                <input type="date" class="form-control" id="fecha_fin" name="fecha_fin" value="<?php echo $fecha_fin; ?>" max="<?php echo utils::get_date(); ?>">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="nomenclatura" class="col-form-label">Poliza nomenclatura:</label>
                                <select class="form-control" id="nomenclatura" name="nomenclatura">
                                    <?php if (is_array($nomenclaturas) && count($nomenclaturas) > 0) { ?>
                                        <option value="">Todos</option>
                                        <?php foreach ($nomenclaturas as $nomenclatura) { ?>
                                            <option <?php echo ($nomenclatura_sel == $nomenclatura['id']) ? 'selected' : ''; ?> value="<?php echo $nomenclatura['id']; ?>">
                                                <?php echo '[' . $nomenclatura['id'] . '] ' . $nomenclatura['descripcion']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class=" col-sm-12">
                            <button class="btn btn-primary btn-sm ml-2 my-2 my-sm-0 float-right" type="submit"><i class="fas fa-search"></i> Buscar</button>
                            <button onclick="APP.abrir_pdf()" class="btn btn-danger btn-sm ml-2 my-2 my-sm-0 float-right" type="button"><i class="fas fa-file-pdf"></i> Exportar</button>
                            <button class="btn btn-secondary  btn-sm ml-2 my-2 my-sm-0 float-right" onclick="APP.limpiar_busqueda();" type="button"> Limpiar</button>
                        </div>
                    </div>
                </form>

                <hr class="mt-3" />

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">C O N T E N I D O</h5>
                    </div>
                </div>


                <div class="row">
                    <div class=" col-sm-12 col-md-10 col-lg-8 mx-auto ">
                        <div class="embed-responsive embed-responsive-1by1">
                            <iframe class="embed-responsive-item" src="" id="contenedor_documento"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection




@section('style')
<?php $this->carabiner->display('datatables', 'css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .blue {
        background-color: #c5d7f2
    }

    .lightgray {
        background-color: #f1f1f1
    }

    table.contenido th {
        font-size: 14px;
        width: 8.333% !important;
        /* height: 20px; */
        padding: 3px;
        text-align: center;
    }

    table.contenido td {
        font-size: 13px;
        width: 8.333% !important;
        height: 20px;
        padding: 5px;
    }

    .justify {
        text-align: justify;
        text-justify: inter-word;
    }

    tfoot tr {
        font-weight: bold;
        /* font-size: x-small; */
        background-color: #f1f1f1;
    }

    tfoot tr td {
        font-size: 10px;
        padding: 0px;
        margin: 0px;
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/transacciones/reportes/ventas_diarias.js'); ?>"></script>
@endsection