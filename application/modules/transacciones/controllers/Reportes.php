<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

   
    public function ventas_diarias(){

        $this->session->set_userdata('poliza','cheque');

        $fecha_ini = $this->input->get('fecha_ini');
        $fecha_fin = $this->input->get('fecha_fin');
        $nomenclatura_sel = $this->input->get('nomenclatura');
       
        if($fecha_ini == false){
            $fecha_ini = utils::get_date();
        }

        if($fecha_fin == false){
            $fecha_fin = utils::get_date();
        }

        $this->load->model('CaPolizasNomenclaturas_model');
        $nomenclaturas = $this->CaPolizasNomenclaturas_model->getAll();

        $content = array(
            'fecha_ini' => $fecha_ini,
            'fecha_fin' => $fecha_fin,
            'nomenclatura_sel' => $nomenclatura_sel,
            'nomenclaturas' => $nomenclaturas
        );

        $this->blade->render('/reportes/ventas_diarias',$content);
    }  
}