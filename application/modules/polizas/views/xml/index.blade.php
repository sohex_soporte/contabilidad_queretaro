@layout('template_blade/estructura')

@section('title')
<center>
    <h3>Polizas XML</h3>
</center>
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Inicio</li>
                <li class="breadcrumb-item">Polizas</li>
            </ol>
        </nav>
    </div>
</div>
@endsection

@section('contenido')
<nav class="navbar navbar-light" >
    <a class="navbar-brand">Listado de Poliza</a>
    <form class="form-inline">
        <a title="Agregar" href="<?php echo site_url('polizas/xml/create'); ?>"class="btn btn btn-secondary float-right" type="button"><i class="fas fa-plus"></i> Nueva poliza</a>
    </form>
</nav>

            <div class=" col-sm-4 mx-auto">
                

                <div class="table-responsive">
                <table class="table  table-bordered">
                    <tbody>
                        <tr>
                            <td width="50%"><b>Nuevo</b></td>
                            <td width="50%" class=""><span class="badge badge-light">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        </tr>
                        <tr>
                            <td><b>Poliza finalizada</b></td>
                            <td width="50%" class=""><span class="badge badge-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        </tr>
                        <tr>
                            <td><b>Creación de cheque parcial</b></td>
                            <td width="50%" class=""><span class="badge badge-warning">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        </tr>
                        <tr>
                            <td><b>Creación de cheque completo</b></td>
                            <td width="50%" class=""><span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        </tr>
                        <!-- <tr>
                            <td><b>Generacion de cheque</b></td>
                            <td width="50%" class=""><span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        </tr> -->
                        
                    </tbody>
                </table>
                </div>
                
            </div>
        </div>
<div class="row mt-3">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                
            <div class="row">
            <div class=" col-sm-12">
                

                <div class="table-responsive">
                <?php echo $tabla; ?>
                </div>
                
            </div>
        </div>

        


            </div>
        </div>
    </div>
</div>

<div id="modal_documentacion"></div>
@endsection

@section('script')
<script>
function modal_documentacion(obj) {
    console.log(obj);
    var iden = $(obj).data('renglon_id');
    console.log(iden);

    $.ajax({
            url: "<?php echo site_url('polizas/xml_api/obtener_documentacion'); ?>",
            dataType: "json",
            data: {
                id: iden
            },
            cache: false,
            type: "get",

            success: function(response) {
                console.log(response);
                var html = atob(response.data.html);
                $('div#modal_documentacion').html(html);
                $('div#modal_documentacion div.modal').modal({
                    keyboard: false,
                    backdrop: 'static'
                });


            },
            error: function(xhr) {

            }
        });
    
}
</script>

@endsection

@section('style')

<style>
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
        text-align: center;
    }
</style>
@endsection
