<form id="data_form_modal" >
    <div class="row">

        <div class="col-sm-12">
            <label for="descripcion" class="control-label">Descripción</label>
            <textarea type="text" class="form-control" id="descripcion" name="descripcion" ></textarea>
            <small id="msg_descripcion" class="form-text text-danger"></small>
        </div>
        
        <div class="col-sm-12" id="contenido_xml">
            <label for="archivo_xml"><small class="text-danger">*</small> Archivo XML</label>
            <div class="row">
                <div class="col-sm-12">
                    
                <label for="file" class="input input-file" style="width: 100%;">
                    <input id="id_archivo_modal" name="id_archivo_modal" type="hidden" />
                    <input id="id_original_modal" name="id_original_modal" type="hidden" />
                    <div id="seleccionar_archivo" style="width: 100%;"></div> 
                </label>

                <div id='singleupload1_progress' style="display:none;width: 100%;" >
                    <div class="">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" ></div>
                        </div>
                    </div>
                </div>

                <div style="display:none;margin-top:-7px;" class="input-group" id="singleupload1_finaly">
                    <input type="text" id="nombre_archivo" name="nombre_archivo" class="form-control" placeholder="Nombre archivo" disabled="disabled" readonly="readonly" style="padding: 0px 10px 0px 10px"/>
                    <div class="input-group-btn">
                        <button class="btn btn-danger" onclick="$('#id_archivo_modal').val('');$('#nombre_archivo').val('');$('#singleupload1_finaly').hide();$('#singleupload1_content').show();" type="button" style="padding: 6px 12px;">
                            Eliminar
                        </button>  
                    </div>
                </div>
                <small id="msg_id_archivo_modal" class="form-text text-danger"></small>

                </div>
            </div>
        </div>

        <div class="col-sm-12" id="contenido_pdf">
            <label for="archivo_xml"><small class="text-danger">*</small> Archivo PDF</label>
            <div class="row">
                <div class="col-sm-12">
                    
                <label for="file" id="singleupload1_content" class="input input-file" style="width: 100%;">
                    <input id="id_archivo_modal2" name="id_archivo_modal2" type="hidden" />
                    <input id="id_original_modal2" name="id_original_modal2" type="hidden" />
                    <div id="seleccionar_archivo" style="width: 100%;"></div> 
                </label>

                <div id='singleupload1_progress' style="display:none;width: 100%;" >
                    <div class="">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" ></div>
                        </div>
                    </div>
                </div>

                <div style="display:none;margin-top:-7px;" class="input-group" id="singleupload1_finaly">
                    <input type="text" id="nombre_archivo2" name="nombre_archivo2" class="form-control" placeholder="Nombre archivo" disabled="disabled" readonly="readonly" style="padding: 0px 10px 0px 10px"/>
                    <div class="input-group-btn">
                        <button class="btn btn-danger" onclick="$('div#contenido_pdf #id_archivo_modal').val('');$('div#contenido_pdf #nombre_archivo').val('');$('div#contenido_pdf #singleupload1_finaly').hide();$('div#contenido_pdf #singleupload1_content').show();" type="button" style="padding: 6px 12px;">
                            Eliminar
                        </button>  
                    </div>
                </div>
                <small id="msg_id_archivo_modal2" class="form-text text-danger"></small>

                </div>
            </div>
        </div>
        
        <div class="col-sm-12">
            <label class="control-label">Subtotal</label>
            <input readonly type="text" class="form-control" id="subtotal" name="subtotal" />
            <small id="msg_subtotal" class="form-text text-danger"></small>
        </div>

        <div class="col-sm-12">
            <label class="control-label">IVA</label>
            <input type="text" readonly class="form-control" id="iva" name="iva" />
            <small id="msg_iva" class="form-text text-danger"></small>
        </div>

        <div class="col-sm-12">
            <label class="control-label">Total</label>
            <input readonly type="text" class="form-control" id="total" name="total" />
            <small id="msg_total" class="form-text text-danger"></small>
        </div>
        
    </div>
</form>