@layout('template_blade/estructura')

@section('title')
<center>
    <h3>Nueva Poliza</h3>
</center>
@endsection

@section('contenido')

<div class="row">
    <div class="col-sm-6 mx-auto">
        <div class="card mt-3">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <form class="form-inlines" id="polizas_fijas_crear" role="form">
                            <div class="form-group">
                                <label for="archivo_xml"><small class="text-danger">*</small> Poliza</label>
                                <div class="row">
                                    <div class="col-sm-12">

                                        <select onchange="ver_polizas_fijas(this.value);" class="form-control"
                                            name="id_poliza">
                                            <?php if(is_array($polizas) && count($polizas)>0){ ?>
                                            <option value="">Selecione una opción</option>
                                            <?php foreach($polizas as $poli){ ?>
                                            <option value="<?php echo $poli['id'] ?>">
                                                <?php echo '['.utils::folio($poli['id'],2).'] '.$poli['descripcion'] ?>
                                            </option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <small id="msg_id_poliza" class="form-text text-danger"></small>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12 " id="id_poliza_fija_div" style="display:none;">
                                        <label for="archivo_xml"><small class="text-danger">*</small>Tipo Poliza
                                            Diario</label>
                                        <select class="form-control" name="id_poliza_fija" id="id_poliza_fija">
                                            <?php if(is_array($polizas_fijas) && count($polizas_fijas)>0){ ?>
                                            <option value="">Selecione una opción</option>
                                            <?php foreach($polizas_fijas as $poliza_fija){ ?>
                                            <option value="<?php echo $poliza_fija['id'] ?>">
                                                <?php echo '['.utils::folio($poliza_fija['id'],2).'] '.$poliza_fija['poliza'] ?>
                                            </option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <small id="msg_id_poliza_fija" class="form-text text-danger"></small>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-sm-12" id="" style="display:block;">
                                        <label for="archivo_xml"><small class="text-danger">*</small> Proveedor</label>
                                        <select class="form-control" name="id_persona" id="id_persona">
                                            <?php if(is_array($proveedores) && count($proveedores)>0){  ?>
                                            <option value="">Selecione una opción</option>
                                            <?php foreach($proveedores as $proveedor){ ?>
                                            <option value="<?php echo $proveedor['persona_id'] ?>">
                                                <?php echo '['.$proveedor['rfc'].'] '.trim($proveedor['nombre'].' '.$proveedor['apellido1'].' '.$proveedor['apellido2']) ?>
                                            </option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <small id="msg_id_persona" class="form-text text-danger"></small>
                                    </div>
                                </div>


                                <div class="row mt-3">
                                    <div class="col-sm-12" id="" style="display:block;">
                                        <label for="archivo_xml"><small class="text-danger">*</small> Descripción</label>
                                        <textarea id="descripcion" class="form-control" name="descripcion"></textarea>
                                        <small id="msg_descripcion" class="form-text text-danger"></small>
                                    </div>
                                </div>


                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="<?php echo site_url('polizas/xml'); ?>"  class="btn btn-secondary" >Cerrar</a>
                    <button type="button" class="btn btn-primary" onclick="crear_poliza()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<?php $this->carabiner->display('select2') ?>
<?php $this->carabiner->display('sweetalert2'); ?>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha512-7VTiy9AhpazBeKQAlhaLRUk+kAMAb8oczljuyJHPsVPWox/QIXDFOnT9DUk1UC8EbnHKRdQowT7sOBe7LAjajQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-file-upload/4.0.11/jquery.uploadfile.min.js" integrity="sha512-uwNlWrX8+f31dKuSezJIHdwlROJWNkP6URRf+FSWkxSgrGRuiAreWzJLA2IpyRH9lN2H67IP5H4CxBcAshYGNw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">

    function crear_poliza(){

        var data_send = $('form#polizas_fijas_crear').serializeArray();
       
        $.ajax({
            url: "<?php echo site_url('polizas/xml_api/crear_poliza'); ?>",
            dataType: "json",
            data: data_send,
            cache: false,
            type: "POST",

            success: function(data) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        // $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            window.location.href = PATH+'/polizas/xml/create?id='+btoa(data.data.poliza_id);
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function(index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
                
            },
            error: function(xhr) {

            }
        });
    }


    function ver_polizas_fijas(valor){  
        if(valor == 'DO'){
            $('div#id_poliza_fija_div').show();
        }else{
            $('div#id_poliza_fija_div').hide();
        }

    }
</script>

@endsection