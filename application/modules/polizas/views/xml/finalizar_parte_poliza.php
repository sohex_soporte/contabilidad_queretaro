<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Listado de comprobantes disponibles</h5>
        
      </div>
      <div class="modal-body">
        <div class="row m-0">
            <div class="col-12">
              <form class="" id="contenedor_tabla">
                <?php echo $table; ?>
              </form>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="app.finalizar_parte_poliza_guardar();" type="button" id="crear_cheque_bton" disabled class="btn btn-primary">Finalizar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>