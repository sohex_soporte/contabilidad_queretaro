@layout('template_blade/estructura')

@section('title')
<center>
    <h3>Polizas</h3>

    <?php if($transaccion_datos != false){ ?>
    <h3><small><?php echo (is_array($poliza_nomenclatura) && array_key_exists('id',$poliza_nomenclatura))? $poliza_nomenclatura['id'].' '.$poliza_nomenclatura['descripcion'] : ''; ?></small>
    </h3>
    <h3><small><?php echo $transaccion_datos['folio'].' '.$transaccion_datos['poliza_fija']; ?></small></h3>
    <?php } ?>

</center>
@endsection

@section('breadcrumb')
<!-- <div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Inicio</li>
                <li class="breadcrumb-item active" ><a href="<?php echo site_url('polizas/xml'); ?>">Poliza</a></li>
                <li class="breadcrumb-item active" aria-current="page">Nuevo</li>
            </ol>
        </nav>
    </div>
</div> -->
@endsection

@section('contenido')

<style>
    input.largerCheckbox {
        width: 20px;
        height: 20px;
    }

    button.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>

<script>
    var id_persona = "<?php echo is_array($poliza_datos) && array_key_exists('id_persona',$poliza_datos)? $poliza_datos['id_persona'] : ""; ?>";
    var id_poliza_act = "<?php echo is_array($poliza_datos) && array_key_exists('id',$poliza_datos)? $poliza_datos['id'] : false; ?>";
    var id_det_poliza_act = null;
    var transaccion_id = "<?php echo is_array($poliza_datos) && array_key_exists('transaccion_id',$poliza_datos)? $poliza_datos['transaccion_id'] : false; ?>";
    var id_poliza_gen = "<?php echo is_array($poliza_datos) && array_key_exists('id_poliza',$poliza_datos)? $poliza_datos['id_poliza'] : false; ?>";
    var id_poliza_nomenclatura = "<?php echo is_array($poliza_datos) && array_key_exists('id_poliza_nomenclatura',$poliza_datos)? $poliza_datos['id_poliza_nomenclatura'] : false; ?>";
    var id_poliza_xml_detalle = 0;

    function crear_sub_transacccion() {
        var data_send = $('form#opciones_tbls').serializeArray();

        data_send.push({
            'name': 'transaccion_id',
            'value': transaccion_id
        });

        data_send.push({
            'name': 'poliza_id',
            'value': id_poliza_act
        });

        $.ajax({
            url: PATH + '/polizas/xml_api/crear_sub_transacccion',
            type: 'POST',
            data: data_send,
            success: function (response) {

                var transaccion_id = response.data.transaccion_id;


                window.location.href = PATH + "cheques/nuevo?transaccion=" + transaccion_id + '&poliza=' +
                    id_poliza_act;

                // if (response.estatus != "error") {
                //     Swal.fire({
                //         icon: 'success',
                //         title: 'Éxito!',
                //         text: response.mensaje,
                //     }).then((result) => {
                //         $("body").LoadingOverlay("show");
                //         setTimeout(function () {
                //             location.reload();
                //         }, 250)
                //     });

                // } else {
                //     Swal.fire({
                //         icon: 'error',
                //         title: 'Error',
                //         text: response.mensaje,
                //     })
                // }
            },
        });

        
    }
</script>

<div class="row">
    <div class="col-xl-11 col-md-12 mx-auto">
        <div class="card mt-3">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <div class="row mt-2">
                            <div class=" col-sm-7">
                                <fieldset class="border p-2">
                                <legend class="float-none w-auto p-2">Factura</legend>
                                <p class="text-justify"><?php echo is_array($poliza_datos) && array_key_exists('descripcion',$poliza_datos)? $poliza_datos['descripcion'] : ""; ?></p>
                                </fieldset>
                            </div>
                            <div class=" col-sm-5">
                                <?php if($poliza_datos['id_estatus_poliza_xml'] <= 3){ ?>
                                    <?php if(is_array($tabla) && count($tabla)>0){ ?>

                                        <button type="button" onclick="app.crear_cheque()" 
                                    class="btn btn-warning ml-2 my-2 my-sm-0 float-right"><i
                                        class="fas fa-money-bill"></i> Crear cheque</button>

                                    <button class="btn btn-secondary ml-2 my-2 my-sm-0 float-right"
                                        onclick="app.finalizar_parte_poliza();" type="button"><i class="fas fa-save"></i> Finalizar
                                        poliza</button>
                                    <?php } ?>

                                    

                                <button title="Agregar" class="btn btn-success ml-2 my-2 my-sm-0 float-right"
                                    onclick="agregar_xml();" type="button"><i class="fas fa-plus"></i> Agregar
                                    comprobante</button>
                                <?php }else{ 
                            if(is_array($poliza_datos) && array_key_exists('id',$poliza_datos)){ ?>
                                <!-- <div class="alert alert-info" role="alert">
                                La poliza ya fue finalizada
                            </div> -->
                                <?php if(is_array($datos_cheque) && array_key_exists('id',$datos_cheque) ){ ?>
                                <!-- <a class="btn btn-warning ml-2 my-2 my-sm-0 float-right" target="_blank" href="<?php echo site_url('cheques/imprimir/index?id='.$datos_cheque['id'] ); ?>"><i class="fas fa-money-bill"></i> Ver Cheque</a> -->
                                <button type="button" onclick="app.crear_cheque()" 
                                    class="btn btn-warning ml-2 my-2 my-sm-0 float-right"><i
                                        class="fas fa-money-bill"></i> Crear cheque</button>
                                <?php }else{ ?>
                                <button type="button" onclick="app.crear_cheque()" 
                                    class="btn btn-warning ml-2 my-2 my-sm-0 float-right"><i
                                        class="fas fa-money-bill"></i> Crear cheque</button>
                                <?php } ?>

                                <?php } 
                             } ?>
                            </div>
                        </div>
                        



                    </div>
                    <div class="col-sm-12">
                        <hr />
                    </div>
                </div>
                <div class="row">
                    <div class=" col-sm-12">
                        <?php if($acumulado_abono <> $acumulado_cargo){ ?>
                        <?php if($transaccion_datos['estatus_id'] == 'ACTIVO'){ ?>
                        <div class="alert alert-danger" role="alert">
                            Los montos de cargos y abonos no coinciden
                        </div>
                        <?php } ?>
                        <?php } ?>

                        <?php if(is_array($tabla) && count($tabla)>0){ ?>
                        <form method="post" id="opciones_tbls">
                            <?php foreach($tabla as $tbl){?>


                            <div class="table-responsive mt-3">
                                <?php echo $tbl; ?>
                            </div>
                            <?php } ?>
                        </form>
                        <?php }else{ echo '<center><h3>Favor de agregar un comprobante</h3></center>'; } ?>
                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                </table> -->
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-sm-10 mx-auto">
                        <hr/>
                        <fieldset class="border p-2">
                        <legend class="float-none w-auto p-2">Ayuda</legend>
                        <p class="text-justify small">
                            Pasos poder crear un cheque:
                            <ul style="list-style-type: decimal;" class=" small">
                                <li>Presionar el boton verde de añadir comprobante</li>
                                <li>En la ventana añadir los datos requeridos y presionar el boton de guardar</li>
                                <li>Editar si se requiere los asientos del o los comprobantes</li>
                                <li>Presionar el boton gris de finalizar poliza</li>
                                <li>En la ventana apareceran los comprobantes agregados, seleccionar una o mas comprobantes</li>
                                <li>Presionar el boton azul para finalizar (en este paso lo asientos se aplicaran en la contabilidad general)</li>
                                <li>Presionar el boton amarillo de crear cheque</li>
                                <li>En la ventana aparecen los comprobantes finalizados, seleccionar una o mas comprobantes los que se usaran para crear los asientos del cheque</li>
                                <li>Presionar el boton azul para finalizar (en este paso se procedera a crear el cheque apartir de los comprobantes anteriormente seleccionados)</li>
                            </ul>
                        </p>
                        <p class="text-justify small">
                            Para ver el o los cheques creados presionar el boton amarillo de Cheque y su folio correspondiente, este se encuentra en dentro del listado de comprobantes a un lado de los botones pdf y xml
                        </p>
                        </fieldset>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<script>
    function ver_polizas_fijas(valor) {
        console.log(valor);
        if (valor == 'DO') {
            $('div#id_poliza_fija_div').show();
        } else {
            $('div#id_poliza_fija_div').hide();
        }

    }
</script>

<div id="contenedor_modal" style="display:block"></div>

<div class="modal fade " id="modal_crear_poliza" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Poliza</h5>
            </div>
            <div class="modal-body">


                <form class="form-inlines" id="polizas_fijas_crear" role="form">
                    <div class="form-group">
                        <label for="archivo_xml"><small class="text-danger">*</small> Poliza</label>
                        <div class="row">
                            <div class="col-sm-12">

                                <select onchange="ver_polizas_fijas(this.value);" class="form-control" name="id_poliza">
                                    <?php if(is_array($polizas) && count($polizas)>0){ ?>
                                    <option value="">Selecione una opción</option>
                                    <?php foreach($polizas as $poli){ ?>
                                    <option value="<?php echo $poli['id'] ?>">
                                        <?php echo '['.utils::folio($poli['id'],2).'] '.$poli['descripcion'] ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                <small id="msg_id_poliza" class="form-text text-danger"></small>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-12 " id="id_poliza_fija_div" style="display:block;">
                                <label for="archivo_xml"><small class="text-danger">*</small>Tipo Poliza Diario</label>
                                <select class="form-control" name="id_poliza_fija" id="id_poliza_fija">
                                    <?php if(is_array($polizas_fijas) && count($polizas_fijas)>0){ ?>
                                    <option value="">Selecione una opción</option>
                                    <?php foreach($polizas_fijas as $poliza_fija){ ?>
                                    <option value="<?php echo $poliza_fija['id'] ?>">
                                        <?php echo '['.utils::folio($poliza_fija['id'],2).'] '.$poliza_fija['poliza'] ?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                <small id="msg_id_poliza_fija" class="form-text text-danger"></small>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-sm-12" id="" style="display:block;">
                                <label for="archivo_xml"><small class="text-danger">*</small> Proveedor</label>
                                <select class="form-control" name="id_persona" id="id_persona">
                                    <?php if(is_array($proveedores) && count($proveedores)>0){  ?>
                                    <option value="">Selecione una opción</option>
                                    <?php foreach($proveedores as $proveedor){ ?>
                                    <option value="<?php echo $proveedor['persona_id'] ?>">
                                        <?php echo '['.$proveedor['rfc'].'] '.trim($proveedor['nombre'].' '.$proveedor['apellido1'].' '.$proveedor['apellido2']) ?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                <small id="msg_id_poliza_fija" class="form-text text-danger"></small>
                            </div>
                        </div>


                    </div>
                </form>


            </div>
            <div class="modal-footer">
                <a href="<?php echo site_url('polizas/xml'); ?>" class="btn btn-secondary">Cerrar</a>
                <button type="button" class="btn btn-primary" onclick="crear_poliza()">Guardar</button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_agregar_asiento" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="modal_agregar_asientoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_agregar_asientoLabel">Agregar XML</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="modal_agregar_asiento_button" class="btn btn-primary" onclick="modal_agregar_asiento_guardar();"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<?php $this->carabiner->display('select2') ?>
<?php $this->carabiner->display('sweetalert2'); ?>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha512-7VTiy9AhpazBeKQAlhaLRUk+kAMAb8oczljuyJHPsVPWox/QIXDFOnT9DUk1UC8EbnHKRdQowT7sOBe7LAjajQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-file-upload/4.0.11/jquery.uploadfile.min.js"
    integrity="sha512-uwNlWrX8+f31dKuSezJIHdwlROJWNkP6URRf+FSWkxSgrGRuiAreWzJLA2IpyRH9lN2H67IP5H4CxBcAshYGNw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo base_url('assets/js/scripts/polizas/xml/create.js'); ?>"></script>

<script type="text/javascript">
    function crear_poliza() {

        var data_send = $('form#polizas_fijas_crear').serializeArray();

        $.ajax({
            url: "<?php echo site_url('polizas/xml_api/crear_poliza'); ?>",
            dataType: "json",
            data: data_send,
            cache: false,
            type: "POST",

            success: function (data) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        // $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            window.location.href = PATH + '/polizas/xml/create?id=' + btoa(
                                data.data.poliza_id);
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function (index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }

            },
            error: function (xhr) {

            }
        });
    }

    $(document).ready(function () {


        if (id_poliza_act == false) {
            $('div#modal_crear_poliza').modal({
                show: true,
                keyboard: false,
                backdrop: 'static'
            });

            $('select[name=id_poliza] option[value=DO]').prop('selected', true);
        }


    });

    function aplicar_transaccion() {
        $.ajax({
            type: 'post',
            url: PATH + '/polizas/xml_api/aplicar_transacciones',
            data: {
                'transaccion_id': transaccion_id,
                'estatus': 'APLICADO',
                'id_poliza_xml': id_poliza_act
            },
            success: function (data, status, xhr) {
                // $('form#form_busqueda').submit();
                window.location.reload();
                // window.location.href = PATH+"/polizas_fijas/finalizar/"+TRANSACCION_ID;

            },
        });
    }

    function modal_editar_asiento_guardar() {
        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "polizas_id",
            value: id_poliza_act
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "renglon_id",
            value: renglon_id
        });

        $('small.form-text.text-danger').html('');
        $.ajax({
            type: 'post',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: data_send,
            success: function (data, status, xhr) {
                if (data.status != "error") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function (index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
        });
    }

    function modal_agregar_asiento_guardar() {

        var data_send = $('form#data_form_modal').serializeArray();
        data_send.push({
            name: "id_poliza_act",
            value: id_poliza_act
        });
        data_send.push({
            name: "transaccion_id",
            value: transaccion_id
        });
        data_send.push({
            name: "id_poliza_gen",
            value: id_poliza_gen
        });
        data_send.push({
            name: "id_poliza_xml_detalle",
            value: id_poliza_xml_detalle
        });

        $('small.form-text.text-danger').html('')

        $.ajax({
            url: "<?php echo site_url('polizas/xml_api/GuardarXml'); ?>",
            dataType: "json",
            data: data_send,
            cache: false,
            type: "POST",

            success: function (data) {
                //window.location.href = PATH+'/polizas/xml/create?id='+btoa(id_poliza_act);
                if (data.status != "error") {
                    // location.reload();
                    // return;
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: 'El registro se ha guardado correctamente',
                    }).then((result) => {
                        $('div#modal_agregar_asiento').modal('hide');
                        $("body").LoadingOverlay("show");
                        setTimeout(function () {
                            location.reload();
                        }, 250)

                        // var table = $('table#tabla_listado').DataTable();
                        // table.ajax.reload( null, false );

                    });
                } else {
                    $.each(data.message, function (index, value) {
                        if ($("small#msg_" + index).length) {
                            $("small#msg_" + index).html(value);
                        }
                    });
                }
            },
            error: function (xhr) {

            }
        });
    }

    function formatState(data) {
        var element = data.element;
        var html = '<small><b>' + data.text + '</b></small>';
        html += '<br/><span>' + $(element).attr('desc') + '</span>';
        return $(html);
    }

    var renglon_id;

    function modal_editar_asiento(_this) {

        renglon_id = $(_this).data('renglon_id');

        $.ajax({
            type: 'get',
            url: PATH + '/cheques/api/modal_editar_asiento.json',
            data: {
                renglon_id: $(_this).data('renglon_id'),
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();


                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Editar asiento');
                    $('#modal_agregar_asiento_button').attr('onclick',
                        'modal_editar_asiento_guardar();');

                    $('select#modal_cuentas').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento'),
                        templateResult: formatState
                    });

                    $('select#modal_tipo').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('div#modal_agregar_asiento')
                    });

                    try {
                        $("div#modal_agregar_asiento select#modal_cuentas option[value=" + data.data
                            .asiento.cuenta + "]").prop('selected', true);
                        $("div#modal_agregar_asiento select#modal_cuentas").trigger('change');
                    } catch (error) {

                    }

                    try {
                        console.log(data);
                        if (data.data.asiento.cargo > 0) {
                            $("div#modal_agregar_asiento select#modal_tipo option[value=1]").prop(
                                'selected', true);
                        } else {
                            $("div#modal_agregar_asiento select#modal_tipo option[value=2]").prop(
                                'selected', true);
                        }
                        $("div#modal_agregar_asiento select#modal_tipo").trigger('change');
                    } catch (error) {}

                    try {
                        if (data.data.asiento.cargo > 0) {
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento
                            .cargo);
                        } else {
                            $("div#modal_agregar_asiento input#precio").val(data.data.asiento
                            .abono);
                        }
                    } catch (error) {

                    }

                    try {
                        $("div#modal_agregar_asiento  #concepto").val(data.data.asiento.concepto);
                    } catch (error) {

                    }


                    // $('select#tipo_unidad').select2({
                    //     theme: 'bootstrap4',
                    //     dropdownParent: $('div#modal_agregar_asiento'),
                    //     templateResult: APP.formatState
                    // });
                    // $('select#tipo_unidad option[value=act]').prop('selected', true);
                    // $("select#tipo_unidad").trigger('change');

                }, 250);
            },
        });
    }

    function eliminar_renglon(_this) {
        Swal.fire({
            title: 'Desea eliminar el asiento?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            denyButtonText: 'Cancelar',
        }).then((result) => {
            console.log(result);
            if (result.value == true) {
                $.ajax({
                    url: PATH + '/asientos/api/aplicar_asiento',
                    type: 'POST',
                    data: {
                        asiento_id: $(_this).data('renglon_id'),
                        estatus: "ANULADO"
                    },
                    success: function (response) {
                        if (response.estatus != "error") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito!',
                                text: response.mensaje,
                            }).then((result) => {
                                $("body").LoadingOverlay("show");
                                setTimeout(function () {
                                    location.reload();
                                }, 250)
                            });

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    },
                });
            } else {
                return false;
            }
        })
    }


    function administrar_asientos($id, $poliza_id) {
        // var $id = $($this).data('renglon_id');
        $("body").LoadingOverlay("show");
        setTimeout(function () {
            window.location.replace(PATH + "/cheques/getId_transaccion?transaccion=" + $id + '&poliza=' +
                $poliza_id);
        }, 500)
    }

    function agregar_xml() {

        $.ajax({
            type: 'get',
            url: PATH + '/polizas/xml_api/modal_agregar_asiento.json',
            data: {
                transaccion_id: transaccion_id
            },
            success: function (data, status, xhr) {
                $('div#modal_agregar_asiento div.modal-body').html(atob(data.data.html));
                $('div#modal_agregar_asiento').modal();
                setTimeout(() => {

                    $('#modal_agregar_asientoLabel').html('Agregar comprobante');
                    $('#modal_agregar_asiento_button').attr('onclick',
                        'modal_agregar_asiento_guardar();');


                    $("div#contenido_xml div#seleccionar_archivo").uploadFile({
                        url: '<?php echo site_url('polizas/xml_api/ProcesarXml?id_poliza_act='.(is_array($poliza_datos) && array_key_exists('id ',$poliza_datos)? $poliza_datos['id'] : false) ); ?>',
                        uploadStr :"<i class='fa fa-paperclip' aria-hidden='true'></i> Seleccionar Archivo",
                        allowedTypes: "*",
                        dragDrop: false,
                        fileName: "myfile",
                        returnType: "json",
                        showDelete: true,
                        showDone: false,
                        maxFileSize: 10485760,
                        acceptFiles: "application/xml",
                        onLoad: function (obj) {
                            var button_file = $('div#contenido_xml  .ajax-file-upload');
                            button_file.addClass('btn btn-primary');
                            button_file.removeClass('ajax-file-upload');
                            button_file.prop('id', 'singleupload1_content');
                            button_file.css({
                                'padding': '6px 12px',
                                'margin-top': '3px',
                                'width': '100%'
                            });
                        },
                        onSuccess: function (files, data, xhr) {
                            if (data.nombre != null) {

                                $('input#subtotal').val('');
                                $('input#iva').val('');
                                $('input#total').val('');

                                $('div#contenido_xml #singleupload1_content').hide();
                                $('div#contenido_xml #singleupload1_progress').hide();
                                $('div#contenido_xml #singleupload1_finaly').show();
                                $('div#contenido_xml #nombre_archivo').val(files);
                                $('div#contenido_xml #eliminar_archivo').attr('data',
                                    data.nombre);
                                $('div#contenido_xml #id_archivo_modal').val(data
                                    .nombre);
                                $('div#contenido_xml #id_original_modal').val(data
                                    .original);

                                $.ajax({
                                    url: "<?php echo site_url('polizas/xml_api/VerXml'); ?>",
                                    dataType: "json",
                                    data: {
                                        id_poliza_act: "<?php echo (is_array($poliza_datos) && array_key_exists('id',$poliza_datos)? $poliza_datos['id'] : false); ?>",
                                        transaccion_id: "<?php echo (is_array($poliza_datos) && array_key_exists('transaccion_id',$poliza_datos)? $poliza_datos['transaccion_id'] : false); ?>",
                                        archivo: data.original
                                        //id_poliza_xml_detalle
                                    },
                                    cache: false,
                                    type: "POST",

                                    success: function (response) {

                                        id_poliza_xml_detalle = response
                                            .detalle;
                                        id_poliza_act = response.catalogo;
                                        $('input#subtotal').val(response
                                            .subtotal);
                                        $('input#iva').val(response.iva);
                                        $('input#total').val(response
                                        .total);
                                    },
                                    error: function (xhr) {

                                    }
                                });

                            } else {
                                $('div#contenido_xml #singleupload1_content').show();
                                $('div#contenido_xml #singleupload1_progress').hide();
                                $('div#contenido_xml #singleupload1_finaly').hide();
                                $('div#contenido_xml small#msg_id_archivo_modal').html(
                                    "El archivo se ha seleccionado correctamente");
                            }

                        }
                    });


                    $("div#contenido_pdf div#seleccionar_archivo").uploadFile({
                        url: '<?php echo site_url('polizas/xml_api/ProcesarPdf?id_poliza_act='.(is_array($poliza_datos) && array_key_exists('id',$poliza_datos)? $poliza_datos['id'] : false) ); ?>',
                        uploadStr :"<i class='fa fa-paperclip' aria-hidden='true'></i> Seleccionar Archivo",
                        allowedTypes: "*",
                        dragDrop: false,
                        fileName: "myfile",
                        returnType: "json",
                        showDelete: true,
                        showDone: false,
                        maxFileSize: 10485760,
                        acceptFiles: "application/pdf",
                        onLoad: function (obj) {
                            var button_file = $('div#contenido_pdf .ajax-file-upload');
                            button_file.addClass('btn btn-primary');
                            button_file.removeClass('ajax-file-upload');
                            button_file.prop('id', 'singleupload1_content2');
                            button_file.css({
                                'padding': '6px 12px',
                                'margin-top': '3px',
                                'width': '100%'
                            });
                        },
                        onSuccess: function (files, data, xhr) {
                            if (data.nombre != null) {

                                $('div#contenido_pdf #singleupload1_content').hide();
                                $('div#contenido_pdf #singleupload1_progress').hide();
                                $('div#contenido_pdf #singleupload1_finaly').show();
                                $('div#contenido_pdf #nombre_archivo2').val(files);
                                $('div#contenido_pdf #eliminar_archivo2').attr('data',
                                    data.nombre);
                                $('div#contenido_pdf #id_archivo_modal2').val(data
                                    .nombre);
                                $('div#contenido_pdf #id_original_modal2').val(data
                                    .original);

                            } else {
                                $('div#contenido_pdf #singleupload1_content').show();
                                $('div#contenido_pdf #singleupload1_progress').hide();
                                $('div#contenido_pdf #singleupload1_finaly').hide();
                                $('div#contenido_pdf small#msg_id_archivo_modal').html(
                                    "El archivo se ha seleccionado correctamente");
                            }

                        }
                    });



                }, 250);
            },
        });
    }
</script>


@endsection

@section('style')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha512-gOQQLjHRpD3/SEOtalVq50iDn4opLVup2TF8c4QPI3/NmUPNZOk2FG0ihi8oCU/qYEsw4P6nuEZT2lAG0UNYaw==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-file-upload/4.0.11/uploadfile.min.css"
    integrity="sha512-MudWpfaBG6v3qaF+T8kMjKJ1Qg8ZMzoPsT5yWujVfvIgYo2xgT1CvZq+r3Ks2kiUKcpo6/EUMyIUhb3ay9lG7A=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    #customFile .custom-file-input:lang(en)::after {
        content: "Seleccionar un archivo...";
    }

    #customFile .custom-file-input:lang(en)::before {
        content: "Click me";
    }

    /*when a value is selected, this class removes the content */
    .custom-file-input.selected:lang(en)::after {
        content: "" !important;
    }

    .custom-file {
        overflow: hidden;
    }

    .custom-file-input {
        white-space: nowrap;
    }

    .ajax-file-upload-container {
        display: none;
    }
</style>

@endsection