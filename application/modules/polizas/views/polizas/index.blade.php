@layout('template_blade/estructura')
@section('style')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/datatables.min.css') }}" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('contenido')
<h1 class="h3 mb-4 text-gray-800">Polizas</h1>
<div class="card">
  <div class="card-body"><!--
    <form action="<?php echo base_url(); ?>index.php/polizas/index" method="get" id="consultar">
            <div class="row">
            
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Fecha</label>
                        <input type="date" class="form-control" name="fecha" min="2021-01-01" value="<?php echo $fecha; ?>" max="<?php echo utils::get_date(); ?>">
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class=" col-sm-12">
                <button onclick="APP.buscar_tabla();" class="btn btn-primary ml-2 my-2 my-sm-0 float-right" 
                        type="submit"><i class="fas fa-search"></i> Consultar</button>
                <a href="<?php echo site_url('polizas'); ?>"class="btn btn-outline-secondary ml-2 my-2 my-sm-0 float-right" 
                        type="submit"> Limpiar</a>
                    
                </div>
            </div>
        </form>
        
        <hr />-->
        <div class="row">
            <!--<div class=" col-sm-12 mt-1 mb-1">
                <a href="<?php echo site_url('polizas/crear') ?>" class="btn btn-success ml-2 my-2 my-sm-0 float-right" 
                    type="buttton"><i class="fas fa-plus"></i> Crear Poliza</a>
            </div>-->
            <div class=" col-sm-12">
                <?php echo $tabla; ?>
            </div>
        </div>

    
    <!-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Poliza</th>
                <th>Concepto</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Fecha</th>
                <th>Poliza</th>
                <th>Concepto</th>
                <th>Opciones</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($lista as $row) : ?>
                <tr>
                    <th><?php echo $row->fecha; ?></th>
                    <th><?php echo $row->poliza; ?></th>
                    <th><?php echo $row->concepto; ?></th>
                    <th>
                        <a href="<?php echo base_url(); ?>index.php/asientos/ver/<?php echo $row->id_id; ?>">
                            <button type="button" class="btn btn-primary">Ver</button>
                        </a>
                    </th>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table> -->
    </div>
</div>
@endsection


@section('style')

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
        text-align: center;
    }
</style>
@endsection

@section('script')
<script src="{{ base_url('assets/components/DataTables/datatables.min.js') }}" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/polizas/polizas/index.js'); ?>"></script>
@endsection