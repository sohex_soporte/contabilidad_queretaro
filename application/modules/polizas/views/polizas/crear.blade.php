@layout('template_blade/estructura')
@section('style')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/datatables.min.css') }}" crossorigin="anonymous"
    referrerpolicy="no-referrer" />
@endsection
@section('contenido')
<h1 class="h3 mb-4 text-gray-800">Crear Polizas</h1>
<div class="card">
    <div class="card-body">
        <form action="<?php echo base_url(); ?>index.php/polizas/lista_polizas" method="get" id="consultar">
        <input type="hidden" name="sucursal_id" value="3" />
            <div class="row">
                <div class="col-md-8">
                    <label for="clave_poliza">Nomenclatura de la poliza:</label>
                    <select class="form-control" id="clave_poliza" name="clave_poliza">
                        <option value=" ">Seleccione una opción</option>
                        <?php if(is_array($nomenclaturas)){ ?>
                            <?php foreach ($nomenclaturas as $key => $value) { ?>
                                <option value="<?php echo $value['id'] ?>"><?php echo '['.$value['id'].'] '. $value['descripcion'] ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <small id="msg_clave_poliza" class="form-text text-danger"></small>
                </div>

                <div class="col-md-8">
                    <label>Fecha:</label>
                    <input type="date" class="form-control" id="fecha" name="fecha" max="<?php echo date("Y-m-d"); ?>" />
                    <small id="msg_fecha" class="form-text text-danger"></small>
                </div>
                
                <div class="col-md-8">
                    <hr/>
                    <button type="button" onclick="app.guardar();" class="btn btn-primary float-right" >Guardar</button>
                    <a type="button" href="<?php echo site_url('polizas'); ?>" class="btn btn-outline-danger float-right mr-2" >Cancelar</a>
                </div>

            </div>
        </form>
    </div>
</div>

<div id="contenedor_modal" style="display:none;"></div>

@endsection



@section('style')
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
@endsection

@section('script')
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="<?php echo base_url('assets/js/scripts/polizas/polizas/crear.js'); ?>"></script>
<script>
    
</script>

@endsection