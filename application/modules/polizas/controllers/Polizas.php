<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Polizas extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

        $this->session->set_userdata('poliza','normal');
       
    }

    public function index(){

        $fecha = date('Y-m-d');$this->input->get('fecha');

    //     $SQL = "SELECT DISTINCT
    //     id_poliza,
    //     id_transaccion,
    //     descripcion,
    //     fecha_creacion,
    //     folio,
    //     fecha_movimiento,
    //     PolizaNomenclatura_id,
    //     dia,
    //     origen,
    //     concepto 
    // FROM
    //     (
    //     SELECT
    //         id_poliza,
    //         id_transaccion,
    //         descripcion,
    //         fecha_creacion,
    //         folio,
    //         fecha_movimiento,
    //         PolizaNomenclatura_id,
    //         dia,
    //         origen,
    //         concepto 
    //     FROM
    //         vw_listado_modulo_polizas_cajas UNION ALL
    //     SELECT
    //         id_poliza,
    //         id_transaccion,
    //         descripcion,
    //         fecha_creacion,
    //         folio,
    //         fecha_movimiento,
    //         PolizaNomenclatura_id,
    //         dia,
    //         origen,
    //         concepto 
    //     FROM
    //     vw_listado_modulo_polizas_otros 
    //     ) AS tbl";
        
        
    //     // return $query->result_array();

    //     // $this->db->from('vw_listado_modulo_polizas');
    //     if($fecha != false){
    //         $SQL = $SQL ." where tbl.fecha_creacion = ".$this->db->escape($fecha);
    //         $SQL = $SQL ." order by tbl.fecha_creacion desc ";

    //     }else{
    //         $SQL = $SQL ." order by tbl.fecha_creacion desc ";
    //         $SQL = $SQL ." limit 100";
    //     }

       
        // $query = $this->db->get();
        //utils::pre($SQL);
        // $query = $this->db->query($SQL);
        // $response = $query->result_array();
        $this->load->model('CaPolizasNomenclaturas_model');
        $response = $this->CaPolizasNomenclaturas_model->getAll();
        
        $this->load->library('table');
        $template = array(
            'table_open'            => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $header = [
            'Clave',
            'Descripción',
            'Acciones'
        ];

        $this->table->set_heading($header);

       
        if (is_array($response) && count($response) > 0) {
            foreach ($response as $key => $value) {
                $botones = '<a title="Editar" type="button" href="' . site_url('polizas/consultar/'.$value['id']) . '" class="btn btn-primary ml-2 btn-sm" onclick="APP.modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></a>';

                $contenido = array(
                    $value['id'],
                    $value['descripcion'],
                    $botones
                );
                $this->table->add_row($contenido);
            }

        } else {
            $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 4);
            $this->table->add_row($cell);
        }

        $data_content = [
            'fecha' => $fecha,
            'tabla' => $this->table->generate()
        ];



        $this->blade->render('/polizas/index',$data_content);
    }

    // public function guarda(){

    //     $this->form_validation->set_rules('fecha', 'fecha', 'required');
    //     $this->form_validation->set_rules('poliza', 'poliza', 'required');
    //     $this->form_validation->set_rules('concepto', 'concepto', 'required');
  
    //     $response = validate($this);
  
  
    //     if($response['status']){
    //       $data['fecha'] = $this->input->post('fecha');
    //       $data['poliza'] = $this->input->post('poliza');
    //       $data['concepto'] = $this->input->post('concepto');
    //       $data['fecha_creacion'] = date('Y-m-d H:i:s');
    //       $data['id_id'] = get_guid();
    //       $data['folio'] = "0";
    //       $data['cargo'] = "0";
    //       $data['abono'] = "0";
  
    //       $this->Mgeneral->save_register('polizas', $data);
    //     }
    //   //  echo $response;
    //   echo json_encode(array('output' => $response));
    // }

    public function crear(){

        $this->load->model('CaPolizasNomenclaturas_model');
        $nomenclaturas = $this->CaPolizasNomenclaturas_model->getAll();

        $content = array(
            'nomenclaturas' => $nomenclaturas
        );

        $this->blade->render('/polizas/crear',$content);
    }


    public function crear_poliza_post($poliza_Cve,$fecha)
    {

        $sucursal_id = 3;//$this->input->post('sucursal_id');
        $dia = utils::get_dia($fecha);

        #CREACION DE POLIZA
        $this->load->model('CaPolizas_model');
        $folio_poliza = false;
        $polizas_datos = $this->CaPolizas_model->get(array(
            'PolizaNomenclatura_id' => $poliza_Cve,
            'ejercicio' => utils::get_anio($fecha),
            'mes' => utils::get_mes($fecha),
            'dia' => utils::get_dia($fecha)
        ));
        if ($polizas_datos == false) {
            $clave_poliza = $poliza_Cve;
            $dia = utils::get_dia($fecha);
            $poliza_id = $this->CaPolizas_model->insert(array(
                'PolizaNomenclatura_id' => $poliza_Cve,
                'ejercicio' => utils::get_anio($fecha),
                'mes' => utils::get_mes($fecha),
                'dia' => $dia,
                'fecha_creacion' => $fecha
            ));
            
            $folio_poliza = $clave_poliza . $dia;
        } else {
            $poliza_id = $polizas_datos['id'];
            $folio_poliza = $polizas_datos['PolizaNomenclatura_id'] . $polizas_datos['dia'];
        }

        #VERIFICA SI EXISTE UN MOVIMIENTO
        $this->load->model('CaTransacciones_model');
        $transaccion_datos = $this->CaTransacciones_model->get(array(
            'folio' => $poliza_Cve.utils::folio($dia,3),
            'fecha' => $fecha,
            'origen' => 'POLIZAS',
            'sucursal_id' => $sucursal_id
        ));

        if ($transaccion_datos == false) {
            $transaccion_id = $this->CaTransacciones_model->insert(
                array(
                    'folio' => $poliza_Cve.utils::folio($dia,3),
                    'origen' => 'POLIZAS',
                    'fecha' => $fecha,
                    'sucursal_id' => $sucursal_id
                )
            );
             
        } else {
            $transaccion_id = $transaccion_datos['id'];
        }

        $this->load->model('ReTransaccionesPolizas_model');
        $datos_rel = $this->ReTransaccionesPolizas_model->get([
            'transaccion_id' => $transaccion_id,
            'poliza_id' => $poliza_id
        ]);
        if ($datos_rel == false) {
            $rel_id = $this->ReTransaccionesPolizas_model->insert(
                array(
                    'transaccion_id' => $transaccion_id,
                    'poliza_id' => $poliza_id
                )
            );
        } else {
            $rel_id = $datos_rel['id'];
        }

        $respuesta = array(
            'transaccion_id' => $transaccion_id,
            'poliza_id' => $poliza_id,
            'folio_poliza' => $folio_poliza,
            'rel_id' => $rel_id
        );
        return $respuesta;
    }

    public function consultar($poliza_cve){
        

        $fecha = ($this->input->get('fecha') != false)? $this->input->get('fecha') : date('Y-m-d');

        $datos_general = $this->crear_poliza_post($poliza_cve,$fecha);
        $transaccion_id = $datos_general['transaccion_id'];

        $poliza_id = $datos_general['poliza_id'];

        $this->load->model('CaTransacciones_model');
        $datos_transaccion = $this->CaTransacciones_model->get([
            'id' => $transaccion_id
        ]);
        
        $this->load->model('CaPolizas_model');
        $datos_polizas = $this->CaPolizas_model->get([
            'id' => $poliza_id
        ]);
        
        $response = utils::api_get([
            'url' => site_url('asientos/api/detalle?nomenclatura=' . $poliza_cve . '&fecha='.$fecha.'&estatus=POLIZAS_FIJAS')
        ]);
        // $this->db->from('vw_polizas_asientos')
        //     //->where('fecha',$fecha)
        //     ->where('nomenclatura_id',$poliza_cve);
        //     $query = $this->db->get();
        // $response = $query->result_array();
        // utils::pre($response);
        
        $this->load->library('table');
        $template = array(
            'table_open'            => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $header = [
            'No. Asiento',
            'Folio',
            'Concepto',
            'Cuenta',
            'Cargo',
            'Abono',
            // 'Acumulado',
            // 'Fecha de registro'

        ];

        if($fecha != date('Y-m-d')){
            $datos_transaccion['estatus_id'] = 'CERRADA';
        }

        if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
            $header[] = 'Acciones';
        }

        $this->table->set_heading($header);

        $acumulado = 0;
        $acumulado_abono = 0;
        $acumulado_cargo = 0;
        $cont = 1;
        if (is_array($response) && array_key_exists('data', $response) && is_array($response['data']) && count($response['data']) > 0) {
            foreach ($response['data'] as $key => $value) {
                
                /**
                 * SE COMENTO || $value['transaccion_estatus_id'] != 'ACTIVO' POR QUE NO APARECIAN LOS ASIENTOS EN LAS POLIZAS QT DE CONTABILIDAD
                 */

                // if($value['transaccion_id'] == $transaccion_id ){ // || $value['transaccion_estatus_id'] != 'ACTIVO'
                    // $acumulado = $acumulado + ($value['abono'] - $value['cargo']);
                    $acumulado_abono = $acumulado_abono + $value['abono'];
                    $acumulado_cargo = $acumulado_cargo + $value['cargo'];

                    $cuenta_descripcion = '<span>'.$value['cuenta'].'<br/><small class="font-weight-bold">'.$value['cuenta_descripcion'].'</small></span>';

                    $contenido = array(
                        utils::folio( $value['asiento_id'], 6),
                        $value['folio'],
                        $value['concepto'],
                        $cuenta_descripcion,
                        utils::format($value['cargo']),
                        utils::format($value['abono']),
                        // utils::format($acumulado),
                    );

                    if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                        $botones = '<button title="Editar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-primary ml-2 btn-sm" onclick="APP.modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>' .
                            '<button title="Eliminar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-danger ml-2 btn-sm" onclick="APP.eliminar_renglon(this);" ><i class="far fa-trash-alt"></i></div>';
                        $contenido[] = (($value['transaccion_id'] == $transaccion_id)? $botones : '');
                    }

                    $this->table->add_row($contenido);
                    $cont++;
                // }
            }

            if (is_array($response) && array_key_exists('total', $response) && is_array($response['total']) && count($response['total']) > 0) {
                $cell = array('data' => 'Total', 'class' => '', 'colspan' => 4);

                if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                    $this->table->add_row($cell, utils::format($acumulado_cargo), utils::format($acumulado_abono), '');
                } else {
                    $this->table->add_row($cell, utils::format($acumulado_cargo), utils::format($acumulado_abono));
                }
            }
        } else {
            if ($datos_transaccion['estatus_id'] == 'ACTIVO') {
                $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 6);
            } else {
                $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 5);
            }
            $this->table->add_row($cell);
        }


        //utils::pre( $this->table->generate());

        $data_content = [
            //'poliza_id' => $poliza_id,
            'datos_polizas' => $datos_polizas,
            'datos_transaccion' => $datos_transaccion,
            'tabla' => $this->table->generate(),
            'acumulado_abono' => utils::format($acumulado_abono),
            'acumulado_cargo' => utils::format($acumulado_cargo),
            'poliza_cve' => $poliza_cve
        ];
        // utils::pre($data_content);

        $this->blade->render('/polizas/consultar3',$data_content);
       
        //$this->blade->render('consultar',$data_content);
        
    }

    
  
}