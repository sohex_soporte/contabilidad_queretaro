<?php defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    public $type = 'api';

    public $http_status = 200;
    public $data;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->data = array(
            'data' => false,
            'message' => false
        );
    }

    public function nomenclaturas_get()
    {
        // $this->load->model('CaPolizasNomenclaturas_mdl');
        // $this->data['data'] = CaPolizasNomenclaturas_mdl::all();
        // $this->response( $this->data , 201 );

        $this->load->model('CaPolizasNomenclaturas_model');
        $this->data['data'] =  $this->CaPolizasNomenclaturas_model->getAll();

        $this->response( $this->data );
        
    }



    public function polizas_get()
    {
        // $this->load->model('CaPolizasNomenclaturas_mdl');
        // $this->data['data'] = CaPolizasNomenclaturas_mdl::all();
        // $this->response( $this->data , 201 );

        $anio = $this->input->get('anio');
        $mes = $this->input->get('mes');
        $dia = $this->input->get('dia');
        $fecha = $this->input->get('fecha');

        $this->load->model('CaPolizas_model');

        $this->db->where('ca_polizas.PolizaNomenclatura_id !=', 'DO');
        $datos = $this->CaPolizas_model->getList([
            'ca_polizas.fecha_creacion' => $fecha
            // 'ejercicio' => $anio,
            // 'mes' => $mes,
            // 'dia' => $dia
        ]);

        $datos2 = $this->CaPolizas_model->getList_ext([
            'ca_polizas.fecha_creacion' => $fecha,
            'ca_polizas.PolizaNomenclatura_id' => 'DO'
            // 'ejercicio' => $anio,
            // 'mes' => $mes,
            // 'dia' => $dia
        ]);

        if(!(is_array($datos2) && count($datos2)>0)){
            $datos = [];
        }

        if(is_array($datos2) && count($datos2)>0){
            foreach ($datos2 as $key => $value) {
                $datos[] = $value;
            }
        }


         $this->data['data'] = $datos;
        $this->response( $this->data);
        
    }

    public function modal_agregar_asiento_post(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        
        // $this->form_validation->set_rules('precio_unitario', 'Precio Unitario', 'trim|required');
        // $this->form_validation->set_rules('cantidad', 'Cantidad', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');

        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');
        // $this->form_validation->set_rules('tipo_unidad', 'Tipo de unidad', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');

            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->insert([
                'polizas_id' => $this->input->post('polizas_id'),
                'estatus_id' => 'POR_APLICAR',
                'transaccion_id' => $this->input->post('transaccion_id'),
                'tipo_pago_id' => 'OTRO',
                'cuenta' => $cuenta_id,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                // 'unidad_id' => $this->input->post('tipo_unidad'),
                'departamento_id' => 9
            ]);

            // if($asiento_id != false){
            //     $this->load->model('MaBalanza_model');
            //     $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);
            //     if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
            //         $this->Asientos_model->update(array(
            //             'estatus_id' => 'APLICADO',
            //             'fecha_asiento_aplicado' => utils::get_datetime()
            //         ), array(
            //             'id' => $asiento_id
            //         ));
            //         $balanza_id = $balaza['balanza_id'];
            //     }
            // }

            $this->data['data'] = [
                'asiento_id' => $asiento_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }
}