<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

/**
 *
 * Controller Xmls
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Xml extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->session->set_userdata('poliza','xml');
  }

  public function index()
  {


    $this->load->library('table');
    $template = array(
        'table_open'            => '<table class="table table-bordered ">',
    );
    $this->table->set_template($template);
    $header = [
        'Poliza',
        'Descripción',
        'Nombre/Razon social',
        'Fecha',
        'Total',
        // 'Total de abonos',
        'Estatus',
        'Acciones',
    ];
    $this->table->set_heading($header);
    $this->db->where('id_estatus_poliza_xml >= 2',null,false);
    $this->db->order_by('created_at','desc');
    $query = $this->db->get('vw_pollizas_xml_general');
    $response = $query->result_array();

    // utils::pre($response);

    $tabla = '';

    if (is_array($response) && count($response) > 0) {
        foreach ($response as $key => $value) {
          // dd($value);
          $botones_acciones = '<a title="Editar" type="button" data-renglon_id="' . $value['id_poliza'] . '" class="btn btn-primary m-1 btn-sm" href="'.site_url('polizas/xml/create?id='.base64_encode($value['id_poliza']) ).'" ><i class="far fa-edit"></i></a>'
          .'<button title="Ver archivos" type="button" data-renglon_id="' . $value['id_poliza'] . '" class="btn btn-secondary m-1 btn-sm" onclick="modal_documentacion(this);"  ><i class="fas fa-folder-open"></i></button>';

          $clase = '';
          switch ($value['id_estatus_poliza_xml']) {
            case 4:
              $clase = 'table-info';
              break;
            
            case 5:
              $clase = 'table-warning';
              break;
            case 6:
              $clase = 'table-success';
              break;
            case 7:
              $clase = 'table-secondary';
              break;
          }

          $cell_1 = array('data' => '<p class="text-uppercase"><b>['.$value['id_polizas_nomenclatura'].$value['folio_poliza'].']</b> '.$value['polizas_nomenclatura'].'</p>', 'class' => $clase);
          $cell_1a = array('data' => '<p class="text-justify">'.(( strlen(trim($value['poliza_xml_descripcion']))>0 )? $value['poliza_xml_descripcion'] : '')   .'</p>', 'class' => $clase);
          $cell_1b = array('data' => '<small>'.$value['persona_rfc'].'</small><br/><small class="font-weight-bold">'.$value['persona_nombre'].' '.$value['persona_apellido1'].' '.$value['persona_apellido2'].'</small>', 'class' => $clase);
          $cell_2 = array('data' => '<center>'.utils::aHora($value['created_at']).'<br/><small class="font-weight-bold">'. utils::aFecha( $value['created_at'],true).'</small></center>', 'class' => $clase);
        //   $cell_3 = array('data' => utils::format( $value['cargos']), 'class' => $clase.' text-right');
          $cell_4 = array('data' => utils::format( $value['total']), 'class' => $clase.' text-right');
          $cell_5 = array('data' => $value['estatus_poliza_xml_cont'], 'class' => $clase);
          $cell_6 = array('data' => $botones_acciones, 'class' => $clase);
          

            $contenido = array(
              $cell_1, 
              $cell_1a, 
              $cell_1b, 
              $cell_2,
            //   $cell_3,
              $cell_4,
              $cell_5,
              $cell_6
            );
            $this->table->add_row($contenido);
        }

        $tabla = $this->table->generate();
      }

      $contenido = [
        'tabla' => $tabla
      ];

    $this->blade->render('xml/index',$contenido);
  }

  public function create()
  {
    $id = @base64_decode($this->input->get('id'));

    $poliza_datos = false;
    $transaccion_datos = false;
    $acumulado = 0;
    $acumulado_abono = 0;
    $acumulado_cargo = 0;
    $poliza_nomenclatura = false;
    $datos_cheque = false;

    $tabla = [];
    if($id != false){

      $this->load->model('CaPolizaXml_model');
      $this->db->where('ca_poliza_xml.deleted_at IS NULL',null, false);
      $poliza_datos = $this->CaPolizaXml_model->get([
        'id' => $id,
      ]);

      if($poliza_datos == false){
        redirect('polizas/xml');
      }
      // utils::pre($poliza_datos);


      $this->load->model('DePolizaXml_model');
      $poliza_datos_det = $this->DePolizaXml_model->getAll([
        'id_poliza_xml' => $poliza_datos['id'],
        'activo' => 1
      ]);
      // utils::pre($poliza_datos_det);

      $this->load->model('CaPolizasNomenclaturas_model');
      $poliza_nomenclatura = $this->CaPolizasNomenclaturas_model->get(array(
          'id' => $poliza_datos['id_poliza_nomenclatura']
      ));
      // utils::pre($poliza_nomenclatura);

      $this->load->model('CaTransacciones_model');
      $transaccion_datos = $this->CaTransacciones_model->get_poliza_xml(array(
          'id' => $poliza_datos['transaccion_id']
      ));
      // utils::pre($transaccion_datos);

      $this->load->model('DeDatosCheque_model');
      $datos_cheque = $this->DeDatosCheque_model->get([
        'transaccion_id' => $poliza_datos['transaccion_id']
      ]);
    


      if(is_array($poliza_datos_det) && count($poliza_datos_det)>0){
        
        foreach ($poliza_datos_det as $key => $poliza_dat_det) {
          
          $this->load->library('table');
          $template = array(
              'table_open'            => '<table class="table table-striped table-bordered mt-3 mb-3">',
          );
          $this->table->set_template($template);
          $cell_head_1 = ['data'=>'Comprobante'];
          $header = [
              'No. Asiento',
              $cell_head_1,
              'Cargo',
              'Abono',
          ];

          $datos_cheque = false;

          $this->load->model('DeDatosCheque_model');
          if(strlen(trim($poliza_dat_det['id_datos_cheque']))>0){
            $datos_cheque = $this->DeDatosCheque_model->get([
              'id' => $poliza_dat_det['id_datos_cheque']
            ]);
          }
        

          if ($transaccion_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false) {
              $header[] = 'Acciones';
          }
          
          $data_head = '<a class="btn btn-primary btn-sm m-1" target="_blank" href="https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/obtener_documento?id_archivo='.$poliza_dat_det['archivo'].'">Ver XML</a>'
          .'&nbsp;&nbsp;'
          .'<a class="btn btn-primary btn-sm  m-1" target="_blank" href="https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/obtener_documento?id_archivo='.$poliza_dat_det['archivo_pdf'].'">Ver PDF</a>';
          // .'<a class="btn btn-primary btn-sm" target="_blank" href="'.BASE_POLIZAS_XML.$poliza_dat_det['archivo_pdf'].'">Ver PDF</a>';

          if(is_array($datos_cheque)){
            $data_head .= '<a type="button" class="btn btn-warning btn-sm ml-1"  href="'.site_url('cheques/asientos?id='.$datos_cheque['id']).'">Cheque '.utils::folio($datos_cheque['folio'],6).'</a>';

          }

          if ($transaccion_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false) {
              $cell = array('data' => $data_head, 'class' => 'table-info','style'=>'width: 200px;', 'colspan' => 3);
          } else {
              $cell = array('data' => $data_head, 'class' => 'table-info','style'=>'width: 200px;', 'colspan' => 2);
          }

          $descripcion = is_array($poliza_dat_det) && array_key_exists('descripcion',$poliza_dat_det)? $poliza_dat_det['descripcion'] : '<b>Comprobante</b>';

          $cell_1 = array('data' => '<p class="text-justify">'.$descripcion.'</p>', 'class' => 'table-info','colspan'=>2,'style'=>'width: 200px;');

          if($poliza_dat_det['finalizado'] == 1){
            if ($poliza_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false) {
                $cell_fin = array('data' => '<center>Comprobante finalizado</center>', 'class' => 'table-warning', 'colspan' => 5);
            } else {
                $cell_fin = array('data' => '<center>Comprobante finalizado</center>', 'class' => 'table-warning', 'colspan' => 4);
            }
            $this->table->add_row($cell_fin);
          }


          $this->table->add_row($cell_1,$cell);

          $this->table->add_row($header);

          $acumulado = 0;
          $acumulado_abono = 0;
          $acumulado_cargo = 0;
          $cont = 1;
            
            $asiento_id_total = $poliza_dat_det['asiento_id_total'];
            $asiento_id_subtotal = $poliza_dat_det['asiento_id_subtotal'];
            $asiento_id_iva = $poliza_dat_det['asiento_id_iva'];
            
            $response = utils::api_get([
              'url' => site_url('asientos/api/detalle?estatus_id=ACTIVOS&asiento_id='.json_encode([
                $asiento_id_iva,
                $asiento_id_subtotal,
                $asiento_id_total
              ]))
            ]);
            
            $band_column = false;
            if (is_array($response) && array_key_exists('data', $response) && is_array($response['data']) && count($response['data']) > 0) {

              
                foreach ($response['data'] as $key => $value) {
                    // utils::pre($value);
                    $acumulado_abono = $acumulado_abono + $value['abono'];
                    $acumulado_cargo = $acumulado_cargo + $value['cargo'];

                    $cell_body_1 = array('data' => utils::folio( $value['asiento_id'], 6), 'class' => '','style'=>'width: 150px;');
                    $cell_body_2 = array('data' => $value['concepto'], 'class' => '','style'=>'width: 500px;');

                      $contenido = array(
                        $cell_body_1,
                        $cell_body_2,
                        utils::format($value['cargo']),
                        utils::format($value['abono']),

                    );
                    
                    if($poliza_dat_det['finalizado'] == 0){
                      if ($poliza_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false ) {
                          $botones = '<button title="Editar" type="button" data-renglon_id="'.$value['asiento_id'].'" class="btn btn-primary ml-2 btn-sm" onclick="modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>' .
                              '<button title="Eliminar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-danger ml-2 btn-sm" onclick="eliminar_renglon(this);" ><i class="far fa-trash-alt"></i></div>';
                          $contenido[] = $botones;
                      }
                    }else{
                      if ($poliza_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false ) {
                        $botones = '<button disabled title="Editar" type="button" data-renglon_id="'.$value['asiento_id'].'" class="btn btn-primary ml-2 btn-sm" onclick="modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>' .
                            '<button disabled title="Eliminar" type="button" data-renglon_id="' . $value['asiento_id'] . '" class="btn btn-danger ml-2 btn-sm" onclick="eliminar_renglon(this);" ><i class="far fa-trash-alt"></i></div>';
                        $contenido[] = $botones;
                      }
                    }

                    $this->table->add_row($contenido);
                    $cont++;
                }

                if (is_array($response) && array_key_exists('total', $response) && is_array($response['total']) && count($response['total']) > 0) {
                    $cell = array('data' => 'Total', 'class' => '', 'colspan' => 1);
                    if ($poliza_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false) {
                        $this->table->add_row('',$cell, utils::format($response['total']['cargo']), utils::format($response['total']['abono']), '');
                    } else {
                        $this->table->add_row('',$cell, utils::format($response['total']['cargo']), utils::format($response['total']['abono']));
                    }
                }
            }else {
              if ($poliza_datos['id_estatus_poliza_xml'] <= 3 && $poliza_dat_det['id_datos_cheque'] == false) {
                  $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 5);
              } else {
                  $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 4);
              }
              $this->table->add_row($cell);
          }
          $tabla[] =  $this->table->generate();
          }
        }
        
      }
    


      $this->load->model('CaPolizasFijas_model');
      $polizas_fijas = $this->CaPolizasFijas_model->getAll();

      $this->load->model('CaPolizasNomenclaturas_model');
      $polizas = $this->CaPolizasNomenclaturas_model->getAll();

      $this->load->model('CaPersonas_model');
      $proveedores = $this->CaPersonas_model->getAll([
        'tipo_persona_id' => 2
      ]);
      // utils::pre($proveedores);


    $content = [
      'id' => $id,
      'tabla' => $tabla,
      'polizas' => $polizas,
      'polizas_fijas' => $polizas_fijas,
      'transaccion_datos' => $transaccion_datos,
      'poliza_datos' => $poliza_datos,
      'acumulado_abono' => $acumulado_abono,
      'acumulado_cargo' => $acumulado_cargo,
      'poliza_nomenclatura' => $poliza_nomenclatura,
      'datos_cheque' => $datos_cheque,
      'proveedores' => $proveedores,
      'poliza_datos_det' => (isset($poliza_datos_det)? $poliza_datos_det : []),

    ];
    // utils::pre($content);
    
    if(strlen(trim($id))>0){
        $this->blade->render('xml/create',$content);
    }else{
        $this->blade->render('xml/create_poliza',$content);
    }

  }

}


/* End of file Xmls.php */
/* Location: ./application/controllers/Xmls.php */