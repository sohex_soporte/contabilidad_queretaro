<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

/**
 *
 * Controller XmlApi.php
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller REST
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Xml_api extends RestController
{
    
  public function __construct()
  {
    parent::__construct();
  }

  public function ProcesarXml_post()
  {

    $fileTmpPath = $_FILES['myfile']['tmp_name'];
    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileType = $_FILES['myfile']['type'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    
    // Directorio a copiar la imagen
    $original = str_replace('-','_',utils::guid()).'.'.$fileExtension;
    $destino_path = PATH_POLIZAS_XML.$original;
    
    move_uploaded_file($fileTmpPath, $destino_path);
    
    echo json_encode([
      'nombre' => $fileName,
      'original' => $original
    ]);
    exit();
  }

  public function ProcesarCheque_post()
  {

    $fileTmpPath = $_FILES['myfile']['tmp_name'];
    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileType = $_FILES['myfile']['type'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    
    // Directorio a copiar la imagen
    $original = str_replace('-','_',utils::guid()).'.'.$fileExtension;
    $destino_path = PATH_CHEQUES.$original;
    
    move_uploaded_file($fileTmpPath, $destino_path);
    
    echo json_encode([
      'nombre' => $fileName,
      'original' => $original
    ]);
    exit();
  }

  public function ProcesarPdf_post()
  {

    $fileTmpPath = $_FILES['myfile']['tmp_name'];
    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileType = $_FILES['myfile']['type'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    
    // Directorio a copiar la imagen
    $original = str_replace('-','_',utils::guid()).'.'.$fileExtension;
    $destino_path = PATH_POLIZAS_XML.$original;
    
    move_uploaded_file($fileTmpPath, $destino_path);
    
    echo json_encode([
      'nombre' => $fileName,
      'original' => $original
    ]);
    exit();
  }

  public function VerXml_post(){
    $id_poliza_act = $this->input->post('id_poliza_act');
    $transaccion_id = $this->input->post('transaccion_id');

    $archivo = $this->input->post('archivo');
    $destino_path = PATH_POLIZAS_XML.$archivo;

    $xml = simplexml_load_file($destino_path); 
    $ns = $xml->getNamespaces(true);
    
    $total = 0;
    $subtotal = 0;
    $iva_xml = 0;
    

    foreach($xml->attributes() as $a => $b) {
      if($a == 'SubTotal'){
        $subtotal = (string)$b;
      }

      if($a == 'Total'){
        $total = (string)$b;
      }

    }

    $this->load->model('DePolizaXml_model');
    $xml_poliza = $this->DePolizaXml_model->insert([
      'xml' => json_encode($xml),
      'id_poliza_xml' => $id_poliza_act,
      'subtotal' => $subtotal,
      'total' => $total,
      'iva' => ($total - $subtotal),
      'archivo' => $archivo,
      'activo' => 0
    ]);

    echo json_encode([
      'catalogo' => $id_poliza_act,
      'detalle' => $xml_poliza,
      'subtotal' => $subtotal,
      'total' => $total,
      'iva' => ($total - $subtotal),
      'transaccion_id' => $transaccion_id
    ]);
    exit();

  }

  public function GuardarXml_wwpost(){
    $id_poliza_act = $this->input->post('id_poliza_act');
    
    

    $this->load->model('CaPolizaXml_model');
    $this->CaPolizaXml_model->update([
      'activo' => 1
    ],[
      'id' => $id_poliza_act
    ]);

    echo json_encode([
      'id_poliza_act' => $id_poliza_act
    ]);
    exit();
  }

  public function modal_agregar_asiento_get(){

    $data_content = [
    ];

    $html = $this->load->view('/xml/modal_agregar_asiento',$data_content,true);

    $this->data['data'] = array(
        'html' => base64_encode(utf8_decode($html)),
    );
    $this->response($this->data);
}

public function crear_cheque_post(){
  
  // $this->load->model('CaPolizaXml_model');
  // $this->CaPolizaXml_model->update([
  //   'id_estatus_poliza_xml' => 4
  // ],[
  //   'id' => $this->input->post('id_poliza_xml')
  // ]);

  $this->load->model('VwPollizasXml_model');
  $this->db->where('id_datos_cheque',null);
  $poliza_datos_det = $this->VwPollizasXml_model->getAll([
    'id_poliza' => $this->input->post('id_poliza_xml'),
    'finalizado' => 1
  ]);
  
  
  $this->load->library('table');
  $template = array(
    'table_open'            => '<table class="table table-bordered">'
  );

  $this->table->set_template($template);
  $this->table->set_heading([
    'Selección',
    'Descripcion',
    'Cargos',
    'Abonos'
  ]);

  if(is_array($poliza_datos_det) && count($poliza_datos_det)>0){
    foreach ($poliza_datos_det as $key => $value) {
      // dd($value);
      $cell_2 = array('data' => $value['poliza_xml_descripcion'], 'class' => '');
      $cell_3 = array('data' => utils::format($value['cargos']), 'class' => '');
      $cell_4 = array('data' => utils::format($value['abonos']), 'class' => '');
      $cell_6 = '<center><label><input onclick="app.habilitar_boton(this)" type="checkbox" value="'.$value['id_transaccion'].'" id="opciones" name="opciones[]" class="largerCheckbox"></label></center>';

      $this->table->add_row([
        $cell_6,
        $cell_2,
        $cell_3,
        $cell_4
      ]);
    }
  }else{
    $cell_1 = array('data' => '<center><h6 class="m-3">No se encontraron datos para realizar un cheque</h6></center>', 'colspan' => '4');
    $this->table->add_row([
      $cell_1
    ]);
  }

  $content = [
    'table' => $this->table->generate()
  ];

  $this->data['status'] = 'ok';
  $this->data['data'] = array(
    'html' => base64_encode(utf8_decode( $this->load->view('xml/crear_cheque',$content,true) ))
  );
  $this->response($this->data);

}

public function finalizar_parte_poliza_guardar_post(){
  
  $opciones = $this->input->post('opciones');
  if(is_array($opciones) && count($opciones)>0){

    $this->load->model('DePolizaXml_model');

    foreach ($opciones as $value){

      // utils::api_post([
      //   'url' => site_url('asientos/api/aplicar_transaccion'),
      //   'params' => [
      //     'estatus' => 'APLICADO',
      //     'transaccion_id' => $value
      //   ]
      // ]);

      

      $curl = curl_init();
      curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/aplicar_transaccion',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => [
              'estatus' => 'APLICADO',
              'transaccion_id' => $value
          ])
      );
      $this->data['data'][] = curl_exec($curl);
      curl_close($curl);





      $this->DePolizaXml_model->update([
        'finalizado' => 1
      ],[
        'activo' => 1,
        'id_transaccion_cheque' => $value,
      ]);

    }
    
  }
  $this->data['status'] = 'ok';
  $this->response($this->data);
}


public function finalizar_parte_poliza_post(){

  // $this->load->model('VwPollizasXml_model');
  // $this->db->where('id_datos_cheque',null);
  // $poliza_datos_det = $this->VwPollizasXml_model->getAll([
  //   'id_poliza' => $this->input->post('id_poliza_xml')
  // ]);

  $this->load->model('DePolizaXml_model');
  $this->db->where('id_datos_cheque',null,false);
  $poliza_datos_det = $this->DePolizaXml_model->getAll([
    'id_poliza_xml' => $this->input->post('id_poliza_xml'),
    'activo' => 1,
    'finalizado' => 0,
  ]);
  
  
  $this->load->library('table');
  $template = array(
    'table_open'            => '<table class="table table-bordered">'
  );

  $this->table->set_template($template);
  $this->table->set_heading([
    'Selección',
    'Descripcion',
    'Total',
    'iva',
    'Subtotal'
  ]);

  if(is_array($poliza_datos_det) && count($poliza_datos_det)>0){
    // dd($poliza_datos_det);
    foreach ($poliza_datos_det as $key => $value) {
      // dd($value);
      $cell_2 = array('data' => $value['descripcion'], 'class' => '');
      $cell_3 = array('data' => utils::format($value['total']), 'class' => '');
      $cell_1 = array('data' => utils::format($value['iva']), 'class' => '');
      $cell_4 = array('data' => utils::format($value['subtotal']), 'class' => '');
      $cell_6 = '<center><label><input onclick="app.habilitar_boton(this)" type="checkbox" value="'.$value['id_transaccion_cheque'].'" id="opciones" name="opciones[]" class="largerCheckbox"></label></center>';

      $this->table->add_row([
        $cell_6,
        $cell_2,
        $cell_3,
        $cell_1,
        $cell_4
      ]);
    }
  }else{
    $cell_1 = array('data' => '<center><h6 class="m-3">No se encontraron datos para realizar un cheque</h6></center>', 'colspan' => '5');
    $this->table->add_row([
      $cell_1
    ]);
  }

  $content = [
    'table' => $this->table->generate()
  ];

  $this->data['status'] = 'ok';
  $this->data['data'] = array(
    'html' => base64_encode(utf8_decode( $this->load->view('xml/finalizar_parte_poliza',$content,true) ))
  );
  $this->response($this->data);

}

public function aplicar_transacciones_post(){
  
  $this->load->model('CaPolizaXml_model');
  $this->CaPolizaXml_model->update([
    'id_estatus_poliza_xml' => 4
  ],[
    'id' => $this->input->post('id_poliza_xml')
  ]);

  $this->load->model('DePolizaXml_model');
  $poliza_datos_det = $this->DePolizaXml_model->getAll([
    'id_poliza_xml' => $this->input->post('id_poliza_xml'),
    'activo' => 1
  ]);
  
  if(is_array($poliza_datos_det) && count($poliza_datos_det)>0){
    foreach ($poliza_datos_det as $key => $value) {
      
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/aplicar_transaccion',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
          'estatus' => 'APLICADO',
          'transaccion_id' => $value['id_transaccion_cheque']
        )
      ));
      $response = curl_exec($curl);
      curl_close($curl);
      

    }
  }


  $this->data['data'] = array(
    
  );
  $this->response($this->data);
}

public function GuardarXml_post(){
  $this->load->library('form_validation');

  // $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
  
  $this->form_validation->set_rules('id_archivo_modal', 'Archivo XML', 'trim|required');
  $this->form_validation->set_rules('id_archivo_modal2', 'Archivo PDF', 'trim|required');
  // $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');
  $this->form_validation->set_rules('subtotal', 'Subtotal', 'trim|required');
  $this->form_validation->set_rules('iva', 'IVA', 'trim|required');
  $this->form_validation->set_rules('total', 'Total', 'trim|required');
  $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
  // $this->form_validation->set_rules('concepto', 'Concepto', 'trim|required');
  // $this->form_validation->set_rules('tipo_unidad', 'Tipo de unidad', 'trim|required');

  if ($this->form_validation->run($this) != FALSE) {

      $id_poliza_act = $this->input->post('id_poliza_act');
      $id_poliza_xml_detalle = $this->input->post('id_poliza_xml_detalle');
      
      $modal_tipo = 1;
      $modal_tipo_2 = 1;
      $modal_tipo_3 = 1;
      $modal_tipo_cuenta = 1;
      $modal_tipo_2_cuenta = 1;
      $modal_tipo_3_cuenta = 1;

      $configuracion = false;

      $this->load->model('CaPolizaXml_model');
      $datos_poliza_xml = $this->CaPolizaXml_model->get([
        'id' => $this->input->post('id_poliza_act'),
      ]);

      if(is_array($datos_poliza_xml)){
        $id_persona = $datos_poliza_xml['id_persona'];

        $this->load->model('DePersonasXmlConfig_model');
        $configuracion = $this->DePersonasXmlConfig_model->get([
          'id_persona' => $id_persona
        ]);

        if(is_array($configuracion) && array_key_exists('id_cuenta_total',$configuracion)){
          $modal_tipo = $configuracion['total_tipo'];     
          $modal_tipo_cuenta = $configuracion['id_cuenta_total'];     
        }

        if(is_array($configuracion) && array_key_exists('id_cuenta_subtotal',$configuracion)){
          $modal_tipo_2 = $configuracion['subtotal_tipo'];     
          $modal_tipo_2_cuenta = $configuracion['id_cuenta_subtotal'];     
        }

        if(is_array($configuracion) && array_key_exists('id_cuenta_iva',$configuracion)){
          $modal_tipo_3 = $configuracion['iva_tipo'];     
          $modal_tipo_3_cuenta = $configuracion['id_cuenta_iva'];     
        }

      }


      $this->load->model('CaPolizaXml_model');
      $datos_poliza_xml = $this->CaPolizaXml_model->get([
        'id' => $id_poliza_act
      ]);

      $this->load->model('CaPolizas_model');
      $datos_poliza = $this->CaPolizas_model->get([
        'id' => $datos_poliza_xml['id_poliza']
      ]);
      
      $this->load->model('CaTransacciones_model');
      $transaccion_id = $this->CaTransacciones_model->insert(array(
          'folio' => $datos_poliza['PolizaNomenclatura_id'].utils::folio($datos_poliza['dia'],3),
          'fecha' => $datos_poliza['fecha_creacion'],
          'origen' => 'POLIZAS_XML',
          'sucursal_id' => 3,
          // 'poliza_fija_id'=> $id_poliza_fija
      ));
      

      $cargo = 0;
      $abono = 0;
      if($modal_tipo_2== 1){
          $cargo = $this->input->post('subtotal');
      }else{
          $abono = $this->input->post('subtotal');
      }

      $this->load->model('Asientos_model');
      $asiento_id_subtotal = $this->Asientos_model->insert([
          'polizas_id' => $this->input->post('id_poliza_gen'),
          'estatus_id' => 'POR_APLICAR',
          'transaccion_id' => $transaccion_id, //$this->input->post('transaccion_id'),
          'tipo_pago_id' => 'OTRO',
          'cuenta' => $modal_tipo_2_cuenta,
          'cargo' => $cargo,
          'abono' => $abono,
          'concepto' => 'SUBTOTAL',//$this->input->post('concepto'),
          'fecha_creacion' => utils::get_date(),
          'precio_unitario' => $this->input->post('subtotal'),
          'cantidad' => 1,
          // 'unidad_id' => $this->input->post('tipo_unidad'),
          'departamento_id' => 9
      ]);


      $cargo = 0;
      $abono = 0;
      if($modal_tipo_3 == 1){
          $cargo = $this->input->post('iva');
      }else{
          $abono = $this->input->post('iva');
      }

      $this->load->model('Asientos_model');
      $asiento_id_iva = $this->Asientos_model->insert([
          'polizas_id' => $this->input->post('id_poliza_gen'),
          'estatus_id' => 'POR_APLICAR',
          'transaccion_id' => $transaccion_id, //$this->input->post('transaccion_id'),
          'tipo_pago_id' => 'OTRO',
          'cuenta' => $modal_tipo_3_cuenta,
          'cargo' => $cargo,
          'abono' => $abono,
          'concepto' => 'IVA', //$this->input->post('concepto'),
          'fecha_creacion' => utils::get_date(),
          'precio_unitario' => $this->input->post('iva'),
          'cantidad' => 1,
          // 'unidad_id' => $this->input->post('tipo_unidad'),
          'departamento_id' => 9
      ]);

      $cargo = 0;
      $abono = 0;
      if($modal_tipo == 1){
          $cargo = $this->input->post('total');
      }else{
          $abono = $this->input->post('total');
      }

      $this->load->model('Asientos_model');
      $asiento_id_total = $this->Asientos_model->insert([
          'polizas_id' => $this->input->post('id_poliza_gen'),
          'estatus_id' => 'POR_APLICAR',
          'transaccion_id' => $transaccion_id, //$this->input->post('transaccion_id'),
          'tipo_pago_id' => 'OTRO',
          'cuenta' => $modal_tipo_cuenta,
          'cargo' => $cargo,
          'abono' => $abono,
          'concepto' => 'TOTAL',//$this->input->post('concepto'),
          'fecha_creacion' => utils::get_date(),
          'precio_unitario' => $this->input->post('total'),
          'cantidad' => 1,
          // 'unidad_id' => $this->input->post('tipo_unidad'),
          'departamento_id' => 9
      ]);



      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/guardar_documento',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
          'id_tipo_archivo' => '1',  //PDF
          'identificador_externo' => $id_poliza_xml_detalle,
          'documento'=> new CURLFILE( PATH_POLIZAS_XML.$this->input->post('id_original_modal2'),'application/pdf' )
        )
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      $pdf_archivo_id = '';
      if ($response) {
          $result = json_decode($response,true);
          if(is_array($result) && array_key_exists('data',$result) && array_key_exists('identificador',$result['data'])){
            $pdf_archivo_id = $result['data']['identificador'];
          }
      }


      


      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/guardar_documento',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
          'id_tipo_archivo' => '2',  //PDF
          'identificador_externo' => $id_poliza_xml_detalle,
          'documento'=> new CURLFILE( PATH_POLIZAS_XML.$this->input->post('id_original_modal'),'text/xml' )
        )
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      $xml_archivo_id = '';
      if ($response) {
          $result = json_decode($response,true);
          if(is_array($result) && array_key_exists('data',$result) && array_key_exists('identificador',$result['data'])){
            $xml_archivo_id = $result['data']['identificador'];
          }
      }
      



      $this->load->model('DePolizaXml_model');
      $this->DePolizaXml_model->update([
        'activo' => 1,
        'asiento_id_total' => $asiento_id_total,
        'asiento_id_subtotal' => $asiento_id_subtotal,
        'asiento_id_iva' => $asiento_id_iva,
        'archivo_pdf' => $pdf_archivo_id,
        'archivo' => $xml_archivo_id,
        'id_transaccion_cheque' => $transaccion_id, //$this->input->post('transaccion_id'),
        'descripcion' => $this->input->post('descripcion')
      ],[
        'id' => $id_poliza_xml_detalle
      ]);

      // if($asiento_id != false){
      //     $this->load->model('MaBalanza_model');
      //     $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);
      //     if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
      //         $this->Asientos_model->update(array(
      //             'estatus_id' => 'APLICADO',
      //             'fecha_asiento_aplicado' => utils::get_datetime()
      //         ), array(
      //             'id' => $asiento_id
      //         ));
      //         $balanza_id = $balaza['balanza_id'];
      //     }
      // }

      $this->data['data'] = [
        'asiento_id_total' => $asiento_id_total,
        'asiento_id_subtotal' => $asiento_id_subtotal,
        'asiento_id_iva' => $asiento_id_iva
      ];
      
  } else {
      $this->data['status'] = 'error';
      $this->data['message'] = $this->form_validation->error_array();
  }

  $this->response($this->data);
}

public function crear_poliza_post(){

  $this->load->library('form_validation');

  $this->form_validation->set_rules('id_poliza', 'Poliza', 'trim|required');
  if($this->input->post('id_poliza') == 'DO'){
    $this->form_validation->set_rules('id_poliza_fija', 'Tipo Poliza Diario', 'trim|required');
  }
  
  $this->form_validation->set_rules('id_persona', 'Proveedor', 'trim|required');
  $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');

  if ($this->form_validation->run($this) != FALSE) {


  $id_poliza = $this->input->post('id_poliza');
  $id_poliza_fija = $this->input->post('id_poliza_fija');
  // utils::pre($_POST);
  if($id_poliza == 'DO'){

    // $this->load->model('CaTransacciones_model');
    // $transaccion_id = $this->CaTransacciones_model->insert(array(
    //     'folio' => 'DO'.utils::folio($id_poliza_fija,3),
    //     'fecha' => utils::get_date(),
    //     'origen' => 'POLIZAS_XML',
    //     'sucursal_id' => 3,
    //     'poliza_fija_id'=> $id_poliza_fija
    // ));


    $this->load->model('CaPolizas_model');
    $d = new DateTime(utils::get_date());
    $poliza_std_id = $this->CaPolizas_model->insert([
        'PolizaNomenclatura_id' => 'DO',
        'ejercicio' => $d->format('Y'),
        'mes' => $d->format('m'),
        'dia' => $id_poliza_fija,
        'fecha_creacion' => utils::get_date(),
        // 'descripcion' => $this->input->post('descripcion'),
        // 'id_persona' => $this->input->post('id_persona')
    ]);

  }else{

    // $this->load->model('CaTransacciones_model');
    // $d = new DateTime(utils::get_date());
    // $transaccion_id = $this->CaTransacciones_model->insert(array(
    //     'folio' => $id_poliza.utils::folio($d->format('d'),3),
    //     'fecha' => utils::get_date(),
    //     'origen' => 'POLIZAS_XML',
    //     'sucursal_id' => 3
    // ));


    $this->load->model('CaPolizas_model');
    $d = new DateTime(utils::get_date());
    $poliza_std_id = $this->CaPolizas_model->insert([
        'PolizaNomenclatura_id' => $id_poliza,
        'ejercicio' => $d->format('Y'),
        'mes' => $d->format('m'),
        'dia' => $d->format('d'),
        'fecha_creacion' => utils::get_date(),
        // 'descripcion' => $this->input->post('descripcion'),
        // 'id_persona' => $this->input->post('id_persona')
    ]);

  }

  $this->load->model('CaPolizaXml_model');
  $poliza_id = $this->CaPolizaXml_model->insert([
    // 'transaccion_id' => $transaccion_id,
    'activo' => 1,
    'id_poliza' => $poliza_std_id,
    'id_estatus_poliza_xml' => 2,
    'id_poliza_nomenclatura' => $id_poliza,
    'id_persona' => $this->input->post('id_persona'),
    'descripcion' => $this->input->post('descripcion')
  ]);

  $this->data['data'] = array(
    'transaccion_id' => null, //$transaccion_id,
    'poliza_id' => $poliza_id
  );
} else {
        $this->data['status'] = 'error';
        $this->data['message'] = $this->form_validation->error_array();
    }
  
    $this->response($this->data);

}
  function crear_sub_transacccion_post(){
    
    $id_poliza_xml = $this->input->post('id_poliza_xml');
    $id_persona = $this->input->post('id_persona');
    $opciones = $this->input->post('opciones');

    $transaccion_nuevo_id = false;
    $id_datos_cheque = false;

    if(is_array($opciones) && count($opciones)>0){
      foreach ($opciones as $transaccion_id) {

        $bandera_creacion = false;
        $transaccion_nuevo_id = false;

        $this->load->model('CaTransacciones_model');
        $transaccion_act = $this->CaTransacciones_model->get([
          'id' => $transaccion_id
        ]);
        
        if(is_array($transaccion_act)){
          
          $poliza_identi = $this->input->post('opciones');
          if(is_array($poliza_identi) && count($poliza_identi)>0){

            if($transaccion_nuevo_id == false){
              $transaccion_nuevo_id = $this->CaTransacciones_model->insert([
                'persona_id' => $id_persona,
                'folio' => $transaccion_act['folio'],
                'origen' => 'CHEQUE',
                'fecha' => $transaccion_act['fecha'],
                'poliza_fija_id' => $transaccion_act['poliza_fija_id']
              ]);
            }
            

            $this->load->model('Asientos_model');
            $asiento_datos = $this->Asientos_model->getAll([
              'transaccion_id' => $transaccion_id
            ]);

            if(is_array($asiento_datos) && count($asiento_datos)>0){
              foreach ($asiento_datos as $key => $asiento_dato) {
                
                if(is_array($asiento_dato) && count($asiento_dato)>0){
                  $asiento_insert = [];
                  foreach ($asiento_dato as $asiento_key => $asiento) {
                    if($asiento_key == 'estatus_id'){
                      $asiento_insert[$asiento_key] = 'POR_APLICAR';

                    }elseif($asiento_key == 'transaccion_id'){
                      $asiento_insert[$asiento_key] = $transaccion_nuevo_id;
                      
                    }elseif(!in_array($asiento_key,['id','fecha_asiento_aplicado','acumulado'])){
                      $asiento_insert[$asiento_key] = $asiento;
                      
                    }
                  }
                  $this->Asientos_model->insert($asiento_insert);
                }

              }
            }

            $this->load->model('DeDatosCheque_model');
            $id_datos_cheque = $this->DeDatosCheque_model->insert([
              'transaccion_id' => $transaccion_nuevo_id,
              'persona_id' => $id_persona,
              // 'referencia' => ''
              'id_datos_cheque_estatus' => 1
            ]);

            $this->load->model('CaPolizaXml_model');
            $poliza_xml_datos = $this->CaPolizaXml_model->get([
              'id' => $id_poliza_xml
            ]);
            // utils::pre($poliza_xml_datos);

            $this->load->model('ReTransaccionesPolizas_model');
            $this->ReTransaccionesPolizas_model->insert([
              'transaccion_id' => $transaccion_nuevo_id,
              'poliza_id' => $poliza_xml_datos['id_poliza']
            ]);

          
            $this->load->model('DePolizaXml_model');
            $this->DePolizaXml_model->update([
              'id_datos_cheque' => $id_datos_cheque,
            ],[
              'id_transaccion_cheque' => $transaccion_id
            ]);
           

          }

        }

      }
    }

    
    $this->data['data'] = array(
      'transaccion_nuevo_id' => $transaccion_nuevo_id,
      'id_datos_cheque' => $id_datos_cheque
    );
    $this->response($this->data);
    
    
    // 

  }

  public function obtener_documentacion_get(){

    $id_poliza_xml = $this->input->get('id');

    $this->load->model('DePolizaXml_model');
    $listado_doc = $this->DePolizaXml_model->getAll([
      'id_poliza_xml' =>$id_poliza_xml
    ]);

    $this->load->library('table');
    $template = array(
      'table_open'            => '<table class="table table-striped table-hover table-bordered">',
    );
    $this->table->set_template($template);

    $this->table->set_heading(array('Comprobante', 'PDF', 'XML'));

    if(is_array($listado_doc) && count($listado_doc)>0){
      foreach ($listado_doc as $key => $value) {

        $this->table->add_row(array($value['descripcion'], '<a class="btn btn-primary btn-sm" target="_blank" href="https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/obtener_documento?id_archivo='.$value['archivo_pdf'].'">Ver PDF</a>', '<a class="btn btn-primary btn-sm" target="_blank" href="https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/obtener_documento?id_archivo='.$value['archivo'].'">Ver XML</a>'));

      }
    }else{
      $cell = array('data' => '<center>No se encontraron datos</center>','colspan'=> 3);
      $this->table->add_row($cell);
    }
    $documentos_1 = $this->table->generate();



    $this->db->select([
      // 'de_poliza_xml.id as id_de_poliza_xml',
      'de_poliza_xml.id_poliza_xml',
      'de_datos_cheque.id as id_datos_cheque',
      'de_datos_cheque.archivo_comprobante'
    ])
    ->distinct()
    ->from('de_poliza_xml')
    ->join('de_datos_cheque','de_poliza_xml.id_datos_cheque = de_datos_cheque.id')
    // ->join('ca_transacciones','de_datos_cheque.transaccion_id = ca_transacciones.id')
    // ->join('ca_personas','ca_transacciones.persona_id = ca_personas.id')
    // ->join('re_transacciones_polizas','ca_transacciones.id = re_transacciones_polizas.transaccion_id')
    // ->join('ca_polizas','re_transacciones_polizas.poliza_id = ca_polizas.id')
    ->where('de_poliza_xml.id_poliza_xml', $id_poliza_xml)
    ->where('de_datos_cheque.id_datos_cheque_estatus', 2);

    $query = $this->db->get();
    $result2_doc = $query->result_array();
    // utils::pre($this->db->last_query());


    $this->table->set_heading(array('Nombre', 'Documento'));

    if(is_array($result2_doc) && count($result2_doc)>0){
      foreach ($result2_doc as $key => $value) {
        
        $botones = '<a target="_blank" class="btn btn-warning ml-2 my-2 my-sm-0 btn-sm " href="'.site_url('cheques/imprimir/index?id='.$value['id_datos_cheque']).'">Ver cheque</a>';
        if(strlen(trim($value['archivo_comprobante']))>0){
          $botones .= '<a target="_blank" class="btn btn-info ml-2 my-2 my-sm-0 btn-sm " href="'.BASE_CHEQUE_COMPROBANTEL.$value['archivo_comprobante'].'">Ver comprobante</a>';
        }

        $this->table->add_row(array('Cheque '.($key+1), $botones));

      }
    }else{
      $cell = array('data' => '<center>No se encontraron datos</center>','colspan'=> 2);
      $this->table->add_row($cell);
    }
    $documentos_2 = $this->table->generate();


    $data_content = [
      'documentos_1' => $documentos_1,
      'documentos_2' => $documentos_2
    ];
    
    $this->data['data'] = array(
      'html' => base64_encode(utf8_decode($this->blade->render('xml_api/obtener_documentacion',$data_content,true)))
    );
    $this->response($this->data);


    

  }

}


/* End of file XmlApi.php.php */
/* Location: ./application/controllers/XmlApi.php.php */