<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');


    }

    public function ver_cuentas()
    {
        header('Content-Type: application/json');
        $response = $this->Mgeneral->get_table('cuentas_dms');
        echo json_encode($response);
    }

    // public function crear_poliza() {

    //     $response = array();

    //     $this->load->library('form_validation');
    //     $this->form_validation->set_rules('id_departamento', 'id_departamento', 'trim|required');
    //     $this->form_validation->set_rules('fecha', 'fecha (Y-m-d)', 'trim|required|valid_date');
    //     $this->form_validation->set_rules('concepto', 'concepto', 'trim|required');
    //     $this->form_validation->set_rules('tipo', 'tipo de poliza (1.-diario,2.-ingresos,3.-egresos,4.-cheques)', 'trim|required|in_list[1,2,3,4]');
    //     $this->form_validation->set_rules('poliza', 'poliza', 'trim|required');

    //     if ($this->form_validation->run($this) != FALSE)
    //     {
    //         $this->load->model('Polizas_model');
    //         $poliza_row = $this->Polizas_model->get(array(
    //             'poliza' => $this->input->post('poliza')
    //         ));
            
            
    //         if (is_array($poliza_row)) {
    //             $response['error'] = false;
    //             $response['mensaje'] = 'poliza encontrada';
    //             $response['id_id'] = $poliza_row['id_id'];
    //             $response['fecha'] = $poliza_row['fecha'];
    //             $response['poliza'] = $poliza_row['poliza'];
    //             $response['concepto'] = $poliza_row['concepto'];
    //             $response['fecha_creacion'] = $poliza_row['fecha_creacion'];
    //             $response['tipo'] = $poliza_row['tipo'];
    //             $response['id_departamento'] = $poliza_row['id_departamento'];

    //             switch ($poliza_row['tipo']) {
    //                 case 1:
    //                     $tipo_pol = "Diario";
    //                     break;
    //                 case 2:
    //                     $tipo_pol = "Ingresos";
    //                     break;
    //                 case 3:
    //                     $tipo_pol = "Egresos";
    //                     break;
    //                 case 4:
    //                     $tipo_pol = "Cheques";
    //                     break;
    //                 default:
    //                     $tipo_pol = "";
    //                     break;
    //             }
    //             $response['tipo_texto'] = $tipo_pol;
                
    //         } else {
    //             $data['fecha'] = $this->input->post('fecha'); //año-mes-dia
    //             $data['poliza'] = $this->input->post('poliza');
    //             $data['concepto'] = $this->input->post('concepto');
    //             $data['fecha_creacion'] = utils::now();
    //             $data['id_id'] = get_guid();
    //             $data['folio'] = "0";
    //             $data['cargo'] = "0";
    //             $data['abono'] = "0";
    //             $data['id_departamento'] = $this->input->post('id_departamento');

    //             $this->load->model('Polizas_model');
    //             $this->Polizas_model->insert($data);

    //             $response['error'] = false;
    //             $response['mensaje'] = "poliza guardada correctamente";
    //             $response['id_id'] = $data['id_id'];
                
    //         }
    //     }
    //     else
    //     {
    //         $response['error'] = true;
    //         $response['mensaje'] = $this->form_validation->error_array();
    //     }
        
    //     $this->response($response); 
    // }

    public function crear_poliza()
    {
        header('Content-Type: application/json');
        $response = array();

        $fecha = $this->input->post('fecha');
        $poliza = $this->input->post('poliza');
        $concepto = $this->input->post('concepto');
        $tipo = $this->input->post('tipo');
        $departamento = $this->input->post('id_departamento');

        $bandera = 0;
        $error = "";


        if ((!empty($departamento) && isset($departamento))) {
        } else {
            $error .= "ingrese una id departamento, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($fecha) && isset($fecha))) {
            $validar_fecha = $this->Mgeneral->check_fecha($fecha);
            if ($validar_fecha == false) {
                $error .= "ingrese una fecha correcta Y-m-d ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese una fecha, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }



        if ((!empty($concepto) && isset($concepto))) {
        } else {
            $error .= "ingrese un concepto, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($tipo) && isset($tipo))) {

            if (($tipo > 0) && ($tipo < 5)) {
            } else {
                $error  .= "ingrese un tipo correcto : 1.-diario,2.-ingresos,3.-egresos,4.-cheques	 ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {

            $error  .= "ingrese un tipo de poliza, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($poliza) && isset($poliza))) {
        } else {
            $error .= "ingrese una poliza, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }



        if ($bandera != 1) {


            $poliza_row = $this->Mgeneral->get_row('poliza', $poliza, 'polizas');
            if (is_object($poliza_row)) {

                $response['error'] = false;
                $response['mensaje'] = 'poliza encontrada';
                $response['id_id'] = $poliza_row->id_id;
                $response['fecha'] = $poliza_row->fecha;
                $response['poliza'] = $poliza_row->poliza;
                $response['concepto'] = $poliza_row->concepto;
                $response['fecha_creacion'] = $poliza_row->fecha_creacion;
                $response['tipo'] = $poliza_row->tipo;
                $response['id_departamento'] = $poliza_row->id_departamento;

                $tipo_pol = "";
                if ($poliza_row->tipo = 1) {
                    $tipo_pol = "Diario";
                }
                if ($poliza_row->tipo = 2) {
                    $tipo_pol = "Ingresos";
                }
                if ($poliza_row->tipo = 3) {
                    $tipo_pol = "Egresos";
                }
                if ($poliza_row->tipo = 4) {
                    $tipo_pol = "Cheques";
                }
                $response['tipo_texto'] = $tipo_pol;
            } else {
                $data['fecha'] = $this->input->post('fecha'); //año-mes-dia
                $data['poliza'] = $this->input->post('poliza');
                $data['concepto'] = $this->input->post('concepto');
                $data['fecha_creacion'] = date('Y-m-d H:i:s');
                $data['id_id'] = get_guid();
                $data['folio'] = "0";
                $data['cargo'] = "0";
                $data['abono'] = "0";
                $data['id_departamento'] = $this->input->post('id_departamento');

                $this->Mgeneral->save_register('polizas', $data);

                $response['error'] = false;
                $response['mensaje'] = "poliza guardada correctamente";
                $response['id_id'] = $data['id_id'];
            }
        }


        echo json_encode($response);
    }


    public function crear_asiento()
    {
        $response = array();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('id_id_poliza', 'id_id_poliza', 'trim|required|exists[polizas.id_id]');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('referencia', 'referencia', 'trim|required');
        $this->form_validation->set_rules('concepto', 'concepto', 'trim|required');
        $this->form_validation->set_rules('total', 'total', 'trim|required|numeric');
        $this->form_validation->set_rules('cuenta', 'cuenta', 'trim|required|exists[cuentas_dms.id]');
        $this->form_validation->set_rules('tipo', 'tipo de movimiento', 'trim|required|in_list[1,2]');
        $this->form_validation->set_rules('fecha', 'tipo de creacion', 'trim|required|valid_date');
        $this->form_validation->set_rules('origen_transaccion', 'origen transaccion', 'trim|exists[ca_origen_transaccion.id]');


        if ($this->form_validation->run($this) != FALSE)
        {
            $abono = 0;
            $cargo = 0;
            switch ((int)$this->input->post('tipo')) {
                case 1:
                    $abono = $this->input->post('total');
                    break;
                case 2:
                    $cargo = $this->input->post('total');
                    break;              
            }

            $data['cuenta'] = $this->input->post('cuenta');
            $data['nombre'] = $this->input->post('nombre');
            $data['referencia'] = $this->input->post('referencia');
            $data['concepto'] = $this->input->post('concepto');
            $data['cargo'] = $cargo;
            $data['abono'] = $abono;
            $data['id_id'] = get_guid();
            $data['poliza_id_id'] = $this->input->post('id_id_poliza');
            $data['fecha_creacion'] = $this->input->post('fecha');
            $data['origen_transaccion_id'] = ($this->input->post('origen_transaccion') == false)? 2 : $this->input->post('origen_transaccion');

            $this->load->model('Asientos_model');
            $this->Asientos_model->insert($data);

            $response['error'] = false;
            $response['mensaje'] = 'asiento guardado correctamente';
            $response['id_asiento'] = $data['id_id'];
            $response['poliza'] = $data['poliza_id_id'];
            $response['cargo'] = $cargo;
            $response['abono'] = $abono;
            $response['fecha_creacion'] = $data['fecha_creacion'];


            $this->load->model('Polizas_model');
            $poliza_row = $this->Polizas_model->get(array(
                'poliza' => $this->input->post('poliza')
            ));
        }
        else
        {
            $response['error'] = true;
            $response['mensaje'] = $this->form_validation->error_array();
        }
        
        $this->response($response);
    }

    public function buscar_poliza()
    {
        header('Content-Type: application/json');
        $poliza = $this->input->post('poliza');
        $response = array();
        $error = "";
        $bandera = 0;

        if ((!empty($poliza) && isset($poliza))) {

            $poliza_row = $this->Mgeneral->get_row('poliza', $poliza, 'polizas');
            if (is_object($poliza_row)) {

                $response['error'] = false;
                $response['mensaje'] = 'poliza encontrada';
                $response['id_id'] = $poliza_row->id_id;
                $response['fecha'] = $poliza_row->fecha;
                $response['poliza'] = $poliza_row->poliza;
                $response['concepto'] = $poliza_row->concepto;
                $response['fecha_creacion'] = $poliza_row->fecha_creacion;
                $response['tipo'] = $poliza_row->tipo;

                $tipo_pol = "";
                if ($poliza_row->tipo = 1) {
                    $tipo_pol = "Diario";
                }
                if ($poliza_row->tipo = 2) {
                    $tipo_pol = "Ingresos";
                }
                if ($poliza_row->tipo = 3) {
                    $tipo_pol = "Egresos";
                }
                if ($poliza_row->tipo = 4) {
                    $tipo_pol = "Cheques";
                }
                $response['tipo_texto'] = $tipo_pol;
            } else {
                $error .= "poliza no encontrada ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese una poliza ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        echo json_encode($response);
    }


    public function documentacion_contabilidad($solicitud,$tipo_persona=null){
        $response = array();

        if(isset($solicitud)){

            $valor = $this->Mgeneral->get_row('solicitud',$solicitud,'factura_solicitud');
            
            

            if(is_object($valor)){
                if($tipo_persona == 1){
                    $this->db->where('FISICA',1);
                }
                if($tipo_persona == 2){
                    $this->db->where('FISICA_ACT',1);
                }
                if($tipo_persona == 3){
                    $this->db->where('MORAL',1);
                }
                //$aux1 = $this->Mgeneral->get_table('solicitudes_factura');
                $aux1 = $this->db->get('solicitudes_factura')->result();
                $docu = array();
                foreach($aux1 as $documentos):
                    $this->db->where('id_factura_solicitud',$valor->id);
                    $this->db->where('id_solicitud_factura',$documentos->id);
                    
                    $res = $this->db->get('solicitudes_factura_campos')->row();
                    if(is_object($res)){
                        $docu[$documentos->DOCUMENTO] = $res->aprobado;

                    }else{
                        $docu[$documentos->DOCUMENTO] = 0;
                    }
                    
                endforeach;

                $response['data'] = $docu;
                $response['error'] = 0;
                if($valor->aprobado == 1){
                    $response['aprobado'] = 1;
                }else{
                    $response['aprobado'] = 0;
                }
                
            }else{
                $data['solicitud'] = $solicitud;
                $this->Mgeneral->save_register('factura_solicitud', $data);
                $valor = $this->Mgeneral->get_row('solicitud',$solicitud,'factura_solicitud');
                $aux1 = $this->Mgeneral->get_table('solicitudes_factura');
                $docu = array();
                foreach($aux1 as $documentos):
                    $this->db->where('id_factura_solicitud',$valor->id);
                    $this->db->where('id_solicitud_factura',$documentos->id);
                    $res = $this->db->get('solicitudes_factura_campos')->row();
                    if(is_object($res)){
                        $docu[$documentos->DOCUMENTO] = $res->aprobado;

                    }else{
                        $docu[$documentos->DOCUMENTO] = 0;
                    }

                endforeach;
                $response['data'] = $docu;
                $response['error'] = 0;
                if($valor->aprobado == 1){
                    $response['aprobado'] = 1;
                }else{
                    $response['aprobado'] = 0;
                }
            }


        }else{
            $response['data'] = "";
            $response['error'] = 0;

        }

        echo json_encode($response);

        exit();

    }
}
