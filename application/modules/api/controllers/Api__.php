<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function ver_cuentas()
    {
        header('Content-Type: application/json');
        $response = $this->Mgeneral->get_table('cuentas_dms');
        echo json_encode($response);
    }

    public function crear_poliza()
    {
        header('Content-Type: application/json');
        $response = array();

        $fecha = $this->input->post('fecha');
        $poliza = $this->input->post('poliza');
        $concepto = $this->input->post('concepto');
        $tipo = $this->input->post('tipo');
        $departamento = $this->input->post('id_departamento');

        $bandera = 0;
        $error = "";


        if ((!empty($departamento) && isset($departamento))) {
        } else {
            $error .= "ingrese una id departamento, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($fecha) && isset($fecha))) {
            $validar_fecha = $this->Mgeneral->check_fecha($fecha);
            if ($validar_fecha == false) {
                $error .= "ingrese una fecha correcta Y-m-d ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese una fecha, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }



        if ((!empty($concepto) && isset($concepto))) {
        } else {
            $error .= "ingrese un concepto, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($tipo) && isset($tipo))) {

            if (($tipo > 0) && ($tipo < 5)) {
            } else {
                $error  .= "ingrese un tipo correcto : 1.-diario,2.-ingresos,3.-egresos,4.-cheques	 ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {

            $error  .= "ingrese un tipo de poliza, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($poliza) && isset($poliza))) {
        } else {
            $error .= "ingrese una poliza, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }



        if ($bandera != 1) {


            $poliza_row = $this->Mgeneral->get_row('poliza', $poliza, 'polizas');
            if (is_object($poliza_row)) {

                $response['error'] = false;
                $response['mensaje'] = 'poliza encontrada';
                $response['id_id'] = $poliza_row->id_id;
                $response['fecha'] = $poliza_row->fecha;
                $response['poliza'] = $poliza_row->poliza;
                $response['concepto'] = $poliza_row->concepto;
                $response['fecha_creacion'] = $poliza_row->fecha_creacion;
                $response['tipo'] = $poliza_row->tipo;
                $response['id_departamento'] = $poliza_row->id_departamento;

                $tipo_pol = "";
                if ($poliza_row->tipo = 1) {
                    $tipo_pol = "Diario";
                }
                if ($poliza_row->tipo = 2) {
                    $tipo_pol = "Ingresos";
                }
                if ($poliza_row->tipo = 3) {
                    $tipo_pol = "Egresos";
                }
                if ($poliza_row->tipo = 4) {
                    $tipo_pol = "Cheques";
                }
                $response['tipo_texto'] = $tipo_pol;
            } else {
                $data['fecha'] = $this->input->post('fecha'); //año-mes-dia
                $data['poliza'] = $this->input->post('poliza');
                $data['concepto'] = $this->input->post('concepto');
                $data['fecha_creacion'] = date('Y-m-d H:i:s');
                $data['id_id'] = get_guid();
                $data['folio'] = "0";
                $data['cargo'] = "0";
                $data['abono'] = "0";
                $data['id_departamento'] = $this->input->post('id_departamento');

                $this->Mgeneral->save_register('polizas', $data);

                $response['error'] = false;
                $response['mensaje'] = "poliza guardada correctamente";
                $response['id_id'] = $data['id_id'];
            }
        }


        echo json_encode($response);
    }


    public function crear_asiento()
    {

        header('Content-Type: application/json');
        $response = array();
        $bandera = 0;
        $error = "";


        $poliza_id_id = $this->input->post('id_id_poliza');
        $nombre = $this->input->post('nombre');
        $referencia = $this->input->post('referencia');
        $concepto = $this->input->post('concepto');
        $total = $this->input->post('total');
        $cuenta = $this->input->post('cuenta'); // id de la cuenta

        $tipo = $this->input->post('tipo'); // 1.- abono, 2.-cargo
        $fecha_creacion = $this->input->post('fecha'); // 1.- abono, 2.-cargo

        if ((!empty($poliza_id_id) && isset($poliza_id_id))) {

            $id_pol = $this->Mgeneral->get_row('id_id', $poliza_id_id, 'polizas');
            if (!(is_object($id_pol))) {
                $error .= " id_id de poliza no encontrada, ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese un id de poliza, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($nombre) && isset($nombre))) {
        } else {
            $error .= "ingrese nombre, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($referencia) && isset($referencia))) {
        } else {
            $error .= "ingrese referencia, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }


        if ((!empty($concepto) && isset($concepto))) {
        } else {
            $error .= "ingrese concepto, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($fecha_creacion) && isset($fecha_creacion))) {
        } else {
            $error .= "ingrese una fecha, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        if ((!empty($total) && isset($total))) {

            if (is_numeric($total)) {
            } else {
                $error .= "el campo total debe ser un numero, ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese un total, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }


        if ((!empty($cuenta) && isset($cuenta))) {

            if (is_numeric($cuenta)) {

                $id_pol = $this->Mgeneral->get_row('id', $cuenta, 'cuentas_dms');
                if (!(is_object($id_pol))) {
                    $error .= " id de la cuenta no encontrada";
                    $bandera = 1;
                    $response['error'] = true;
                    $response['mensaje'] = $error;
                }
            } else {
                $error .= "el campo cuenta no es un id, ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese un id de la cuenta, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        $abono = 0;
        $cargo = 0;
        if ((!empty($tipo) && isset($tipo))) {

            if ($tipo == 1) {
                $abono = $total;
            } elseif ($tipo == 2) {
                $cargo = $total;
            } else {
                $error .= "ingrese un tipo de movimiento 1.-abono, 2.-cargo, ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese un tipo de movimiento 1.-abono, 2.-cargo, ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }


        if ($bandera != 1) {
            $data['cuenta'] = $this->input->post('cuenta');
            $data['nombre'] = $this->input->post('nombre');
            $data['referencia'] = $this->input->post('referencia');
            $data['concepto'] = $this->input->post('concepto');
            $data['cargo'] = $cargo;
            $data['abono'] = $abono;
            $data['id_id'] = get_guid();
            $data['poliza_id_id'] = $poliza_id_id;
            $data['fecha_creacion'] = $fecha_creacion;
            $data['origen_transaccion_id'] = ($this->input->post('origen_transaccion') == false)? 1 : $this->input->post('origen_transaccion');
            $this->Mgeneral->save_register('asientos', $data);

            $response['error'] = false;
            $response['mensaje'] = 'asiento guardado correctamente';
            $response['id_asiento'] = $data['id_id'];
            $response['poliza'] = $data['poliza_id_id'];
            $response['cargo'] = $cargo;
            $response['abono'] = $abono;
            $response['fecha_creacion'] = $fecha_creacion;
        }

        echo json_encode($response);
    }

    public function buscar_poliza()
    {
        header('Content-Type: application/json');
        $poliza = $this->input->post('poliza');
        $response = array();
        $error = "";
        $bandera = 0;

        if ((!empty($poliza) && isset($poliza))) {

            $poliza_row = $this->Mgeneral->get_row('poliza', $poliza, 'polizas');
            if (is_object($poliza_row)) {

                $response['error'] = false;
                $response['mensaje'] = 'poliza encontrada';
                $response['id_id'] = $poliza_row->id_id;
                $response['fecha'] = $poliza_row->fecha;
                $response['poliza'] = $poliza_row->poliza;
                $response['concepto'] = $poliza_row->concepto;
                $response['fecha_creacion'] = $poliza_row->fecha_creacion;
                $response['tipo'] = $poliza_row->tipo;

                $tipo_pol = "";
                if ($poliza_row->tipo = 1) {
                    $tipo_pol = "Diario";
                }
                if ($poliza_row->tipo = 2) {
                    $tipo_pol = "Ingresos";
                }
                if ($poliza_row->tipo = 3) {
                    $tipo_pol = "Egresos";
                }
                if ($poliza_row->tipo = 4) {
                    $tipo_pol = "Cheques";
                }
                $response['tipo_texto'] = $tipo_pol;
            } else {
                $error .= "poliza no encontrada ";
                $bandera = 1;
                $response['error'] = true;
                $response['mensaje'] = $error;
            }
        } else {
            $error .= "ingrese una poliza ";
            $bandera = 1;
            $response['error'] = true;
            $response['mensaje'] = $error;
        }

        echo json_encode($response);
    }
}