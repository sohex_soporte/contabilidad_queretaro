<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Domicilios extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function getEstados_get(){
        $this->load->model('CaDomicilioEstados_model');
        $this->data['data'] = $this->CaDomicilioEstados_model->getAll();
        $this->response($this->data);
    }


    public function getMunicipios_get(){
        $this->load->model('CaDomicilioMunicipios_model');
        $this->data['data'] = $this->CaDomicilioMunicipios_model->getAll([
            'id_estado' => $this->input->get('id')
        ]);
        // utils::pre($this->db->last_query());
        $this->response($this->data);
    }

    public function getColonias_get(){
        $this->load->model('CaDomiciliosColonias_model');
        $this->data['data'] = $this->CaDomiciliosColonias_model->getAll([
            'id_municipio' => $this->input->get('id')
        ]);
        // utils::pre($this->db->last_query());
        $this->response($this->data);
    }


}