<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_factura_ser_ref extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
    /*ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    */

  }

  function redondeado($numero, $decimales)
  {
    $factor = pow(10, $decimales);
    return (round($numero * $factor) / $factor);
  }

  function truncateFloat($numero, $digitos)
  {
    $truncar = 10 ** $digitos;
    return intval($numero * $truncar) / $truncar;
  }

  public function generar_factura($id_factura)
  {

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $this->load->model('FMfactura', '', TRUE);
    $factura_nueva = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura');



    $cfdi = $this->FMfactura;
    $cfdi->cargarInicio();
    $cfdi->cabecera();

    $cfdi->fact_serie        = $factura_nueva->factura_serie;                             // 4.1 Número de serie.
    $cfdi->fact_folio        = $factura_nueva->factura_folio;             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
    $cfdi->NoFac             = $cfdi->fact_serie . $cfdi->fact_folio;         // 4.3 Serie de la factura concatenado con el número de folio.
    $cfdi->fact_tipcompr     = "I";                             // 4.4 Tipo de comprobante.
    $cfdi->fact_exportacion  = "01";                            // 4.5 Atributo requerido para expresar si el comprobante ampara una operación de exportación.
    $cfdi->tasa_iva          = 16;                              // 4.6 Tasa del impuesto IVA.
    $cfdi->subTotal          = $factura_nueva->subtotal; //$this->Mgeneral->factura_subtotal($id_factura);                              // 4.7 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    $cfdi->descuento         = $factura_nueva->descuento;                               // 4.8 Descuento (se calculan mas abajo).
    $cfdi->IVA               = $factura_nueva->iva; //$this->Mgeneral->factura_iva_total($id_factura);                               // 4.9 IVA, suma de los impuestos (se calculan mas abajo).
    $cfdi->total             = $factura_nueva->total; //$this->truncateFloat($this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura), 2)  ;                            // 4.10 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    $cfdi->fecha_fact        = date("Y-m-d") . "T" . date("H:i:s"); // 4.11 Fecha y hora de facturación.
    $cfdi->NumCtaPago        = $factura_nueva->NumCtaPago;                          // 4.12 Número de cuenta (sólo últimos 4 dígitos, opcional).
    $cfdi->condicionesDePago = $factura_nueva->condicionesDePago;                   // 4.13 Condiciones de pago.
    $cfdi->formaDePago       = $factura_nueva->factura_formaPago; //"01";                            // 4.14 Forma de pago.
    $cfdi->metodoDePago      = $factura_nueva->factura_medotoPago; //"PUE";                           // 4.15 Clave del método de pago. Consultar catálogos de métodos de pago del SAT.
    $cfdi->TipoCambio        = 1;                               // 4.16 Tipo de cambio de la moneda.
    $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;// "45079";                         // 4.17 Lugar de expedición (código postal).
    $cfdi->moneda            = "MXN";                           // 4.18 Moneda
    $cfdi->totalImpuestosRetenidos   = 0;                       // 4.19 Total de impuestos retenidos (se calculan mas abajo).
    $cfdi->totalImpuestosTrasladados = 0;

    $cfdi->datosGenerales();



    ### 9. DATOS GENERALES DEL EMISOR #################################################  
    $cfdi->emisor_rs = "ESCUELA KEMPER URGATE";  // 9.1 Nombre o Razón social.
    $cfdi->emisor_rfc = "EKU9003173C9";  // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
    $cfdi->emisor_ClaRegFis = "601"; // 9.3 Clave del Régimen fiscal.  
    $cfdi->datosEmisor();

    ### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
    $cfdi->receptor_rfc = $factura_nueva->receptor_RFC;// "MASO451221PM4";  // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
    $cfdi->receptor_rs  = $factura_nueva->receptor_nombre;//"MARIA OLIVIA MARTINEZ SAGAZ";  // 10.4 Nombre o razón social.
    $cfdi->DomicilioFiscalReceptor = $factura_nueva->DomicilioFiscalReceptor;//"80290";             // 10.5 Domicilio fiscal del Receptor (código postal).
    $cfdi->RegimenFiscalReceptor =  $factura_nueva->RegimenFiscalReceptor;//"616";                 // 10.6 Régimen fiscal del receptor.
    $cfdi->UsoCFDI = $factura_nueva->receptor_uso_CFDI;//"S01";                               // Uso del CFDI.
    $cfdi->datosReceptor();



    $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId', $id_factura, 'factura_conceptos');
    $cfdi->inicia_conceptos();
    foreach ($conceptos_factura as $concepto_fac) :


      $concepto = array();
      $concepto['ClaveProdServ'] = $concepto_fac->clave_sat;
      $concepto['NoIdentificacion'] = $concepto_fac->concepto_NoIdentificacion;
      $concepto['Cantidad'] = $concepto_fac->concepto_cantidad;
      $concepto['ClaveUnidad'] = $concepto_fac->unidad_sat;
      $concepto['Unidad'] = $concepto_fac->id_producto_servicio_interno;
      $concepto['Descripcion'] = $concepto_fac->concepto_nombre;
      $concepto['ValorUnitario'] = $concepto_fac->concepto_precio;
      $concepto['Importe'] = $concepto_fac->concepto_importe;
      $concepto['Descuento'] = $concepto_fac->concepto_descuento;
      $concepto['ObjetoImp'] = $concepto_fac->ObjetoImp;//'02';



      $impuesto = array();
      $impuesto['TipoFactor'] = "Tasa";
      $impuesto['Base'] = $concepto_fac->concepto_importe;
      $impuesto['Impuesto'] = '002';
      $impuesto['TasaOCuota'] = '0.160000';
      $impuesto['Importe'] = $concepto_fac->importe_iva;

      $concepto['traslado'] = $impuesto;
      $cfdi->conceptos($concepto); // agrega los conceptos y los impuestos

    endforeach;


    $cfdi->impuestosGeneral(); // agrega impuestos generales
    $cfdi->sellar();




    //$cfdi->MfacturaContado->imprimir();
    //die();





    if ($cfdi->obtenerXml()) {
      //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
      //file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      file_put_contents("/var/www/web/dms/contabilidad_queretaro/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      //file_put_contents("https://planificadorempresarial.com/facturacion/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
      $data_url_prefactura['url_prefactura'] = base_url() . "statics/facturas/prefactura/$id_factura.xml";
      $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
      $this->Mgeneral->update_table_row('factura', $data_url_prefactura, 'facturaID', $id_factura);
      $this->enviar_factura($id_factura);
      //echo $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";
    } else {
      $data_respone['error'] = 1;
      $data_respone['error_mensaje'] = "Ocurrio un error";
      $data_respone['factura'] = $id_factura;


      echo json_encode($data_respone);
      die;
    }
    //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
    //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
    //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

    //$this->enviar_factura($id_factura);

    die;

    // Mostrar objeto que contiene los datos del CFDI
    print_r($cfdi);


    die;



    die('OK');
  }


  public function enviar_factura($id_factura)
  {
    //echo "".base_url()."index.php/factura/generar_factura/".$id_factura;
    //echo  $datos_fac->url_prefactura;
    $datos_fac = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura');

    # Username and Password, assigned by FINKOK
    $username = 'liberiusg@gmail.com';
    $password = '*Libros7893811';

    # Read the xml file and encode it on base64
    $invoice_path = "/var/www/web/dms/contabilidad_queretaro/statics/facturas/prefactura/$id_factura.xml"; //$this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
    $xml_file = fopen($invoice_path, "rb");
    $xml_content = fread($xml_file, filesize($invoice_path));
    fclose($xml_file);

    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$xml_content = base64_encode($xml_content);

    # Consuming the stamp service
    $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
    $client = new SoapClient($url);

    $params = array(
      "xml" => $xml_content,
      "username" => $username,
      "password" => $password
    );
    $response = $client->__soapCall("stamp", array($params));
    //print_r($response);
    ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
    //print $response->stampResult->xml;

    ####mostrar el código de error en caso de presentar alguna incidencia
    #print $response->stampResult->Incidencias->Incidencia->CodigoError;
    ####mostrar el mensaje de incidencia en caso de presentar alguna
    if (isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)) {
      $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
      //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $this->Mgeneral->update_table_row('factura', $data_sat, 'facturaID', $id_factura);
      $data_respone['error'] = 1;
      $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      $data_respone['factura'] = $id_factura;
      echo json_encode($data_respone);
      exit();
      die;
    } else {
      $data_sat['sat_uuid'] = $response->stampResult->UUID;
      $data_sat['sat_fecha'] = $response->stampResult->Fecha;
      $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
      $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
      $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
      $data_sat['sat_error'] = "0";
      $data_sat['sat_codigo_error'] = "0";
      $this->Mgeneral->update_table_row('factura', $data_sat, 'facturaID', $id_factura);
      //file_put_contents($this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
      file_put_contents("/var/www/web/dms/contabilidad_queretaro/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
      $data_respone['error'] = 0;
      $data_respone['error_mensaje'] = "Factura timbrada";
      $data_respone['factura'] = $id_factura;
      $data_respone['sat_uuid'] = $response->stampResult->UUID;
      $data_respone['pdf'] = "" . base_url() . "index.php/facturacion/genera_pdf_old/" . $id_factura . "";
      $data_respone['xml'] = "" . base_url() . "statics/facturas/facturas_xml/" . $id_factura . ".xml";
      echo json_encode($data_respone);
      exit();
      die;
    }
    #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
  }


  

  public function cancelar_factura($id_factura)
  {
    $factura_datos = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura');
    # Generar el certificado y llave en formato .pem
    shell_exec("openssl x509 -inform DER -outform PEM -in " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.cer -pubkey -out " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.cer.pem");
    shell_exec("openssl pkcs8 -inform DER -in " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.key -passin pass:12345678a -out " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.key.pem");
    shell_exec("openssl rsa -in " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.key.pem -des3 -out " . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.enc -passout pass:*Libros7893811");

    # Username and Password, assigned by FINKOK
    $username = 'liberiusg@gmail.com';
    $password = '*Libros7893811';
    # Consuming the cancel service
    # Read the x509 certificate file on PEM format and encode it on base64
    $cer_path = '' . $this->config->item('url_real') . 'cfdi41/archs_pem/EKU9003173C9.cer.pem'; //$this->config->item('url_real').'cfdi/LAN7008173R5.cer.pem';
    $cer_file = fopen($cer_path, "r");
    $cer_content = fread($cer_file, filesize($cer_path));
    fclose($cer_file);
    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$cer_content = base64_encode($cer_content);

    # Read the Encrypted Private Key (des3) file on PEM format and encode it on base64
    $key_path = "" . $this->config->item('url_real') . "cfdi41/archs_pem/EKU9003173C9.enc"; //$this->config->item('url_real')."cfdi/LAN7008173R5.enc";
    $key_file = fopen($key_path, "r");
    $key_content = fread($key_file, filesize($key_path));
    fclose($key_file);
    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$key_content = base64_encode($key_content);

    $taxpayer_id = 'EKU9003173C9'; # The RFC of the Emisor
    //$invoices = array(); # A list of UUIDs
    //$invoices = array("UUID" => "".$factura_datos->sat_uuid."", "Motivo" => "02", "FolioSustitucion" => "");
    $uuids = array("UUID" => "" . $factura_datos->sat_uuid . "", "Motivo" => "02", "FolioSustitucion" => "");
    $uuid_ar = array('UUID' => $uuids);

    // var_dump($invoices);

    $url = "https://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl";
    $client = new SoapClient($url);
    $params = array(
      "UUIDS" =>  $uuid_ar,
      "username" => $username,
      "password" => $password,
      "taxpayer_id" => $taxpayer_id,
      "cer" => $cer_content,
      "key" => $key_content,
      "get_sat_status" => false
    );
    $response = $client->__soapCall("cancel", array($params));
    // print_r($response);

    //print_r($params);
    //echo $response->cancelResult->CodEstatus ;
    //die;

    /*  echo "<br/>";
          echo "<br/>";
          echo $response->cancelResult->Fecha;
          echo "<br/>";
          echo "<br/>";
          echo $response->cancelResult->Folios->Folio->UUID;
          echo "<br/>";
          echo "<br/>";
          echo $response->cancelResult->Folios->Folio->EstatusUUID;
          echo "<br/>";
          echo "<br/>";
          echo $response->cancelResult->Folios->Folio->EstatusCancelacion;
          echo "<br/>";
          echo "<br/>";
          echo $response->cancelResult->RfcEmisor;
          */
    // var_dump($response);
    //die();
    if (is_object($response)) {
      if ($response->cancelResult->Folios->Folio->EstatusUUID == "201") {
        $data_cancelar['cancelar_Fecha'] = $response->cancelResult->Fecha;
        $data_cancelar['cancelar_UUID'] = $response->cancelResult->Folios->Folio->UUID;
        $data_cancelar['cancelar_EstatusUUID'] = $response->cancelResult->Folios->Folio->EstatusUUID;
        $data_cancelar['cancelar_EstatusCancelacion'] = $response->cancelResult->Folios->Folio->EstatusCancelacion;
        $data_cancelar['cancelar_RfcEmisor'] = $response->cancelResult->RfcEmisor;
        $data_cancelar['acuse_cancelacion'] = $response->cancelResult->Acuse;

        $this->Mgeneral->update_table_row('factura', $data_cancelar, 'facturaID', $id_factura);
        //redirect("factura/lista");
        $data_cancelar['facturaID'] = $id_factura;
        $data_cancelar['error'] = false;
        echo json_encode($data_cancelar);
      } else {
        $data_cancelar['error'] = true;
        $data_cancelar['mensaje'] = "error1";
        echo json_encode($data_cancelar);
      }
    } else {
      $data_cancelar['error'] = true;
      $data_cancelar['mensaje'] = "error2";
      echo json_encode($data_cancelar);
    }

    exit();
  }

  public function acuse_cancelacion_xml($id_factura)
  {
    $factura_datos = $this->Mgeneral->get_row('facturaID', $id_factura, 'factura');
    header('Content-type: application/xml; charset=UTF-8');
    header('Content-Disposition: attachment; filename="xml/' . $id_factura . '.xml');
    if (is_object($factura_datos)) {
      print_r($factura_datos->acuse_cancelacion);
    } else {
      echo "Error al generar xml";
    }
    exit();
  }


  public function api_genera_factura()
  {

   
    $json1 = file_get_contents('php://input');   
   

    // Converts it into a PHP object
    $factura_campo = json_decode($json1);
   // var_dump($factura_campo);

    $emisor = $this->Mgeneral->get_row('id', 1, 'datos');
    $id_factura = get_guid();
    $data['facturaID'] = $id_factura;
    $data['status'] = 1;
    $data['emisor_RFC'] = $emisor->rfc;
    $data['emisor_regimenFiscal'] = $emisor->regimen;
    $data['emisor_nombre'] = $emisor->razon_social;
    $data['receptor_uso_CFDI'] = "G03";
    $data['pagada'] = "SI";

    //$data['sucursal'] = $this->validar_capo_json($factura_campo->sucursal);
    //$data['tipo'] = $this->validar_capo_json($factura_campo->tipo);
    $this->Mgeneral->save_register('factura', $data);

    $data_datos['receptor_nombre'] = $this->validar_capo_json($factura_campo->receptor_nombre, 'receptor_nombre');
    //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
    $data_datos['factura_moneda'] = 'MXN'; //$this->validar_capo_json($factura_campo->moneda);
    $data_datos['fatura_lugarExpedicion'] = $this->validar_capo_json($factura_campo->LugarExpedicion, 'fatura_lugarExpedicion');
    $data_datos['factura_fecha'] = date('Y-m-d H:i:s'); //$this->input->post('numero');
    $data_datos['receptor_email'] =  $this->validar_capo_json($factura_campo->receptor_email, 'receptor_email');
    $data_datos['factura_folio'] = $this->validar_capo_json($factura_campo->Folio, 'Folio');
    $data_datos['factura_serie'] = $this->validar_capo_json($factura_campo->Serie, 'Serie');
    $data_datos['receptor_direccion'] = $this->validar_capo_json($factura_campo->receptor_direccion, 'receptor_direccion');
    $data_datos['factura_formaPago'] = $this->validar_capo_json($factura_campo->FormaPago, 'factura_formaPago');
    $data_datos['factura_medotoPago'] = $this->validar_capo_json($factura_campo->MetodoPago, 'factura_medotoPago');
    $data_datos['factura_tipoComprobante'] = $this->validar_capo_json($factura_campo->TipoDeComprobante, 'factura_tipoComprobante');
    $data_datos['receptor_RFC'] = $this->validar_capo_json($factura_campo->receptor_RFC, 'receptor_RFC');
    $data_datos['receptor_uso_CFDI'] = $this->validar_capo_json($factura_campo->receptor_uso_CFDI, 'receptor_uso_CFDI');

    $data_datos['DomicilioFiscalReceptor'] = $this->validar_capo_json($factura_campo->DomicilioFiscalReceptor, 'DomicilioFiscalReceptor');             // 10.5 Domicilio fiscal del Receptor (código postal).
    $data_datos['RegimenFiscalReceptor'] = $this->validar_capo_json($factura_campo->RegimenFiscalReceptor, 'RegimenFiscalReceptor');     
    //$data['pagada'] = $this->input->post('pagada');
    $data_datos['comentario'] = $this->validar_capo_json($factura_campo->comentario, 'comentario');
    $data_datos['descuento'] = $this->validar_capo_json($factura_campo->descuento, 'descuento');
    $data_datos['condicionesDePago'] = $this->validar_capo_json($factura_campo->condicionesDePago, 'condicionesDePago');
    $data_datos['NumCtaPago'] = $this->validar_capo_json($factura_campo->NumCtaPago, 'NumCtaPago'); 
    //$data['almacen'] = $this->input->post('almacen');

   

    $data_datos['total'] = $factura_campo->total;
    $data_datos['subtotal'] = $factura_campo->subtotal;
    $data_datos['iva'] = $factura_campo->iva;

    $this->Mgeneral->update_table_row('factura', $data_datos, 'facturaID', $id_factura);

    foreach ($factura_campo->conceptos as $concepto_fac) :

      $data_conceptos['concepto_facturaId'] = $id_factura;
      $data_conceptos['concepto_NoIdentificacion'] = $this->validar_capo_json($concepto_fac->concepto_NoIdentificacion, 'concepto_NoIdentificacion');
      $data_conceptos['concepto_unidad'] = $this->validar_capo_json($concepto_fac->unidad_interna, 'concepto_unidad');
      $data_conceptos['concepto_descuento'] = $this->validar_capo_json($concepto_fac->concepto_descuento, 'concepto_descuento');
      $data_conceptos['clave_sat'] = $this->validar_capo_json($concepto_fac->clave_sat, 'clave_sat');
      $data_conceptos['unidad_sat'] = $this->validar_capo_json($concepto_fac->unidad_sat, 'unidad_sat');
      $data_conceptos['concepto_nombre'] = $this->validar_capo_json($concepto_fac->concepto_nombre, 'concepto_nombre');
      $data_conceptos['concepto_precio'] = $this->validar_capo_json($concepto_fac->concepto_precio, 'concepto_precio');
      $data_conceptos['concepto_importe'] = $this->validar_capo_json($concepto_fac->concepto_importe, 'concepto_importe');
      $data_conceptos['impuesto_iva'] = $this->validar_capo_json($concepto_fac->concepto_importe, 'impuesto_iva');
      $data_conceptos['impuesto_iva_tipoFactor'] = 0; //$this->input->post('impuesto_iva_tipoFactor');
      $data_conceptos['impuesto_iva_tasaCuota'] = 0; //$this->input->post('impuesto_iva_tasaCuota');
      $data_conceptos['impuesto_ISR'] = 0; //$this->input->post('impuesto_ISR');
      $data_conceptos['impuesto_ISR_tasaFactor'] = $this->validar_capo_json($concepto_fac->TipoFactor, 'impuesto_ISR_tasaFactor');
      $data_conceptos['impuestoISR_tasaCuota'] = $this->validar_capo_json($concepto_fac->TasaOCuota, 'impuestoISR_tasaCuota');
      $data_conceptos['tipo'] = '0'; //$this->input->post('tipo');
      $data_conceptos['nombre_interno'] = $this->validar_capo_json($concepto_fac->concepto_nombre, 'nombre_interno');
      $data_conceptos['id_producto_servicio_interno'] = $this->validar_capo_json($concepto_fac->id_producto_servicio_interno, 'id_producto_servicio_interno');
      $data_conceptos['concepto_cantidad'] = $this->validar_capo_json($concepto_fac->concepto_cantidad, 'concepto_cantidad');
      $data_conceptos['importe_iva'] = $this->validar_capo_json($concepto_fac->Importe, 'importe_iva');
      $data_conceptos['fecha_creacion'] = date('Y-m-d H:i:s');
      $data_conceptos['ObjetoImp'] = $this->validar_capo_json($concepto_fac->ObjetoImp, 'ObjetoImp');
      $this->Mgeneral->save_register('factura_conceptos', $data_conceptos);

    endforeach;

    $this->generar_factura($id_factura);
    //$this->generar_prefactura($id_factura);
  }

  function validar_capo_json($campo, $label)
  {
    try {
      if (!isset($campo)) {
        throw new Exception("Falta campo  => " . $label);
      }
      return $campo;
    }
    //catch exception
    catch (Exception $e) {
      echo 'Message: ' . $e->getMessage();
      die();
    }
  }

}
