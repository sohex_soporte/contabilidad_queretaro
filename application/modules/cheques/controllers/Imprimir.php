<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Imprimir extends MX_Controller {

    private $options;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){

        $id = $this->input->get('id');

        $this->load->model('DeDatosCheque_model');
        $cheque_datos = $this->DeDatosCheque_model->get_detalle([
            'de_datos_cheque.id' => $id
        ]);

        $this->load->model('Asientos_model');
        $asientos_datos = $this->Asientos_model->get_detalle([
            'transaccion_id' => $cheque_datos['transaccion_id']
        ]);
        
        $total = [
            'cargo' => 0,
            'abono' => 0
        ];
        if (is_array($asientos_datos) && count($asientos_datos) > 0) {
            foreach ($asientos_datos as $value) {
                $total['cargo'] += utils::numberPrecision($value['cargo'], 3);
                $total['abono'] += utils::numberPrecision($value['abono'], 3);
            }
        }

        $formatter = new Luecano\NumeroALetras\NumeroALetras();
        if($value['abono'] > 0){
            $letras = $formatter->toMoney($total['abono'], 2);
        }else{
            $letras = $formatter->toMoney($total['cargo'], 2);
        }

        $total_cheque = 0;
        if($value['abono'] > 0){
            $total_cheque = $total['abono'];
        }else{
            $total_cheque = $total['cargo'];
        }


        $data_content = [
            'datos_cheque_id'  => $id,
            'cheque' => $cheque_datos,
            'asientos' => $asientos_datos,
            'total' => $total,
            'letras' => $letras,
            'total_cheque' => $total_cheque
        ];
        // utils::pre($data_content);
        
        $this->options = new Dompdf\Options();

        $this->options->set('isHtml5ParserEnabled', 'true');
        $this->options->set('chroot', realpath( __DIR__ ));
        $this->options->set('enable_remote', true);
        $this->options->set('enable_php', true);

        $dompdf = new Dompdf\Dompdf($this->options);


		$html = $this->blade->render('/imprimir/index3',$data_content,true);
        //echo $html;exit();
		$dompdf->loadHtml($html);
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('letter');
		// Render the HTML as PDF
		$dompdf->render();
		// Get the generated PDF file contents
		$pdf = $dompdf->output();
		// Output the generated PDF to Browser
		//$dompdf->stream();
        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
        exit();

    }
}