<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Anticipos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){

        $listado = utils::api_get([
                'url' => 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/anticipos/get-filter?cliente_id=&estatus_id='
        ]);

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Folio',
            'Cliente',
            'Total',
            // 'Proceso',
            'Estatus',
            'Acciones'
        ));

        if(is_array($listado) && count($listado)>0){

            $this->load->model('CaTransacciones_model');

            foreach ($listado as $key => $value) {

                // $this->db->where('ca_transacciones.deleted_at IS NULL',null, false);
                $transaccion_datos = $this->CaTransacciones_model->get([
                    'anticipo_id' => $value['id']
                ]);
                $cheque_datos = false;
                if(is_array($transaccion_datos)){
                    $this->load->model("DeDatosCheque_model");
                    $cheque_datos = $this->DeDatosCheque_model->get([
                        'transaccion_id' => $transaccion_datos['id']
                    ]); 
                    if(is_array($cheque_datos)){
                        $cheque_datos = '<button title="Imprimir" type="button" data-renglon_id="'.$cheque_datos['id'].'" class="btn btn-secondary btn-sm ml-2" onclick="APP.administrar_cheque(this);" ><i class="fas fa-print"></i></div>';
                    }
                }

                if($transaccion_datos == false){
                    $acciones = '<button type="button" data-renglon_id="'.$value['id'].'" onclick="APP.modal_detalle_anticipo(this);" class="btn btn-primary btn-sm ml-2"> <i class="fas fa-money-check-alt"></i> </button>';
                }else{
                    $acciones = '
                        <button title="Ver contenido cheque" type="button" data-renglon_id="'.$transaccion_datos['id'].'" onclick="APP.modal_detalle_anticipo2(this);" class="btn btn-primary btn-sm  ml-2"> <i class="fas fa-file-alt"></i> </button>
                        '.$cheque_datos.'
                    ';
                }
               
                $this->table->add_row(array(
                        utils::folio($value['folio_id'],6),
                        ''.$value['numero_cliente'].'<br/>'.''.$value['nombre'].' '.$value['apellido_paterno'].' '.$value['apellido_materno'].'',
                        utils::format($value['total']),
                        // $value['proceso'],
                        $value['estatus'],
                        $acciones
                ));
                
            }
        }else{
            $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 5);
            $this->table->add_row($cell);
        }



        $content = array(
            'tabla' => $this->table->generate(),
        );

        $this->blade->render('/anticipos/index',$content);
    }

    public function buscar_cheque(){
        $id = $this->input->get('id');
        
        $this->load->model('DeDatosCheque_model');
        $cheque_datos = $this->DeDatosCheque_model->get_detalle([
            'de_datos_cheque.transaccion_id' => $id
        ]);

        redirect("cheques/asientos?id=".$cheque_datos['datos_cheque_id'].'&devolucion=1');
    }

    public function detalle($id_anticipo){

        $id_anticipo = base64_decode($id_anticipo);

        $anticipo_datos = utils::api_get([
            'url' => 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/anticipos/'.$id_anticipo
        ]);

        $cliente_datos = array();
        $polizaNomenclatura = '';
        $fecha_creacion = '';

        if(is_array($anticipo_datos) && array_key_exists('cliente_id',$anticipo_datos) ){
            $cliente_id = $anticipo_datos['cliente_id'];
            $folio_id = $anticipo_datos['folio_id'];

            $cliente_datos = utils::api_get([
                'url' => 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/clientes/'.$cliente_id
            ]);

            if(is_array($cliente_datos) && count($cliente_datos) > 0 ){
                $cliente_datos = current($cliente_datos);
            }

            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get([
                'cliente_id' => $cliente_id,
                'folio' => $folio_id,
                //'origen' => 'DEVOLUCION_ANTICIPO'
            ]);

            if(is_array($transaccion_datos)){
                $this->load->model('Asientos_model');
                $asiento_datos = $this->Asientos_model->get([
                    'transaccion_id' => $transaccion_datos['id'],
                    // 'estatus_id' => 'APLICADO'
                ]);

                if(is_array($asiento_datos)){
                    $this->load->model('CaPolizas_model');
                    $poliza_datos = $this->CaPolizas_model->get([
                        'id' => $asiento_datos['polizas_id'],
                    ]);

                    if(is_array($poliza_datos)){
                        $polizaNomenclatura = $poliza_datos['PolizaNomenclatura_id'];
                        $fecha_creacion = $poliza_datos['fecha_creacion'];
                    }
                }
            }
        }

        

        $content = array(
            'identificador' => $id_anticipo,
            'cliente_datos' => $cliente_datos,
            'anticipo_datos' => $anticipo_datos,
            'polizaNomenclatura' => $polizaNomenclatura,
            'fecha_creacion' => $fecha_creacion
        );
        //  utils::pre($content);
        $this->blade->render('/anticipos/detalle',$content);

        

    }
  
}