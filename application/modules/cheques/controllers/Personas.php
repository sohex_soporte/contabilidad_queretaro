<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Personas extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index($tipo = 'proveedor'){

        $this->load->model('CaPolizasNomenclaturas_model');
        $nomenclaturas = $this->CaPolizasNomenclaturas_model->getAll();
        
        $content = array(
             'tipo_persona_id' => (($tipo == 'proveedor')? 2 : 1),
            'nomenclaturas' => $nomenclaturas
        );

        $this->blade->render('/personas/index',$content);
    }

    public function nuevo($tipo_persona = 'proveedor'){
        $this->load->model('CaTipoPersonas_model');
        $listado = $this->CaTipoPersonas_model->getAll();

        $this->load->model('CaPolizasNomenclaturas_model');
        $listado_polizas = $this->CaPolizasNomenclaturas_model->getAll();
        
        $this->load->model('CaPolizasFijas_model');
        $listado_polizas_fijas = $this->CaPolizasFijas_model->getAll();

        $this->load->model('CaCuentas_model');
        $cuentas = $this->CaCuentas_model->getAll();
        // utils::pre($listado_polizas_fijas);

        $tipo = array(
            ['id' => 1, 'nombre' =>'Abono'],
            ['id' => 2, 'nombre' =>'Cargo'],
        );


        $data_content = [
            'tipo_persona' => ($tipo_persona == 'cliente') ? 1 : 2,
            'listado' => $listado,
            'cuentas' => $cuentas,
            'listado_polizas' => $listado_polizas,
            'listado_polizas_fijas' => $listado_polizas_fijas,
            'tipo'  => $tipo,
            'identificador'  => utils::guid()
        ];
        $this->blade->render('/personas/nuevo',$data_content);
    }

    public function editar(){
        $id = $this->input->get('id');

        $this->load->model('CaTipoPersonas_model');
        $listado = $this->CaTipoPersonas_model->getAll();

        $this->load->model('CaPersonas_model');
        $datos = $this->CaPersonas_model->get([
            'id' => $id
        ]);


        $this->load->model('DePersonasXmlConfig_model');
        $datos_config = $this->DePersonasXmlConfig_model->get([
            'id_persona' => $id
        ]);
        if($datos_config == false){
            $this->DePersonasXmlConfig_model->insert([
                'id_persona' => $id
            ]); 
            $datos_config = $this->DePersonasXmlConfig_model->get([
                'id_persona' => $id
            ]);
        }
        
        $this->load->model('CaPolizasNomenclaturas_model');
        $listado_polizas = $this->CaPolizasNomenclaturas_model->getAll();
        
        $this->load->model('CaPolizasFijas_model');
        $listado_polizas_fijas = $this->CaPolizasFijas_model->getAll();

        $this->load->model('CaCuentas_model');
        $cuentas = $this->CaCuentas_model->getAll();
        // utils::pre($listado_polizas_fijas);

        $tipo = array(
            ['id' => 1, 'nombre' =>'Abono'],
            ['id' => 2, 'nombre' =>'Cargo'],
        );
        
        $datos_credito = utils::api_get([
            'url' => 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/clientes/datos-credito?rfc=' . $datos['rfc']
        ]);
        


        $data_content = [
            'datos_credito' => $datos_credito,
            'tipo_persona' => ($datos['tipo_persona_id']),
            'listado' => $listado,
            'cuentas' => $cuentas,
            'datos_config' => $datos_config,
            'listado_polizas' => $listado_polizas,
            'listado_polizas_fijas' => $listado_polizas_fijas,
            'datos' => $datos,
            'identificador'  => $id,
            'tipo'  => $tipo
        ];
        $this->blade->render('/personas/editar',$data_content);
    }

    public function asientos(){
        $id = $this->input->get('id');

        $this->load->model('DeDatosCheque_model');
        $cheque_datos = $this->DeDatosCheque_model->get_detalle([
            'de_datos_cheque.id' => $id
        ]);

        $data_content = [
            'datos_cheque_id'  => $id,
            'polizas_id'  => $cheque_datos['polizas_id'],
            'transaccion_id'  => $cheque_datos['transaccion_id'],
            'cheque' => $cheque_datos,
        ];
        $this->blade->render('/cheques/asientos',$data_content);
    }


    

    public function consultar($id){

      $this->load->model('DeFacturas_model');
      $factura_datos = $this->data =  $this->DeFacturas_model->detalle([
          'de_facturas.id' => $id
      ]);
      

      $this->load->model('Asientos_model');
      $listado_asientos = $this->data =  $this->Asientos_model->get_detalle([
          'transaccion_id' => $factura_datos['transaccion_id']
      ]);

      # FORMATEADO DE TABLA
      $this->load->library('table');

      $template = array(
          'table_open'            => '<table class="table table-striped">'
      );
      $this->table->set_template($template);

      $this->table->set_heading(array('No. asiento', 'Concepto', 'Cuenta','Departamento','Abono','Cargo'));

      $monto_abono = 0;
      $monto_cargo = 0;
      foreach ($listado_asientos as $key => $value) {
          $monto_abono = $monto_abono + $value['abono'];
          $monto_cargo = $monto_cargo + $value['cargo'];

          $this->table->add_row([
              '<b>'.utils::folio($value['asiento_id'],6).'</b>',
              $value['concepto'],
              $value['cuenta'].'<br/><small><b>'.$value['cuenta_descripcion'].'</b></small>',
              $value['departamento_descripcion'],
              utils::format($value['abono']),
              utils::format($value['cargo'])
          ]);
      }

      $cell = array('data' => 'Total',  'colspan' => 4);
      $this->table->add_row($cell, utils::format($monto_abono), utils::format($monto_cargo));

      $tabla = $this->table->generate();


      $data = [
          'factura_datos' => $factura_datos,
          'listado_asientos' => $listado_asientos,
          'tabla_content' => $tabla
      ];

      $this->blade->render('/facturacion/consultar',$data);
      
  }
  
}