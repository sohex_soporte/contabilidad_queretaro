<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cheques extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function getId_transaccion(){
        
        $id_transaccion = $this->input->get('transaccion');
        $id_poliza = $this->input->get('poliza');
        //http://localhost/contabilidad_queretaro/index.phpcheques/nuevo?transaccion=021D2C19-8BDE-45A2-8826-0B911089E8A8&poliza=38
        $this->load->model('DeDatosCheque_model');
        $datos_cheque = $this->DeDatosCheque_model->get_detalle([
            'de_datos_cheque.transaccion_id' => $id_transaccion
        ]);

        if(is_array($datos_cheque)){
            redirect('cheques/asientos?id='.$datos_cheque['datos_cheque_id']);
        }else{
            redirect('cheques/nuevo?transaccion='.$id_transaccion.'&poliza='.base64_encode($id_poliza));
        }
        

    }

    public function index(){

        $this->session->set_userdata('poliza','cheque');

        $fecha_ini = $this->input->get('fecha_ini');
        $fecha_fin = $this->input->get('fecha_fin');
        $nomenclatura = $this->input->get('nomenclatura');
        $folio = $this->input->get('folio');

        // if($fecha_ini == false){
        //     $fecha_ini = utils::get_date();
        // }

        if($fecha_fin == false){
            $fecha_fin = utils::get_date();
        }

        $this->load->model('CaPolizasNomenclaturas_model');
        $nomenclaturas = $this->CaPolizasNomenclaturas_model->getAll();

        $params = http_build_query([
            'fecha_ini' => $fecha_ini,
            'fecha_fin' => $fecha_fin,
            'nomenclatura' => $nomenclatura,
            'folio' => $folio
        ]);
        // utils::pre($params);
        $response = utils::api_get([
            'url' => site_url('cheques/api/detalleAll?'.$params)
        ]);
        // utils::pre($response);

        $this->load->library('table');
        $template = array(
            'table_open' => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Folio',
            'Poliza',
            'Ejercicio',
            'Nombre',
            'RFC',
            'Abono',
            'Cargo',
            'Fecha de registro',
            'Acciones'
        ));

        $acumulado_abono = 0;
        $acumulado_cargo = 0;
        if(is_array($response) && array_key_exists('data',$response) && is_array($response['data']) && count($response['data'])>0){
            foreach ($response['data'] as $key => $value) {
                // dd($value);
                // $acumulado = $acumulado + ($value['abono'] - $value['cargo']);
                $acumulado_abono = $acumulado_abono + $value['abonos'];
                $acumulado_cargo = $acumulado_cargo + $value['cargos'];

                $this->table->add_row(array(
                    utils::folio($value['folio'],6),
                    $value['PolizaNomenclatura_id'].utils::folio($value['dia'],3),
                    $value['ejercicio'],
                    $value['nombre'].' '.$value['apellido1'].' '.$value['apellido2'],
                    $value['rfc'],

                    utils::format($value['abonos']),
                    utils::format($value['cargos']),

                    utils::aHora($value['datos_cheque_fecha_registro'],true).'<br/>'.
                    '<small>'.utils::aFecha($value['datos_cheque_fecha_registro'],true).'</small>',

                    '<button type="button" data-renglon_id="'.$value['datos_cheque_id'].'" title="Editar" class="btn btn-primary ml-2 btn-sm" onclick="APP.administrar_asientos(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>'.
                    (($value['id_datos_cheque_estatus'] == 2 && $value['tipo_movimeinto'] == 1)? '<button title="Imprimir" type="button" data-renglon_id="'.$value['datos_cheque_id'].'" class="btn btn-secondary ml-2 btn-sm" onclick="APP.administrar_cheque(this);" ><i class="fas fa-print"></i></div>' : '')
                ));
                
            }

            $cell = array('data' => 'Total', 'class' => '', 'colspan' => 5);
            $cell2 = array('data' => '', 'class' => '', 'colspan' => 2);
            $this->table->add_row($cell, utils::format($acumulado_abono) , utils::format( $acumulado_cargo),$cell2);

        }else{
            $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 9);
            $this->table->add_row($cell);
        }



        $content = array(
            'nomenclaturas' => $nomenclaturas,
            'listado' => $response,
            'tabla' => $this->table->generate(),
            'fecha_ini' => $fecha_ini,
            'fecha_fin' => $fecha_fin
        );

        $this->blade->render('/cheques/index',$content);
    }

    public function nuevo(){

        $transaccion_id = $this->input->get('transaccion');
        if($transaccion_id != false){
            $this->load->model('DeDatosCheque_model');
            $datos_cheque = $this->DeDatosCheque_model->get([
                'transaccion_id' => $transaccion_id
            ]);
            if($datos_cheque != false){
                $id = $datos_cheque['id'];
                redirect('cheques/asientos?id='.$id);
            }
        }
        

        $this->load->model('CaTipoPersonas_model');
        $listado = $this->CaTipoPersonas_model->getAll();

        $data_content = [
            'identificador'  => utils::guid(),
            'listado' => $listado
        ];
        $this->blade->render('/cheques/nuevo',$data_content);
    }

    public function asientos(){
        $id = $this->input->get('id');

        $this->load->model('DeDatosCheque_model');
        $cheque_datos = $this->DeDatosCheque_model->get_detalle([
            'de_datos_cheque.id' => $id
        ]);
        // utils::pre($this->db->last_query());

        $response = utils::api_get([
            'url' => site_url('asientos/api/detalle?transaccion_id='.$cheque_datos['transaccion_id'].'&estatus=ACTIVOS')
        ]);
        // utils::pre($response);

        $this->load->library('table');
        $template = array(
            'table_open'            => '<table class="table table-striped table-hover table-bordered">',
        );
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'No. Asiento',
            'Concepto',
            'Cuenta',
            'Cargo',
            'Abono',
            // 'Acumulado',
            // 'Fecha de registro'
            'Acciones'
        ));

        $acumulado = 0;
        $acumulado_abono = 0;
        $acumulado_cargo = 0;
        if(is_array($response) && array_key_exists('data',$response) && is_array($response['data']) && count($response['data'])>0){
            foreach ($response['data'] as $key => $value) {
                // $acumulado = $acumulado + ($value['abono'] - $value['cargo']);
                $acumulado_abono = $acumulado_abono + $value['abono'];
                $acumulado_cargo = $acumulado_cargo + $value['cargo'];

                $this->table->add_row(array(
                    utils::folio($value['asiento_id'],6),
                    $value['concepto'],
                    $value['cuenta'].'<small><br/>'.$value['cuenta_descripcion'].'</small>',
                    utils::format($value['cargo']),
                    utils::format($value['abono']),
                    // utils::format($acumulado),
                    '<button type="button" data-renglon_id="'.$value['asiento_id'].'" title="Editar" class="btn btn-primary ml-2 btn-sm" onclick="APP.modal_editar_asiento(this);" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></div>'.
                    '<button type="button" data-renglon_id="'.$value['asiento_id'].'" title="Eliminar" class="btn btn-danger ml-2 btn-sm" onclick="APP.eliminar_renglon(this);" ><i class="far fa-trash-alt"></i></div>'
                ));
                
            }

            if(is_array($response) && array_key_exists('total',$response) && is_array($response['total']) && count($response['total'])>0){
                $cell = array('data' => 'Total', 'class' => '', 'colspan' => 3);
                $this->table->add_row($cell, utils::format($response['total']['cargo']) , utils::format($response['total']['abono']),'');
            }
        }else{
            $cell = array('data' => '<center>No se encontraron registros</center>', 'class' => '', 'colspan' => 6);
            $this->table->add_row($cell);
        }

        $data_content = [
            'datos_cheque_id'  => $id,
            'polizas_id'  => $cheque_datos['polizas_id'],
            'transaccion_id'  => $cheque_datos['transaccion_id'],
            'acumulado_abono' => $acumulado_abono,
            'acumulado_cargo' => $acumulado_cargo,
            'cheque' => $cheque_datos,
            'tabla' => $this->table->generate()
        ];
        // utils::pre($data_content);
        $this->blade->render('/cheques/asientos',$data_content);
    }


    

    public function consultar($id){

      $this->load->model('DeFacturas_model');
      $factura_datos = $this->data =  $this->DeFacturas_model->detalle([
          'de_facturas.id' => $id
      ]);
      

      $this->load->model('Asientos_model');
      $listado_asientos = $this->data =  $this->Asientos_model->get_detalle([
          'transaccion_id' => $factura_datos['transaccion_id']
      ]);

      # FORMATEADO DE TABLA
      $this->load->library('table');

      $template = array(
          'table_open'            => '<table class="table table-striped">'
      );
      $this->table->set_template($template);

      $this->table->set_heading(array('No. asiento', 'Concepto', 'Cuenta','Departamento','Abono','Cargo'));

      $monto_abono = 0;
      $monto_cargo = 0;
      foreach ($listado_asientos as $key => $value) {
          $monto_abono = $monto_abono + $value['abono'];
          $monto_cargo = $monto_cargo + $value['cargo'];

          $this->table->add_row([
              '<b>'.utils::folio($value['asiento_id'],6).'</b>',
              $value['concepto'],
              $value['cuenta'].'<br/><small><b>'.$value['cuenta_descripcion'].'</b></small>',
              $value['departamento_descripcion'],
              utils::format($value['abono']),
              utils::format($value['cargo'])
          ]);
      }

      $cell = array('data' => 'Total',  'colspan' => 4);
      $this->table->add_row($cell, utils::format($monto_abono), utils::format($monto_cargo));

      $tabla = $this->table->generate();


      $data = [
          'factura_datos' => $factura_datos,
          'listado_asientos' => $listado_asientos,
          'tabla_content' => $tabla
      ];

      $this->blade->render('/facturacion/consultar',$data);
      
  }
  
}