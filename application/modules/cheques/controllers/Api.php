<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public $http_status = 200;
    public $type = 'api';
    public $data = array();

    function __construct()
    {
        parent::__construct();

        $this->data = array(
            'status' => 'success',
            'data' => false,
            'message' => false
        );
    }

    public function comprobande_delete_get(){
        $datos_cheque_id = $this->input->get('datos_cheque_id');
        $this->load->model('DeDatosCheque_model');
        $response=$this->DeDatosCheque_model->update([
            'archivo_comprobante' => null
        ],[
            'id' => $datos_cheque_id
        ]);
        echo json_encode([
            'response' => $response
        ]);
        exit();
    }

    public function comprobande_upload_post(){
        $datos_cheque_id = $this->input->get('datos_cheque_id');
        

        $fileTmpPath = $_FILES['myfile']['tmp_name'];
        $fileName = $_FILES['myfile']['name'];
        $fileSize = $_FILES['myfile']['size'];
        $fileType = $_FILES['myfile']['type'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));
        
        // Directorio a copiar la imagen
        $original = str_replace('-','_',utils::guid()).'.'.$fileExtension;
        $destino_path = PATH_CHEQUE_COMPROBANTE.$original;
        
        move_uploaded_file($fileTmpPath, $destino_path);

        $this->load->model('DeDatosCheque_model');
        $this->DeDatosCheque_model->update([
            'archivo_comprobante' => $original
        ],[
            'id' => $datos_cheque_id
        ]);

        echo json_encode([
            'nombre' => $fileName,
            'original' => $original
        ]);
        exit();

    }

    public function listado_personas_get(){

        $tipo_persona_id = $this->input->get('tipo_persona_id');
        
        $this->load->model('CaPersonas_model');
        $this->data['data'] = $this->CaPersonas_model->getAll([
            'ca_personas.tipo_persona_id' => $tipo_persona_id
        ]);
        
        $this->response($this->data);
    }

    private function cache_listado_proveedores_dms(){

        $listado_proveedores = [];
        $this->load->driver('cache');
        $listado = utils::api_get([
            'url' => URL_DMS.'api/catalogo-proveedor'
        ]);

        if(is_array($listado) && count($listado)>0){
            foreach ($listado as $key => $value) {
                $listado_proveedores[ $value['proveedor_rfc'] ] = [
                    'id' => $value['id'],
                    'rfc' => $value['proveedor_rfc'],
                    'nombre' => $value['proveedor_nombre'],
                    'apellido_paterno' => $value['apellido_paterno'],
                    'apellido_materno' => $value['apellido_materno'],
                    'direccion' => $value['proveedor_calle'],
                    'numero_int' => $value['proveedor_numero'],
                    'numero_ext' => '',
                    'colonia' => $value['proveedor_colonia'],
                    'municipio' => '',
                    'estado' => $value['proveedor_estado'],
                    'codigo_postal' => $value['codigo_postal'],
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo'],
                    'origen' => 'DMS'
                ];
            }
            $this->cache->file->save('listado_proveedores', json_encode($listado_proveedores), 120);
        }
        return $listado_proveedores;
    }
      
    private function cache_listado_clientes_dms(){

        $listado_clientes = [];
        $this->load->driver('cache');
        $listado = utils::api_get([
            'url' => URL_DMS.'api/clientes'
        ]);

        if(is_array($listado) && count($listado)>0){
            foreach ($listado as $key => $value) {
                $listado_clientes[ $value['rfc'] ] = [
                    'id' => $value['id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido_paterno'],
                    'apellido_materno' => $value['apellido_materno'],
                    'direccion' => $value['direccion'],
                    'numero_int' => $value['numero_int'],
                    'numero_ext' => $value['numero_ext'],
                    'colonia' => $value['colonia'],
                    'municipio' => $value['municipio'],
                    'estado' => $value['estado'],
                    'codigo_postal' => $value['codigo_postal'],
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'DMS'
                ];
            }
            $this->cache->file->save('listado_clientes', json_encode($listado_clientes), 120);
        }
        return $listado_clientes;
    }

    public function listado_clientes_get(){

        $listado_clientes = [];

        $this->load->driver('cache');
    
        $listado = $this->cache->file->get('listado_clientes');
        if($listado == false){
            $listado_clientes = $this->cache_listado_clientes_dms();       
        }else{
            $listado_clientes = @json_decode($listado,true);
        }

        $this->load->model('CaPersonas_model');
        $listado_personas = $this->CaPersonas_model->getAll([
            'tipo_persona_id' => 1
        ]);

        if(is_array($listado_personas) && count($listado_personas)>0){
            foreach ($listado_personas as $key => $value) {
                $listado_clientes[ $value['rfc'] ] = [
                    'id' => $value['persona_id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido1'],
                    'apellido_materno' => $value['apellido2'],
                    'direccion' => $value['domicilio'],
                    'numero_int' => '',
                    'numero_ext' => '',
                    'colonia' => '',
                    'municipio' => '',
                    'estado' => '',
                    'codigo_postal' => '',
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'SQL'
                ];
            }
        }

        $this->data['data'] = $listado_clientes;
        $this->response($this->data);
    }

    public function listado_proveedores_get(){

        $listado_proveedores = [];

        $this->load->driver('cache');
    
        $listado = $this->cache->file->get('listado_proveedores');
        if($listado == false){
            $listado_proveedores = $this->cache_listado_proveedores_dms();       
        }else{
            $listado_proveedores = @json_decode($listado,true);
        }

        $this->load->model('CaPersonas_model');
        $listado_personas = $this->CaPersonas_model->getAll([
            'tipo_persona_id' => 2
        ]);

        if(is_array($listado_personas) && count($listado_personas)>0){
            foreach ($listado_personas as $key => $value) {
                $listado_proveedores[ $value['rfc'] ] = [
                    'id' => $value['persona_id'],
                    'rfc' => $value['rfc'],
                    'nombre' => $value['nombre'],
                    'apellido_paterno' => $value['apellido1'],
                    'apellido_materno' => $value['apellido2'],
                    'direccion' => $value['domicilio'],
                    'numero_int' => $value['numero'],
                    'numero_ext' => '',
                    'colonia' => $value['colonia'],
                    'municipio' => $value['municipio'],
                    'estado' => $value['estado'],
                    'codigo_postal' => $value['codigo_postal'],
                    'telefono' => $value['telefono'],
                    'correo_electronico' => $value['correo_electronico'],
                    'origen' => 'SQL'
                ];
            }
        }

        $this->data['data'] = $listado_proveedores;
        $this->response($this->data);
    }

    function gen_uid($l=10){
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, $l);
    }

    public function agregar_cheque_post(){

        $poliza_id = $this->input->post('poliza');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('cliente_id', 'RFC', 'trim|required');

        $this->form_validation->set_rules('referencia', 'Referencia', 'trim|required');
        $this->form_validation->set_rules('comentarios', 'Comentarios', 'trim');

        $this->form_validation->set_rules('tipo_movimeinto', 'Tipo', 'trim|required');

        $this->form_validation->set_rules('codigo_postal', 'Codigo Postal', 'trim|required');

        if(strlen(trim($poliza_id)) == 0 ){
            $this->form_validation->set_rules('nomenclatura_id', 'Nomenclatura de la poliza', 'trim|required');
            $this->form_validation->set_rules('fecha_poliza', 'Fecha', 'trim|required|valid_date');
        }

        
        
        if ($this->form_validation->run($this) != FALSE) {

            

            $poliza_id = $this->input->post('poliza');
            $cheque_id = null;
            $persona_id = null;
            $transaccion_id = $this->input->post('transaccion');

            $fecha_poliza = $this->input->post('fecha_poliza');
            $tipo_poliza_id = $this->input->post('tipo_persona_id');

            # buscar poliza
            $this->load->model('CaPolizas_model');
            if($poliza_id != false){
                $poliza_datos = $this->CaPolizas_model->get([
                    'ca_polizas.id' => $poliza_id
                ]);
            }else{
                $poliza_datos = $this->CaPolizas_model->get([
                    'ca_polizas.PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                    'ca_polizas.fecha_creacion' => $fecha_poliza
                ]);
            }
            
            if($poliza_datos == false){
                $d = new DateTime($fecha_poliza);
                $poliza_id = $this->CaPolizas_model->insert([
                    'PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                    'ejercicio' => $d->format('Y'),
                    'mes' => $d->format('m'),
                    'dia' => $d->format('d'),
                    'fecha_creacion' => $fecha_poliza,
                ]);
            }else{
                $poliza_id = $poliza_datos['id'];
            }

            #CREACION DEL DE LA PERSONA SI NO EXISTE
            $this->load->model('CaPersonas_model');
            $persona_datos = $this->CaPersonas_model->get([
                'ca_personas.id' => $this->input->post('cliente_id'),
                'ca_personas.tipo_persona_id' => $tipo_poliza_id
            ]);

            $cliente_id = null;
            $proveedor_id = null;

            if($tipo_poliza_id == 1){
                $cliente_id = $this->input->post('cliente_id');
            }else{
                $proveedor_id = $this->input->post('cliente_id');
            }
            
            if($persona_datos == false){
                $persona_datos = $this->CaPersonas_model->get([
                    'ca_personas.cliente_id' => $this->input->post('cliente_id'),
                    'ca_personas.tipo_persona_id' =>$this->input->post('tipo_persona_id')
                ]);
            }
            if($persona_datos == false){
                $persona_id = $this->CaPersonas_model->insert([
                    'cliente_id' => $this->input->post('cliente_id'),
                    'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                    'rfc' => $this->input->post('rfc'),
                    'apellido1' => $this->input->post('apellido1'),
                    'apellido2' => $this->input->post('apellido2'),
                    'nombre' => $this->input->post('nombre'),
                    'telefono' => $this->input->post('telefono'),
                    'domicilio' => $this->input->post('domicilio'),
                    'codigo_postal' => $this->input->post('codigo_postal'),
                    'correo_electronico' => $this->input->post('correo_electronico')  
                ]);
            }else{
                $persona_id = $persona_datos['persona_id'];
            }

            #CREACION DE LA TRANSACCION
            $this->load->model('CaTransacciones_model');
           
           
            if($transaccion_id != false){
                $transaccion_datos = $this->CaTransacciones_model->get([
                    'id' => $transaccion_id
                ]);
            }else{
                $transaccion_datos = false;
            }

            if($transaccion_datos == false){

                // $transaccion_folio = $this->CaTransacciones_model->get_max_folio([
                //     'origen' => 'CHEQUE',
                // ]);
                // utils::pre($transaccion_folio);
                $folio_asignado = $this->gen_uid();

                $transaccion_id = $this->CaTransacciones_model->insert([
                    'cliente_id' => $cliente_id,
                    'proveedor_id' => $proveedor_id,
                    'persona_id' => $persona_id,
                    'folio' => $folio_asignado,
                    'origen' => 'CHEQUE',
                    'fecha' => $fecha_poliza
                ]);

            }else{

                $folio_asignado = $transaccion_datos['folio'];

                $this->CaTransacciones_model->update([
                    'cliente_id' => $cliente_id,
                    'proveedor_id' => $proveedor_id,
                    'persona_id' => $persona_id,
                ],[
                    'id' => $transaccion_id
                ]);

            }
            
            $this->load->model('DeDatosCheque_model');
            $cheque_id = $this->DeDatosCheque_model->insert([
                'transaccion_id' => $transaccion_id,
                'referencia' => $this->input->post('referencia'),
                'comentarios' => $this->input->post('comentarios'),
                'tipo_movimeinto' => $this->input->post('tipo_movimeinto')
            ]);

            $this->load->model('ReTransaccionesPolizas_model');
            $rel_poliza_transaccion_id = $this->ReTransaccionesPolizas_model->insert([
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id
            ]);
            

            // $this->load->model('ReTransaccionesDatosCheque_model');
            // $rel_id_transacion = $this->ReTransaccionesDatosCheque_model->insert([
            //     'transaccion_id' => $transaccion_id,
            //     'datos_cheque_id' => $cheque_id
            // ]);

            // $this->load->model('RePolizasDatosCheque_model');
            // $rel_id_poliza = $this->RePolizasDatosCheque_model->insert([
            //     'poliza_id' => $poliza_id,
            //     'datos_cheque_id' => $cheque_id
            // ]);
            
            $this->data['data'] = array(
                // 'rel_id_transacion' => $rel_id_transacion,
                // 'rel_id_poliza' => $rel_id_poliza,
                'rel_poliza_transaccion_id' => $rel_poliza_transaccion_id,
                'poliza_id' => $poliza_id,
                'cheque_id' => $cheque_id,
                'folio_asignado' => $folio_asignado
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }



    public function crear_poliza_cheque_post(){

        $poliza_id = $this->input->post('poliza');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('cliente_id', 'RFC', 'trim|required');

        $this->form_validation->set_rules('referencia', 'Referencia', 'trim|required');
        $this->form_validation->set_rules('comentarios', 'Comentarios', 'trim');

        $this->form_validation->set_rules('tipo_movimeinto', 'Tipo', 'trim|required');


        if(strlen(trim($poliza_id)) == 0 ){
            $this->form_validation->set_rules('nomenclatura_id', 'Nomenclatura de la poliza', 'trim|required');
            $this->form_validation->set_rules('fecha_poliza', 'Fecha', 'trim|required|valid_date');
        }

        
        
        if ($this->form_validation->run($this) != FALSE) {

            

            $poliza_id = $this->input->post('poliza');
            $cheque_id = null;
            $persona_id = null;
            $transaccion_id = $this->input->post('transaccion');

            $fecha_poliza = $this->input->post('fecha_poliza');
            $tipo_poliza_id = $this->input->post('tipo_persona_id');

          

          

            $cliente_id = null;
            $proveedor_id = null;

            if($tipo_poliza_id == 1){
                $cliente_id = $this->input->post('cliente_id');
            }else{
                $proveedor_id = $this->input->post('cliente_id');
            }
            
          
            

            #CREACION DE LA TRANSACCION
            $this->load->model('CaTransacciones_model');
           
           
            if($transaccion_id != false){
                $transaccion_datos = $this->CaTransacciones_model->get([
                    'id' => $transaccion_id
                ]);
            }else{
                $transaccion_datos = false;
            }

            if($transaccion_datos == false){

                $transaccion_folio = $this->CaTransacciones_model->get_max_folio([
                    'origen' => 'CHEQUE',
                ]);
                $folio_asignado = $transaccion_folio + 1;

                $transaccion_id = $this->CaTransacciones_model->insert([
                    'cliente_id' => $cliente_id,
                    'proveedor_id' => $proveedor_id,
                    'persona_id' => $persona_id,
                    'folio' => $folio_asignado,
                    'origen' => 'CHEQUE',
                    'fecha' => $fecha_poliza
                ]);

            }else{

                $folio_asignado = $transaccion_datos['folio'];

                $this->CaTransacciones_model->update([
                    'cliente_id' => $cliente_id,
                    'proveedor_id' => $proveedor_id,
                    'persona_id' => $persona_id,
                ],[
                    'id' => $transaccion_id
                ]);

            }
            
            $this->load->model('DeDatosCheque_model');
            $cheque_id = $this->DeDatosCheque_model->insert([
                'transaccion_id' => $transaccion_id,
                'referencia' => $this->input->post('referencia'),
                'comentarios' => $this->input->post('comentarios'),
                'tipo_movimeinto' => $this->input->post('tipo_movimeinto')
            ]);

            $this->load->model('ReTransaccionesPolizas_model');
            $rel_poliza_transaccion_id = $this->ReTransaccionesPolizas_model->insert([
                'transaccion_id' => $transaccion_id,
                'poliza_id' => $poliza_id
            ]);
            
            $this->data['data'] = array(
                'rel_poliza_transaccion_id' => $rel_poliza_transaccion_id,
                'poliza_id' => $poliza_id,
                'cheque_id' => $cheque_id,
                'folio_asignado' => $folio_asignado
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function finalizar_cheque_post(){
        
        $id_dato_cheque = $this->input->post('id_datos_cheque');
        $firma_autorizacion = $this->input->post('firma_autorizacion');
        $transaccion_id = $this->input->post('transaccion_id');
        $tipo_movimeinto = $this->input->post('tipo_movimeinto');
        
        $this->load->model('DeDatosCheque_model');
        $this->DeDatosCheque_model->update([
            'tipo_movimeinto' => $tipo_movimeinto,
            'firma_autorizacion' => $firma_autorizacion,
            'id_datos_cheque_estatus' => 2
        ],[
            'id' => $id_dato_cheque
        ]);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/asientos/api/aplicar_transaccion',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'estatus' => 'APLICADO',
                'transaccion_id' => $transaccion_id
            ])
        );
        $this->data['data'] = curl_exec($curl);
        curl_close($curl);


        $this->db->from('vw_poliza_cheque');
        $this->db->where('id_dato_cheque',$id_dato_cheque);
        $query = $this->db->get();
        $result = $query->row_array();
        $query->free_result();

        $resultado = 0;
        if(is_array($result) && count($result)>0){
            
            $this->db->from('vw_polizas_xml_cheques_faltantes');
            $this->db->where('id_poliza_xml',$result['id_poliza_xml']);
            $query = $this->db->get();
            $result2 = $query->num_rows();
            if( $result2 > 0){
                $resultado = $result2;
            }
            $query->free_result();

            if($resultado > 0){
                $this->load->model('CaPolizaXml_model');
                $this->CaPolizaXml_model->update([
                    'id_estatus_poliza_xml' => 5
                ],[
                    'id' => $result['id_poliza_xml']
                ]);
            }else{
                $this->load->model('CaPolizaXml_model');
                $this->CaPolizaXml_model->update([
                    'id_estatus_poliza_xml' => 6
                ],[
                    'id' => $result['id_poliza_xml']
                ]);
            }
        }

        

        $this->data['status'] = 'ok';
        $this->response($this->data);
    }



    public function agregar_devolucion_post(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('cliente_id', 'RFC', 'trim|required');

        $this->form_validation->set_rules('referencia', 'Referencia', 'trim|required');
        $this->form_validation->set_rules('comentarios', 'Comentarios', 'trim');

        $this->form_validation->set_rules('nomenclatura_id', 'Nomenclatura de la poliza', 'trim|required');
        $this->form_validation->set_rules('fecha_poliza', 'Fecha', 'trim|required|valid_date');
        
        if ($this->form_validation->run($this) != FALSE) {

            $poliza_id = null;
            $cheque_id = null;
            $persona_id = null;

            $fecha_poliza = $this->input->post('fecha_poliza');
            $tipo_poliza_id = $this->input->post('tipo_persona_id');

            # buscar poliza
            $this->load->model('CaPolizas_model');
            $poliza_datos = $this->CaPolizas_model->get([
                'ca_polizas.PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                'ca_polizas.fecha_creacion' => $fecha_poliza
            ]);

            if($poliza_datos == false){
                $d = new DateTime($fecha_poliza);
                $poliza_id = $this->CaPolizas_model->insert([
                    'PolizaNomenclatura_id' => $this->input->post('nomenclatura_id'),
                    'ejercicio' => $d->format('Y'),
                    'mes' => $d->format('m'),
                    'dia' => $d->format('d'),
                    'fecha_creacion' => $fecha_poliza,
                ]);
            }else{
                $poliza_id = $poliza_datos['id'];
            }
            

            #CREACION DEL DE LA PERSONA SI NO EXISTE
            $this->load->model('CaPersonas_model');
            $persona_datos = $this->CaPersonas_model->get([
                'ca_personas.id' => $this->input->post('cliente_id'),
                'ca_personas.tipo_persona_id' => $tipo_poliza_id
            ]);
            if($persona_datos == false){

                $domicilio = $this->input->post('direccion').' '.$this->input->post('numero_ext').', '.$this->input->post('colonia').', '.$this->input->post('municipio').' '.$this->input->post('estado').', '.$this->input->post('codigo_postal');

                $persona_id = $this->CaPersonas_model->insert([
                    'cliente_id' => $this->input->post('cliente_id'),
                    'tipo_persona_id' => 1,//$tipo_poliza_id,
                    'rfc' => $this->input->post('rfc'),
                    'apellido1' => $this->input->post('apellido_paterno'),
                    'apellido2' => $this->input->post('apellido_materno'),
                    'nombre' => $this->input->post('nombre'),
                    'telefono' => $this->input->post('telefono'),
                    'domicilio' => $domicilio,
                    'correo_electronico' => $this->input->post('correo_electronico')  
                ]);
            }else{
                $persona_id = $persona_datos['persona_id'];
            }
            
            // $cliente_id = null;
            // $proveedor_id = null;

            // if($tipo_poliza_id == 1){
            //     $cliente_id = $this->input->post('cliente_id');
            // }else{
            //     $proveedor_id = $this->input->post('cliente_id');
            // }
            
            #CREACION DE LA TRANSACCION
            $this->load->model('CaTransacciones_model');
            $transaccion_datos = $this->CaTransacciones_model->get([
                'cliente_id' => $this->input->post('cliente_id'),
                'origen' => 'DEVOLUCION_ANTICIPO',
                'anticipo_id' => $this->input->post('anticipo_id')
            ]);

            if($transaccion_datos == false){

                $this->db->where_in('origen',['CHEQUE','DEVOLUCION_ANTICIPO']);
                $transaccion_folio = $this->CaTransacciones_model->get_max_folio();
                $folio_asignado = $transaccion_folio + 1;

                $transaccion_id = $this->CaTransacciones_model->insert([
                    'cliente_id' => $this->input->post('cliente_id'),
                    'persona_id' => $persona_id,
                    'folio' => $folio_asignado,
                    'origen' => 'DEVOLUCION_ANTICIPO',
                    'fecha' => $fecha_poliza,
                    'anticipo_id' => $this->input->post('anticipo_id')
                ]);
            }else{
                $transaccion_id = $transaccion_datos['id'];
                $folio_asignado = $transaccion_datos['folio'];
            }

            $this->load->model('DeDatosCheque_model');
            $cheque_datos = $this->DeDatosCheque_model->get([
                'transaccion_id' => $transaccion_id
            ]);

            if($cheque_datos == false){
                $cheque_id = $this->DeDatosCheque_model->insert([
                    'transaccion_id' => $transaccion_id,
                    'referencia' => $this->input->post('referencia'),
                    'comentarios' => $this->input->post('comentarios')
                ]);
            }else{
                $cheque_id = $cheque_datos['id'];
            }

                
            $this->load->model('ReTransaccionesPolizas_model');
            $rel_datos = $this->ReTransaccionesPolizas_model->get([
                'transaccion_id' => $transaccion_id
            ]);

            if($rel_datos == false){
                $rel_poliza_transaccion_id = $this->ReTransaccionesPolizas_model->insert([
                    'transaccion_id' => $transaccion_id,
                    'poliza_id' => $poliza_id
                ]);
            }else{
                $rel_poliza_transaccion_id = $rel_datos['id'];
            }

            $this->load->model('CaCuentas_model');
            $cuenta_devolucion_anticipo = CaCuentas_model::CTA_DEVOLUCION_ANTICIPO;

            $this->load->model('Asientos_model');
            $asientos_datos = $this->Asientos_model->get([
                'transaccion_id' => $transaccion_id,
                'polizas_id' => $poliza_id,
                'cuenta' => $cuenta_devolucion_anticipo
            ]);

            if($asientos_datos == false){
                $rfc = $this->input->post('rfc');

                $nombre = $this->input->post('nombre');
                $apellido_paterno = $this->input->post('apellido_paterno');
                $apellido_materno = $this->input->post('apellido_materno');
                $telefono = $this->input->post('telefono');
                $folio_anticipo = $this->input->post('folio_anticipo');
                $cfdi_clave = $this->input->post('cfdi_clave');
                $cfdi_descripcion = $this->input->post('cfdi_descripcion');

                $concepto = <<<EOD
                    Devolución de anticipo.
                    Folio $folio_anticipo.
                    Cliente $nombre $apellido_paterno $apellido_materno, RFC $rfc, telefono $telefono.
                    CFDI: $cfdi_clave $cfdi_descripcion.
                EOD;

                $asiento_id = $this->Asientos_model->insert([
                    'polizas_id' => $poliza_id,
                    'estatus_id' => 'APLICADO',
                    'transaccion_id' => $transaccion_id,
                    'tipo_pago_id' => 'CHEQUE',
                    'cuenta' => $cuenta_devolucion_anticipo,
                    'cargo' => $this->input->post('total'),
                    'abono' => 0,
                    'concepto' => $concepto,
                    'fecha_creacion' => utils::get_date(),
                    'precio_unitario' => $this->input->post('total'),
                    'cantidad' => 1,
                    'departamento_id' => 9
                ]);

                //revisar la la aplicacion de polizas
            }else{
                $asiento_id = $asientos_datos['id'];
            }
            

           
            $this->data['data'] = array(
                'rel_poliza_transaccion_id' => $rel_poliza_transaccion_id,
                'poliza_id' => $poliza_id,
                'cheque_id' => $cheque_id,
                'asiento_id' => $asiento_id,
                'folio_asignado' => (int)$folio_asignado
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function regex_rfc($rfc) {
        if ($rfc == FALSE) {
            if( strlen(trim($rfc)) == 12 ){
                $patrón = '/^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$/';
            }else{
                $patrón = '/^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$/';
            }
            if(preg_match($patrón, $rfc, $coincidencias, PREG_OFFSET_CAPTURE, 3)){
                $this->form_validation->set_message('regex_rfc', 'El RFC no es valido');
                return false;
            }else{
                return true;
            }
            


        } else {
            return true;
        }
    }

    public function agregar_persona_post(){
        
        $this->load->library('form_validation');
        // $this->form_validation->set_rules('cliente_id', 'Cliente', 'trim');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('rfc', 'RFC', 'trim|required|is_unique[ca_personas.rfc]|min_length[12]|callback_regex_rfc');
        $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|valid_email|required');

        $this->form_validation->set_rules('razon_social', 'Razón social', 'trim');
    
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('apellido1', 'Primer apellido', 'trim|required');
        $this->form_validation->set_rules('apellido2', 'Segundo apellido', 'trim');
        
        
        $this->form_validation->set_rules('id_estado', 'Estado', 'trim|required');
        $this->form_validation->set_rules('id_municipio', 'Municipio', 'trim|required');
        $this->form_validation->set_rules('id_colonia', 'Colonia', 'trim|required');

        $this->form_validation->set_rules('numero', 'Numero', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');
        $this->form_validation->set_rules('codigo_postal', 'Codigo Postal', 'trim|required|exact_length[5]');
        
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required|exists[ca_tipo_persona.id]');
        
        if($this->input->post('tipo_persona_id') == 2){
            $this->form_validation->set_rules('id_poliza_nomenclatura', 'Poliza Postal', 'trim|required');
            $id_poliza_nomenclatura = $this->input->post('id_poliza_nomenclatura');
            if($id_poliza_nomenclatura == 'DO'){
                $this->form_validation->set_rules('id_poliza_fija',  'Poliza Fija', 'trim|required');
            }
            $this->form_validation->set_rules('id_cuenta_subtotal', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('subtotal_tipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('id_cuenta_iva', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('iva_tipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('id_cuenta_total', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('total_tipo', 'Tipo', 'trim|required');
        }
        
        if ($this->form_validation->run($this) != FALSE) {
   
            $this->load->model('CaPersonas_model');
            $cheque_id = $this->CaPersonas_model->insert([
                // 'cliente_id' => $this->input->post('cliente_id'),
                'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                'rfc' => $this->input->post('rfc'),
                'razon_social' => $this->input->post('razon_social'),
                'apellido1' => $this->input->post('apellido1'),
                'apellido2' => $this->input->post('apellido2'),
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                
                
                'domicilio' => $this->input->post('calle').' #'.$this->input->post('numero').', '.$this->input->post('colonia').', '.$this->input->post('municipio').' '.$this->input->post('estado'),

                'id_estado' => $this->input->post('id_estado'),
                'id_municipio' => $this->input->post('id_municipio'),
                'id_colonia' => $this->input->post('id_colonia'),
                'estado' => $this->input->post('estado'),
                'municipio' => $this->input->post('municipio'),
                'colonia' => $this->input->post('colonia'),

                'numero' => $this->input->post('numero'),
                'calle' => $this->input->post('calle'),


                'correo_electronico' => $this->input->post('correo_electronico') ,
                'codigo_postal' => $this->input->post('codigo_postal')
            ]);

            if($this->input->post('tipo_persona_id') == 2){
                $this->load->model('DePersonasXmlConfig_model');
                $this->DePersonasXmlConfig_model->insert([
                    'id_persona' => $cheque_id,
                    'id_poliza_nomenclatura' => $this->input->post('id_poliza_nomenclatura'),
                    'id_poliza_fija' => (($id_poliza_nomenclatura == 'DO')? $this->input->post('id_poliza_fija') : null),
                    'id_cuenta_subtotal' => $this->input->post('id_cuenta_subtotal'),
                    'subtotal_tipo' => $this->input->post('subtotal_tipo'),
                    'id_cuenta_iva' => $this->input->post('id_cuenta_iva'),
                    'iva_tipo' => $this->input->post('iva_tipo'),
                    'id_cuenta_total' => $this->input->post('id_cuenta_total'),
                    'total_tipo' => $this->input->post('total_tipo')
                ]);
                
                $correo_electronico = $this->input->post('correo_electronico');
                $this->enviar_correo_proveedors($cheque_id);
                
                
            }
            
            $this->data['data'] = array(
                'cheque_id' => $cheque_id
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function enviar_correo_proveedor_get($id_persona) {
        $this->enviar_correo_proveedors($id_persona);
    }

public function enviar_correo_proveedors($id_persona) {


        $this->load->model('CaPersonas_model');

        $this->load->library('CaPersonas_model');
        $datos_persona = $this->CaPersonas_model->get([
            'id' => $id_persona
        ]);
        

        if(is_array($datos_persona) && array_key_exists('envio_correo',$datos_persona) && $datos_persona['envio_correo'] == 0){

            $comb = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $pass = array(); 
            $combLen = strlen($comb) - 1; 
            for ($i = 0; $i < 8; $i++) {
                $n = rand(0, $combLen);
                $pass[] = $comb[$n];
            }
            
            
            $contenido = "
            <h3>Acceso al sitio de proveedores mylsa</h3>
            <p>
                    Estimado proveedor: ".$datos_persona['nombre']." ".$datos_persona['apellido1']." ".$datos_persona['apellido2']." <br/>
                    Te enviamos la información de acceso al sistema de provedores mylsa<br/><br/>

                    usuario: ".$datos_persona['rfc']."<br/>
                    contraseña: ".implode($pass)."<br/>

                    Clic en el enlace para ir al sitio <a href='https://www.providermylsa.com/proveedores'>https://www.providermylsa.com/proveedores</a>
            </p>";

            $this->load->library('parser');
            $data = array(
                'contenido' => $contenido
            );
        
            $contenido = $this->parser->parse('/correos/tpl', $data,true);

            $config = Array(
                'protocol'        => "smtp",
                'smtp_host'       => "smtp.mail.yahoo.com",
                'smtp_user'       => "desarrollo_aplicaciones@yahoo.com",
                'smtp_pass'       => "wvgltpjhcpbkzubj", //"rz418lwy0";
                'smtp_port'       => "587",
                'smtp_crypto'     => "tls",
                'mailtype'        => "html",
                'smtp_timeout'    => "30",
                'charset'         => "utf-8",
                'crlf'            => "\r\n",
                'newline'         => "\r\n",
                'email'           => "desarrollo_aplicaciones@yahoo.com",
                // 'smtp_debug'		=> 1
            );
            $this->load->library('email');
            $this->email->initialize($config);

            $this->email->from('desarrollo_aplicaciones@yahoo.com', 'Proveedores MYLSA');
            $this->email->to($datos_persona['correo_electronico']);
            // $this->email->to('marcos-alcantara@hotmail.com');
            $this->email->subject('Acceso al sitio de proveedores mylsa');
            $this->email->message($contenido);

            if($this->email->send() == true){
                $this->CaPersonas_model->update([
                    'contrasena' => implode($pass),
                    'envio_correo' => 1
                ],[
                    'id' => $datos_persona['persona_id']
                ]);
            }
            

        }

    }

    public function editar_persona_post(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('identificador', 'identificador', 'trim|required|exists[ca_personas.id]');
        $this->form_validation->set_rules('cliente_id', 'Cliente', 'trim');
        $this->form_validation->set_rules('rfc', 'RFC', 'trim|required');
        $persona_rfc = $this->input->post('rfc');
        
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        
        $this->form_validation->set_rules('apellido1', 'Primer apellido', 'trim|required');
        $this->form_validation->set_rules('apellido2', 'Segundo apellido', 'trim');
        
        


        $this->form_validation->set_rules('id_estado', 'Estado', 'trim|required');
        $this->form_validation->set_rules('id_municipio', 'Municipio', 'trim|required');
        $this->form_validation->set_rules('id_colonia', 'Colonia', 'trim|required');

        $this->form_validation->set_rules('numero', 'Numero', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');



        
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        // $this->form_validation->set_rules('domicilio', 'Domicilio', 'trim|required');
        $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|valid_email');
        $this->form_validation->set_rules('tipo_persona_id', 'Tipo', 'trim|required|exists[ca_tipo_persona.id]');
        $this->form_validation->set_rules('codigo_postal', 'Codigo Postal', 'trim|required|exact_length[5]');

        if($this->input->post('tipo_persona_id') == 2){
            $this->form_validation->set_rules('id_poliza_nomenclatura', 'Poliza Postal', 'trim|required');
            $id_poliza_nomenclatura = $this->input->post('id_poliza_nomenclatura');
            if($id_poliza_nomenclatura == 'DO'){
                $this->form_validation->set_rules('id_poliza_fija',  'Poliza Fija', 'trim|required');
            }

            $this->form_validation->set_rules('id_cuenta_subtotal', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('subtotal_tipo', 'Tipo', 'trim|required');

            $this->form_validation->set_rules('id_cuenta_iva', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('iva_tipo', 'Tipo', 'trim|required');

            $this->form_validation->set_rules('id_cuenta_total', 'Cuenta', 'trim|required');
            $this->form_validation->set_rules('total_tipo', 'Tipo', 'trim|required');
        }
      
        
        if ($this->form_validation->run($this) != FALSE) {

            $identificador = $this->input->post('identificador');

            $this->load->model('CaPersonas_model');
            $id = $this->CaPersonas_model->update([
                'tipo_persona_id' => $this->input->post('tipo_persona_id'),
                'rfc' => $this->input->post('rfc'),
                'apellido1' => $this->input->post('apellido1'),
                'apellido2' => $this->input->post('apellido2'),
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                
                'correo_electronico' => $this->input->post('correo_electronico'),
                'codigo_postal' => $this->input->post('codigo_postal') ,

                

                'domicilio' => $this->input->post('calle').' #'.$this->input->post('numero').', '.$this->input->post('colonia').', '.$this->input->post('municipio').' '.$this->input->post('estado'),
                'id_estado' => $this->input->post('id_estado'),
                'id_municipio' => $this->input->post('id_municipio'),
                'id_colonia' => $this->input->post('id_colonia'),
                'estado' => $this->input->post('estado'),
                'municipio' => $this->input->post('municipio'),
                'colonia' => $this->input->post('colonia'),
                'numero' => $this->input->post('numero'),
                'calle' => $this->input->post('calle'),
            ],[
                'id' => $identificador
            ]);


            if($this->input->post('tipo_persona_id') == 2){
                $this->load->model('DePersonasXmlConfig_model');
                $this->DePersonasXmlConfig_model->update([
                    'id_poliza_nomenclatura' => $this->input->post('id_poliza_nomenclatura'),
                    'id_poliza_fija' => (($id_poliza_nomenclatura == 'DO')? $this->input->post('id_poliza_fija') : null),
                    'id_cuenta_subtotal' => $this->input->post('id_cuenta_subtotal'),
                    'subtotal_tipo' => $this->input->post('subtotal_tipo'),
                    'id_cuenta_iva' => $this->input->post('id_cuenta_iva'),
                    'iva_tipo' => $this->input->post('iva_tipo'),
                    'id_cuenta_total' => $this->input->post('id_cuenta_total'),
                    'total_tipo' => $this->input->post('total_tipo')  
                ],[
                    'id_persona' => $identificador
                ]);
            }
            
            $this->data['data'] = array(
                'persona_id' => $identificador
            );

        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);

    }

    public function modal_agregar_asiento_get(){

        $this->load->model('CaCuentas_model');
        $cuentas_datos = $this->CaCuentas_model->getAll();

        $this->load->model('CaUnidades_model');
        $unidades_datos = $this->CaUnidades_model->getAll();

        $this->load->model('Asientos_model');
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $asientos_datos = $this->Asientos_model->get([
            'transaccion_id' => $this->input->get('transaccion_id'),
            'estatus_id' => 'POR_APLICAR'
        ]);

        $data_content = [
            'cuentas' => $cuentas_datos,
            'unidades' => $unidades_datos,
        ];

        $html = $this->load->view('/api/modal_agregar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
            'asiento' => $asientos_datos
        );
        $this->response($this->data);
    }

    public function modal_editar_asiento_get(){

        $renglon_id = $this->input->get('renglon_id');

        $this->load->model('CaCuentas_model');
        $cuentas_datos = $this->CaCuentas_model->getAll();

        $this->load->model('CaUnidades_model');
        $unidades_datos = $this->CaUnidades_model->getAll();

        $this->load->model('Asientos_model');
        $asiento = $this->Asientos_model->get([
            'id' => $renglon_id
        ]);

        $data_content = [
            'cuentas' => $cuentas_datos,
            'unidades' => $unidades_datos,
        ];

        $html = $this->load->view('/api/modal_editar_asiento',$data_content,true);

        $this->data['data'] = array(
            'html' => base64_encode(utf8_decode($html)),
            'asiento' => $asiento
        );
        $this->response($this->data);
    }

    public function detalleAll_get(){

        $nomenclatura = $this->input->get('nomenclatura');
        $folio = $this->input->get('folio');
        $fecha = $this->input->get('fecha');

        $fecha_ini = $this->input->get('fecha_ini');
        $fecha_fin = $this->input->get('fecha_fin');

        $contenido_busqueda = [];
        // $this->db->where_in('ca_transacciones.origen',['CHEQUE','DEVOLUCION_ANTICIPO']);

        if (strlen($fecha_ini) > 0 && strlen($fecha_fin) > 0) {
            $this->db->where( "ca_transacciones.fecha BETWEEN ".$this->db->escape($fecha_ini)." AND ".$this->db->escape($fecha_fin)." ", NULL, FALSE );
        }
        if (strlen($nomenclatura) > 0) {
            $contenido_busqueda['ca_polizas.PolizaNomenclatura_id'] = $nomenclatura;
        }
        if (strlen($folio) > 0) {
            $contenido_busqueda['ca_transacciones.folio'] = $folio;
        }
        if (strlen($fecha) > 0) {
            $contenido_busqueda['ca_transacciones.fecha'] = $fecha;
        }

        // $this->db->order_by('ca_transacciones.folio','desc');
        $this->db->limit(100);

        $this->load->model('DeDatosCheque_model');
        $this->db->order_by('de_datos_cheque.created_at','desc');
        $this->data['data'] = $this->DeDatosCheque_model->get_detalleAll($contenido_busqueda);
        // $this->data['sql'] = $this->db->last_query();
        $this->response($this->data);
    }

    public function modal_agregar_asiento_post(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        
        // $this->form_validation->set_rules('precio_unitario', 'Precio Unitario', 'trim|required');
        // $this->form_validation->set_rules('cantidad', 'Cantidad', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');

        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');
        // $this->form_validation->set_rules('tipo_unidad', 'Tipo de unidad', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');

            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->insert([
                'polizas_id' => $this->input->post('polizas_id'),
                'estatus_id' => 'POR_APLICAR',
                'transaccion_id' => $this->input->post('transaccion_id'),
                'tipo_pago_id' => 'CHEQUE',
                'cuenta' => $cuenta_id,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                // 'unidad_id' => $this->input->post('tipo_unidad'),
                'departamento_id' => 9
            ]);

            // if($asiento_id != false){
            //     $this->load->model('MaBalanza_model');
            //     $balaza = $this->MaBalanza_model->aplicar_asiento($cuenta_id, $cargo, $abono, $asiento_id);
            //     if (is_array($balaza) && array_key_exists('balanza_id', $balaza)) {
            //         $this->Asientos_model->update(array(
            //             'estatus_id' => 'APLICADO',
            //             'fecha_asiento_aplicado' => utils::get_datetime()
            //         ), array(
            //             'id' => $asiento_id
            //         ));
            //         $balanza_id = $balaza['balanza_id'];
            //     }
            // }

            $this->data['data'] = [
                'asiento_id' => $asiento_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


    public function modal_editar_asiento_post(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('modal_cuentas', 'Cuenta', 'trim|required');
        $this->form_validation->set_rules('precio', 'Monto', 'trim|required');
        $this->form_validation->set_rules('modal_tipo', 'Tipo', 'trim|required');

        if ($this->form_validation->run($this) != FALSE) {

            $renglon_id = $this->input->post('renglon_id');
            $cargo = 0;
            $abono = 0;
            if($this->input->post('modal_tipo') == 1){
                $cargo = $this->input->post('precio');
            }else{
                $abono = $this->input->post('precio');
            }

            $cuenta_id = $this->input->post('modal_cuentas');
            
            $this->load->model('Asientos_model');
            $asiento_id = $this->Asientos_model->update([
                // 'polizas_id' => $this->input->post('polizas_id'),
                // 'estatus_id' => 'POR_APLICAR',
                // 'transaccion_id' => $this->input->post('transaccion_id'),
                // 'tipo_pago_id' => 'CHEQUE',
                'cuenta' => $cuenta_id,
                'cargo' => $cargo,
                'abono' => $abono,
                'concepto' => $this->input->post('concepto'),
                // 'fecha_creacion' => utils::get_date(),
                'precio_unitario' => $this->input->post('precio'),
                'cantidad' => 1,
                // 'unidad_id' => $this->input->post('tipo_unidad'),
                // 'departamento_id' => 9
            ],[
                'id' => $renglon_id
            ]);

            $this->data['data'] = [
                'asiento_id' => $renglon_id
            ];
            
        } else {
            $this->data['status'] = 'error';
            $this->data['message'] = $this->form_validation->error_array();
        }

        $this->response($this->data);
    }


}
