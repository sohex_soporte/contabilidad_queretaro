@section('contenido')
<script>
    const datos_cheque_id = "<?php echo $datos_cheque_id; ?>";
    const polizas_id = "<?php echo $polizas_id; ?>";
    const transaccion_id = "<?php echo $transaccion_id; ?>";
    const archivo_comprobante = "<?php echo $cheque['archivo_comprobante']; ?>";
    const id_datos_cheque_estatus = "<?php echo $cheque['id_datos_cheque_estatus']; ?>";
</script>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    CHEQUE <br />
    <small>FOLIO: <?php echo $cheque['folio']; ?></small>
</h4>

<div class="row" id="contenedor_cheque">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title mb-4">DATOS DEL CHEQUE</h5>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="rfc" class="col-sm-4 col-form-label">RFC:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="rfc" name="rfc" disabled
                                            value="<?php echo $cheque['rfc']; ?>" />
                                        <small id="msg_rfc" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nombre" class="col-sm-4 col-form-label">Nombre:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nombre" name="nombre" disabled
                                            value="<?php echo $cheque['nombre']; ?>" />
                                        <small id="msg_nombre" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="apellido1" class="col-sm-4 col-form-label">Primer apellido:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="apellido1" name="apellido1" disabled
                                            value="<?php echo $cheque['apellido1']; ?>" />
                                        <small id="msg_apellido1" class="form-text text-danger"></small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="apellido2" class="col-sm-4 col-form-label">Segundo apellido:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="apellido2" name="apellido2" disabled
                                            value="<?php echo $cheque['apellido2']; ?>" />
                                        <small id="msg_apellido2" class="form-text text-danger"></small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="telefono" class="col-sm-4 col-form-label">Telefono:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="telefono" name="telefono" disabled
                                            value="<?php echo $cheque['telefono']; ?>">
                                        <small id="msg_telefono" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="domicilio" class="col-sm-4 col-form-label">Domicilio:</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" disabled id="domicilio"
                                            name="domicilio"><?php echo $cheque['domicilio']; ?></textarea>
                                        <small id="msg_domicilio" class="form-text text-danger"></small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="correo_electronico" class="col-sm-4 col-form-label">Correo
                                        electronico:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="correo_electronico"
                                            name="correo_electronico" disabled
                                            value="<?php echo $cheque['correo_electronico']; ?>">
                                        <small id="msg_correo_electronico" class="form-text text-danger"></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <hr />
                    </div>
                    <?php if($cheque['id_datos_cheque_estatus'] == 2){ ?>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                            <div class="form-group row">
                                    <label for="rfc" class="col-sm-4 col-form-label">Tipo de pago:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="correo_electronico"
                                            name="correo_electronico" disabled
                                            value="<?php echo ($cheque['tipo_movimeinto'] == 2)? 'Transferencia' : 'Cheque' ; ?>">
                                    </div>
                                    </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="rfc" class="col-sm-4 col-form-label">Comprobante:</label>
                                    <div class="col-sm-8">

                                        <label for="file" class="input input-file" style="width: 100%;">
                                            <input id="id_archivo_modal" name="id_archivo_modal" type="hidden" />
                                            <input id="id_original_modal" name="id_original_modal" type="hidden" />
                                            <div id="seleccionar_archivo" style="width: 100%;"></div>
                                        </label>

                                        <div id='singleupload1_progress' style="display:none;width: 100%;">
                                            <div class="">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                        aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display:none;margin-top:-7px;" class="input-group"
                                            id="singleupload1_finaly">
                                            <input type="text" id="nombre_archivo" name="nombre_archivo"
                                                class="form-control" placeholder="Nombre archivo" disabled="disabled"
                                                readonly="readonly" style="padding: 0px 10px 0px 10px" />
                                            <div class="input-group-btn">
                                               
                                                <a target="_blank" class="btn btn-info" href="<?php echo BASE_CHEQUE_COMPROBANTEL.$cheque['archivo_comprobante']; ?>">Ver comprobante</a>
                                                <button class="btn btn-danger"
                                                    onclick="$('#id_archivo_modal').val('');$('#nombre_archivo').val('');$('#singleupload1_finaly').hide();$('#singleupload1_content').show();eliminar_archivo()"
                                                    type="button" style="padding: 6px 12px;">
                                                    Eliminar
                                                </button>
                                            </div>
                                        </div>
                                        <small id="msg_id_archivo_modal" class="form-text text-danger"></small>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <hr />
                    </div>
                    <?php } ?>

                    <div class="col-sm-12">
                        <h5 class="card-title mb-4">POLIZA</h5>
                    </div>
                    <div class="col-sm-12">

                        <div class="form-group row">
                            <label for="nomenclatura_id" class="col-sm-2 col-form-label">Poliza:</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="nomenclatura_id" name="nomenclatura_id" disabled
                                    value="<?php echo $cheque['PolizaNomenclatura_id'].$cheque['dia']; ?>" />
                                <small id="msg_nomenclatura_id" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_poliza" class="col-sm-2 col-form-label">Fecha:</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="fecha_poliza" name="fecha_poliza" disabled
                                    value="<?php echo $cheque['poliza_fecha_creacion']; ?>">
                                <small id="msg_fecha_poliza" class="form-text text-muted"></small>
                            </div>
                        </div>

                        </form>
                    </div>

                    <div class="col-sm-12">
                        <hr />
                    </div>


                    <div class="col-sm-12">

                        <div class="form-group row">
                            <label for="referencia" class="col-sm-2 col-form-label">Referencia:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" disabled id="true"
                                    name="referencia"><?php echo $cheque['referencia']; ?></textarea>
                                <small id="msg_referencia" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="comentarios" class="col-sm-2 col-form-label">Comentarios:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" disabled id="true"
                                    name="comentarios"><?php echo $cheque['comentarios']; ?></textarea>
                                <small id="msg_comentarios" class="form-text text-muted"></small>
                            </div>
                        </div>

                        </form>
                    </div>


                </div>

                <hr class="mt-3" />

                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title ">LISTADO DE ASIENTOS</h5>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class=" col-sm-12">
                        <?php if($cheque['id_datos_cheque_estatus'] == 2){ ?>
                            <?php if($cheque['tipo_movimeinto'] == 1){ ?>
                            <a target="_blank" class="btn btn-warning m-1  my-sm-0 float-right"
                            href="<?php echo site_url('cheques/imprimir/index?id='.$datos_cheque_id); ?>"><i
                                class="fas fa-money-bill"></i> Ver Cheque</a>
                                <?php } ?>
                        <?php }else{ ?>
                            <button onclick="APP.finalizar_cheque()" class="btn btn-secondary m-1  my-sm-0 float-right"><i
                            class="fas fa-money-bill"></i> Finalizar</button>

                        <button title="Agregar" class="btn btn-success m-1  my-sm-0 float-right"
                            onclick="APP.modal_agregar_asiento();" type="button"><i class="fas fa-plus"></i> Agregar
                            asiento</button>

                        
                        <?php } ?>
                        
                    </div>
                </div>

                <div class="row">
                    <div class=" col-sm-12">
                        <?php if($acumulado_abono <> $acumulado_cargo){ ?>
                        <div class="alert alert-danger" role="alert">
                            Los montos de cargos y abonos no coinciden
                        </div>
                        <?php } ?>

                        <div class="table-responsive">
                            <?php echo $tabla; ?>
                        </div>
                        <!-- <table class="table table-striped table-hover table-bordered" id="tabla_listado">
                        </table> -->
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-sm-10 mx-auto">
                        <hr/>
                        <fieldset class="border p-2">
                        <legend class="float-none w-auto p-2">Ayuda</legend>
                        <p class="text-justify small">
                            <ul>
                                <li>Para concluir el cheque o transferencia presionar el boton de Finalizar</li>
                            </ul>
                        </p>
                        </fieldset>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php if($this->input->get('devolucion') == 1){ ?>
        <a type="button" href="<?php echo site_url('cheques/anticipos'); ?>" class="btn btn-secondary mt-4 mb-3"><i
                class="fa fa-arrow-left"></i> Regresar</a>
        <?php } else { ?>
        <?php if($this->session->userdata('poliza') == 'xml'){ ?>
        <a type="button" href="<?php echo site_url('polizas/xml'); ?>" class="btn btn-secondary mt-4 mb-3"><i
                class="fa fa-arrow-left"></i> Regresar</a>
        <?php } else { ?>
        <a type="button" href="<?php echo site_url('cheques'); ?>" class="btn btn-secondary mt-4 mb-3"><i
                class="fa fa-arrow-left"></i> Regresar</a>
        <?php } ?>
        <?php } ?>


    </div>
</div>


<center style="display:none;">
    <input type="hidden" id="firma_tecnico_equipo" name="firma_tecnico_equipo" value="">
    <img src="" style="height:80px;" onclick="app.firmar('firma_tecnico_equipo','modal_equipo_diagnostico')" class="img-fluid img-firma-big" id="firma_tecnico_equipo" onerror="this.onerror=null;this.src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';">
    <div class="sep10"></div>
    <b>Firma <button type="button" onclick="app.firmar('firma_tecnico_equipo','modal_equipo_diagnostico')" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button></b>
</center>

<div class="modal fade" id="modal_agregar_asiento" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="modal_agregar_asientoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_agregar_asientoLabel">Agregar asiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="modal_agregar_asiento_button" class="btn btn-primary"
                    onclick="APP.modal_agregar_asiento_guardar();"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal para la firma eléctronica-->
<div class="modal" id="firmaDigital" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">Autorización</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="tipo_movimeinto" class="col-sm-12 col-form-label"><span class="text-danger">*</span> Tipo de pago:</label>
                            <div class="col-sm-12">
                                <select class="form-control" id="tipo_movimeinto" name="tipo_movimeinto" onchange="changetipo_movimeinto(this.value)">
                                    <option value="1">Cheque</option>
                                    <option value="2">Transferencia</option>
                                </select>
                                <small id="msg_tipo_movimeinto" class="form-text text-danger"></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-12" id="contenedor_div">

                        <div class="form-group row">
                            <label for="tipo_movimeinto" class="col-sm-12 col-form-label"><span class="text-danger">*</span> Firma de autorización:</label>
                            <small id="msg_firma" class="form-text text-danger"></small>
                            <div class="col-sm-12">
                                <div class="signatureparent_cont_1" align="center">
                                    <canvas id="canvas" width="500" height="200" style='border: 1px solid #CCC;width: 100%;'>
                                        Su navegador no soporta el firmado
                                    </canvas>
                                </div>
                                <button type="button" class="btn btn-info" onclick="APP.limpiar();" id="limpiar">Limpiar firma</button>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                
                <button type="button" class="btn btn-primary" onclick="APP.convertCanvasToImage();" name="btnSign" id="btnSign">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    function changetipo_movimeinto($valor){
        if($valor == 1){
            $('div#contenedor_div').show();
        }else{
            $('div#contenedor_div').hide();
        }
    }
</script>

@endsection

@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-file-upload/4.0.11/uploadfile.min.css" integrity="sha512-MudWpfaBG6v3qaF+T8kMjKJ1Qg8ZMzoPsT5yWujVfvIgYo2xgT1CvZq+r3Ks2kiUKcpo6/EUMyIUhb3ay9lG7A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }

    

    #customFile .custom-file-input:lang(en)::after {
  content: "Seleccionar un archivo...";
}

#customFile .custom-file-input:lang(en)::before {
  content: "Click me";
}

/*when a value is selected, this class removes the content */
.custom-file-input.selected:lang(en)::after {
  content: "" !important;
}

.custom-file {
  overflow: hidden;
}
.custom-file-input {
  white-space: nowrap;
}

.ajax-file-upload-container{
    display: none;
}

</style>

@endsection

@section('script')

<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/cheques/asientos.js'); ?>"></script>






<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-file-upload/4.0.11/jquery.uploadfile.min.js" integrity="sha512-uwNlWrX8+f31dKuSezJIHdwlROJWNkP6URRf+FSWkxSgrGRuiAreWzJLA2IpyRH9lN2H67IP5H4CxBcAshYGNw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    

    //ProcesarCheque_post

    function eliminar_archivo(){
        	
        $.get( PATH+'/cheques/api/comprobande_delete', { datos_cheque_id: datos_cheque_id } );
    }

    $(function(){
    $("div#seleccionar_archivo").uploadFile({
                    url: PATH+'/cheques/api/comprobande_upload?datos_cheque_id='+datos_cheque_id,
                    uploadStr: "<i class='fa fa-paperclip' aria-hidden='true'></i> Seleccionar Archivo",
                    allowedTypes:"*",
                    dragDrop: false,
                    fileName: "myfile",
                    returnType: "json",
                    showDelete: true,
                    showDone: false,
                    maxFileSize: 10485760,
                    // acceptFiles: "application/pdf",
                    onLoad:function(obj){
                        var button_file = $('.ajax-file-upload');
                        button_file.addClass('btn btn-primary');
                        button_file.removeClass('ajax-file-upload');
                        button_file.prop('id','singleupload1_content');
                        button_file.css({'padding':'6px 12px','margin-top':'3px','width':'100%'});
                    },
                    onSuccess:function(files,data,xhr)
                    {   
                        if(data.nombre != null){

                            

                            $(' #singleupload1_content').hide();
                            $(' #singleupload1_progress').hide();
                            $(' #singleupload1_finaly').show();
                            $(' #nombre_archivo').val(files);
                            $(' #eliminar_archivo').attr('data',data.nombre);
                            $(' #id_archivo_modal').val(data.nombre);
                            $(' #id_original_modal').val(data.original);

                            

                        }else{
                            $(' #singleupload1_content').show();
                            $(' #singleupload1_progress').hide();
                            $(' #singleupload1_finaly').hide();
                            $(' small#msg_id_archivo_modal').html("El archivo se ha seleccionado correctamente");
                        }
                        window.location.reload();

                    }
                });


        setTimeout(() => {
            if(archivo_comprobante.length > 0){
                $(' #singleupload1_content').hide();
                $(' #singleupload1_progress').hide();
                $(' #singleupload1_finaly').show();
                $(' #nombre_archivo').val(archivo_comprobante);
                $(' #eliminar_archivo').attr('data',archivo_comprobante);
                $(' #id_archivo_modal').val(archivo_comprobante);
                $(' #id_original_modal').val(archivo_comprobante);

                $('div#singleupload1_finaly .btn.btn-danger').removeAttr('disabled');
            }
        }, 500);
        


            });

</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>



@endsection