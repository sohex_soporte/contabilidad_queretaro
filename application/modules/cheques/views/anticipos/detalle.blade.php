@section('contenido')
<!-- <h4 class="mt-4 mb-4 text-gray-800 text-center">
    Alta de cheque
</h4> -->

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <form id="data_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="card-title mb-4">DATOS DEL CLIENTE</h5>
                        </div>

                        <input type="hidden" name="anticipo_id" id="anticipo_id" value="<?php echo array_key_exists('id',$anticipo_datos)? $anticipo_datos['id'] : ''; ?>" />
                        <input type="hidden" name="folio_anticipo" id="folio_anticipo" value="<?php echo array_key_exists('folio_id',$anticipo_datos)? $anticipo_datos['folio_id'] : ''; ?>" />

                        <input type="hidden" name="cfdi_clave" id="cfdi_clave" value="<?php echo (array_key_exists('cfdi',$cliente_datos) && is_array($cliente_datos['cfdi']) && array_key_exists('clave',$cliente_datos['cfdi']))? $cliente_datos['cfdi']['clave'] : ''; ?>" />
                        <input type="hidden" name="cfdi_descripcion" id="cfdi_descripcion" value="<?php echo (array_key_exists('cfdi',$cliente_datos) && is_array($cliente_datos['cfdi']) && array_key_exists('descripcion',$cliente_datos['cfdi']))? $cliente_datos['cfdi']['descripcion'] : ''; ?>" />

                        <input type="hidden" name="cliente_id" id="cliente_id" value="<?php echo array_key_exists('id',$cliente_datos)? $cliente_datos['id'] : ''; ?>" />
                        <input type="hidden" name="tipo_persona_id" id="tipo_persona_id" value="<?php echo (array_key_exists('regimen_fiscal',$cliente_datos) && $cliente_datos['regimen_fiscal'] == 'F')? 1 : 2; ?>" />

                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">RFC:</label>
                                        <div class="col-sm-10">
                                            <input readonly type="text" name="rfc" class="form-control"
                                                value="<?php echo array_key_exists('rfc',$cliente_datos)? $cliente_datos['rfc'] : ''; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Cliente:</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Nombre:</label>
                                                    <input readonly name="nombre" type="text" class="form-control" value="<?php echo array_key_exists('nombre',$cliente_datos)? $cliente_datos['nombre'] : ''; ?>" />
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Primer apellido:</label>
                                                    <input readonly name="apellido_paterno" type="text" class="form-control" value="<?php echo array_key_exists('apellido_paterno',$cliente_datos)? $cliente_datos['apellido_paterno'] : ''; ?>" />
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Segundo apellido:</label>
                                                    <input readonly name="apellido_materno" type="text" class="form-control" value="<?php echo array_key_exists('apellido_materno',$cliente_datos)? $cliente_datos['apellido_materno'] : ''; ?>" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Telefono:</label>
                                        <div class="col-sm-10">
                                            <input readonly name="telefono" type="text" class="form-control"
                                                value="<?php echo array_key_exists('telefono',$cliente_datos)? $cliente_datos['telefono'] : ''; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Correo electrónico:</label>
                                        <div class="col-sm-10">
                                            <input readonly type="text" name="correo_electronico" class="form-control"
                                                value="<?php echo array_key_exists('correo_electronico',$cliente_datos)? $cliente_datos['correo_electronico'] : ''; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Domicilio:</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Calle</label>
                                                    <input readonly name="direccion" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('direccion',$cliente_datos)? $cliente_datos['direccion'] : ''; ?>" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Numero</label>
                                                    <input readonly name="numero_ext" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('numero_ext',$cliente_datos)? $cliente_datos['numero_ext'] : ''; ?>" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Colonia</label>
                                                    <input readonly name="colonia" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('colonia',$cliente_datos)? $cliente_datos['colonia'] : ''; ?>" />
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-sm-4">
                                                    <label>Municipio</label>
                                                    <input readonly name="municipio" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('municipio',$cliente_datos)? $cliente_datos['municipio'] : ''; ?>" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Estado</label>
                                                    <input readonly name="estado" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('estado',$cliente_datos)? $cliente_datos['estado'] : ''; ?>" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Codigo postal</label>
                                                    <input readonly name="codigo_postal" type="text" class="form-control"
                                                        value="<?php echo array_key_exists('codigo_postal',$cliente_datos)? $cliente_datos['codigo_postal'] : ''; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-12">
                            <hr />
                        </div>

                        <div class="col-sm-12">
                            <h5 class="card-title mb-4">POLIZA</h5>
                        </div>
                        <div class="col-sm-8">

                            <div class="form-group row">
                                <label for="nomenclatura_id" class="col-sm-2 col-form-label">Nomenclatura de la
                                    poliza:</label>
                                <div class="col-sm-10">
                                    <select value="<?php echo $polizaNomenclatura; ?>" class="form-control"
                                        id="nomenclatura_id" name="nomenclatura_id"></select>
                                    <small id="msg_nomenclatura_id" class="form-text text-danger"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fecha_poliza" class="col-sm-2 col-form-label">Fecha:</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="fecha_poliza" name="fecha_poliza"
                                        value="<?php echo $fecha_creacion; ?>" max="<?php echo utils::get_date(); ?>">
                                    <small id="msg_fecha_poliza" class="form-text text-danger"></small>
                                </div>
                            </div>


                        </div>

                        <div class="col-sm-12">
                            <hr />
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Total:</label>
                                <div class="col-sm-10">
                                    <input readonly type="text" class="form-control input-lg"
                                        value="<?php echo array_key_exists('total',$anticipo_datos)? utils::format($anticipo_datos['total']) : ''; ?>" />
                                    <input name="total" type="hidden" class="form-control input-lg"
                                        value="<?php echo array_key_exists('total',$anticipo_datos)? $anticipo_datos['total'] : ''; ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <hr />
                        </div>


                        <div class="col-sm-8">

                            <div class="form-group row">
                                <label for="referencia" class="col-sm-2 col-form-label">Referencia:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="referencia"></textarea>
                                    <small id="msg_referencia" class="form-text text-danger"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="comentarios" class="col-sm-2 col-form-label">Comentarios:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="comentarios"></textarea>
                                    <small id="msg_comentarios" class="form-text text-danger"></small>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">

                            <button class="btn btn-warning ml-2 my-2 my-sm-0 float-right " onclick="APP.guardar();"
                                type="button"><i class="fas fa-save"></i> Generar cheque de devolución</button>

                            <a type="button" href="<?php echo site_url('cheques/anticipos'); ?>"
                                class="btn btn-outline-secondary ml-2 my-2 my-sm-0 float-right"><i class="fa fa-chevron-left"></i>
                                Regresar</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        </form>
    </div>
</div>
</div>
</div>

@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script>
    var identificador = "<?php echo $identificador; ?>";
</script>


<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/anticipos/detalle.js'); ?>"></script>
@endsection