@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    <?php echo ($tipo_persona == 1)? 'Alta de Cliente' : 'Alta de Proveedor'; ?>
</h4>

<script>
    const tipo_persona_id = '<?php echo $tipo_persona; ?>';
</script>

<div class="row">
    <div class="col-xl-7 col-lg-9 col-md-11 col-sm-12 mx-auto">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <form id="data_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 mt-3">
                                    <h5 class="card-title mb-4">Datos del <?php echo ($tipo_persona == 1)? 'cliente' : 'proveedor'; ?></h5>
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <div class="row" style="display:none;">
                                        <input type="hidden" id="tipo_persona_id" name="tipo_persona_id" class="form-control" value="<?php echo $tipo_persona; ?>" />
                                    </div>

                                    
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="rfc" class="col-form-label"><span
                                                        class="text-danger">*</span> RFC:</label>
                                                <div class="">
                                                    <input type="text" class="form-control" id="rfc" name="rfc" />
                                                    <small id="msg_rfc" class="form-text text-danger"></small>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="telefono" class=" col-form-label"><span
                                                        class="text-danger">*</span> Telefono:</label>
                                                <div class="">
                                                    <input type="number" class="form-control" id="telefono"
                                                        name="telefono">
                                                    <small id="msg_telefono" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="correo_electronico" class=" col-form-label">Correo
                                                    electronico:</label>
                                                <div>
                                                    <input type="text" class="form-control" id="correo_electronico"
                                                        name="correo_electronico"
                                                        value="<?php echo (isset($datos)? $datos['correo_electronico'] : '') ?>">
                                                    <small id="msg_correo_electronico"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   

                                    <div class="row">
                                        <div class="col-sm-12" id="">
                                            <div class="form-group ">
                                                <label for="razon_social" id="label_razon_social" class="col-form-label"><span
                                                        class="text-danger">*</span> Razon social:</label>
                                                <div class="">
                                                    <input type="text" class="form-control" id="razon_social" name="razon_social" />
                                                    <small id="msg_razon_social" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="row">
                                        <div class="col-sm-4" id="campo_nombre_razon">
                                            <div class="form-group ">
                                                <label for="nombre" id="label_nombre" class="col-form-label"><span
                                                        class="text-danger">*</span> Nombre:</label>
                                                <div class="">
                                                    <input type="text" class="form-control" id="nombre" name="nombre" />
                                                    <small id="msg_nombre" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group  cliente_comp">
                                                <label for="apellido1" class="col-form-label"><span
                                                        class="text-danger">*</span> Primer apellido:</label>
                                                <div class="">
                                                    <input type="text" class="form-control" id="apellido1"
                                                        name="apellido1" />
                                                    <small id="msg_apellido1" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group  cliente_comp">
                                                <label for="apellido2" class=" col-form-label">Segundo apellido:</label>
                                                <div class="">
                                                    <input type="text" class="form-control" id="apellido2"
                                                        name="apellido2" />
                                                    <small id="msg_apellido2" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="id_estado" class="c col-form-label"><span
                                                        class="text-danger">*</span> Estado:</label>
                                                <div class="">
                                                    <select class="form-control" id="id_estado" name="id_estado">
                                                    </select>
                                                    <small id="msg_id_estado"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="id_municipio" class="c col-form-label"><span
                                                        class="text-danger">*</span> Municipio:</label>
                                                <div class="">
                                                    <select class="form-control" id="id_municipio" name="id_municipio">
                                                    </select>
                                                    <small id="msg_id_municipio"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="id_colonia" class="c col-form-label"><span
                                                        class="text-danger">*</span> Colonia:</label>
                                                <div class="">
                                                    <select class="form-control" id="id_colonia" name="id_colonia">
                                                    </select>
                                                    <small id="msg_id_colonia"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="calle" class="col-form-label">Calle:</label>
                                                <div class="">
                                                    <input type="text" max-length="5" class="form-control"
                                                        id="calle" name="calle">
                                                    <small id="msg_calle" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="numero" class="col-form-label">Numero:</label>
                                                <div class="">
                                                    <input type="text" max-length="5" class="form-control"
                                                        id="numero" name="numero">
                                                    <small id="msg_numero" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>

                                       

                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="codigo_postal" class="col-form-label">Codigo
                                                    postal:</label>
                                                <div class="">
                                                    <input type="number" max-length="5" class="form-control"
                                                        id="codigo_postal" name="codigo_postal">
                                                    <small id="msg_codigo_postal" class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($tipo_persona == 2){ ?>
                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <h5 class="card-title mb-4">Configuración del proveedor</h5>
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="id_poliza_nomenclatura" class="col-form-label"><span
                                                        class="text-danger">*</span> Poliza:</label>
                                                <div class="">
                                                    <select onchange="APP.ver_polizas(this.value);" class="form-control"
                                                        id="id_poliza_nomenclatura" name="id_poliza_nomenclatura">
                                                        <option value="">Seleccione una opción</option>
                                                        <?php if(is_array($listado_polizas)){ ?>
                                                        <?php foreach ($listado_polizas as $key => $value) { ?>
                                                        <option value="<?php echo $value['id'] ?>">
                                                            <?php echo '['.$value['id'].'] '.$value['descripcion'] ?>
                                                        </option>
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                    <small id="msg_id_poliza_nomenclatura"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4" id="polizas_fijas_content" style="display:none;">
                                            <div class="form-group row">
                                                <label for="id_poliza_fija" class="col-form-label"><span
                                                        class="text-danger">*</span> Poliza Fija:</label>
                                                <div class="">
                                                    <select class="form-control" id="id_poliza_fija"
                                                        name="id_poliza_fija">
                                                        <option value="">Seleccione una opción</option>
                                                        <?php if(is_array($listado_polizas_fijas)){ ?>
                                                        <?php foreach ($listado_polizas_fijas as $key => $value) { ?>
                                                        <option value="<?php echo $value['id'] ?>">
                                                            <?php echo '['.$value['id'].'] '.$value['poliza'] ?>
                                                        </option>
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                    <small id="msg_id_poliza_fija"
                                                        class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>Subtotal</h6>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-4">
                                            <label class="control-label">Cuenta</label>
                                            <select class="form-control" id="id_cuenta_subtotal"
                                                name="id_cuenta_subtotal">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($cuentas)){ ?>
                                                <?php foreach ($cuentas as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo '['.$value['cuenta'].'] '.$value['decripcion'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_id_cuenta_subtotal" class="form-text text-danger"></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control" id="subtotal_tipo" name="subtotal_tipo">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($tipo)){ ?>
                                                <?php foreach ($tipo as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo $value['nombre'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_subtotal_tipo" class="form-text text-danger"></small>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>IVA</h6>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-4">
                                            <label class="control-label">Cuenta</label>
                                            <select class="form-control" id="id_cuenta_iva" name="id_cuenta_iva">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($cuentas)){ ?>
                                                <?php foreach ($cuentas as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo '['.$value['cuenta'].'] '.$value['decripcion'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_id_cuenta_iva" class="form-text text-danger"></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control" id="iva_tipo" name="iva_tipo">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($tipo)){ ?>
                                                <?php foreach ($tipo as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo $value['nombre'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_iva_tipo" class="form-text text-danger"></small>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h6>TOTAL</h6>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-4">
                                            <label class="control-label">Cuenta</label>
                                            <select class="form-control" id="id_cuenta_total" name="id_cuenta_total">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($cuentas)){ ?>
                                                <?php foreach ($cuentas as $key => $value) {  ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo '['.$value['cuenta'].'] '.$value['decripcion'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_id_cuenta_total" class="form-text text-danger"></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control" id="total_tipo" name="total_tipo">
                                                <option value="">Seleccione una opción</option>
                                                <?php if(is_array($tipo)){ ?>
                                                <?php foreach ($tipo as $key => $value) { ?>
                                                <option value="<?php echo $value['id'] ?>">
                                                    <?php echo $value['nombre'] ?>
                                                </option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <small id="msg_total_tipo" class="form-text text-danger"></small>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <?php } ?>                             
                        </div>


                    </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class=" col-sm-12">
                        <button class="btn btn-primary ml-2 my-2 my-sm-0 float-right" onclick="APP.guardar();" type="button"><i class="fas fa-save"></i> Guardar</button>
                        <a type="button" href="<?php echo site_url('cheques/personas'); ?>"
                            class="btn btn-secondary ml-2 my-2 my-sm-0 float-right"><i class="fa fa-times"></i>
                            Cancelar</a>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('style')
<?php $this->carabiner->display('datatables','css') ?>
<?php $this->carabiner->display('select2','css') ?>
<?php $this->carabiner->display('sweetalert2','css') ?>
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 12px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script>
    var identificador = "<?php echo $identificador; ?>";


    // $(function () {

    //     $('input#rfc').blur(function () {

    //         if ($('#rfc').val().length == 12) {
    //             $('div#campo_nombre_razon').removeClass('col-sm-4');
    //             $('div#campo_nombre_razon').addClass('col-sm-12');
    //             $('#label_nombre').html('<span class="text-danger">*</span> Razón social:');
    //             $('.cliente_comp').hide();

    //             $('input#apellido1').val('');
    //             $('input#apellido2').val('');
    //         } else {
    //             $('div#campo_nombre_razon').removeClass('col-sm-12');
    //             $('div#campo_nombre_razon').addClass('col-sm-4');
    //             $('#label_nombre').html('<span class="text-danger">*</span> Nombre:');
    //             $('.cliente_comp').show();
    //         }

    //     });

    //     setTimeout(() => {
    //         $('input#rfc').blur()
    //     }, 50);

    // })
</script>


<?php $this->carabiner->display('datatables','js') ?>
<?php $this->carabiner->display('select2','js') ?>
<?php $this->carabiner->display('sweetalert2','js') ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"
    integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="<?php echo base_url('assets/js/scripts/cheques/personas/nuevo.js'); ?>"></script>
@endsection