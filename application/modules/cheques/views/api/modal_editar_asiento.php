<form id="data_form_modal" >
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group ">
                <label for="modal_cuentas" class="col-form-label">Cuenta:</label>
                <select  class="form-control" id="modal_cuentas" name="modal_cuentas" >
                    <?php if(is_array($cuentas) && count($cuentas)>0){ ?>
                        <?php foreach ($cuentas as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>" desc="<?php echo $value['decripcion']; ?>"><?php echo $value['cuenta']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                <small id="msg_modal_cuentas" class="form-text text-danger"></small>
                
            </div>

            <div class="form-group ">
                <label for="modal_tipo" class="col-form-label">Tipo:</label>
                
                    <select  class="form-control" id="modal_tipo" name="modal_tipo" >
                            <option value="1">Cargo</option>
                            <option value="2">Abono</option>
                    </select>
                    <small id="msg_modal_tipo" class="form-text text-danger"></small>
                
            </div>
        </div>
        <!-- <div class="col-sm-4">
            <div class="form-group">
                <label for="precio_unitario" class="col-form-label">Precio Unitario</label>
                <input onblur="APP.calcular_total();" type="number" step="0.01" class="form-control" id="precio_unitario" name="precio_unitario" min="0">
                <small id="msg_precio_unitario" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="cantidad" class="col-form-label">Cantidad</label>
                <input onblur="APP.calcular_total();" type="number" step="0.1" class="form-control" id="cantidad" name="cantidad"  min="0" value="1">
                <small id="msg_cantidad" class="form-text text-danger"></small>
            </div>
        </div> -->
        <div class="col-sm-12">
            <div class="form-group">
                <label for="precio" class=" col-form-label">Monto</label>
                <input type="number" step="0.01" class="form-control" id="precio" name="precio" min="0" >
                <small id="msg_precio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="concepto" class="col-form-label">Concepto</label>
                <textarea type="text"  class="form-control" id="concepto" name="concepto"></textarea>
                <small id="msg_concepto" class="form-text text-danger"></small>
            </div>

            <!-- <div class="form-group">
                <label for="tipo_unidad" class="col-form-label">Tipo de unidad</label>
                <select  class="form-control" id="tipo_unidad" name="tipo_unidad" >
                    <?php if(is_array($unidades) && count($unidades)>0){ ?>
                        <?php foreach ($unidades as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>" desc="<?php echo $value['descripcion']; ?>"><?php echo $value['nombre']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                <small id="msg_tipo_unidad" class="form-text text-danger"></small>
            </div> -->
        </div>
    </div>
</form>