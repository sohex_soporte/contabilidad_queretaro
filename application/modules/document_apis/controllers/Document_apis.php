<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @OA\Info(
 *  title="WebServices REST",
 *  version="1.0"
 * )
 * @OA\Server(url="https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/")
 * @OA\Server(url="http://localhost/contabilidad_queretaro")
 */
class Document_apis extends MX_Controller
{
    protected $format    = 'json';

    public function __construct()
    {
        parent::__construct();
    }

    public function contabilidad()
    {
        $path = __FILE__;
        $path2 = FCPATH.'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'asientos'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR;
        $path3 = FCPATH.'application'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'enlaces'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR;
        $openapi = \OpenApi\Generator::scan([
            $path,
            $path2,
            $path3
        ]);
        header('Content-Type: application/json');
        echo $openapi->toJson();
        exit;
    }
}
