<?php
/**

 **/
class Refacciones_sjrModel extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    
    public function getRefacciones(){
      $database = $this->load->database('default', TRUE);
      
      $query = $database->query("call pa_calcular_refacciones_sjr();");

      $database->close();
      return $query->result_array();
  }
      
}
