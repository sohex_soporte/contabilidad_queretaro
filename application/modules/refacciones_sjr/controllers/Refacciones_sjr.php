<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Refacciones_sjr extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }

    public function index(){ 
        $this->load->model('Refacciones_sjrModel');
        $procedure  = $this->Refacciones_sjrModel->getRefacciones();
        $data = [];
        $class = '';
        $tipo_porcentaje = '';
        foreach($procedure as $pro) {
            switch ($pro['nombre']) {
                case 'TOTAL VENTAS':
                    $class = "text-success bold";
                    break;
                case 'COSTO DE VENTAS':
                    $class = 'text-warning bold';
                    break;
                case '% SOBRE VENTAS':
                    $tipo_porcentaje = true;
                    break;
                case '% UT BRUTA':
                    $tipo_porcentaje = true;
                    break;
                case '% SOBRE VENTAS':
                    $tipo_porcentaje = true;
                    break;
                default:
                    $class = "";
                    $tipo_porcentaje = false;
                    break;
            }
            $total = 0;
            for ($i=1; $i<=12; $i++) {
                 $total += $pro['mes_'.$i];
            }
            $pro['class'] = $class;
            $pro['total'] = $total;
            $pro['tipo_porcentaje'] = $tipo_porcentaje;

            array_push($data, $pro);
        }
        $this->blade->render('formato', $data);
    }
    public function formato()
    {
        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre','diciembre'];
        $captions = ['nombre','enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre','diciembre','total'];

        $data['captions'] = $captions;
        $data['ventas'] = [
            [
                'nombre' => 'MENUDEO', 'enero' => 6400, 'febrero' => 1000, 'marzo' => 700, 'abril' => 0, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 100, 'total' => 4000
            ],
            [
                'nombre' => 'MAYOREO', 'enero' => 200, 'febrero' => 150, 'marzo' => 100, 'abril' => 0, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 1550
            ],
            [
                'nombre' => 'SERVICIO', 'enero' => 5500, 'febrero' => 100, 'marzo' => 1300, 'abril' => 9300, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 7000
            ],
            [
                'nombre' => 'GARANTIAS', 'enero' => 100, 'febrero' => 100, 'marzo' => 100, 'abril' => 100, 'mayo' => 180, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 980
            ],
            [
                'nombre' => 'HYP', 'enero' => 0, 'febrero' => 0, 'marzo' => 100, 'abril' => 100, 'mayo' => 180, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 1380
            ]
        ];

        $total_ventas = [];
        foreach ($data['ventas'] as $val) {
            $tot['nombre'] = "TOTAL VENTAS";
            $tot['total'] += $val['total'];

            for ($i=0; $i<12; $i++) {
                $tot[$meses[$i]] += $val[$meses[$i]];
            }
        }
        array_push($total_ventas, $tot);
        $data['total_ventas'] = $total_ventas;
        

        $data['costos_ventas'] = [

            [
                'nombre' => 'MENUDEO', 'enero' => 100, 'febrero' => 100, 'marzo' => 100, 'abril' => 0, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 400
            ],
            [
                'nombre' => 'MAYOREO', 'enero' => 100, 'febrero' => 50, 'marzo' => 200, 'abril' => 100, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 600
            ],
            [
                'nombre' => 'SERVICIO', 'enero' => 200, 'febrero' => 100, 'marzo' => 100, 'abril' => 100, 'mayo' => 100, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 800
            ],
            [
                'nombre' => 'GARANTIAS', 'enero' => 100, 'febrero' => 100, 'marzo' => 100, 'abril' => 100, 'mayo' => 180, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 580
            ],
            [
                'nombre' => 'HYP', 'enero' => 0, 'febrero' => 0, 'marzo' => 100, 'abril' => 100, 'mayo' => 180, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 600
            ],
            [
                'nombre' => 'PROV. INVENTARIO OBSOLETO', 'enero' => 70, 'febrero' => 40, 'marzo' => 100, 'abril' => 100, 'mayo' => 180, 'junio' => 0, 'julio' => 0, 'agosto' => 0, 'septiembre' => 0, 'octubre' => 0,
                'noviembre' => 0, 'diciembre' => 0, 'total' => 500
            ],
        ];

        $total_costo_ventas = [];
        $tot = [];
        foreach ($data['costos_ventas'] as $val) {
            $tot['nombre'] = "COSTO DE VENTAS";
            $tot['total'] += $val['total'];
            for ($i=0; $i<12; $i++) {
                $tot[$meses[$i]] += $val[$meses[$i]];
            }
        }
        array_push($total_costo_ventas, $tot);

        $data['total_costo_ventas'] = $total_costo_ventas;
        
        $utilidades = [];
        $porcentajes = [];

        foreach ($total_ventas as $tot_venta) {
            foreach($total_costo_ventas as $tot_costo) {
                for ($i=0; $i<12; $i++) {
                    $tot_utilidad = $tot_venta[$meses[$i]] - $tot_costo[$meses[$i]];
                    $utilidad['nombre'] = 'UTILIDAD BRUTA';
                    $utilidad[$meses[$i]] = $tot_utilidad;
                    $utilidad['total']  = $tot_venta['total'] - $tot_costo['total'];
                    $porcentaje['nombre'] = '% UT BRUTA';
                    $calculo_porcentaje = ((($tot_utilidad * 100) / $tot_venta[$meses[$i]]) / 100);
                    $calculo_porcentaje_total = ((($utilidad['total'] * 100) / $tot_venta['total']) / 100);
                    $porcentaje[$meses[$i]] = $calculo_porcentaje >= 0 ? round($calculo_porcentaje,2) : 0;
                    $porcentaje['total'] = $calculo_porcentaje_total >= 0 ? round($calculo_porcentaje_total,2) : 0;
                }
                array_push($utilidades, $utilidad);
                array_push($porcentajes, $porcentaje);
            }
        }

        $data['utilidades'] = $utilidades;
        $data['porcentajes'] = $porcentajes;

        $data['sueldos_comisiones'] = [
            [
                'nombre' => 'SUELDOS OTROS', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'PRESTACIONES', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'INDEMNIZACIONES', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'IMSS, SAR E INFONAVIT', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => '2% SOBRE NOMINA', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'SEGURO DE TRASLADO', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'GASTOS DE ENTREGA', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'FLETES', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'INTERESES', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'MANTENIMIENTO', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'TOTAL GASTOS DE VENTA', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
        ];

        $data['porcentaje_sobre_ventas'] = [
            [
                'nombre' => '% SOBRE VENTAS', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
        ];
        $data['antes_transferencia'] = [
            [
                'nombre' => 'ANTES DE TRANSFERENCIA', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
        ];

        $data['transferencias'] = [
            [
                'nombre' => 'A SERVICIO FORD', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'A SERVICIO LINCOLN', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'A BODY SHOP', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => 'TOTAL TRANSF', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
        ];
        $data['utilidad_ajustes_porcentajes'] = [
            [
                'nombre' => 'UTILIDAD AJUSTADA', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
            [
                'nombre' => '% SOBRE VENTAS', 'enero' => '-', 'febrero' => '-', 'marzo' => '-', 'abril' => '-', 'mayo' => '-', 'junio' => '-', 'julio' => '-', 'agosto' => '-', 'septiembre' => '-', 'octubre' => '-',
                'noviembre' => '-', 'diciembre' => '-', 'total' => '-'
            ],
        ];

        $this->blade->render('index', $data);
    }
}
