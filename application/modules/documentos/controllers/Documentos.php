<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Documentos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minventario', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
        $datos['index'] = '';
        
        $this->blade->render('alta_documentos',$datos);
    }
}