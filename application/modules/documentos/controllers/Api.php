<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller
{
    public function get_archivo(){

        $id_archivo = $this->input->get('id_archivo');
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://mylsa.iplaneacion.com/dms/contabilidad_apis/documentos/archivos/obtener_documento?id_archivo='.$id_archivo,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: ci_session=goe7jf2q9ah4tkdimeas08b9f0s2kqhh'
        ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        try {
            $documento_nombre = 'documento.txt';
            $content_type = $info['content_type'];
            
            if (preg_match("/pdf/i", $content_type)) {
                $documento_nombre = 'documento.pdf';
            }elseif (preg_match("/pdf/i", $content_type)) {
                $documento_nombre = 'documento.xml';
            }
            
            $this->load->helper('download');
            force_download($response,$documento_nombre);        

        } catch (\Throwable $th) {
            //throw $th;
        }
        
        exit();
    }
}