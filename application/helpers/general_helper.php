<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function existe_cita($id_operador = '', $fecha = '', $hora = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->existe_cita($id_operador, $fecha, $hora);
}
function getHoraComidaTecnico($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraComidaTecnico($hora, $fecha, $id_tecnico);
}
function getHoraLaboralTecnico($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraLaboralTecnico($hora, $fecha, $id_tecnico);
	//1
}
function getStatusCita($id_horario = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getStatusCita($id_horario);
}
function HoraBetweenCita($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->HoraBetweenCita($hora, $fecha, $id_tecnico);
}
function getColorCita($id_cita = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getColorCita($id_cita);
}
function getHoraWork($fecha = '', $idtecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraWork($fecha, $idtecnico);
	//1
}
function getIdIntelisis($id_cita = '')
{
	$ins = &get_instance();
	$ins->load->model('intelisis/m_intelisis', 'mi');
	return $ins->mi->getIdIntelisis($id_cita);
}
//Regresa si el usuario tiene permisos para realizar una acción, ej. editar el horario o precio de una operación
function PermisoAccion($accion = '', $conAdmin = true)
{
	$CI = &get_instance();
	if ($CI->session->userdata('id') == '') {
		redirect(site_url());
	}
	if ($conAdmin) {
		$q_user = $CI->db->where('adminId', $CI->session->userdata('id'))
			->select('idRol')
			->get('admin');

		if ($q_user->row()->idRol == 1 || $q_user->row()->idRol == 2) {
			return true;
		}
	}
	$q = $CI->db->where('id', $CI->session->userdata('id'))
		->where('accion', $accion)
		->get('permisos_acciones_usuarios');
	if ($q->num_rows() == 1) {
		return true;
	}
	return false;
}
//Regresa si el usuario tiene permisos para realizar una acción, ej. editar el horario o precio de una operación
function PermisoModulo($controlador = '', $accion = '')
{
	$CI = &get_instance();
	//Revisar si es admin o gerente
	//adminStatus 1.-activada,0.-desactivada
	//status 1.-admin, 2.-empleado,3.-contacto, 4 gerente
	if ($CI->session->userdata('id') == '') {
		redirect(site_url());
	}
	$q_user = $CI->db->where('adminId', $CI->session->userdata('id'))
		->select('idRol')
		->get('admin');

	if ($q_user->row()->idRol == 1 || $q_user->row()->idRol == 2) {
		return true;
	}
	if ($accion != '') {
		$CI->db->where('accion', $accion);
	}
	if ($controlador == '') {
		return false;
	}
	$q = $CI->db->where('id_usuario', $CI->session->userdata('id'))
		->where('controlador', $controlador)
		->get('permisos_modulos');
	if ($q->num_rows() == 1) {
		return true;
	}
	return false;
}
function minutosTranscurridos($fecha_i, $fecha_f)
{
	$minutos = (strtotime($fecha_i) - strtotime($fecha_f)) / 60;
	$minutos = abs($minutos);
	$minutos = floor($minutos);
	return $minutos;
}
function dateDiffMinutes($fecha_inicio = '', $fecha_fin = '')
{
	if ($fecha_inicio == '0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00') {
		return 0;
	}
	// ESTABLISH THE MINUTES PER DAY FROM START AND END TIMES
	$start_time = '08:0:00';
	$end_time = '18:00:00';

	$start_ts = strtotime($start_time);
	$end_ts = strtotime($end_time);
	$minutes_per_day = (int)(($end_ts - $start_ts) / 60) + 1;

	// ESTABLISH THE HOLIDAYS
	$holidays = array(
		//'Feb 04', // MLK Day
	);

	// CONVERT HOLIDAYS TO ISO DATES
	foreach ($holidays as $x => $holiday) {
		$holidays[$x] = date('Y-m-d', strtotime($holiday));
	}

	$fecha_sol = $fecha_inicio;
	$fecha_menor = $fecha_fin;
	// CHECK FOR VALID DATES
	$start = strtotime($fecha_sol);
	$end = strtotime($fecha_menor);
	$start_p = date('Y-m-d H:i:s', $start);
	$end_p = date('Y-m-d H:i:s', $end);

	// MAKE AN ARRAY OF DATES
	$workdays = array();
	$workminutes = array();
	// ITERATE OVER THE DAYS
	$start = $start - 60;
	while ($start < $end) {
		$start = $start + 60;
		// ELIMINATE WEEKENDS - SAT AND SUN
		$weekday = date('D', $start);
		//if (substr($weekday,0,1) == 'S') continue;
		// ELIMINATE HOURS BEFORE BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime < date('H:i:s', $start_ts))) continue;
		// ELIMINATE HOURS PAST BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime > date('H:i:s', $end_ts))) continue;
		// ELIMINATE HOLIDAYS
		$iso_date = date('Y-m-d', $start);
		if (in_array($iso_date, $holidays)) continue;
		$workminutes[] = $iso_date;
		// END ITERATOR
	}
	$number_of_workminutes = (count($workminutes));
	$number_of_minutes = number_format($minutes_per_day);
	$horas_habiles = number_format($number_of_workminutes / 60, 2);
	if ($number_of_workminutes > 0) {
		$number_of_minutes = $number_of_workminutes - 1;
	}
	return $number_of_workminutes;
}
function random($num)
{
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	for ($i = 0; $i < $num; $i++) {
		$string .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $string;
}
function dayOfweek($day)
{
	switch ($day) {
		case 1:
			return 'Lunes';
			break;
		case 2:
			return 'Martes';
			break;
		case 3:
			return 'Míercoles';
			break;
		case 4:
			return 'Jueves';
			break;
		case 5:
			return 'Viernes';
			break;
		case 6:
			return 'Sábado';
			break;
		case 7:
			return 'Domingo';
			break;
	}
}
function getFolioMostrar($id_sucursal = '', $paquete = false)
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_servicios', 'ms');
	$siglas = $ins->ms->getSiglasSucursal($id_sucursal);
	$folio = (int)$ins->ms->getFolio() + 1;
	return ($paquete ? 'P-' : 'S-') . $siglas . '-' . $folio;
}
function getFolioControl()
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_servicios', 'ms');
	$folio = $ins->ms->getFolio();
	return (int)$folio + 1;
}
function getFolioSubPaquete($idservicio = '')
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_paquetes', 'mp');
	return $ins->mp->getFolioSubpaquete($idservicio);
}
function getPrecioServicio($id_modelo = '', $id_servicio)
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_servicios', 'ms');
	return $ins->ms->getPrecioServicio($id_modelo, $id_servicio);
}
function curlPost($url = '', $data, $is_json_request = false)
{
	$curl = curl_init();
	$final_url =  $url;
	$parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);

	curl_setopt_array($curl, array(
		CURLOPT_URL => $final_url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_HEADER => true,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $parametros,
	));
	$body = curl_exec($curl);
	// extract header
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	$headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	$header = substr($body, 0, $headerSize);
	$header = getHeaders($header);

	// extract body
	$body = substr($body, $headerSize);
	curl_close($curl);

	if ($httpcode == 400 && isset($header) && count($header) > 0) {
		return [
			'status_code' => $httpcode,
			'data' => $header['X-Message']
		];
	}

	return $body;
}
function curlGet($url = '', $data, $is_json_request = false)
{
	$curl = curl_init();
	$final_url =  $url;
	$parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);
	curl_setopt_array($curl, array(
		CURLOPT_URL => $final_url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_HEADER => true,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_POSTFIELDS => $parametros,
	));
	$body = curl_exec($curl);
	// extract header
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	$headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	$header = substr($body, 0, $headerSize);
	$header = getHeaders($header);

	// extract body
	$body = substr($body, $headerSize);
	curl_close($curl);

	if ($httpcode == 400 && isset($header) && count($header) > 0) {
		return [
			'status_code' => $httpcode,
			'data' => $header['X-Message']
		];
	}

	return $body;
}
function getHeaders($respHeaders)
{
	$headers = array();
	$headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));


	foreach (explode("\r\n", $headerText) as $i => $line) {
		if ($i === 0) {
			$headers['http_code'] = $line;
		} else {
			list($key, $value) = explode(': ', $line);
			if ($key == 'X-Message') {
				$headers[$key] = $value;
			}
		}
	}

	return $headers;
}

function allUsers($telefono)
{
	$CI = &get_instance();

	$usuarios_admin = $CI->db->select('adminNombre as nombre, telefono, "operador" as tipo')->where('telefono', $telefono)->get('admin')->result_array();
	$usuarios_cliente = $CI->db->select('nombre, telefono, "cliente" as tipo')->where('telefono', $telefono)->get('usuarios')->result_array();
	$usuarios_recomendador = $CI->db->select('nombre, celular as telefono, "recomentador" as tipo')->where('celular', $telefono)->get('recomendadores')->result_array();
	$usuarios_lavadores = $CI->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "lavador" as tipo')->where('lavadorTelefono', $telefono)->get('lavadores')->result_array();
	$usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);

	return $usuarios;
}
function procesarResponseApiJsonToArray($response_data)
{
	return json_decode($response_data);
}
function renderInputText($type, $name, $label, $value, $reaonly = false)
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo "<input id='" . $name . "' name='" . $name . "' readonly type='" . $type . "' class='form-control' value='" . $value . "'>";
	} else {
		echo "<input id='" . $name . "' name='" . $name . "' type='" . $type . "' class='form-control' value='" . $value . "'>";
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function renderInputTextArea($name, $label, $value, $reaonly = false)
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo form_textarea('', $value, 'class="form-control" readonly');
	} else {
		echo form_textarea(['id' => $name, 'name' => $name], $value, 'class="form-control"');
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}
function porcentajeProducto($descuento, $precio_producto, $cantidad = 1)
{
	$des = (int) $descuento / 100;
	$precio_total = (100 * $des) + $precio_producto;
	return $precio_total * $cantidad;
}

function obtenerPorcentaje($porcentaje_descuento, $precio_producto, $cantidad = 1)
{

	$descuento = (int) $porcentaje_descuento / 100;
	$tot_porcentaje = $precio_producto * $descuento;
	return $precio_producto + $tot_porcentaje;
}

function totalPrecioCantidadProducto($precio_producto, $cantidad)
{
	return $precio_producto * $cantidad;
}

function obtenerFechaEnLetra($fecha)
{
	$num = date("j", strtotime($fecha));
	$anno = date("Y", strtotime($fecha));
	$mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	$mes = $mes[(date('m', strtotime($fecha)) * 1) - 1];

	return  $num . ' de ' . $mes . ' del ' . $anno;
}

function renderSelectArray($name, $label = null, $dataArray, $nameValue, $nameOption, $selected = null, $disabled = false)
{
	echo "<div class='form-group'>";
	if ($label != null) {
		echo "<label for='" . $name . "'>" . $label . "</label>";
	}
	$disabled = $disabled ? "disabled" : null;
	echo "<select " . $disabled . " class='form-control' name='" . $name . "' id='" . $name . "'>";
	if (is_array($dataArray)) {
		echo "<option value=''> Seleccionar ..</option>";
		foreach ($dataArray as $row) :
			if ($selected != null) {
				if ($row->{$nameValue} == $selected) {
					echo "<option value='" . $row->{$nameValue} . "' selected>" . $row->{$nameOption} . "</option>";
				} else {
					echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
				}
			} else {
				echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
			}
		endforeach;
	}
	echo "</select>";
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function aFecha($date, $small_format = FALSE, $tipo = false)
{

	//valida si viene vacío.
	if (strlen(trim($date)) == 0 or $date == "0000-00-00 00:00:00") {
		return '';
	}

	//convierte la fecha a timestamp
	$date = strtotime($date);

	//extrae el mes.
	$mes = date('m', $date);

	if ($small_format) {
		switch ($mes) {
			case '01':
				$_mes = "ene";
				break;
			case '02':
				$_mes = "feb";
				break;
			case '03':
				$_mes = "mar";
				break;
			case '04':
				$_mes = "abr";
				break;
			case '05':
				$_mes = "may";
				break;
			case '06':
				$_mes = "jun";
				break;
			case '07':
				$_mes = "jul";
				break;
			case '08':
				$_mes = "ago";
				break;
			case '09':
				$_mes = "sep";
				break;
			case '10':
				$_mes = "oct";
				break;
			case '11':
				$_mes = "nov";
				break;
			case '12':
				$_mes = "dic";
				break;
		}
	} else {
		switch ($mes) {
			case '01':
				$_mes = "Enero";
				break;
			case '02':
				$_mes = "Febrero";
				break;
			case '03':
				$_mes = "Marzo";
				break;
			case '04':
				$_mes = "Abril";
				break;
			case '05':
				$_mes = "Mayo";
				break;
			case '06':
				$_mes = "Junio";
				break;
			case '07':
				$_mes = "Julio";
				break;
			case '08':
				$_mes = "Agosto";
				break;
			case '09':
				$_mes = "Septiembre";
				break;
			case '10':
				$_mes = "Octubre";
				break;
			case '11':
				$_mes = "Noviembre";
				break;
			case '12':
				$_mes = "Diciembre";
				break;
		}
	}

	if ($small_format) {
		//Formato YY
		$anio = date('Y', $date);
		$mes = date('m', $date);
		$dia = date('d', $date);
	} else {
		//Formato YYYY
		$anio = date('Y', $date);
		$dia = date('j', $date);
	}

	//forma la cadena de la fecha en formato legible.
	if ($small_format) {
		$output = "$dia/$mes/$anio";
	} else {
		$output = "$dia de $_mes de $anio";
	}
	if ($tipo == true) {
		if ($tipo == "dia")
			return $dia;
		if ($tipo == "anio")
			return $anio;
		else
			return false;
	}

	//retorna la fecha en formato legible.
	return $output;
}

if (!function_exists('validation_errors_array')) {

	function validation_errors_array($prefix = '', $suffix = '')
	{
		if (FALSE === ($OBJ = &_get_validation_object())) {
			return '';
		}

		return $OBJ->error_array($prefix, $suffix);
	}
}

if (!function_exists('dd')) {

	function dd($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die();
	}
}


function renderTexto($name, $label, $id = '')
{

	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'><strong>" . $label . "</strong></label>";
	//echo form_upload(['name' => $name, 'id' => empty($id) ? $name : $id], '', ['class' => 'form-control-file']);
	//echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}
