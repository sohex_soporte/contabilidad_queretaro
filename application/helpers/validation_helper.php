<?php

function get_guid()
{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return $charid;
}
function nombre_cuenta($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('cuentas_dms')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->cuenta."-".$nombre->decripcion	;
}

	function validate($context) {
		$output = array();
		$output['status'] = false;
		$context->form_validation->set_error_delimiters('', '');
		$validated = $context->form_validation->run();
		if ($validated)
		{
			$output['status'] = true;
			$output =  $output;
		}
		else
		{
			$output['errors'] = validation_errors();
		}
		if (array_key_exists('errors', $output)) {
			$errors = explode("\n", $output['errors']);
			foreach ($errors as $key => $error) {
				$errors[$key] = $error;//json_decode($error);
			}
			$output['errors'] = $errors;
		}
		/*
		if (defined('PHPUNIT_TEST')) {
			return json_encode(array('output' => $output));
		} else {
			$context->load->view('json', array('output' => $output));
		}
		*/
		//return json_encode(array('output' => $output));
		return $output;
	}
