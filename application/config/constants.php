<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

////////////////////////////////////////////////////////////////////////////////////////////////////
//Constantes para encriptados
switch (trim($_SERVER['HTTP_HOST'], '/')) {
    case 'iplaneacion.com.mx':
    case 'mylsa.iplaneacion.com':
        $url = 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/api/';
        $url_autos = 'https://mylsa.iplaneacion.com/dms/autos_nuevos_queretaro/';
        break;
    default:
        $url = 'http://localhost:8000/api/';
        $url_autos = 'http://localhost/autos_queretaro/';
        break;
}

defined('API_URL_DMS') or define('API_URL_DMS', $url);
defined('API_URL_AUTOS') or define('API_URL_AUTOS', $url_autos);
defined('HTTP_OK') or define('HTTP_OK', 200);



#CONFIGURACIONES
$settings = include('settings.php');
switch (trim($_SERVER['HTTP_HOST'], '/')) {
    case 'mylsa.iplaneacion.com':
    case 'www.mylsa.iplaneacion.com':
        $config = $settings->produccion;
        break;
    default:
        $config = $settings->desarrollo;
        break;
}

$port = ($_SERVER['SERVER_PORT'] == '80') ? '' : $_SERVER['SERVER_PORT'];

define('DESARROLLO', $config['desarrollo']);
$server_protocol =  'https'; //strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false ? 'http' : 'https';
$base_url = $config['host'];
define('BASE_URL', $base_url);

//BASE DE DATOS
define("DB_SERVER", $config['bd_hostname']);
define("DB_USR", $config['bd_user']);
define("DB_PWD", $config['bd_password']);
define("DB_DATABASE", $config['bd_database']);
define("DB_PORT", (array_key_exists('bd_port', $config) ? $config['bd_port'] : ' 3306'));




defined('LUGAR_EXPEDICION') or define('LUGAR_EXPEDICION', 76046);
defined('DEFAULT_UNIDAD_ID') or define('DEFAULT_UNIDAD_ID', 'ACT');
defined('DEFAULT_PRODUCTO_SERVICIO_ID') or define('DEFAULT_PRODUCTO_SERVICIO_ID', '78181500');
defined('DEFAULT_TIPO_MONEDA') or define('DEFAULT_TIPO_MONEDA', 'MXN');
defined('DEFAULT_TIPO_COMPROBANTE') or define('DEFAULT_TIPO_COMPROBANTE', 'I');
defined('DEFAULT_LUGAR_EXPEDICION') or define('DEFAULT_LUGAR_EXPEDICION', '76046');

defined('URL_DMS') or define('URL_DMS', 'https://mylsa.iplaneacion.com/dms/queretaro_dms_api/public/');
defined('TITULO') or define('TITULO', 'CONTABILIDAD QUERETARO');

defined('PATH_POLIZAS_XML') or define('PATH_POLIZAS_XML', FCPATH . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . 'polizas_xml' . DIRECTORY_SEPARATOR);
defined('BASE_POLIZAS_XML') or define('BASE_POLIZAS_XML', BASE_URL . '/assets/documents/polizas_xml/');

defined('PATH_CHEQUE_COMPROBANTE') or define('PATH_CHEQUE_COMPROBANTE', FCPATH . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . 'cheque_comprobante' . DIRECTORY_SEPARATOR);
defined('BASE_CHEQUE_COMPROBANTEL') or define('BASE_CHEQUE_COMPROBANTEL', BASE_URL . '/assets/documents/cheque_comprobante/');

defined('PATH_CHEQUES') or define('PATH_CHEQUES', FCPATH . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . 'cheques' . DIRECTORY_SEPARATOR);
defined('BASE_CHEQUES') or define('BASE_CHEQUES', BASE_URL . '/assets/documents/cheques/');



defined('URL_CONTABILIDAD') or define('URL_CONTABILIDAD', 'https://mylsa.iplaneacion.com/dms/contabilidad_queretaro/');
defined('CONTABILIDAD_APIS') or define('CONTABILIDAD_APIS', 'https://mylsa.iplaneacion.com/dms/contabilidad_apis/');
