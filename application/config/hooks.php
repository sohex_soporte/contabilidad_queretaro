<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'] = array(
    'class'    => 'Session',
    'function' => 'check_auth',
    'filename' => 'Session.php',
    'filepath' => 'hooks',
    'params'   => array(
        'modulos_libres' => array('inicio','formatos','linea_autos','facturacion','document_apis','api_factura_ser_ref','facturacion_vehiculos','factura_documentos')
        )
);


$hook['display_override'][] = array(
    'class'    => 'Output',
    'function' => 'render_template',
    'filename' => 'output.php',
    'filepath' => 'hooks',
    'params'   => array()
);
