<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * carabiner config
 * 
 * The config file for carabiner
 * 
 * @license		http://www.opensource.org/licenses/bsd-license.php BSD licensed.
 * @author		Mike Funk, Tony Dewan
 * @link		http://mikefunk.com
 * @email		mike@mikefunk.com
 * 
 * @file		carabiner.php
 * @version		1.5.4
 * @date		03/05/2012
 */

/*
|--------------------------------------------------------------------------
| Script Directory
|--------------------------------------------------------------------------
|
| Path to the script directory.  Relative to the CI front controller.
|
*/

$config['script_dir'] = 'assets/scripts/';


/*
|--------------------------------------------------------------------------
| Style Directory
|--------------------------------------------------------------------------
|
| Path to the style directory.  Relative to the CI front controller
|
*/

$config['style_dir'] = 'assets/scripts/';

/*
|--------------------------------------------------------------------------
| Cache Directory
|--------------------------------------------------------------------------
|
| Path to the cache directory. Must be writable. Relative to the CI 
| front controller.
|
*/

$config['cache_dir'] = 'assets/cache/';




/*
* The following config values are not required.  See Libraries/Carabiner.php
* for more information.
*/



/*
|--------------------------------------------------------------------------
| Base URI
|--------------------------------------------------------------------------
|
|  Base uri of the site, like http://www.example.com/ Defaults to the CI 
|  config value for base_url.
|
*/

//$config['base_uri'] = 'http://www.example.com/';


/*
|--------------------------------------------------------------------------
| Development Flag
|--------------------------------------------------------------------------
|
|  Flags whether your in a development environment or not. Defaults to FALSE.
|
*/

// defaulting to true until I can figure out what to do about image urls.
// they are different depending on whether an item is cached or not.

$config['dev'] = FALSE;

/*
|--------------------------------------------------------------------------
| Combine
|--------------------------------------------------------------------------
|
| Flags whether files should be combined. Defaults to TRUE.
|
*/

$config['combine'] = FALSE;


/*
|--------------------------------------------------------------------------
| Minify Javascript
|--------------------------------------------------------------------------
|
| Global flag for whether JS should be minified. Defaults to TRUE.
|
*/

$config['minify_js'] = FALSE;


/*
|--------------------------------------------------------------------------
| Minify CSS
|--------------------------------------------------------------------------
|
| Global flag for whether CSS should be minified. Defaults to TRUE.
|
*/

$config['minify_css'] = FALSE;

/*
|--------------------------------------------------------------------------
| Force cURL
|--------------------------------------------------------------------------
|
| Global flag for whether to force the use of cURL instead of file_get_contents()
| Defaults to FALSE.
|
*/

$config['force_curl'] = FALSE;

/*
|--------------------------------------------------------------------------
| Predifined Asset Groups
|--------------------------------------------------------------------------
|
| Any groups defined here will automatically be included.  Of course, they
| won't be displayed unless you explicity display them ( like this: $this->carabiner->display('jquery') )
| See docs for more.
| 
| Currently created groups:
|	> jQuery (latest in 1.xx version)
|	> jQuery UI (latest in 1.xx version)
|	> Ext Core (latest in 3.xx version)
|	> Chrome Frame (latest in 1.xx version)
|	> Prototype (latest in 1.x.x.x version)
|	> script.aculo.us (latest in 1.x.x version)
|	> Mootools (1.xx version)
|	> Dojo (latest in 1.xx version)
|	> SWFObject (latest in 2.xx version)
|	> YUI (latest core JS/CSS in 2.x.x version)
|
*/

/* -----------------------------------------------------------------------------
 * BOWER JQUERY
 * ----------------------------------------------------------------------------- */
$config['groups']['jquery'] = array(    
    'js' => array(
        array('../components/jquery/dist/jquery.min.js'),
        array('../components/jquery-migrate/jquery-migrate.min.js')
    )
);

/* -----------------------------------------------------------------------------
 * BOWER bootstrap
 * ----------------------------------------------------------------------------- */
$config['groups']['bootstrap'] = array(    
    'js' => array(
        array('../components/bootstrap/dist/js/bootstrap.min.js'),
    ),
    'css' => array(
        array('../components/bootstrap/dist/css/bootstrap.min.css'),
    )
);

/* -----------------------------------------------------------------------------
 * BOWER UNDERSCORE
 * ----------------------------------------------------------------------------- */
$config['groups']['underscore'] = array(    
    'js' => array(
        array('../components/underscore/underscore-min.js')
    )
);

/* -----------------------------------------------------------------------------
 * BOWER MOMENTJS
 * ----------------------------------------------------------------------------- */
$config['groups']['moment'] = array(    
    'js' => array(
        array('../components/moment/moment.js')
    )
);

/* -----------------------------------------------------------------------------
 * BOWER CHARTS JS
 * ----------------------------------------------------------------------------- */
$config['groups']['chartjs'] = array(    
    'js' => array(
        array('../components/Chart-js/dist/Chart.min.js')
    ),
    'css' => array(
        array('../components/Chart-js/dist/Chart.min.css')
    )
);

/* -----------------------------------------------------------------------------
 * CDNJS BACKBONE
 * ----------------------------------------------------------------------------- */
$config['groups']['backbone'] = array(    
    'js' => array(
        array('../components/backbone/backbone-min.js')
    )
);

/* -----------------------------------------------------------------------------
 * BOWER TOASTR
 * ----------------------------------------------------------------------------- */
$config['groups']['toastr'] = array(    
    'js' => array(
        array('../components/toastr/toastr.min.js')
    ),
    'css' => array(
        array('../components/toastr/toastr.min.css')
    )
);

/* -----------------------------------------------------------------------------
 * CDNJS EASING
 * ----------------------------------------------------------------------------- */
$config['groups']['easing'] = array(    
    'js' => array(
        array('../cdnjs/jquery-easing/jquery.easing.min.js'),
//        array('../cdnjs/jquery-easing/jquery.easing.compatibility.min.js')
    )
);

/* -----------------------------------------------------------------------------
 * CDNJS DATATABLES
 * ----------------------------------------------------------------------------- */
$config['groups']['datatables'] = array(
    'js' => array(
        array('../components/DataTables/datatables.min.js'),
        // array('../components/DataTables/DataTables-1.11.3/js/dataTables.bootstrap4.min.js')
    ),
    'css' => array(
        // array('../components/DataTables/media/css/jquery.dataTables.min.css'),
        array('../components/DataTables/datatables.min.css')
     )
);

/* -----------------------------------------------------------------------------
 * CDNJS SELECT2
 * ----------------------------------------------------------------------------- */
$config['groups']['select2'] = array(
    'js' => array(
        array('../components/select2/dist/js/select2.full.min.js'),
        // array('../components/DataTables/DataTables-1.11.3/js/dataTables.bootstrap4.min.js')
    ),
    'css' => array(
        array('../components/select2/dist/css/select2.min.css'),
        array('../components/select2/dist/css/select2-bootstrap4.min.css')
     )
);

/* -----------------------------------------------------------------------------
 * CDNJS SWALL
 * ----------------------------------------------------------------------------- */
$config['groups']['sweetalert2'] = array(
    'js' => array(
        array('../components/sweetalert2/dist/sweetalert2.all.min.js'),
        // array('../components/DataTables/DataTables-1.11.3/js/dataTables.bootstrap4.min.js')
    ),
    'css' => array(
        array('../components/sweetalert2/dist/sweetalert2.min.css')
     )
);

/* -----------------------------------------------------------------------------
 * font-awesome
 * ----------------------------------------------------------------------------- */
$config['groups']['fontawesome'] = array(
    'css' => array(
        array('../components/font-awesome/css/all.min.css'),
    ),
    'js' => array(
        array('../components/font-awesome/js/all.min.js'),
    )
);


/* -----------------------------------------------------------------------------
 * sparkline dist BOWER COMPONENTS
 * ----------------------------------------------------------------------------- */
$config['groups']['sparkline'] = array(
    'js' => array(
        array('../librerias/jquery.sparkline.dist/dist/jquery.sparkline.js','../librerias/jquery.sparkline.dist/dist/jquery.sparkline.min.js',FALSE,FALSE),
    )
);

/* -----------------------------------------------------------------------------
 * UTILS LIBRERIA
 * ----------------------------------------------------------------------------- */
$config['groups']['utils'] = array(
  'js' => array(
      array('../librerias/utils/utils.min.js','../librerias/utils/utils.min.js',FALSE,FALSE),
    ),
    'css' => array(
        array('../librerias/utils/css_general.css','screen','../librerias/utils/css_general.min.css',FALSE,FALSE),
//        array('../librerias/utils/template.css','screen','../librerias/utils/template.min.css',FALSE,FALSE),
        array('../librerias/utils/form.css','screen','../librerias/utils/form.min.css',FALSE,FALSE),
        array('../librerias/utils/utils.css','screen','../librerias/utils/utils.min.css',FALSE,FALSE)
     )
);

$config['groups']['material'] = array(
    'js' => array(
        # Core JS Files
        array('../template/material/js/core/popper.min.js'),
        array('../template/material/js/core/bootstrap-material-design.min.js'),
        array('../template/material/js/plugins/perfect-scrollbar.jquery.min.js'),

        # Control Center for Material Dashboard: parallax effects, scripts for the example pages etc
        array('../template/material/js/material-dashboard.js?v=2.1.1'),

        # Material Dashboard DEMO methods, don't include it in your project!
        array('../template/material/demo/demo.js')
    ),
    'css' => array(
        # Fonts and icons
        array('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons'),
        array('https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css'),
        # CSS Files
        array('../template/material/css/material-dashboard.css?v=2.1.1'),
        # CSS Just for demo purpose, don't include it in your project
        array('../template/material/demo/demo.css')
    )
);

$config['groups']['metronic5'] = array(
    'js' => array(
        # CORE PLUGINS
        array('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'),
        array('../template/metronic5/css/plugins/global/plugins.bundle.js'),
        array('../template/metronic5/css/js/scripts.bundle.js'),
    ),
    'css' => array(
        # GLOBAL MANDATORY STYLES
        array('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700'),
        array('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'),
        array('../template/metronic5/plugins/global/plugins.bundle.css'),
        array('../template/metronic5/css/style.bundle.custom.css'),
    )
);

$config['groups']['template'] = array(
    'js' => array(
        # CORE PLUGINS
        // array('../template/global/plugins/bootstrap/js/bootstrap.min.js'),
        array('../template/global/plugins/js.cookie.min.js'),
        array('../template/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'),
        array('../template/global/plugins/jquery.blockui.min.js'),
        array('../template/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'),
        # THEME GLOBAL SCRIPTS
        array('../template/global/scripts/app.min.js'),
        # THEME LAYOUT SCRIPTS
        array('../template/layouts/layout4/scripts/layout.min.js'),
        array('../template/layouts/layout4/scripts/demo.min.js'),
        array('../template/layouts/global/scripts/quick-sidebar.min.js'),
        array('../template/layouts/global/scripts/quick-nav.min.js')
    ),
    'css' => array(
        # GLOBAL MANDATORY STYLES
        array('http://fonts.googleapis.com/css?family=Open+Sans|Roboto:400,300,600,700&subset=all'),
        array('../template/global/plugins/simple-line-icons/simple-line-icons.min.css'),
        // array('../template/global/plugins/bootstrap/css/bootstrap.min.css'),
        // array('../template/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'),
        # THEME GLOBAL STYLES
        array('../template/global/css/components-md.css'),
        array('../template/global/css/plugins-md.min.css'),
        array('../template/pages/css/search.min.css'),
        # THEME LAYOUT STYLES
        array('../template/layouts/layout4/css/layout.css'),
        array('../template/layouts/layout4/css/themes/default.min.css'),
        array('../template/layouts/layout4/css/custom.min.css')
    )
);



$config['groups']['snippet'] = array(
    'css' => array('../librerias/snippet/jquery.snippet.css'),
    'js' => array( '../librerias/snippet/jquery.snippet.js')
);

/* End of file carabiner.php */
/* Location: ./carabiner/config/carabiner.php */