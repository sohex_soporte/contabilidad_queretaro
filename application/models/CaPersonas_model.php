<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaPersonas_model extends CI_Model {

    public $rows_names;

    function __construct()
    {
        parent::__construct();
       
    }

    public function get($where = false){
        $this->db            
            ->select([
                'ca_personas.id as persona_id', 
                'ca_personas.nombre', 
                'ca_personas.apellido1', 
                'ca_personas.apellido2', 
                'ca_personas.rfc', 
                'ca_personas.telefono', 
                'ca_personas.domicilio', 
                'ca_personas.correo_electronico', 
                'ca_personas.tipo_persona_id',
                'ca_personas.created_at', 
                'ca_personas.updated_at',
                'ca_personas.codigo_postal',
                'ca_personas.contrasena',
                'ca_personas.envio_correo',

                'ca_personas.id_estado',
                'ca_personas.id_municipio',
                'ca_personas.id_colonia',
                'ca_personas.calle',
                'ca_personas.numero'

            ])
            ->from('ca_personas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select([
                'ca_personas.id as persona_id', 
                'ca_personas.nombre', 
                'ca_personas.apellido1', 
                'ca_personas.apellido2', 
                'ca_personas.rfc', 
                'ca_personas.telefono', 
                'ca_personas.domicilio', 
                'ca_personas.correo_electronico', 
                'ca_personas.tipo_persona_id',
                'ca_tipo_persona.nombre as tipo_persona',
                'ca_personas.created_at', 
                'ca_personas.updated_at',

                'ca_personas.estado',
                'ca_personas.municipio',
                'ca_personas.colonia',
                'ca_personas.calle',
                'ca_personas.numero',
                'ca_personas.codigo_postal'
            ])
            ->from('ca_personas')
            ->join('ca_tipo_persona','ca_personas.tipo_persona_id = ca_tipo_persona.id');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_personas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $uuid = utils::guid();
        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('ca_personas', $contents);
        return $uuid;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('ca_personas', $contents);
    }

}