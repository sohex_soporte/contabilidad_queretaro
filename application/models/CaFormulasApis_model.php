<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaFormulasApis_model extends CI_Model {

    public function get($where = false)
    {
        $this->db->from('ca_formulas_apis');
        if(is_array($where)){
            $this->db->where($where);
        }
        // $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false)
    {
        $this->db->from('ca_formulas_apis');
        if(is_array($where)){
            $this->db->where($where);
        }
        // $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}