<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaProcedencias_model extends CI_Model {

    public function get($where = false){
        $this->db
            ->from('ca_procedencias');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('ca_procedencias');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}