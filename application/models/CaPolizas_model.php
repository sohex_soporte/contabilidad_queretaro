<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaPolizas_model extends CI_Model {

    public $rows_names;

    function __construct()
    {
        parent::__construct();
        $this->rows_names = array(
            'ca_polizas.id',
            'ca_polizas.PolizaNomenclatura_id',
            'ca_polizas.ejercicio',
            'ca_polizas.mes',
            'ca_polizas.dia',
            'ca_polizas.fecha_creacion'
        );
    }

    public function get($where = false){
        $this->db
            ->select($this->rows_names)
            ->from('ca_polizas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getDetalle($where = false){
        $this->db
            ->select([
                'ca_polizas.*',
                'ca_polizas_nomenclatura.descripcion as nomenclatura'
            ])
            ->from('ca_polizas')
            ->join('ca_polizas_nomenclatura','ca_polizas.PolizaNomenclatura_id = ca_polizas_nomenclatura.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select($this->rows_names)
            ->from('ca_polizas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_polizas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }


    public function getList_ext($where = false){
        $this->db
            ->distinct()
            ->select([
                'ca_polizas.*',
                'ca_polizas_nomenclatura.descripcion as nomenclatura',
                'ca_polizas_fijas.poliza as poliza_fija',
            ])
            ->from('ca_polizas')
            ->join('ca_polizas_nomenclatura','ca_polizas.PolizaNomenclatura_id = ca_polizas_nomenclatura.id')
            ->join('asientos','ca_polizas.id = asientos.polizas_id')
            ->join('ca_transacciones','asientos.transaccion_id = ca_transacciones.id')
            ->join('ca_polizas_fijas','ca_transacciones.poliza_fija_id = ca_polizas_fijas.id')
            ->where('ca_transacciones.estatus_id','CERRADO');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_polizas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }


    public function getList($where = false){
        $this->db
            ->select([
                'ca_polizas.*',
                'ca_polizas_nomenclatura.descripcion as nomenclatura'
            ])
            ->from('ca_polizas')
            ->join('ca_polizas_nomenclatura','ca_polizas.PolizaNomenclatura_id = ca_polizas_nomenclatura.id');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_polizas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents) {
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('ca_polizas', $contents);
        return ($response)? $this->db->insert_id() : false;
    }

}