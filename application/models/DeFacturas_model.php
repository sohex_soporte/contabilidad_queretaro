<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeFacturas_model extends CI_Model {

  
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('de_facturas');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_facturas')
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_facturas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents) {
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_facturas', $contents);
        return ($response)? $this->db->insert_id() : false;
    }

    public function detalle($where = false){
        $this->db->select([
                'de_facturas.id as factura_id', 
                'de_facturas.transaccion_id', 
                'ca_transacciones.folio', 
                'ca_transacciones.fecha as transaccion_fecha', 
                'de_facturas.forma_pago_id', 
                'ca_formas_pago.nombre as forma_pago', 
                'de_facturas.metodo_pago_id', 
                'ca_metodos_pago.nombre as metodo_pago', 
                'de_facturas.total', 

                'de_facturas.sub_total', 
                'de_facturas.error_mensaje', 
                'de_facturas.factura', 
                'de_facturas.pdf', 
                'de_facturas.xml', 

                'de_facturas.receptor_rfc', 
                'de_facturas.receptor_nombre', 
                'de_facturas.receptor_uso_cfdi', 
                'ca_uso_cfdi.nombre as uso_cfdi', 
                'de_facturas.cfdi',
                'de_facturas.created_at'
            ])
            ->from('de_facturas')
            ->join('ca_transacciones','de_facturas.transaccion_id = ca_transacciones.id','left')
            ->join('ca_formas_pago','de_facturas.forma_pago_id = ca_formas_pago.id','left')
            ->join('ca_metodos_pago','de_facturas.metodo_pago_id = ca_metodos_pago.id','left')
            ->join('ca_uso_cfdi','de_facturas.receptor_uso_cfdi = ca_uso_cfdi.id','left');

        if(is_array($where)){
            $this->db->where($where);
        }

        $this->db->where('de_facturas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function detalle_list($where = false){
        $this->db->select([
                'de_facturas.id as factura_id', 
                'de_facturas.transaccion_id', 
                'ca_transacciones.folio', 
                'ca_transacciones.fecha as transaccion_fecha', 
                'de_facturas.forma_pago_id', 
                'ca_formas_pago.nombre as forma_pago', 
                'de_facturas.metodo_pago_id', 
                'ca_metodos_pago.nombre as metodo_pago', 
                'de_facturas.total', 
                'de_facturas.receptor_rfc', 
                'de_facturas.receptor_nombre', 
                'de_facturas.receptor_uso_cfdi', 
                'ca_uso_cfdi.nombre as uso_cfdi', 
                'de_facturas.created_at'
            ])
            ->from('de_facturas')
            ->join('ca_transacciones','de_facturas.transaccion_id = ca_transacciones.id','left')
            ->join('ca_formas_pago','de_facturas.forma_pago_id = ca_formas_pago.id','left')
            ->join('ca_metodos_pago','de_facturas.metodo_pago_id = ca_metodos_pago.id','left')
            ->join('ca_uso_cfdi','de_facturas.receptor_uso_cfdi = ca_uso_cfdi.id','left');

        if(is_array($where)){
            $this->db->where($where);
        }

        $this->db->where('de_facturas.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}