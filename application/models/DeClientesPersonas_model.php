<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeClientesPersonas_model extends CI_Model {

    function __construct()
    {
        parent::__construct();       
    }

    public function get($where = false){
        $this->db            
            ->from('de_clientes_personas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_clientes_personas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($data_form){
        $this->db->set('created_at',utils::get_datetime());
        $this->db->set('updated_at',utils::get_datetime());
        $this->db->insert('de_clientes_personas',$data_form);
        return ($this->db->affected_rows() > 0)? $this->db->insert_id() : false;
    }

}