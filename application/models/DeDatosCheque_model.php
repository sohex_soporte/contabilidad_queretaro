<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeDatosCheque_model extends CI_Model {

  
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('de_datos_cheque');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function get_detalle($where = false){
        $this->db
            ->select([
                'de_datos_cheque.id as datos_cheque_id', 
                'de_datos_cheque.referencia',
                'de_datos_cheque.comentarios',
                'ca_personas.cliente_id', 
                'ca_personas.nombre', 
                'ca_personas.apellido1', 
                'ca_personas.apellido2', 
                'ca_personas.rfc', 
                'ca_personas.telefono', 
                'ca_personas.domicilio', 
                'ca_personas.correo_electronico', 
                
                'ca_transacciones.id as transaccion_id', 
                'ca_transacciones.cliente_id', 
                'ca_transacciones.folio', 
                'ca_transacciones.origen', 
                'ca_transacciones.fecha', 
                'ca_polizas.id as polizas_id', 
                'ca_polizas.PolizaNomenclatura_id', 
                'ca_polizas.ejercicio', 
                'ca_polizas.mes', 
                'ca_polizas.dia',
                'ca_polizas.fecha_creacion as poliza_fecha_creacion',
                'de_datos_cheque.archivo_comprobante',
                'de_datos_cheque.id_datos_cheque_estatus',
                'de_datos_cheque.firma_autorizacion',
                'de_datos_cheque.tipo_movimeinto'

            ])
            ->from('de_datos_cheque')
            ->join('ca_transacciones','de_datos_cheque.transaccion_id = ca_transacciones.id')
            ->join('ca_personas','ca_transacciones.persona_id = ca_personas.id')
            ->join('re_transacciones_polizas','ca_transacciones.id = re_transacciones_polizas.transaccion_id')
            ->join('ca_polizas','re_transacciones_polizas.poliza_id = ca_polizas.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_datos_cheque')
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_datos_cheque.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get_detalleAll($where = false){
        $this->db
            ->distinct()
            ->select([
                'de_datos_cheque.id as datos_cheque_id', 
                'ca_personas.cliente_id', 
                'ca_personas.nombre', 
                'ca_personas.apellido1', 
                'ca_personas.apellido2', 
                'ca_personas.rfc', 
                'ca_personas.telefono', 
                'ca_personas.domicilio', 
                'ca_personas.correo_electronico', 
                'ca_transacciones.id as transaccion_id', 
                'ca_transacciones.cliente_id', 
                'ca_transacciones.folio', 
                'ca_transacciones.origen', 
                'ca_transacciones.fecha', 
                'ca_polizas.id as polizas_id', 
                'ca_polizas.PolizaNomenclatura_id', 
                'ca_polizas.ejercicio', 
                'ca_polizas.mes', 
                'ca_polizas.dia',
                'ca_polizas.fecha_creacion as poliza_fecha_creacion',
                'de_datos_cheque.created_at as datos_cheque_fecha_registro',
                'de_datos_cheque.id_datos_cheque_estatus',
                'de_datos_cheque.tipo_movimeinto',
                '(SELECT sum(abono) FROM asientos where transaccion_id = ca_transacciones.id and estatus_id in ("POR_APLICAR","APLICADO") ) as abonos',
                '(SELECT sum(cargo) FROM asientos where transaccion_id = ca_transacciones.id and estatus_id in ("POR_APLICAR","APLICADO") ) as cargos'
            ])
            ->from('de_datos_cheque')
            ->join('ca_transacciones','de_datos_cheque.transaccion_id = ca_transacciones.id')
            ->join('ca_personas','ca_transacciones.persona_id = ca_personas.id')
            ->join('re_transacciones_polizas','ca_transacciones.id = re_transacciones_polizas.transaccion_id')
            ->join('ca_polizas','re_transacciones_polizas.poliza_id = ca_polizas.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents,$id = false) {
        $uuid = ($id == false)? utils::guid() : $id;
        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_datos_cheque', $contents);
        return ($response)? $uuid : false;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('de_datos_cheque', $contents);
    }

}