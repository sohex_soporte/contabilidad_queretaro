<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }


    public function complementoPagoPagado($uuid){
      $this->db->select('complemento_datos.total_pago');
      $this->db->from('complemento_cfdi_relacionado_p');
      $this->db->join('complemento_datos', 'complemento_cfdi_relacionado_p.id_complemento = complemento_datos.id_complemento');
      $this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
      $resultado = $this->db->get()->result();
      
      $subtotal = 0;
      foreach($resultado as $fil):
        $subtotal+= $fil->total_pago;
      endforeach;
      return $subtotal;
    }

    public function complemento_numero_pagos($uuid){
      $this->db->select('complemento_datos.total_pago');
      $this->db->from('complemento_cfdi_relacionado_p');
      $this->db->join('complemento_datos', 'complemento_cfdi_relacionado_p.id_complemento = complemento_datos.id_complemento');
      $this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
      $resultado = $this->db->get()->result();
      
      $subtotal = 0;
      foreach($resultado as $fil):
        $subtotal+= 1;
      endforeach;
      return $subtotal;
    }


    public function get_facturas_ppd_vehiculo($rfc){

      if($rfc == null){
        //$this->db->where('receptor_RFC',$rfc);
      $this->db->where('factura_medotoPago','PPD');
      $res = $this->db->get('factura_vehiculo')->result();
      return $res ;

      }else{
        $this->db->where('receptor_RFC',$rfc);
      $this->db->where('factura_medotoPago','PPD');
      $res = $this->db->get('factura_vehiculo')->result();
      return $res ;

      }

      
   }

    
    public function get_facturas_ppd($rfc){

      if($rfc == null){
        //$this->db->where('receptor_RFC',$rfc);
      $this->db->where('factura_medotoPago','PPD');
      $res = $this->db->get('factura')->result();
      return $res ;

      }else{
        $this->db->where('receptor_RFC',$rfc);
      $this->db->where('factura_medotoPago','PPD');
      $res = $this->db->get('factura')->result();
      return $res ;

      }

      
   }
    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}

      public function update_table_row($tabla,$data,$id_table,$id){
    		$this->db->update($tabla, $data, array($id_table=>$id));
       
    	}


      function check_fecha($x) {
        if (date('Y-m-d', strtotime($x)) == $x) {
          return true;
        } else {
          return false;
        }
    }

     
    function truncateFloat($numero, $digitos)
    {
      $truncar = 10**$digitos;
      return intval($numero * $truncar) / $truncar;

    }
    
    public function factura_subtotal($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal;
    }

    public function factura_subtotal2($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal;
    }

    public function factura_subtotal2_seminuevo($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->precio_venta;
      endforeach;
      return $subtotal;
    }

    public function factura_subtotal2_seminuevo_subtotal($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_precio + $fil->base_iva;
      endforeach;
      return $subtotal;
    }

    public function factura_iva_total($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_iva_total2($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_iva_total2_seminuevo($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->base_iva;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_descuento($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $descuento = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_descuento2($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos_vehiculo');
      $descuento = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_iva(){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $iva = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }



    function getEmployees($postData=null){

      $response = array();
 
      ## Read value
      $draw = $postData['draw'];
      $start = $postData['start'];
      $rowperpage = $postData['length']; // Rows display per page
      $columnIndex = $postData['order'][0]['column']; // Column index
      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
      $searchValue = $postData['search']['value']; // Search value
 
      ## Search 
      $searchQuery = "";
      if($searchValue != ''){
         $searchQuery = " (cuenta like '%".$searchValue."%' or decripcion like '%".$searchValue."%' ) ";
      }
 
      ## Total number of records without filtering
      $this->db->select('count(*) as allcount');
      $records = $this->db->get('cuentas_dms')->result();
      $totalRecords = $records[0]->allcount;
 
      ## Total number of record with filtering
      $this->db->select('count(*) as allcount');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $records = $this->db->get('cuentas_dms')->result();
      $totalRecordwithFilter = $records[0]->allcount;
 
      ## Fetch records
      $this->db->select('*');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $this->db->order_by($columnName, $columnSortOrder);
      $this->db->limit($rowperpage, $start);
      $records = $this->db->get('cuentas_dms')->result();
 
      $data = array();
 
      foreach($records as $record ){
 
         $data[] = array( 
            "id"=>$record->id,
            "cuenta"=>$record->cuenta,
            "decripcion"=>$record->decripcion,
            "opciones"=>$record->id.','.$record->cuenta,
            
         ); 
      }
 
      ## Response
      $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordwithFilter,
         "aaData" => $data
      );
 
      return $response; 
    }

    function busqueda_clave_serv_prod($postData=null){

      $response = array();
 
      ## Read value
      $draw = $postData['draw'];
      $start = $postData['start'];
      $rowperpage = $postData['length']; // Rows display per page
      $columnIndex = $postData['order'][0]['column']; // Column index
      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
      $searchValue = $postData['search']['value']; // Search value
 
      ## Search 
      $searchQuery = "";
      if($searchValue != ''){
         $searchQuery = " (id like '%".$searchValue."%' or ClaveProdServ like '%".$searchValue."%' or Des like'%".$searchValue."%' ) ";
      }
 
      ## Total number of records without filtering
      $this->db->select('count(*) as allcount');
      $records = $this->db->get('ca_clave_prodserv')->result();
      $totalRecords = $records[0]->allcount;
 
      ## Total number of record with filtering
      $this->db->select('count(*) as allcount');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $records = $this->db->get('ca_clave_prodserv')->result();
      $totalRecordwithFilter = $records[0]->allcount;
 
      ## Fetch records
      $this->db->select('*');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $this->db->order_by($columnName, $columnSortOrder);
      $this->db->limit($rowperpage, $start);
      $records = $this->db->get('ca_clave_prodserv')->result();
 
      $data = array();
 
      foreach($records as $record ){
        $data_opciones['ClaveProdServ'] = $record->ClaveProdServ;
         $data[] = array( 
            "id"=>$record->id,
            "ClaveProdServ"=>$record->ClaveProdServ,
            "Des"=>$record->Des,
            "opciones"=>$data_opciones,

            
         ); 
      }
 
      ## Response
      $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordwithFilter,
         "aaData" => $data
      );
 
      return $response; 
    }


    function busqueda_clave_unidad($postData=null){

      $response = array();
 
      ## Read value
      $draw = $postData['draw'];
      $start = $postData['start'];
      $rowperpage = $postData['length']; // Rows display per page
      $columnIndex = $postData['order'][0]['column']; // Column index
      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
      $searchValue = $postData['search']['value']; // Search value
 
      ## Search 
      $searchQuery = "";
      if($searchValue != ''){
         $searchQuery = " (id like '%".$searchValue."%' or ClaveUnidad like '%".$searchValue."%' or Nombre like'%".$searchValue."%' ) ";
      }
 
      ## Total number of records without filtering
      $this->db->select('count(*) as allcount');
      $records = $this->db->get('ca_claveunidad')->result();
      $totalRecords = $records[0]->allcount;
 
      ## Total number of record with filtering
      $this->db->select('count(*) as allcount');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $records = $this->db->get('ca_claveunidad')->result();
      $totalRecordwithFilter = $records[0]->allcount;
 
      ## Fetch records
      $this->db->select('*');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $this->db->order_by($columnName, $columnSortOrder);
      $this->db->limit($rowperpage, $start);
      $records = $this->db->get('ca_claveunidad')->result();
 
      $data = array();
 
      foreach($records as $record ){
        $data_opciones['ClaveUnidad'] = $record->ClaveUnidad;
         $data[] = array( 
            "id"=>$record->id,
            "ClaveUnidad"=>$record->ClaveUnidad,
            "Nombre"=>$record->Nombre,
            "opciones"=>$data_opciones,

            
         ); 
      }
 
      ## Response
      $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordwithFilter,
         "aaData" => $data
      );
 
      return $response; 
    }


    function busqueda_asientos($postData=null){

      $response = array();
 
      ## Read value
      $draw = $postData['draw'];
      $start = $postData['start'];
      $rowperpage = $postData['length']; // Rows display per page
      $columnIndex = $postData['order'][0]['column']; // Column index
      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
      $searchValue = $postData['search']['value']; // Search value
 
      ## Search 
      $searchQuery = "";
      if($searchValue != ''){
         $searchQuery = " (id like '%".$searchValue."%' or cuenta like '%".$searchValue."%' or decripcion like'%".$searchValue."%' ) ";
      }
 
      ## Total number of records without filtering
      $this->db->select('count(*) as allcount');
      $records = $this->db->get('cuentas_dms')->result();
      $totalRecords = $records[0]->allcount;
 
      ## Total number of record with filtering
      $this->db->select('count(*) as allcount');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $records = $this->db->get('cuentas_dms')->result();
      $totalRecordwithFilter = $records[0]->allcount;
 
      ## Fetch records
      $this->db->select('*');
      if($searchQuery != '')
         $this->db->where($searchQuery);
      $this->db->order_by($columnName, $columnSortOrder);
      $this->db->limit($rowperpage, $start);
      $records = $this->db->get('cuentas_dms')->result();
 
      $data = array();
 
      foreach($records as $record ){
        $data_opciones['cuenta'] = $record->cuenta;
        $data_opciones['id'] = $record->id;
        $data_opciones['descripcion'] = $record->decripcion;

         $data[] = array( 
            "id"=>$record->id,
            "cuenta"=>$record->cuenta,
            "descripcion"=>$record->decripcion,
            "opciones"=>$data_opciones,

            
         ); 
      }
 
      ## Response
      $response = array(
         "draw" => intval($draw),
         "iTotalRecords" => $totalRecords,
         "iTotalDisplayRecords" => $totalRecordwithFilter,
         "aaData" => $data
      );
 
      return $response; 
    }

      
}
