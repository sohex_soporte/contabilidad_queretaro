<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class CaPolizasNomenclaturas_mdl extends Eloquent {

    use SoftDeletes;

    protected $table = 'ca_PolizasNomenclatura';
    public $timestamps = false;
    protected $fillable = ['id','descripcion'];
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}