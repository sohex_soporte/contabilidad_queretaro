<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeFormatoFlujoEfectivo extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('de_formato_flujo_efectivo');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->distinct()
            ->select([
                'de_formato_flujo_efectivo.*',
                'ca_personas.tipo_persona_id', 'ca_personas.nombre', 'ca_personas.apellido1','ca_personas.apellido2',
                'ca_formato_flujo_efectivo.fecha as formato_flujo_efectivo_fecha',
                'ca_formato_flujo_efectivo.banco_id as formato_flujo_efectivo_banco_id',
                'ca_sucursales.nombre as sucursal',
                'ca_procedencias.nombre as procedencia'
            ])
            ->from('de_formato_flujo_efectivo')
            ->join('ca_formato_flujo_efectivo','de_formato_flujo_efectivo.formato_flujo_efectivo_id = ca_formato_flujo_efectivo.id')
            ->join('ca_personas','de_formato_flujo_efectivo.persona_id = ca_personas.id' , 'left')
            ->join('ca_procedencias','de_formato_flujo_efectivo.procedencia_id = ca_procedencias.id' , 'left')
            ->join('ca_sucursales','de_formato_flujo_efectivo.sucursal_id = ca_sucursales.id' , 'left')
            ->order_by('ca_formato_flujo_efectivo.banco_id','asc')
            ->order_by('de_formato_flujo_efectivo.orden','asc');
            
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getFirst($where = false){
        $this->db
            ->distinct()
            ->select([
                'de_formato_flujo_efectivo.*',
                'ca_formato_flujo_efectivo.fecha as formato_flujo_efectivo_fecha',
                'ca_formato_flujo_efectivo.banco_id as formato_flujo_efectivo_banco_id'
            ])
            ->from('de_formato_flujo_efectivo')
            ->join('ca_formato_flujo_efectivo','de_formato_flujo_efectivo.formato_flujo_efectivo_id = ca_formato_flujo_efectivo.id')
            ->order_by('ca_formato_flujo_efectivo.banco_id','asc')
            ->order_by('de_formato_flujo_efectivo.orden','asc')
            ->limit(1);
            
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }


    public function getLast($where = false){
        $this->db
            ->distinct()
            ->select([
                'de_formato_flujo_efectivo.*',
                'ca_formato_flujo_efectivo.fecha as formato_flujo_efectivo_fecha',
                'ca_formato_flujo_efectivo.banco_id as formato_flujo_efectivo_banco_id'
            ])
            ->from('de_formato_flujo_efectivo')
            ->join('ca_formato_flujo_efectivo','de_formato_flujo_efectivo.formato_flujo_efectivo_id = ca_formato_flujo_efectivo.id')
            ->where('de_formato_flujo_efectivo.saldo >',0,false)
            ->order_by('ca_formato_flujo_efectivo.banco_id','desc')
            ->order_by('de_formato_flujo_efectivo.orden','desc')
            ->limit(1);
            
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function insert($contents) {
        $uuid = utils::guid();

        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_formato_flujo_efectivo', $contents);
        return ($response)? $uuid : false;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('de_formato_flujo_efectivo', $contents);
    }

    public function getSum($where = false){
        $this->db
            ->select_sum('cargo')
            ->select_sum('abono')
            ->from('de_formato_flujo_efectivo')
            ->join('ca_formato_flujo_efectivo','de_formato_flujo_efectivo.formato_flujo_efectivo_id = ca_formato_flujo_efectivo.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }


    public function getMax($where = false){
        $this->db
            ->select_max('saldo')
            ->from('de_formato_flujo_efectivo')
            ->join('ca_formato_flujo_efectivo','de_formato_flujo_efectivo.formato_flujo_efectivo_id = ca_formato_flujo_efectivo.id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

}