<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaPolizasFijas_model extends CI_Model {

    public function get($where = false)
    {
        $this->db->from('ca_polizas_fijas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false)
    {
        $this->db->from('ca_polizas_fijas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $this->db->insert('ca_polizas_fijas', $contents);
        $this->db->insert_id();
    }
}