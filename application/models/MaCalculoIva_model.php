<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MaCalculoIva_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($where = false)
    {
        $this->db
            ->from('ma_calculo_iva');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false)
    {
        $this->db
            ->from('ma_calculo_iva');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($data_form)
    {
        $uuid = utils::guid();
        $this->db->set('id', $uuid);
        $this->db->set('created_at', utils::get_datetime());
        $this->db->set('updated_at', utils::get_datetime());
        $this->db->insert('ma_calculo_iva', $data_form);
        return ($this->db->affected_rows() > 0) ? $uuid : false;
    }

    public function calculo_iva($importe = 0)
    {
        $importe_subtotal = 0;
        $importe_iva = 0;
        $importe_total = 0;

        $importe = (float)($importe);

        if ($importe > 0) {
            $moneda_abono_sin_iva = 0;
            $moneda_abono_iva = 0;
            $i = 100000000;
            $i_fin = $importe - ($importe * 0.10);

            $this->db->limit(1);
            $this->db->order_by('margen_inicial', 'desc');
            $this->db->order_by('updated_at', 'desc');
            $this->db->where('cantidad_total <=',$importe);
            $this->db->where('cantidad_calculada <',$i_fin);
            $data_temp = $this->get();
            
            $i = $importe - $importe*17.5/100;
            $i_alt = $importe - ($importe*13/100);
            if($data_temp != false) {
                $margen_calculado = (float)$data_temp['margen_inicial'];
                if($i < $margen_calculado && $margen_calculado < $i_alt) {
                    $i = $margen_calculado;
                }

                if(utils::redondeo($importe) == utils::redondeo($data_temp['cantidad_total']) && $data_temp['completo'] == 1){
                    $moneda_abono_sin_iva = $data_temp['cantidad_calculada'];
                }

            }                

            $error_final = 500000;
            $margen_inicial = 0;
            if($moneda_abono_sin_iva == 0){
                
                $error = 0;
                $porcentaje = 0;
                while ($i < $i_fin):
                    $porcentaje = (float) ($i * 0.16) + $i;
                    $margen_inicial = $i;

                    if ($porcentaje >= $importe) {
                        $moneda_abono_iva = $i * 0.16;
                        $moneda_abono_sin_iva = $importe - $moneda_abono_iva;
                        // $i = $i_fin;
                        $error_final = 0;    
                    }

                    
                    $error = $error + 1;

                    if ($error >= $error_final) {
                        $i = $i_fin;
                    }

                    $i = $i + 0.1;

                endwhile;

                $this->insert([
                    'margen_final' => $i_fin,
                    'iteraciones' => $error,
                    'cantidad_calculada' => $moneda_abono_sin_iva,
                    'cantidad_total' => $importe,
                    'margen_inicial' => $margen_inicial,
                    'completo' => ($moneda_abono_sin_iva == 0)? 0 : 1
                ]);
            }

            $importe_total = $importe;
            $importe_subtotal = $moneda_abono_sin_iva;
            $importe_iva = $importe - $moneda_abono_sin_iva;
            

        }

        return [
            'importe_subtotal' => utils::redondeo($importe_subtotal),
            'importe_iva' => utils::redondeo($importe_iva),
            'importe_total' => $importe_total,
        ];
    }

}
