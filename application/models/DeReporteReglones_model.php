<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeReporteReglones_model extends CI_Model
{

    public function insert($contents)
    {
        $this->db->set('created_at', utils::now());
        $response = $this->db->insert('de_reporte_reglones', $contents);
        return $response;
    }

    public function update($contents, $where)
    {
        $this->db->where($where);
        $this->db->set('updated_at', utils::now());
        return $this->db->update('de_reporte_reglones', $contents);
    }

    public function delete($where = false)
    {
        $response = 0;
        if (is_array($where) && count($where) > 0) {
            $this->db->where($where);
            $response = $this->db->delete('de_reporte_reglones');
        }
        return $response;
    }

    public function get($where = false)
    {
        $this->db
            ->from('de_reporte_reglones');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false)
    {
        $this->db
            ->from('de_reporte_reglones');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get_list($where = false)
    {
        $this->db
            ->from('de_reporte_reglones')
            ->order_by('de_reporte_reglones.orden', 'asc');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $this->db->where('de_reporte_reglones.deleted_at IS NULL', null, false);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }


    public function get_max_orden($where = false)
    {
        $this->db->select('orden');
        $this->db
            ->from('de_reporte_reglones');
        if (is_array($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('orden', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->row_array() : false;
    }
}
