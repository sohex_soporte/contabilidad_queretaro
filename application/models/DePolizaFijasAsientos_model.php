<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DePolizaFijasAsientos_model extends CI_Model {

    public function get($where = false)
    {
        $this->db->from('de_poliza_fijas_asientos');
        if(is_array($where)){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function insert($contents)
    {
        // $id = utils::guid();
        // $this->db->set('id',$id);
        $this->db->set('created_at',utils::now());
        // $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_poliza_fijas_asientos', $contents);
        return ($response != false)? $this->db->insert_id() : false;
    }

    public function delete($where){
        $this->db->where($where);
        return $this->db->delete('de_poliza_fijas_asientos');
    }
}