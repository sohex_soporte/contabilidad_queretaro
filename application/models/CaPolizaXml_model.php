<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaPolizaXml_model extends CI_Model {

    public $rows_names;

    function __construct()
    {
        parent::__construct();
       
    }

    public function get($where = false){
        $this->db            
            ->from('ca_poliza_xml');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('ca_poliza_xml');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_poliza_xml.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents = array())
    {
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('ca_poliza_xml', $contents);
        return $this->db->insert_id();
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('ca_poliza_xml', $contents);
    }

}