<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeTiposIngresosLibres_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('de_ingresos_libres');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_ingresos_libres');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        // $this->db->set('created_at',utils::now());
        $response = $this->db->insert('de_ingresos_libres', $contents);
        return $response;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        // $this->db->set('updated_at',utils::now());
        return $this->db->update('de_ingresos_libres', $contents);
    }

    public function delete($where = false){
        $response = 0;
        if(is_array($where) && count($where)>0){
            $this->db->where($where);
            $response = $this->db->delete('de_ingresos_libres');
        }
        return $response;
    }


}