<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Archivos_model extends CI_Model {

    public $rows_names;

    function __construct()
    {
        parent::__construct();
       
    }

    public function get($where = false){
        $this->db            
            ->from('archivos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select('nombreArchivo')
            ->from('archivos')
            ->order_by('archivos.id');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $this->db->set('created_at',utils::get_datetime());
        $response = $this->db->insert('archivos', $contents);
        return ($response)? $this->db->insert_id() : false;
    }

}