<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaTransacciones_model extends CI_Model {

    public $rows_names;

    const ORIGEN_CHEQUE = 'CHEQUE';
    const ORIGEN_CAJA = 'CAJA';

    function __construct()
    {
        parent::__construct();
        $this->rows_names = array(
            'id',
            'departamento_id'
        );
    }

    public function get($where = false){
        $this->db
            ->select([
                'id',
                'cliente_id',
                'folio',
                'fecha',
                'poliza_fija_id',
                'estatus_id',
                'persona_id'
            ])
            ->from('ca_transacciones');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function get_poliza_xml($where = false){
        $this->db
            ->from('wv_polizas_fijas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select([
                'id',
                'cliente_id',
                'folio',
                'fecha'
            ])
            ->from('ca_transacciones');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_transacciones.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents,$id = false) {
        $uuid = ($id == false)? utils::guid() : $id;
        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('ca_transacciones', $contents);
        return ($response)? $uuid : false;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('ca_transacciones', $contents);
    }

    public function delete($where)
    {
        $this->db->where($where);
        $this->db->set('deleted_at',utils::now());
        return $this->db->update('ca_transacciones');
    }


    public function get_max_folio($where = false){
        $this->db
            ->select_max('folio')
            ->from('ca_transacciones');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $query->num_rows() > 0 ? $resultado['folio'] : 0;
    }

    public function getTransacionFactura($folio){
        $this->db
            ->select([
              'ca_transacciones.id',
              'de_facturas.pdf',
              'de_facturas.xml',
              'de_facturas.sat_uuid',
              'de_facturas.factura as factura_id'
            ])
            ->from('ca_transacciones')
            ->join('de_facturas','de_facturas.transaccion_id = ca_transacciones.id')
            ->where('ca_transacciones.folio', $folio)
            ->where('de_facturas.completo', 'si');
        $query = $this->db->get();
        // echo $this->db->last_query(); exit();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

}