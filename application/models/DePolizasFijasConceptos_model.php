<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DePolizasFijasConceptos_model extends CI_Model {

    public function get($where = false){
        $this->db
            ->from('de_polizas_fijas_conceptos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_polizas_fijas_conceptos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->order_by('id','asc');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}