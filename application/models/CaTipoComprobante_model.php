<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaTipoComprobante_model extends CI_Model {

  
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('ca_tipo_comprobante');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('ca_tipo_comprobante')
            ->order_by('id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_tipo_comprobante.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}