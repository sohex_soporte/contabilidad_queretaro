<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaFormatoFlujoEfectivo extends CI_Model {
 
    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->select([
                'ca_formato_flujo_efectivo.*',
                'ca_bancos.nombre as banco_nombre'
            ])
            ->from('ca_formato_flujo_efectivo')
            ->join('ca_bancos','ca_bancos.id = ca_formato_flujo_efectivo.banco_id');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select([
                'ca_formato_flujo_efectivo.*',
                'ca_bancos.nombre as banco_nombre'
            ])
            ->from('ca_formato_flujo_efectivo')
            ->join('ca_bancos','ca_bancos.id = ca_formato_flujo_efectivo.banco_id')
            ->order_by('created_at','asc');

        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('ca_formato_flujo_efectivo.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents) {
        $uuid = utils::guid();

        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('ca_formato_flujo_efectivo', $contents);
        return ($response)? $uuid : false;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('ca_formato_flujo_efectivo', $contents);
    }

}