<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DePolizasFijas_model extends CI_Model {

    public function get($where = false)
    {
        $this->db
            ->select([
                'id',
                'id_poliza_fija',
                'fecha_poliza'
            ])
            ->from('de_polizas_fijas');

        if(is_array($where)){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false)
    {
        $this->db
            ->select([
                'id',
                'id_poliza_fija',
                'fecha_poliza'
            ])
            ->from('de_polizas_fijas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $id = utils::guid();
        $this->db->set('id',$id);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_polizas_fijas', $contents);
        return ($response != false)? $id : false;
    }
}