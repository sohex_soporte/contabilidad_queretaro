<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeReporteContenido_model extends CI_Model {

    public function insert($contents)
    {
        $uuid = utils::guid();

        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        // $this->db->set('updated_at',utils::now());
        $this->db->insert('de_reporte_contenido', $contents);
        return $uuid;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('de_reporte_contenido', $contents);
    }

    public function delete($where = false){
        $response = 0;
        if(is_array($where) && count($where)>0){
            $this->db->where($where);
            $response = $this->db->delete('de_reporte_contenido');
        }
        return $response;
    }

    public function get($where = false){
        $this->db
            ->from('de_reporte_contenido');

        if(is_array($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function get_contenido($where = false){
        $this->db
            ->select('
                de_reporte_contenido.reporte_reglon_id,
                de_reporte_reglones.formato, 
                de_reporte_reglones.tipo, 
                de_reporte_reglones.nombre, 
                de_reporte_contenido.enero, 
                de_reporte_contenido.febrero, 
                de_reporte_contenido.marzo, 
                de_reporte_contenido.abril, 
                de_reporte_contenido.mayo, 
                de_reporte_contenido.junio, 
                de_reporte_contenido.julio, 
                de_reporte_contenido.agosto, 
                de_reporte_contenido.septiembre, 
                de_reporte_contenido.octubre, 
                de_reporte_contenido.noviembre, 
                de_reporte_contenido.diciembre, 
                de_reporte_contenido.total
            ')
            ->from('de_reporte_contenido')
            ->join('de_reporte_reglones','de_reporte_contenido.reporte_reglon_id = de_reporte_reglones.id')
            ->order_by('de_reporte_reglones.orden','asc');

        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get_contenido_suma($where = false){
        $this->db
            ->select_sum('de_reporte_contenido.enero')
            ->select_sum('de_reporte_contenido.febrero')
            ->select_sum('de_reporte_contenido.marzo')
            ->select_sum('de_reporte_contenido.abril')
            ->select_sum('de_reporte_contenido.mayo')
            ->select_sum('de_reporte_contenido.junio')
            ->select_sum('de_reporte_contenido.julio')
            ->select_sum('de_reporte_contenido.agosto')
            ->select_sum('de_reporte_contenido.septiembre')
            ->select_sum('de_reporte_contenido.octubre')
            ->select_sum('de_reporte_contenido.noviembre') 
            ->select_sum('de_reporte_contenido.diciembre')
            ->select_sum('de_reporte_contenido.total')
            ->from('de_reporte_contenido');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }
}