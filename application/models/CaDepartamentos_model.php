<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaDepartamentos_model extends CI_Model {

    public $rows_names;

    function __construct()
    {
        parent::__construct();
        $this->rows_names = array(
            'id',
            'nombre',
            'descripcion'
        );
    }

    public function get($where = false){
        $this->db
            ->select($this->rows_names)
            ->from('departamentos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->select($this->rows_names)
            ->from('departamentos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('departamentos.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}