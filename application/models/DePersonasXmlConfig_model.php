<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DePersonasXmlConfig_model extends CI_Model {

    public function get($where = false)
    {
        $this->db->from('de_personas_xml_config');
        if(is_array($where)){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    // $this->db->where('de_facturas.deleted_at IS NULL',null, false);

    public function insert($contents)
    {
        $id = utils::guid();
        $this->db->set('id',$id);
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $response = $this->db->insert('de_personas_xml_config', $contents);
        return ($response != false)? $id : false;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('de_personas_xml_config', $contents);
    }


    public function delete($where){
        $this->db->where($where);
        return $this->db->delete('de_poliza_fijas_asientos');
    }
}