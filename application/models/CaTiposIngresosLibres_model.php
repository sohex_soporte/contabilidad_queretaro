<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaTiposIngresosLibres_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function get($where = false){
        $this->db
            ->from('ca_tipos_ingresos_libres');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('ca_tipos_ingresos_libres');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}