<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

    <head>
        <?php echo $header; ?>
    </head>

    <body id="page-top">
        <div id="wrapper">
            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content" class="">
                    
                    <div class="col-12">
                        <?php echo $breadcrumb; ?>
                    </div>
                    <div class="col-12 mt-4">
                    </div>
                        <?php echo $title; ?>
                        <hr/>
                        <?php echo $content; ?>
                    </div>



                </div>
                <?php echo $footer; ?>
            </div>
        </div>
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

    </body>
</html>