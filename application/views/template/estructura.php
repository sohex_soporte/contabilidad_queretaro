<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

    <head>
        <?php echo $header; ?>
    </head>

    <body id="page-top">
        <div id="wrapper">
            <?php echo $sidebar; ?>

            <div id="content-wrapper" class="d-flex flex-column">
                <div class="headertitle" id="title_contabilidad">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="ml-3 mt-4">Contabilidad DMS <small>- QUERETARO</small></h4>
                        </div>
                        <div class="col-md-2">
                            <center>
                                <a href="<?php echo site_url('administrador/logout') ?>"><i style="color:#fff" class="fas fa-fw fa-sign-out-alt fa-2x mt-1"></i></a>
                            </center>
                            <center>
                            <small>Cerrar sesión</small>
                            </center>
                        </div>
                    </div>
                </div>
                <div id="content" class="p-3">
                    
                    <div class="col-12">
                        <?php echo $breadcrumb; ?>
                    </div>
                    <div class="col-12 mt-4">
                    </div>
                        <?php echo $title; ?>
                        <hr/>
                        <?php echo $content; ?>
                    </div>
                    
                </div>
                <?php echo $footer; ?>
            </div>
        </div>
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

    </body>
</html>