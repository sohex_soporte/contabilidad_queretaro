
<!-- Core plugin JavaScript-->
<script type="text/javascript" src="<?php echo base_url();?>assets/tema/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script type="text/javascript" src="<?php echo base_url();?>assets/tema/js/sb-admin-2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/general.js"></script>

<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/tema/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/components/bootstrap-4.6/js/bootstrap.min.js'); ?>" ></script>

<script type="text/javascript" src="<?php echo base_url('assets/libraries/jquery-loading-overlay-master/dist/loadingoverlay.min.js'); ?>"></script>

<script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous"></script>

<?php echo $contenido; ?>

