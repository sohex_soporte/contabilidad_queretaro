<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url(); ?>/administrador/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Inicio</span></a>
    </li>
    <?php
    if ($this->session->userdata('perfil_id') == 1) { ?>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdministrador" aria-expanded="true" aria-controls="collapseAdministrador">
            <i class="fas fa-fw fa-folder"></i>
            <span>Administrar</span>
        </a>
        <div id="collapseAdministrador" class="collapse" aria-labelledby="" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('administrador/usuarios') ?>">Usuarios</a>
                <a class="collapse-item" href="<?php echo site_url('administrador/reportes') ?>">Reportes</a>
                <a class="collapse-item" href="<?php echo site_url('administrador/cuentas') ?>">Cuentas</a>
            </div>
        </div>
    </li>
    <?php } ?>
    
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('cheques/personas/index/cliente'); ?>">
            <i class="fas fa-users"></i>
            <span>Clientes</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('cheques/personas/index/proveedor'); ?>">
            <i class="fas fa-users"></i>
            <span> Proveedores</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('polizas'); ?>">
        <i class="fas fa-book"></i>
            <span>Ver Polizas</span></a>
    </li>
    


    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('polizas_fijas'); ?>">
        <i class="fas fa-book"></i>
            <span>Ver Polizas Fijas</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('polizas/xml'); ?>">
        <i class="fas fa-book"></i>
            <span>Ver Polizas XML</span></a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('cheques'); ?>">
            <i class="fas fa-money-check-alt"></i>
            <span>Cheques</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('cheques/anticipos'); ?>">
            <i class="fas fa-money-check-alt"></i>
            <span>Anticipos</span></a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/cuentas/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Cuentas</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/asientos">
        <i class="fas fa-list"></i>
            <span>Asientos</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/balanza_comprobacion/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Balanza de comprobación</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/autorizaciones/clientes">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Autorizar Creditos</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/cobranza/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Cobranza</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('facturacion'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Facturas</span></a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('autos_nuevos/solicitudes_facturas'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Autos nuevos</span></a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('autos_seminuevos/solicitudes_facturas'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Autos seminuevos</span></a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('linea_autos/lista'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Linea autos</span></a>
    </li>
   
    <hr class="sidebar-divider my-0">

  


    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFacturacion" aria-expanded="true" aria-controls="collapseFacturacion">
            <i class="fas fa-fw fa-folder"></i>
            <span>Facturación</span>
        </a>
        <div id="collapseFacturacion" class="collapse" aria-labelledby="" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('facturacion/lista') ?>">Facturación</a>
                <a class="collapse-item" href="<?php echo site_url('facturacion_vehiculos/lista') ?>">Facturación vehículo</a>
                <a class="collapse-item" href="<?php echo site_url('complemento_pago/lista_complementos') ?>">Complementos vehículo</a>
            </div>
        </div>
    </li>
    <!-- <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReportes" aria-expanded="true" aria-controls="collapseReportes">
            <i class="fas fa-fw fa-folder"></i>
            <span>Balanzas</span>
        </a>
        <div id="collapseReportes" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('reportes/balanza_mylsa') ?>">Balanza Mylsa</a>
                <a class="collapse-item" href="<?php echo site_url('reportes/balance_consejo') ?>">Balance Consejo</a>
                <a class="collapse-item" href="<?php echo site_url('reportes/balance_ford') ?>">Balance Ford</a>
            </div>
        </div>
    </li> -->
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEstadosFinancieros" aria-expanded="true" aria-controls="collapseEstadosFinancieros">
            <i class="fas fa-fw fa-folder"></i>
            <span>Reportes</span>
        </a>
        <?php $reportes = $this->session->userdata('reportes'); ?>
        <div id="collapseEstadosFinancieros" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <?php foreach ($reportes as $reporte){ ?>
                <a class="collapse-item" href="<?php echo site_url("estados_financieros/generico") . '?reporte_id='.base64_encode($reporte['id']) ?>"><?php echo $reporte['nombre']; ?></a>
                <?php } ?>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFormatosEfectivo" aria-expanded="true" aria-controls="collapseFormatosEfectivo">
            <i class="fas fa-fw fa-folder"></i>
            <span>Formatos de efectivo</span>
        </a>
        <div id="collapseFormatosEfectivo" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/administrador_contenido') ?>">Administrador Contenido</a>
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/flujo_efectivo') ?>">Formato Flujo de efectivo</a>
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/ingresos') ?>">Ingresos</a>
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/ingresos_diarios') ?>">Ingresos diarios</a>
                
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/flujo_bancos') ?>">Formato de flujo de bancos</a>
                
                
            </div>
        </div>
    </li>
    <!-- <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/servicio_gral/ServicioGral">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Servicio Gral</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/estado_resultados/ResultadosSanjuan">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Edo. Res. Analitico</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/refacciones_sjr/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Refacciones SJR</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/seminuevos/reporte/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Seminuevos</span></a>
    </li> -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" onclick="ajustar_menu();" id="sidebarToggle"></button>
    </div>

</ul>

<script>
function ajustar_menu(){
    if($('#page-top.sidebar-toggled').length == 0){
        $('#page-top').removeClass('sidebar-toggled');
        $('#accordionSidebar').removeClass('toggled');
    }else{
        $('#page-top').addClass('sidebar-toggled');
        $('#accordionSidebar').addClass('toggled');
    }
    
}
// $(function(){
//     ajustar_menu();
// })
</script>