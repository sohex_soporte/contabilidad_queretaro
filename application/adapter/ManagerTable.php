<?php

class ManagerTable
{
	public $ci;
	public $tbl_content;
    function __construct(string $table,$columns = '*',$where = false) {
		$this->ci =& get_instance();	

		$this->ci->db->select($columns);
		if($where != false){
			$this->ci->db->where($where);
		}
		$query = $this->ci->db->get($table);
		$this->tbl_content = $query->result();
	}

	function search($columna,$busqueda){
		$result = null;
		if(is_array($this->tbl_content)){
			foreach ($this->tbl_content as $key => $value) {
				if(array_key_exists($columna,(array)$value) && $value->{$columna} == $busqueda ){
					$result = $value;
					break;
				}
			}
		}
		return $result;
	}
}
