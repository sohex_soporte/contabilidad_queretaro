<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class RestServer extends RestController
{
    public static $ESTATUS_WARNING = '200';
    public static $ESTATUS_OK = '201';
    public static $ESTATUS_NO_AUTORIZADO = '401';
    public static $ESTATUS_ERROR = '406';

    public $status = 'ok';
    public $data = null;
    public $message = array();
    public $code = null;
    public $content = null;
    public $events = array();

    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function send()
    {
        switch ($this->status) {
            case 'ok':
                $this->code = self::$ESTATUS_OK;
                break;
            case 'error':
                $this->code = self::$ESTATUS_ERROR;
                break;
            default:
                $this->code = self::$ESTATUS_WARNING;
                break;
        }

        $response = [
            'status' => $this->status,
            'data' => $this->data,
            'message' => $this->message,
            // 'events' => $this->events
        ];
        $this->response($response, $this->code);
    }
}
