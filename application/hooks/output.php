<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Output{

    private $ci;

    public function  __construct() {
    	$this->ci = & get_instance();
    }


    public function render_template(){

        $module = strtolower($this->ci->router->module);
        $controller = strtolower($this->ci->router->class); # CONTROLADOR
        $method = strtolower($this->ci->router->method);    # METODO

        if (!$this->ci->session->userdata('logged_in') || $module == 'inicio') {
            print $this->ci->output->get_output();
            exit();
        }


        $this->ci->load->library('template');
        // if((int)$this->ci->session->userdata('acceso') == 1){
        //     $this->ci->template->set_template('template_iframe');
        //     # HEADER
        //     $encabezado_pagina = [
        //         'contenido' => $this->ci->blade->_yield('style')
        //     ];
        //     $this->ci->template->write('header',$this->ci->load->view('template/header',$encabezado_pagina,true));
        //     #CONTENT
        //     $contenido = $this->ci->blade->_yield('contenido');
        //     $this->ci->template->write('content',$contenido);
        //     #FOOTER
        //     $pie_pagina = [
        //         'contenido' => $this->ci->blade->_yield('script')
        //     ];
        //     $this->ci->template->write('footer', $this->ci->load->view('template/footer',$pie_pagina,TRUE));
        // }else{
             # HEADER
             $encabezado_pagina = [
                'contenido' => $this->ci->blade->_yield('style')
            ];
            $this->ci->template->write('header',$this->ci->load->view('template/header',$encabezado_pagina,true));
            # SIDEBAR
            $this->ci->template->write('sidebar',$this->ci->load->view('template/sidebar',[],true));

             #BREADCUMB
             $title = $this->ci->blade->_yield('title');
             $this->ci->template->write('title',$title);

            #BREADCUMB
            $breadcrumb = $this->ci->blade->_yield('breadcrumb');
            $this->ci->template->write('breadcrumb',$breadcrumb);
            #CONTENT
            $contenido = $this->ci->blade->_yield('contenido');
            $this->ci->template->write('content',$contenido);
            #FOOTER
            $pie_pagina = [
                'contenido' => $this->ci->blade->_yield('script')
            ];
            $this->ci->template->write('footer', $this->ci->load->view('template/footer',$pie_pagina,TRUE));
        // }
        
        print($this->ci->template->render('',true));
        exit();

    }
    
    private function login($controller,$method) {
        $this->ci->load->library('template');

        #HEADER
        $data = array(
            // 'message' => $this->ci->session->flashdata('message'),
            'controller' => $controller,
            'method' => $method,
        );
        $this->ci->template->write('header',$this->ci->load->view('template/header',$data,TRUE));
        
        #SIDEBAR
        // $this->ci->load->model('CaControladores_model');
        // $controladores = $this->ci->CaControladores_model->get();

        // if(is_array($controladores) && count($controladores)>0){
        //     $this->ci->load->model('CaMetodos_model');
        //     foreach ($controladores as $keyControlador => $Controlador) {
        //         $controladores[$keyControlador]['metodos'] = $this->ci->CaMetodos_model->get_idControlador( $Controlador['id_Controlador'] );
        //     }
        // }
        // $this->ci->template->write('sidebar',$this->ci->load->view('template/sidebar',array('controladores'=>$controladores),TRUE));
        
        #TITLE
        // $detalle = $this->ci->CaControladores_model->getInformacionModulo($controller,$method);
        // $this->ci->template->write('title',$this->ci->load->view('template/title',array('detalle'=>$detalle),TRUE));
        
        #CONTENT
        $this->ci->template->write('content',$this->ci->output->get_output());
        
        #FOOTER
        $this->ci->template->write('footer', $this->ci->load->view('template/footer',FALSE,TRUE));

        print($this->ci->template->render('',true));
        exit();
    }
    
    private function metronic($controller,$method) {
        
        $this->ci->load->library('template');
        $this->ci->template->set_template('metronic');
        $this->ci->carabiner->css('styles.css');
        #HEADER
        $data = array(
            // 'message' => $this->ci->session->flashdata('message'),
            'controller' => $controller,
            'method' => $method,
        );
        $this->ci->template->write('header',$this->ci->load->view('template/header',$data,TRUE));
        
        #SIDEBAR
        // $this->ci->load->helper('text');
        // $this->ci->load->model('CaControladores_model');
        // $userId = $this->ci->ion_auth->get_user_id();
        // $controladores = $this->ci->CaControladores_model->getControladores_usuario($userId);
        
        // $user = $this->ci->ion_auth->user()->row();
        // $user_groups = $this->ci->ion_auth->get_users_groups()->result();
        
        // $this->ci->load->model('CaNiveles_model');
        // $nivelUsuario = $this->ci->CaNiveles_model->getNiveles_usuario($userId);
        // define('_SOPORTE', in_array(1, $nivelUsuario));
        // define('_ADMINISTRADOR', in_array(2, $nivelUsuario));
        // define('_AUDITOR', in_array(3, $nivelUsuario));
        // define('_CONTROLADOR', in_array(4, $nivelUsuario));

        // $this->ci->load->model('CaDependencias_model');
        // $dependencia = $this->ci->CaDependencias_model->getDependencia($user->id_Dependencia);

        // if(is_array($controladores) && count($controladores)>0){
        //     $this->ci->load->model('CaMetodos_model');
        //     foreach ($controladores as $keyControlador => $Controlador) {
        //         $controladores[$keyControlador]['Nombre'] = $Controlador['Nombre'];
        //         $controladores[$keyControlador]['metodos'] = $this->ci->CaMetodos_model->getMetodos_usuario( $Controlador['id_Controlador'],$userId );
        //     }
        // }
        $this->ci->template->write('sidebar',$this->ci->load->view('template/sidebar',array('controladores'=>array()),TRUE));
        
        #TITLE
        // $detalle = $this->ci->CaControladores_model->getInformacionModulo($controller,$method);
        // $this->ci->template->write('title',$this->ci->load->view('template/title',array('detalle'=>$detalle),TRUE));
        
        #CONTENT
        $this->ci->template->write('content',$this->ci->output->get_output());
        
        #FOOTER
        // $this->ci->template->write('footer', $this->ci->load->view('template/footer',FALSE,TRUE));
//        $this->ci->CaControladores_model->registraMovimiento();
        print($this->ci->template->render('',true));
        
        exit();
    }
}