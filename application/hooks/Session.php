<?php

use Illuminate\Database\Capsule\Manager as Capsule;
class Session
{

    public $CI;

    function __construct()
    {
    }

    public function check_auth($params)
    {
        // utils::pre($_SESSION);
        $this->CI = &get_instance();

        // $capsule = new Capsule;
        // $capsule->addConnection([
        //     'driver'    => 'mysql',
        //     'host'      => $this->CI->db->hostname,
        //     'database'  => $this->CI->db->database,
        //     'username'  => $this->CI->db->username,
        //     'password'  => $this->CI->db->password,
        //     'charset'   => $this->CI->db->char_set,
        //     'collation' => $this->CI->db->dbcollat,
        //     'prefix'    => $this->CI->db->dbprefix
        // ]);
        // $capsule->setAsGlobal();
        // $capsule->bootEloquent();
        
        $module = $this->CI->router->module;
        $controller = $this->CI->router->class; # CONTROLADOR
        $method = current(explode('.', $this->CI->router->method)); # METODO

        // if (!($module == 'facturacion' && $controller == 'facturacion' && $method == 'api_genera_factaura')) {
        //     log_message('debug', 'hook 1');

        if(!(isset($this->CI->type) && $this->CI->type == 'API') && !($controller == 'inicio' && $method == 'index') && !($controller == 'swagger' && $method == 'json') && !($controller == 'logger' && $method == 'index') ) {
            // utils::pre($_SESSION);
            $acceso = $this->CI->input->get('acceso');
            if(in_array((int)$acceso,array(1,2))){
                $this->CI->session->set_userdata('acceso',$acceso);
                $this->CI->session->set_userdata('logged_in',true);
            }else{
                // log_message('debug', 'hook 2');
                // log_message('debug', $this->CI->router->class);
                // log_message('debug', $this->CI->type);
                if ($this->CI->router->class != 'api' ) { //&& isset($this->CI->type) && $this->CI->type != 'api'
                    $modulos_libres = $params['modulos_libres'];
                    log_message('debug', 'hook 3');
                    if (!$this->CI->input->is_ajax_request() && !in_array($this->CI->router->module, $modulos_libres)) {
                        log_message('debug', 'hook 4');
                        if (!$this->CI->session->userdata('logged_in')) {
                            log_message('debug', 'hook 5');
                            redirect('inicio/index');
                        }
                    }  
                    log_message('debug', 'hook 6');
                    if ($this->CI->session->userdata('perfil_id') != 1 && $this->CI->router->module == 'administrador' && $this->CI->router->class == 'reportes') {
                        log_message('debug', 'hook 7');
                        redirect('inicio/index');
                    }
                }
            }
        }
    }
}
